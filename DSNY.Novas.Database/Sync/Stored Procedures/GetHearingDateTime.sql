﻿create procedure [sync].[GetHearingDateTime]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	AgencyId, 
	HearingFromDate, 
	HearingToDate,HearingTime 
	from dbo.BS_HearingDateTime 
	where IsMobileRecord='Y' 
	order by AgencyId,HearingTime;
end
go
