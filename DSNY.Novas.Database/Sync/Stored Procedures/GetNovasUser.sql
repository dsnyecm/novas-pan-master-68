﻿create procedure [sync].[GetNovasUser]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select UserId, NfcUid, Salt, UserHash from dbo.NovasUser;	
end
go