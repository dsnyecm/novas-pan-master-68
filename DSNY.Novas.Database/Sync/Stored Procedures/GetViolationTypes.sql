﻿create procedure [sync].[GetViolationTypes]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from 
	dbo.BS_ViolationTypes 
	where IsMobileRecord='Y' 
	order by ViolationGroupId,Sequence,Name;
end
go