
-- =============================================
-- Author:		Sara Morsi
-- Create date: 11/15/2017
-- Description:	Stored Proc for LaterSvcInfo
-- =============================================
create procedure [sync].[GetLaterSvcInfo]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from HH_LaterSvcInfo where DLDeviceId='@HHID';
end
go
