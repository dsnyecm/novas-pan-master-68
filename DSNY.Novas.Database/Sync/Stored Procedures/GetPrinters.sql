﻿create procedure [sync].[GetPrinters]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	DeviceId,
	DeviceName,
	IPAddress,
	SerialNumber,
	Status,
	Type,
	IsMobileDevice,
	MacAddress 
	from dbo.BS_HHdevices 
	where type IN ('P','E') and status='I';
end
go