﻿create procedure [sync].[GetViolator]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select @recordCount = count(1) from bs_hhtPropertyDetails;

	with Violator as
	(
		select 
		BoroCode, 
		StreetCode, 
		LowHouseNo, 
		BatchDate, 
		BusinessName, 
		ViolationCode, 
		NextViolationCode, 
		MinFine, 
		MaxFine, 
		NoOfTimes, 
		DisplaySign,
		row_number() over(order by BoroCode) as rownum 
		from dbo.BS_HHViolator
	)
		select 
		BoroCode, 
		StreetCode, 
		LowHouseNo, 
		BatchDate, 
		BusinessName, 
		ViolationCode, 
		NextViolationCode, 
		MinFine, 
		MaxFine, 
		NoOfTimes, 
		DisplaySign,
		rownum
		from Violator
		where rownum between @startRowNum and @endRowNum 
		order by rownum, BoroCode, StreetCode, LowHouseNo, ViolationCode;
end
go