﻿create procedure [sync].[GetBoroMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	BoroCode as BoroId, 
	BoroName as Name, 
	BoroId as ShortName
	from dbo.BS_BoroMaster
	WHERE BoroCode <> 0;
end
go