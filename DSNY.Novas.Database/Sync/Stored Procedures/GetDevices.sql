﻿create procedure [sync].[GetDevices]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	DeviceId, 
	DeviceName, 
	IPAddress, 
	SerialNumber, 
	Status, 
	Type as DeviceType, 
	IsMobileDevice, 
	OwnerLoaction as OwnerLocation, 
	CurrentLocation,
	Salt,
	SerializedPrivateKey,
	SerializedCertificate,
	Certificate,
	MacAddress
	from [Masterdata].[BS_HHDevices];
end
go
