﻿create procedure [sync].[GetRoutingTime]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	BoroCode,
	DistrictId,
	SectionId,
	EffectDate,
	RoutingTimeAM,
	RoutingTimePM,
	Violationtype 
	from BS_RoutingTime 
	where IsMobileRecord='Y' 
	order by BoroCode,DistrictId,SectionId,EffectDate desc;
end
go