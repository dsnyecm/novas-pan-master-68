﻿create procedure [sync].[GetAddressSuffixMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select
	SuffixId, 
	SuffixName 
	from dbo.BS_AddressSuffixMaster 
	where IsMobileRecord='Y';
end
go