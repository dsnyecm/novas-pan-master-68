﻿create procedure [sync].[SaveDeviceTicketRanges]
	@DeviceId char(50),
	@NovRangeId int,
	@RangeStatus char(1),
	@TicketStartNumber varchar(25),
	@TicketEndNumber varchar(25),
	@LastTicketNumber varchar(25)
as

begin

	set nocount on;

	if  exists (select * from [Ticketdata].[BS_DeviceTicketRanges] where DeviceId = @DeviceId 
	and NOVRangeId = @NovRangeId
	and (RangeStatus='C' or RangeStatus='A'))
		begin
			 update [Ticketdata].[BS_DeviceTicketRanges] 
			 set LastTicketNumbe=@LastTicketNumber, RangeStatus=@RangeStatus
			 where DeviceId = @DeviceId and NOVRangeId = @NovRangeId;
		end

end
go