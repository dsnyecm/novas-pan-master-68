-- =============================================
-- Author:		Sara Morsi
-- Create date: 11/15/2017
-- Description:	Stored Proc for LaterSvcData
-- =============================================
create procedure [sync].[GetLaterSvcData]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from HH_LaterSvcData where DLDeviceId='@HHID';
end
go
