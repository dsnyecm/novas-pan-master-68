﻿CREATE PROCEDURE [sync].[GetBroadcastMessages]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
AS
begin
	set nocount on;

	select * from dbo.BS_BroadcastMessages where IsMobileRecord='Y' and Broadcastdate>=convert(char(12), GetDate());
end
go
