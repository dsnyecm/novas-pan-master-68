﻿create procedure [sync].[GetViolationScriptVariables]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	FieldName,
	Description,
	Type,
	Mask,
	MapField,
	IsMultiplesAllowed,
	IsOtherAllowed,
	IsFreeForm 
	from dbo.BS_ViolationScriptVariables 
	order by FieldName;
end
go