﻿create procedure [sync].[GetHolidayMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from BS_HolidayMaster;
end
go