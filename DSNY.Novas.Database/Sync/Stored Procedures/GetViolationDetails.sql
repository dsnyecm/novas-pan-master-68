﻿create procedure [sync].[GetViolationDetails]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from bs_violationdetails 
	where IsMobileRecord='Y' 
	order by ViolationGroupId,Sequence,ViolationCode,InternalShortDescription;
end
go