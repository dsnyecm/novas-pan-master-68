﻿create procedure [sync].[GetViolationGroups]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;
	
	select 
	ViolationGroupId,
	GroupName,
	TypeName,
	Sequence 
	from bs_violationgroups 
	where IsMobileRecord='Y' 
	order by Sequence,GroupName;
end
go
