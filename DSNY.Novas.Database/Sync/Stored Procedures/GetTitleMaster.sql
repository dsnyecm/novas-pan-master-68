﻿create procedure [sync].[GetTitleMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select TitleId,TitleName from BS_TitleMaster;
end
go