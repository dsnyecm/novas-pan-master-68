﻿create procedure [sync].[GetHearingDates]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select @recordCount = count(1) from dbo.BS_HearingDates where IsMobileRecord='Y';

	with HearingDates as
	(
		select 
		BoroCode, 
		CurrentDate, 
		Timestamp,
		row_number() over(order by BoroCode) as rownum
		from dbo.BS_HearingDates 
		where IsMobileRecord='Y'
	)
		select 
		BoroCode, 
		CurrentDate, 
		Timestamp,
		row_number() over(order by BoroCode) as rownum
		from HearingDates 
		where rownum between @startRowNum and @endRowNum 
		order by rownum;
end
go