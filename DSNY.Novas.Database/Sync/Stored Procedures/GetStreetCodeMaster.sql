﻿create procedure [sync].[GetStreetCodeMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select @recordCount = count(1) from dbo.BS_StreetCodeMaster;

	with ScreetCodeMaster as
	(
		select
		BoroCode,
		StreetCode,
		StreetName,
		LGC,
		SNC,
		CreatedDate,
		EndEffDate,
		row_number() over(order by BoroCode) as rownum
		from dbo.BS_StreetCodeMaster 
		where endeffdate > getdate() 
	)
		select
		BoroCode,
		StreetCode,
		StreetName,
		LGC,
		SNC,
		CreatedDate,
		EndEffDate,
		row_number() over(order by BoroCode) as rownum
		from ScreetCodeMaster
		where rownum between @startRowNum and @endRowNum
		order by rownum;
end
go