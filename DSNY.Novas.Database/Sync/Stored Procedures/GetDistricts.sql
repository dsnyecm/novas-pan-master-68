﻿create procedure [sync].[GetDistricts]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	Select 
	BoroCode, 
	DistrictId, 
	Description, 
	Name 
	from dbo.BS_Districts 
	where IsMobileRecord='Y' 
	order by BoroCode, DistrictId;
end
go