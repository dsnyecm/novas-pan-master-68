﻿create procedure [sync].[GetBoroSiteMaster]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
AS
begin
	set nocount on;

	select 
	   [BoroCode]
      ,[BoroSiteCode]
      ,[BoroSiteName]
      ,[SiteType]
      ,[ReportLevel]
	from dbo.BS_BoroSiteMaster;
end
go