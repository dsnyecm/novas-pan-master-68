﻿create procedure [sync].[GetViolationLawCodes]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from dbo.BS_ViolationLawCodes 
	where IsMobileRecord='Y' 
	order by LawId;
end
go