﻿create procedure [sync].[GetDeviceTicketRanges]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	rtrim(DeviceId) as DeviceId, 
	NOVRangeId as NovRangeId, 
	RangeStatus, 
	TicketStartNumber, 
	TicketEndNumber,
	case LastTicketNumbe
		when NULL then 0
		else LastTicketNumbe
	end as LastTicketNumber 
	from BS_DeviceTicketRanges;
end
go