﻿create procedure [sync].[GetAgencies]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	[AgencyId],
	[Name],
	[Desciption] as Description,
	[IsMobileReocrd] as IsMobileRecord,
	[CreatedBy],
	[Createddate] as CreatedDate
	from dbo.BS_Agencies;
end
go