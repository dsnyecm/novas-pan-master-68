﻿create procedure [sync].[GetPropertyDetails]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select @recordCount = count(1) from bs_hhtPropertyDetails;

	with PropertyDetails as
	(
		select 
		 BBL,
		 StreetCode,
		 LowHouseNo,
		 LowHouseSfx,
		 LowHouseInd,
		 HighHouseNo,
		 HighHouseSfx,
		 HighHouseInd,
		 MDRNo,
		 DistrictId,
		 SectionId,
		 POP,
		 ContinuousParity,
		 VacantLot,
		 Ownstatus,
		 FirstName,
		 ZipCode,
		 LowHouseKey, 
		 row_number() over(order by BBL) as rownum 
		 from bs_hhtPropertyDetails
	)
		select 
		BBL,
		PropertyDetails.StreetCode,
		LowHouseNo,
		LowHouseSfx,
		LowHouseInd,HighHouseNo,
		HighHouseSfx,
		HighHouseInd,
		MDRNo,DistrictId,
		SectionId,
		POP,
		ContinuousParity,
		VacantLot,
		Ownstatus,
		FirstName,
		ZipCode,
		LowHouseKey, 
		rownum
		from PropertyDetails
		where rownum between @startRowNum and @endRowNum 
		order by rownum, BBL,StreetCode,LowHouseKey;
end
go