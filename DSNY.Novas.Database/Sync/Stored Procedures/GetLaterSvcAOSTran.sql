
-- =============================================
-- Author:		Sara Morsi
-- Create date: 11/15/2017
-- Description:	Stored Proc for LaterSvcAOSTran
-- =============================================
create procedure [sync].[GetLaterSvcAOSTran]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select * from HH_LaterSvcAOSTran where DLDeviceId='@HHID';
end
go
