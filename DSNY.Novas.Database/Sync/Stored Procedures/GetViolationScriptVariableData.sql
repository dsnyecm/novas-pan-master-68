﻿create procedure [sync].[GetViolationScriptVariableData]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select
	FieldName,
	Data,
	Sequence 
	from dbo.BS_ViolationScriptVariableData 
	where IsMobileRecord='Y' 
	Order By FieldName,Data;
end
go