﻿create procedure [sync].[GetSections]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select 
	DistrictId, 
	SectionId, 
	Name, 
	Description, 
	Sequence 
	from [dbo].[BS_Sections];
end
go