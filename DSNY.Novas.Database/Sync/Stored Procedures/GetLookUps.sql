﻿create procedure [sync].[GetLookups]
	@startRowNum int = 1, -- default value
	@endRowNum int = 5000, -- default value
	@recordCount int output
as
begin
	set nocount on;

	select Field1 as TableName, 
	Field2 as Code, 
	Field3 as Description 
	from dbo.BS_Miscellaneous 
	Where IsMobileRecord='Y';
end
go
