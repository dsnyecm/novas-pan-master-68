﻿CREATE TABLE [dbo].[NovasUser] (
    [UserId]   VARCHAR (16)   NOT NULL,
    [Pin]      VARCHAR (25)   NOT NULL,
    [NfcUid]   VARCHAR (25)   NOT NULL,
    [Salt]     VARCHAR (25)   NOT NULL,
    [UserHash] VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_NovasUser] PRIMARY KEY CLUSTERED ([UserId] ASC)
);
