D-O SDK For Windows Phone 8.1+

Version 2.4.4.4 (8/15/2016)
- Add new WriteImage() API to allow user to adjust darkness threshold

Version 2.4.4.3 (7/06/2016)
- Change namespace from DatamaxONeil to Honeywell
- Improve performance of WriteImage() function for DocumentDPL, DocumentExPCL_LP, and LP

Version 2.4.4.2 (7/06/2016)
- Add new method WriteWithResponse() for Connection classes
- Fix issue with PrinterStatus not returning correct value when feedback mode is on

Version 2.4.4.1 (6/30/2016)
- Fix issue with printing multiple images for DocumentDPL
- Fix issue with writeImage() method clearing default memory module.
- Fix issue with xAVR and AVR version not displaying after query in PrinterInformation_DPL class.
- Fix issue with Height Dot multiplier not being set for DocumentDPL
- Fix issue with GS1 Limited Barcode reporting error on valid input in DocumentDPL
- Added getXAVRVersionInformation for PrinterInformation_DPL class
- Added getEmulationsUsedWithAutoMode(), setEmulationsUsedWithAutoMode() for SystemSettings_DPL class
- Added getBackupDistance() and setBackupDistance() for MediaLabel_DPL class

Version 2.4.4.0 (2/17/2016)
- Fix Errors in API documentations
- Fix bugs in AvalancheSettings class
- Add Advance Format Attribute capability (Font bold, font italic, etc..)
- Add WriteTextInternalSmooth() and WriteTextDownloadedBitmapped() methods

Version 2.4.3.0 (1/08/2016)
- Add readLine() method
- Fix read() methods to correctly truncate data at end sequence

Version 2.4.2.0 (1/04/2016)
- Fix data being truncated for Magnetic Card Data (ExPCL)
- Fix bugs and update DPL configuration classes in SDK to comply with XX.06_0014 firmware
- Revise logic of getCurrentStatus() for PrinterStatus_DPL class

Version 2.4.1.0 (12/20/2015)
- Move getImageRotation() and setImageRotation() from SystemSettings_DPL class to Miscellaneous_DPL class

Version 2.4.0.0 (12/01/2015)
- Add IP and MAC address validation in Connection.createClient() methods
- Add AirWatch_DPL, SmartBattery_DPL configuration classes
- Add BatteryCondition_ExPCL, MemoryStatus_ExPCL, PrintheadStatus_ExPCL, and PrinterOptions_ExPCL classes 
- Add set methods for configuration classes
- DocumentDPL.GetDocumentImageData() is now deprecated
- Change logic of GetDocumentData() to include image in print job.

Version 2.3.0.1 (11/01/2015)
- Remove optional commands <ESC>P$ and <ESC>P# in DocumentExPCL_PP class.

Version 2.3.0.0 (10/16/2015)
- Revise PrinterOption class to provide more meaningful values for query values(enum)
- Fix DocumentLP.advanceQueueMark() bug

Version 2.2.0.7 (11/09/2015)
- Fix Connection.Read() bug
- DocumentDPL.GetDocumentImageData() is now deprecated
- Change logic of GetDocumentData() to include image in print job.

Version 2.2.0.6 (2/20/2015)
- Add new API in Connection_Bluetooth.getPairedDevices() that will return a list of paired devices(name and MAC address)
- Add IP and MAC address validation in Connection.createClient() methods

Version 2.2.0.5 (2/13/2015)
- Add synchronous connection with new API CreateClient(..., boolean isAsync) for connection classes
- More optimization for query methods

Version 2.2.0.4 (2/11/2015)
- Change Bluetooth connectivity for GDR2

Version 2.2.0.3 (1/7/2015)
- Improve query response time

Version 2.2.0.2 (10/17/2014)
- New release for Windows Phone 8
- Add isBold property for EZ language for ASPAC printers (OC3, OC2, etc..)
- Revise logic to print Unicode characters for writeTextScalable.
- Add PrinterStatus_DPL class for DPL printers
- Change logic of bitmap conversion to better account for alpha channel