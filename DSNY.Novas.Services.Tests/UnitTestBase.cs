﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.Services.Tests
{
    public class UnitTestBase
    {
        [TestInitialize]
        public void UnitTestBaseInit()
        {
            var unitTestDependancyProvider = new UnitTestDependancyProvider();
            unitTestDependancyProvider.Register();
            DependencyResolver.SetDependencyProvider(unitTestDependancyProvider);
        }
    }

    public class MoqAppRuntimeSettings : AppRuntimeSettingsBase
    {
        public override string GetSqlitePath()
        {
            return string.Empty;
        }
        public override string HardwareIdentifier => "TestDeviceId";

        public override string Version => "1.0.0.1";

        public override string HHTNumber => "PAN999";

        public override string DeviceIMEI => "777222888111999";
        public override string DeviceSerialNumber => "PAN12ABC12345";
        public override string PlatformDeviceName => "PAN12ABC12345";

        public override string DeviceMemory => "0";
    }
}
