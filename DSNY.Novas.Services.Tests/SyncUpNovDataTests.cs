﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpNovDataTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateNovDataTest()
        {
            var data = await CreateNovData();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBNovData> CreateNovData()
        {
            IRepository<DBNovData> repo =
                DependencyResolver.Get<IRepository<DBNovData>>();
            
            var fieldname = await repo.GetAsync(x => x.FieldName == "FieldName");
            return fieldname;
        }
    }
}
