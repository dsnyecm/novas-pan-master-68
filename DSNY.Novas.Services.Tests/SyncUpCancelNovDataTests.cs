﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpCancelNovDataTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateCancelNovDataTest()
        {
            var data = await CreateCancelNovData();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBCancelNovData> CreateCancelNovData()
        {
            IRepository<DBCancelNovData> repo =
                DependencyResolver.Get<IRepository<DBCancelNovData>>();

            var deviceId = "deviceid";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
