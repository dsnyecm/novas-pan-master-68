﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpNovInformationTests : UnitTestBase
    {

        [TestInitialize]
        public void TestInit()
        {
            NovService novService = new NovService();
            var tickets = novService.GetViolations();
            Console.WriteLine(tickets);
        }

        [TestMethod]
        public async Task CreateNovInformationTest()
        {
            var data = await CreateNovInformation();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBNovInformation> CreateNovInformation()
        {
            IRepository<DBNovInformation> repo =
              DependencyResolver.Get<IRepository<DBNovInformation>>();

            var deviceId = "deviceid";
            var confirmeddeviceId = await repo.GetAsync(x=>x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
