﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpDeviceTicketRangesTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateDeviceTicketRangesTest()
        {
            var data = await CreateDeviceTicketRanges();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBDeviceTicketRanges> CreateDeviceTicketRanges()
        {
            IRepository<DBDeviceTicketRanges> repo =
                DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();

            var deviceId = "TestDevice";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
