﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpAffidavitOfServiceTranTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateAffidavitOfServiceTranTest()
        {
            var data = await CreateAffidavitOfServiceTran();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBAffidavitOfServiceTran> CreateAffidavitOfServiceTran()
        {
            IRepository<DBAffidavitOfServiceTran> repo =
               DependencyResolver.Get<IRepository<DBAffidavitOfServiceTran>>();

            var deviceId = "deviceid";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
