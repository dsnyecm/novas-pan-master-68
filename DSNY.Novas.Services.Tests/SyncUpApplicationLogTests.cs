﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpApplicationLogTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateApplicationLogTest()
        {
            var data = await CreateApplicationLog();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBApplicationLog> CreateApplicationLog()
        {
            IRepository<DBApplicationLog> repo =
                DependencyResolver.Get<IRepository<DBApplicationLog>>();

            var deviceId = "deviceid";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
