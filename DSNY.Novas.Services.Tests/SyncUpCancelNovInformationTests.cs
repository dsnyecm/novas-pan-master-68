﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpCancelNovInformationTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateCancelNovInformationTest()
        {
            var data = await CreateCancelNovInformation();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBCancelNovInformation> CreateCancelNovInformation()
        {
            IRepository<DBCancelNovInformation> repo =
                DependencyResolver.Get<IRepository<DBCancelNovInformation>>();

            var deviceId = "deviceid";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
