﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpAffidavitOfServiceTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateAffidavitOfServiceTest()
        {
            var data = await CreateAffidavitOfService();
            Assert.IsNotNull(data);
        }
        
        public async Task<DBAffidavitOfService> CreateAffidavitOfService()
        {
            var affidavitOfServiceRepo = DependencyResolver.Get<IRepository<DBAffidavitOfService>>();
            var deviceId = "deviceid";
            var confirmeddeviceId = await affidavitOfServiceRepo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
