﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpDutyHeadersTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateDutyHeadersTest()
        {
            var data = await CreateDutyHeaders();
            Assert.IsNotNull(data);
        }
        
        private async Task<DBDutyHeader> CreateDutyHeaders()
        {
            IRepository<DBDutyHeader> repo =
                DependencyResolver.Get<IRepository<DBDutyHeader>>();

            var deviceId = "HHT163";
            var confirmeddeviceId = await repo.GetAsync(x => x.DeviceId == deviceId);
            return confirmeddeviceId;
        }
    }
}
