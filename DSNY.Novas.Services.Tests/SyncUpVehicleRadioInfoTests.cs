﻿using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncUpVehicleRadioInfoTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateVehicleRadioInfoTest()
        {
            var data = await CreateVehicleRadioInfo();
            Assert.IsNotNull(data);
        }
        
        public async Task<DBVehicleRadioInfo> CreateVehicleRadioInfo()
        {
            IRepository<DBVehicleRadioInfo> repo =
              DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();

            var vehicleID = "456";
            var datetime = await repo.GetAsync(x => x.VehicleId == vehicleID);
            return datetime;
        }
    }
}
