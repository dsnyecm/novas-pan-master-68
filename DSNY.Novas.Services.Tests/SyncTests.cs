using System;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.Services.Tests
{
    [TestClass]
    public class SyncTests
    {
        [TestInitialize]
        public void SyncTestsInit()
        {
            var unitTestDependancyProvider = new UnitTestDependancyProvider();
            unitTestDependancyProvider.Register();
            DependencyResolver.SetDependencyProvider(unitTestDependancyProvider);
        }

        [TestMethod]
        public async Task InitializeClientDB()
        {
            // Need to run this before the sync test any time there are db changes
            // so that the old tables are dropped and new ones are created
            var repo = DependencyResolver.Get<IDatabaseSynchronizer>();
            await repo.SetUpNovasDb();
            await SetDataForSync();
        }

        [TestMethod]
        public async Task SyncTest()
        {
            try
            {
                await SetDataForSync();
                var syncService = new SyncService("http://localhost:54262/");
                await syncService.Sync();
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task SetDataForSync()
        {
            var deviceRangeRepo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
           
            await deviceRangeRepo.QueryAsync("delete from DBDeviceTicketRanges");

            var deviceTicketRange = new DBDeviceTicketRanges
            {
                DeviceId = "TestDevice",
                NovRangeId = 43,
                RangeStatus = "A",
                TicketStartNumber = 100,
                TicketEndNumber = 500,
                LastTicketNumber = 499
            };

            await deviceRangeRepo.InsertAsync(deviceTicketRange);

            var dutyHeaderRepo = DependencyResolver.Get<IRepository<DBDutyHeader>>();
            await dutyHeaderRepo.QueryAsync("delete from DBDutyHeader");

            var dutyHeader = new DBDutyHeader
            {
                LoginTimestamp = DateTime.Now,
                UserId = "TestUser",
                BoroId = "4",
                SiteId = "QNSP",
                DeviceId = "HHT161",
                TicketCount = 5,
                LogoutTimestamp = DateTime.Now,
                SentTimestamp = DateTime.Now,
                SignatureBitmap = null,
                HearingDate = DateTime.Now,
                ReportingTimestamp = DateTime.Now,
                ReceivedTimestamp = DateTime.Now
            };

            await dutyHeaderRepo.InsertAsync(dutyHeader);

            dutyHeader = new DBDutyHeader
            {
                LoginTimestamp = DateTime.Now,
                UserId = "TestUser",
                BoroId = "4",
                SiteId = "QNSP",
                DeviceId = "HHT163",
                TicketCount = 10,
                LogoutTimestamp = DateTime.Now,
                SentTimestamp = DateTime.Now,
                SignatureBitmap = null,
                HearingDate = DateTime.Now,
                ReportingTimestamp = DateTime.Now,
                ReceivedTimestamp = DateTime.Now
            };

            await dutyHeaderRepo.InsertAsync(dutyHeader);

            var novRepo = DependencyResolver.Get<IRepository<DBNovInformation>>();

            await novRepo.QueryAsync("delete from DBNovInformation");

            var novInfo = new DBNovInformation
            {
                NovNumber = 56577867,
                TicketStatus = "g",
                IssuedTimestamp = DateTime.Now,
                SystemTimestamp = DateTime.Now,
                LoginTimestamp =DateTime.Now,
                ReportLevel = "l",
                IsResp1AddressHit = "h",
                Resp1LastName = "smith",
                Resp1FirstName = "John",
                Resp1MiddleInitial = "j",
                Resp1Sex = "male",
                PropertyBBL = "BBL",
                Resp1DistrictId = "D",
                Resp1SectionId = "S",
                Resp1StreetId = 165,
                Resp1Address = "123 Maple St",
                Resp1Address1 = "123 Maple St",
                Resp1City = "New York",
                Resp1State = "New York",
                Resp1Zip = "10001",
                LicenseNumber = "546546g",
                LicenseAgency = "agency1",
                LicenseType = "hello",
                IsPlaceAddressHit = "y",
                PlaceLastName = "last",
                PlaceFirstName = "first",
                PlaceMiddleInitial = "",
                PlaceBBL = "B",
                PlaceDistrictId = "d",
                PlaceSectionId = "s",
                PlaceStreetId = 13,
                PlaceAddress1 = "add1",
                PlaceAddress2 = "add2",
                PlaceAddressDescriptor = "adddescript",
                PlaceSideOfStreet = "sideofstreet",
                PlaceCross1StreetId = 15,
                PlaceCross2StreetId = 12,
                MailableAmount = 100,
                MaximumAmount = 75,
                HearingTimestamp = DateTime.Now,
                IsAppearRequired = "yes",
                AlternateService = "no",
                BuildingType = "prewar",
                IsMultipleOffences = "yes",
                DigitalSignature = "yes",
                IsSent = "yes",
                SentTimestamp = DateTime.Now,
                IsReceived = "yes",
                ReceivedTimestamp = DateTime.Now,
                ViolationCode = "345456",
                HHTIdentifier = "45",
                ViolationScript = "hello",
                UserId = "user1",
                VoidCancelScreen = "no",
                PlaceBoroCode = "5675",
                PlaceHouseNo = "15-70",
                Resp1BoroCode = "5675",
                Resp1HouseNo = "30-40",
                ViolationGroupId = 145,
                ViolationTypeId = "h",
                LicenseExpDate = DateTime.Now.AddDays(5),
                BusinessName = "b",
                CheckSum = "45565767678",
                FreeAddrees = "h",
                DeviceId = "deviceid",
                PublicKeyId = 667,
                PrintViolationCode = "viocode",
                IsPetitionerCourtAppear = "y",
                LicenseTypeDesc = "h",
                ViolGroupName = "v",
                CodeLawDescription = "codelaw",
                OfficerName = "simpson",
                LawSection = "l",
                AbbrevName = "e",
                AgencyId = "ds",
                Title = "officier",
                OrigViolationTypeId = "4",
                OrigViolationCode = "67",
                OrigHHTIdentifier = "767",
                IsBusiness = true,
            };

            await novRepo.InsertAsync(novInfo);
            
            var affidavitOfServiceRepo = DependencyResolver.Get<IRepository<DBAffidavitOfService>>();
            var affidavitOfServiceTranRepo = DependencyResolver.Get<IRepository<DBAffidavitOfServiceTran>>();
            var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
            var cancelNovInformationRepo = DependencyResolver.Get<IRepository<DBCancelNovInformation>>();
            var cancelNovDataRepo = DependencyResolver.Get<IRepository<DBCancelNovData>>();
            var novDataRepo = DependencyResolver.Get<IRepository<DBNovData>>();
            var vehicleRadioInfoRepo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();


            await affidavitOfServiceRepo.QueryAsync("delete from DBAffidavitOfService");
            await affidavitOfServiceTranRepo.QueryAsync("delete from DBAffidavitOfServiceTran");
            await appLogRepo.QueryAsync("delete from DBApplicationLog");
            await cancelNovDataRepo.QueryAsync("delete from DBCancelNovData");
            await cancelNovInformationRepo.QueryAsync("delete from DBCancelNovInformation");
            await novDataRepo.QueryAsync("delete from DBNovData");
            await vehicleRadioInfoRepo.QueryAsync("delete from DBVehicleRadioInfo");


            var affidavitOfService = new DBAffidavitOfService
            {
                DBAffidavitOfServiceKey = 12,
                NovNumber = 1,
                PersonallyServeFlag = "ServeFlag",
                Sex = "F",
                Age = "24",
                HairColor = "Brown",
                SkinColor = "Olive",
                Height = "5feet5inches",
                Weight = "130lbs",
                OtherIdentifying = "",
                LicenseNumber = "5657676M4545",
                ExpDate = DateTime.Now.AddDays(3),
                TypeOfLicense = "drivers",
                IssuedNy = "dsny",
                AlternativeService1 = "3",
                Comments = "data",
                SignDate = DateTime.Now,
                UserId = "44564",
                PublicKeyId = 15,
                DigitalSignature = "hg",
                ServedCounty = "yes",
                ServiceTo = "",
                ServedTitle = "",
                ServedTitleOther = "",
                ServedLName = "hugh",
                ServedFName = "helen",
                ServedMInit = "",
                ServedSex = "F",
                ServedAddress = "123 Maple St",
                ServedCity = "New York",
                ServedState = "NY",
                ServedZip = "11102",
                AlternativeService2 = "2",
                PremiseLName = "",
                PremiseFName = "",
                PremiseMInit = "",
                ServedLocation = "",
                CheckSum = "12343434",
                DeviceId = "deviceid",
                LicenseTypeDesc = "",
                OfficerName = "",
                AbbrevName = "",
                AgencyId = "212",
                Title = "",
                LoginTimestamp = DateTime.Now,
                ServedHouseNo = "",
                ServedBoroCode = "",
            };

            await affidavitOfServiceRepo.InsertAsync(affidavitOfService);

            var affidavitOfServiceTran = new DBAffidavitOfServiceTran
            {
                NovNumber = 12,
                PersonallyServeFlag = "yes",
                Sex = "M",
                Age = "34",
                HairColor = "blonde",
                SkinColor = "blue",
                Height = "6feet",
                Weight = "180lbs",
                OtherIdentifying = "",
                LicenseNumber = "675757",
                ExpDate = DateTime.Now.AddDays(3),
                TypeOfLicense = "67676hm",
                IssuedNy = "",
                AlternativeService1 = "3",
                Comments = "data",
                SignDate = DateTime.Now,
                UserId = "4545",
               // PublickKeyId = "",
                DigitalSignature = "34343",
                ServedCounty = "343",
                ServiceTo = "23",
                ServedTitle = "345",
                ServedTitleOther = "title",
                ServedLName = "lname",
                ServedFName = "fname",
                ServedMInit = "",
                ServedSex = "m",
                ServedAddress = "344 grand ave",
                ServedCity = "New York",
                ServedState = "NY",
                ServedZip = "10001",
                AlternativeService2 = "",
                PremiseLName = "",
                PremiseFName = "",
                PremiseMInit = "",
                ServedLocation = "ny",
                CheckSum = "",
                DeviceId = "deviceid",
                LicenseTypeDesc = "",
                OfficerName = "",
                AbbrevName = "",
                AgencyId = "agency",
                Title = "",
                LoginTimestamp = DateTime.Now,
                ServedHouseNo = "",
                ServedBoroCode = ""
            };

            await affidavitOfServiceTranRepo.InsertAsync(affidavitOfServiceTran);

           var applicationLog = new DBApplicationLog
            {
                LogTimestamp = DateTime.Now,
                RecordNum = 12,
                ComputerName = "compname",
                ProcessTimestamp = DateTime.Now,
                LogGroup = "log",
                EventId = 445,
                UserId = "23",
                Line = 3,
                Description = "desc",
                Source = "source",
                Data = "data",
                AddTimestamp = DateTime.Now,
                DeviceId = "deviceid"
            };

            await appLogRepo.InsertAsync(applicationLog);

            var cancelNovData = new DBCancelNovData
            {
                DeviceId = "deviceid",
                SystemTimestamp = DateTime.Now, 
                FieldName = "fieldname",
                Data = "data"
            };

           await cancelNovDataRepo.InsertAsync(cancelNovData);

            var cancelNovInfo = new DBCancelNovInformation
            {
                CancelNo = "cancelno",
                DeviceId = "deviceid",
                TicketStatus = "ticketstatus",
                SystemTimestamp = DateTime.Now,
                LoginTimestamp = DateTime.Now,
                ReportLevel = "reportlvl",
                IsResp1AddressHit = "Y",
                Resp1LastName = "last",
                Resp1FirstName = "first",
                Resp1MiddleInitial = "",
                Resp1Sex = "f",
                PropertyBBL = "bblprop",
                Resp1DistrictId = "distid",
                Resp1SectionId = "secid",
                Resp1StreetId = 123456,
                Resp1Address = "123 Maple St",
                Resp1Address1 = "",
                Resp1City = "New York",
                Resp1State = "NY",
                Resp1Zip = "10001",
                LicenseNumber = "num",
                LicenseAgency = "agency",
                LicenseType = "",
                IsPlaceAddressHit = "N",
                PlaceLastName = "",
                PlaceFirstName = "",
                PlaceMiddleInitial = "",
                PlaceBBL = "",
                PlaceDistrictId = "",
                PlaceSectionId = "",
                PlaceStreetId = 123456,
                PlaceAddress1 = "123 Maple St",
                PlaceAddress2 = "",
                PlaceAddressDescriptor = "",
                PlaceSideOfStreet = "",
                PlaceCross1StreetId = 123546,
                PlaceCross2StreetId = 0,
                MailableAmount = 0,
                MaximumAmount = 0,
                HearingTimestamp = DateTime.Now.AddDays(3),
                IsAppearRequired = "Y",
                AlternateService = "",
                BuildingType = "postwar",
                IsMultipleOffences = "",
                DigitalSignature = "",
                IsSent = "Y",
                SentTimestamp = DateTime.Now,
                IsReceived = "Y",
                ReceivedTimestamp = DateTime.Today,
                ViolationCode = "",
                HHTIdentifier = "HHTident",
                ViolationScript = "",
                UserId = "userid",
                VoidCancelScreen = "",
                PlaceBoroCode = "",
                PlaceHouseNo = "",
                Resp1BoroCode = "",
                Resp1HouseNo = "",
                ViolationGroupId = "",
                ViolationTypeId = "",
                MDRNumber = "",
                LicenseExpDate = DateTime.Now,
                BusinessName = "",
                CheckSum = "",
                FreeAddress = "",
                PrintViolationCode = "",
                LicenseTypeDesc = "",
                ViolGroupName = "",
                CodeLawDescription = "",
                OfficerName = "",
                LawSection = "",
                AbbrevName = "",
                AgencyId = "",
                Title = ""
            };

          await cancelNovInformationRepo.InsertAsync(cancelNovInfo);

           var novData = new DBNovData
            {
                  NovNumber = 1246767,
                  FieldName = "FieldName",
                  Data = "Data"
            };

            await novDataRepo.InsertAsync(novData);

            var vehicleRadioInfo = new DBVehicleRadioInfo
            {
                UserId = "1234",
                LoginTimestamp = DateTime.Now,
                VehicleRadioId = 12,
                VehicleId = "456",
                RadioId = "600",
                StartMileage = 100,
                EndMileage = 600
            };

            await vehicleRadioInfoRepo.InsertAsync(vehicleRadioInfo);

        }
    }
}
