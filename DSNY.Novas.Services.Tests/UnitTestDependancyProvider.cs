﻿using System;
using System.Collections.Generic;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services.Tests
{
    public class UnitTestDependancyProvider : IDependencyProvider
    {
        private readonly Dictionary<Type, Type> _types = new Dictionary<Type, Type>();

        public T Get<T>() where T : class
        {
            return Activator.CreateInstance(_types[typeof(T)]) as T;
        }

        public void Register<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public void Register()
        {
            //_types.Add(typeof(IAlertService), typeof(MoqAlertService));
            //_types.Add(typeof(INavigationService), typeof(MoqNavService));
            //_types.Add(typeof(INovService), typeof(NovService));
            //_types.Add(typeof(IPersonBeingServedService), typeof(PersonBeingServedService));
            //_types.Add(typeof(ILookupService), typeof(LookupService));
            //_types.Add(typeof(IPlaceOfOccurrenceService), typeof(PlaceOfOccurrenceService));
            //_types.Add(typeof(IPersonCommercialIDService), typeof(PersonCommercialIDService));
            //_types.Add(typeof(ILoginService), typeof(LoginService));
            //_types.Add(typeof(IRepository<DBViolationGroups>), typeof(SQLiteRepository<DBViolationGroups>));
            //_types.Add(typeof(IRepository<DBViolationTypes>), typeof(SQLiteRepository<DBViolationTypes>));
            //_types.Add(typeof(IRepository<DBViolationDetails>), typeof(SQLiteRepository<DBViolationDetails>));
            //_types.Add(typeof(IRepository<DBViolationLawCodes>), typeof(SQLiteRepository<DBViolationLawCodes>));
            //_types.Add(typeof(IRepository<DBBoroMaster>), typeof(SQLiteRepository<DBBoroMaster>));
            //_types.Add(typeof(IRepository<DBFirstDescriptors>), typeof(SQLiteRepository<DBFirstDescriptors>));
            //_types.Add(typeof(IRepository<DBDistricts>), typeof(SQLiteRepository<DBDistricts>));
            //_types.Add(typeof(IRepository<DBLookup>), typeof(SQLiteRepository<DBLookup>));
            //_types.Add(typeof(IRepository<DBPropertyDetails>), typeof(SQLiteRepository<DBPropertyDetails>));
            //_types.Add(typeof(IRepository<DBIdMatrix>), typeof(SQLiteRepository<DBIdMatrix>));
            //_types.Add(typeof(IRepository<DBNovasUser>), typeof(SQLiteRepository<DBNovasUser>));
            _types.Add(typeof(IRepository<DBNovInformation>), typeof(SQLiteRepository<DBNovInformation>));
            _types.Add(typeof(IRepository<DBNovData>), typeof(SQLiteRepository<DBNovData>));
            _types.Add(typeof(IRepository<DBCancelNovInformation>), typeof(SQLiteRepository<DBCancelNovInformation>));
            _types.Add(typeof(IRepository<DBCancelNovData>), typeof(SQLiteRepository<DBCancelNovData>));
            _types.Add(typeof(IRepository<DBAffidavitOfServiceTran>), typeof(SQLiteRepository<DBAffidavitOfServiceTran>));
            _types.Add(typeof(IRepository<DBApplicationLog>), typeof(SQLiteRepository<DBApplicationLog>));
            _types.Add(typeof(IRepository<DBAffidavitOfService>), typeof(SQLiteRepository<DBAffidavitOfService>));
            _types.Add(typeof(IRepository<DBVehicleRadioInfo>), typeof(SQLiteRepository<DBVehicleRadioInfo>));
            _types.Add(typeof(IRepository<DBDutyHeader>), typeof(SQLiteRepository<DBDutyHeader>));
            _types.Add(typeof(IRepository<DBDeviceTicketRanges>), typeof(SQLiteRepository<DBDeviceTicketRanges>));
            _types.Add(typeof(IRepository<DBLaterSvcAOSTran>), typeof(SQLiteRepository<DBLaterSvcAOSTran>));
            _types.Add(typeof(IRepository<DBLaterSvcData>), typeof(SQLiteRepository<DBLaterSvcData>));
            _types.Add(typeof(IRepository<DBLaterSvcInfo>), typeof(SQLiteRepository<DBLaterSvcInfo>));
            _types.Add(typeof(IRepository<DBLaterSvcDutyH>), typeof(SQLiteRepository<DBLaterSvcDutyH>));
            _types.Add(typeof(IDatabaseSynchronizer), typeof(DatabaseSynchronizer));
        }
    }
}
