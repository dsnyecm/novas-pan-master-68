﻿using DSNY.Novas.Common.Interfaces;

namespace DSNY.Novas.Common
{
    public static class DependencyResolver
    {
        private static IDependencyProvider _dependencyProvider;

        public static void SetDependencyProvider(IDependencyProvider dependencyProvider)
        {
            _dependencyProvider = dependencyProvider;
        }

        public static T Get<T>() where T : class
        {
            return _dependencyProvider.Get<T>();
        }

        public static void Set<T>() where T : class
        {
            _dependencyProvider.Register<T>();
        }
    }
}
