﻿using DSNY.Novas.Common.Interfaces;

namespace DSNY.Novas.Common
{
    public abstract class AppRuntimeSettingsBase : IAppRuntimeSettings
    {
        public abstract string GetSqlitePath();
        public string DatabaseFilename => "Novas.db";
        public abstract string HardwareIdentifier { get; }
        public abstract string Version { get; }

        public abstract string HHTNumber { get; }
        public abstract string PlatformDeviceName { get; }
        public abstract string DeviceIMEI { get; }
        public abstract string DeviceSerialNumber { get; }

        public abstract string DeviceMemory { get; }

    }
}