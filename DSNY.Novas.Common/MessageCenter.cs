﻿using System;
using DSNY.Novas.Common.Interfaces;

namespace DSNY.Novas.Common
{
    public sealed class EmptyMessagingSender
    {
        internal EmptyMessagingSender()
        {

        }
    }

    public static class MessageCenter
    {

        

        private static IMessagingProvider _messagingProvider;
        private readonly static EmptyMessagingSender EmptySender = new EmptyMessagingSender();

        public static void SetMessagingProvider(IMessagingProvider messagingProvider)
        {
            _messagingProvider = messagingProvider;
        }

        public static void Unsubscribe<TSender>(object subscriber, string message) where TSender : class
        {
            _messagingProvider.Unsubscribe<TSender>(subscriber, message);
        }

        public static void Unsubscribe(object subscriber, string message)
        {
            _messagingProvider.Unsubscribe<EmptyMessagingSender>(subscriber, message);
        }

        public static void Subscribe<TSender>(object subscriber, string message, Action<TSender> callback, TSender source = null) where TSender : class
        {
            _messagingProvider.Subscribe(subscriber, message, callback, source);
        }

        public static void Subscribe(object subscriber, string message, Action<EmptyMessagingSender> callback)
        {
            _messagingProvider.Subscribe(subscriber, message, callback, EmptySender);
        }

        public static void Send<TSender>(TSender sender, string message) where TSender : class
        {
            _messagingProvider.Send(sender, message);
        }

        public static void Send(string message)
        {
            _messagingProvider.Send(EmptySender, message);
        }
    }
}
