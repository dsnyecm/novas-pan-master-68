﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.Common.Interfaces
{
    public interface IPrinter
    {
        Task<List<string>> GetPairedDevices();
        Task<bool> InitiateConnection(string macAddress, bool testPrint, Dictionary<string, string> MapFieldsToNovInfo, string summonType);
        Task<bool> InitiateConnectionForPrintReport(string macAddress, Dictionary<string, string> MapFieldsToNovInfo, string summonType);
        Task<bool> StartPrintJob();
        string GetSerialNumber();
        string GetMacAddress();
        void SetConnectionOpenSuccessCommand(ICommand connectionOpenSuccessCommand);
        void SetConnectionClosedSuccessCommand(ICommand connectionClosedSuccessCommand);
        void SetSendCommandSuccess(ICommand sendCommandSuccess);
    }
}
