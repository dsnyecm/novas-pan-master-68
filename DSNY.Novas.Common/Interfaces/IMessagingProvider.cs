﻿using System;

namespace DSNY.Novas.Common.Interfaces
{
    public interface IMessagingProvider
    {
        void Unsubscribe<TSender>(object subscriber, string message) where TSender : class;

        void Subscribe<TSender>(object subscriber, string message, Action<TSender> callback, TSender source = null) where TSender : class;

        void Send<TSender>(TSender sender, string message) where TSender : class;
    }
}
