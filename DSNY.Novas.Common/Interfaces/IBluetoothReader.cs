﻿using DSNY.Novas.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;

namespace DSNY.Novas.Common.Interfaces
{
    public interface IBluetoothReader
    {
        void FindDevices(string scanResult);
        void FindPairedDevices();
        void SetPairSuccessfulCommand(ICommand pairSuccessfulCommand);
        void SetDeviceInfoListUpdate(ICommand deviceInfoListUpdate);
        void RemoveBluetoothWatchers();

        Task<DeviceInformation> PairAsync(string name);

        Task<BluetoothDevice> FromIdAsync(string id);
        Task Pair(string name);
        Task<bool> IsPaired(string name);
        Task<List<Printer>> ListAllPrinters();
        IEnumerable<Printer> Printers { get; set; }
    }
}
