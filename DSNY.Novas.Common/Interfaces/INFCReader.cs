﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.Common.Interfaces
{
    public interface INFCReader
    {
        Task<bool> IsNFCAvailable();

        Task<bool> IsNFCEnabled();

        Task<bool> NavigateToNFCSettings();

        Task<bool> StartListening();

        void StopListening();

        ICommand CardRecognizedCommand { get; set; }

        ICommand CardRemovedCommand { get; set; }

        string CurrentCardUID { get; }
    }
}
