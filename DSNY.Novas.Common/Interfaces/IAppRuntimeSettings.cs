﻿namespace DSNY.Novas.Common.Interfaces
{
    public interface IAppRuntimeSettings
    {
        string GetSqlitePath();
        string DatabaseFilename { get; }
        string HardwareIdentifier { get; }

        string Version { get; }

        string HHTNumber { get; }
        string PlatformDeviceName { get; }
        string DeviceIMEI { get; }

        string DeviceSerialNumber { get; }

        string DeviceMemory { get; }
    }
}