﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.Common.Interfaces
{
    public interface IBarcodeScanner
    {
        Task<bool> IsScannerAvailable();
        Task InitiateScan();
        void SetDataRecievedCommand(ICommand dataRecievedCommand);
    }
}
