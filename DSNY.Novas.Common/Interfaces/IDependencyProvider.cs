﻿namespace DSNY.Novas.Common.Interfaces
{
    public interface IDependencyProvider
    {
        T Get<T>() where T : class;
        void Register<T>() where T : class;
    }
}
