﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Windows.Networking;

namespace DSNY.Novas.Common.Interfaces
{
    public interface IDnsWatcher
    {
        Task<IEnumerable<IPAddress>> GetIpAddressesAsync();
        IPAddress GetIpAddress();
        string GetNetworkName();
        IEnumerable<HostName> GetHostNames();

    }
}