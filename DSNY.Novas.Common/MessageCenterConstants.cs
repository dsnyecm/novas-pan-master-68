﻿namespace DSNY.Novas.Common
{
    public static class MessageCenterConstants
    {
        public static string SyncFailed = "Data Synchronization Failed.";
        public static string NetworkTimeout = "NetworkTimeout";
        public static string NoNetwork = "NoNetwork";
        public static string SubmitError = "SubmitError";
        public static string UnauthorizedError = "UnauthorizedError";
        public static string GeneralServerError = "GeneralServerError";
        public static string NotFoundError = "NotFoundError";
        public static string NovasDbFileNotFound = "NovasDbFileNotFound";
        public static string NovasDbCreationFailed = "Failed to create Novas database.";
    }
}