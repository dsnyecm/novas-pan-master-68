﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Common
{
    public class TimeManager
    {
        static TimeSpan _offset = new TimeSpan(0, 0, 0);
   
        public static TimeSpan CurrentOffset
        {
            get { return _offset; }
            private set { _offset = value; }
        }

        public static DateTime Now
        {
            get
            {
                return DateTime.Now - CurrentOffset;
            }
        }

        public static void UpdateOffset(DateTime currentCorrectTime) {
            CurrentOffset = DateTime.Now - currentCorrectTime;
                    }

        public static void UpdateOffset(TimeSpan offset)
        {
            CurrentOffset = offset;
           
        }
    }
}
