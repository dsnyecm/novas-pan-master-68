﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SQLite;

namespace DSNY.Novas.Data
{
    public interface IRepository<T> where T : class, new()
    {
        Task<List<T>> GetAsync();
        Task<T> GetAsync(int id);
        Task<List<T>> GetAsync<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null);
        Task<T> GetAsync(Expression<Func<T, bool>> predicate);
        AsyncTableQuery<T> AsQueryable();
        Task<int> InsertAsync(T entity);
        Task<int> InsertAllAsync(List<T> entities);
        Task<int> UpdateAsync(T entity);
        Task<int> DeleteAsync(T entity);
        Task<List<T>> QueryAsync(string query);
        Task<bool> ExecuteScalerAsync(string qury, object[] parm);
    }
}