﻿using System.Threading.Tasks;

namespace DSNY.Novas.Data
{
    public interface IDatabaseSynchronizer
    {
        Task SetUpNovasDb();
        Task SyncAsync(string table, string json);

        Task SetUpNovasDbTable(string table);
    }
}