﻿using System;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using SQLite;

namespace DSNY.Novas.Data
{
    public class DbBase
    {
        private static string SqlitePath
        {
            get
            {
                try
                {
#if TEST
                    var baseDir = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
                    var sqlitePath = $"{baseDir.Replace("Data", "ViewModelsTests")}Novas.db";
                    return sqlitePath;
#else
                    return DependencyResolver.Get<IAppRuntimeSettings>().GetSqlitePath();
#endif
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        protected SQLiteAsyncConnection Context { get; set; }

        public DbBase()
        {
            Context = new SQLiteAsyncConnection(SqlitePath);
        }
    }
}
