﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SQLite;

namespace DSNY.Novas.Data
{
    public class SQLiteRepository<T> : DbBase, IRepository<T> where T : class, new()
    {
        public AsyncTableQuery<T> AsQueryable()
        {
            return Context.Table<T>();
        }

        public async Task<List<T>> QueryAsync(string query)
        {
            return await Context.QueryAsync<T>(query);
        }

        public async Task<List<T>> GetAsync()
        {
            return await Context.Table<T>().ToListAsync();
        }

        public async Task<List<T>> GetAsync<TValue>(Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, TValue>> orderBy = null)
        {
            var query = Context.Table<T>();

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = query.OrderBy<TValue>(orderBy);

            return await query.ToListAsync();
        }

        public async Task<T> GetAsync(int id)
        {
            return await Context.FindAsync<T>(id);
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.FindAsync<T>(predicate);
        }

        public async Task<int> InsertAsync(T entity)
        {
            return await Context.InsertAsync(entity);
        }

        public async Task<int> InsertAllAsync(List<T> entities)
        {
            return await Context.InsertAllAsync(entities);
        }

        public async Task<int> UpdateAsync(T entity)
        {
            return await Context.UpdateAsync(entity);
        }

        public async Task<int> DeleteAsync(T entity)
        {
            return await Context.DeleteAsync(entity);
        }

        public async Task<bool> ExecuteScalerAsync(string qury, object[] parm )
        {
            //Context.GetConnection().Update()

            return await Context.ExecuteScalarAsync<bool>(qury, parm);
        }
       

    }
}