﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DSNY.Novas.Entities;
using Newtonsoft.Json;

namespace DSNY.Novas.Data
{
    public class DatabaseSynchronizer : DbBase, IDatabaseSynchronizer
    {
        public async Task SetUpNovasDb()
        {
            await DropTablesAsync();
            await CreateTablesAsync();
            await SetDescriptors();
           // await SetLookupValues();
        }

        public async Task SetUpNovasDbTable(string table)
        {
            Assembly assembly = Assembly.Load(new AssemblyName("DSNY.Novas.Entities"));
            Type t = assembly.ExportedTypes.First(x => x.FullName.Equals($"DSNY.Novas.Entities.DB{table}"));

            //If we have the table, we should be able to truncate it
            if (t != null)
            {
                string sql = String.Format("DELETE FROM DB{0};", table);
                await Context.ExecuteScalarAsync<object>(sql);
            }
        }

        public async Task SyncAsync(string table, string json)
        {
            await SyncToClientAsync(table, json);
        }

        private async Task SyncToClientAsync(string table, string json)
        {
            Assembly assembly = Assembly.Load(new AssemblyName("DSNY.Novas.Entities"));
            Type t = assembly.ExportedTypes.First(x => x.FullName.Equals($"DSNY.Novas.Entities.DB{table}"));
            await WriteToNovasDb(t, json);
        }
        
        private async Task DropTablesAsync()
        {
            await Context.DropTableAsync<DBNovInformation>();
            await Context.DropTableAsync<DBDutyHeader>();
            await Context.DropTableAsync<DBHearingDateTime>();
            await Context.DropTableAsync<DBDevices>();
            await Context.DropTableAsync<DBDeviceTicketRanges>();
            await Context.DropTableAsync<DBTitleReportLevel>();
            await Context.DropTableAsync<DBViolationDetails>();
            await Context.DropTableAsync<DBViolationGroups>();
            await Context.DropTableAsync<DBViolationLawCodes>();
            await Context.DropTableAsync<DBViolationScriptVariableData>();
            await Context.DropTableAsync<DBViolationScriptVariables>();
            await Context.DropTableAsync<DBViolationTypes>();
            await Context.DropTableAsync<DBVehicleRadioInfo>();
            await Context.DropTableAsync<DBCodeLaw>();
            await Context.DropTableAsync<DBAddressSuffixMaster>();
            await Context.DropTableAsync<DBBoroMaster>();
            await Context.DropTableAsync<DBAgencies>();
            await Context.DropTableAsync<DBBroadcastMessages>();
            await Context.DropTableAsync<DBViolator>();
            await Context.DropTableAsync<DBDistricts>();
            await Context.DropTableAsync<DBHearingDates>();
            await Context.DropTableAsync<DBPropertyDetails>();
            await Context.DropTableAsync<DBLookup>();
            await Context.DropTableAsync<DBHolidayMaster>();
            await Context.DropTableAsync<DBSections>();
            await Context.DropTableAsync<DBPrinters>();
            await Context.DropTableAsync<DBCourtLocations>();
            await Context.DropTableAsync<DBCodeEnforceControl>();
            await Context.DropTableAsync<DBIdMatrix>();
            await Context.DropTableAsync<DBNovasUser>();
            await Context.DropTableAsync<DBTitleMaster>();
            await Context.DropTableAsync<DBFirstDescriptors>();
            await Context.DropTableAsync<DBRoutingTime>();
            await Context.DropTableAsync<DBStreetCodeMaster>();
            await Context.DropTableAsync<DBNovData>();
            await Context.DropTableAsync<DBNovasUserMaster>();
            await Context.DropTableAsync<DBAffidavitOfService>();
            await Context.DropTableAsync<DBAffidavitOfServiceTran>();
            await Context.DropTableAsync<DBApplicationLog>();
            await Context.DropTableAsync<DBCancelNovData>();
            await Context.DropTableAsync<DBCancelNovInformation>();
            await Context.DropTableAsync<DBLaterSvcData>();
            await Context.DropTableAsync<DBLaterSvcDutyH>();
            await Context.DropTableAsync<DBLaterSvcInfo>();
            await Context.DropTableAsync<DBLaterSvcAOSTran>();
            await Context.DropTableAsync<DBDCALicenseDetails>();
        }

        private async Task CreateTablesAsync()
        {
            await Context.CreateTableAsync<DBNovInformation>();
            await Context.CreateTableAsync<DBDutyHeader>();
            await Context.CreateTableAsync<DBHearingDateTime>();
            await Context.CreateTableAsync<DBDevices>();
            await Context.CreateTableAsync<DBDeviceTicketRanges>();
            await Context.CreateTableAsync<DBTitleReportLevel>();
            await Context.CreateTableAsync<DBViolationDetails>();
            await Context.CreateTableAsync<DBViolationGroups>();
            await Context.CreateTableAsync<DBViolationLawCodes>();
            await Context.CreateTableAsync<DBViolationScriptVariableData>();
            await Context.CreateTableAsync<DBViolationScriptVariables>();
            await Context.CreateTableAsync<DBViolationTypes>();
            await Context.CreateTableAsync<DBVehicleRadioInfo>();
            await Context.CreateTableAsync<DBCodeLaw>();
            await Context.CreateTableAsync<DBAddressSuffixMaster>();
            await Context.CreateTableAsync<DBBoroMaster>();
            await Context.CreateTableAsync<DBAgencies>();
            await Context.CreateTableAsync<DBBroadcastMessages>();
            await Context.CreateTableAsync<DBViolator>();
            await Context.CreateTableAsync<DBDistricts>();
            await Context.CreateTableAsync<DBHearingDates>();
            await Context.CreateTableAsync<DBPropertyDetails>();
            await Context.CreateTableAsync<DBLookup>();
            await Context.CreateTableAsync<DBHolidayMaster>();
            await Context.CreateTableAsync<DBSections>();
            await Context.CreateTableAsync<DBPrinters>();
            await Context.CreateTableAsync<DBCourtLocations>();
            await Context.CreateTableAsync<DBCodeEnforceControl>();
            await Context.CreateTableAsync<DBIdMatrix>();
            await Context.CreateTableAsync<DBNovasUser>();
            await Context.CreateTableAsync<DBTitleMaster>();
            await Context.CreateTableAsync<DBFirstDescriptors>();
            await Context.CreateTableAsync<DBRoutingTime>();
            await Context.CreateTableAsync<DBStreetCodeMaster>();
            await Context.CreateTableAsync<DBNovData>();
            await Context.CreateTableAsync<DBNovasUserMaster>();
            await Context.CreateTableAsync<DBAffidavitOfService>();
            await Context.CreateTableAsync<DBAffidavitOfServiceTran>();
            await Context.CreateTableAsync<DBApplicationLog>();
            await Context.CreateTableAsync<DBCancelNovData>();
            await Context.CreateTableAsync<DBCancelNovInformation>();
            await Context.CreateTableAsync<DBLaterSvcData>();
            await Context.CreateTableAsync<DBLaterSvcDutyH>();
            await Context.CreateTableAsync<DBLaterSvcInfo>();
            await Context.CreateTableAsync<DBLaterSvcAOSTran>();
            await Context.CreateTableAsync<DBDCALicenseDetails>();
        }

        private async Task<int> WriteToNovasDb(Type t, string json)
        {
            Type listType = typeof(List<>).MakeGenericType(t);
            object objectList = Activator.CreateInstance(listType);
            JsonConvert.PopulateObject(json, objectList);
            Type repoType = typeof(SQLiteRepository<>).MakeGenericType(t);
            object repo = Activator.CreateInstance(repoType);
            object recordsAffected = await repoType.GetMethod("InsertAllAsync").InvokeAsync(repo, objectList);
            return (int) recordsAffected;
        }

        //TODO: consider making this more generic in case we need to reuse down the road
        public async Task SetDescriptors()
        {
            // TODO: Reading the csv file didn't work on the device, look into other options
            //using (StreamReader oStreamReader = new StreamReader(File.OpenRead("Descriptor.csv")))
            //{
            //var contents = oStreamReader.ReadToEnd();
            //string[] lines = contents.Split(Environment.NewLine.ToCharArray(),
            //    StringSplitOptions.RemoveEmptyEntries);
            string sql = String.Format("DELETE FROM DB{0};", "FirstDescriptors");
            await Context.ExecuteScalarAsync<object>(sql);
            //string[] lines = new string[]
            //    {
            //        "O,Opposite of,A or O,S,20,1982,A O T",
            //        "C,At Corner of,A or O,S,50,36810,A O T",
            //        "B,On/Between,A or O,S,60,36817,A O T",
            //        "N,Near,A or O,S,80,36774,A O T",
            //        "A,At,A or O,S,90,36780,A O T",
            //        "P,On/Bet C/M,A or O,S,100,36785,A O T",
            //        "Q,Front of C/M,A or O,S,110,36792,A O T",
            //        "F,Front of,ALL,S,10,1962,A R C O T",
            //        "R,At Rear of,ALL,S,30,2413,A R C O T",
            //        "S,At Side of,ALL,S,40,2436,A R C O T",
            //        "F_MDR,Front of,ALL - M,R,11,36747,M",
            //        "R_MDR,At Rear of,ALL - M,R,31,36756,M",
            //        "S_MDR,At Side of,ALL - M,R,41,36765,M",
            //        "T,At,Not A and O,R,120,36801,R C",
            //        "T_MDR,At,Not A and O - M,R,130,70034,M",
            //        "V,Vacant Lot,O,S,70,36829,O",
            //        "T,Inside of,T,T,75,36830,T"
            //    };

            string[] lines = new string[]
               {
                    "O,Opposite of,A or O,S,20,1982,A O",
                    "C,At Corner of,A or O,S,50,36810,A O T",  // sherirff
                    "B,On/Between,A or O,S,60,36817,A O T",  // sherirff
                    "N,Near,A or O,S,80,36774,A O",
                    "A,At,A or O,S,90,36780,A O T",  // sherirff
                    "P,On/Bet C/M,A or O,S,100,36785,A O",
                    "Q,Front of C/M,A or O,S,110,36792,A O",
                    "F,Front of,ALL,S,10,1962,A R C O",
                    "R,At Rear of,ALL,S,30,2413,A R C O",
                    "S,At Side of,ALL,S,40,2436,A R C O",
                    "F_MDR,Front of,ALL - M,R,11,36747,M",
                    "R_MDR,At Rear of,ALL - M,R,31,36756,M",
                    "S_MDR,At Side of,ALL - M,R,41,36765,M",
                    "T,At,Not A and O,R,120,36801,R C",
                    "T_MDR,At,Not A and O - M,R,130,70034,M",
                    "V,Vacant Lot,O,S,70,36829,O",
                    "T,Inside of,T,T,75,36830,T"  // sherirff
               };

            var descriptorsRepo = new SQLiteRepository<DBFirstDescriptors>();

                foreach (string l in lines)
                {
                    var s = l.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    var descriptor = new DBFirstDescriptors
                    {
                        Code = s[0],
                        Description = s[1],
                        ViolationTypeDesc = s[2],
                        GroupName = s[3],
                        Sort = Convert.ToInt32(s[4]),
                        GoElement = Convert.ToInt32(s[5]),
                        ViolationTypes = s[6]
                    };

                    await descriptorsRepo.InsertAsync(descriptor);
                }
            //}
        }

        //No Longer needed as values will be added to borosite db. 
       /* private async Task SetLookupValues()
        {
            var repo = new SQLiteRepository<DBLookup>();

            var lookups = new List<DBLookup>
            {
                new DBLookup {TableName = "CORNEROF", Code = "N/E", Description = "N/E"},
                new DBLookup {TableName = "CORNEROF", Code = "N/W", Description = "N/W"},
                new DBLookup {TableName = "CORNEROF", Code = "S/E", Description = "S/E"},
                new DBLookup {TableName = "CORNEROF", Code = "S/W", Description = "S/W"},
                new DBLookup {TableName = "ONBETWEEN", Code = "N/S", Description = "N/S"},
                new DBLookup {TableName = "ONBETWEEN", Code = "S/S", Description = "S/S"},
                new DBLookup {TableName = "ONBETWEEN", Code = "E/S", Description = "E/S"},
                new DBLookup {TableName = "ONBETWEEN", Code = "W/S", Description = "W/S"}
            };

            await repo.InsertAllAsync(lookups);
        }*/
    }

    public static class ExtensionMethods
    {
        public static async Task<object> InvokeAsync(this MethodInfo @this, object obj, params object[] parameters)
        {
            dynamic awaitable = @this.Invoke(obj, parameters);
            await awaitable;
            return awaitable.GetAwaiter().GetResult();
        }
    }
}

