﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("BS_HHNOVData")]
    public class NovData
    {
        [ExplicitKey]
        public Int64 NOVNumber { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
    }
}