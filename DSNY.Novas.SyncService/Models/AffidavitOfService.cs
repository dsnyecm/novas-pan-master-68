﻿using System;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("BS_AffidavitOfService")]
    public class AffidavitOfService
    {
        public Int64 NOVNumber { get; set; }
        public string PersonallyServeFlag { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string HairColor { get; set; }
        public string SkinColor { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string OtherIdentifying { get; set; }
        public string LicenseNumber { get; set; }
        public DateTime? ExpDate { get; set; }
        public string TypeOfLicense { get; set; }
        public string IssuedNy { get; set; }
        public string AlternativeService1 { get; set; }
        public string Comments { get; set; }
        public DateTime? SignDate { get; set; }
        public string UserId { get; set; }
        public int PublickKeyID { get; set; }
        public string DigitalSignature { get; set; }
        public string ServedCounty { get; set; }
        public string ServiceTo { get; set; }
        public string ServedTitle { get; set; }
        public string ServedTitleOther { get; set; }
        public string ServedLName { get; set; }
        public string ServedFName { get; set; }
        public string ServedMInit { get; set; }
        public string ServedSex { get; set; }
        public string ServedAddress { get; set; }
        public string ServedCity { get; set; }
        public string ServedState { get; set; }
        public string ServedZip { get; set; }
        public string AlternativeService2 { get; set; }
        public string PremiseLName { get; set; }
        public string PremiseFName { get; set; }
        public string PremiseMInit { get; set; }
        public string ServedLocation { get; set; }
        public string CheckSum { get; set; }
        public string DeviceId { get; set; }
        public string LicenseTypeDesc { get; set; }
        public string OfficerName { get; set; }
        public string AbbrevName { get; set; }
        public string AgencyId { get; set; }
        public string Title { get; set; }
        public DateTime? LoginTimestamp { get; set; }
        public string ServedHouseNo { get; set; }
        public string ServedBoroCode { get; set; }
    }
}