﻿using System;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("[digi].[BS_NOVPublicKey]")]
    public class NovCertificate
    {
        public Int64 NovNumber { get; set; }
        public string Certificate { get; set; }
    }
}