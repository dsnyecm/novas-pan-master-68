﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("BS_CancelHHNOVData")]
    public class CancelNovData
    {
        [ExplicitKey]
        public string DeviceId { get; set; }
        public DateTime? SystemTimestamp { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
    }
}