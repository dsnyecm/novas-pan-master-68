﻿using System;
using Dapper.Contrib.Extensions;

namespace DSNY.Novas.SyncService.Models
{
    [Table("BS_DutyHeader")]
    public class DutyHeader
    {
        [ExplicitKey]
        public string UserId { get; set; }
        [ExplicitKey]
        public DateTime LoginTimestamp { get; set; }
        public string BoroId { get; set; }
        public string DeviceId { get; set; }
        public string SiteId { get; set; }
        public string Title { get; set; }
        public int VehicleRadioId { get; set; }
        public DateTime? LogoutTimestamp { get; set; }
        public DateTime? HearingDate { get; set; }
        public string IsActingSupervisor { get; set; }
        public int TicketCount { get; set; }
        public int VoidCount { get; set; }
        public string IsSent { get; set; }
        public DateTime? SentTimestamp { get; set; }
        public string IsReceived { get; set; }
        public DateTime? ReceivedTimestamp { get; set; }
        public DateTime? ReportingTimestamp { get; set; }
        public byte[] SignatureBitmap { get; set; }
    }
}