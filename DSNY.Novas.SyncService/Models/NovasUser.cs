﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Table("NovasUser")]
    public class NovasUser
    {  
        [Key]
        public string UserId { get; set; }
        
        public string Salt { get; set; }
        
        public string UserHash { get; set; }
        public string NfcUid { get; set; }

        public string Pin { get; set; }
        public byte[] SignatureBitmap { get; set; }

    }

    public class NovasUserArray
    {
        public NovasUser[] NovasUser;
       // public NovasUserMaster[] NovasUserMaster;
    }
}