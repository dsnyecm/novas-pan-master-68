﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Table("HH_LaterSvcDutyH")]
    public class LaterSvcDutyH
    {
        [ExplicitKey]
        public DateTime LoginTimestamp { get; set; }
        [ExplicitKey]
        public string UserId { get; set; }
        public string BoroId { get; set; }
        public string DeviceId { get; set; }
        public string SiteId { get; set; }
        public string Title { get; set; }
        public int VehicleRadioId { get; set; }
        public DateTime? LogoutTimestamp { get; set; }
        public DateTime? HearingDate { get; set; }
        public string IsActingSupervisor { get; set; }
        public int TicketCount { get; set; }
        public int VoidCount { get; set; }
        public string IsSent { get; set; }
        public DateTime? SentTimestamp { get; set; }
        public string IsReceived { get; set; }
        public DateTime? ReceivedTimestamp { get; set; }
        public DateTime? ReportingTimestamp { get; set; }
        public byte[] SignatureBitmap { get; set; }
    }
}