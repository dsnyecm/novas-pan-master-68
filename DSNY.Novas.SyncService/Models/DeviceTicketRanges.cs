﻿using System;
using System.Data.Linq.Mapping;
using System.Drawing.Imaging;
using Dapper.Contrib.Extensions;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("BS_DeviceTicketRanges")]
    public class DeviceTicketRanges
    {
        [ExplicitKey]
        public string DeviceId { get; set; }
        [ExplicitKey]
        public int NOVRangeId { get; set; }
        public string RangeStatus { get; set; }
        public Int64 TicketStartNumber { get; set; }
        public Int64 TicketEndNumber { get; set; }
        public Int64 LastTicketNumber { get; set; }
    }
}