﻿using System;
using Dapper.Contrib.Extensions;

namespace DSNY.Novas.SyncService.Models
{
    [Table("BS_ApplicationLogs")]
    public class ApplicationLog
    {
        [ExplicitKey]
        public DateTime LogTimestamp { get; set; }
        [ExplicitKey]
        public int RecordNum { get; set; }
        [ExplicitKey]
        public string ComputerName { get; set; }
        public DateTime? ProcessTimestamp { get; set; }
        public string LogGroup { get; set; }
        public string Type { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }
        public int Line { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Data { get; set; }
        public DateTime? AddTimestamp { get; set; }
        public string DeviceId { get; set; }
    }

    public class ApplicationLogsArray
    {
        public ApplicationLog[] ApplicationLogs;
    }
}