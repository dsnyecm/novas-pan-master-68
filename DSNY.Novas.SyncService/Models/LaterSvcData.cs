﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Table("HH_LaterSvcData")]
    public class LaterSvcData
    {
        public Int64 NOVNumber { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
        public string DLDeviceId { get; set; }
    }
}