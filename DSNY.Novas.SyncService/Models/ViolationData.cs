﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    public class ViolationData
    {
        public DutyHeader[] DutyHeaders { get; set; }
        public DeviceTicketRanges[] DeviceTicketRanges { get; set; }
        public VehicleRadioInfo[] VehicleRadioInfo { get; set; }
        public NovInformation NovInformation { get; set; }
        public NovData[] NovData { get; set; }
        public NovCertificate NovCertificate { get; set; }
        public AffidavitOfService AffidavitOfService { get; set; }
        public AffidavitOfServiceTran AffidavitOfServiceTran { get; set; }
        public CancelNovInformation CancelNovInfo { get; set; }
        public CancelNovData[] CancelNovData { get; set; }
    }
}