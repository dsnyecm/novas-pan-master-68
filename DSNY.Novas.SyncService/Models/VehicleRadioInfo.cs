﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Novas.SyncService.Models
{
    [Dapper.Contrib.Extensions.Table("BS_VehicleRadioInfo")]
    public class VehicleRadioInfo
    {
        [ExplicitKey]
        public string UserID { get; set; }
        public DateTime LoginTimeStamp { get; set; }
        public int VehicleRadioId { get; set; }
        public string VehicleID { get; set; }
        public string RadioID { get; set; }
        public int StartMileage { get; set; }
        public int EndMileage { get; set; }
    }
}