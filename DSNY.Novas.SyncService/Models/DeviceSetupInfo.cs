﻿using System;
using System.Data.Linq.Mapping;
using System.Drawing.Imaging;
using Dapper.Contrib.Extensions;

namespace DSNY.Novas.SyncService.Models
{
    /**
       CREATE TABLE [CentralSite].[dbo].[CS_DeviceSetupInfo] (
            DeviceIMEI varchar(128) NOT NULL,
            SerialNumber varchar(20) NOT NULL,
            PlatformDeviceName varchar(128) NOT NULL,
            HHTNumber char(10) NOT NULL,
			BoroSiteCode char(4) NOT NULL,
            IsPendingSetup char(1) NOT NULL,
            createdate datetime NOT NULL
        CONSTRAINT PK_DEVICEID PRIMARY KEY (DeviceIMEI, HHTNumber)
            )
     * */
    [Dapper.Contrib.Extensions.Table("CS_DeviceSetupInfo")]
    public class DeviceSetupInfo
    {
        [ExplicitKey]
        public string DeviceIMEI { get; set; }
        public string PlatformDeviceName { get; set; }
        public string HHTNumber { get; set; }
        public string SerialNumber { get; set; }
        public string BoroSiteCode { get; set; }

        public bool IsPendingSetup { get; set; }

        public DateTime createdate { get; set; }

        //public string BoroSiteCode { get; set; }
        //TODO: add additional columns e.g. ipaddress , device model
    }
}