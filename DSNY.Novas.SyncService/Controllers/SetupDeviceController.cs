﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using System.Web.Http.Results;

using Dapper;
using Dapper.Contrib.Extensions;
using MiniProfiler.Integrations;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using DSNY.Novas.SyncService.Models;
using static DSNY.Novas.SyncService.LogHelper;
using System.Configuration;

namespace DSNY.Novas.SyncService.Controllers
{
    [RoutePrefix("api/setupdevice")]
    public class SetupDeviceController : ApiController
    {
        private static readonly string _CENTRALSITE = "CentralSite";
        private static readonly string _centralsiteConnectionString = ConfigurationManager.ConnectionStrings[_CENTRALSITE].ConnectionString;

        Logger Log = LogManager.GetLogger("databaseLogger");
        Logger LogTxt = LogManager.GetLogger("fileLogger");

        [Route("{saveToCentral}")]
        [HttpPost]
        // sample Json for Swagger : {"DeviceIMEI": "777222888111999", "PlatformDeviceName": "PAN12ABC12345", "HHTNumber":"PAN999", "SerialNumber" : "ABCDE99999", "BoroSiteCode" : "BX01", "IsPendingSetup": "true", "createdate": "2018-05-24"}
        public async Task<IHttpActionResult> SaveDeviceSetup(JObject jsonData)
        {
            try
            {
                var jsonTxt = jsonData.ToString();
                DeviceSetupInfo deviceSetupInfo = JsonConvert.DeserializeObject<DeviceSetupInfo>(jsonTxt);
                var transactionSuccess = await SaveDeviceSetup(deviceSetupInfo);

                if (transactionSuccess)
                    return Ok();
                else
                    return InternalServerError();

            }
            catch (Exception exc)
            {
                var message = "Failed to deserialize deviceSetupInfo JSON";
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = jsonData;
                var log_data = _CENTRALSITE;
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                return BadRequest();
            }
        }

        private async Task<bool> SaveDeviceSetup(DeviceSetupInfo deviceSetupInfo)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            {
                var factory = new SqlServerDbConnectionFactory(_centralsiteConnectionString);

                using (var conn = DbConnectionFactoryHelper.New(factory, CustomDbProfiler.Current))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Close();
                            await conn.OpenAsync();
                        }

                        bool saveResult = await SaveDeviceSetup(deviceSetupInfo, conn);

                        scope.Complete();
                        return saveResult;

                    }
                    catch (Exception exc)
                    {
                        scope.Complete();
                        var message = String.Format("Error saving DeviceSetupInfo: ex: {0} - {1}", exc.Message);
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eError;
                        var log_eventId = 0;
                        var log_description = String.Format("Error saving DeviceSetupInfo: ex: {0} - {1}", exc.Message, exc.StackTrace);
                        var log_data = _CENTRALSITE;
                        var log_deviceId = (deviceSetupInfo == null) ? "" : deviceSetupInfo.HHTNumber;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        return false;
                    }

                }
            }
        }
        private async Task<bool> SaveDeviceSetup(DeviceSetupInfo deviceSetupInfo, DbConnection conn)
        {


            // TODO: using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            try
            {
                if (deviceSetupInfo == null || deviceSetupInfo.DeviceIMEI == null || deviceSetupInfo.PlatformDeviceName == null)
                {
                    throw new Exception("Novas App Internal Error! deviceSetupInfo is NULL");
                }
                //var deviceSetupInfo_deviceUnitId = deviceSetupInfo.PlatformDeviceName;

                //var newDevice_exists = conn.ExecuteScalar<bool>("select count(1) from CS_DeviceSetupInfo where PlatformDeviceName=@PlatformDeviceName", new { PlatformDeviceName = deviceSetupInfo_deviceUnitId });
                //var newMasterDevice_exists = conn.ExecuteScalar<bool>("select count(1) from Masterdata.CS_Devices where UnitId=@PlatformDeviceName", new { PlatformDeviceName = deviceSetupInfo_deviceUnitId });

                //if (newDevice_exists )
                //{
                //    throw new Exception("Device UnitId already exists!");
                //}

                int insertResult = await conn.InsertAsync(deviceSetupInfo);

                var message = "DeviceSetupInfo successfully synced";
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eSuccess;
                var log_eventId = (int)eEventId.eInsertStmt;
                var log_description = "DeviceSetupInfo successfully synced";
                var log_data = _CENTRALSITE;
                var log_deviceId = deviceSetupInfo.PlatformDeviceName;
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                theEventCommands.Properties["log_group"] = log_group;
                theEventCommands.Properties["log_type"] = log_type;
                theEventCommands.Properties["log_eventId"] = log_eventId;
                theEventCommands.Properties["log_description"] = commands;
                theEventCommands.Properties["log_data"] = log_data;
                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEventCommands);
                LogTxt.Log(theEventCommands);
                return (insertResult == 0) ? true : false;
            }
            catch (Exception exc)
            {
                var message = String.Format("Failed to insert into CS_DeviceSetupInfo #{0}: {1}", deviceSetupInfo.DeviceIMEI, exc.Message);
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eWarning;
                var log_eventId = 0;
                var log_description = String.Format("Duplicate Failed to Upload to CS_DeviceSetupInfo #{0}:", deviceSetupInfo.DeviceIMEI);
                var log_data = _CENTRALSITE;
                var log_deviceId = deviceSetupInfo.DeviceIMEI;
                var log_recordNum = 1;

                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = commands;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);
                return false;
            }
        }
    }
}
