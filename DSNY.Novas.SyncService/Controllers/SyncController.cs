﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using System.Web.Http.Results;
using Dapper;
using Dapper.Contrib.Extensions;
using DSNY.Novas.SyncService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MiniProfiler.Integrations;
using NLog;
using System.Data.Common;
using static DSNY.Novas.SyncService.LogHelper;
using System.Text;

namespace DSNY.Novas.SyncService.Controllers
{
    [RoutePrefix("api/sync")]
    public class SyncController : ApiController
    {
        private static readonly string _borositeConnectionString;
        private static readonly Dictionary<string, string> _sqlToRun;
        Logger Log = LogManager.GetLogger("databaseLogger");
        Logger LogTxt = LogManager.GetLogger("fileLogger");
        private List<long> errorTickets;
        private string gDeviceId = "";


        

        static SyncController()
        {
            _borositeConnectionString = ConfigurationManager.ConnectionStrings["Borosite"].ConnectionString;
            _sqlToRun = new Dictionary<string, string>
            {
                {
                    "AddressSuffixMaster",
                    "[sync].[GetAddressSuffixMaster]"
                },
                {
                    "Agencies",
                    "[sync].[GetAgencies]"
                },
                {
                    "BoroMaster",
                    "[sync].[GetBoroMaster]"
                },
                {
                    "BoroSiteMaster",
                    "[sync].[GetBorositeMaster]"
                },
                {
                    "BroadcastMessages",
                    "[sync].[GetBroadcastMessages]"
                },
                {
                    "Districts",
                    "[sync].[GetDistricts]"
                },
                {
                    "HearingDates",
                    "[sync].[GetHearingDates]"
                },
                {
                    "HearingDateTime",
                    "[sync].[GetHearingDateTime]"
                },
                {
                    "HHViolator",
                    "[sync].[GetViolator]"
                },
                {
                    "Miscellaneous",
                    "[sync].[GetLookups]"
                },
                {
                    "ViolationDetails",
                    "[sync].[GetViolationDetails]"
                },
                {
                    "ViolationGroups",
                    "[sync].[GetViolationGroups]"
                },
                {
                    "ViolationScriptVariableData",
                    "[sync].[GetViolationScriptVariableData]"
                },
                {
                    "ViolationTypes",
                    "[sync].[GetViolationTypes]"
                },
                {
                    "ViolationLawCodes",
                    "[sync].[GetViolationLawCodes]"
                },
                {
                    "ViolationScriptVariables",
                    "[sync].[GetViolationScriptVariables]"
                },
                {
                    "HolidayMaster",
                    "[sync].[GetHolidayMaster]"
                },
                {
                    "CodeLaw",
                    "[sync].[GetCodeLaw]"
                },
                {
                    "Printers",
                    "[sync].[GetPrinters]"
                },
                {
                    "HHTPropertyDetails",
                    "[sync].[GetPropertyDetails]"
                },
                {
                    "DCALicenseDetails",
                    "[sync].[GetDCALicenseDetails]"
                },
                {
                    "Titlereportlevel",
                    "[sync].[GetTitleReportLevel]"
                },
                {
                    "CourtLocations",
                    "[sync].[GetCourtLocations]"
                },
                {
                    "CodeEnforceControl",
                    "[sync].[GetCodeEnforceControl]"
                },
                {
                    "IDMatrix",
                    "[sync].[GetIdMatrix]"
                },
                {
                    "TitleMaster",
                    "[sync].[GetTitleMaster]"
                },
                {
                    "RoutingTime",
                    "[sync].[GetRoutingTime]"
                },
                {
                    "StreetCodeMaster",
                    "[sync].[GetStreetCodeMaster]"
                },
                {
                    "Devices",
                    "[sync].[GetDevices]"
                },
                {
                    "DeviceTicketRanges",
                    "[sync].[GetDeviceTicketRanges]"
                },
                {
                    "Sections",
                    "[sync].[GetSections]"
                },
                {
                    "Users",
                    "[sync].[GetNovasUserMaster]"
                },
                {
                    "NovasUser",
                    "[sync].[GetNovasUser]"
                },
                {
                    "LaterSvcInfo",
                    "[sync].[GetLaterSvcInfo]"
                },
                {
                    "LaterSvcData",
                    "[sync].[GetLaterSvcData]"
                },
                {
                    "LaterSvcAOSTran",
                    "[sync].[GetLaterSvcAOSTran]"
                },
                {
                    "LaterSvcDutyH",
                    "[sync].[GetLaterSvcDutyH]"
                }
            };
        }

        [Route("{config}")]
        [HttpGet]
        public async Task<OkNegotiatedContentResult<BorositeResponse>> GetTablesToSync([FromUri] string lastsync)
        {
            var resp = new BorositeResponse();
            var data = new List<Dictionary<string, object>>();
            try
            {

                using (var conn = new SqlConnection(_borositeConnectionString))
                {
                    //string sql = String.Format("select * from [Configuration].[BS_MobileDataPull] where tableName='HH_Users'  ActivityTimestamp > '{0}'", lastsync);
                    string sql = String.Format("select * from [Configuration].[BS_MobileDataPull] where  ActivityTimestamp > '{0}'", lastsync);
                    var cmd = new SqlCommand(sql, conn) { CommandType = CommandType.Text };

                    var outputParam = cmd.Parameters.Add("@recordCount", SqlDbType.Int);
                    outputParam.Direction = ParameterDirection.Output;

                    await conn.OpenAsync();
                    using (var rdr = await cmd.ExecuteReaderAsync())
                    {
                        while (await rdr.ReadAsync())
                        {
                            var dictionary = new Dictionary<string, object>();
                            foreach (var i in Enumerable.Range(0, rdr.FieldCount))
                            {
                                var dbValue = string.IsNullOrWhiteSpace(rdr.GetValue(i).ToString()) == true
                                    ? string.Empty
                                    : rdr.GetValue(i);

                                dictionary.Add(rdr.GetName(i), dbValue);
                            }

                            data.Add(dictionary);
                        }
                    }

                    resp.BorositeData = data;
                    resp.RecordCount = data.Count;


                    return Ok(resp);
                }
            }
            catch (Exception exc)
            {
                var message = String.Format("Error retrieving data from BS_MobileDataPull, ex: {0}" + exc.StackTrace);
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = String.Format("Error getting Violations ex:{0}" + exc.StackTrace);
                var log_data = "Borosite";
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                return null;
            }
        }

        [Route("{time}")]
        [HttpGet]
        public OkNegotiatedContentResult<DateTime> GetServerTime([FromUri] string deviceId)
        {
            try
            {
                if(!string.IsNullOrEmpty(deviceId))
                {
                    var message = "Sync start time";
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eError;
                    var log_eventId = 0;
                    var log_description = "Device start pinging to server to get updates";
                    var log_data = "BorodeviceIdsite";
                    var log_deviceId = deviceId;
                    var log_recordNum = 1;

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = log_description;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);

                }

                return Ok(DateTime.Now);
            }
            catch (Exception exc)
            {
                var message = exc.StackTrace;
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = exc.StackTrace;
                var log_data = "Borosite";
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [DeflateCompression]
        [Route("{borosite}")]
        [HttpGet]
        public async Task<OkNegotiatedContentResult<BorositeResponse>> GetBorositeData(
            [FromUri] string table, int? start, int? end)
        {
            try
            {
                //Log.Info($"Getting data for {table}");
                var resp = await GetBorositeDataAsync(table, start, end);
                //Log.Info($"End getting data for {table}");
                return Ok(resp);
            }
            catch (Exception exc)
            {
                var message = exc.StackTrace;
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = exc.StackTrace;
                var log_data = "Borosite";
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [Route("{violation}")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveViolation(JObject jsonData)
        {
            try
            {
                var jsonTxt = jsonData.ToString();
                ViolationData tran = JsonConvert.DeserializeObject<ViolationData>(jsonTxt);
                if (tran.NovInformation != null)
                    gDeviceId = tran.NovInformation.DeviceId ?? string.Empty;

                if (tran.CancelNovInfo != null)
                    gDeviceId = tran.CancelNovInfo.DeviceId ?? string.Empty;

                gDeviceId = gDeviceId.Trim();

                var transactionSuccess = await SaveViolation(tran);

                if (transactionSuccess)
                    return Ok();
                else

                    return InternalServerError();

            }
            catch (Exception exc)
            {
                var message = "Failed to deserialize violation JSON";
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = jsonData;
                var log_data = "Borosite";
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);


                return BadRequest();
            }
        }

        private async Task<BorositeResponse> GetBorositeDataAsync(string table, int? start, int? end)
        {

            try
            {
                if (_sqlToRun.ContainsKey(table))
                {
                    var sql = _sqlToRun[table];
                    var data = new List<Dictionary<string, object>>();
                    var resp = new BorositeResponse();

                    using (var conn = new SqlConnection(_borositeConnectionString))
                    {
                        var cmd = new SqlCommand(sql, conn) { CommandType = CommandType.StoredProcedure };

                        var startParam = new SqlParameter
                        {
                            ParameterName = "@startRowNum",
                            Value = start,
                            SqlDbType = SqlDbType.Int
                        };

                        cmd.Parameters.Add(startParam);

                        var endParam = new SqlParameter
                        {
                            ParameterName = "@endRowNum",
                            Value = end,
                            SqlDbType = SqlDbType.Int
                        };

                        cmd.Parameters.Add(endParam);
                        var outputParam = cmd.Parameters.Add("@recordCount", SqlDbType.Int);
                        outputParam.Direction = ParameterDirection.Output;

                        await conn.OpenAsync();
                        using (var rdr = await cmd.ExecuteReaderAsync())
                        {
                            while (await rdr.ReadAsync())
                            {
                                var dictionary = new Dictionary<string, object>();
                                foreach (var i in Enumerable.Range(0, rdr.FieldCount))
                                {
                                    var dbValue = string.IsNullOrWhiteSpace(rdr.GetValue(i).ToString()) == true
                                        ? string.Empty
                                        : rdr.GetValue(i);

                                    dictionary.Add(rdr.GetName(i), dbValue);
                                }

                                data.Add(dictionary);
                            }
                        }

                        Int32.TryParse(cmd.Parameters["@recordCount"].Value.ToString(), out var recordCount);
                        resp.BorositeData = data;
                        resp.RecordCount = recordCount;
                    }

                    return resp;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exc)
            {
                var message = String.Format("Error retrieving data from table: " + table + ", rows " + start + " - " + end + " ex: {0}" + exc.StackTrace);
                var log_group = (char)eGroups.eSQL;
                var log_type = (char)eTypes.eError;
                var log_eventId = 0;
                var log_description = String.Format("Error getting Violations ex:{0}" + exc.StackTrace);
                var log_data = "Borosite";
                var log_deviceId = "";
                var log_recordNum = 1;

                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                theEvent.Properties["log_group"] = log_group;
                theEvent.Properties["log_type"] = log_type;
                theEvent.Properties["log_eventId"] = log_eventId;
                theEvent.Properties["log_description"] = log_description;
                theEvent.Properties["log_data"] = log_data;
                theEvent.Properties["log_deviceId"] = log_deviceId;
                theEvent.Properties["log_recordNum"] = log_recordNum;
                Log.Log(theEvent);
                LogTxt.Log(theEvent);

                return null;
            }
        }

        private async Task<bool> SaveViolation(ViolationData violationTran)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            {
                var factory = new SqlServerDbConnectionFactory(_borositeConnectionString);

                using (var conn = DbConnectionFactoryHelper.New(factory, CustomDbProfiler.Current))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Close();
                            await conn.OpenAsync();
                        }

                        var isDutyHeaderSaved = await SaveDutyHeaders(violationTran.DutyHeaders, conn);
                        var isTicketRangeSaved = await SaveDeviceTicketRanges(violationTran.DeviceTicketRanges, conn);
                        var isVehicleRadioSaved = await SaveVehicleRadioInfo(violationTran.VehicleRadioInfo, conn);

                        //Date: 09/10/2018 As per Navi comments
                        // 2.	Resp1FirstName should be empty for ALL property ticket and Commercial tickets.
                        //3.	Resp1BoroCode should be same as PlaceBoroCode for ALL property ticket and Commercial tickets.
                        //if ((violationTran.NovInformation.ViolationGroupId == 2 || violationTran.NovInformation.ViolationGroupId == 3))
                        //{
                        //    violationTran.NovInformation.Resp1LastName = "";
                        //    violationTran.NovInformation.Resp1FirstName = "";
                        //    violationTran.NovInformation.Resp1BoroCode = Convert.ToString(violationTran.NovInformation.PlaceBoroCode);

                        //    violationTran.CancelNovInfo.Resp1LastName = "";
                        //    violationTran.CancelNovInfo.Resp1FirstName = "";
                        //    violationTran.CancelNovInfo.Resp1BoroCode = Convert.ToString(violationTran.CancelNovInfo.PlaceBoroCode);
                        //}
                        //only attempt to save the NOV if the metadata saved
                        if (isDutyHeaderSaved && isTicketRangeSaved && isVehicleRadioSaved)
                        {
                            var isNovInfoSaved = await SaveNovInformation(violationTran.NovInformation, conn);
                            var isNovDataSaved = await SaveNovData(violationTran.NovData, conn);
                            var isNovCertificateSaved = await SaveNovCertificate(violationTran.NovCertificate, conn);
                            var isAosSaved = await SaveAffidavitOfService(violationTran.AffidavitOfService, conn);
                            var isAosTranSaved = await SaveAffidavitOfServiceTran(violationTran.AffidavitOfServiceTran, conn);
                            var isCancelInfoSaved = await SaveCancelNovInformation(violationTran.CancelNovInfo, conn);
                            var isCancelDataSaved = await SaveCancelNovData(violationTran.CancelNovData, conn);
                            if (!isNovInfoSaved || !isNovDataSaved || !isNovCertificateSaved || !isAosSaved || !isAosTranSaved || !isCancelInfoSaved || !isCancelDataSaved)
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }

                        scope.Complete();
                        return true;

                    }
                    catch (Exception exc)
                    {
                        scope.Complete();
                        var message = String.Format("Error saving Violations: ex: {0} - {1}", exc.Message);
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eError;
                        var log_eventId = 0;
                        var log_description = String.Format("Error saving Violations: ex: {0} - {1}", exc.Message, exc.StackTrace);
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        return false;
                    }

                }
            }
        }

        private async Task<bool> SaveDutyHeaders(DutyHeader[] dutyHeaders, DbConnection conn)
        {
            bool blnSuccess = false;

            if (dutyHeaders != null)
            {
                if (dutyHeaders.Length == 0)
                {
                    var message = String.Format("No duty headers provided");
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = String.Format("No duty headers provided in request");
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 1;

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = log_description;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    blnSuccess = true;

                }

                foreach (var item in dutyHeaders)
                {
                    try
                    {
                        //DutyHeaders
                        var dutyHeaders_LoginTimeStamp = item.LoginTimestamp;
                        var sig = item.SignatureBitmap;
                        var lusrId = item.UserId;
                        // var dutyHeaders_exists = conn.ExecuteScalar<bool>("select count(1) from BS_DutyHeader where LoginTimestamp=@LoginTimestamp and UserId=@UserId", new { LoginTimestamp = dutyHeaders_LoginTimeStamp, UserId= lusrId });

                        string isSqlExites = string.Format("select count(1) from BS_DutyHeader where LoginTimestamp='{0}' and UserId='{1}'", item.LoginTimestamp, item.UserId);
                        //var dutyHeaders_exists = conn.ExecuteScalar<bool>("select count(1) from BS_DutyHeader where LoginTimestamp=@LoginTimestamp and UserId=@UserId", new { LoginTimestamp = dutyHeaders_LoginTimeStamp, UserId = lusrId });
                        var dutyHeaders_exists = conn.ExecuteScalar<bool>(isSqlExites);

                        if (!dutyHeaders_exists)
                        {

                            if (item.LogoutTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.LogoutTimestamp = DateTime.Now;
                            }
                            if (item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.ReceivedTimestamp = DateTime.Now;
                            }
                            if (item.ReportingTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.ReportingTimestamp = DateTime.Now;
                            }
                            if (item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.SentTimestamp = DateTime.Now;
                            }
                            if (item.HearingDate.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.HearingDate = DateTime.Now.AddMonths(1);
                            }

                            string sql = string.Format("INSERT INTO BS_DutyHeader(UserId, LoginTimestamp, BoroId, DeviceId, SiteId, Title,"
                                        + "VehicleRadioId, LogoutTimestamp, HearingDate, IsActingSupervisor, TicketCount, VoidCount, IsSent, SentTimestamp,"
                                        + "IsReceived, ReceivedTimestamp, ReportingTimestamp)"
                                        + " values('{0}', '{1}','{2}' ,'{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}',"
                                        + "'{14}', '{15}', '{16}')", item.UserId, item.LoginTimestamp, item.BoroId, item.DeviceId, item.SiteId, item.Title,
                                        item.VehicleRadioId, item.LogoutTimestamp, item.HearingDate, item.IsActingSupervisor, item.TicketCount, item.VoidCount, item.IsSent, item.SentTimestamp,
                                        item.IsReceived, item.ReceivedTimestamp, item.ReportingTimestamp
                                        );

                            conn.ExecuteScalar(sql);

                            //string updateSql = string.Format("update BS_DutyHeader  set SignatureBitmap = (select SignatureBitmap from BS_Users where UserId = '{0}')"
                            //                    + " where userId = '{0}' and LoginTimeStamp = '{1}'", lusrId, dutyHeaders_LoginTimeStamp);

                            string updateSql = string.Format("update BS_DutyHeader  set SignatureBitmap = (select SignatureBitmap from NovasUser where UserId = '{0}')"
                                               + " where userId = '{0}' and LoginTimeStamp = '{1}'", lusrId, dutyHeaders_LoginTimeStamp);


                            var updateSinature = conn.ExecuteScalar<int>(updateSql);

                            //await conn.InsertAsync(item);

                            var message = "DutyHeader successfully synced";
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eSuccess;
                            var log_eventId = (int)eEventId.eInsertStmt;
                            var log_description = "DutyHeader successfully synced";
                            var log_data = "Borosite";
                            var log_deviceId = item.DeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                            blnSuccess = true;
                        }
                        else
                        {
                            if (item.LogoutTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.LogoutTimestamp = DateTime.Now;
                            }
                            if (item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.ReceivedTimestamp = DateTime.Now;
                            }
                            if (item.ReportingTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.ReportingTimestamp = DateTime.Now;
                            }
                            if (item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.SentTimestamp = DateTime.Now;
                            }
                            if (item.HearingDate.ToString() == "1/1/0001 12:00:00 AM")
                            {
                                item.HearingDate = DateTime.Now.AddMonths(1);
                            }
                            await conn.UpdateAsync(item);

                            var message = "DutyHeader successfully synced";
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eSuccess;
                            var log_eventId = (int)eEventId.eInsertStmt;
                            var log_description = "DutyHeader successfully synced";
                            var log_data = "Borosite";
                            var log_deviceId = item.DeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                            blnSuccess = true;
                        }
                    }
                    catch (Exception exc)
                    {
                        var message = String.Format("DutyHeader failed to sync: {0}", exc.Message);
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_data = "Borosite";
                        var log_deviceId = item.DeviceId;
                        var log_recordNum = 1;

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        blnSuccess = false;

                    }
                }
            }

            #region Dad code
            //else
            //{

            //    var message = String.Format("No duty headers provided");
            //    var log_group = (char)eGroups.eSQL;
            //    var log_type = (char)eTypes.eWarning;
            //    var log_eventId = 0;
            //    var log_description = String.Format("No duty headers provided in request");
            //    var log_data = "Borosite";
            //    var log_deviceId = gDeviceId;
            //    var log_recordNum = 1;

            //    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
            //    theEvent.Properties["log_group"] = log_group;
            //    theEvent.Properties["log_type"] = log_type;
            //    theEvent.Properties["log_eventId"] = log_eventId;
            //    theEvent.Properties["log_description"] = log_description;
            //    theEvent.Properties["log_data"] = log_data;
            //    theEvent.Properties["log_deviceId"] = log_deviceId;
            //    theEvent.Properties["log_recordNum"] = log_recordNum;
            //    Log.Log(theEvent);
            //    LogTxt.Log(theEvent);
            //    blnSuccess = true;
            //}

            #endregion

            return blnSuccess;
        }

        private async Task<bool> SaveDeviceTicketRanges(DeviceTicketRanges[] ticketRanges, DbConnection conn)
        {
            bool blnSuccess = false;
            if (ticketRanges != null)
            {

                foreach (var item in ticketRanges)
                {
                    //DeviceTicketRanges 
                    var deviceTicketRange_exists = conn.ExecuteScalar<bool>("select count(1) from BS_DeviceTicketRanges where DeviceId=@DeviceId and NovRangeId=@NovRangeId", new { DeviceId = item.DeviceId, NovRangeId = item.NOVRangeId });

                    if (deviceTicketRange_exists)
                    {

                        if (Convert.ToChar(item.RangeStatus) == 'A' || Convert.ToChar(item.RangeStatus) == 'C')
                        {
                            try
                            {
                                // can't use Dapper Contrib for DeviceTicketRanges as the column names don't match
                                await conn.ExecuteAsync("[sync].[SaveDeviceTicketRanges]", item, commandType: CommandType.StoredProcedure);

                                var message = "DeviceTicketRange info successfully synced";
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = (int)eEventId.eInsertStmt;
                                var log_description = "DeviceTicketRange info successfully synced";
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                blnSuccess = true;
                            }
                            catch (Exception exc)
                            {
                                var message = String.Format("Failed to upload last ticket issued for Id: {1} exc:{2}", item.DeviceId, exc.StackTrace);
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = 0;
                                var log_description = String.Format("Failed to upload last ticket issued for Id: {1} exc:{2}", item.DeviceId, exc.StackTrace);
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                blnSuccess = false;
                            }
                        }

                        #region dad code
                        /*
                        if (Convert.ToChar(item.RangeStatus) == 'C')
                        {
                            try
                            {
                                // can't use Dapper Contrib for DeviceTicketRanges as the column names don't match
                                await conn.ExecuteAsync("[sync].[SaveDeviceTicketRanges]", item, commandType: CommandType.StoredProcedure);

                                var message = "DeviceTicketRange info successfully synced";
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = (int)eEventId.eInsertStmt;
                                var log_description = "DeviceTicketRange info successfully synced";
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                return true;
                            }
                            catch (Exception exc)
                            {
                                var message = String.Format("Failed to upload HHTkRng set status of Closed for Id: {1} exc:{2}", item.DeviceId, exc.StackTrace);
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = 0;
                                var log_description = String.Format("Failed to upload HHTkRng set status of Closed for Id: {1} exc:{2}", item.DeviceId, exc.StackTrace);
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                return false;
                            }
                        }

                        if (Convert.ToChar(item.RangeStatus) == 'U')
                        {
                            //do nothing 
                        }
                        */
                        #endregion
                    }
                    else
                    {
                        var message = "Unable to find NOV Range ID in database";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Unable to find NOV Range ID {0} for Device ID {1}", item.NOVRangeId, item.DeviceId);
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Warn, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        blnSuccess = false;
                    }
                }
            }

            return blnSuccess;
        }

        private async Task<bool> SaveVehicleRadioInfo(VehicleRadioInfo[] vehicleRadio, DbConnection conn)
        {
            bool blnSuccess = false;
            if (vehicleRadio != null)
            {
                if (vehicleRadio.Length > 0)
                {
                    foreach (var item in vehicleRadio)
                    {
                        try
                        {
                            //VehicleRadioInfo
                            var vehicleRadioInfo_loginTimestamp = item.LoginTimeStamp;
                            var vehicleRadioInfo_exists = conn.ExecuteScalar<bool>("select count(1) from BS_VehicleRadioInfo where UserId=@UserId and VehicleRadioId=@VehicleRadioId and LoginTimestamp=@LoginTimestamp", new { UserId = item.UserID, VehicleRadioId = item.VehicleRadioId, LoginTimestamp = vehicleRadioInfo_loginTimestamp });

                            if (!vehicleRadioInfo_exists)
                            {
                                await conn.InsertAsync(item);

                                var message = "VehicleRadioInfo successfully synced";
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = (int)eEventId.eInsertStmt;
                                var log_description = "VehicleRadioInfo successfully synced";
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                blnSuccess = true;
                            }
                            else
                            {
                                var updateVehicleRadio = conn.ExecuteScalar<bool>("update BS_VehicleRadioInfo set VehicleId=@VehicleId, RadioId=@RadioId, StartMileage=@StartMileage, EndMileage=@EndMileage where UserId=@UserId and VehicleRadioId=@VehicleRadioId and LoginTimestamp=@LoginTimestamp",
                                    new { VehicleId = item.VehicleID, RadioId = item.RadioID, StartMileage = item.StartMileage, EndMileage = item.EndMileage, UserId = item.UserID, VehicleRadioId = item.VehicleRadioId, LoginTimestamp = vehicleRadioInfo_loginTimestamp });

                                var message = "VehicleRadioInfo successfully synced";
                                var log_group = (char)eGroups.eSQL;
                                var log_type = (char)eTypes.eSuccess;
                                var log_eventId = (int)eEventId.eInsertStmt;
                                var log_description = "VehicleRadioInfo successfully synced";
                                var log_data = "Borosite";
                                var log_deviceId = gDeviceId;
                                var log_recordNum = 1;

                                LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                                theEvent.Properties["log_group"] = log_group;
                                theEvent.Properties["log_type"] = log_type;
                                theEvent.Properties["log_eventId"] = log_eventId;
                                theEvent.Properties["log_description"] = log_description;
                                theEvent.Properties["log_data"] = log_data;
                                theEvent.Properties["log_deviceId"] = log_deviceId;
                                theEvent.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEvent);
                                LogTxt.Log(theEvent);

                                string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                                LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                                theEventCommands.Properties["log_group"] = log_group;
                                theEventCommands.Properties["log_type"] = log_type;
                                theEventCommands.Properties["log_eventId"] = log_eventId;
                                theEventCommands.Properties["log_description"] = commands;
                                theEventCommands.Properties["log_data"] = log_data;
                                theEventCommands.Properties["log_deviceId"] = log_deviceId;
                                theEventCommands.Properties["log_recordNum"] = log_recordNum;
                                Log.Log(theEventCommands);
                                LogTxt.Log(theEventCommands);
                                blnSuccess = true;
                            }
                        }
                        catch (Exception exc)
                        {
                            var message = String.Format("Failed to sync to BS_VehicleRadioInfo UserId: {0} LoginTimestamp: {1} Id: {2}", item.UserID.ToString(), item.LoginTimeStamp.ToString(), item.VehicleRadioId.ToString());
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eWarning;
                            var log_eventId = 0;
                            var log_description = String.Format("Duplicate: Failed to Upload BS_VehicleRadioInfo UserId: {0} LoginTimestamp: {1} Id: {2}", item.UserID.ToString(), item.LoginTimeStamp.ToString(), item.VehicleRadioId.ToString());
                            var log_data = "Borosite";
                            var log_deviceId = gDeviceId;
                            var log_recordNum = 0;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                            blnSuccess = false;
                        }
                    }
                }
                else
                {
                    blnSuccess = true;
                }
            }
            else
            {
                blnSuccess = true;
            }

            return blnSuccess;
        }

        private async Task<bool> SaveNovInformation(NovInformation item, DbConnection conn)
        {
            if (item != null)
            {
                FixIStatusTicket(item);
                try
                {
                    //NovInformation
                    var novInformation_NovNumber = item.NOVNumber;
                    var novInformation_exists = conn.ExecuteScalar<bool>("select count(1) from BS_HHTNOVInformation where NOVNumber=@NOVNumber", new { NOVNumber = novInformation_NovNumber });

                    if (!novInformation_exists)
                    {
                        item.NOVNumber = item.NOVNumber;
                        item.TicketStatus = !String.IsNullOrEmpty(item.TicketStatus) ? StringTool.Truncate(item.TicketStatus, 2) : item.TicketStatus;
                        item.IssuedTimestamp = item.IssuedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.IssuedTimestamp;
                        item.SystemTimestamp = item.SystemTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SystemTimestamp;
                        item.LoginTimestamp = item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LoginTimestamp;
                        item.ReportLevel = !String.IsNullOrEmpty(item.ReportLevel) ? StringTool.Truncate(item.ReportLevel, 4) : item.ReportLevel;
                        item.IsResp1AddressHit = !String.IsNullOrEmpty(item.IsResp1AddressHit) ? StringTool.Truncate(item.IsResp1AddressHit, 1) : item.IsResp1AddressHit;
                        item.Resp1LastName = !String.IsNullOrEmpty(item.Resp1LastName) ? StringTool.Truncate(item.Resp1LastName, 30) : item.Resp1LastName;
                        item.Resp1FirstName = !String.IsNullOrEmpty(item.Resp1FirstName) ? StringTool.Truncate(item.Resp1FirstName, 25) : item.Resp1FirstName;
                        item.Resp1MiddleInitial = !String.IsNullOrEmpty(item.Resp1MiddleInitial) ? StringTool.Truncate(item.Resp1MiddleInitial, 1) : item.Resp1MiddleInitial;
                        item.Resp1Sex = !String.IsNullOrEmpty(item.Resp1Sex) ? StringTool.Truncate(item.Resp1Sex, 1) : item.Resp1Sex;
                        item.PropertyBBL = !String.IsNullOrEmpty(item.PropertyBBL) ? StringTool.Truncate(item.PropertyBBL, 10) : item.PropertyBBL;
                        item.Resp1DistrictId = !String.IsNullOrEmpty(item.Resp1DistrictId) ? StringTool.Truncate(item.Resp1DistrictId, 3) : item.Resp1DistrictId;
                        item.Resp1SectionId = !String.IsNullOrEmpty(item.Resp1SectionId) ? StringTool.Truncate(item.Resp1SectionId, 2) : item.Resp1SectionId;
                        item.Resp1StreetId = item.Resp1StreetId;
                        item.Resp1Address = !String.IsNullOrEmpty(item.Resp1Address) ? StringTool.Truncate(item.Resp1Address, 50) : item.Resp1Address;
                        item.Resp1Address1 = !String.IsNullOrEmpty(item.Resp1Address1) ? StringTool.Truncate(item.Resp1Address1, 25) : item.Resp1Address1;
                        item.Resp1City = !String.IsNullOrEmpty(item.Resp1City) ? StringTool.Truncate(item.Resp1City, 25) : item.Resp1City;
                        item.Resp1State = !String.IsNullOrEmpty(item.Resp1State) ? StringTool.Truncate(item.Resp1State, 2) : item.Resp1State;
                        item.Resp1Zip = !String.IsNullOrEmpty(item.Resp1Zip) ? StringTool.Truncate(item.Resp1Zip, 10) : item.Resp1Zip;
                        item.LicenseNumber = !String.IsNullOrEmpty(item.LicenseNumber) ? StringTool.Truncate(item.LicenseNumber, 45) : item.LicenseNumber;
                        item.LicenseAgency = !String.IsNullOrEmpty(item.LicenseAgency) ? StringTool.Truncate(item.LicenseAgency, 10) : item.LicenseAgency;
                        item.LicenseType = !String.IsNullOrEmpty(item.LicenseType) ? StringTool.Truncate(item.LicenseType, 10) : item.LicenseType;
                        item.IsPlaceAddressHit = !String.IsNullOrEmpty(item.IsPlaceAddressHit) ? StringTool.Truncate(item.IsPlaceAddressHit, 1) : item.IsPlaceAddressHit;
                        item.PlaceLastName = !String.IsNullOrEmpty(item.PlaceLastName) ? StringTool.Truncate(item.PlaceLastName, 25) : item.PlaceLastName;
                        item.PlaceFirstName = !String.IsNullOrEmpty(item.PlaceFirstName) ? StringTool.Truncate(item.PlaceFirstName, 20) : item.PlaceFirstName;
                        item.PlaceMiddleInitial = !String.IsNullOrEmpty(item.PlaceMiddleInitial) ? StringTool.Truncate(item.PlaceMiddleInitial, 1) : item.PlaceMiddleInitial;
                        item.PlaceBBL = !String.IsNullOrEmpty(item.PlaceBBL) ? StringTool.Truncate(item.PlaceBBL, 10) : item.PlaceBBL;
                        item.PlaceDistrictId = !String.IsNullOrEmpty(item.PlaceDistrictId) ? StringTool.Truncate(item.PlaceDistrictId, 3) : item.PlaceDistrictId;
                        item.PlaceSectionId = !String.IsNullOrEmpty(item.PlaceSectionId) ? StringTool.Truncate(item.PlaceSectionId, 2) : item.PlaceSectionId;
                        item.PlaceStreetId = item.PlaceStreetId;
                        item.PlaceAddress1 = !String.IsNullOrEmpty(item.PlaceAddress1) ? StringTool.Truncate(item.PlaceAddress1, 50) : item.PlaceAddress1;
                        item.PlaceAddress2 = !String.IsNullOrEmpty(item.PlaceAddress2) ? StringTool.Truncate(item.PlaceAddress2, 25) : item.PlaceAddress2;
                        item.PlaceAddressDescriptor = !String.IsNullOrEmpty(item.PlaceAddressDescriptor) ? StringTool.Truncate(item.PlaceAddressDescriptor, 1) : item.PlaceAddressDescriptor;
                        item.PlaceSideOfStreet = !String.IsNullOrEmpty(item.PlaceSideOfStreet) ? StringTool.Truncate(item.PlaceSideOfStreet, 4) : item.PlaceSideOfStreet;
                        item.PlaceCross1StreetId = item.PlaceCross1StreetId;
                        item.PlaceCross2StreetId = item.PlaceCross2StreetId;
                        item.MailableAmount = item.MailableAmount;
                        item.MaximumAmount = item.MaximumAmount;
                        item.HearingTimestamp = item.HearingTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.HearingTimestamp;
                        item.IsAppearRequired = !String.IsNullOrEmpty(item.IsAppearRequired) ? StringTool.Truncate(item.IsAppearRequired, 1) : item.IsAppearRequired;
                        item.AlternateService = !String.IsNullOrEmpty(item.AlternateService) ? StringTool.Truncate(item.AlternateService, 1) : item.AlternateService;
                        item.BuildingType = !String.IsNullOrEmpty(item.BuildingType) ? StringTool.Truncate(item.BuildingType, 2) : item.BuildingType;
                        item.IsMultipleOffences = !String.IsNullOrEmpty(item.IsMultipleOffences) ? StringTool.Truncate(item.IsMultipleOffences, 1) : item.IsMultipleOffences;
                        item.DigitalSignature = !String.IsNullOrEmpty(item.DigitalSignature) ? StringTool.Truncate(item.DigitalSignature, 512) : item.DigitalSignature;
                        item.IsSent = !String.IsNullOrEmpty(item.IsSent) ? StringTool.Truncate(item.IsSent, 1) : item.IsSent;
                        item.SentTimestamp = item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SentTimestamp;
                        item.IsReceived = !String.IsNullOrEmpty(item.IsReceived) ? StringTool.Truncate(item.IsReceived, 1) : item.IsReceived;
                        item.ReceivedTimestamp = item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.ReceivedTimestamp;
                        item.ViolationCode = !String.IsNullOrEmpty(item.ViolationCode) ? StringTool.Truncate(item.ViolationCode, 10) : item.ViolationCode;
                        item.HHTIdentifier = !String.IsNullOrEmpty(item.HHTIdentifier) ? StringTool.Truncate(item.HHTIdentifier, 50) : item.HHTIdentifier;
                        item.ViolationScript = !String.IsNullOrEmpty(item.ViolationScript) ? StringTool.Truncate(item.ViolationScript, 512) : item.ViolationScript;
                        item.UserId = !String.IsNullOrEmpty(item.UserId) ? StringTool.Truncate(item.UserId, 16) : item.UserId;
                        item.VoidCancelScreen = !String.IsNullOrEmpty(item.VoidCancelScreen) ? StringTool.Truncate(item.VoidCancelScreen, 6) : item.VoidCancelScreen;
                        item.PlaceBoroCode = !String.IsNullOrEmpty(item.PlaceBoroCode) ? StringTool.Truncate(item.PlaceBoroCode, 1) : item.PlaceBoroCode;
                        item.PlaceHouseNo = !String.IsNullOrEmpty(item.PlaceHouseNo) ? StringTool.Truncate(item.PlaceHouseNo, 16) : item.PlaceHouseNo;
                        item.Resp1BoroCode = !String.IsNullOrEmpty(item.Resp1BoroCode) ? StringTool.Truncate(item.Resp1BoroCode, 1) : item.Resp1BoroCode;
                        item.Resp1HouseNo = !String.IsNullOrEmpty(item.Resp1HouseNo) ? StringTool.Truncate(item.Resp1HouseNo, 16) : item.Resp1HouseNo;
                        item.ViolationGroupId = item.ViolationGroupId;
                        item.ViolationTypeId = !String.IsNullOrEmpty(item.ViolationTypeId) ? StringTool.Truncate(item.ViolationTypeId, 1) : item.ViolationTypeId;
                        item.MDRNumber = item.MDRNumber;
                        item.LicenseExpDate = item.LicenseExpDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LicenseExpDate;
                        item.BusinessName = item.BusinessName.Truncate(70);
                        item.CheckSum = !String.IsNullOrEmpty(item.CheckSum) ? StringTool.Truncate(item.CheckSum, 1) : item.CheckSum;
                        item.FreeAddrees = !String.IsNullOrEmpty(item.FreeAddrees) ? StringTool.Truncate(item.FreeAddrees, 80) : item.FreeAddrees;
                        item.DeviceId = !String.IsNullOrEmpty(item.DeviceId) ? StringTool.Truncate(item.DeviceId, 10) : item.DeviceId;
                        item.PublicKeyId = item.PublicKeyId;
                        item.PrintViolationCode = !String.IsNullOrEmpty(item.PrintViolationCode) ? StringTool.Truncate(item.PrintViolationCode, 10) : item.PrintViolationCode;
                        item.IsPetitionerCourtAppear = !String.IsNullOrEmpty(item.IsPetitionerCourtAppear) ? StringTool.Truncate(item.IsPetitionerCourtAppear, 1) : item.IsPetitionerCourtAppear;
                        item.LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? StringTool.Truncate(item.LicenseTypeDesc, 32) : item.LicenseTypeDesc;
                        item.ViolGroupName = !String.IsNullOrEmpty(item.ViolGroupName) ? StringTool.Truncate(item.ViolGroupName, 1) : item.ViolGroupName;
                        item.CodeLawDescription = !String.IsNullOrEmpty(item.CodeLawDescription) ? StringTool.Truncate(item.CodeLawDescription, 100) : item.CodeLawDescription;
                        item.OfficerName = !String.IsNullOrEmpty(item.OfficerName) ? StringTool.Truncate(item.OfficerName, 50) : item.OfficerName;
                        item.LawSection = !String.IsNullOrEmpty(item.LawSection) ? StringTool.Truncate(item.LawSection, 50) : item.LawSection;
                        item.AbbrevName = !String.IsNullOrEmpty(item.AbbrevName) ? StringTool.Truncate(item.AbbrevName, 30) : item.AbbrevName;
                        item.AgencyId = !String.IsNullOrEmpty(item.AgencyId) ? StringTool.Truncate(item.AgencyId, 3) : item.AgencyId;
                        item.Title = !String.IsNullOrEmpty(item.Title) ? StringTool.Truncate(item.Title, 30) : item.Title;

                        await conn.InsertAsync(item);

                        var message = "NovInformation successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "NovInformation successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = item.DeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;
                    }
                    else
                    {
                        string sqlLaterSvc = string.Format("select count(1) from BS_HHTNOVInformation where NOVNumber = '{0}' and TicketStatus = 'L'", novInformation_NovNumber);
                        var laterSvcNovExists = conn.ExecuteScalar<bool>(sqlLaterSvc);
                        //var laterSvcNovExists = conn.ExecuteScalar<bool>("select count(1) from BS_HHTNOVInformation where NOVNumber=@NOVNumber and TicketStatus=@TicketStatus", new { NOVNumber = novInformation_NovNumber, TicketStatus="L" });

                        if (laterSvcNovExists)
                        {

                            string sqlsUpdate = string.Format("update BS_HHTNOVInformation set TicketStatus='{0}' where NOVNumber={1}", item.TicketStatus, novInformation_NovNumber);
                            //var updateLaterSvcNov = conn.ExecuteScalar<bool>("update BS_HHTNOVInformation set TicketStatus=@Status where NOVNumber=@NOVNumber", new { NOVNumber = novInformation_NovNumber, TicketStatus = item.TicketStatus });
                            var updateLaterSvcNov = conn.ExecuteScalar<bool>(sqlsUpdate);

                            var message = "NovInformation successfully updated";
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eSuccess;
                            var log_eventId = (int)eEventId.eInsertStmt;
                            var log_description = "NovInformation successfully synced";
                            var log_data = "Borosite";
                            var log_deviceId = item.DeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                            return true;
                        }
                        else
                        {
                            var message = String.Format("Duplicate Failed to Upload to BS_HHTNovInformation NOV #{0}:", item.NOVNumber.ToString());
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eWarning;
                            var log_eventId = 0;
                            var log_description = String.Format("Duplicate Failed to Upload to BS_HHTNovInformation NOV #{0}:", item.NOVNumber.ToString());
                            var log_data = "Borosite";
                            var log_deviceId = item.DeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            //Insert into dupNovInformation 
                            var res = conn.ExecuteScalar($"INSERT INTO BS_DupHHTNovinformation ([NOVNumber],[TicketStatus],[IssuedTimestamp],[SystemTimestamp],[LoginTimestamp],[ReportLevel]" +
                                                ",[IsResp1AddressHit],[Resp1LastName],[Resp1FirstName],[Resp1MiddleInitial],[Resp1Sex],[PropertyBBL],[Resp1DistrictId]" +
                                                ",[Resp1SectionId],[Resp1StreetId],[Resp1Address],[Resp1Address1],[Resp1City],[Resp1State],[Resp1Zip],[LicenseNumber]" +
                                                ",[LicenseAgency],[LicenseType],[IsPlaceAddressHit],[PlaceLastName],[PlaceFirstName],[PlaceMiddleInitial]" +
                                                ",[PlaceBBL],[PlaceDistrictId],[PlaceSectionId],[PlaceStreetId],[PlaceAddress1],[PlaceAddress2],[PlaceAddressDescriptor]," +
                                                "[PlaceSideOfStreet],[PlaceCross1StreetId],[PlaceCross2StreetId],[MailableAmount]" +
                                                ",[MaximumAmount],[HearingTimestamp],[IsAppearRequired],[AlternateService],[BuildingType],[IsMultipleOffences]" +
                                                ",[DigitalSignature],[IsSent],[SentTimestamp],[IsReceived],[ReceivedTimestamp],[ViolationCode],[HHTIdentifier]" +
                                                ",[ViolationScript],[UserId],[VoidCancelScreen],[PlaceBoroCode],[PlaceHouseNo],[Resp1BoroCode],[Resp1HouseNo]" +
                                                ",[ViolationGroupId],[ViolationTypeId],[MDRNumber],[LicenseExpDate],[BusinessName],[CheckSum]" +
                                                ",[FreeAddrees],[DeviceId],[PublicKeyId],[PrintViolationCode],[IsPetitionerCourtAppear],[LicenseTypeDesc]" +
                                                ",[ViolGroupName],[CodeLawDescription],[OfficerName],[LawSection],[AbbrevName],[AgencyId],[Title])" +
                                                "VALUES" +
                                                " (@NOVNumber, @TicketStatus, @IssuedTimestamp, @SystemTimestamp, @LoginTimestamp, @ReportLevel, @IsResp1AddressHit, @Resp1LastName" +
                                                " , @Resp1FirstName, @Resp1MiddleInitial, @Resp1Sex, @PropertyBBL, @Resp1DistrictId, @Resp1SectionId, @Resp1StreetId, @Resp1Address" +
                                                "  , @Resp1Address1, @Resp1City, @Resp1State, @Resp1Zip, @LicenseNumber, @LicenseAgency, @LicenseType, @IsPlaceAddressHit, @PlaceLastName" +
                                                "  , @PlaceFirstName, @PlaceMiddleInitial, @PlaceBBL, @PlaceDistrictId, @PlaceSectionId, @PlaceStreetId, @PlaceAddress1, @PlaceAddress2" +
                                                "  , @PlaceAddressDescriptor, @PlaceSideOfStreet, @PlaceCross1StreetId, @PlaceCross2StreetId, @MailableAmount, @MaximumAmount" +
                                                "  , @HearingTimestamp, @IsAppearRequired, @AlternateService, @BuildingType, @IsMultipleOffences, @DigitalSignature, @IsSent" +
                                                "  , @SentTimestamp, @IsReceived, @ReceivedTimestamp, @ViolationCode, @HHTIdentifier, @ViolationScript, @UserId, @VoidCancelScreen" +
                                                " , @PlaceBoroCode, @PlaceHouseNo, @Resp1BoroCode, @Resp1HouseNo, @ViolationGroupId, @ViolationTypeId, @MDRNumber, @LicenseExpDate" +
                                                " , @BusinessName, @CheckSum, @FreeAddrees, @DeviceId, @PublicKeyId, @PrintViolationCode, @IsPetitionerCourtAppear" +
                                                "  , @LicenseTypeDesc, @ViolGroupName, @CodeLawDescription, @OfficerName, @LawSection, @AbbrevName, @AgencyId, @Title)"
                                                ,
                                                new
                                                {
                                                    NOVNumber = item.NOVNumber,
                                                    TicketStatus = !String.IsNullOrEmpty(item.TicketStatus) ? StringTool.Truncate(item.TicketStatus, 2) : item.TicketStatus,
                                                    IssuedTimestamp = item.IssuedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.IssuedTimestamp,
                                                    SystemTimestamp = item.SystemTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SystemTimestamp,
                                                    LoginTimestamp = item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LoginTimestamp,
                                                    ReportLevel = !String.IsNullOrEmpty(item.ReportLevel) ? StringTool.Truncate(item.ReportLevel, 4) : item.ReportLevel,
                                                    IsResp1AddressHit = !String.IsNullOrEmpty(item.IsResp1AddressHit) ? StringTool.Truncate(item.IsResp1AddressHit, 1) : item.IsResp1AddressHit,
                                                    Resp1LastName = !String.IsNullOrEmpty(item.Resp1LastName) ? StringTool.Truncate(item.Resp1LastName, 30) : item.Resp1LastName,
                                                    Resp1FirstName = !String.IsNullOrEmpty(item.Resp1FirstName) ? StringTool.Truncate(item.Resp1FirstName, 25) : item.Resp1FirstName,
                                                    Resp1MiddleInitial = !String.IsNullOrEmpty(item.Resp1MiddleInitial) ? StringTool.Truncate(item.Resp1MiddleInitial, 1) : item.Resp1MiddleInitial,
                                                    Resp1Sex = !String.IsNullOrEmpty(item.Resp1Sex) ? StringTool.Truncate(item.Resp1Sex, 1) : item.Resp1Sex,
                                                    PropertyBBL = !String.IsNullOrEmpty(item.PropertyBBL) ? StringTool.Truncate(item.PropertyBBL, 10) : item.PropertyBBL,
                                                    Resp1DistrictId = !String.IsNullOrEmpty(item.Resp1DistrictId) ? StringTool.Truncate(item.Resp1DistrictId, 3) : item.Resp1DistrictId,
                                                    Resp1SectionId = !String.IsNullOrEmpty(item.Resp1SectionId) ? StringTool.Truncate(item.Resp1SectionId, 2) : item.Resp1SectionId,
                                                    Resp1StreetId = item.Resp1StreetId,
                                                    Resp1Address = !String.IsNullOrEmpty(item.Resp1Address) ? StringTool.Truncate(item.Resp1Address, 50) : item.Resp1Address,
                                                    Resp1Address1 = !String.IsNullOrEmpty(item.Resp1Address1) ? StringTool.Truncate(item.Resp1Address1, 25) : item.Resp1Address1,
                                                    Resp1City = !String.IsNullOrEmpty(item.Resp1City) ? StringTool.Truncate(item.Resp1City, 25) : item.Resp1City,
                                                    Resp1State = !String.IsNullOrEmpty(item.Resp1State) ? StringTool.Truncate(item.Resp1State, 2) : item.Resp1State,
                                                    Resp1Zip = !String.IsNullOrEmpty(item.Resp1Zip) ? StringTool.Truncate(item.Resp1Zip, 10) : item.Resp1Zip,
                                                    LicenseNumber = !String.IsNullOrEmpty(item.LicenseNumber) ? StringTool.Truncate(item.LicenseNumber, 45) : item.LicenseNumber,
                                                    LicenseAgency = !String.IsNullOrEmpty(item.LicenseAgency) ? StringTool.Truncate(item.LicenseAgency, 10) : item.LicenseAgency,
                                                    LicenseType = !String.IsNullOrEmpty(item.LicenseType) ? StringTool.Truncate(item.LicenseType, 10) : item.LicenseType,
                                                    IsPlaceAddressHit = !String.IsNullOrEmpty(item.IsPlaceAddressHit) ? StringTool.Truncate(item.IsPlaceAddressHit, 1) : item.IsPlaceAddressHit,
                                                    PlaceLastName = !String.IsNullOrEmpty(item.PlaceLastName) ? StringTool.Truncate(item.PlaceLastName, 25) : item.PlaceLastName,
                                                    PlaceFirstName = !String.IsNullOrEmpty(item.PlaceFirstName) ? StringTool.Truncate(item.PlaceFirstName, 20) : item.PlaceFirstName,
                                                    PlaceMiddleInitial = !String.IsNullOrEmpty(item.PlaceMiddleInitial) ? StringTool.Truncate(item.PlaceMiddleInitial, 1) : item.PlaceMiddleInitial,
                                                    PlaceBBL = !String.IsNullOrEmpty(item.PlaceBBL) ? StringTool.Truncate(item.PlaceBBL, 10) : item.PlaceBBL,
                                                    PlaceDistrictId = !String.IsNullOrEmpty(item.PlaceDistrictId) ? StringTool.Truncate(item.PlaceDistrictId, 3) : item.PlaceDistrictId,
                                                    PlaceSectionId = !String.IsNullOrEmpty(item.PlaceSectionId) ? StringTool.Truncate(item.PlaceSectionId, 2) : item.PlaceSectionId,
                                                    PlaceStreetId = item.PlaceStreetId,
                                                    PlaceAddress1 = !String.IsNullOrEmpty(item.PlaceAddress1) ? StringTool.Truncate(item.PlaceAddress1, 50) : item.PlaceAddress1,
                                                    PlaceAddress2 = !String.IsNullOrEmpty(item.PlaceAddress2) ? StringTool.Truncate(item.PlaceAddress2, 25) : item.PlaceAddress2,
                                                    PlaceAddressDescriptor = !String.IsNullOrEmpty(item.PlaceAddressDescriptor) ? StringTool.Truncate(item.PlaceAddressDescriptor, 1) : item.PlaceAddressDescriptor,
                                                    PlaceSideOfStreet = !String.IsNullOrEmpty(item.PlaceSideOfStreet) ? StringTool.Truncate(item.PlaceSideOfStreet, 4) : item.PlaceSideOfStreet,
                                                    PlaceCross1StreetId = item.PlaceCross1StreetId,
                                                    PlaceCross2StreetId = item.PlaceCross2StreetId,
                                                    MailableAmount = item.MailableAmount,
                                                    MaximumAmount = item.MaximumAmount,
                                                    HearingTimestamp = item.HearingTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.HearingTimestamp,
                                                    IsAppearRequired = !String.IsNullOrEmpty(item.IsAppearRequired) ? StringTool.Truncate(item.IsAppearRequired, 1) : item.IsAppearRequired,
                                                    AlternateService = !String.IsNullOrEmpty(item.AlternateService) ? StringTool.Truncate(item.AlternateService, 1) : item.AlternateService,
                                                    BuildingType = !String.IsNullOrEmpty(item.BuildingType) ? StringTool.Truncate(item.BuildingType, 2) : item.BuildingType,
                                                    IsMultipleOffences = !String.IsNullOrEmpty(item.IsMultipleOffences) ? StringTool.Truncate(item.IsMultipleOffences, 1) : item.IsMultipleOffences,
                                                    DigitalSignature = !String.IsNullOrEmpty(item.DigitalSignature) ? StringTool.Truncate(item.DigitalSignature, 512) : item.DigitalSignature,
                                                    IsSent = !String.IsNullOrEmpty(item.IsSent) ? StringTool.Truncate(item.IsSent, 1) : item.IsSent,
                                                    SentTimestamp = item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SentTimestamp,
                                                    IsReceived = !String.IsNullOrEmpty(item.IsReceived) ? StringTool.Truncate(item.IsReceived, 1) : item.IsReceived,
                                                    ReceivedTimestamp = item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.ReceivedTimestamp,
                                                    ViolationCode = !String.IsNullOrEmpty(item.ViolationCode) ? StringTool.Truncate(item.ViolationCode, 10) : item.ViolationCode,
                                                    HHTIdentifier = !String.IsNullOrEmpty(item.HHTIdentifier) ? StringTool.Truncate(item.HHTIdentifier, 50) : item.HHTIdentifier,
                                                    ViolationScript = !String.IsNullOrEmpty(item.ViolationScript) ? StringTool.Truncate(item.ViolationScript, 512) : item.ViolationScript,
                                                    UserId = !String.IsNullOrEmpty(item.UserId) ? StringTool.Truncate(item.UserId, 16) : item.UserId,
                                                    VoidCancelScreen = !String.IsNullOrEmpty(item.VoidCancelScreen) ? StringTool.Truncate(item.VoidCancelScreen, 6) : item.VoidCancelScreen,
                                                    PlaceBoroCode = !String.IsNullOrEmpty(item.PlaceBoroCode) ? StringTool.Truncate(item.PlaceBoroCode, 1) : item.PlaceBoroCode,
                                                    PlaceHouseNo = !String.IsNullOrEmpty(item.PlaceHouseNo) ? StringTool.Truncate(item.PlaceHouseNo, 16) : item.PlaceHouseNo,
                                                    Resp1BoroCode = !String.IsNullOrEmpty(item.Resp1BoroCode) ? StringTool.Truncate(item.Resp1BoroCode, 1) : item.Resp1BoroCode,
                                                    Resp1HouseNo = !String.IsNullOrEmpty(item.Resp1HouseNo) ? StringTool.Truncate(item.Resp1HouseNo, 16) : item.Resp1HouseNo,
                                                    ViolationGroupId = item.ViolationGroupId,
                                                    ViolationTypeId = !String.IsNullOrEmpty(item.ViolationTypeId) ? StringTool.Truncate(item.ViolationTypeId, 1) : item.ViolationTypeId,
                                                    MDRNumber = item.MDRNumber,
                                                    LicenseExpDate = item.LicenseExpDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LicenseExpDate,
                                                    BusinessName = item.BusinessName.Truncate(70),
                                                    CheckSum = !String.IsNullOrEmpty(item.CheckSum) ? StringTool.Truncate(item.CheckSum, 1) : item.CheckSum,
                                                    FreeAddrees = !String.IsNullOrEmpty(item.FreeAddrees) ? StringTool.Truncate(item.FreeAddrees, 80) : item.FreeAddrees,
                                                    DeviceId = !String.IsNullOrEmpty(item.DeviceId) ? StringTool.Truncate(item.DeviceId, 10) : item.DeviceId,
                                                    PublicKeyId = item.PublicKeyId,
                                                    PrintViolationCode = !String.IsNullOrEmpty(item.PrintViolationCode) ? StringTool.Truncate(item.PrintViolationCode, 10) : item.PrintViolationCode,
                                                    IsPetitionerCourtAppear = !String.IsNullOrEmpty(item.IsPetitionerCourtAppear) ? StringTool.Truncate(item.IsPetitionerCourtAppear, 1) : item.IsPetitionerCourtAppear,
                                                    LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? StringTool.Truncate(item.LicenseTypeDesc, 32) : item.LicenseTypeDesc,
                                                    ViolGroupName = !String.IsNullOrEmpty(item.ViolGroupName) ? StringTool.Truncate(item.ViolGroupName, 1) : item.ViolGroupName,
                                                    CodeLawDescription = !String.IsNullOrEmpty(item.CodeLawDescription) ? StringTool.Truncate(item.CodeLawDescription, 100) : item.CodeLawDescription,
                                                    OfficerName = !String.IsNullOrEmpty(item.OfficerName) ? StringTool.Truncate(item.OfficerName, 50) : item.OfficerName,
                                                    LawSection = !String.IsNullOrEmpty(item.LawSection) ? StringTool.Truncate(item.LawSection, 50) : item.LawSection,
                                                    AbbrevName = !String.IsNullOrEmpty(item.AbbrevName) ? StringTool.Truncate(item.AbbrevName, 30) : item.AbbrevName,
                                                    AgencyId = !String.IsNullOrEmpty(item.AgencyId) ? StringTool.Truncate(item.AgencyId, 3) : item.AgencyId,
                                                    Title = !String.IsNullOrEmpty(item.Title) ? StringTool.Truncate(item.Title, 30) : item.Title,
                                                });


                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                            return true;
                        }
                    }
                }
                catch (Exception exc)
                {
                    var message = String.Format("Failed to insert into BS_HHTNovInformation NOV #{0}: {1}", item.NOVNumber.ToString(), exc.Message);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = String.Format("Duplicate Failed to Upload to BS_HHTNovInformation NOV #{0}:", item.NOVNumber.ToString());
                    var log_data = "Borosite";
                    var log_deviceId = item.DeviceId;
                    var log_recordNum = 1;

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = commands;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    return false;
                }

            }
            return true;
        }

        private  void FixIStatusTicket(NovInformation nov)
        {
            if(nov.TicketStatus.ToUpper()=="I")
            {
                if (nov.NOVNumber == null)
                    nov.NOVNumber = 0;
                if (string.IsNullOrEmpty(nov.Resp1Address1))
                    nov.Resp1Address1 = "No address";
                if (string.IsNullOrEmpty(nov.PlaceDistrictId))
                    nov.PlaceDistrictId = "0";
                if (string.IsNullOrEmpty(nov.PlaceSectionId))
                    nov.PlaceSectionId = "0";
                if (nov.PlaceStreetId == null)
                    nov.PlaceStreetId = 0;
                if (string.IsNullOrEmpty(nov.HHTIdentifier))
                    nov.HHTIdentifier = "No HHT Indetifier";
                if (string.IsNullOrEmpty(nov.ViolationScript))
                    nov.ViolationScript = "No script";
                if (string.IsNullOrEmpty(nov.UserId))
                    nov.UserId = "Unknown";
            }
            
        }
        private async Task<bool> SaveNovCertificate(NovCertificate item, DbConnection conn)
        {
            if (item != null)
            {
                try
                {
                    var novNumber = item.NovNumber;
                    var certificateExists = conn.ExecuteScalar<bool>("select count(1) from [digi].[BS_NOVPublicKey] where NOVNumber=@NOVNumber", new { NOVNumber = novNumber });

                    if (!certificateExists)
                    {
                        await conn.InsertAsync(item);

                        var message = "NOV Cerificate successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "NOV certificate successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;

                    }
                }
                catch (Exception ex)
                {
                    var message = String.Format("Failed to sync to BS_NOVPublicKey NovNumber: {0}", item.NovNumber);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = ex.StackTrace;
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 0;

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = log_description;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                    LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                    theEventCommands.Properties["log_group"] = log_group;
                    theEventCommands.Properties["log_type"] = log_type;
                    theEventCommands.Properties["log_eventId"] = log_eventId;
                    theEventCommands.Properties["log_description"] = commands;
                    theEventCommands.Properties["log_data"] = log_data;
                    theEventCommands.Properties["log_deviceId"] = log_deviceId;
                    theEventCommands.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEventCommands);
                    LogTxt.Log(theEventCommands);
                    return false;

                }
            }

            return true;
        }

        private async Task<bool> SaveNovData(NovData[] items, DbConnection conn)
        {
            if (items != null && items.Length > 0)
            {
                foreach (var item in items)
                {
                    try
                    {
                        //NovData
                        var novData_NovNumber = item.NOVNumber;
                        var novData_Field = item.FieldName;
                        var novData_exists = conn.ExecuteScalar<bool>("select count(1) from BS_HHNOVData where NOVNumber=@NOVNumber and FieldName=@FieldName", new { NOVNumber = novData_NovNumber, FieldName = novData_Field });


                        if (!novData_exists)
                        {
                            await conn.InsertAsync(item);

                            var message = "NovData successfully synced";
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eSuccess;
                            var log_eventId = (int)eEventId.eInsertStmt;
                            var log_description = "NovData successfully synced";
                            var log_data = "Borosite";
                            var log_deviceId = gDeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                        }
                        else
                        {

                            var message = String.Format("Duplicate Failed to upload to BS_HHTNovData Nov # {0}:", item.NOVNumber.ToString());
                            var log_group = (char)eGroups.eSQL;
                            var log_type = (char)eTypes.eWarning;
                            var log_eventId = 0;
                            var log_description = String.Format("Duplicate Failed to upload to BS_HHTNovData Nov #{0}:", item.NOVNumber.ToString());
                            var log_data = "Borosite";
                            var log_deviceId = gDeviceId;
                            var log_recordNum = 1;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);
                        }
                    }
                    catch (Exception exc)
                    {
                        var message = String.Format("Failed to insert into BS_HHTNovData NOV #{0}: {1}", item.NOVNumber.ToString(), exc.Message);
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Duplicate Failed to Upload to BS_HHTNovData NOV #{0}:", item.NOVNumber.ToString());
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = commands;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        return false;
                    }
                }

            }
            return true;
        }

        private async Task<bool> SaveAffidavitOfService(AffidavitOfService item, DbConnection conn)
        {
            if (item != null)
            {
                item.PersonallyServeFlag = !String.IsNullOrEmpty(item.PersonallyServeFlag) ? StringTool.Truncate(item.PersonallyServeFlag, 1) : item.PersonallyServeFlag;
                item.Sex = !String.IsNullOrEmpty(item.Sex) ? StringTool.Truncate(item.Sex, 1) : item.Sex;
                item.Age = !String.IsNullOrEmpty(item.Age) ? StringTool.Truncate(item.Age, 10) : item.Age;
                item.HairColor = !String.IsNullOrEmpty(item.HairColor) ? StringTool.Truncate(item.HairColor, 1) : item.HairColor;
                item.SkinColor = !String.IsNullOrEmpty(item.SkinColor) ? StringTool.Truncate(item.SkinColor, 1) : item.SkinColor;
                item.Height = !String.IsNullOrEmpty(item.Height) ? StringTool.Truncate(item.Height, 10) : item.Height;
                item.Weight = !String.IsNullOrEmpty(item.Weight) ? StringTool.Truncate(item.Weight, 15) : item.Weight;
                item.OtherIdentifying = !String.IsNullOrEmpty(item.OtherIdentifying) ? StringTool.Truncate(item.OtherIdentifying, 30) : item.OtherIdentifying;
                // item.ExpDate = !String.IsNullOrEmpty(item.ExpDate) ? StringTool.Truncate(item.ExpDate, 45) : item.ExpDate;
                item.TypeOfLicense = !String.IsNullOrEmpty(item.TypeOfLicense) ? StringTool.Truncate(item.TypeOfLicense, 10) : item.TypeOfLicense;
                item.IssuedNy = !String.IsNullOrEmpty(item.IssuedNy) ? StringTool.Truncate(item.IssuedNy, 10) : item.IssuedNy;
                item.AlternativeService1 = !String.IsNullOrEmpty(item.AlternativeService1) ? StringTool.Truncate(item.AlternativeService1, 1) : item.AlternativeService1;
                item.Comments = !String.IsNullOrEmpty(item.Comments) ? StringTool.Truncate(item.Comments, 50) : item.Comments;
                //item.SignDate = !String.IsNullOrEmpty(item.SignDate) ? StringTool.Truncate(item.SignDate, 30) : item.SignDate;
                item.UserId = !String.IsNullOrEmpty(item.UserId) ? StringTool.Truncate(item.UserId, 16) : item.UserId;
                //item.PublickKeyID = !String.IsNullOrEmpty(item.PublickKeyID) ? StringTool.Truncate(item.PublickKeyID, 30) : item.PublickKeyID;
                item.DigitalSignature = !String.IsNullOrEmpty(item.DigitalSignature) ? StringTool.Truncate(item.DigitalSignature, 512) : item.DigitalSignature;
                item.ServedCounty = !String.IsNullOrEmpty(item.ServedCounty) ? StringTool.Truncate(item.ServedCounty, 20) : item.ServedCounty;

                item.ServiceTo = !String.IsNullOrEmpty(item.ServiceTo) ? StringTool.Truncate(item.ServiceTo, 1) : item.ServiceTo;
                item.ServedTitle = !String.IsNullOrEmpty(item.ServedTitle) ? StringTool.Truncate(item.ServedTitle, 1) : item.ServedTitle;
                item.ServedTitleOther = !String.IsNullOrEmpty(item.ServedTitleOther) ? StringTool.Truncate(item.ServedTitleOther, 15) : item.ServedTitleOther;
                item.ServedLName = !String.IsNullOrEmpty(item.ServedLName) ? StringTool.Truncate(item.ServedLName, 25) : item.ServedLName;
                item.ServedFName = !String.IsNullOrEmpty(item.ServedFName) ? StringTool.Truncate(item.ServedFName, 25) : item.ServedFName;
                item.ServedMInit = !String.IsNullOrEmpty(item.ServedMInit) ? StringTool.Truncate(item.ServedMInit, 1) : item.ServedMInit;

                item.ServedSex = !String.IsNullOrEmpty(item.ServedSex) ? StringTool.Truncate(item.ServedSex, 1) : item.ServedSex;
                item.ServedAddress = !String.IsNullOrEmpty(item.ServedAddress) ? StringTool.Truncate(item.ServedAddress, 50) : item.ServedAddress;
                item.ServedCity = !String.IsNullOrEmpty(item.ServedCity) ? StringTool.Truncate(item.ServedCity, 25) : item.ServedCity;

                item.ServedState = !String.IsNullOrEmpty(item.ServedState) ? StringTool.Truncate(item.ServedState, 2) : item.ServedState;
                item.ServedZip = !String.IsNullOrEmpty(item.ServedZip) ? StringTool.Truncate(item.ServedZip, 5) : item.ServedZip;
                item.AlternativeService2 = !String.IsNullOrEmpty(item.AlternativeService2) ? StringTool.Truncate(item.AlternativeService2, 1) : item.AlternativeService2;
                item.PremiseLName = !String.IsNullOrEmpty(item.PremiseLName) ? StringTool.Truncate(item.PremiseLName, 25) : item.PremiseLName;
                item.PremiseFName = !String.IsNullOrEmpty(item.PremiseFName) ? StringTool.Truncate(item.PremiseFName, 25) : item.PremiseFName;
                item.PremiseMInit = !String.IsNullOrEmpty(item.PremiseMInit) ? StringTool.Truncate(item.PremiseMInit, 1) : item.PremiseMInit;
                item.ServedLocation = !String.IsNullOrEmpty(item.ServedLocation) ? StringTool.Truncate(item.ServedLocation, 20) : item.ServedLocation;
                item.CheckSum = !String.IsNullOrEmpty(item.CheckSum) ? StringTool.Truncate(item.CheckSum, 1) : item.CheckSum;
                item.DeviceId = !String.IsNullOrEmpty(item.DeviceId) ? StringTool.Truncate(item.DeviceId, 10) : item.DeviceId;
                item.LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? StringTool.Truncate(item.LicenseTypeDesc, 32) : item.LicenseTypeDesc;
                item.OfficerName = !String.IsNullOrEmpty(item.OfficerName) ? StringTool.Truncate(item.OfficerName, 50) : item.OfficerName;

                item.AbbrevName = !String.IsNullOrEmpty(item.AbbrevName) ? StringTool.Truncate(item.AbbrevName, 30) : item.AbbrevName;
                item.AgencyId = !String.IsNullOrEmpty(item.AgencyId) ? StringTool.Truncate(item.AgencyId, 3) : item.AgencyId;
                item.Title = !String.IsNullOrEmpty(item.Title) ? StringTool.Truncate(item.Title, 30) : item.Title;
                //item.LoginTimestamp = !String.IsNullOrEmpty(item.LoginTimestamp) ? StringTool.Truncate(item.LoginTimestamp, 30) : item.LoginTimestamp;
                item.ServedHouseNo = !String.IsNullOrEmpty(item.ServedHouseNo) ? StringTool.Truncate(item.ServedHouseNo, 16) : item.ServedHouseNo;
                item.ServedBoroCode = !String.IsNullOrEmpty(item.ServedBoroCode) ? StringTool.Truncate(item.ServedBoroCode, 30) : item.ServedBoroCode;

                try
                {
                    //AffidavitOfService
                    var affidavitOfService_NovNumber = item.NOVNumber;
                    var affidavitOfService_exists = conn.ExecuteScalar<bool>("select count(1) from BS_AffidavitOfService where NOVNumber=@NOVNumber", new { NOVNumber = affidavitOfService_NovNumber });
                    var AOSTable = !affidavitOfService_exists ? "BS_AffidavitOfService" : "[Ticketdata].[BS_DupAffidavitOfService]";


                    if (!affidavitOfService_exists)
                    {

                        if (item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.LoginTimestamp = null;
                        }

                        if (item.ExpDate.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.ExpDate = null;
                        }

                        if (item.SignDate.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.SignDate = null;
                        }
                        #region dad code
                        /*
                        item.PersonallyServeFlag = !String.IsNullOrEmpty(item.PersonallyServeFlag) ? StringTool.Truncate(item.PersonallyServeFlag, 1) : item.PersonallyServeFlag;
                        item.Sex = !String.IsNullOrEmpty(item.Sex) ? StringTool.Truncate(item.Sex, 1) : item.Sex;
                        item.Age = !String.IsNullOrEmpty(item.Age) ? StringTool.Truncate(item.Age, 10) : item.Age;
                        item.HairColor = !String.IsNullOrEmpty(item.HairColor) ? StringTool.Truncate(item.HairColor, 1) : item.HairColor;
                        item.SkinColor = !String.IsNullOrEmpty(item.SkinColor) ? StringTool.Truncate(item.SkinColor, 1) : item.SkinColor;
                        item.Height = !String.IsNullOrEmpty(item.Height) ? StringTool.Truncate(item.Height, 10) : item.Height;
                        item.Weight = !String.IsNullOrEmpty(item.Weight) ? StringTool.Truncate(item.Weight, 15) : item.Weight;
                        item.OtherIdentifying = !String.IsNullOrEmpty(item.OtherIdentifying) ? StringTool.Truncate(item.OtherIdentifying, 30) : item.OtherIdentifying;
                       // item.ExpDate = !String.IsNullOrEmpty(item.ExpDate) ? StringTool.Truncate(item.ExpDate, 45) : item.ExpDate;
                        item.TypeOfLicense = !String.IsNullOrEmpty(item.TypeOfLicense) ? StringTool.Truncate(item.TypeOfLicense, 10) : item.TypeOfLicense;
                        item.IssuedNy = !String.IsNullOrEmpty(item.IssuedNy) ? StringTool.Truncate(item.IssuedNy, 10) : item.IssuedNy;
                        item.AlternativeService1 = !String.IsNullOrEmpty(item.AlternativeService1) ? StringTool.Truncate(item.AlternativeService1, 1) : item.AlternativeService1;
                        item.Comments = !String.IsNullOrEmpty(item.Comments) ? StringTool.Truncate(item.Comments, 50) : item.Comments;
                        //item.SignDate = !String.IsNullOrEmpty(item.SignDate) ? StringTool.Truncate(item.SignDate, 30) : item.SignDate;
                        item.UserId = !String.IsNullOrEmpty(item.UserId) ? StringTool.Truncate(item.UserId, 16) : item.UserId;
                        //item.PublickKeyID = !String.IsNullOrEmpty(item.PublickKeyID) ? StringTool.Truncate(item.PublickKeyID, 30) : item.PublickKeyID;
                        item.DigitalSignature = !String.IsNullOrEmpty(item.DigitalSignature) ? StringTool.Truncate(item.DigitalSignature, 512) : item.DigitalSignature;
                        item.ServedCounty = !String.IsNullOrEmpty(item.ServedCounty) ? StringTool.Truncate(item.ServedCounty, 20) : item.ServedCounty;

                        item.ServiceTo = !String.IsNullOrEmpty(item.ServiceTo) ? StringTool.Truncate(item.ServiceTo, 1) : item.ServiceTo;
                        item.ServedTitle = !String.IsNullOrEmpty(item.ServedTitle) ? StringTool.Truncate(item.ServedTitle, 1) : item.ServedTitle;
                        item.ServedTitleOther = !String.IsNullOrEmpty(item.ServedTitleOther) ? StringTool.Truncate(item.ServedTitleOther, 15) : item.ServedTitleOther;
                        item.ServedLName = !String.IsNullOrEmpty(item.ServedLName) ? StringTool.Truncate(item.ServedLName, 25) : item.ServedLName;
                        item.ServedFName = !String.IsNullOrEmpty(item.ServedFName) ? StringTool.Truncate(item.ServedFName, 25) : item.ServedFName;
                        item.ServedMInit = !String.IsNullOrEmpty(item.ServedMInit) ? StringTool.Truncate(item.ServedMInit, 1) : item.ServedMInit;

                        item.ServedSex = !String.IsNullOrEmpty(item.ServedSex) ? StringTool.Truncate(item.ServedSex, 1) : item.ServedSex;
                        item.ServedAddress = !String.IsNullOrEmpty(item.ServedAddress) ? StringTool.Truncate(item.ServedAddress, 50) : item.ServedAddress;
                        item.ServedCity = !String.IsNullOrEmpty(item.ServedCity) ? StringTool.Truncate(item.ServedCity, 25) : item.ServedCity;

                        item.ServedState = !String.IsNullOrEmpty(item.ServedState) ? StringTool.Truncate(item.ServedState, 2) : item.ServedState;
                        item.ServedZip = !String.IsNullOrEmpty(item.ServedZip) ? StringTool.Truncate(item.ServedZip, 5) : item.ServedZip;
                        item.AlternativeService2 = !String.IsNullOrEmpty(item.AlternativeService2) ? StringTool.Truncate(item.AbbrevName, 1) : item.AlternativeService2;
                        item.PremiseLName = !String.IsNullOrEmpty(item.PremiseLName) ? StringTool.Truncate(item.PremiseLName, 25) : item.PremiseLName;
                        item.PremiseFName = !String.IsNullOrEmpty(item.PremiseFName) ? StringTool.Truncate(item.PremiseFName, 25) : item.PremiseFName;
                        item.PremiseMInit = !String.IsNullOrEmpty(item.PremiseMInit) ? StringTool.Truncate(item.PremiseMInit, 1) : item.PremiseMInit;
                        item.ServedLocation = !String.IsNullOrEmpty(item.ServedLocation) ? StringTool.Truncate(item.ServedLocation, 20) : item.ServedLocation;
                        item.CheckSum = !String.IsNullOrEmpty(item.CheckSum) ? StringTool.Truncate(item.CheckSum, 1) : item.CheckSum;
                        item.DeviceId = !String.IsNullOrEmpty(item.DeviceId) ? StringTool.Truncate(item.DeviceId, 10) : item.DeviceId;
                        item.LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? StringTool.Truncate(item.LicenseTypeDesc, 32) : item.LicenseTypeDesc;
                        item.OfficerName = !String.IsNullOrEmpty(item.OfficerName) ? StringTool.Truncate(item.OfficerName, 50) : item.OfficerName;

                        item.AbbrevName = !String.IsNullOrEmpty(item.AbbrevName) ? StringTool.Truncate(item.AbbrevName, 30) : item.AbbrevName;
                        item.AgencyId = !String.IsNullOrEmpty(item.AgencyId) ? StringTool.Truncate(item.AgencyId, 3) : item.AgencyId;
                        item.Title = !String.IsNullOrEmpty(item.Title) ? StringTool.Truncate(item.Title, 30) : item.Title;
                        //item.LoginTimestamp = !String.IsNullOrEmpty(item.LoginTimestamp) ? StringTool.Truncate(item.LoginTimestamp, 30) : item.LoginTimestamp;
                        item.ServedHouseNo = !String.IsNullOrEmpty(item.ServedHouseNo) ? StringTool.Truncate(item.ServedHouseNo, 16) : item.ServedHouseNo;
                        item.ServedBoroCode = !String.IsNullOrEmpty(item.ServedBoroCode) ? StringTool.Truncate(item.ServedBoroCode, 30) : item.ServedBoroCode;
                        */
                        #endregion
                        await conn.InsertAsync(item);

                        var message = "AffidavitOfService successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "AffidavitOfService successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;
                    }
                    else
                    {

                        var message = String.Format("Duplicate Failed to Upload BS_AffidavitOfService NOV #: {0}", item.NOVNumber.ToString());
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Duplicate Failed to Upload BS_AffidavitOfService NOV #: {0}", item.NOVNumber.ToString());
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        var res = conn.ExecuteScalar($"INSERT INTO {AOSTable} ([NOVNumber],[PersonallyServeFlag],[Sex],[Age],[HairColor],[SkinColor]" +
                                             ",[Height],[Weight],[OtherIdentifying],[LicenseNumber],[ExpDate],[TypeofLicense],[IssuedNy],[AlternativeService1]" +
                                             ",[Comments],[SignDate],[UserID],[PublickKeyID],[DigitalSignature],[ServedCounty],[ServiceTo],[ServedTitle]" +
                                             ",[ServedTitleOther],[ServedLName],[ServedFName],[ServedMInit],[ServedSex],[ServedAddress]" +
                                             ",[ServedCity],[ServedState],[ServedZip],[AlternativeService2],[PremiseLName]" +
                                             ",[PremiseFName],[PremiseMInit],[ServedLocation],[CheckSum],[DeviceId],[LicenseTypeDesc],[OfficerName]," +
                                             "[AbbrevName],[AgencyId],[Title],[LoginTimestamp],[ServedHouseNo],[ServedBoroCode])" +
                                             "VALUES" +
                                             "(@NOVNumber, @PersonallyServeFlag, @Sex, @Age, @HairColor, @SkinColor, @Height, @Weight, @OtherIdentifying, @LicenseNumber" +
                                               ",@ExpDate, @TypeofLicense, @IssuedNy, @AlternativeService1, @Comments, @SignDate, @UserID, @PublickKeyID" +
                                               ",@DigitalSignature, @ServedCounty, @ServiceTo, @ServedTitle, @ServedTitleOther, @ServedLName, @ServedFName, @ServedMInit" +
                                               ",@ServedSex, @ServedAddress, @ServedCity, @ServedState, @ServedZip, @AlternativeService2, @PremiseLName, @PremiseFName" +
                                               ",@PremiseMInit, @ServedLocation, @CheckSum , @DeviceId, @LicenseTypeDesc, @OfficerName, @AbbrevName, @AgencyId" +
                                               ",@Title, @LoginTimestamp, @ServedHouseNo, @ServedBoroCode)", new
                                               {
                                                   NOVNumber = item.NOVNumber,
                                                   PersonallyServeFlag = item.PersonallyServeFlag,
                                                   Sex = item.Sex,
                                                   Age = item.Age,
                                                   HairColor = item.HairColor,
                                                   SkinColor = item.SkinColor,
                                                   Height = item.Height,
                                                   Weight = item.Weight,
                                                   OtherIdentifying = item.OtherIdentifying,
                                                   LicenseNumber = item.LicenseNumber,
                                                   ExpDate = item.ExpDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.ExpDate,
                                                   TypeofLicense = item.TypeOfLicense,
                                                   IssuedNy = item.IssuedNy,
                                                   AlternativeService1 = item.AlternativeService1,
                                                   Comments = item.Comments,
                                                   SignDate = item.SignDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SignDate,
                                                   UserID = item.UserId,
                                                   PublickKeyID = item.PublickKeyID,
                                                   DigitalSignature = item.DigitalSignature,
                                                   ServedCounty = item.ServedCounty,
                                                   ServiceTo = item.ServiceTo,
                                                   ServedTitle = item.ServedTitle,
                                                   ServedTitleOther = item.ServedTitleOther,
                                                   ServedLName = item.ServedLName,
                                                   ServedFName = item.ServedFName,
                                                   ServedMInit = item.ServedMInit,
                                                   ServedSex = item.ServedSex,
                                                   ServedAddress = item.ServedAddress,
                                                   ServedCity = item.ServedCity,
                                                   ServedState = item.ServedState,
                                                   ServedZip = item.ServedZip,
                                                   AlternativeService2 = item.AlternativeService2,
                                                   PremiseLName = item.PremiseLName,
                                                   PremiseFName = item.PremiseFName,
                                                   PremiseMInit = item.PremiseMInit,
                                                   ServedLocation = item.ServedLocation,
                                                   CheckSum = item.CheckSum,
                                                   DeviceId = item.DeviceId,
                                                   LicenseTypeDesc = item.LicenseTypeDesc,
                                                   OfficerName = item.OfficerName,
                                                   AbbrevName = item.AbbrevName,
                                                   AgencyId = item.AgencyId,
                                                   Title = item.Title,
                                                   LoginTimestamp = item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LoginTimestamp,
                                                   ServedHouseNo = item.ServedHouseNo,
                                                   ServedBoroCode = item.ServedBoroCode

                                               });


                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;
                    }
                }
                catch (Exception exc)
                {
                    var message = String.Format("Failed to insert into BS_AffidavitOfService NOV #{0}: {1}", item.NOVNumber.ToString(), exc.Message);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = String.Format("Duplicate Failed to Upload to BS_AffidavitOfService NOV #{0}:", item.NOVNumber.ToString());
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 1;

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = commands;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    return false;
                }

            }
            return true;
        }

        private async Task<bool> SaveAffidavitOfServiceTran(AffidavitOfServiceTran item, DbConnection conn)
        {
            if (item != null)
            {
                try
                {
                    //AffidavitOfServiceTran
                    var affidavitOfServiceTran_NovNumber = item.NOVNumber;
                    var affidavitOfServiceTran_exists = conn.ExecuteScalar<bool>("select count(1) from BS_AffidavitOfServiceTran where NOVNumber=@NOVNumber", new { NOVNumber = affidavitOfServiceTran_NovNumber });
                    var affidavitOfServiceTran_DupCount = conn.ExecuteAsync("select * from BS_AffidavitOfServiceTran where NOVNumber = @NOVNumber", new { NOVNumber = affidavitOfServiceTran_NovNumber });


                    if (!affidavitOfServiceTran_exists)
                    {
                        if (item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.LoginTimestamp = null;
                        }

                        if (item.ExpDate.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.ExpDate = null;
                        }

                        if (item.SignDate.ToString() == "1/1/0001 12:00:00 AM")
                        {
                            item.SignDate = null;
                        }

                        //Fix NH-963: Store blank values instead of NULL 
                        item.Sex = !String.IsNullOrEmpty(item.Sex) ? "" : item.Sex;
                        item.Age = !String.IsNullOrEmpty(item.Age) ? "" : item.Age;
                        item.HairColor = !String.IsNullOrEmpty(item.HairColor) ? "" : item.HairColor;
                        item.SkinColor = !String.IsNullOrEmpty(item.SkinColor) ? "" : item.SkinColor;
                        item.Height = !String.IsNullOrEmpty(item.Height) ? "" : item.Height;
                        item.Weight = !String.IsNullOrEmpty(item.Weight) ? "" : item.Weight;
                        item.OtherIdentifying = !String.IsNullOrEmpty(item.OtherIdentifying) ? "" : item.OtherIdentifying;
                        item.LicenseNumber = !String.IsNullOrEmpty(item.LicenseNumber) ? "" : item.LicenseNumber;

                        item.TypeOfLicense = !String.IsNullOrEmpty(item.TypeOfLicense) ? "" : item.TypeOfLicense;
                        item.IssuedNy = !String.IsNullOrEmpty(item.IssuedNy) ? "" : item.IssuedNy;
                        item.AlternativeService1 = !String.IsNullOrEmpty(item.AlternativeService1) ? "" : item.AlternativeService1;

                        item.ServiceTo = !String.IsNullOrEmpty(item.ServiceTo) ? "" : item.ServiceTo;
                        item.ServedTitle = !String.IsNullOrEmpty(item.ServedTitle) ? "" : item.ServedTitle;
                        item.ServedTitleOther = !String.IsNullOrEmpty(item.ServedTitleOther) ? "" : item.ServedTitleOther;
                        item.ServedLName = !String.IsNullOrEmpty(item.ServedLName) ? "" : item.ServedLName;
                        item.ServedFName = !String.IsNullOrEmpty(item.ServedFName) ? "" : item.ServedFName;
                        item.ServedMInit = !String.IsNullOrEmpty(item.ServedMInit) ? "" : item.ServedMInit;
                        item.ServedSex = !String.IsNullOrEmpty(item.ServedSex) ? "" : item.ServedSex;
                        item.ServedAddress = !String.IsNullOrEmpty(item.ServedAddress) ? "" : item.ServedAddress;
                        item.ServedCity = !String.IsNullOrEmpty(item.ServedCity) ? "" : item.ServedCity;
                        item.ServedState = !String.IsNullOrEmpty(item.ServedState) ? "" : item.ServedState;
                        item.ServedZip = !String.IsNullOrEmpty(item.ServedZip) ? "" : item.ServedZip;

                        item.AlternativeService2 = !String.IsNullOrEmpty(item.AlternativeService2) ? "" : item.AlternativeService2;
                        item.PremiseLName = !String.IsNullOrEmpty(item.PremiseLName) ? "" : item.PremiseLName;
                        item.PremiseFName = !String.IsNullOrEmpty(item.PremiseFName) ? "" : item.PremiseFName;
                        item.PremiseMInit = !String.IsNullOrEmpty(item.PremiseMInit) ? "" : item.PremiseMInit;
                        item.ServedLocation = !String.IsNullOrEmpty(item.ServedLocation) ? "" : item.ServedLocation;

                        item.LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? "" : item.LicenseTypeDesc;
                        item.ServedHouseNo = !String.IsNullOrEmpty(item.ServedHouseNo) ? "" : item.ServedHouseNo;
                        item.ServedBoroCode = !String.IsNullOrEmpty(item.ServedBoroCode) ? "" : item.ServedBoroCode;

                        await conn.InsertAsync(item);

                        var message = "AffidavitOfServiceTran successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "AffidavitOfServiceTran successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;
                    }
                    else
                    {

                        var message = String.Format("Duplicate: Failed to Upload BS_AffidavitOfServiceTran Nov #: {0} ", item.NOVNumber.ToString());
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = String.Format("Duplicate: Failed to Upload BS_AffidavitOfServiceTran Nov #: {0} ", item.NOVNumber.ToString());
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        return true;
                    }
                }
                catch (Exception exc)
                {
                    var message = String.Format("Failed to insert into BS_AffidavitOfServiceTran NOV #{0}: {1}", item.NOVNumber.ToString(), exc.Message);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = String.Format("Duplicate Failed to Upload to BS_AffidavitOfServiceTran NOV #{0}:", item.NOVNumber.ToString());
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 1;

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = commands;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    return false;
                }

            }
            return true;
        }

        private async Task<bool> SaveCancelNovData(CancelNovData[] items, DbConnection conn)
        {

            bool blnSuccess = false;

            if (items == null)
            {
                blnSuccess = true;

                return blnSuccess;
            }


            //if (item != null)
            foreach (var item in items)
            {
                if (item.FieldName != null)
                {
                    if (item.FieldName.Length > 10)
                        item.FieldName = item.FieldName.Substring(0, 9);
                }

                try
                {
                    //CancelNovData
                    var cancelNovData_systemTimestamp = item.SystemTimestamp;
                    var cancelNovData_exists = conn.ExecuteScalar<bool>("select count(1) from BS_CancelHHNOVData where SystemTimestamp=@SystemTimestamp", new { SystemTimestamp = cancelNovData_systemTimestamp });


                    if (!cancelNovData_exists)
                    {
                        await conn.InsertAsync(item);
                        Log.Info("CancelNovData successfully synced");

                        var message = "CancelNovData successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "CancelNovData successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        blnSuccess = true;
                    }
                    else
                    {

                        var message = String.Format("Duplicate Failed to Upload BS_CancelHHTNovData NOV #:", item.DeviceId.ToString());
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = (int)eEventId.eInvalidState;
                        var log_description = String.Format("Duplicate Failed to Upload BS_CancelHHTNovData NOV #:", item.DeviceId.ToString());
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        blnSuccess = true;
                    }
                }
                catch (Exception exc)
                {
                    var message = String.Format("Failed to insert into BS_CancelHHTNovData Device ID #{0}: {1}", item.DeviceId.ToString(), exc.Message);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 1;

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = commands;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    blnSuccess = false;
                }
            }
            return blnSuccess;
        }

        private async Task<bool> SaveCancelNovInformation(CancelNovInformation item, DbConnection conn)
        {
            if (item != null)
            {

                try
                {
                    //CancelNovInformation
                    //var cancelNovInformation_LoginTimestamp = item.LoginTimestamp;
                    var cancelNovNumber = item.CancelNo;
                    var cancelNovInformation_exists = conn.ExecuteScalar<bool>("select count(1) from BS_CancelHHTNOVInformation where CancelNo=@CancelNo", new { CancelNo = cancelNovNumber });

                    if (!cancelNovInformation_exists)
                    {

                        item.CancelNo = item.CancelNo;
                        item.TicketStatus = !String.IsNullOrEmpty(item.TicketStatus) ? StringTool.Truncate(item.TicketStatus, 2) : item.TicketStatus;
                        //item.IssuedTimestamp = item.IssuedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.IssuedTimestamp;
                        item.SystemTimestamp = item.SystemTimestamp;
                        item.LoginTimestamp = item.LoginTimestamp;
                        item.ReportLevel = !String.IsNullOrEmpty(item.ReportLevel) ? StringTool.Truncate(item.ReportLevel, 4) : item.ReportLevel;
                        //item.IsResp1AddressHit = !String.IsNullOrEmpty(item.IsResp1AddressHit) ? StringTool.Truncate(item.IsResp1AddressHit, 1) : item.IsResp1AddressHit;
                        item.IsResp1AddressHit = false;
                        item.Resp1LastName = !String.IsNullOrEmpty(item.Resp1LastName) ? StringTool.Truncate(item.Resp1LastName, 30) : item.Resp1LastName;
                        item.Resp1FirstName = !String.IsNullOrEmpty(item.Resp1FirstName) ? StringTool.Truncate(item.Resp1FirstName, 25) : item.Resp1FirstName;
                        item.Resp1MiddleInitial = !String.IsNullOrEmpty(item.Resp1MiddleInitial) ? StringTool.Truncate(item.Resp1MiddleInitial, 1) : item.Resp1MiddleInitial;
                        item.Resp1Sex = !String.IsNullOrEmpty(item.Resp1Sex) ? StringTool.Truncate(item.Resp1Sex, 1) : item.Resp1Sex;
                        item.PropertyBBL = !String.IsNullOrEmpty(item.PropertyBBL) ? StringTool.Truncate(item.PropertyBBL, 10) : item.PropertyBBL;
                        item.Resp1DistrictId = !String.IsNullOrEmpty(item.Resp1DistrictId) ? StringTool.Truncate(item.Resp1DistrictId, 3) : item.Resp1DistrictId;
                        //item.Resp1SectionId = !String.IsNullOrEmpty(item.Resp1SectionId) ? StringTool.Truncate(item.Resp1SectionId, 2) : item.Resp1SectionId;
                        item.Resp1SectionId = item.Resp1SectionId;
                        item.Resp1StreetId = item.Resp1StreetId;
                        item.Resp1Address = !String.IsNullOrEmpty(item.Resp1Address) ? StringTool.Truncate(item.Resp1Address, 50) : item.Resp1Address;
                        item.Resp1Address1 = !String.IsNullOrEmpty(item.Resp1Address1) ? StringTool.Truncate(item.Resp1Address1, 25) : item.Resp1Address1;
                        item.Resp1City = !String.IsNullOrEmpty(item.Resp1City) ? StringTool.Truncate(item.Resp1City, 25) : item.Resp1City;
                        item.Resp1State = !String.IsNullOrEmpty(item.Resp1State) ? StringTool.Truncate(item.Resp1State, 2) : item.Resp1State;
                        item.Resp1Zip = !String.IsNullOrEmpty(item.Resp1Zip) ? StringTool.Truncate(item.Resp1Zip, 10) : item.Resp1Zip;
                        item.LicenseNumber = !String.IsNullOrEmpty(item.LicenseNumber) ? StringTool.Truncate(item.LicenseNumber, 45) : item.LicenseNumber;
                        item.LicenseAgency = !String.IsNullOrEmpty(item.LicenseAgency) ? StringTool.Truncate(item.LicenseAgency, 10) : item.LicenseAgency;
                        item.LicenseType = !String.IsNullOrEmpty(item.LicenseType) ? StringTool.Truncate(item.LicenseType, 10) : item.LicenseType;
                        item.IsPlaceAddressHit = !String.IsNullOrEmpty(item.IsPlaceAddressHit) ? StringTool.Truncate(item.IsPlaceAddressHit, 1) : item.IsPlaceAddressHit;
                        item.PlaceLastName = !String.IsNullOrEmpty(item.PlaceLastName) ? StringTool.Truncate(item.PlaceLastName, 25) : item.PlaceLastName;
                        item.PlaceFirstName = !String.IsNullOrEmpty(item.PlaceFirstName) ? StringTool.Truncate(item.PlaceFirstName, 20) : item.PlaceFirstName;
                        item.PlaceMiddleInitial = !String.IsNullOrEmpty(item.PlaceMiddleInitial) ? StringTool.Truncate(item.PlaceMiddleInitial, 1) : item.PlaceMiddleInitial;
                        item.PlaceBBL = !String.IsNullOrEmpty(item.PlaceBBL) ? StringTool.Truncate(item.PlaceBBL, 10) : item.PlaceBBL;
                        item.PlaceDistrictId = !String.IsNullOrEmpty(item.PlaceDistrictId) ? StringTool.Truncate(item.PlaceDistrictId, 3) : item.PlaceDistrictId;
                        item.PlaceSectionId = !String.IsNullOrEmpty(item.PlaceSectionId) ? StringTool.Truncate(item.PlaceSectionId, 2) : item.PlaceSectionId;
                        item.PlaceStreetId = item.PlaceStreetId;
                        item.PlaceAddress1 = !String.IsNullOrEmpty(item.PlaceAddress1) ? StringTool.Truncate(item.PlaceAddress1, 50) : item.PlaceAddress1;
                        item.PlaceAddress2 = !String.IsNullOrEmpty(item.PlaceAddress2) ? StringTool.Truncate(item.PlaceAddress2, 25) : item.PlaceAddress2;
                        item.PlaceAddressDescriptor = !String.IsNullOrEmpty(item.PlaceAddressDescriptor) ? StringTool.Truncate(item.PlaceAddressDescriptor, 1) : item.PlaceAddressDescriptor;
                        item.PlaceSideOfStreet = !String.IsNullOrEmpty(item.PlaceSideOfStreet) ? StringTool.Truncate(item.PlaceSideOfStreet, 4) : item.PlaceSideOfStreet;
                        item.PlaceCross1StreetId = item.PlaceCross1StreetId;
                        item.PlaceCross2StreetId = item.PlaceCross2StreetId;
                        item.MailableAmount = item.MailableAmount;
                        item.MaximumAmount = item.MaximumAmount;
                        item.HearingTimestamp = item.HearingTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.HearingTimestamp;
                        item.IsAppearRequired = !String.IsNullOrEmpty(item.IsAppearRequired) ? StringTool.Truncate(item.IsAppearRequired, 1) : item.IsAppearRequired;
                        item.AlternateService = !String.IsNullOrEmpty(item.AlternateService) ? StringTool.Truncate(item.AlternateService, 1) : item.AlternateService;
                        item.BuildingType = !String.IsNullOrEmpty(item.BuildingType) ? StringTool.Truncate(item.BuildingType, 2) : item.BuildingType;
                        item.IsMultipleOffences = !String.IsNullOrEmpty(item.IsMultipleOffences) ? StringTool.Truncate(item.IsMultipleOffences, 1) : item.IsMultipleOffences;
                        item.DigitalSignature = !String.IsNullOrEmpty(item.DigitalSignature) ? StringTool.Truncate(item.DigitalSignature, 512) : item.DigitalSignature;
                        item.IsSent = !String.IsNullOrEmpty(item.IsSent) ? StringTool.Truncate(item.IsSent, 1) : item.IsSent;
                        item.SentTimestamp = item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SentTimestamp;
                        item.IsReceived = !String.IsNullOrEmpty(item.IsReceived) ? StringTool.Truncate(item.IsReceived, 1) : item.IsReceived;
                        item.ReceivedTimestamp = item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.ReceivedTimestamp;
                        item.ViolationCode = !String.IsNullOrEmpty(item.ViolationCode) ? StringTool.Truncate(item.ViolationCode, 10) : item.ViolationCode;
                        item.HHTIdentifier = !String.IsNullOrEmpty(item.HHTIdentifier) ? StringTool.Truncate(item.HHTIdentifier, 50) : item.HHTIdentifier;
                        item.ViolationScript = !String.IsNullOrEmpty(item.ViolationScript) ? StringTool.Truncate(item.ViolationScript, 512) : item.ViolationScript;
                        item.UserId = !String.IsNullOrEmpty(item.UserId) ? StringTool.Truncate(item.UserId, 16) : item.UserId;
                        item.VoidCancelScreen = !String.IsNullOrEmpty(item.VoidCancelScreen) ? StringTool.Truncate(item.VoidCancelScreen, 6) : item.VoidCancelScreen;
                        item.PlaceBoroCode = !String.IsNullOrEmpty(item.PlaceBoroCode) ? StringTool.Truncate(item.PlaceBoroCode, 1) : item.PlaceBoroCode;
                        item.PlaceHouseNo = !String.IsNullOrEmpty(item.PlaceHouseNo) ? StringTool.Truncate(item.PlaceHouseNo, 16) : item.PlaceHouseNo;
                        item.Resp1BoroCode = !String.IsNullOrEmpty(item.Resp1BoroCode) ? StringTool.Truncate(item.Resp1BoroCode, 1) : item.Resp1BoroCode;
                        item.Resp1HouseNo = !String.IsNullOrEmpty(item.Resp1HouseNo) ? StringTool.Truncate(item.Resp1HouseNo, 16) : item.Resp1HouseNo;
                        item.ViolationGroupId = item.ViolationGroupId;
                        item.ViolationTypeId = !String.IsNullOrEmpty(item.ViolationTypeId) ? StringTool.Truncate(item.ViolationTypeId, 1) : item.ViolationTypeId;
                        item.MDRNumber = item.MDRNumber;
                        item.LicenseExpDate = item.LicenseExpDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LicenseExpDate;
                        item.BusinessName = !String.IsNullOrEmpty(item.BusinessName) ? StringTool.Truncate(item.BusinessName, 70) : item.BusinessName;
                        item.CheckSum = !String.IsNullOrEmpty(item.CheckSum) ? StringTool.Truncate(item.CheckSum, 1) : item.CheckSum;
                        item.FreeAddress = !String.IsNullOrEmpty(item.FreeAddress) ? StringTool.Truncate(item.FreeAddress, 80) : item.FreeAddress;
                        item.DeviceId = !String.IsNullOrEmpty(item.DeviceId) ? StringTool.Truncate(item.DeviceId, 10) : item.DeviceId;
                        //item.PublicKeyId = item.PublicKeyId;
                        item.PrintViolationCode = !String.IsNullOrEmpty(item.PrintViolationCode) ? StringTool.Truncate(item.PrintViolationCode, 10) : item.PrintViolationCode;
                        //item.IsPetitionerCourtAppear = !String.IsNullOrEmpty(item.IsPetitionerCourtAppear) ? StringTool.Truncate(item.IsPetitionerCourtAppear, 1) : item.IsPetitionerCourtAppear;
                        item.LicenseTypeDesc = !String.IsNullOrEmpty(item.LicenseTypeDesc) ? StringTool.Truncate(item.LicenseTypeDesc, 32) : item.LicenseTypeDesc;
                        item.ViolGroupName = !String.IsNullOrEmpty(item.ViolGroupName) ? StringTool.Truncate(item.ViolGroupName, 1) : item.ViolGroupName;
                        item.CodeLawDescription = !String.IsNullOrEmpty(item.CodeLawDescription) ? StringTool.Truncate(item.CodeLawDescription, 100) : item.CodeLawDescription;
                        item.OfficerName = !String.IsNullOrEmpty(item.OfficerName) ? StringTool.Truncate(item.OfficerName, 50) : item.OfficerName;
                        item.LawSection = !String.IsNullOrEmpty(item.LawSection) ? StringTool.Truncate(item.LawSection, 50) : item.LawSection;
                        item.AbbrevName = !String.IsNullOrEmpty(item.AbbrevName) ? StringTool.Truncate(item.AbbrevName, 30) : item.AbbrevName;
                        item.AgencyId = !String.IsNullOrEmpty(item.AgencyId) ? StringTool.Truncate(item.AgencyId, 3) : item.AgencyId;
                        item.Title = !String.IsNullOrEmpty(item.Title) ? StringTool.Truncate(item.Title, 30) : item.Title;


                        await conn.InsertAsync(item);

                        var message = "CancelNovInformation successfully synced";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eSuccess;
                        var log_eventId = (int)eEventId.eInsertStmt;
                        var log_description = "CancelNovInformation successfully synced";
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);

                        string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                        LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                        theEventCommands.Properties["log_group"] = log_group;
                        theEventCommands.Properties["log_type"] = log_type;
                        theEventCommands.Properties["log_eventId"] = log_eventId;
                        theEventCommands.Properties["log_description"] = commands;
                        theEventCommands.Properties["log_data"] = log_data;
                        theEventCommands.Properties["log_deviceId"] = log_deviceId;
                        theEventCommands.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEventCommands);
                        LogTxt.Log(theEventCommands);
                        return true;
                    }
                    else
                    {
                        var message = String.Format("Duplicate Failed to Upload BS_CancelHHTNovInformation NOV #: {0}", item.CancelNo.ToString());
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Duplicate Failed to Upload BS_CancelHHTNovInformation NOV #: {0}", item.CancelNo.ToString());
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                        theEvent.Properties["log_group"] = log_group;
                        theEvent.Properties["log_type"] = log_type;
                        theEvent.Properties["log_eventId"] = log_eventId;
                        theEvent.Properties["log_description"] = log_description;
                        theEvent.Properties["log_data"] = log_data;
                        theEvent.Properties["log_deviceId"] = log_deviceId;
                        theEvent.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent);
                        LogTxt.Log(theEvent);
                        return true;
                    }
                }
                catch (Exception exc)
                {
                    var message = String.Format("Failed to insert into BS_CancelHHTNovInformation NOV #{0}: {1}", item.CancelNo.ToString(), exc.Message);
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_data = "Borosite";
                    var log_deviceId = gDeviceId;
                    var log_recordNum = 1;

                    string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();

                    LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                    theEvent.Properties["log_group"] = log_group;
                    theEvent.Properties["log_type"] = log_type;
                    theEvent.Properties["log_eventId"] = log_eventId;
                    theEvent.Properties["log_description"] = commands;
                    theEvent.Properties["log_data"] = log_data;
                    theEvent.Properties["log_deviceId"] = log_deviceId;
                    theEvent.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent);
                    LogTxt.Log(theEvent);
                    return false;
                }

            }
            return true;
        }
    }

    public class BorositeResponse
    {
        public int RecordCount { get; set; }
        public List<Dictionary<string, object>> BorositeData { get; set; }
    }

    public static class StringTool
    {
        /// <summary>
        /// Returns a substring of N characters counted from the startIndex.
        /// </summary>
        /// <param name="source">The string to be truncated.</param>
        /// <param name="length">The maximum number of characters to be taken from <paramref name="source"/>.</param>
        /// <param name="startIndex">The zero-based index of <paramref name="source"/> from which to count characters for the truncated string. Defaults to 0.</param>
        /// <param name="defaultValue">The string that will be returned in the event that <paramref name="source"/> or the calculated substring is null or whitespace. Defaults to empty string.</param>
        /// <returns>A substring of <param name="source"/> with a maximum length of <param name="length"/> characters counted from <paramref name="startIndex"/>.</returns>
        public static string Truncate(this string source, int length = 1, int startIndex = 0, string defaultValue = "")
        {
            if (string.IsNullOrWhiteSpace(source))
                return defaultValue;

            if (length >= source.Length)
                return source;

            var retVal = source.Substring(startIndex, length);

            return string.IsNullOrWhiteSpace(retVal) ? defaultValue : retVal;
        }
    }

    public static class NovInformationExtensions
    {
        public static NovInformation ToPreparedNovInformation(this NovInformation item) => new NovInformation
        {
            NOVNumber = item.NOVNumber,
            TicketStatus = item.TicketStatus.Truncate(2),
            IssuedTimestamp = item.IssuedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.IssuedTimestamp,
            SystemTimestamp = item.SystemTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SystemTimestamp,
            LoginTimestamp = item.LoginTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LoginTimestamp,
            ReportLevel = item.ReportLevel.Truncate(4),
            IsResp1AddressHit = item.IsResp1AddressHit.Truncate(),
            Resp1LastName = item.Resp1LastName.Truncate(30),
            Resp1FirstName = item.Resp1FirstName.Truncate(25),
            Resp1MiddleInitial = item.Resp1MiddleInitial.Truncate(),
            Resp1Sex = item.Resp1Sex.Truncate(),
            PropertyBBL = item.PropertyBBL.Truncate(10),
            Resp1DistrictId = item.Resp1DistrictId.Truncate(3),
            Resp1SectionId = item.Resp1SectionId.Truncate(2),
            Resp1StreetId = item.Resp1StreetId,
            Resp1Address = item.Resp1Address.Truncate(50),
            Resp1Address1 = item.Resp1Address1.Truncate(25),
            Resp1City = item.Resp1City.Truncate(25),
            Resp1State = item.Resp1State.Truncate(2),
            Resp1Zip = item.Resp1Zip.Truncate(10),
            LicenseNumber = item.LicenseNumber.Truncate(45),
            LicenseAgency = item.LicenseAgency.Truncate(10),
            LicenseType = item.LicenseType.Truncate(10),
            IsPlaceAddressHit = item.IsPlaceAddressHit.Truncate(),
            PlaceLastName = item.PlaceLastName.Truncate(25),
            PlaceFirstName = item.PlaceFirstName.Truncate(20),
            PlaceMiddleInitial = item.PlaceMiddleInitial.Truncate(),
            PlaceBBL = item.PlaceBBL.Truncate(10),
            PlaceDistrictId = item.PlaceDistrictId.Truncate(3),
            PlaceSectionId = item.PlaceSectionId.Truncate(2),
            PlaceStreetId = item.PlaceStreetId,
            PlaceAddress1 = item.PlaceAddress1.Truncate(50),
            PlaceAddress2 = item.PlaceAddress2.Truncate(25),
            PlaceAddressDescriptor = item.PlaceAddressDescriptor.Truncate(),
            PlaceSideOfStreet = item.PlaceSideOfStreet.Truncate(4),
            PlaceCross1StreetId = item.PlaceCross1StreetId,
            PlaceCross2StreetId = item.PlaceCross2StreetId,
            MailableAmount = item.MailableAmount,
            MaximumAmount = item.MaximumAmount,
            HearingTimestamp = item.HearingTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.HearingTimestamp,
            IsAppearRequired = item.IsAppearRequired.Truncate(),
            AlternateService = item.AlternateService.Truncate(),
            BuildingType = item.BuildingType.Truncate(2),
            IsMultipleOffences = item.IsMultipleOffences.Truncate(),
            DigitalSignature = item.DigitalSignature.Truncate(512),
            IsSent = item.IsSent.Truncate(),
            SentTimestamp = item.SentTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.SentTimestamp,
            IsReceived = item.IsReceived.Truncate(),
            ReceivedTimestamp = item.ReceivedTimestamp.ToString() == "1/1/0001 12:00:00 AM" ? null : item.ReceivedTimestamp,
            ViolationCode = item.ViolationCode.Truncate(10),
            HHTIdentifier = item.HHTIdentifier.Truncate(50),
            ViolationScript = item.ViolationScript.Truncate(512),
            UserId = item.UserId.Truncate(16),
            VoidCancelScreen = item.VoidCancelScreen.Truncate(6),
            PlaceBoroCode = item.PlaceBoroCode.Truncate(),
            PlaceHouseNo = item.PlaceHouseNo.Truncate(16),
            Resp1BoroCode = item.Resp1BoroCode.Truncate(),
            Resp1HouseNo = item.Resp1HouseNo.Truncate(16),
            ViolationGroupId = item.ViolationGroupId,
            ViolationTypeId = item.ViolationTypeId.Truncate(),
            MDRNumber = item.MDRNumber,
            LicenseExpDate = item.LicenseExpDate.ToString() == "1/1/0001 12:00:00 AM" ? null : item.LicenseExpDate,
            BusinessName = item.BusinessName.Truncate(70),
            CheckSum = item.CheckSum.Truncate(),
            FreeAddrees = item.FreeAddrees.Truncate(80),
            DeviceId = item.DeviceId.Truncate(10),
            PublicKeyId = item.PublicKeyId,
            PrintViolationCode = item.PrintViolationCode.Truncate(10),
            IsPetitionerCourtAppear = item.IsPetitionerCourtAppear.Truncate(),
            LicenseTypeDesc = item.LicenseTypeDesc.Truncate(32),
            ViolGroupName = item.ViolGroupName.Truncate(),
            CodeLawDescription = item.CodeLawDescription.Truncate(100),
            OfficerName = item.OfficerName.Truncate(50),
            LawSection = item.LawSection.Truncate(50),
            AbbrevName = item.AbbrevName.Truncate(30),
            AgencyId = item.AgencyId.Truncate(3),
            Title = item.Title.Truncate(30)
        };
    }
}
