﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using Dapper;
using Dapper.Contrib.Extensions;
using DSNY.Novas.SyncService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MiniProfiler.Integrations;
using NLog;
using static DSNY.Novas.SyncService.LogHelper;

namespace DSNY.Novas.SyncService.Controllers
{
    [RoutePrefix("api/logs")]
    public class LogsController : ApiController
    {
        private static readonly string _borositeConnectionString;
        private static readonly Dictionary<string, string> _sqlToRun;
        Logger Log = LogManager.GetLogger("databaseLogger");
        Logger LogTxt = LogManager.GetLogger("fileLogger");
        private string gDeviceId="";

        static LogsController()
        {
            _borositeConnectionString = ConfigurationManager.ConnectionStrings["Borosite"].ConnectionString;
        }

        [HttpPost]
        public async Task<IHttpActionResult> SaveLogs(JObject jsonData)
        {
            var jsonTxt = jsonData.ToString();
            ApplicationLogsArray logs = JsonConvert.DeserializeObject<ApplicationLogsArray>(jsonTxt);
            if (logs != null)
            {
                
                foreach (var item in logs.ApplicationLogs)
                {
                    gDeviceId = item.DeviceId;

                    await SaveLog(item);
                }
            }

            return Ok();
        }

        private async Task<bool> SaveLog(ApplicationLog item)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            {
                var factory = new SqlServerDbConnectionFactory(_borositeConnectionString);

                using (var conn = DbConnectionFactoryHelper.New(factory, CustomDbProfiler.Current))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Close();
                            await conn.OpenAsync();
                        }

                        var applicationLogs_logTimestamp = item.LogTimestamp;
                        var applicationLogs_exists = conn.ExecuteScalar<bool>("select count(1) from BS_ApplicationLogs where LogTimestamp=@LogTimestamp", new { LogTimestamp = applicationLogs_logTimestamp });

                        if (!applicationLogs_exists)
                        {
                            await conn.InsertAsync(item);

                            var message = "ApplicationLogs successfully synced";
                            var log_group = item.LogGroup;
                            var log_type = item.Type;
                            var log_eventId = item.EventId;
                            var log_description = "ApplicationLogs successfully synced";
                            var log_data = "Borosite";
                            var log_deviceId = gDeviceId;
                            var log_recordNum = item.RecordNum;

                            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
                            theEvent.Properties["log_group"] = log_group;
                            theEvent.Properties["log_type"] = log_type;
                            theEvent.Properties["log_eventId"] = log_eventId;
                            theEvent.Properties["log_description"] = log_description;
                            theEvent.Properties["log_data"] = log_data;
                            theEvent.Properties["log_deviceId"] = log_deviceId;
                            theEvent.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEvent);
                            LogTxt.Log(theEvent);

                            string commands = CustomDbProfiler.Current.ProfilerContext.GetCommands();
                            LogEventInfo theEventCommands = new LogEventInfo(LogLevel.Info, "", commands);
                            theEventCommands.Properties["log_group"] = log_group;
                            theEventCommands.Properties["log_type"] = log_type;
                            theEventCommands.Properties["log_eventId"] = log_eventId;
                            theEventCommands.Properties["log_description"] = commands;
                            theEventCommands.Properties["log_data"] = log_data;
                            theEventCommands.Properties["log_deviceId"] = log_deviceId;
                            theEventCommands.Properties["log_recordNum"] = log_recordNum;
                            Log.Log(theEventCommands);
                            LogTxt.Log(theEventCommands);
                        }

                        return true;
                    }
                    catch (Exception exc)
                    {
                        var message = "Failed to save application log record";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Failed to save log record: {0}{1]}", exc.Message, exc.StackTrace);
                        var log_data = "Borosite";
                        var log_deviceId = gDeviceId;
                        var log_recordNum = 1;

                        LogEventInfo theEvent_serUser = new LogEventInfo(LogLevel.Warn, "", message);
                        theEvent_serUser.Properties["log_group"] = log_group;
                        theEvent_serUser.Properties["log_type"] = log_type;
                        theEvent_serUser.Properties["log_eventId"] = log_eventId;
                        theEvent_serUser.Properties["log_description"] = log_description;
                        theEvent_serUser.Properties["log_data"] = log_data;
                        theEvent_serUser.Properties["log_deviceId"] = log_deviceId;
                        theEvent_serUser.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent_serUser);
                        LogTxt.Log(theEvent_serUser);
                    }
                }
            }

            return true;
        }
    }
}
