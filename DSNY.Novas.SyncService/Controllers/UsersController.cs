﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using Dapper;
using Dapper.Contrib.Extensions;
using DSNY.Novas.SyncService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MiniProfiler.Integrations;
using NLog;
using static DSNY.Novas.SyncService.LogHelper;

namespace DSNY.Novas.SyncService.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private static readonly string _borositeConnectionString;
        private static readonly Dictionary<string, string> _sqlToRun;
        Logger Log = LogManager.GetLogger("databaseLogger");
        Logger LogTxt = LogManager.GetLogger("fileLogger");

        static UsersController ()
        {
            _borositeConnectionString = ConfigurationManager.ConnectionStrings["Borosite"].ConnectionString;
        }

        [HttpPost]
        public async Task<IHttpActionResult> SaveUsers(JObject jsonData)
        {
            if (jsonData != null)
            {
                try
                {
                    var jsonTxt = jsonData.ToString();
                    NovasUserArray users = JsonConvert.DeserializeObject<NovasUserArray>(jsonTxt);

                    // since signatrue is gettng stored in novamaster so no need to update NovasUserMaster
                    //foreach(var item in users.NovasUserMaster)
                    //{
                    //    var isSaved = await SaveUserMasterRecord(item);
                    //}


                    foreach (NovasUser user in users.NovasUser)
                    {
                        var userSaved = await SaveUserRecord(user);

                        if (!userSaved)
                            return InternalServerError(new Exception("Failed to save user:" + user.UserId));
                    }
                    
                } catch (Exception exc)
                {
                    var message = "Failed to save users";
                    var log_group = (char)eGroups.eSQL;
                    var log_type = (char)eTypes.eWarning;
                    var log_eventId = 0;
                    var log_description = "Failed to deserialize NOVAS user records";
                    var log_data = "Borosite";
                    var log_deviceId = "";
                    var log_recordNum = 1;

                    LogEventInfo theEvent_serUser = new LogEventInfo(LogLevel.Warn, "", message);
                    theEvent_serUser.Properties["log_group"] = log_group;
                    theEvent_serUser.Properties["log_type"] = log_type;
                    theEvent_serUser.Properties["log_eventId"] = log_eventId;
                    theEvent_serUser.Properties["log_description"] = log_description;
                    theEvent_serUser.Properties["log_data"] = log_data;
                    theEvent_serUser.Properties["log_deviceId"] = log_deviceId;
                    theEvent_serUser.Properties["log_recordNum"] = log_recordNum;
                    Log.Log(theEvent_serUser);
                    LogTxt.Log(theEvent_serUser);
                }   
            }
           
            return Ok();
        }

        private async Task<bool> SaveUserRecord(NovasUser user)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            {
                var factory = new SqlServerDbConnectionFactory(_borositeConnectionString);

                using (var conn = DbConnectionFactoryHelper.New(factory, CustomDbProfiler.Current))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Close();
                            await conn.OpenAsync();
                        }

                        var userExists = conn.ExecuteScalar<bool>("select count(1) from dbo.NovasUser where UserId=@UserId", new { UserId = user.UserId });

                        if (userExists)
                            await conn.UpdateAsync(user);
                        else
                            await conn.InsertAsync(user);

                        scope.Complete();

                        return true;
                    }
                    catch (Exception exc)
                    {
                        var message = "Failed to save user record";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Failed to execute [sync].[UpdateNovasUser]: {0}{1}", exc.Message, exc.StackTrace);
                        var log_data = "Borosite";
                        var log_deviceId = "";
                        var log_recordNum = 1;

                        LogEventInfo theEvent_serUser = new LogEventInfo(LogLevel.Warn, "", message);
                        theEvent_serUser.Properties["log_group"] = log_group;
                        theEvent_serUser.Properties["log_type"] = log_type;
                        theEvent_serUser.Properties["log_eventId"] = log_eventId;
                        theEvent_serUser.Properties["log_description"] = log_description;
                        theEvent_serUser.Properties["log_data"] = log_data;
                        theEvent_serUser.Properties["log_deviceId"] = log_deviceId;
                        theEvent_serUser.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent_serUser);
                        LogTxt.Log(theEvent_serUser);

                        return false;
                    }
                }
            }

            return true;
        }

        private async Task<bool> SaveUserMasterRecord(NovasUserMaster user)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)) // enables transaction flow across thread continuations
            {
                var factory = new SqlServerDbConnectionFactory(_borositeConnectionString);

                using (var conn = DbConnectionFactoryHelper.New(factory, CustomDbProfiler.Current))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Close();
                            await conn.OpenAsync();
                        }

                        var userExists2 = conn.ExecuteScalar<int>("update dbo.vwUsers set  SignatureBitmap=@SignatureBitmap where UserId=@UserId", new { UserId = user.UserId, SignatureBitmap = user.SignatureBitmap });


                        //if (userExists)
                        //    await conn.UpdateAsync(user);
                        //else
                        //    await conn.InsertAsync(user);

                        scope.Complete();

                        return true;
                    }
                    catch (Exception exc)
                    {
                        var message = "Failed to save user record";
                        var log_group = (char)eGroups.eSQL;
                        var log_type = (char)eTypes.eWarning;
                        var log_eventId = 0;
                        var log_description = String.Format("Failed to execute [sync].[UpdateNovasUser]: {0}{1}", exc.Message, exc.StackTrace);
                        var log_data = "Borosite";
                        var log_deviceId = "";
                        var log_recordNum = 1;

                        LogEventInfo theEvent_serUser = new LogEventInfo(LogLevel.Warn, "", message);
                        theEvent_serUser.Properties["log_group"] = log_group;
                        theEvent_serUser.Properties["log_type"] = log_type;
                        theEvent_serUser.Properties["log_eventId"] = log_eventId;
                        theEvent_serUser.Properties["log_description"] = log_description;
                        theEvent_serUser.Properties["log_data"] = log_data;
                        theEvent_serUser.Properties["log_deviceId"] = log_deviceId;
                        theEvent_serUser.Properties["log_recordNum"] = log_recordNum;
                        Log.Log(theEvent_serUser);
                        LogTxt.Log(theEvent_serUser);

                        return false;
                    }
                }
            }

            return true;
        }
    }
}
