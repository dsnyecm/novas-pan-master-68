﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace DSNY.Novas.SyncService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Logger logger = LogManager.GetLogger("databaseLogger");
            Logger logTxt = LogManager.GetLogger("fileLogger");

            var message = "DSNY.Novas.SyncService Started";
            var log_group = "";
            var log_type = "";
            var log_eventId = 0;
            var log_description = "DSNY.Novas.SyncService Started";
            var log_data = "Borosite";
            var log_deviceId = "";
            var log_recordNum = 1;
            
            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
            theEvent.Properties["log_group"] = log_group;
            theEvent.Properties["log_type"] = log_type;
            theEvent.Properties["log_eventId"] = log_eventId;
            theEvent.Properties["log_description"] = log_description;
            theEvent.Properties["log_data"] = log_data;
            theEvent.Properties["log_deviceId"] = log_deviceId;
            theEvent.Properties["log_recordNum"] = log_recordNum;
            logger.Log(theEvent);
            logTxt.Log(theEvent);

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
        
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}
