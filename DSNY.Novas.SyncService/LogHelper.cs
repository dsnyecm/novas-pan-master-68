﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace DSNY.Novas.SyncService
{
    public class LogHelper
    {
        static Logger Log = LogManager.GetLogger("databaseLogger");
        static Logger LogTxt = LogManager.GetLogger("fileLogger");
        public enum eGroups
        {
            eSQL = 'Q',
            eApplication = 'A',
            eSystem = 'Y',
            eSecurity = 'E'
        };
        public enum eTypes
        {
            eFailure = 'F',
            eSuccess = 'S',
            eInfo = 'I',
            eWarning = 'W',
            eError = 'E'
        };

        public enum enGetStatus
        {
            eGetSuccess = 0,
            eGetEOF = 1,
            eGetError = 2,
            eGetCorrupt = 3
        };

        public enum eEventId
        {
            // RDA
            eUploadRecord = 1000,
            ePullTable = 1001,
            eDeleteRecord = 1002,

            // DBA
            eCorruptDB = 2000,
            eCreateDB = 2001,
            eCreateIndex = 2002,
            eDropStmt = 2003,
            eSelectStmt = 2004,
            eInsertStmt = 2005,
            eDeleteStmt = 2006,
            eUpdateStmt = 2007,
            eBeginTransStmt = 2008,
            eCommitTransStmt = 2009,
            eRollBackTransStmt = 2010,
            eConnectDB = 20050,

            // File I/O
            eCreateFile = 3000,
            eDeleteFile = 3001,
            eReadFile = 3002,
            eWriteFile = 3003,
            eOpenFile = 3004,

            // Application
            eAppStarted = 4000,
            eDataCalc = 4001,
            eHardwareInit = 4002,
            eMemoryAlloc = 4003,
            eApplicationInit = 4004,
            eInvalidState = 4005,
            eTicketPrint = 4006,

            // Time
            eSyncTime = 5000,

            // Security
            eSigValidate = 6000,
            eSigTempLoad = 6001,
            eMemoryErase = 6002,
            eUserLogin = 6003,
            eCrypto = 6004,

            //Hardware
            eMeterComm = 7000,
            eCoreComm = 10000

        };

        public static void LogEventToDB(string message, string log_group, string log_type, int log_eventId, string log_description, string log_data, string log_deviceId, int log_recordNum)
       {
            LogEventInfo theEvent = new LogEventInfo(LogLevel.Info, "", message);
            theEvent.Properties["log_group"] = log_group;
            theEvent.Properties["log_type"] = log_type;
            theEvent.Properties["log_eventId"] = log_eventId;
            theEvent.Properties["log_description"] = log_description;
            theEvent.Properties["log_data"] = log_data;
            theEvent.Properties["log_deviceId"] = log_deviceId;
            theEvent.Properties["log_recordNum"] = log_recordNum;
            Log.Log(theEvent);
            LogTxt.Log(theEvent);
        }
    }
}