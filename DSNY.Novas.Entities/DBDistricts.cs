﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBDistricts")]
    public class DBDistricts
    {
        [PrimaryKey, AutoIncrement]
        public int DBDistrictsKey { get; set; }
        [NotNull]
        [Indexed]
        public string BoroCode { get; set; }
        [NotNull]
        public string DistrictId { get; set; }
        [NotNull]
        public string Description { get;set; }
        [NotNull]
        public string Name { get; set; }
    }
}