﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBPrinters")]
    public class DBPrinters
    {
        [PrimaryKey, AutoIncrement]
        public int DBPrintersKey { get; set; }
        [NotNull]
        public string DeviceId { get; set; }
        [NotNull]
        public string DeviceName { get; set; }
        public string IpAddress { get; set; }
        [NotNull]
        public string SerialNumber { get; set; }
        [NotNull]
        public string Status { get; set; }
        public string Type { get; set; }
        public string IsMobileDevice { get; set; }
        //public string MacAddress { get; set; }
    }
}