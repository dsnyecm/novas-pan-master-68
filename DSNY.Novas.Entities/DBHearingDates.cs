﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBHearingDates")]
    public class DBHearingDates
    {
        [PrimaryKey, AutoIncrement]
        public int DBHearingDatesKey { get; set; }
        [NotNull]
        public string BoroCode { get; set; }
        [NotNull]
        public DateTime CurrentDate { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}