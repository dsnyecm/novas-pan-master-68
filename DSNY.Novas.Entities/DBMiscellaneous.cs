﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBMiscellaneous")]
    public class DBMiscellaneous
    {
        [PrimaryKey, AutoIncrement]
        public int DBMiscellaneousKey { get; set; }
        [NotNull]
        public string TableName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
