﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("Local_DBInfo")]
    public class Local_DBInfo
    {
        [PrimaryKey]
        [NotNull]
        public string TableName { get; set; }
        public DateTime ActivityTimestamp { get; set; }
    }
}