﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;


namespace DSNY.Novas.Entities
{
    [Table("DBLaterSvcData")]
    public class DBLaterSvcData
    {
        [NotNull]
        [Indexed]
        public Int64 NovNumber { get; set; }
        [NotNull]
        public string FieldName { get; set; }
        [NotNull]
        public string Data { get; set; }

        public string DLDeviceId { get; set; }
    }
}
