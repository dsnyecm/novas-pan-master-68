﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationTypes")]
    public class DBViolationTypes
    {
        [PrimaryKey, AutoIncrement]
        public int ViolationTypesKey { get; set; }
        [Indexed]
        public int ViolationGroupId { get; set; }
        public string ViolationTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public string IsMobileRecord { get; set; }
    }
}