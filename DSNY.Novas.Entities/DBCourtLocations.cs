﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBCourtLocations")]
    public class DBCourtLocations
    {
        [PrimaryKey, AutoIncrement]
        public int DBCourtLocationsKey { get; set; }
        [NotNull]
        public string ViolationCode { get; set; }
        [NotNull]
        public string PlaceBoroCode { get; set; }
        [NotNull]
        public string CourtBoroCode { get; set; }
    }
}