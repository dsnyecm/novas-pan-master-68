﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationLawCodes")]
    public class DBViolationLawCodes
    {
        [PrimaryKey, AutoIncrement]
        public int ViolationLawCodesKey { get; set; }
        [Indexed]
        public int LawId { get; set; }
        public string LawSection { get; set; }
        public DateTime ChangeEffectDate { get; set; }
        public DateTime EndEffDate { get; set; }
        public string IsMobileRecord { get; set; }
        public string LawDescription { get; set; }
    }
}