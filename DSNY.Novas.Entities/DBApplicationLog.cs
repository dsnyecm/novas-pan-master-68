﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBApplicationLog")]
    public class DBApplicationLog
    {
        [PrimaryKey, AutoIncrement]
        public int DBApplicationLogKey { get; set; }
        public DateTime LogTimestamp { get; set; }
        public int RecordNum { get; set; }
        public string ComputerName { get; set; }
        public DateTime ProcessTimestamp { get; set; }
        public string LogGroup { get; set; }
        public string Type { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }
        public int Line { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Data { get; set; }
        public DateTime AddTimestamp { get; set; }
        public string DeviceId { get; set; }
    }
}