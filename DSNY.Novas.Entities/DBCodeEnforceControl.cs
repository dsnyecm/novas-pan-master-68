﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBCodeEnforceControl")]
    public class DBCodeEnforceControl
    {
        [PrimaryKey, AutoIncrement]
        public int DBCodeEnforceControlKey { get; set; }
        [NotNull]
        public Int16 ControlId { get; set; }
        [NotNull]
        public string IsComplexPassword { get; set; }
        public Int16? PasswordMinLength { get; set; }
        public Int16? PasswordAttempts { get; set; }
        public Int16? SignatureAttempts { get; set; }
        [NotNull]
        public Int16 PasswordResetMinutes { get; set; }
        [NotNull]
        public Int16 PasswordExpirationDays { get; set; }
        [NotNull]
        public Int16 InactivityTimeoutMinutes { get; set; }
        public string TicketNumberChecksum { get; set; }
        public int? TicketAllocationLow { get; set; }
        public string LocalSite { get; set; }

    }
}