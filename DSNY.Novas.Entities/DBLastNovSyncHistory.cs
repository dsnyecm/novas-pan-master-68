﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace DSNY.Novas.Entities
{
    public class DBLastNovSyncHistory
    {
        [NotNull]
        public DateTime SyncTimestamp { get; set; }
        [NotNull]
        public Int64 LastNovNumber { get; set; }
    }
}
