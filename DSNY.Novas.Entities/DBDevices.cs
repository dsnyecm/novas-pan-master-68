﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBDevices")]
    public class DBDevices
    {
        [AutoIncrement, PrimaryKey]
        public int DeviceKey { get; set; }
        [Indexed]
        [NotNull]
        public string DeviceId { get; set; }
        [Indexed]
        [NotNull]
        public string DeviceName { get; set; }
        public string IPAddress { get; set; }
        public string SerialNumber { get; set; }
        public string Status { get; set; }
        public string DeviceType { get; set; }
        public string IsMobileDevice { get; set; }
        public string OwnerLocation { get; set; }
        public string CurrentLocation { get; set; }
        public string Salt { get; set; }
        public string SerializedPrivateKey { get; set; }
        public string Certificate { get; set; }
        public string MacAddress { get; set; }
    }
}
