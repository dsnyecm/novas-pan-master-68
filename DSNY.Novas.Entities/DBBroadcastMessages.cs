﻿using SQLite;
using System;

namespace DSNY.Novas.Entities
{
    [Table("DBBroadcastMessages")]
    public class DBBroadcastMessages
    {
        [PrimaryKey, AutoIncrement]
        public int DBBroadcastMessagesKey { get; set; }
        [NotNull]
        public int BroadcastId { get; set; }
        [NotNull]
        public DateTime? Broadcastdate { get; set; }
        [NotNull]
        public string Location { get; set; }
        [NotNull]
        public string Message { get; set; }
        public string IsMobileRecord { get; set; }
    }
}