﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBNovData")]
    public class DBNovData
    {
        [PrimaryKey, AutoIncrement]
        public int DBNovDataKey { get; set; }
        [NotNull]
        [Indexed]
        public Int64 NovNumber { get; set; }
        [NotNull]
        public string FieldName { get; set; }
        [NotNull]
        public string Data { get; set; }
    }
}