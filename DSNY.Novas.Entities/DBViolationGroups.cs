﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationGroups")]
    public class DBViolationGroups
    {
        [PrimaryKey, AutoIncrement]
        public int ViolationGroupsKey {get; set; }
        [NotNull]
        [Indexed]
        public int ViolationGroupId { get; set; }
        [NotNull]
        public string GroupName { get; set; }
        public string TypeName { get; set; }
        public int Sequence { get; set; }
    }
}