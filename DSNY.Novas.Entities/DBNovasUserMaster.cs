﻿using System;
using SQLite;
using SQLitePCL;

namespace DSNY.Novas.Entities
{
    public class DBNovasUserMaster
    {
        [PrimaryKey, AutoIncrement]
        public int DBNovasUserMasterKey { get; set; }
        [Indexed]
        public string UserId { get; set; }
        public string AgencyId { get; set; } 
        public string AbbrevName { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string SaltValue { get; set; }
        public int PublicKeyID { get; set; }
        public DateTime TemplateTimestamp { get; set; }
        public DateTime PasswordTimestamp { get; set; }
        public DateTime LastActivityTimestamp { get; set; }
        public DateTime CreateTimestamp { get; set; }
        public DateTime ChangeTimestamp { get; set; }
        public Int16 LoginAttempts { get; set; }
        public DateTime LastLoginAttempt { get; set; }
        public string IsSupervisor { get; set; }
        public string IsPwordChangeRequired { get; set; }
        public string IsDisabled { get; set; }
        public string HHPwCh { get; set; }
        public string SigChg { get; set; }
        public string KeyChg { get; set; }
        public string UsrAct { get; set; }
        public int BoroId { get; set; }
        public string SiteId { get; set; }
        public int SignatureAttempts { get; set; }
        public DateTime SignatureLastAttempt { get; set; }
        public byte[] BiometricSignatureTemplate { get; set; }
        public byte[] SignatureBitmap { get; set; }
        public byte[] PrivateKey { get; set; }
        public byte[] PubKey { get; set; }
    }
}