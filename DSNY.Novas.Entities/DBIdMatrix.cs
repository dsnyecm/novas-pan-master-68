﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBIdMatrix")]
    public class DBIdMatrix
    {
        [PrimaryKey, AutoIncrement]
        public int DBIdMatrixKey { get; set; }
        [NotNull]
        public string IdType { get; set; }
        [NotNull]
        public string IdDesc { get; set; }
        public string IsActionOff { get; set; }
        public string IsCommOff { get; set; }
        public string IsMultiOff { get; set; }
        public string IsPersonalSvc { get; set; }
        public string AskIdNo { get; set; }
        public string AskExpDate { get; set; }
        public string AskIssuedBy { get; set; }
        public string IssuedBy { get; set; }
    }
}