﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBStreetCodeMaster")]
    public class DBStreetCodeMaster
    {
        [PrimaryKey, AutoIncrement]
        public int DBStreetCodeMasterKey { get; set; }
        [NotNull]
        [Indexed]
        public string BoroCode { get; set; }
        [NotNull]
        [Indexed]
        public int StreetCode { get; set; }
        [NotNull]
        [Indexed]
        public string StreetName { get; set; }
        public string Lgc { get; set; }
        public string Snc { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EndEffDate { get; set; }
    }
}
