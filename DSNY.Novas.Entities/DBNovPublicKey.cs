﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBNovPublicKey")]
    public class DBNovPublicKey
    {
        [PrimaryKey]
        public Int64 NovNumber { get; set; }

        [NotNull]
        public string Certificate { get; set; }
    }
}
