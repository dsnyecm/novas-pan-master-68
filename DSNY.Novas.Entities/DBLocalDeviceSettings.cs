﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBLocalDeviceSettings")]
    public class DBLocalDeviceSettings
    {
        [PrimaryKey, AutoIncrement]
        public int DBLocalDeviceSettingKey { get; set; }
        [Indexed]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
