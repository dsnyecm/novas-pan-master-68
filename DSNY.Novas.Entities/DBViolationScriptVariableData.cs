﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationScriptVariableData")]
    public class DBViolationScriptVariableData
    {
        [PrimaryKey, AutoIncrement]
        public int ViolationScriptVariableDataKey { get; set; }
        [NotNull]
        public string FieldName { get; set; }
        [NotNull]
        public string Data { get; set; }
        public int? Sequence { get; set; }
    }
}