﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBDeviceTicketRanges")]
    public class DBDeviceTicketRanges
    {
        [PrimaryKey, AutoIncrement]
        public int DeviceTicketRangesKey { get; set; }
        [NotNull]
        [Indexed]
        public string DeviceId { get; set; }
        [NotNull]
        public int NovRangeId { get; set; }
        public string RangeStatus { get; set; }
        public Int64 TicketStartNumber { get;set; }
        public Int64 TicketEndNumber { get; set; }
        public Int64 LastTicketNumber { get; set; }
    }
}