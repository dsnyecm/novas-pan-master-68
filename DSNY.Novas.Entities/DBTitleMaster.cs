﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBTitleMaster")]
    public class DBTitleMaster
    {
        [PrimaryKey, AutoIncrement]
        public int DBTitleMasterKey { get; set; }
        [NotNull]
        public int TitleId { get; set; }
        [NotNull]
        public string TitleName { get; set; }
    }
}
