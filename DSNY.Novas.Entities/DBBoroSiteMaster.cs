﻿using SQLite;


namespace DSNY.Novas.Entities
{
    [Table("DBBoroSiteMaster")]
    public class DBBoroSiteMaster
    {

        public string BoroCode { get; set; }
        public string BoroSiteCode { get; set; }
        public string BoroSiteName { get; set; }
        public string SiteType { get; set; }
        public string ReportLevel { get; set; }
        public int BoroId => string.IsNullOrWhiteSpace(BoroCode) ? 0 : int.Parse(BoroCode);
    }
}
