﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBRoutingTime")]
    public class DBRoutingTime
    {
        [PrimaryKey, AutoIncrement]
        public int DBRoutingTimeKey { get; set; }
        [NotNull]
        public string BoroCode { get; set; }
        [NotNull]
        public string DistrictId { get; set; }
        [NotNull]
        public string SectionId { get; set; }
        public DateTime EffectDate { get; set; }
        public string RoutingTimeAm { get; set; }
        public string RoutingTimePm { get; set; }
        public string ViolationType { get; set; }
    }
}