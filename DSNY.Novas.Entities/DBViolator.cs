﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolator")]
    public class DBViolator
    {
        [PrimaryKey, AutoIncrement]
        public int DBViolatorKey { get; set; }
        [NotNull]
        [Indexed]
        public string BoroCode { get; set; }
        [NotNull]
        [Indexed]
        public int StreetCode { get; set; }
        [NotNull]
        [Indexed]
        public string LowHouseNo { get; set; }
        [NotNull]
        public DateTime BatchDate { get; set; }
        [NotNull]
        public string BusinessName { get; set; }
        [NotNull]
        [Indexed]
        public string ViolationCode { get; set; }
        public string NextViolationCode { get; set; }
        public float MinFine { get; set; }
        public float MaxFine { get; set; }
        public Int16 NoOfTimes { get; set; }
        public string DisplaySign { get; set; }
    }
}