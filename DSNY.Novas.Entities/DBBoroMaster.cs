﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBBoroMaster")]
    public class DBBoroMaster
    {
        [PrimaryKey, AutoIncrement]
        public int DBBoroMasterKey { get; set; }
        [NotNull]
        public string BoroId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}