﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBPropertyDetails")]
    public class DBPropertyDetails
    {
        [PrimaryKey, AutoIncrement]
        public int DBPropertyDetailsKey { get; set; }
        [NotNull]
        [Indexed]
        public Int64 Bbl { get; set; }
        [NotNull]
        [Indexed]
        public int StreetCode { get; set; }
        [Indexed]
        public int? LowHouseNo { get; set; }
        [NotNull]
        [Indexed]
        public string LowHouseKey { get; set; }
        public string LowHouseSfx { get; set; }
        public string LowHouseInd { get; set; }
        public int? HighHouseNo { get; set; }
        public string HighHouseSfx { get; set; }
        public string HighHouseInd { get; set; }
        public int? MdrNo { get; set; }
        public Int16? DistrictId { get; set; }
        public Int16? SectionId { get; set; }
        public string Pop { get; set; }
        public string ContinuousParity { get; set; }
        public string VacantLot { get; set; }
        public string OwnStatus { get; set; }
        public string FirstName { get; set; }
        public int? ZipCode { get; set; }
    }
}