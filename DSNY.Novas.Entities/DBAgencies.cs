﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBAgencies")]
    public class DBAgencies
    {
        [PrimaryKey, AutoIncrement]
        public int DBAgenciesKey { get; set; }
        [NotNull]
        public int AgencyId { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public string Description { get; set; }
        public string IsMobileRecord { get; set; }
        [NotNull]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}