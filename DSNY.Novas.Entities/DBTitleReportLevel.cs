﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBTitleReportLevel")]
    public class DBTitleReportLevel
    {
        [PrimaryKey, AutoIncrement]
        public int TitleReportLevelKey { get; set; }
        [NotNull]
        public Int16 Rank { get; set; }
        public string ReportLevel { get; set; }
        public string Title { get; set; }
    }
}