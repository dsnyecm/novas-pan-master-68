﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBFirstDescriptors")]
    public class DBFirstDescriptors
    {
        [PrimaryKey, AutoIncrement]
        public int DBFirstDescriptorsKey { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        [Indexed]
        public string ViolationTypeDesc { get; set; }
        public string GroupName { get; set; }
        public int Sort { get; set; }
        public int GoElement { get; set; }

        [Indexed]
        public string ViolationTypes { get; set; }
    }
}
