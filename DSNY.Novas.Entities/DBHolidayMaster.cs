﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBHolidayMaster")]
    public class DBHolidayMaster
    {
        [PrimaryKey, AutoIncrement]
        public int DBHolidayMasterKey { get; set; }
        [NotNull]
        public string BoroCode { get; set; }
        [NotNull]
        public DateTime EffectiveDate { get; set; }
        public DateTime EndEffDate { get; set; }
        public string Comments { get; set; }
    }
}