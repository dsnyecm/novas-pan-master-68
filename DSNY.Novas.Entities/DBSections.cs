﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBSections")]
    public class DBSections
    {
        [PrimaryKey, AutoIncrement]
        public int DBSectionsKey { get; set; }
        [NotNull]
        [Indexed]
        public string DistrictId { get; set; }
        [NotNull]
        public string SectionId { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public string Description { get; set; }
        public int? Sequence { get; set; }
    }
}