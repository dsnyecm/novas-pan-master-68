﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Entities
{
    [Table("DBCancelNovData")]
    public class DBCancelNovData
    {
        [PrimaryKey]
        public string DeviceId { get; set; }
        public DateTime SystemTimestamp { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
    }
}
