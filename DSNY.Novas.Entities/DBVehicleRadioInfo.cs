﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBVehicleRadioInfo")]
    public class DBVehicleRadioInfo
    {
        [PrimaryKey, AutoIncrement]
        public int VehicleRadioInfoKey { get; set; }
        [Indexed]
        public string UserId { get; set; }
        public DateTime LoginTimestamp { get; set; }
        [NotNull]
        public Int16 VehicleRadioId { get; set; }
        public string VehicleId { get; set; }
        public string RadioId { get; set; }
        public int StartMileage { get; set; }
        [Indexed]
        public int EndMileage { get; set; }
    }
}