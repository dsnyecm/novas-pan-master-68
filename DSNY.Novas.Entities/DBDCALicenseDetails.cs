﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Entities
{
    [Table("DBDCALicenseDetails")]
    public class DBDCALicenseDetails
    {

        [PrimaryKey, AutoIncrement]
        public int DBDCALicenseDetailsKey { get; set; }
        [NotNull]
        [Indexed]
        public string License { get; set; }
        public string BusinessName { get; set; }
        public string ExpirationDate { get; set; }
        public string TradeName { get; set; }
        public string PremiseBuildingNumber { get; set; }
        public string PremiseStreetName { get; set; }
        public string PremiseLocation { get; set; }
        public string PremiseCity { get; set; }
        public string PremiseState { get; set; }
        public string PremiseZipCode { get; set; }
        public int PremiseCouncilDist { get; set; }
        public string PremiseHouseNumber { get; set; }
        public string Bbl { get; set; }
        public int PremiseCommunityDist { get; set; }
        public string MailingBuildingNumber { get; set; }
        public string MailingStreetName { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }

        public string PolicePrecinct{ get; set; }
        //newly added to Accmodate DCA new fields
        public string ApplicationNumber { get; set; }
        public string RenwalNumber{ get; set; }
        public string AmendementNumber { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        

    }
}
