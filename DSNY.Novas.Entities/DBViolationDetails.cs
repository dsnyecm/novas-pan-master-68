﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationDetails")]
    public class DBViolationDetails
    {
        [PrimaryKey, AutoIncrement]
        public int DBViolationDetailsKey { get; set; }
        [NotNull]
        public string ViolationCode { get; set; }
        [NotNull]
        public string HHTIdentifier { get; set; }
        [NotNull]
        [Indexed]
        public int ViolationGroupId { get; set; }
        public string CodeLaw { get; set; }
        public int LawId { get; set; }
        [NotNull]
        public string InternalShortDescription { get; set; }
        public int Sequence { get; set; }
        public decimal MailableAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        public DateTime FromEnforceTime { get; set; }
        public DateTime ToEnforceTime { get; set; }
        [NotNull]
        [Indexed]
        public string IsMobileRecord { get; set; }
        [NotNull]
        public DateTime ChangeEffDate { get; set; }
        public DateTime EndEffDate { get; set; }
        public string CodeLawId { get; set; }
        public string RoutingFlag { get; set; }
        public string AutoSend { get; set; }
        [NotNull]
        public string CodeScript { get; set; }
    }
}