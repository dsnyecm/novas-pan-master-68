﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBLookup")]
    public class DBLookup
    {
        [PrimaryKey, AutoIncrement]
        public int DBMiscellaneousKey { get; set; }
        [Indexed]
        public string TableName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}