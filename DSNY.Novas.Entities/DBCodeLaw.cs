﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBCodeLaw")]
    public class DBCodeLaw
    {
        [PrimaryKey, AutoIncrement]
        public int DBCodeLawKey { get; set; }
        [NotNull]
        public string CodeLawId { get; set; }
        public string CodeLawDescription { get; set; }
        public string Comments { get; set; }
    }
}