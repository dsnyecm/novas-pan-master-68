﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBAddressSuffixMaster")]
    public class DBAddressSuffixMaster
    {
        [PrimaryKey, AutoIncrement]
        public int AddressSuffixMasterKey { get; set; }
        [NotNull]
        public string SuffixId { get; set; }
        public string SuffixName { get; set; }
    }
}