﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBHearingDateTime")]
    public class DBHearingDateTime
    {
        [PrimaryKey, AutoIncrement]
        public int DBHearingDateTimeKey { get; set; }
        [NotNull]
        public int AgencyId { get; set; }
        [NotNull]
        public DateTime HearingFromDate { get; set; }
        [NotNull]
        public DateTime HearingToDate { get; set; }
        [NotNull]
        public DateTime HearingTime { get; set; }
    }
}