﻿using System;
using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBNovasUser")]
    public class DBNovasUser
    {
        [PrimaryKey, AutoIncrement]
        public int DBNovasUserId { get; set; }

        [Indexed, NotNull]
        public string UserId { get; set; }

        [NotNull]
        public string Salt { get; set; }

        [NotNull]
        public string UserHash { get; set; }
        public string NfcUid { get; set; }

        public string Pin { get; set; }
        public byte[] SignatureBitmap { get; set; }

    }
}