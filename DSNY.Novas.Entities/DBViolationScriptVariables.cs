﻿using SQLite;

namespace DSNY.Novas.Entities
{
    [Table("DBViolationScriptVariables")]
    public class DBViolationScriptVariables
    {
        [PrimaryKey, AutoIncrement]
        public int ViolationScriptVariablesKey { get; set; }
        [NotNull]
        public string FieldName { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Mask { get; set; }
        public string MapField { get; set; }
        [NotNull]
        public string IsMultiplesAllowed { get; set; }
        [NotNull]
        public string IsOtherAllowed { get; set; }
        [NotNull]
        public string IsFreeForm { get; set; }
    }
}