﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TestApi
{
    public class test
    {

        public async Task<bool> PostAsync()
        {
            try
            {
                var jsonData = File.ReadAllText(@"D:\Work\NOVASmaintenance\Working\BIT_NOVAS2\TestApi\data.txt");


                string uri = "http://localhost:54262/api/sync/violation/";
                HttpClient _client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.GZip });
                StringContent content = new StringContent(jsonData.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await _client.PostAsync(uri, content);

                return response.IsSuccessStatusCode;
            }
            catch(Exception ex)
            {
                string ss = ex.Message;
            }

            return true;
        }


        public async Task<bool> PostSaveLogsAsync()
        {
            try
            {
                var jsonData = File.ReadAllText(@"D:\Work\NOVASmaintenance\Working\BIT_NOVAS2\TestApi\data.txt");


                string uri = "http://localhost:54262/api/logs/";
                HttpClient _client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.GZip });
                StringContent content = new StringContent(jsonData.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await _client.PostAsync(uri, content);

                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }

            return true;
        }
    }
}
