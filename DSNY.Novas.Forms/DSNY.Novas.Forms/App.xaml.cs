﻿using System.Reflection;
using DSNY.Novas.Common;
using DSNY.Novas.Services;

using Xamarin.Forms;

using DSNY.Novas.ViewModels;
using DSNY.Novas.ViewModels.Interfaces;
using DSNY.Novas.Forms.Components;
using System;
using DSNY.Novas.Models;
using System.Linq;

namespace DSNY.Novas.Forms
{
    public partial class App : Application
    {
        protected INavigationService NavigationService;
        protected ILocalDeviceSettingsService LocalSettingsService;
        public App()
        {
            InitializeComponent();

            DependencyResolver.SetDependencyProvider(new XamarinFormsDependencyProvider());
            MessageCenter.SetMessagingProvider(new XamarinFormsMessagingProvider());

            SetDependencies();
            RegisterPages();

            var loginScreen = new LoginScreen();
            loginScreen.BindingContext = new LoginViewModel();

            //only use if you need to set the main navigation to MainPage
            var mainPage = new MainPage();
            mainPage.BindingContext = new MainPageViewModel();

            MainPage = new NavigationPage(loginScreen);
            NavigationService = DependencyResolver.Get<INavigationService>();
            LocalSettingsService = DependencyResolver.Get<ILocalDeviceSettingsService>();
        }

        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected async override void OnResume()
        {
            // Handle when your app resumes
            var currentTime = DateTime.Now;
            //MainPage = new NavigationPage(new DSNY.Novas.Forms.MainPage());
            var mainPage = MainPage as NavigationPage;
            var currentPage = mainPage.Navigation.NavigationStack.LastOrDefault();
            var viewModel = currentPage.BindingContext as ViewModelBase;
            if (viewModel != null)
            {
                if (viewModel.UserSession != null && viewModel.UserSession?.TimeoutTimeStamp != default(DateTime))
                {
                    if ((currentTime - viewModel.UserSession.TimeoutTimeStamp).TotalMinutes >= 15)
                    {

                        await NavigationService.PushModalAsync(new TimeoutViewModel()
                        {
                            UserSession = viewModel.UserSession
                        });
                    }
                }
                else if (viewModel.NovMaster?.UserSession != null && 
                    viewModel.NovMaster?.UserSession.TimeoutTimeStamp != default(DateTime))
                {
                    if ((currentTime - viewModel.NovMaster.UserSession.TimeoutTimeStamp).TotalMinutes >= 15)
                    {

                        await NavigationService.PushModalAsync(new TimeoutViewModel()
                        {
                            UserSession = viewModel.NovMaster.UserSession
                        });
                    }
                }
            }
        }

        private void SetDependencies()
        {
            DependencyResolver.Set<DeviceService>();
            DependencyResolver.Set<NovService>();
            DependencyResolver.Set<SyncService>();
            DependencyResolver.Set<PlaceOfOccurrenceService>();
            DependencyResolver.Set<PersonBeingServedService>();
            DependencyResolver.Set<PersonCommercialIDService>();
            DependencyResolver.Set<LookupService>();
            DependencyResolver.Set<LoginService>();
            DependencyResolver.Set<NavigationService>();
            DependencyResolver.Set<VehicleService>();
            DependencyResolver.Set<PersonnelDataService>();
            DependencyResolver.Set<NovasUserService>();
            DependencyResolver.Set<ViolationDetailsService>();
            DependencyResolver.Set<AffirmationService>();
            DependencyResolver.Set<AffidavitService>();
            DependencyResolver.Set<LocalDeviceSettingsService>();
        }

        private void RegisterPages()
        {
            NavigationService navService = (NavigationService)DependencyService.Get<INavigationService>();

            navService.RegisterViewModel(typeof(NovSummaryViewModel), typeof(NovSummaryScreen));
            navService.RegisterViewModel(typeof(GroupTypeViewModel), typeof(GroupTypeScreen));
            navService.RegisterViewModel(typeof(PlaceOfOccurrenceViewModel), typeof(PlaceOfOccurrenceScreen));
            navService.RegisterViewModel(typeof(ViolatorHitViewModel), typeof(ViolatorHitScreen));
            navService.RegisterViewModel(typeof(StreetNameCheckViewModel), typeof(StreetNameCheckScreen));
            navService.RegisterViewModel(typeof(PersonBeingServedViewModel), typeof(PersonBeingServedScreen));
            navService.RegisterViewModel(typeof(PersonCommercialIDViewModel), typeof(PersonBeingServedIDScreen));
            navService.RegisterViewModel(typeof(VehicleDataViewModel), typeof(VehicleDataScreen));
            navService.RegisterViewModel(typeof(PersonnelDataViewModel), typeof(PersonnelDataScreen));
            navService.RegisterViewModel(typeof(ResidentialOwnerViewModel), typeof(ResidentialOwnerScreen));
            navService.RegisterViewModel(typeof(ViolationDetailsViewModel), typeof(ViolationDetailsScreen));
            navService.RegisterViewModel(typeof(ViolationDetailsFreeformPopoutViewModel), typeof(ViolationDetailsFreeformPopout));
            navService.RegisterViewModel(typeof(ViolationDetailsListPopoutViewModel), typeof(ViolationDetailsListPopout));
            navService.RegisterViewModel(typeof(ViolationCodeSearchViewModel), typeof(ViolationCodeSearchScreen));
            navService.RegisterViewModel(typeof(AffirmationSignatureViewModel), typeof(AffirmationSignatureScreen));
            navService.RegisterViewModel(typeof(ServiceTypeViewModel), typeof(ServiceTypeScreen));
            navService.RegisterViewModel(typeof(PersonalServiceViewModel), typeof(PersonalServiceScreen));
            navService.RegisterViewModel(typeof(PersonBeingServed2ViewModel), typeof(PersonBeingServedScreen));
            navService.RegisterViewModel(typeof(PersonCommercialID2ViewModel), typeof(PersonBeingServedIDScreen));
            navService.RegisterViewModel(typeof(PersonalServiceAffirmationSignatureViewModel), typeof(AffirmationSignatureScreen));
            navService.RegisterViewModel(typeof(OfficerAgentOtherViewModel), typeof(OfficerAgentOtherScreen));
            navService.RegisterViewModel(typeof(PremiseDetailsViewModel), typeof(PremiseDetailsScreen));
            navService.RegisterViewModel(typeof(AlternativeServiceAffirmationViewModel), typeof(AlternativeServiceAffirmationSignatureScreen));
            navService.RegisterViewModel(typeof(PremisePersonViewModel), typeof(PremisePersonScreen));
            navService.RegisterViewModel(typeof(ViolationSummaryViewModel), typeof(ViolationSummaryScreen));
            navService.RegisterViewModel(typeof(AboutNovasViewModel), typeof(AboutNovasScreen));
            navService.RegisterViewModel(typeof(DeviceStatusViewModel), typeof(DeviceStatusScreen));
            navService.RegisterViewModel(typeof(HelpViewModel), typeof(HelpScreen));
            navService.RegisterViewModel(typeof(PickerSearchScreenViewModel), typeof(PickerSearchScreen));
            navService.RegisterViewModel(typeof(PrinterViewModel), typeof(PrinterScreen));
            navService.RegisterViewModel(typeof(TimeoutViewModel), typeof(TimeoutScreen));
            navService.RegisterViewModel(typeof(SyncStatusViewModel), typeof(SyncStatusScreen));
            navService.RegisterViewModel(typeof(SetPinViewModel), typeof(SetPinView));
            navService.RegisterViewModel(typeof(MainPageViewModel), typeof(DSNY.Novas.Forms.MainPage));
        }
    }
}
