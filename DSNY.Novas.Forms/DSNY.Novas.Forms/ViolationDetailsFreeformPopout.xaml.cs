﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViolationDetailsFreeformPopout : BaseContentPage
    {
        public ViolationDetailsFreeformPopout()
        {
            InitializeComponent();
        }

        void TextChanged(object sender, TextChangedEventArgs e)
        {
            ((ViolationDetailsFreeformPopoutViewModel)BindingContext).TextChanged(e.NewTextValue, e.OldTextValue);
        }
    }
}