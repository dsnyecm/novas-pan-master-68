﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NovSummaryScreen : BaseContentPage
    {
        public NovSummaryScreen()
        {
            InitializeComponent();
        }

        public void ListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null; // We don't want to allow selection on this screen
        }
    }
}