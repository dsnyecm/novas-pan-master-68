﻿using DSNY.Novas.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PersonnelDataScreen : BaseContentPage
	{
		public PersonnelDataScreen ()
		{
			InitializeComponent ();
		}

        private void ListAllPrintersPressed(object sender, EventArgs e)
        {
            var vm = BindingContext as PersonnelDataViewModel;
            vm?.ListAllPrintersCommand?.Execute(null);
        }
    }

}