﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViolationDetailsListPopout : BaseContentPage
	{
		public ViolationDetailsListPopout ()
		{
			InitializeComponent ();
		}

        public void TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue == "" && OtherFreeForm.IsFocused)
            {
                OtherFreeForm.Unfocus();
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await System.Threading.Tasks.Task.Delay(200);
                    OtherFreeForm.Focus();
                });
            }
                
        }
    }
}