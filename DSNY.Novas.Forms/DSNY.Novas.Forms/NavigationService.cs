﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Xamarin.Forms;

using DSNY.Novas.Forms;
using DSNY.Novas.ViewModels;
using DSNY.Novas.ViewModels.Interfaces;
using DSNY.Novas.Common;
using Plugin.Settings;

// Based on the View Model First navigation strategy described here:
// https://codemilltech.com/xamarin-forms-view-model-first-navigation/

[assembly: Xamarin.Forms.Dependency(typeof(NavigationService))]
namespace DSNY.Novas.Forms
{
    public class NavigationService : INavigationService
    {
        List<string> _CurrentViewStack = new List<string>();
        List<string> _CompleteViewStack = new List<string>();
        INavigation CurrentNavigation => Application.Current.MainPage.Navigation;
        protected IAlertService AlertService;
        private string didResume = "false"; 

        // We should be ok with a regular Dictionary here because the NavigationService should only
        // be getting called from the Main Thread (in the View layer)
        static readonly Dictionary<Type, Type> _viewModelViewDictionary = new Dictionary<Type, Type>();

        public void RegisterViewModel(Type viewModelType, Type viewType)
        {
            _viewModelViewDictionary.Add(viewModelType, viewType);
            AlertService = DependencyResolver.Get<IAlertService>();
        }

        public async Task PopAsync()
        {
            if (_CurrentViewStack.Count > 0)
            {
                _CurrentViewStack.Remove(_CurrentViewStack.Last());
            }
            await CurrentNavigation.PopAsync(true);
        }

        public async Task PopModalAsync()
        {
            if (CurrentNavigation.ModalStack.Count > 0)
            {
                await CurrentNavigation.PopModalAsync(true);
            }
        }

        public async Task PopToRootAsync()
        {
           // _CurrentViewStack = new List<string>();
            await CurrentNavigation.PopToRootAsync(true);
        }

        public async Task PopToSummaryScreenAsync()
        {
            var navStack = CurrentNavigation.NavigationStack.Reverse();
            var enumerator = navStack.GetEnumerator();
            enumerator.MoveNext();
            enumerator.MoveNext();
            var page = enumerator.Current;
            while (!(page is NovSummaryScreen))
            {
                CurrentNavigation.RemovePage(page);

                enumerator.MoveNext();
                page = enumerator.Current;
            }

          //  _CurrentViewStack = new List<string>();
            await CurrentNavigation.PopAsync();
        }

        public async Task PopFromSyncToLoginIfTimeoutScreenPresentOtherwiseGoBack()
        {
            List<Page> navStack = new List<Page>(CurrentNavigation.ModalStack);

            int timeoutIndex = navStack.FindIndex(ns =>
            {
                if (ns.GetType() == typeof(NavigationPage))
                {
                    return ((NavigationPage)ns).RootPage.Title == "Timeout Screen";
                }

                return false;
            });


            if (timeoutIndex > -1)
            {
                int current = navStack.Count();
                while (current >= timeoutIndex)
                {
                    CurrentNavigation.PopModalAsync();
                    current--;
                }

                await PopToLoginScreenAsync();
            } else
            {
                await PopModalAsync();
            }            
        }

        public async Task PopToLoginScreenAsync()
        {
            var navStack = CurrentNavigation.NavigationStack.Reverse();
            var enumerator = navStack.GetEnumerator();
            enumerator.MoveNext();
            enumerator.MoveNext();
            var page = enumerator.Current;
            while (!(page is LoginScreen))
            {
                CurrentNavigation.RemovePage(page);

                enumerator.MoveNext();
                page = enumerator.Current;
            }

           // _CurrentViewStack = new List<string>();
            await CurrentNavigation.PopAsync();
        }

        public async Task PushAsync(ViewModelBase viewModel)
        {
            await SetUserSessionTimeoutTimestamp(viewModel);
            var page = await InstantiatePage(viewModel);

            await CurrentNavigation.PushAsync(page);
            _CompleteViewStack = _CompleteViewStack?.Union(CurrentNavigation?.NavigationStack?.Select(s => s.GetType().Name)).ToList();
        }

        public async Task PushModalAsync(ViewModelBase viewModel)
        {
            await SetUserSessionTimeoutTimestamp(viewModel);
            var view = await InstantiatePage(viewModel);

            // Most likely we're going to want to wrap this in a navigation page so we can have a title bar, etc.
            var navPage = new NavigationPage(view);

            await CurrentNavigation.PushModalAsync(navPage);
        }

        async Task<Page> InstantiatePage(ViewModelBase viewModel)
        {
            // Look up the Page type based on the view model type
            var viewModelType = viewModel.GetType();
            
            var viewType = _viewModelViewDictionary[viewModelType];

            var page = (Page)Activator.CreateInstance(viewType);

            page.BindingContext = viewModel;
            
            if (viewModel.NovMaster != null) /* we have entered a flow */
            {
                var viewModelName = viewModel.GetType().FullName;
                var viewModelBase = ViewModelBase.GetViewModelBase(viewModelName); /* check if view model has default constructor */

                if (viewModel.NovMaster.NovInformation != null)
                {
                    didResume = "false";
                    var didResumeAlready =  CrossSettings.Current.GetValueOrDefault("DidResumeAlready_" + viewModel.NovMaster.NovInformation.NovNumber, didResume);


                    if (_CurrentViewStack != null && (didResume == "false" || didResumeAlready == "false") && string.IsNullOrEmpty(viewModel?.NovMaster?.NovInformation?.AlternateService?.Trim()))
                    {
                        _CurrentViewStack.Add(viewModelName);
                    }
                }else
                {
                    if (_CurrentViewStack != null)
                    {
                        _CurrentViewStack.Add(viewModelName);
                    }
                }

                if (viewModelBase != null)
                {
                    object viewStackObj;
                    Application.Current.Properties.TryGetValue("ViewStack", out viewStackObj);

                    var prevModelStack = ((string)viewStackObj ?? "").Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                    var prevModelName = prevModelStack.LastOrDefault();

                    // NH-1258
                    //if (viewModelName != prevModelName)
                   //     Application.Current.Properties["AttemptNumber"] = 0;

                    Application.Current.Properties["ViewStack"] = String.Join("||", _CurrentViewStack);
                    await Application.Current.SavePropertiesAsync();
                }
                
            }

            return page;
        }

        public async Task SetLatestNovNumberAndAttemptNumber(string latestIncompleteNovNumberInSqlLite) {
            object numInPersistStorage;
            Application.Current.Properties.TryGetValue("latestIncompleteNovNumber", out numInPersistStorage);

            if (!string.IsNullOrEmpty(latestIncompleteNovNumberInSqlLite)
                && numInPersistStorage != null && numInPersistStorage.ToString() != ""
                && latestIncompleteNovNumberInSqlLite != numInPersistStorage.ToString()) {

                Application.Current.Properties["latestIncompleteNovNumber"] = latestIncompleteNovNumberInSqlLite;
                Application.Current.Properties["AttemptNumber"] = 0;
                await Application.Current.SavePropertiesAsync();
            }
            if ((numInPersistStorage == null || numInPersistStorage == "")
                && !string.IsNullOrEmpty(latestIncompleteNovNumberInSqlLite)) {
                Application.Current.Properties["latestIncompleteNovNumber"] = latestIncompleteNovNumberInSqlLite;
                await Application.Current.SavePropertiesAsync();
            }

        }

        public async Task PopToCachedScreenAsync(ViewModelBase baseModel)
        {
            List<string> stackList = new List<string>();

            //Create a List of viewmodels where the resume functionality should occur 
            List<string> resumeViewModels = new List<string>()
            {
                "DSNY.Novas.ViewModels.GroupTypeViewModel",
                "DSNY.Novas.ViewModels.PlaceOfOccurrenceViewModel",
                "DSNY.Novas.ViewModels.PersonBeingServedViewModel",
                "DSNY.Novas.ViewModels.PersonCommercialIDViewModel",
                "DSNY.Novas.ViewModels.ViolationDetailsViewModel",
                "DSNY.Novas.ViewModels.ResidentialOwnerViewModel",
                "DSNY.Novas.ViewModels.AffirmationSignatureViewModel",
                "DSNY.Novas.ViewModels.ViolatorHitViewModel"
            };
            
            //get the last saved view model
            object viewStackObj;

            Application.Current.Properties.TryGetValue("ViewStack", out viewStackObj);

            if (viewStackObj == null)
                return;

            //how many times have we tried to load this page?
            object attemptNumberObj;
            Application.Current.Properties.TryGetValue("AttemptNumber", out attemptNumberObj);

            var attemptNumber = attemptNumberObj != null ? (int)attemptNumberObj : 0;
            attemptNumber++;

            Application.Current.Properties["AttemptNumber"] = attemptNumber;
            await Application.Current.SavePropertiesAsync();

            if (attemptNumber > 3)
                return;

            string[] viewModelStack = ((string)viewStackObj ?? "").Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            stackList = new List<string>(viewModelStack);

            if (viewModelStack.Contains("DSNY.Novas.ViewModels.GroupTypeViewModel"))
            {
               var groupTypeIndex =  stackList.LastIndexOf("DSNY.Novas.ViewModels.GroupTypeViewModel");

                viewModelStack = viewModelStack.Skip(groupTypeIndex).ToArray();

            }

            //Do not resume if user is on back of the ticket/AoS process 
            if (!stackList.Contains("DSNY.Novas.ViewModels.ServiceTypeViewModel"))
            {
                stackList.RemoveAll(item => !resumeViewModels.Contains(item));

                if (stackList.Count != 0 && String.IsNullOrWhiteSpace(baseModel.NovMaster.NovInformation.AlternateService))
                {
                    if (resumeViewModels.Contains(stackList.Last()))
                    {
                        //Add alert here 
                        await AlertService.DisplayAlert(new AlertViewModel("12063", "A NOV was previously started. You will be required to finish that NOV."));

                        //rebuild the navigation stack
                        foreach (var viewModelName in stackList)
                        {
                            ViewModelBase viewModel = ViewModelBase.GetViewModelBase(viewModelName) ?? new ViewModelBase();
                            viewModel.NovMaster = baseModel.NovMaster;

                            await PushAsync(viewModel);
                        }

                        didResume = "true";
                        CrossSettings.Current.AddOrUpdateValue("DidResumeAlready_" + baseModel.NovMaster.NovInformation.NovNumber, didResume);
                    }
                }
            }
            else
            {
                didResume = "false";
                CrossSettings.Current.AddOrUpdateValue("DidResumeAlready_" + baseModel.NovMaster.NovInformation.NovNumber, didResume);
            }
        }

        public IEnumerable<string> CompleteViewStack(List<string> completeViewStack = null)
        {
            if (completeViewStack != null)
                _CompleteViewStack = completeViewStack;

            return _CompleteViewStack;
        }

        public async Task<bool> SetUserSessionTimeoutTimestamp(ViewModelBase baseModel)
        {
            if (baseModel.UserSession != null)
            {
                baseModel.UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (baseModel.NovMaster?.UserSession != null)
            {
                baseModel.NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            return true;
        }
    }
}