﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DSNY.Novas.ViewModels;
using System.Text.RegularExpressions;
using Plugin.Settings;

namespace DSNY.Novas.Forms
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginScreen : BaseContentPage
    {       
        public LoginScreen()
        {
            InitializeComponent();

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var vm = BindingContext as LoginViewModel;
            vm.SyncStatus = CrossSettings.Current.GetValueOrDefault("SYNCFAILED", string.Empty, "SYNCFAILED");
                                    

            if (vm.SyncStatus == "1")
            {
                
                vm?.SetupCommand?.Execute(null);                
            }

            await Task.Delay(0);
        }

        private void DepartmentEnterPressed(object sender, EventArgs e)
        {
            var vm = BindingContext as LoginViewModel;
            vm?.DepartmentSelectedCommand?.Execute(null);
        }

        private void PINTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length == 6)
            {
                //PINEnterPressed(sender, e);
            }
        }
        private void HHTNumberTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 0)
            {
                //((Entry)sender).Text = Regex.Replace(e.NewTextValue, @"[^a-zA-Z0-9-_.~]", "");
                ((Entry)sender).Text = Regex.Replace(e.NewTextValue, @"[^0-9-_.~]", "");
            }
            if (e.NewTextValue.Length > 3)
            {
                ((Entry)sender).Text = e.NewTextValue.Remove(e.NewTextValue.Length - 1);
            }
        }
        private void SerialNumberTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 0)
            {
                var result = Regex.Replace(e.NewTextValue, @"[^a-zA-Z0-9-_.~]", "");
                ((Entry)sender).Text = result;
            }
        }
        private void PINEnterPressed(object sender, EventArgs e)
        {
            var vm = BindingContext as LoginViewModel;
            vm?.PINEnteredCommand?.Execute(null);
        }
    }
}