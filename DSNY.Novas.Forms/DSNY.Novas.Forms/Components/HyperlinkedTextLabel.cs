using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace DSNY.Novas.Forms.Components
{
    public class HyperlinkedTextLabel : Label
    {
        public static readonly BindableProperty ScriptTextProperty = BindableProperty.Create(nameof(ScriptText), typeof(string), typeof(HyperlinkedTextLabel), null, BindingMode.TwoWay);
        public static readonly BindableProperty OutputTextProperty = BindableProperty.Create(nameof(OutputText), typeof(string), typeof(HyperlinkedTextLabel), null, BindingMode.TwoWay);

        // Markers that delineate text placeholders that should be hyperlinked.
        public static readonly BindableProperty LinkDemarcatorsProperty = BindableProperty.Create(nameof(LinkDemarcators), typeof(List<string>), typeof(HyperlinkedTextLabel), null, BindingMode.OneWay);
        
        // List of strings that should become hyperlinks
        public static readonly BindableProperty LinksProperty = BindableProperty.Create(nameof(Links), typeof(List<string>), typeof(HyperlinkedTextLabel), null, BindingMode.OneWay);

        // Dictionary that maps text placeholders to the new text they should be replaced with.  Used to replace the replace the violation data placeholders with the actual values selected by the user.
        // NOTE: If key lookup should be case-insensitive, be sure to set an appropriate IEqualityComparator on your dictionary, such as StringComparer.OrdinalIgnoreCase
        public static readonly BindableProperty ReplacementMappingProperty = BindableProperty.Create(nameof(ReplacementMapping), typeof(Dictionary<string, string>), typeof(HyperlinkedTextLabel), null, BindingMode.OneWay);

        // Called when a link is clicked.  Passes in the text of the link as the parameter.
        public static readonly BindableProperty LinkClickedCommandProperty = BindableProperty.Create(nameof(LinkClickedCommand), typeof(ICommand), typeof(HyperlinkedTextLabel), null, BindingMode.OneWay);
        
        private static List<string> DefaultLinkDemarcators = new List<string>() { "(", ")" };

        public string ScriptText
        {
            get { return (string)GetValue(ScriptTextProperty); }
            set
            {
                SetValue(ScriptTextProperty, value);
            }
        }

        public string OutputText
        {
            get { return (string)GetValue(OutputTextProperty); }
            set
            {
                SetValue(OutputTextProperty, value);
            }
        }

        public List<string> LinkDemarcators
        {
            get { return (List<string>)GetValue(LinkDemarcatorsProperty) ?? DefaultLinkDemarcators; }
            set
            {
                SetValue(LinkDemarcatorsProperty, value);
            }
        }

        public List<string> Links
        {
            get { return (List<string>)GetValue(LinksProperty); }
            set
            {
                SetValue(LinksProperty, value);
            }
        }

        public Dictionary<string, string> ReplacementMapping
        {
            get { return (Dictionary<string, string>)GetValue(ReplacementMappingProperty); }
            set
            {
                SetValue(ReplacementMappingProperty, value);
            }
        }

        public ICommand LinkClickedCommand
        {
            get { return (ICommand)GetValue(LinkClickedCommandProperty); }
            set
            {
                SetValue(LinkClickedCommandProperty, value);
            }
        }
    }
}
