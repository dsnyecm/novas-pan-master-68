﻿using System;
using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;

using Xam.Plugin;

using DSNY.Novas.ViewModels;

namespace DSNY.Novas.Forms.Components
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuBar : ContentView
    {
        public static readonly BindableProperty ShowMenuProperty = BindableProperty.Create(nameof(ShowMenu), typeof(bool), typeof(MenuBar), true, BindingMode.OneWay);
        public static readonly BindableProperty ShowBackProperty = BindableProperty.Create(nameof(ShowBack), typeof(bool), typeof(MenuBar), true, BindingMode.OneWay);
        public static readonly BindableProperty ShowNextProperty = BindableProperty.Create(nameof(ShowNext), typeof(bool), typeof(MenuBar), true, BindingMode.OneWay);
        public static readonly BindableProperty ShowPlusProperty = BindableProperty.Create(nameof(ShowPlus), typeof(bool), typeof(MenuBar), false, BindingMode.OneWay);

        public static readonly BindableProperty MenuItemSourceProperty = BindableProperty.Create(nameof(MenuItemSource), typeof(IList), typeof(MenuBar), null, BindingMode.OneWay);
        public static readonly BindableProperty MenuItemTappedCommandProperty = BindableProperty.Create(nameof(MenuItemTappedCommand), typeof(ICommand), typeof(MenuBar), null, BindingMode.OneWay);
        
        public PopupMenu Menu;

        public MenuBar()
        {
            InitializeComponent();

            this.SetBinding(ShowMenuProperty, nameof(ViewModelBase.ShowMenuButton));
            this.SetBinding(ShowBackProperty, nameof(ViewModelBase.ShowBackButton));
            this.SetBinding(ShowNextProperty, nameof(ViewModelBase.ShowNextButton));
            this.SetBinding(ShowPlusProperty, nameof(ViewModelBase.ShowPlusButton));
            this.SetBinding(MenuItemSourceProperty, nameof(ViewModelBase.MenuItems));
            this.SetBinding(MenuItemTappedCommandProperty, nameof(ViewModelBase.MenuItemTappedCommand));

            Menu = new PopupMenu();
            Menu.BindingContext = this;
            Menu.SetBinding(PopupMenu.ItemsSourceProperty, nameof(MenuItemSource));
            Menu.OnItemSelected += (string item) =>
            {
                MenuItemTappedCommand?.Execute(item);
            };
        }

        private void MenuButton_Tapped(object sender, EventArgs e)
        {
            Menu.ShowPopup(sender as Button);
        }

        public bool ShowMenu
        {
            get { return (bool)GetValue(ShowMenuProperty); }
            set
            {
                SetValue(ShowMenuProperty, value);
            }
        }

        public bool ShowBack
        {
            get { return (bool)GetValue(ShowBackProperty); }
            set
            {
                SetValue(ShowBackProperty, value);
            }
        }

        public bool ShowNext
        {
            get { return (bool)GetValue(ShowNextProperty); }
            set
            {
                SetValue(ShowNextProperty, value);
            }
        }

        public bool ShowPlus
        {
            get { return (bool)GetValue(ShowPlusProperty); }
            set
            {
                SetValue(ShowPlusProperty, value);
            }

        }

        public IList MenuItemSource
        {
            get { return (IList)GetValue(MenuItemSourceProperty); }
            set
            {
                SetValue(MenuItemSourceProperty, value);
            }
        }

        public ICommand MenuItemTappedCommand
        {
            get { return (ICommand)GetValue(MenuItemTappedCommandProperty); }
            set
            {
                SetValue(MenuItemTappedCommandProperty, value);
            }
        }
    }
}