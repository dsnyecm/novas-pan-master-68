﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

using DSNY.Novas.ViewModels;

//
// Based on Matthew Leibowitz's MultiSelectListView
// https://github.com/mattleibow/MultiSelectListView
//
namespace DSNY.Novas.Forms.Components
{
    public static class MultiSelectListView
    {
        public static readonly BindableProperty IsMultiSelectProperty = BindableProperty.CreateAttached("IsMultiSelect", typeof(bool), typeof(ListView), false, propertyChanged: OnIsMultiSelectChanged);

        public static bool GetIsMultiSelect(BindableObject view) => (bool)view.GetValue(IsMultiSelectProperty);

        public static void SetIsMultiSelect(BindableObject view, bool value) => view.SetValue(IsMultiSelectProperty, value);

        private static void OnIsMultiSelectChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var listView = bindable as ListView;
            if (listView != null)
            {
                // always remove event
                listView.ItemSelected -= OnItemSelected;

                // add the event if true
                if (true.Equals(newValue))
                {
                    listView.ItemSelected += OnItemSelected;
                }
            }
        }

        private static void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as SelectableItem;
            if (item != null)
            {
                // Toggle the selection property if we haven't already toggled it in the last 0.5 seconds
                // (Due to a bug in Xamarin Forms, OnItemSelected gets called twice on UWP: https://bugzilla.xamarin.com/show_bug.cgi?id=44886)
                if (item.LastSelectedTimestamp == null || item.LastSelectedTimestamp.AddSeconds(0.5).CompareTo(DateTime.Now) < 0)
                {
                    item.IsSelected = !item.IsSelected;
                    item.LastSelectedTimestamp = DateTime.Now;
                }
            }

            // deselect the item
            ((ListView)sender).SelectedItem = null;
        }
    }
}
