﻿using System;
using Xamarin.Forms;

namespace DSNY.Novas.Forms.Components
{
    public class Radiobutton : View
    {
        public static readonly BindableProperty IsCheckedProperty = BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(Radiobutton), false, BindingMode.TwoWay);
        public static readonly new BindableProperty IsEnabledProperty = BindableProperty.Create(nameof(IsEnabled), typeof(bool), typeof(Radiobutton), true, BindingMode.TwoWay);

        public EventHandler<bool> CheckedChanged;
        public EventHandler<bool> EnabledChanged; 

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set
            {
                SetValue(IsCheckedProperty, value);
                CheckedChanged?.Invoke(this, value);
            }
        }

        public new bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set
            {
                SetValue(IsEnabledProperty, value);
               // base.IsEnabled = value;
                EnabledChanged?.Invoke(this, value);
            }
        }
    }
}
