﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xam.Plugin;
using Xamarin.Forms;

namespace DSNY.Novas.Forms.Components
{
    public class LongPressMenuFrame : Frame
    {
        public static readonly BindableProperty LongPressCommandProperty = BindableProperty.Create(nameof(LongPressCommand), typeof(ICommand), typeof(LongPressMenuFrame), null, BindingMode.OneWay);
        public static readonly BindableProperty LongPressItemProperty = BindableProperty.Create(nameof(LongPressItem), typeof(object), typeof(LongPressMenuFrame), null, BindingMode.OneWay);
        public static readonly BindableProperty MenuItemSourceProperty = BindableProperty.Create(nameof(MenuItemSource), typeof(IList), typeof(LongPressMenuFrame), null, BindingMode.OneWay);
        public static readonly BindableProperty MenuItemTappedCommandProperty = BindableProperty.Create(nameof(MenuItemTappedCommand), typeof(ICommand), typeof(LongPressMenuFrame), null, BindingMode.OneWay);

        public PopupMenu Menu;

        public LongPressMenuFrame()
        {

            Menu = new PopupMenu();
            Menu.BindingContext = this;
            Menu.SetBinding(PopupMenu.ItemsSourceProperty, nameof(MenuItemSource));
            Menu.OnItemSelected += (string item) =>
            {
                MenuItemTappedCommand?.Execute(item);
            };
        }
        public ICommand LongPressCommand
        {
            get { return (ICommand)GetValue(LongPressCommandProperty); }
            set
            {
                SetValue(LongPressCommandProperty, value);
            }
        }

        public object LongPressItem
        {
            get { return GetValue(LongPressItemProperty); }
            set
            {
                SetValue(LongPressItemProperty, value);
            }
        }

        public IList MenuItemSource
        {
            get { return (IList)GetValue(MenuItemSourceProperty); }
            set
            {
                SetValue(MenuItemSourceProperty, value);
            }
        }

        public ICommand MenuItemTappedCommand
        {
            get { return (ICommand)GetValue(MenuItemTappedCommandProperty); }
            set
            {
                SetValue(MenuItemTappedCommandProperty, value);
            }
        }

        public async void OnLongPress()
        {
            LongPressCommand?.Execute(LongPressItem);
            await Task.Delay(50);
            Menu.ShowPopup(this);
        }
    }
}
