﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DSNY.Novas.ViewModels;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.ViewModels.Interfaces;

namespace DSNY.Novas.Forms.Components
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchablePicker : ContentView
    {
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IList), typeof(SearchablePicker), null, BindingMode.TwoWay, propertyChanged: OnItemsSourceChanged);
        public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create(nameof(SelectedItem), typeof(object), typeof(SearchablePicker), null, BindingMode.TwoWay, propertyChanged: OnSelectedItemChanged);
        public static readonly BindableProperty SearchScreenTitleProperty = BindableProperty.Create(nameof(SearchScreenTitle), typeof(string), typeof(SearchablePicker), null, BindingMode.OneWay);
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(SearchablePicker), null, BindingMode.OneWay);

        public static readonly BindableProperty CustomTapCommandProperty = BindableProperty.Create(nameof(CustomTapCommand), typeof(ICommand), typeof(SearchablePicker), null, BindingMode.OneWay);

        public string DisplayBindingPath { get; set; }

        public SearchablePicker()
        {
            InitializeComponent();

            DisplayLabel.BindingContext = this;
        }

        public async void PickerTapped(object sender, EventArgs e)
        {
            if (CustomTapCommand != null)
            {
                CustomTapCommand.Execute(null);
                return;
            }

            if (ItemsSource == null)
            {
                return;
            }

            List<string> dataSource;
            if (DisplayBindingPath != null)
            {
                dataSource = ItemsSource.OfType<object>().Select((object item) => item.GetType().GetProperty(DisplayBindingPath)?.GetValue(item)?.ToString()).ToList();
            }
            else
            {
                dataSource = ItemsSource.OfType<object>().Select((object item) => item.ToString()).ToList();
            }

            var selectedItemDisplayValue = SelectedItem != null ? DisplayValue : null;
            var searchViewModel = new PickerSearchScreenViewModel(SearchScreenTitle, dataSource, selectedItemDisplayValue, ItemSelectedCommand);
            await DependencyResolver.Get<INavigationService>().PushModalAsync(searchViewModel);
        }

        private ICommand ItemSelectedCommand => new Command(ItemSelected);

        private void ItemSelected(object itemDisplayString)
        {
            if (DisplayBindingPath != null)
            {
                SelectedItem = ItemsSource.OfType<object>().FirstOrDefault((object item) => item.GetType().GetProperty(DisplayBindingPath)?.GetValue(item)?.ToString()?.Equals(itemDisplayString) ?? false);
            }
            else
            {
                SelectedItem = ItemsSource.OfType<object>().FirstOrDefault((object item) => item.ToString().Equals(itemDisplayString));
            }
        }

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        static void OnItemsSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((SearchablePicker)bindable).OnItemsSourceChanged((IList)oldValue, (IList)newValue);
        }

        void OnItemsSourceChanged(IList oldValue, IList newValue)
        {
            ItemsSource = newValue;

            if (ItemsSource != null)
            {
                if (!ItemsSource.Contains(SelectedItem))
                    SelectedItem = null;
            }
        }

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set
            {
                SetValue(SelectedItemProperty, value);
                OnPropertyChanged(nameof(DisplayValue));
            }
        }

        static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((SearchablePicker)bindable).OnSelectedItemChanged(oldValue, newValue);
        }

        void OnSelectedItemChanged(object oldValue, object newValue)
        {
            SelectedItem = newValue;
        }

        public String Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set
            {
                SetValue(PlaceholderProperty, value);
            }
        }

        public String SearchScreenTitle
        {
            get { return (string)GetValue(SearchScreenTitleProperty); }
            set
            {
                SetValue(SearchScreenTitleProperty, value);
            }
        }

        public ICommand CustomTapCommand
        {
            get { return (ICommand)GetValue(CustomTapCommandProperty); }
            set
            {
                SetValue(CustomTapCommandProperty, value);
            }
        }

        public String DisplayValue
        {
            get
            {
                string displayValue = Placeholder;
                if (SelectedItem != null)
                {
                    if (DisplayBindingPath != null)
                    {
                        displayValue = SelectedItem.GetType().GetProperty(DisplayBindingPath)?.GetValue(SelectedItem)?.ToString();
                    }
                    else
                    {
                        displayValue = SelectedItem.ToString();
                    }
                }

                return displayValue;
            }
        }
    }
}