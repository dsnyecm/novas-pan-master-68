﻿using Xamarin.Forms;

using DSNY.Novas.ViewModels.Interfaces;
using DSNY.Novas.ViewModels;

namespace DSNY.Novas.Forms
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var viewModel = BindingContext as IVisibilityAwareViewModel;
            if (viewModel != null)
            {
                viewModel.OnAppearing();
            }

            var loadableViewModel = BindingContext as ILoadableViewModel;
            if (loadableViewModel != null)
            {
                await loadableViewModel.LoadAsync();
            }

            MessagingCenter.Subscribe<AlertService, string>(this, "InvalidEntry", (sender, elementName) => {
                var element = this.FindByName<View>(elementName);

                if (element == null)
                    return;

                element.Focus();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            var viewModel = BindingContext as IVisibilityAwareViewModel;
            if (viewModel != null)
            {
                viewModel.OnDisappearing();
            }

            MessagingCenter.Unsubscribe<AlertService, string>(this, "InvalidEntry");
        }

        protected override bool OnBackButtonPressed()
        {
            var viewModel = BindingContext as ViewModelBase;
            if (viewModel != null)
            {
                if (viewModel.ShowBackButton || viewModel.ShowCancelButton)
                {
                    viewModel.BackCommand?.Execute(null);
                }
                return true;
            }
            return base.OnBackButtonPressed();
        }
    }
}