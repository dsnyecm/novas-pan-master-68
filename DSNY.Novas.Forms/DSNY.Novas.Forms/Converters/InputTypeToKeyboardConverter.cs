﻿using System;
using System.Globalization;

using Xamarin.Forms;

using DSNY.Novas.ViewModels.Utils;

namespace DSNY.Novas.Forms.Converters
{
    public class InputTypeToKeyboardConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is InputType))
            {
                return Keyboard.Default;
            }

            var inputType = (InputType)value;
            switch (inputType)
            {
                case InputType.Alphanumeric:
                    return Keyboard.Text;
                case InputType.Numeric:
                    return Keyboard.Numeric;
                case InputType.Currency:
                    return Keyboard.Numeric;
                case InputType.Email:
                    return Keyboard.Email;
                case InputType.Phone:
                    return Keyboard.Telephone;
                default:
                    return Keyboard.Default;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
