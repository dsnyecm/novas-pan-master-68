﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace DSNY.Novas.Forms.Converters
{
    public class AlternatingListColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = 0;
            var listView = parameter as ListView;

            foreach (var item in listView.ItemsSource)
            {
                if (item != value)
                {
                    index++;
                }
                else
                {
                    return (index % 2 == 0) ? Color.FromHex("#20a3b1ff") : Color.Transparent;
                }
            }

            return Color.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
