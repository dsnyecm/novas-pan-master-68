﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Globalization;
namespace DSNY.Novas.Forms.Converters
{
    public class StringToHtmlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var htmlSource = new HtmlWebViewSource();
            htmlSource.Html = (string) value;
            return htmlSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
