﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;

using DSNY.Novas.Forms;
using DSNY.Novas.ViewModels;
using DSNY.Novas.ViewModels.Interfaces;
using Plugin.Settings;

[assembly: Xamarin.Forms.Dependency(typeof(AlertService))]
namespace DSNY.Novas.Forms
{
    public class AlertService : IAlertService
    {
        public async Task<bool> DisplayAlert(AlertViewModel alert)
        {
           

            if (String.IsNullOrEmpty(alert.CancelTitle))
            {
                await CurrentPage.DisplayAlert(alert.Title, alert.Message, alert.OkTitle);

                if (alert.OkAction != null)
                {
                   
                    alert.OkAction.Invoke();
                }

                if(alert.OkTitle=="Ok")
                {
                    // why even do this here?
                    // http://jira.dsnyad.nycnet/browse/NH-1366 only set back to "0" when it SUCCEEDS.
                    // CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "0", "SYNCFAILED");
                }                    

                if (alert.ElementName != null)
                    MessagingCenter.Send<AlertService, string>(this, "InvalidEntry", alert.ElementName);

                return true; // If there's only one button, return true
            }
            else
            {
                var result = await CurrentPage.DisplayAlert(alert.Title, alert.Message, alert.OkTitle, alert.CancelTitle);

                if (result)
                {
                    if (alert.OkAction != null)
                    {
                        alert.OkAction.Invoke();
                    }
                }
                else
                {
                    if (alert.CancelAction != null)
                    {
                        alert.CancelAction.Invoke();
                    }
                }

                if (alert.ElementName != null)
                    MessagingCenter.Send<AlertService, string>(this, "InvalidEntry", alert.ElementName);

               
                return result;
            }
        }

        private Page CurrentPage
        {
            get
            {
                return App.Current.MainPage; // May need to adapt later to handle modal pages
            }
        }
    }
}
