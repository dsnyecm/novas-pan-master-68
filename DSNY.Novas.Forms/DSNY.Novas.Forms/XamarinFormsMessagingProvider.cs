﻿using System;
using Xamarin.Forms;
using DSNY.Novas.Common.Interfaces;

namespace DSNY.Novas.Forms
{
    class XamarinFormsMessagingProvider : IMessagingProvider
    {
        public void Unsubscribe<TSender>(object subscriber, string message) where TSender : class
        {
            MessagingCenter.Unsubscribe<TSender>(subscriber, message);
        }

        public void Subscribe<TSender>(object subscriber, string message, Action<TSender> callback, TSender source = null) where TSender : class
        {
            MessagingCenter.Subscribe(subscriber, message, callback, source);
        }

        public void Send<TSender>(TSender sender, string message) where TSender : class
        {
            MessagingCenter.Send(sender, message);
        }
    }
}
