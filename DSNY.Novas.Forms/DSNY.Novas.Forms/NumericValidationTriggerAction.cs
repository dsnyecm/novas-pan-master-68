﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DSNY.Novas.Forms
{
    class NumericValidationTriggerAction : Xamarin.Forms.TriggerAction<Entry>
    {
        protected override void Invoke(Entry entry)
        {
            double result;
            bool isValid = Double.TryParse(entry.Text, out result);
            //entry.BackgroundColor =
            //      isValid ? Color.Default : Color.Red;
            entry.Text = entry.Text.Replace(".", "");
        }
    }
}
