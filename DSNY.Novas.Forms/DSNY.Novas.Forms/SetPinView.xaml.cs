﻿using System;
using System.IO;

using Xamarin.Forms;
using SignaturePad.Forms;

using DSNY.Novas.ViewModels;

namespace DSNY.Novas.Forms
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetPinView : BaseContentPage
    {
        public SetPinView()
        {
            InitializeComponent();
            signature.CaptionLabel.FontSize = 14;
            signature.SignaturePrompt.FontSize = 20;
            signature.ClearLabel.FontSize = 14;
        }

        private async void SubmitTapped(object sender, EventArgs e)
        {
            var viewModel = BindingContext as SetPinViewModel;
            Stream image = await signature.GetImageStreamAsync(SignatureImageFormat.Png);

            if (image != null)
            {
                MemoryStream byteStream = new MemoryStream();
                image.CopyTo(byteStream);

                byte[] signatureBytes = byteStream.ToArray();
                viewModel.Signature = signatureBytes;
            }
            else
            {
                viewModel.Signature = null;
            }

            viewModel.SubmitCommand.Execute(null);
        }

        private async void ResetTapped(object sender, EventArgs e)
        {
            var viewModel = BindingContext as SetPinViewModel;

            signature.Clear();
            viewModel.ResetCommand.Execute(null);
        }
    }
}
