﻿using DSNY.Novas.Common.Interfaces;
using Xamarin.Forms;

namespace DSNY.Novas.Forms
{
    public class XamarinFormsDependencyProvider : IDependencyProvider
    {
        public T Get<T>() where T : class
        {
            return DependencyService.Get<T>();
        }

        public void Register<T>() where T : class
        {
            DependencyService.Register<T>();
        }
    }
}
