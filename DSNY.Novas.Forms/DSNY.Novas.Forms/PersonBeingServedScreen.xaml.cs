﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonBeingServedScreen : BaseContentPage
    {
        public PersonBeingServedScreen()
        {
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Device.BeginInvokeOnMainThread(async () =>
            {
                await System.Threading.Tasks.Task.Delay(200);
                LicenseNumber.Focus();
            });
        }

        public void TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
                LicenseNumber.Unfocus();
        }
    }
}