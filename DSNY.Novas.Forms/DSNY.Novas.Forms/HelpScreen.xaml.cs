﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HelpScreen : BaseContentPage
	{
		public HelpScreen ()
		{
			InitializeComponent ();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            WebView.Source = WebView.Source;

        }

        void webOnNavigating (object sender, WebNavigatingEventArgs e)
        {
            //about:blank shows up after setting href value of location, so hits infinite loop if not accounted for
            if (e.Url.Contains("about:blank"))
            {
                return;
            }

            //Xamarin forms is not handling page anchors correctly so we have to use javascript
            var script = "location.href = \'#" + e.Url.Split('#').LastOrDefault() + "\'";
            WebView.Eval(script);
            e.Cancel = true;
        }
    }
}