﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StreetNameCheckScreen : BaseContentPage
    {
        public StreetNameCheckScreen()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            SearchField.Focus();
        }
    }
}