﻿using DSNY.Novas.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DSNY.Novas.Forms
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TimeoutScreen : BaseContentPage
	{
		public TimeoutScreen ()
		{
			InitializeComponent ();
		}

        private void PINTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length == 6)
            {
                //PINEnterPressed(sender, e);
            }
        }

        private void PINEnterPressed(object sender, EventArgs e)
        {
            var vm = BindingContext as TimeoutViewModel;
            vm?.PINEnteredCommand?.Execute(null);
        }
        private void ExitEnterPressed(object sender, EventArgs e)
        {
            var vm = BindingContext as TimeoutViewModel;
            vm?.ExitEnterPressed?.Execute(null);
        }
    }
}