﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels;
using Xamarin.Forms;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Services;

namespace DSNY.Novas.Forms
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Login_OnClicked(object sender, EventArgs e)
        {
            var loginScreen = new LoginScreen();
            loginScreen.BindingContext = new LoginViewModel();

            await Navigation.PushAsync(loginScreen);
        }

        private async void PinTool_OnClicked(object sender, EventArgs e)
        {
            var pinToolScreen = new SetPinView();
            pinToolScreen.BindingContext = new SetPinViewModel();

            await Navigation.PushAsync(pinToolScreen);
        }

        private async void PersonnelData_OnClicked(object sender, EventArgs e)
        {
            var personnelDataScreen = new PersonnelDataScreen();
            personnelDataScreen.BindingContext = new PersonnelDataViewModel()
            {
                UserSession = new UserSession()
                {
                    UserId = "dsny00065",
                    DutyHeader = new DutyHeader()
                    {
                        DeviceId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier,
                        LoginTimestamp = DateTime.Now
                    }
                }
            };

            await Navigation.PushAsync(personnelDataScreen);
        }

        private async void Start_OnClicked(object sender, EventArgs e)
        {
            var summaryScreen = new NovSummaryScreen();
            //NH-979
            //string hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //string deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdForHardware(hardwareId);
            string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            string deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);
            summaryScreen.BindingContext = new NovSummaryViewModel()
            {
                UserSession = new UserSession()
                {
                    UserId = "dsny00065",
                    AgencyId = "827",
                    Title = "SEA",
                    ReportLevel = "QNEA",
                    DefaultBoroCode = "1",
                    DutyHeader = new DutyHeader()
                    {
                        DeviceId = deviceId,
                        LoginTimestamp = DateTime.Now
                    }
                }
            };

            await Navigation.PushAsync(summaryScreen);
        }
        private async void Violation_OnClicked(object sender, EventArgs e)
        {
            var summaryScreen = new ViolationDetailsScreen();
            summaryScreen.BindingContext = new ViolationDetailsViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() { ViolationGroupId = 1, HHTIdentifier = "Littering - From Vehicle", ViolationCode = "S03" },
                    NovData = new List<NovData>(),
                    ViolationGroup = new ViolationGroups() { GroupName = "S", TypeName = "Action" }
                }
            };

            await Navigation.PushAsync(summaryScreen);
        }

        private async void Print_OnClicked(object sender, EventArgs e)
        {
            var print = new PrinterScreen();
            print.BindingContext = new PrinterViewModel()
            {
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation
                    {
                        UserId = "PO528381"
                    },
                    UserSession = new UserSession
                    {
                        SelectedPrinter = new Printer
                        {
                            SerialNumber = "MPA35141",
                            MacAddress = "0017ac1727fb"
                        }
                    }
                }
            };
            await Navigation.PushAsync(print);
        }

        private async void Timeout_OnClicked(object sender, EventArgs e)
        {
            var timeout = new TimeoutScreen();
            timeout.BindingContext = new TimeoutViewModel();
            await Navigation.PushAsync(timeout);
        }

        private async void Sync_OnClicked(object sender, EventArgs e)
        {
            var syncStatusScreen = new SyncStatusScreen();
            syncStatusScreen.BindingContext = new SyncStatusViewModel();

            await Navigation.PushModalAsync(syncStatusScreen);
        }
    }
}
