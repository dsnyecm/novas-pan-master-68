﻿using System;
using System.Windows.Input;
using System.Threading.Tasks;

using Pcsc;
using Pcsc.Common;
using Windows.Devices.SmartCards;
using Windows.Devices.Enumeration;

using DSNY.Novas.Forms.UWP;
using DSNY.Novas.Common.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(NFCReader))]
namespace DSNY.Novas.Forms.UWP
{
    class NFCReader : INFCReader
    {
        private DeviceInformation _deviceInfo;
        private SmartCardReader _cardReader;
        private string _currentCardUID;

        public ICommand CardRecognizedCommand { get; set; }
        public ICommand CardRemovedCommand { get; set; }

        public string CurrentCardUID => _currentCardUID;

        private async Task<DeviceInformation> GetDeviceInformation()
        {
            if (_deviceInfo != null)
            {
                return _deviceInfo;
            }

            _deviceInfo = await SmartCardReaderUtils.GetFirstSmartCardReaderInfo(SmartCardReaderKind.Nfc);

            if (_deviceInfo == null)
            {
                _deviceInfo = await SmartCardReaderUtils.GetFirstSmartCardReaderInfo(SmartCardReaderKind.Any);
            }

            return _deviceInfo;
        }

        public async Task<bool> IsNFCAvailable()
        {
            var deviceInfo = await GetDeviceInformation();
            return deviceInfo != null;
        }

        public async Task<bool> IsNFCEnabled()
        {
            var deviceInfo = await GetDeviceInformation();
            return deviceInfo?.IsEnabled ?? false;
        }

        public async Task<bool> NavigateToNFCSettings()
        {
            return await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-proximity:"));
        }

        public async Task<bool> StartListening()
        {
            if (_cardReader != null)
            {
                return true;
            }

            var deviceId = (await GetDeviceInformation())?.Id;
            if (deviceId == null)
            {
                return false;
            }

            _cardReader = await SmartCardReader.FromIdAsync(deviceId);
            if (_cardReader == null)
            {
                return false;
            }

            _cardReader.CardAdded += CardRecognizedHandler;
            _cardReader.CardRemoved += CardRemovedHandler;

            return true;
        }

        public void StopListening()
        {
            if (_cardReader != null)
            {
                _cardReader.CardAdded -= CardRecognizedHandler;
                _cardReader.CardRemoved -= CardRemovedHandler;
                _cardReader = null;
            }
        }

        private async void CardRecognizedHandler(SmartCardReader sender, CardAddedEventArgs args)
        {
            _currentCardUID = await GetUIDFromCard(args.SmartCard);

            CardRecognizedCommand?.Execute(_currentCardUID);
        }

        private void CardRemovedHandler(SmartCardReader sender, CardRemovedEventArgs args)
        {
            CardRemovedCommand?.Execute(null);
        }

        private async Task<string> GetUIDFromCard(SmartCard card)
        {
            string cardId = null;

            try
            {
                // Connect to the card
                using (SmartCardConnection connection = await card.ConnectAsync())
                {
                    // Try to identify what type of card it was
                    IccDetection cardIdentification = new IccDetection(card, connection);
                    await cardIdentification.DetectCardTypeAync();
                    //LogMessage("Connected to card\r\nPC/SC device class: " + cardIdentification.PcscDeviceClass.ToString());
                    //LogMessage("Card name: " + cardIdentification.PcscCardName.ToString());
                    //LogMessage("ATR: " + BitConverter.ToString(cardIdentification.Atr));

                    if ((cardIdentification.PcscDeviceClass == Pcsc.Common.DeviceClass.StorageClass) &&
                        (cardIdentification.PcscCardName == Pcsc.CardName.MifareUltralightC
                        || cardIdentification.PcscCardName == Pcsc.CardName.MifareUltralight
                        || cardIdentification.PcscCardName == Pcsc.CardName.MifareUltralightEV1))
                    {
                        // Handle MIFARE Ultralight
                        MifareUltralight.AccessHandler mifareULAccess = new MifareUltralight.AccessHandler(connection);

                        // Each read should get us 16 bytes/4 blocks, so doing
                        // 4 reads will get us all 64 bytes/16 blocks on the card
                        for (byte i = 0; i < 4; i++)
                        {
                            byte[] response = await mifareULAccess.ReadAsync((byte)(4 * i));
                            //LogMessage("Block " + (4 * i).ToString() + " to Block " + (4 * i + 3).ToString() + " " + BitConverter.ToString(response));
                        }

                        byte[] responseUid = await mifareULAccess.GetUidAsync();
                        //LogMessage("UID:  " + BitConverter.ToString(responseUid));
                        cardId = BitConverter.ToString(responseUid);
                    }
                    else if (cardIdentification.PcscDeviceClass == Pcsc.Common.DeviceClass.MifareDesfire)
                    {
                        // Handle MIFARE DESfire
                        Desfire.AccessHandler desfireAccess = new Desfire.AccessHandler(connection);
                        Desfire.CardDetails desfire = await desfireAccess.ReadCardDetailsAsync();

                        //LogMessage("DesFire Card Details:  " + Environment.NewLine + desfire.ToString());
                        cardId = BitConverter.ToString(desfire.UID);
                    }
                    else if (cardIdentification.PcscDeviceClass == Pcsc.Common.DeviceClass.StorageClass
                        && cardIdentification.PcscCardName == Pcsc.CardName.FeliCa)
                    {
                        // Handle Felica
                        //LogMessage("Felica card detected");
                        var felicaAccess = new Felica.AccessHandler(connection);
                        var uid = await felicaAccess.GetUidAsync();
                        //LogMessage("UID:  " + BitConverter.ToString(uid));
                        cardId = BitConverter.ToString(uid);
                    }
                    else if (cardIdentification.PcscDeviceClass == Pcsc.Common.DeviceClass.StorageClass
                        && (cardIdentification.PcscCardName == Pcsc.CardName.MifareStandard1K || cardIdentification.PcscCardName == Pcsc.CardName.MifareStandard4K))
                    {
                        // Handle MIFARE Standard/Classic
                        //LogMessage("MIFARE Standard/Classic card detected");
                        var mfStdAccess = new MifareStandard.AccessHandler(connection);
                        var uid = await mfStdAccess.GetUidAsync();
                        //LogMessage("UID:  " + BitConverter.ToString(uid));
                        cardId = BitConverter.ToString(uid);

                        /*
                        ushort maxAddress = 0;
                        switch (cardIdentification.PcscCardName)
                        {
                            case Pcsc.CardName.MifareStandard1K:
                                maxAddress = 0x3f;
                                break;
                            case Pcsc.CardName.MifareStandard4K:
                                maxAddress = 0xff;
                                break;
                        }
                        await mfStdAccess.LoadKeyAsync(MifareStandard.DefaultKeys.FactoryDefault);

                        for (ushort address = 0; address <= maxAddress; address++)
                        {
                            var response = await mfStdAccess.ReadAsync(address, Pcsc.GeneralAuthenticate.GeneralAuthenticateKeyType.MifareKeyA);
                            //LogMessage("Block " + address.ToString() + " " + BitConverter.ToString(response));
                        }
                        */
                    }
                    else if (cardIdentification.PcscDeviceClass == Pcsc.Common.DeviceClass.StorageClass
                        && (cardIdentification.PcscCardName == Pcsc.CardName.ICODE1 ||
                            cardIdentification.PcscCardName == Pcsc.CardName.ICODESLI ||
                            cardIdentification.PcscCardName == Pcsc.CardName.iCodeSL2))
                    {
                        // Handle ISO15693
                        //LogMessage("ISO15693 card detected");
                        var iso15693Access = new Iso15693.AccessHandler(connection);
                        var uid = await iso15693Access.GetUidAsync();
                        //LogMessage("UID:  " + BitConverter.ToString(uid));
                        cardId = BitConverter.ToString(uid);
                    }
                    else
                    {
                        // Unknown card type
                        // Note that when using the XDE emulator the card's ATR and type is not passed through, so we'll
                        // end up here even for known card types if using the XDE emulator

                        // Some cards might still let us query their UID with the PC/SC command, so let's try:
                        var apduRes = await connection.TransceiveAsync(new Pcsc.GetUid());
                        if (!apduRes.Succeeded)
                        {
                            //LogMessage("Failure getting UID of card, " + apduRes.ToString());
                        }
                        else
                        {
                            //LogMessage("UID:  " + BitConverter.ToString(apduRes.ResponseData));
                            cardId = BitConverter.ToString(apduRes.ResponseData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //LogMessage("Exception handling card: " + ex.ToString(), NotifyType.ErrorMessage);
            }

            return cardId;
        }
    }
}
