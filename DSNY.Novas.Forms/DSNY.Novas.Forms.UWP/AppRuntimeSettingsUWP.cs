﻿using System;
using System.IO;

using Windows.ApplicationModel;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Profile;

using DSNY.Novas.Common;
using DSNY.Novas.Forms.UWP;
using Windows.Networking.NetworkOperators;
using Windows.System;

[assembly: Xamarin.Forms.Dependency(typeof(AppRuntimeSettingsUWP))]
namespace DSNY.Novas.Forms.UWP
{
    public class AppRuntimeSettingsUWP : AppRuntimeSettingsBase
    {
        public override string GetSqlitePath()
        {
            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, DatabaseFilename);
            return path;
        }

        public override string HardwareIdentifier
        {
            get
            {
                var hardwareId = string.Empty;
#if TEST
                return "TestDevice";
#else
                try
                {
                    //HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
                    //IBuffer tokenIdBuffer = token.Id;
                    //byte[] bytes = new byte[tokenIdBuffer.Length];

                    //using (var dataReader = DataReader.FromBuffer(tokenIdBuffer))
                    //    dataReader.ReadBytes(bytes);

                    //// Use the MD5 hash of the token id to create a shorter value
                    //HashAlgorithmProvider alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
                    //IBuffer hashed = alg.HashData(CryptographicBuffer.CreateFromByteArray(bytes));
                    //hardwareId = CryptographicBuffer.EncodeToHexString(hashed);

                    var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
                    hardwareId = deviceInfo.FriendlyName;
                }
                catch (Exception exc)
                {
                    //TODO: Log error
                    MessageCenter.Send("Can not determine Device Id");
                }

                return hardwareId;
#endif
            }
        }

        public override string Version
        {
            get
            {
                var packageVersion = Package.Current.Id.Version;
                return string.Format("{0}.{1}.{2}.{3}", packageVersion.Major, packageVersion.Minor, packageVersion.Build, packageVersion.Revision);
            }
        }

        //public override string DeviceHardwareToken
        //{
        //    get
        //    {
        //        HardwareToken token = Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);
        //        IBuffer hardwareId = token.Id;
        //        var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

        //        byte[] bytes = new byte[hardwareId.Length];
        //        dataReader.ReadBytes(bytes);

        //        return BitConverter.ToString(bytes);
        //    }
        //}

        public override string HHTNumber
        {
            get
            {
                var hardwareId = string.Empty;
                var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
                hardwareId = deviceInfo.FriendlyName;
                return hardwareId;
            }
        }
        public override string PlatformDeviceName
        {
            get
            {
                var hardwareId = string.Empty;
                var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
                hardwareId = deviceInfo.FriendlyName;
                return hardwareId;
            }
        }

        public override string DeviceIMEI
        {
            get
            {
                var modem = MobileBroadbandModem.GetDefault();
                
                var imei = modem?.DeviceInformation.MobileEquipmentId;
                return imei;
            }
        }

        public override string DeviceSerialNumber
        {
            get
            {
                var modem = MobileBroadbandModem.GetDefault();
                var serialNum = modem?.DeviceInformation.SerialNumber;
                return serialNum;
            }
        }

        public override string DeviceMemory
        {
            get
            {
                ulong limit = MemoryManager.AppMemoryUsageLimit;
                ulong usage = MemoryManager.AppMemoryUsage;
                ulong headroom = limit - usage;

                return "";
            }
        }

    }
}
