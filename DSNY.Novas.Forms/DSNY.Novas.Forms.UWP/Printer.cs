﻿
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Forms.UWP;
using Honeywell.Connection;
using Honeywell.Printer;
using Honeywell.Printer.Configuration.DPL;
using Honeywell.Printer.Configuration.ExPCL;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Controls;

[assembly: Xamarin.Forms.Dependency(typeof(DSNY.Novas.Forms.UWP.Printer))]
namespace DSNY.Novas.Forms.UWP
{
    public class Printer : IPrinter
    {
        public const string SHERIFF_DEP = "Sheriff's Department";
        private int PRN_PRINT_STOP = 1482;

        private string _serialNumber;
        private string _macAddress;
        private ConnectionBase _connection;
        private byte[] _jobData;

        public async Task<List<string>> GetPairedDevices()
        {
            var printerList = new List<string>();
            printerList = await Connection_Bluetooth.getPairedDevices();
            return printerList;
        }

        public async Task<bool> InitiateConnection(string macAddress, bool testPrint, Dictionary<string, string> MapFieldNamesToValues, string summonType)
        {
            try
            {
                if (summonType == null || summonType == "")
                    summonType = "Department of Sanitation";
                Connection = (Connection_Bluetooth)Connection_Bluetooth.CreateClient(macAddress, false);
                //Initialize connection
                await Connection.Init();
                if (testPrint)
                {
                    switch (summonType)
                    {
                        case SHERIFF_DEP:
                            {
                                JobData = await TestPrintLogoDynamicForSheriff(MapFieldNamesToValues);
                                break;
                            }
                        default:
                            {
                                //var signatureBitmap = NovMaster.UserSession.DutyHeader.SignatureBitmap;
                                //JobData = await TestPrintLogo(MapFieldNamesToValues);

                                JobData = await TestPrintLogoDynamic(MapFieldNamesToValues);

                                //JobData =  TestPrint().GetDocumentData();
                                break;
                            }
                    }
                }
                else
                {
                    switch (summonType)
                    {
                        case SHERIFF_DEP:
                            {
                                JobData = await PrintSheriffWithLogo(MapFieldNamesToValues);
                                break;
                            }
                        default:
                            {
                                //JobData = await OpenPrnFile(MapFieldNamesToValues);
                                JobData = await PrintTicketWithLogo(MapFieldNamesToValues);
                                break;
                            }
                    }
                }
                var printSuccess = await StartPrintJob();
                return printSuccess;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public async Task<bool> InitiateConnectionForPrintReport(string macAddress, Dictionary<string, string> MapFieldNamesToValuesForPrintReport, string summonType)
        {
            try
            {
                Connection = (Connection_Bluetooth)Connection_Bluetooth.CreateClient(macAddress, false);
                //Initialize connection
                await Connection.Init();

                JobData = await PrintNovSummaryReport(MapFieldNamesToValuesForPrintReport);

                var printSuccess = await StartPrintJob();
                return printSuccess;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public async Task<bool> StartPrintJob()
        {

            var connectionSuccess = true;
            //Opening Connection to the Printer
            try
            {
                if (Connection.Open())
                {
                    Connection.Close();
                }
                Connection.Open();
            }
            catch (AggregateException e)
            {
                foreach (var innerException in e.Flatten().InnerExceptions)
                {
                    // handle error
                    connectionSuccess = false;
                    ConnectionOpenSuccessCommand?.Execute(false);
                }
                if (Connection.Open())
                {
                    Connection.Close();
                }
            }
            if (!connectionSuccess)
            {
                return connectionSuccess;
            }

            ConnectionOpenSuccessCommand?.Execute(true);
            int retryCount = 0;
            var currentStatus = GetStatus();
            //Wait for printer to finish printing before closing connection
            while (retryCount < 5)
            {
                //Check if printer is ready
                if (GetStatus() == PrinterStatus_DPL.PrinterStatus.PrinterReady)
                {
                    currentStatus = GetStatus();
                    break;
                }
                retryCount++;
            }

            try
            {
                //Sending Data to the printer
                uint bytesWritten = 0;
                uint bytesToWrite = 1024;
                uint totalBytes = (uint)JobData.Length;
                uint remainingBytes = totalBytes;
                while (bytesWritten < totalBytes)
                {
                    if (remainingBytes < bytesToWrite)
                        bytesToWrite = remainingBytes;

                    Connection.Write(JobData, (int)bytesWritten, (int)bytesToWrite);
                    bytesWritten += bytesToWrite;
                    remainingBytes = remainingBytes - bytesToWrite;
                    new ManualResetEvent(false).WaitOne(100);
                }
                SendCommandSuccess?.Execute(true);
                //=============Check status of printer==========//
                await Task.Delay(2000);
                //Retrieve current status

                retryCount = 0;
                //Wait for printer to finish printing before closing connection
                while (retryCount < 5)
                {

                    //Check if printer is ready
                    GetStatus();
                    if (currentStatus != PrinterStatus_DPL.PrinterStatus.Unknown)
                        break;

                    retryCount++;
                }

                //==============Close connection =====================//
                Connection.Close();
                ConnectionClosedSuccessCommand?.Execute(true);
                return true;
            }
            catch (Exception ex)
            {
                if (Connection.Open())
                {
                    Connection.Close();
                }
            }
            return false;
        }
        private PrinterStatus_DPL.PrinterStatus GetStatus()
        {
            var currentStatus = PrinterStatus_DPL.PrinterStatus.Unknown;
            currentStatus = PrinterStatus_DPL.PrinterStatus.PrinterReady;

            return currentStatus;
        }

        private DocumentEZ TestPrint()
        {
            //var docEZ1 = new DocumentEZ("MF204");
            var docEZ = new DocumentEZ("MF204");
            docEZ.WriteText("Test Print", 1, 300);
            docEZ.WriteText("Test Print", 50, 1);
            docEZ.WriteText("Test Print", 75, 1);
            docEZ.WriteText("Test Print", 100, 1);
            docEZ.WriteText("Test Print", 125, 1);
            docEZ.WriteText("Test Print", 150, 1);
            // docEZ.WriteText("  ", 580, 1);
            //docEZ.WriteImage()



            return docEZ;
        }

        private async Task<byte[]> TestPrintFullSizeTicket()
        {
            //var uri = new Uri("ms-appx:///UniversalSummon/UniversalSummon.txt");
            try
            {
                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/logo1.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/TestPrint.txt"));
                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];

                ibuff.CopyTo(jobData);

                string str = "Hello printer";
                return jobData;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }


            return null;
        }

        private async Task<byte[]> TestPrintLogoDynamic(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/logo3.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                var signatureDoc = await CreateSignatureDocumentLP(MapFieldNamesToValues);

                // End building Signatue Image object

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/UniversalSummon.txt"));

                int summeryOfLawPosition = 3240;

                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);

                string valFound = "";
                var replacedNov = re.Replace(jobDataString,
                    match => (MapFieldNamesToValues.TryGetValue(match.Value, out valFound)) ? (MapFieldNamesToValues[match.Groups[0].Value]) : "xxxxxxxxxx");

                var novSections = replacedNov.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var secondSection = novSections[1];
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var secondSectionUpdated = verticalOffset.Replace(secondSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 230).ToString() + ",");
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - PRN_PRINT_STOP).ToString() + ",");

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(secondSectionUpdated)).ToArray();
                jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();

                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private async Task<byte[]> TestPrintLogo(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/logo3.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                //building Signature Image object
                var signatureDoc = await CreateSignatureDocumentLP(MapFieldNamesToValues);

                // End building Signatue Image object

                //var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/SignaturePrintTest.txt"));
                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/TestPrint.txt"));
                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];

                ibuff.CopyTo(jobData);

                var jobDataString = Encoding.UTF8.GetString(jobData);

                var novSections = jobDataString.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var secondSection = novSections[1];
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);

                var secondSectionUpdated = verticalOffset.Replace(secondSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 230).ToString() + ",");
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 1490).ToString() + ",");

                // comment the following because duplicate section is printed
                //jobData = Encoding.UTF8.GetBytes(firstSection);
                //jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                //jobData = jobData.Concat(Encoding.UTF8.GetBytes(secondSectionUpdated)).ToArray();
                //jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                //jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();
                return jobData;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }


            return null;
        }

        private async Task<byte[]> PrintTicketWithLogo(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/logo3.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                //building Signature Image object
                var signatureDoc = await CreateSignatureDocumentLP(MapFieldNamesToValues);

                // End building Signatue Image object

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/UniversalSummon.txt"));

                int summeryOfLawPosition = 3240;

                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);
                var replacedNov = re.Replace(jobDataString, match => MapFieldNamesToValues[match.Groups[0].Value]);

                var novSections = replacedNov.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var secondSection = novSections[1];
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var secondSectionUpdated = verticalOffset.Replace(secondSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 230).ToString() + ",");
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - PRN_PRINT_STOP).ToString() + ",");

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(secondSectionUpdated)).ToArray();
                jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();

                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private async Task<byte[]> PrintNovSummaryReport(Dictionary<string, string> MapFieldNamesToValuesForPrintReport)
        {
            try
            {
                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/logo3.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/PrintReport.txt"));
                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);
                var replacedNov = re.Replace(jobDataString, match => MapFieldNamesToValuesForPrintReport[match.Groups[0].Value]);

                var novSections = replacedNov.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 230).ToString() + ",");

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();

                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private async Task<DocumentLP> CreateSignatureDocumentLP(Dictionary<string, string> MapFieldNamesToValues)
        {
            var signatureDoc = new DocumentLP("A");
            if (MapFieldNamesToValues.TryGetValue("<<Signature>>", out var signatureData))
            {
                var signatureImage = Convert.FromBase64String(signatureData);
                var memoryStream = new MemoryStream(signatureImage).AsRandomAccessStream();

                var bitmapImage = new WriteableBitmap(1, 1);
                await bitmapImage.SetSourceAsync(memoryStream);

                int imgHeadWidth = 832;
                signatureDoc.WriteImage(bitmapImage, imgHeadWidth);
            }
            return signatureDoc;
        }

        /**
         * Sheriff
         * */
        private async Task<Dictionary<string, string>> FillSheriffStubData(Dictionary<string, string> MapFieldNamesToValues)
        {
            // TO-DO: replace the hardcode value by dynamic value
            // fields exists in Sheriff only
            MapFieldNamesToValues.Add("<<CellPhone>>", "347-322-7170");
            MapFieldNamesToValues.Add("<<OathCode>>", "BA1");
            //                MapFieldNamesToValues.Add("<<TaxRegistryNumber>>", "642741");
            MapFieldNamesToValues.Add("<<IsAlternativeServiceChecked>>", "x");
            MapFieldNamesToValues.Add("<<IsOffenseSeekingRevocationChecked>>", "x");

            // common fields
            if (MapFieldNamesToValues.ContainsKey("<<NovNumber>>"))
            {
                MapFieldNamesToValues.Remove("<<NovNumber>>");
                MapFieldNamesToValues.Add("<<NovNumber>>", "0196761272");
            }
            if (MapFieldNamesToValues.ContainsKey("<<Resp1FirstName>>"))
            {
                MapFieldNamesToValues.Remove("<<Resp1LastName>>");
                MapFieldNamesToValues.Add("<<Resp1LastName>>", "Ali" + ",  ");
                MapFieldNamesToValues.Remove("<<Resp1FirstName>>");
                MapFieldNamesToValues.Add("<<Resp1FirstName>>", "Hazem" + "  ");
            }
            if (MapFieldNamesToValues.ContainsKey("<<Resp1Address>>"))
            {
                MapFieldNamesToValues.Remove("<<Resp1Address>>");
                MapFieldNamesToValues.Add("<<Resp1Address>>", "PARK AVENUE");
                MapFieldNamesToValues.Remove("<<Resp1HouseNo>>");
                MapFieldNamesToValues.Add("<<Resp1HouseNo>>", "123");
                MapFieldNamesToValues.Remove("<<Resp1Apt>>");
                MapFieldNamesToValues.Add("<<Resp1Apt>>", ", Apt " + "3" + ",  ");
                MapFieldNamesToValues.Remove("<<Resp1City>>");
                MapFieldNamesToValues.Add("<<Resp1City>>", "BROOKLYN" + ",  ");
                MapFieldNamesToValues.Remove("<<Resp1State>>");
                MapFieldNamesToValues.Add("<<Resp1State>>", "NY");
                MapFieldNamesToValues.Remove("<<Resp1Zip>>");
                MapFieldNamesToValues.Add("<<Resp1Zip>>", "11205");
            }
            if (MapFieldNamesToValues.ContainsKey("<<LicenseNumber>>"))
            {
                MapFieldNamesToValues.Remove("<<LicenseNumber>>");
                MapFieldNamesToValues.Add("<<LicenseNumber>>", "490498145");
            }
            //if (MapFieldNamesToValues.ContainsKey("<<IsAppearInHearingChecked>>"))
            //{
            //    MapFieldNamesToValues.Remove("<<IsAppearInHearingChecked>>");
            //    MapFieldNamesToValues.Add("<<IsAppearInHearingChecked>>", "x");
            //}
            if (MapFieldNamesToValues.TryGetValue("<<Borough>>", out string BoroughTwoDigitCode))
            {
                MapFieldNamesToValues.Remove("<<Borough>>");
                MapFieldNamesToValues.Add("<<Borough>>", await GetBoroughByTwoDigitCode("BK"));
                MapFieldNamesToValues.Remove("<<CBNO>>");
                MapFieldNamesToValues.Add("<<CBNO>>", "88");
                MapFieldNamesToValues.Remove("<<PlaceAddressDescriptor>>");
                MapFieldNamesToValues.Add("<<PlaceAddressDescriptor>>", "");
                MapFieldNamesToValues.Remove("<<PlaceBBL>>");
                MapFieldNamesToValues.Add("<<PlaceBBL>>", "");
                MapFieldNamesToValues.Remove("<<DisplayAddress>>");
                MapFieldNamesToValues.Add("<<DisplayAddress>>", "123 PARK AVENUE");
            }
            if (MapFieldNamesToValues.ContainsKey("<<Comments>>"))
            {
                MapFieldNamesToValues.Remove("<<Comments>>");
                MapFieldNamesToValues.Add("<<Comments>>", "Manhattan");
            }
            if (MapFieldNamesToValues.ContainsKey("<<LawSection>>"))
            {
                MapFieldNamesToValues.Remove("<<LawSection>>");
                MapFieldNamesToValues.Add("<<LawSection>>", "17-703");
            }
            if (MapFieldNamesToValues.ContainsKey("<<ViolationScript1>>"))
            {
                MapFieldNamesToValues.Remove("<<ViolationScript1>>");
                MapFieldNamesToValues.Add("<<ViolationScript1>>", "At T/P/O I did observe the respondent without the proper cigarette tail license,");
                MapFieldNamesToValues.Remove("<<ViolationScript2>>");
                MapFieldNamesToValues.Add("<<ViolationScript2>>", "which is in violation of AD CODE 17-703");
            }
            if (MapFieldNamesToValues.ContainsKey("<<PropertyType>>"))
            {
                MapFieldNamesToValues.Remove("<<PropertyType>>");
                MapFieldNamesToValues.Add("<<PropertyType>>", "Property Removed");
            }
            if (MapFieldNamesToValues.ContainsKey("<<Title>>"))
            {
                MapFieldNamesToValues.Remove("<<Title>>");
                MapFieldNamesToValues.Add("<<Title>>", "DETECTIVE");
                MapFieldNamesToValues.Remove("<<ReportLevel>>");
                MapFieldNamesToValues.Add("<<ReportLevel>>", "BCI");
                MapFieldNamesToValues.Remove("<<AgencyId>>");
                MapFieldNamesToValues.Add("<<AgencyId>>", "837");
                MapFieldNamesToValues.Remove("<<AbbrevName>>");
                MapFieldNamesToValues.Add("<<AbbrevName>>", "642741");
            }
            return MapFieldNamesToValues;
        }
        private async Task<byte[]> PrintSheriffWithLogo(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                //MapFieldNamesToValues = await FillSheriffStubData(MapFieldNamesToValues);

                // fields exists in Sheriff only
                if (!MapFieldNamesToValues.ContainsKey("<<CellPhone>>"))
                {
                    MapFieldNamesToValues.Add("<<CellPhone>>", "");
                }
                if (!MapFieldNamesToValues.ContainsKey("<<OathCode>>"))
                {
                    MapFieldNamesToValues.Add("<<OathCode>>", "");
                }
                if (!MapFieldNamesToValues.ContainsKey("<<IsAlternativeServiceChecked>>"))
                {
                    MapFieldNamesToValues.Add("<<IsAlternativeServiceChecked>>", " ");
                }
                //if (!MapFieldNamesToValues.ContainsKey("<<IsAppearInHearingChecked>>"))
                //{
                //    MapFieldNamesToValues.Add("<<IsAppearInHearingChecked>>", " ");
                //}
                //else
                //{
                //    MapFieldNamesToValues.Remove("<<IsAppearInHearingChecked>>");
                //    MapFieldNamesToValues.Add("<<IsAppearInHearingChecked>>", "x");
                //}
                if (!MapFieldNamesToValues.ContainsKey("<<IsOffenseSeekingRevocationChecked>>"))
                {
                    // MapFieldNamesToValues.Add("<<IsOffenseSeekingRevocationChecked>>", "x");
                    MapFieldNamesToValues.Add("<<IsOffenseSeekingRevocationChecked>>", " ");
                }

                // Offical code starts -->

                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/Sheriff-logo-text.png"));
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                //building Signature Image object
                var signatureDoc = await CreateSignatureDocumentLP(MapFieldNamesToValues);

                // End building Signatue Image object

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/Sheriff-UniversalSummon.txt"));

                int summeryOfLawPosition = 3240;

                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);
                var replacedNov = re.Replace(jobDataString, match => MapFieldNamesToValues[match.Groups[0].Value]);

                var novSections = replacedNov.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var secondSection = novSections[1];
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var secondSectionUpdated = verticalOffset.Replace(secondSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - 230 + 50).ToString() + ",");
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - PRN_PRINT_STOP).ToString() + ",");

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(secondSectionUpdated)).ToArray();
                jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();

                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        private async Task<byte[]> TestPrintLogoDynamicForSheriff(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                var SHERIFF_DIVISION_OFFSET = 230 + 50;
                var LAST_SECTION_OFFSET = PRN_PRINT_STOP;
                var SUMMARY_OF_LAW_OFFSET = 3240;
                var LOGO_URI = new Uri("ms-appx:///UniversalSummon/Sheriff-logo-text.png");
                var PRN_URI = new Uri("ms-appx:///UniversalSummon/Sheriff-UniversalSummon.txt");

                if (!MapFieldNamesToValues.ContainsKey("<<IsAlternativeServiceChecked>>"))
                {
                    MapFieldNamesToValues.Add("<<IsAlternativeServiceChecked>>", " ");
                }
                if (!MapFieldNamesToValues.ContainsKey("<<IsAppearInHearingChecked>>"))
                {
                    MapFieldNamesToValues.Add("<<IsAppearInHearingChecked>>", " ");
                }
                else
                {
                    MapFieldNamesToValues.Remove("<<IsAppearInHearingChecked>>");
                    MapFieldNamesToValues.Add("<<IsAppearInHearingChecked>>", " ");
                }
                if (!MapFieldNamesToValues.ContainsKey("<<IsOffenseSeekingRevocationChecked>>"))
                {
                    MapFieldNamesToValues.Add("<<IsOffenseSeekingRevocationChecked>>", " ");
                }

                var storageFileLogo = await StorageFile.GetFileFromApplicationUriAsync(LOGO_URI);
                IBuffer ibuffLogo = await FileIO.ReadBufferAsync(storageFileLogo);
                var jobDataLogo = new byte[ibuffLogo.Length];

                ibuffLogo.CopyTo(jobDataLogo);

                //building logo Image object
                var logoDoc = new DocumentLP("A");

                var bitmapLogoImage = new WriteableBitmap(1, 1);
                var memoryStreamLogo = new MemoryStream(jobDataLogo).AsRandomAccessStream();
                await bitmapLogoImage.SetSourceAsync(memoryStreamLogo);
                logoDoc.WriteImage(bitmapLogoImage, 832);

                // End building logo Image object

                var signatureDoc = await CreateSignatureDocumentLP(MapFieldNamesToValues);

                // End building Signatue Image object

                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(PRN_URI);

                int summeryOfLawPosition = SUMMARY_OF_LAW_OFFSET;

                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);

                string valFound = "";
                var replacedNov = re.Replace(jobDataString,
                    match => (MapFieldNamesToValues.TryGetValue(match.Value, out valFound)) ? (MapFieldNamesToValues[match.Groups[0].Value]) : "xxxxxxxxxx");

                var novSections = replacedNov.Split(new string[] { "--logo--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var secondSection = novSections[1];
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var secondSectionUpdated = verticalOffset.Replace(secondSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - SHERIFF_DIVISION_OFFSET).ToString() + ",");
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - LAST_SECTION_OFFSET).ToString() + ",");

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(logoDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(secondSectionUpdated)).ToArray();
                jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();
                var ss=Encoding.UTF8.GetString(jobData);

              // var  my_string = Encoding.Unicode.GetString(jobData, 0, jobData.Length);
                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        private async Task<string> GetBoroughByTwoDigitCode(string BoroughTwoDigitCode)
        {
            Dictionary<string, string> boroMap = new Dictionary<string, string>();
            boroMap.Add("MN", "Manhattan");
            boroMap.Add("BX", "Bronx");
            boroMap.Add("BK", "Brooklyn");
            boroMap.Add("QN", "Queens");
            boroMap.Add("SI", "Staten Island");
            string result = boroMap.TryGetValue(BoroughTwoDigitCode, out string Borough) ? Borough : "";
            return result;
        }

        private async Task<byte[]> OpenPrnFile(Dictionary<string, string> MapFieldNamesToValues)
        {
            try
            {
                var uri = new Uri("ms-appx:///UniversalSummon/UniversalSummon.txt");
                var storageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UniversalSummon/UniversalSummon.txt"));
                IBuffer ibuff = await FileIO.ReadBufferAsync(storageFile);
                var jobData = new byte[ibuff.Length];
                ibuff.CopyTo(jobData);
                var jobDataString = Encoding.UTF8.GetString(jobData);
                Regex re = new Regex(@"<<(\w+)>>", RegexOptions.Compiled);
                var replacedNov = re.Replace(jobDataString, match => MapFieldNamesToValues[match.Groups[0].Value]);

                var novSections = replacedNov.Split(new string[] { "--Signature--" }, StringSplitOptions.None);
                var firstSection = novSections.First();
                var lastSection = novSections.Last();

                Regex verticalOffset = new Regex(@"@(\d+),", RegexOptions.Compiled);
                var lastSectionUpdated = verticalOffset.Replace(lastSection, match => "@" + (Convert.ToInt32(match.Groups[1].Value) - PRN_PRINT_STOP).ToString() + ",");

                var signatureDoc = new DocumentLP("A");

                var bitmapImage = new WriteableBitmap(1, 1);


                if (MapFieldNamesToValues.TryGetValue("<<Signature>>", out var signatureData))
                {
                    try
                    {

                        var signatureImage = Convert.FromBase64String(signatureData);
                        var memoryStream = new MemoryStream(signatureImage).AsRandomAccessStream();

                        //var bitmapDecoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(new MemoryStream(signatureImage).AsRandomAccessStream());


                        await bitmapImage.SetSourceAsync(memoryStream);
                    }
                    catch (Exception e)
                    {
                        // Image was in an invalid format, or the data passed in was not a valid image
                    }
                }

                // Use this in case you need to determine the height and width of the image
                //var bitmapDecoder = await Windows.Graphics.Imaging.BitmapDecoder.CreateAsync(new MemoryStream(signatureImage).AsRandomAccessStream());


                signatureDoc.WriteImage(bitmapImage, 832);

                jobData = Encoding.UTF8.GetBytes(firstSection);
                jobData = jobData.Concat(signatureDoc.GetDocumentData()).ToArray();
                jobData = jobData.Concat(Encoding.UTF8.GetBytes(lastSectionUpdated)).ToArray();

                return jobData;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public string SerialNumber
        {
            get => _serialNumber;
            set { _serialNumber = value; }
        }

        public string MacAddress
        {
            get => _macAddress;
            set { _macAddress = value; }
        }

        public string GetSerialNumber()
        {
            return SerialNumber;
        }

        public string GetMacAddress()
        {
            return MacAddress;
        }

        public ConnectionBase Connection
        {
            get => _connection;
            set { _connection = value; }
        }

        public byte[] JobData
        {
            get => _jobData;
            set { _jobData = value; }
        }

        public void SetConnectionOpenSuccessCommand(ICommand connectionOpenSuccessCommand)
        {
            ConnectionOpenSuccessCommand = connectionOpenSuccessCommand;
        }

        public void SetConnectionClosedSuccessCommand(ICommand connectionClosedSuccessCommand)
        {
            ConnectionClosedSuccessCommand = connectionClosedSuccessCommand;
        }

        public void SetSendCommandSuccess(ICommand sendCommandSuccess)
        {
            SendCommandSuccess = sendCommandSuccess;
        }
        public ICommand ConnectionOpenSuccessCommand { get; set; }
        public ICommand ConnectionClosedSuccessCommand { get; set; }
        public ICommand SendCommandSuccess { get; private set; }
    }
}
