﻿using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Forms.UWP;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Devices.Enumeration;
using Windows.Devices.PointOfService;
using Windows.Storage.Streams;
using Windows.UI.Core;

[assembly: Xamarin.Forms.Dependency(typeof(DSNY.Novas.Forms.UWP.BarcodeScanner))]
namespace DSNY.Novas.Forms.UWP
{
    public class BarcodeScanner : IBarcodeScanner
    {
        private Windows.Devices.PointOfService.BarcodeScanner _scanner;
        public async Task<T> GetFirstDeviceAsync<T>(string selector, Func<string, Task<T>> convertAsync)
            where T : class
        {
            var completionSource = new TaskCompletionSource<T>();
            var pendingTasks = new List<Task>();
            DeviceWatcher watcher = DeviceInformation.CreateWatcher(selector);

            watcher.Added += (DeviceWatcher sender, DeviceInformation device) =>
            {
                Func<string, Task> lambda = async (id) =>
                {
                    T t = await convertAsync(id);
                    if (t != null)
                    {
                        completionSource.TrySetResult(t);
                    }
                };
                pendingTasks.Add(lambda(device.Id));
            };

            watcher.EnumerationCompleted += async (DeviceWatcher sender, object args) =>
            {
                // Wait for completion of all the tasks we created in our "Added" event handler.
                await Task.WhenAll(pendingTasks);
                // This sets the result to "null" if no task was able to produce a device.
                completionSource.TrySetResult(null);
            };

            watcher.Start();

            // Wait for enumeration to complete or for a device to be found.
            T result = await completionSource.Task;

            watcher.Stop();

            return result;
        }

        public async Task<bool> IsScannerAvailable()
        {
            Scanner = await GetFirstDeviceAsync(Windows.Devices.PointOfService.BarcodeScanner.GetDeviceSelector(), async (id) => await Windows.Devices.PointOfService.BarcodeScanner.FromIdAsync(id));
            if (Scanner == null)
            {
                return false;
            }
            return true;
        }

        public async Task InitiateScan()
        {
            if (Scanner == null)
            {
                if (!await IsScannerAvailable())
                {
                    //TODO: Log error
                    return;
                }
            }

            var claimedScanner = await Scanner.ClaimScannerAsync();
            if (claimedScanner != null)
            {
                claimedScanner.ReleaseDeviceRequested += ClaimedDevice_ReleaseDeviceRequested;
                claimedScanner.DataReceived += ClaimedScanner_DataReceived;
                claimedScanner.IsDecodeDataEnabled = true;
                await claimedScanner.EnableAsync();
            }
        }

        public void ClaimedDevice_ReleaseDeviceRequested(object sender, ClaimedBarcodeScanner e)
        {
            e.RetainDevice();
        }

        public void ClaimedScanner_DataReceived(ClaimedBarcodeScanner sender, BarcodeScannerDataReceivedEventArgs args)
        {
            // read the data from the buffer and convert to string.
            var scanDataLabelReader = DataReader.FromBuffer(args.Report.ScanDataLabel);
            var str = scanDataLabelReader.ReadString(args.Report.ScanDataLabel.Length);
            DataRecievedCommand?.Execute(str);
        }

        public void SetDataRecievedCommand(ICommand dataRecievedCommand)
        {
            DataRecievedCommand = dataRecievedCommand; 
        }

        public ICommand DataRecievedCommand { get; set; }

        public Windows.Devices.PointOfService.BarcodeScanner Scanner
        {
            get => _scanner;
            set { _scanner = value; }
        }
    }
}
