﻿using DSNY.Novas.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Devices.Bluetooth;
using System.Windows.Input;
using DSNY.Novas.Services;
using DSNY.Novas.Common;
using DSNY.Novas.ViewModels.Utils;

[assembly: Xamarin.Forms.Dependency(typeof(DSNY.Novas.Forms.UWP.BluetoothReader))]
namespace DSNY.Novas.Forms.UWP
{

    public class BluetoothReader : IBluetoothReader
    {
        private List<Printer> _printerList;
        private List<Printer> _printerListPaired;

        private string _scanResult;
        private DeviceWatcher _deviceWatcher;
        private TypedEventHandler<DeviceWatcher, DeviceInformation> _handlerAdded = null;
        private TypedEventHandler<DeviceWatcher, DeviceInformationUpdate> _handlerRemoved = null;
        private readonly IDeviceService _deviceService;

        public BluetoothReader()
        {
            _deviceService = DependencyResolver.Get<IDeviceService>();
        }

        public IEnumerable<Models.Printer> Printers { get; set; }
        public async Task<List<Models.Printer>> ListAllPrinters()
        {
            //Gets all unpaired Bluetooth devices
            var unpairedDevices = BluetoothDevice.GetDeviceSelectorFromPairingState(false);

            //Gets all paired Bluetooth devices
            var pairedDevices = BluetoothDevice.GetDeviceSelectorFromPairingState(true);

            var unpairedNearDevices = (await DeviceInformation.FindAllAsync(unpairedDevices)).Select(x => x.Name).ToList();

            // run second time - first time devices have "" as a name. Running this a second time fixes that API issue.
            if (unpairedNearDevices.Contains(""))
            {
                unpairedNearDevices = (await DeviceInformation.FindAllAsync(unpairedDevices)).Select(x => x.Name).ToList();
            }            

            var pairedNearDevices = (await DeviceInformation.FindAllAsync(pairedDevices)).Select(x => x.Name).ToList();
            var nearbyDevices = unpairedNearDevices.Union(pairedNearDevices).ToList();

            var printers = (await _deviceService.GetAllPrinters()).FindAll(p => p.DeviceName.ToLower().StartsWith("mpa"));
            var inRangePrinters = printers.FindAll(pd => nearbyDevices.Contains(pd.SerialNumber));

            var retVal = inRangePrinters
                .Select(pd => new Models.Printer
                {
                    SerialNumber = pd.SerialNumber,
                    MacAddress = pd.MacAddress,
                    IsInRange = true
                })
                .OrderBy(p => p.SerialNumber)
                .ToList();

            retVal.Add(new Models.Printer { SerialNumber = "--", MacAddress = "--" });

            retVal.AddRange(printers.Except(inRangePrinters)
                .Select(pd => new Models.Printer
                {
                    SerialNumber = pd.SerialNumber,
                    MacAddress = pd.MacAddress
                })
                .OrderBy(p => p.SerialNumber)
                .ToList());

            Printers = retVal.FindAll(p => !string.IsNullOrWhiteSpace(p.MacAddress));
            return Printers.ToList();
        }

        public void FindDevices(string scanResult)
        {
            string selector = "System.Devices.Aep.ProtocolId:=\"{e0cbf06c-cd8b-4647-bb8a-263b43f0f974}\"";
            
            DeviceWatcher = DeviceInformation.CreateWatcher(selector, null, DeviceInformationKind.AssociationEndpoint);
            if (PrinterList?.Count == 0 || PrinterList == null)
            {
                PrinterList = new List<Printer>();
                ScanResult = scanResult;
            }

            HandlerAdded = async (watcher, deviceInfo) =>
            {
                PrinterList.Add(new Printer
                {
                    SerialNumber = deviceInfo.Name,
                    MacAddress = deviceInfo.Id.Split('-')[1]
                });

                if (deviceInfo.Name != ScanResult)
                    return;

                var printer = new Printer
                {
                    SerialNumber = deviceInfo.Name,
                    MacAddress = deviceInfo.Id.Split('-')[1]
                };
                    
                if (!(await deviceInfo.Pairing.PairAsync()).Status.ToString().In(new[] { "AlreadyPaired", "Paired" }))
                    PairSuccessfulCommand?.Execute(printer);
                else
                    PairSuccessfulCommand?.Execute(false);
            };

            DeviceWatcher.Added += HandlerAdded;

            HandlerRemoved = (watcher, deviceInfoUpdate) =>
            {
                // Find the corresponding DeviceInformation in the collection and remove it
                PrinterList?.RemoveAll(p => p.MacAddress == deviceInfoUpdate.Id.Split('-')[1]);
            };

            DeviceWatcher.Removed += HandlerRemoved;
            DeviceWatcher.Start();
        }

        public async Task Pair(string name)
        {
            await PairAsync(name);
        }

        public void FindPairedDevices()
        {
            DeviceWatcher = DeviceInformation.CreateWatcher(PairedDevicesFilterString, null, DeviceInformationKind.AssociationEndpoint);

            if (PrinterListPaired == null || PrinterListPaired.Count == 0)
                PrinterListPaired = new List<Printer>();

            HandlerAdded = (watcher, deviceInfo) =>
            {
                var printer = new Printer
                {
                    SerialNumber = deviceInfo.Name,
                    MacAddress = deviceInfo.Id.Split('-')[1]
                };

                PrinterListPaired.Add(printer);
                DeviceInfoListUpdate?.Execute(printer);
            };

            DeviceWatcher.Added += HandlerAdded;
            HandlerRemoved = (watcher, deviceInfoUpdate) =>
            {
                // Find the corresponding DeviceInformation in the collection and remove it
                PrinterList?.RemoveAll(p => p.MacAddress == deviceInfoUpdate.Id.Split('-')[1]);
            };

            DeviceWatcher.Removed += HandlerRemoved;
            DeviceWatcher.Start();
        }

        public void RemoveBluetoothWatchers()
        {
            DeviceWatcher.Added -= HandlerAdded;
            DeviceWatcher.Removed -= HandlerRemoved;

            if (DeviceWatcher.Status.In(new[] { DeviceWatcherStatus.Started, DeviceWatcherStatus.EnumerationCompleted }))
                DeviceWatcher.Stop();
        }

        public void SetPairSuccessfulCommand(ICommand pairSuccessfulCommand)
        {
            PairSuccessfulCommand = pairSuccessfulCommand;
        }

        public void SetDeviceInfoListUpdate(ICommand deviceInfoListUpdate)
        {
            DeviceInfoListUpdate = deviceInfoListUpdate;
        }

        //Checks if printer is paired already or not
        public async Task<bool> IsPaired(string name) =>
            (await DeviceInformation.FindAllAsync(PairedDevicesFilterString))
            .Select(x => x.Name)
            .Any(p => p.Equals(name));

        public async Task<DeviceInformation> PairAsync(string name)
        {
            var printer = (await DeviceInformation.FindAllAsync(UnpairedDevicesFilterString)).FirstOrDefault(p => p.Name.Contains(name));

            if (printer != null)
                await printer.Pairing.PairAsync(DevicePairingProtectionLevel.Encryption);

            return printer;
        }

        public async Task<BluetoothDevice> FromIdAsync(string id)
        {
            var device = await BluetoothDevice.FromIdAsync(id);
            if (device != null)
            {
                var result = await device.GetRfcommServicesAsync();
                foreach (var service in result.Services)
                {
                    var attributes = await service.GetSdpRawAttributesAsync();
                    foreach (var attr in attributes)
                    {
                    }
                }
            }

            return device;
        }

        /// <summary>
        /// BluetoothDevice.GetDeviceSelectorFromPairingState(false);
        /// </summary>
        public string UnpairedDevicesFilterString => BluetoothDevice.GetDeviceSelectorFromPairingState(false);
        /// <summary>
        /// BluetoothDevice.GetDeviceSelectorFromPairingState(true)
        /// </summary>
        public string PairedDevicesFilterString => BluetoothDevice.GetDeviceSelectorFromPairingState(true);

        public string ScanResult
        {
            get => _scanResult;
            set { _scanResult = value; }
        }

        public DeviceWatcher DeviceWatcher
        {
            get => _deviceWatcher;
            set { _deviceWatcher = value; }
        }

        public TypedEventHandler<DeviceWatcher, DeviceInformation> HandlerAdded
        {
            get => _handlerAdded;
            set { _handlerAdded = value; }
        }

        public TypedEventHandler<DeviceWatcher, DeviceInformationUpdate> HandlerRemoved
        {
            get => _handlerRemoved;
            set { _handlerRemoved = value; }
        }
        public List<Printer> PrinterList
        {
            get => _printerList;
            set { _printerList = value; }
        }

        public List<Printer> PrinterListPaired
        {
            get => _printerListPaired;
            set { _printerListPaired = value; }
        }

        public ICommand PairSuccessfulCommand { get; set; }

        public ICommand DeviceInfoListUpdate { get; set; }
    }
}
