﻿using System.ComponentModel;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;

using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

using DSNY.Novas.Forms.Components;
using DSNY.Novas.Forms.UWP.CustomRenderers;
using System.Collections.Generic;
using DSNY.Novas.ViewModels.Utils;
using System;

[assembly: ExportRenderer(typeof(HyperlinkedTextLabel), typeof(HyperlinkedTextLabelRenderer))]
namespace DSNY.Novas.Forms.UWP.CustomRenderers
{
    public class HyperlinkedTextLabelRenderer : LabelRenderer
    {
        

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            var hyperlinkLabel = e.NewElement as HyperlinkedTextLabel;
            if (hyperlinkLabel != null && Control != null)
            {
                Control.LayoutUpdated += TextBlock_LayoutUpdated;
                UpdateText(Control);
            }
        }


        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // We are overriding the behavior of the base renderer when the text is changed
            if (e.PropertyName == Label.TextProperty.PropertyName || e.PropertyName == Label.FormattedTextProperty.PropertyName
                || e.PropertyName == HyperlinkedTextLabel.ScriptTextProperty.PropertyName
                || e.PropertyName == HyperlinkedTextLabel.ReplacementMappingProperty.PropertyName
                || e.PropertyName == HyperlinkedTextLabel.LinkClickedCommandProperty.PropertyName)
            {
                UpdateText(Control);
            }
            else
            {
                base.OnElementPropertyChanged(sender, e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (Control != null)
                Control.LayoutUpdated -= TextBlock_LayoutUpdated;

            base.Dispose(disposing);
        }

        private void UpdateText(TextBlock textBlock)
        {
            var hyperlinkLabel = Element as HyperlinkedTextLabel;
            if (hyperlinkLabel?.ScriptText != null && hyperlinkLabel?.ReplacementMapping != null)
            {
                var tokens = HyperlinkedTextLabelUtil.GetScriptTokens(hyperlinkLabel?.ScriptText);
                textBlock.Inlines.Clear();

                var outputText = "";
                foreach (var token in tokens)
                {
                    var replacementText = "";

                    if (token.Item1 == HyperlinkedTextLabelUtil.TokenType.Dynamic)
                    {
                        var hyperlink = new Hyperlink();

                        hyperlinkLabel.ReplacementMapping.TryGetValue(token.Item2, out replacementText);
                        replacementText = string.IsNullOrEmpty(replacementText) ? token.Item3 : replacementText;
                        hyperlink.Inlines.Add(new Run() { Text = replacementText });

                        hyperlink.Click += (sender, eventArgs) =>
                        {
                            hyperlinkLabel.LinkClickedCommand?.Execute(token.Item2);
                        };

                        textBlock.Inlines.Add(hyperlink);
                    }
                    else
                    {
                        textBlock.Inlines.Add(new Run() { Text = token.Item3 });
                    }

                    replacementText = string.IsNullOrEmpty(replacementText) ? token.Item3 : replacementText;
                    outputText += replacementText;
                }

                hyperlinkLabel.OutputText = outputText;
                textBlock.Width = hyperlinkLabel.Width;
                textBlock.UpdateLayout();
            }
        }

        private void TextBlock_LayoutUpdated(object sender, object e)
        {
            var rect = Element.Bounds;
            rect.Height = Control.ActualHeight;
            Element.Layout(rect);
        }
    }
}
