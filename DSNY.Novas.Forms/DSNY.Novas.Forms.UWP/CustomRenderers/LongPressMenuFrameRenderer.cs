﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Platform.UWP;
using DSNY.Novas.Forms;
using DSNY.Novas.Forms.Components;
using DSNY.Novas.Forms.UWP.CustomRenderers;
using Xamarin.Forms;
using Windows.UI.Xaml;

[assembly: ExportRenderer(typeof(LongPressMenuFrame), typeof(LongPressMenuFrameRenderer))]

namespace DSNY.Novas.Forms.UWP.CustomRenderers
{
    public class LongPressMenuFrameRenderer : FrameRenderer
    {
        LongPressMenuFrame frameView;
        bool isHolding = false;
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            frameView = Element as LongPressMenuFrame;
            if (frameView == null || this.Control == null)
            {
                return;
            }

            this.Control.Holding += LongPressHandler;
        }


        private void LongPressHandler(object sender, Windows.UI.Xaml.Input.HoldingRoutedEventArgs e)
        {
            if (!isHolding)
            {
                frameView.OnLongPress();
                isHolding = true;
            }
            else
            {
                isHolding = false;
            }
        }
    }
}
