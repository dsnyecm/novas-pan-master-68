﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

using DSNY.Novas.Forms.Components;
using DSNY.Novas.Forms.UWP.CustomRenderers;
using System.ComponentModel;
using Windows.UI.Xaml;

[assembly: ExportRenderer(typeof(Radiobutton), typeof(RadiobuttonRenderer))]
namespace DSNY.Novas.Forms.UWP.CustomRenderers
{
    using NativeRadioButton = Windows.UI.Xaml.Controls.RadioButton;

    public class RadiobuttonRenderer : ViewRenderer<Radiobutton, Windows.UI.Xaml.Controls.RadioButton>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Radiobutton> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= ElementOnPropertyChanged;
            }
            
            if (Control == null && Element != null)
            {
                var radioButton = new NativeRadioButton();
                radioButton.Content = string.Empty;
                radioButton.MinWidth = 20;
                radioButton.Checked += radioButton_Checked;
                radioButton.Unchecked += radioButton_Unchecked;

                SetNativeControl(radioButton);

                Control.IsChecked = e.NewElement?.IsChecked;

                Element.CheckedChanged += CheckedChanged;
                Element.EnabledChanged += EnabledChanged;
                Element.PropertyChanged += ElementOnPropertyChanged;
            }

            
        }

        private void CheckedChanged(object sender, bool e)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                Control.IsChecked = e;
            });
        }

        private void EnabledChanged(object sender, bool e)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                Control.IsEnabled = e;
            });
        }

        private void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (Element != null)
                Element.IsChecked = true;
        }

        private void radioButton_Enabled(object sender, RoutedEventArgs e)
        {
            if (Element != null)
                Element.IsEnabled = true;
        }

        private void radioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if(Element!=null)
                Element.IsChecked = false;
        }

        private void ElementOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsChecked":
                    Control.IsChecked = Element.IsChecked;
                    break;
                case "IsEnabled":
                    Control.IsEnabled = Element.IsEnabled;
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("Property change for {0} has not been implemented.", e.PropertyName);
                    break;
            }
        }
    }
}
