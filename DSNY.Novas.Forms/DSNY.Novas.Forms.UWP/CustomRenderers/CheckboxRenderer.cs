﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

using DSNY.Novas.Forms.Components;
using DSNY.Novas.Forms.UWP.CustomRenderers;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(Checkbox), typeof(CheckboxRenderer))]
namespace DSNY.Novas.Forms.UWP.CustomRenderers
{
    public class CheckboxRenderer : ViewRenderer<Checkbox, Windows.UI.Xaml.Controls.CheckBox>
    {
        private Windows.UI.Xaml.Controls.CheckBox nativeCheckbox;

        protected override void OnElementChanged(ElementChangedEventArgs<Checkbox> e)
        {
            base.OnElementChanged(e);

            var formsCheckbox = e.NewElement;
            if (formsCheckbox != null)
            {
                nativeCheckbox = new Windows.UI.Xaml.Controls.CheckBox();
                CheckboxPropertyChanged(formsCheckbox, null);

                nativeCheckbox.Click += (object sender, Windows.UI.Xaml.RoutedEventArgs args) => formsCheckbox.IsChecked = !formsCheckbox.IsChecked;
                
                // Reset the native control to match the current enabled state, as it can be affected by changes further up in the view hierarchy
                nativeCheckbox.IsEnabledChanged += (object sender, Windows.UI.Xaml.DependencyPropertyChangedEventArgs args) =>
                {
                    nativeCheckbox.IsEnabled = formsCheckbox.IsEnabled;
                };

                SetNativeControl(nativeCheckbox);
            }
        }

        private void CheckboxPropertyChanged(Checkbox formsCheckbox, String propertyName)
        {
            if (propertyName == null || Checkbox.IsCheckedProperty.PropertyName == propertyName)
            {
                nativeCheckbox.IsChecked = formsCheckbox.IsChecked;
            }

            // Always setting the IsEnabled value when a property changes, as it can be affected by changes further up in the view hierarchy
            nativeCheckbox.IsEnabled = formsCheckbox.IsEnabled;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            CheckboxPropertyChanged((Checkbox)sender, e.PropertyName);
        }
    }
}
