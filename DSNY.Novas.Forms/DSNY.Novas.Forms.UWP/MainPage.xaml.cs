﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using DSNY.Novas.ViewModels;
using Windows.ApplicationModel.Background;

namespace DSNY.Novas.Forms.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new DSNY.Novas.Forms.App());
            BackgroundTaskFactory.RegisterBackgroundTask("MyBackgroundTask", new SystemTrigger(SystemTriggerType.NetworkStateChange, false), null);
            //#if DEBUG
            //            { 
            //            BackgroundTaskFactory.RegisterBackgroundTask("MyBackgroundTask", new SystemTrigger(SystemTriggerType.InternetAvailable, false), null);
            //            }
            //#else
            //            {
            //                BackgroundTasksFactory.RegisterBackgroundTask("MyBackgroundTask", new SystemTrigger(SystemTriggerType.NetworkStateChange, false), null);
            //            }
            //#endif

            //       
        }
    }
}
