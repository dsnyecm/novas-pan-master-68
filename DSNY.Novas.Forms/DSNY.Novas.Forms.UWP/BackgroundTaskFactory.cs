﻿using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

namespace DSNY.Novas.Forms.UWP
{
    public class BackgroundTaskFactory
    {
        public static BackgroundTaskRegistration RegisterBackgroundTask(string name, IBackgroundTrigger trigger, IBackgroundCondition condition)
        {

            // UNREGISTER background tasks if version has changed
            string cachedVersion = CrossSettings.Current.GetValueOrDefault("APPVERSION", "-1", "APPVERSION");
            string currentVersion = DependencyResolver.Get<IAppRuntimeSettings>().Version;

            foreach (var cur in BackgroundTaskRegistration.AllTasks)
            {
                if (cur.Value.Name == name)
                {
                    if (cachedVersion != currentVersion)
                    {
                        // versions are different, so update version cache and unregister it
                        CrossSettings.Current.AddOrUpdateValue("APPVERSION", currentVersion, "APPVERSION");
                        cur.Value.Unregister(true);
                    }
                    else
                    {
                        // The task is already registered and the app version is unchanged.
                        return (BackgroundTaskRegistration)(cur.Value);
                    }
                }

            }

            //Register new background task:
            var builder = new BackgroundTaskBuilder();

            // var net=new SystemCondition(SystemConditionType.InternetAvailable);

            builder.Name = name;
            //builder.TaskEntryPoint = taskEntryPoint;
            builder.SetTrigger(trigger);


            // comment out per discussion with Charan
            // builder.AddCondition(new SystemCondition(SystemConditionType.InternetNotAvailable));


            if (condition != null)
            {

                builder.AddCondition(condition);
            }

            BackgroundTaskRegistration task = builder.Register();

            return task;

        }
    }
}
