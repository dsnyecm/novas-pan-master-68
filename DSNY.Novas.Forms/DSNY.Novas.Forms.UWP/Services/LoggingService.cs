﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Serilog.Sinks.RollingFileAlternate;
using Serilog;
using System.IO;
using Windows.Storage;

using DSNY.Novas.Services;

[assembly: Xamarin.Forms.Dependency(typeof(DSNY.Novas.Forms.UWP.LoggingService))]
namespace DSNY.Novas.Forms.UWP
{
    public class LoggingService : ILoggingService
    {
        private LoggingService instance;
        public LoggingService()
        {
            const string fileOutputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] {Message}{NewLine}{Exception}";
            var logPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "Logs", "DSNY.NOVAS-{Date}.log");

            var logConfiguration = new LoggerConfiguration()
                                    .MinimumLevel.Verbose()
                                    .WriteTo.RollingFileAlternate(null, logPath, minimumLevel: Serilog.Events.LogEventLevel.Verbose, outputTemplate: fileOutputTemplate);

            Log.Logger = logConfiguration.CreateLogger();
        }

        public LoggingService GetInstance()
        {
            if (instance == null)
                instance = new LoggingService();

            return instance;
        }

        public void LogDebug(string message)
        {
            Log.Debug(message);
        }

        public void LogError(string message)
        {
            Log.Error(message);
        }
        public void LogInfo(string message)
        {
            Log.Information(message);
        }

        public void LogWarning(string message)
        {
            Log.Warning(message);
        }
    }
}