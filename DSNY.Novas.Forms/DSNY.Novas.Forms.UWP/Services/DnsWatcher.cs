﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Connectivity;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.ViewModels.Utils;

[assembly: Xamarin.Forms.Dependency(typeof(DSNY.Novas.Forms.UWP.Services.DnsWatcher))]
namespace DSNY.Novas.Forms.UWP.Services
{
    public class DnsWatcher : IDnsWatcher
    {
        //private DnsWatcher _instance;

        public async Task<IEnumerable<IPAddress>> GetIpAddressesAsync()
        {
            return NetworkInformation.GetHostNames()
                .Where(hostName => hostName.Type == HostNameType.Ipv4)
                .SelectMany(hostName => Dns.GetHostAddressesAsync(hostName.DisplayName).Result)
                .Distinct();
        }

        public IPAddress GetIpAddress()
        {
            var ifo = NetworkInformation.GetHostNames()
                   .Where(hostName => hostName.Type == HostNameType.Ipv4)
                   .SelectMany(hostName => Dns.GetHostAddressesAsync(hostName.DisplayName).Result)
                   .Distinct();


            return NetworkInformation.GetHostNames()
                .Where(hostName => hostName.Type == HostNameType.Ipv4)
                .SelectMany(hostName => Dns.GetHostAddressesAsync(hostName.DisplayName).Result)
                .Distinct()
                .FirstOrDefault();
        }

        public string GetNetworkName()
        {
            var ss = NetworkInformation.GetInternetConnectionProfile();
            return NetworkInformation.GetInternetConnectionProfile()?.GetNetworkNames().FirstOrDefault();
        }

        public IEnumerable<HostName> GetHostNames()
        {
            return NetworkInformation.GetHostNames();
        }
    }
}
