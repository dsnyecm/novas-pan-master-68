﻿using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Services;
using Microsoft.HockeyApp;
using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Background;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Xamarin.Forms.Platform.UWP;

namespace DSNY.Novas.Forms.UWP
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    /// 

    sealed partial class App : Application
    {
        private LocalDeviceSettingsService _localSettingsService;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            Microsoft.HockeyApp.HockeyClient.Current.Configure("bfb921ad6df04f7a93e12645d38baeae");

            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.UnhandledException += OnUnhandledException;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        { 

            try
            {
#if DEBUG
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    //this.DebugSettings.EnableFrameRateCounter = true;
                }
#endif
                //RegisterBackgroundTasks();

                Frame rootFrame = Window.Current.Content as Frame;

                // Do not repeat app initialization when the Window already has content,
                // just ensure that the window is active
                if (rootFrame == null)
                {
                    // Create a Frame to act as the navigation context and navigate to the first page
                    rootFrame = new Frame();

                    rootFrame.NavigationFailed += OnNavigationFailed;

                    Xamarin.Forms.Forms.Init(e);

                    var storageFile = IsolatedStorageFile.GetUserStoreForApplication();
                    // TODO: For now, we are copying over the db from the Assets folder on first launch.
                    // This may not be the desired behavior for the final release.
                    if (!storageFile.FileExists(Path.Combine(ApplicationData.Current.LocalFolder.Path, "Novas.db")))
                    {
                        // copy storage file; replace if exists.
                        // var dataPath = ApplicationData.Current.LocalFolder.Path;
                        // var root = Windows.ApplicationModel.Package.Current.InstalledLocation.Path;
                        var fileToRead = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Novas.db"));
                        StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                        StorageFile fileCopy = await fileToRead.CopyAsync(storageFolder, "Novas.db", NameCollisionOption.ReplaceExisting);
                    }

                    if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                    {
                        //TODO: Load state from previously suspended application
                    }

                    // Place the frame in the current Window
                    Window.Current.Content = rootFrame;
                }

                Windows.UI.Color BackgroundColor = Windows.UI.Color.FromArgb(255, 11, 20, 41);
                Windows.UI.Color FieldBackgroundColor = Windows.UI.Color.FromArgb(255, 46, 61, 85);
                Windows.UI.Color FieldBorderColor = Windows.UI.Color.FromArgb(255, 29, 38, 62);
                Windows.UI.Color DisabledFieldTextColor = Windows.UI.Color.FromArgb(255, 156, 162, 172);

                var style = Application.Current.Resources["FormsTextBoxStyle"] as Windows.UI.Xaml.Style;
                try
                {
                    style.Setters.Add(new Setter { Property = FormsTextBox.MinWidthProperty, Value = 30 });
                    style.Setters.Add(new Setter { Property = FormsTextBox.BorderBrushProperty, Value = new SolidColorBrush(FieldBorderColor) });
                }
                catch (Exception ex)
                {
                    // If these setters already exist, we can get a crash here (usually happens when the app is relaunched from the home screen while it is still running)
                }
                Application.Current.Resources["SystemControlBackgroundChromeMediumLowBrush"] = new SolidColorBrush(FieldBackgroundColor);
                Application.Current.Resources["CheckBoxCheckGlyphForegroundChecked"] = new SolidColorBrush(BackgroundColor);
                Application.Current.Resources["ComboBoxBorderBrush"] = new SolidColorBrush(FieldBorderColor);
                Application.Current.Resources["SystemControlDisabledChromeDisabledLowBrush"] = new SolidColorBrush(DisabledFieldTextColor);
                Application.Current.Resources["SystemControlForegroundBaseMediumLowBrush"] = new SolidColorBrush(FieldBorderColor);
                Application.Current.Resources["SystemControlBackgroundAltMediumLowBrush"] = new SolidColorBrush(FieldBackgroundColor);

                if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
                {
                    var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
                    statusBar.BackgroundColor = BackgroundColor;
                    statusBar.BackgroundOpacity = 1;
                }

                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
            catch(Exception ex)
            {
                //startup exception, so write to file, rather than DB which may be inaccessible or corrupt
                var deviceId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier?.Substring(0,6);

                StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
                StorageFile exceptionFile = await storageFolder.CreateFileAsync("novasExceptionFile.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);

                await Windows.Storage.FileIO.AppendTextAsync(exceptionFile, String.Format("Exception Time: {0}", DateTime.Now));
                await Windows.Storage.FileIO.AppendTextAsync(exceptionFile, String.Format("Device ID: {0}", deviceId));
                await Windows.Storage.FileIO.AppendTextAsync(exceptionFile, "Source: DSNY.NOVAS.Forms.UWP App.Xaml.cs OnLaunched()");
                await Windows.Storage.FileIO.AppendTextAsync(exceptionFile, String.Format("{0}{1}", ex.Message, ex.StackTrace));
            }
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            try
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                //TODO: Save application state and stop any background activity
                deferral.Complete();
            }
            catch (Exception ex)
            {
               
            }
        }

        //private void RegisterBackgroundTasks()
        //{
        //    //Register task to respond to docking
        //    string taskName = "SyncServiceTask";
        //    string taskEntryPoint = "DSNY.Novas.Forms.UWP.SyncServiceTask";
        //    IBackgroundTrigger trigger = new SystemTrigger(SystemTriggerType.InternetAvailable, false);

        //    BackgroundTaskBuilder builder = new BackgroundTaskBuilder();
        //    builder.Name = taskName;
        //    builder.TaskEntryPoint = taskEntryPoint;
        //    builder.SetTrigger(trigger);
        //    BackgroundTaskRegistration task = builder.Register();

        //}
        protected override void OnBackgroundActivated(BackgroundActivatedEventArgs args)
        {
            base.OnBackgroundActivated(args);
            IBackgroundTaskInstance taskInstance = args.TaskInstance;
            IBackgroundTask task = null;
            if (taskInstance.Task.Name == "MyBackgroundTask" && SyncServiceTask.syncData == false)
            {

                task = new SyncServiceTask();

            }

            ToastCustomRenderer toastCustom = new ToastCustomRenderer();
            try
            {
                HttpClient client = new HttpClient();

                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    task?.Run(taskInstance);
                }
                else
                {
                   
                    toastCustom.DoToast("Home Network is not available " + DateTime.Now);
                }


            }
            catch (Exception ex)
            {
                toastCustom.DoToast("Network is not available " + DateTime.Now);
            }
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var exc = unhandledExceptionEventArgs.Exception;
            Task.Run(() => DependencyResolver.Get<IRepository<DBApplicationLog>>()
                .InsertAsync(new DBApplicationLog
                {
                    AddTimestamp = DateTime.Now,
                    Data = exc.Message,
                    LogTimestamp = DateTime.Now,
                    Source = exc.StackTrace
                }));
        }
    }
}
