﻿using System;
using Windows.ApplicationModel.Background;
using Windows.Devices.Power;
using Windows.System.Power;
using DSNY.Novas.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using DSNY.Novas.Forms;
using DSNY.Novas.ViewModels;
using DSNY.Novas.ViewModels.Utils;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using DSNY.Novas.ViewModels.Interfaces;
using DSNY.Novas.Common;
using DSNY.Novas.Services;
using Windows.UI.Popups;
using Xamarin.Forms;
using Plugin.Settings;
using System.Net.NetworkInformation;

namespace DSNY.Novas.Forms.UWP
{
    public sealed class SyncServiceTask : IBackgroundTask
    {
        private BackgroundTaskDeferral deferral = null;
        private IBackgroundTaskInstance backgroundtaskInstance = null;
        private SyncService _syncService;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            taskInstance.Canceled += this.TaskInstanceCanceled;
            this.deferral = taskInstance.GetDeferral();
            var batteryReport = Battery.AggregateBattery.GetReport();

            await StartSync();
            //await ShowDialog();
            //If the battery is charging, we're docked/connected
          /*  if (batteryReport != null)
            {
                if (batteryReport.Status == BatteryStatus.Charging)
                {
                    await StartSync();
                }

            }*/

            this.deferral.Complete();
        }

        private void TaskInstanceCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            this.deferral.Complete();
        }


        public static bool syncData;
        private async Task StartSync()
        {
            //IAlertService alert = DependencyResolver.Get<IAlertService>();
            //await alert.DisplayAlert(new AlertViewModel("Sync", "Syncing"));

            var api = new ApiUrl();
            //string url = await api.ServicUrl();
            string url = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);

            //ISyncService syncService = new SyncService("http://dvw033novaskk31/NOVAShhtSynchServiceDEV41/");
            ISyncService syncService = new SyncService(url);
            // await syncService.Sync();

            var syncStatusViewModel = new SyncStatusViewModel();

            // var model = new ViewModelBase();


            syncData = true;
            var NavigationService = new NavigationService();
            CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "0");
            await NavigationService.PushModalAsync(syncStatusViewModel);
            await syncStatusViewModel.serviecall();
            Device.StartTimer(new TimeSpan(0, 45, 0), () =>
            {
                var batteryReport = Battery.AggregateBattery.GetReport();
                //if (batteryReport != null)
                //{
                //    if (batteryReport.Status == BatteryStatus.Charging)
                //    {
                //        Task.Run(async () =>
                //        {
                //            await syncStatusViewModel.serviecall();
                //        });
                //    }

                //}


                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    Task.Run(async () =>
                    {
                        _syncService = new SyncService(url);
                        var serverPing = await _syncService.SyncTime();
                        if(serverPing)
                            await syncStatusViewModel.serviecall();
                    });
                }


                //if(batteryReport != null)
                //{

                //    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() && batteryReport.Status == BatteryStatus.Charging)
                //    {
                //        Task.Run(async () =>
                //        {
                //            await syncStatusViewModel.serviecall();
                //        });
                //    }
                //}


                return true;
            });
           
            syncData = false;
        }      
    }
}
