﻿using System;
using System.IO;
using DSNY.Novas.Common;
using DSNY.Novas.Forms.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppRuntimeSettingsIOS))]
namespace DSNY.Novas.Forms.iOS
{
    public class AppRuntimeSettingsIOS : AppRuntimeSettingsBase
    {
        public override string GetSqlitePath()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentsPath, "..", "Library");
            var path = Path.Combine(libraryPath, DatabaseFilename);
            return path;
        }

        public override string HardwareIdentifier { get; }

        public override string Version { get; }

        public override string HHTNumber { get; }
        public override string PlatformDeviceName { get; }
        public override string DeviceIMEI { get; }
        public override string DeviceSerialNumber { get; }

    }
}