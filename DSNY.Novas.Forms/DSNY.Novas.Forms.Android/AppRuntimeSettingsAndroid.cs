﻿using System.IO;
using DSNY.Novas.Common;
using DSNY.Novas.Forms.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppRuntimeSettingsAndroid))]
namespace DSNY.Novas.Forms.Droid
{
    public class AppRuntimeSettingsAndroid : AppRuntimeSettingsBase
    {
        public override string GetSqlitePath()
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, DatabaseFilename);
            return path;
        }

        public override string HardwareIdentifier { get; }

        public override string Version { get; }

        public override string HHTNumber { get; }
        public override string PlatformDeviceName { get; }
        public override string DeviceIMEI { get; }
        public override string DeviceSerialNumber { get; }
    }
}