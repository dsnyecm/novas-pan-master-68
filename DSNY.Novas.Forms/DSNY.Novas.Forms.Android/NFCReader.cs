﻿using System;
using System.Windows.Input;
using System.Threading.Tasks;

using DSNY.Novas.Forms.Droid;
using DSNY.Novas.Common.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(NFCReader))]
namespace DSNY.Novas.Forms.Droid
{
    class NFCReader : INFCReader
    {
        public ICommand CardRecognizedCommand { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ICommand CardRemovedCommand { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string CurrentCardUID => throw new NotImplementedException();

        public Task<bool> IsNFCAvailable()
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsNFCEnabled()
        {
            throw new NotImplementedException();
        }

        public Task<bool> NavigateToNFCSettings()
        {
            throw new NotImplementedException();
        }

        public Task<bool> StartListening()
        {
            throw new NotImplementedException();
        }

        public void StopListening()
        {
            throw new NotImplementedException();
        }
    }
}
