﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using System.Collections.Generic;
using System.Linq;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        #region Fields
        
        private ApiUrl apiUrl;
        private bool _isPinEntryVisible;
        private bool _isInstructionTextVisible;
        private bool _isSetupVisible;
        private bool _isDSNYLogoVisible;
        private bool _isSanPoliceLogoVisible;
        private bool _isSheriffLogoVisible;
        private bool _isNovasLogoVisible;
        private string _deviceId;
        private string _version;
        private string _pin;
        private string _userName;
        private string _userAgency;
        private string _HHTNumber;
        private string _SerialNumber;
        //private string _deviceBoroSiteCode = "KK0A"; // TODO: remove init value after implement obtain deviceBoroSite by IPAddress
        private string _deviceBoroSiteCode = "";

        private List<String> _boroSiteCodeList;
        private string _boroSiteCode;

        private List<string> _departmentPickerItems;
        private string _selectedDepartmentItems;
        private bool _isDepartmentPickerVisible;

        private string ServiceUrl = null;

        private string _syncDeviceIdText;
        private string _syncUserNFCIDText;
        private bool _isSyncDeviceIdVisible;
        private SyncService _syncService;
        private IBluetoothReader _bluetoothReader;

        private IDnsWatcher _dnsWatcher;
        private string networkName;
        private string ipAddress;
        private bool _isSetingVisiable;

        #endregion // Fields

        public override string Title => "Login";

        private string syncStatus;

        #region Constuctor

        public LoginViewModel()
        {
            apiUrl = new ApiUrl();
            NetworkName = apiUrl.NetworkName;
            IpAddress = apiUrl.IpAddress;
            
            UserName = "Hold ID card against reader.";
            IsInstructionTextVisible = false;
            IsPinEntryVisible = false;
            IsSyncDeviceIdVisible = true;
            SyncDeviceIdText = "";
            SyncUserNFCIDText = "";

            resetLogoVisible();
            IsNovasLogoVisible = true;
            _isSetingVisiable = false;
            string preSyncServiceUrl = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);

            CrossSettings.Current.Clear();
            if(!string.IsNullOrEmpty(preSyncServiceUrl))
                CrossSettings.Current.AddOrUpdateValue("SyncServiceUrl", preSyncServiceUrl);
        }

        #endregion // Constructor

        #region Methods

        public override async Task LoadAsync()
        {
            await base.LoadAsync();

            IList<LookupTable> stites = await LookupService.GetLookupCode("SITE");
            var lsite = stites.FirstOrDefault();
            if(lsite != null)
            {
                _deviceBoroSiteCode = lsite.Description;
            }

            //string SyncServiceUrl = await apiUrl.ServicUrl();
            string SyncServiceUrl = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);

            _syncService = new SyncService(SyncServiceUrl);
            IsSyncDeviceIdVisible = false;
            SyncDeviceIdText = "";
            setDeviceIds();

            // NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //var deviceId = await _deviceService.GetDeviceIdForHardware(hardwareId);
            //if (string.IsNullOrEmpty(deviceId))
            if (!await AuthenticateDevice(DeviceBoroSiteCode))

            {
                IsSyncDeviceIdVisible = true;
                var boroSiteMasterRepo = DependencyResolver.Get<IRepository<DBBoroSiteMaster>>();
                var BoroSiteMasterList = await boroSiteMasterRepo.GetAsync();
                if (BoroSiteMasterList != null && BoroSiteMasterList.LongCount() > 0)
                {
                    BoroSiteCodeList = BoroSiteMasterList.Select(bsm => bsm.BoroSiteCode).Distinct().ToList();
                    if (String.IsNullOrEmpty(BoroSiteCode))
                        BoroSiteCode = BoroSiteCodeList.FirstOrDefault();
                }
            }
            if (!String.IsNullOrEmpty(_HHTNumber))
                CrossSettings.Current.AddOrUpdateValue("SyncStatus_FullSyncCalled_", false.ToString()); // TODO: set to false

            var syncTime = await DeviceSettingsService.GetLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey);
            // it can be disabled when read to prod
            IsSetupVisible = true;

            if (syncTime == null)
                IsSetupVisible = true;

            IsDepartmentPickerVisible = true;

            if (DepartmentPickerItems == null)
                DepartmentPickerItems = new List<string> { SHERIFF_DEP_KEY, SHERIFF_DEP };

            DeviceId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            Version = DependencyResolver.Get<IAppRuntimeSettings>().Version;

            IsPinEntryVisible = false;
            IsInstructionTextVisible = false;
            PIN = "";
            if (!await NfcReader.IsNFCAvailable())
            {
                await AlertService.DisplayAlert(new AlertViewModel("90001", WorkFlowMessages.DSNYMSG_PANNOV001));
                return;
            }

            if (!await NfcReader.IsNFCEnabled())
            {
                await AlertService.DisplayAlert(new AlertViewModel("90002", WorkFlowMessages.DSNYMSG_PANNOV002));
                await NfcReader.NavigateToNFCSettings();
                return;
            }

            await NfcReader.StartListening();
            await LoadPrintersTask;
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            NfcReader.StopListening();
        }

        public async Task<bool> IsValidUser(string userId)
        {
            var isValid = await LoginService.IsValidUser(userId);
            return isValid;
        }

        public async Task<string> AuthenticateUser(string pin, string nfcUid)
        {
            var userIdReturned = await LoginService.GetUserIdFromCardID(pin, nfcUid);
            if (!String.IsNullOrEmpty(userIdReturned))
            {
                LoginTimestamp = DateTime.Now;
                return userIdReturned;
            }
            return "";
        }

        public async Task<bool> AuthenticateDevice()
        {
            var deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            var deviceId = await DeviceService.GetDeviceIdByIMEI(deviceIMEI);
            return (string.IsNullOrEmpty(deviceId)) ? false : true;
        }
        public async Task<bool> AuthenticateDevice(string boroLocation)
        {
            var deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            var deviceId = await DeviceService.GetDeviceIdByIMEIAndBoro(deviceIMEI, boroLocation);
            _HHTNumber = deviceId;
            return (string.IsNullOrEmpty(deviceId)) ? false : true;
        }
        #endregion // Methods

        #region Properties
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                ipAddress = value;
                NotifyPropertyChanged();
            }
        }
        public string NetworkName
        {
            get => networkName;
            set
            {
                networkName = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsSetupVisible
        {
            get => _isSetupVisible;
            set { _isSetupVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsPinEntryVisible
        {
            get => _isPinEntryVisible;
            set { _isPinEntryVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsDSNYLogoVisible
        {
            get => _isDSNYLogoVisible;
            set { _isDSNYLogoVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsSanPoliceLogoVisible
        {
            get => _isSanPoliceLogoVisible;
            set { _isSanPoliceLogoVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsSheriffLogoVisible
        {
            get => _isSheriffLogoVisible;
            set { _isSheriffLogoVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsNovasLogoVisible
        {
            get => _isNovasLogoVisible;
            set { _isNovasLogoVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsInstructionTextVisible
        {
            get => _isInstructionTextVisible;
            set { _isInstructionTextVisible = value; NotifyPropertyChanged(); }
        }

        public string PIN
        {
            get => _pin;
            set { _pin = value; NotifyPropertyChanged(); }
        }

        public string DeviceId
        {
            get => _deviceId;
            set { _deviceId = value; NotifyPropertyChanged(); }
        }

        public string Version
        {
            get => _version;
            set { _version = value; NotifyPropertyChanged(); }
        }

        public bool IsSetingVisiable
        {
            get => _isSetingVisiable;
            set { _isSetingVisiable = value; NotifyPropertyChanged(); }
        }

        public string BuildDate
        {
            get => "Build Date: " + DateTime.Now.ToString("MM-dd-yyyy");
            //set { _version = value; NotifyPropertyChanged(); }
        }

        public List<string> DepartmentPickerItems
        {
            get => _departmentPickerItems;
            set
            {
                if (_departmentPickerItems?.Equals(value) == true)
                    return;

                _departmentPickerItems = value;
                NotifyPropertyChanged();

                SelectedDepartmentItems = DepartmentPickerItems.FirstOrDefault();
            }
        }

        public string SelectedDepartmentItems
        {
            get => _selectedDepartmentItems;
            set { _selectedDepartmentItems = value; NotifyPropertyChanged(); }
        }

        public bool IsDepartmentPickerVisible
        {
            get => _isDepartmentPickerVisible;
            set
            {
                if (_isDepartmentPickerVisible == value)
                    return;

                _isDepartmentPickerVisible = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(ShowBackButton));
            }
        }

        public string UserName
        {
            get => _userName;
            set
            {
                if (_userName?.Equals(value) == true)
                    return;

                _userName = value;
                NotifyPropertyChanged();
            }
        }
        public string UserAgency
        {
            get => _userAgency;
            set
            {
                if (_userAgency?.Equals(value) == true)
                    return;

                _userAgency = value;
                NotifyPropertyChanged();
            }
        }
        public string HHTNumber
        {
            get => _HHTNumber;
            set
            {
                if (_HHTNumber?.Equals(value) == true)
                    return;

                _HHTNumber = value;
                NotifyPropertyChanged();
            }
        }
        public string SerialNumber
        {
            get => _SerialNumber;
            set
            {
                if (_SerialNumber?.Equals(value) == true)
                    return;

                _SerialNumber = value;
                NotifyPropertyChanged();
            }
        }
        public ICommand SetupCommand => new Command(async () =>
        {
            await NavigationService.PushModalAsync(new SyncStatusViewModel());
        });

        public ICommand SettingCommand => new Command(async () =>
        {
            MainPage();
            IsSetingVisiable = false;
            //await NavigationService.PushModalAsync(new SyncStatusViewModel());
        });

        public ICommand ResetSyncTimeCommand => new Command(async () =>
        {
            DateTime dt = DateTime.Now.AddMonths(-1);
            var urls = Task.Run(() => DeviceSettingsService.InsertOrUpdateLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey, dt.ToString("yyyy-MM-dd h:mm:ss tt"))).Result;
        });

        public ICommand DepartmentSelectedCommand => new Command(async () =>
        {
            if (NfcReader.CurrentCardUID=="A2-48-1D-0A" || NfcReader.CurrentCardUID=="2A-F1-B0-62")
                IsSetingVisiable = true;
            // NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //var deviceId = await _deviceService.GetDeviceIdForHardware(hardwareId);

            // NH-1037 must be completed before using IMEI for validation
            if (!await AuthenticateDevice(DeviceBoroSiteCode))
            {
                await AlertService.DisplayAlert(new AlertViewModel("90003", WorkFlowMessages.DSNYMSG_PANNOV003));
                return;
            }

            IsDepartmentPickerVisible = false;
            IsInstructionTextVisible = true;
            resetLogoVisible();

            /* 
             * clear username on hitting submit button because 99% of the time it needs to say "Hold ID Card..."
             * the other 1% is when you scan your card on the screen (which shows the PIN entry at the bottom of original screen) but STILL hit submit. 
             * In that case, we keep the username from the card.
             * 
             */

            if (!IsPinEntryVisible)
            {
                UserName = "Hold ID card against reader";
            }            

            NfcReader.CardRecognizedCommand = new Command(async () =>
            {
                IsPinEntryVisible = true;

                UserName = await LoginService.GetUserNameFromCardID(NfcReader.CurrentCardUID);
                UserAgency = await LoginService.GetUserAgencyFromCardID(NfcReader.CurrentCardUID);
                //if (UserAgency != null)
                //{
                //    resetLogoVisible();
                //    // [CentralSite].[WEB].[CS_Agencies]
                //    switch (UserAgency)
                //    {
                //        case "837":
                //            {
                //                IsDSNYLogoVisible = false;
                //                IsSanPoliceLogoVisible = false;
                //                IsSheriffLogoVisible = true;
                //                break;
                //            }
                //        case "828":
                //            {
                //                IsDSNYLogoVisible = false;
                //                IsSanPoliceLogoVisible = true;
                //                IsSheriffLogoVisible = false;
                //                break;
                //            }
                //        default:
                //            {
                //                IsDSNYLogoVisible = true;
                //                IsSanPoliceLogoVisible = false;
                //                IsSheriffLogoVisible = false;
                //                break;
                //            }
                //    }
                //}

                if (SelectedDepartmentItems == SHERIFF_DEP)
                {
                    IsDSNYLogoVisible = false;
                    IsSanPoliceLogoVisible = false;
                    IsSheriffLogoVisible = true;
                }
                else
                {
                    IsDSNYLogoVisible = true;
                    IsSanPoliceLogoVisible = false;
                    IsSheriffLogoVisible = false;
                }


                if (string.IsNullOrEmpty(UserName) && NfcReader?.CurrentCardUID != null)
                {
                    SyncUserNFCIDText = "User Account not setup. Contact Adminstrator. Provide code ( "+ NfcReader.CurrentCardUID + " )";
                }
                else
                {
                    SyncUserNFCIDText = "";
                }

            });
        });
        private void setDeviceIds()
        {
            var PlatformDeviceName = DependencyResolver.Get<IAppRuntimeSettings>().PlatformDeviceName;
            var DeviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            var DeviceSerialNumber = DependencyResolver.Get<IAppRuntimeSettings>().DeviceSerialNumber;
            var deviceIdText = "Platform DeviceId: " + PlatformDeviceName + "\n";
            deviceIdText += "IMEI: " + DeviceIMEI + "\n";
            //deviceIdText += "Serial Number: " + DeviceSerialNumber + "\n";
            SyncDeviceIdText = deviceIdText;
            SerialNumber = "";
        }
        private void resetLogoVisible()
        {
            IsDSNYLogoVisible = false;
            IsSanPoliceLogoVisible = false;
            IsSheriffLogoVisible = false;
            IsNovasLogoVisible = false;
        }

        public ICommand PINEnteredCommand => new Command(async () =>
        {
            var userID = await AuthenticateUser(PIN, NfcReader.CurrentCardUID);
            if (!String.IsNullOrEmpty(userID))
            {
                UserSession = new UserSession { UserId = userID, Department = SelectedDepartmentItems };

                if (UserSession.Department == SHERIFF_DEP)
                    UserSession.AgencyId = "837";
                else
                    UserSession.AgencyId = "827";

                // NH-979
                //var deviceId = await _deviceService.GetDeviceIdForHardware(DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier);
                string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
                string deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);

                CrossSettings.Current.AddOrUpdateValue("Login_UserId_" , userID.ToString());

                UserSession.DutyHeader = new DutyHeader
                {
                    DeviceId = deviceId,
                    LoginTimestamp = TimeManager.Now
                };

                var personnelDataViewModel = new PersonnelDataViewModel { UserSession = UserSession };
                await NavigationService.PushAsync(personnelDataViewModel);
            }
            else
            {
                PIN = string.Empty;
                await AlertService.DisplayAlert(new AlertViewModel("90004", WorkFlowMessages.DSNYMSG_PANNOV004));
            }
        });

        public override bool ShouldSaveTicketOnNext => false;
        public override bool ShowNextButton => false;
        public override bool ShowMenuButton => false;

        public ICommand SyncDeviceIdCommand => new Command(async () => {
            string SYNC_DEVICEID = "Syncing DeviceId...\r\n";
            //string SYNC_DEVICEID_COMPLETE = "\r\nSync DeviceId is completed successfully!\r\n";
            //string SYNC_DEVICEID_FAIL = "\r\nSync DeviceId failed!\r\n";
            SyncDeviceIdText = SYNC_DEVICEID; // "Syncing DeviceId to Server...";

            //var HHTNumber = DependencyResolver.Get<IAppRuntimeSettings>().HHTNumber;

            // TODO: replace user input serial number by obtaining it from API when it is available
            //var DeviceSerialNumber = DependencyResolver.Get<IAppRuntimeSettings>().DeviceSerialNumber; 

            var PlatformDeviceName = DependencyResolver.Get<IAppRuntimeSettings>().PlatformDeviceName;
            var DeviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;

            var result = await _syncService.SyncDeviceId(DeviceIMEI, SerialNumber, PlatformDeviceName, "PAN" + HHTNumber, BoroSiteCode);

            SyncDeviceIdText = result;
        });

        public bool IsSyncDeviceIdVisible
        {
            get => _isSyncDeviceIdVisible;
            set { _isSyncDeviceIdVisible = value; NotifyPropertyChanged(); }
        }
        public string SyncUserNFCIDText
        {
            get => _syncUserNFCIDText;
            set { _syncUserNFCIDText = value; NotifyPropertyChanged(); }
        }
        public string SyncDeviceIdText
        {
            get => _syncDeviceIdText;
            set { _syncDeviceIdText = value; NotifyPropertyChanged(); }
        }
        public List<String> BoroSiteCodeList
        {
            get => _boroSiteCodeList;
            set { _boroSiteCodeList = value; NotifyPropertyChanged(); }
        }

        public string BoroSiteCode
        {
            get => _boroSiteCode;
            set
            {
                _boroSiteCode = value;
                NotifyPropertyChanged();
            }
        }

        public string DeviceBoroSiteCode
        {
            get => _deviceBoroSiteCode;
            set
            {
                _deviceBoroSiteCode = value;
                NotifyPropertyChanged();
            }
        }

        public override bool ShowBackButton => !IsDepartmentPickerVisible;

        public override ICommand BackCommand => new Command(async () =>
        {
            IsDepartmentPickerVisible = true;
            IsInstructionTextVisible = false;
            IsPinEntryVisible = false;
            UserName = "Hold ID card against reader.";
            resetLogoVisible();
            IsNovasLogoVisible = true;
            SyncUserNFCIDText = "";
        });

        // http://jira.dsnyad.nycnet/browse/NH-1366 track sync status
        public string SyncStatus
        {
            get => syncStatus;
            set { syncStatus = value; NotifyPropertyChanged(); }
        }


        #endregion // Properties
    }
}