﻿using DSNY.Novas.Common;
using DSNY.Novas.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.ViewModels.Utils;

namespace DSNY.Novas.ViewModels
{
    public class ResidentialOwnerViewModel : ViewModelBase
    {
        private string _residentialOwner;
        private string _noOwnerFound; 
        public ResidentialOwnerViewModel()
        {
            _placeofOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
        }

        private readonly IPlaceOfOccurrenceService _placeofOccurrenceService;
        public override string Title
        {
            get
            {
                var title = "Residential Owner";
                if (NovMaster.NovInformation.TicketStatus == "C")
                {
                    title = "Residential Owner - Cancel";
                }
                else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    title = "Residential Owner - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }

        }

        public string ResidentialOwner
        {
            get => _residentialOwner;
            set { _residentialOwner = value; NotifyPropertyChanged(); }
        }

        public string NoOwnerFound
        {
            get => _noOwnerFound;
            set { _noOwnerFound = value; NotifyPropertyChanged(); }
        }

        public override async Task LoadAsync()
        {
            // to consider all three parametr of address and lowerhouseno and highhouseno range.
            //ResidentialOwner = (await _placeofOccurrenceService.GetPropertyDetails(NovMaster.NovInformation.PlaceStreetId, NovMaster.NovInformation.PlaceHouseNo, NovMaster.NovInformation.PlaceBoroCode))?.FirstName;
            ResidentialOwner = (await _placeofOccurrenceService.GetPropertyDetails(NovInformation.PlaceStreetId, NovInformation.PlaceHouseNo1, NovInformation.PlaceHouseNo2, NovInformation.PlaceHouseIndetifier, NovInformation.PlaceBoroCode))?.FirstName;

            /*if (NovMaster.NovInformation.IsPlaceAddressHit != "Y")
            {
                if (NovMaster.NovInformation.ViolationGroupId == 2 || NovMaster.NovInformation.ViolationGroupId == 3)
                {
                    ResidentialOwner = NovMaster.NovInformation.BusinessName;
                }
                else
                {
                    ResidentialOwner = (await _placeofOccurrenceService.GetPropertyDetails(NovMaster.NovInformation.PlaceStreetId, NovMaster.NovInformation.PlaceHouseNo1, NovMaster.NovInformation.PlaceHouseNo2, NovMaster.NovInformation.PlaceHouseIndetifier, NovMaster.NovInformation.PlaceBoroCode))?.FirstName;
                }
            }
            else
            {
                ResidentialOwner = NovMaster.NovInformation.BusinessName;
            }*/

            //var bblList = await _placeofOccurrenceService.GetAllBBLNumbers(NovMaster.NovInformation.PlaceBBL);
            if (!ResidentialOwner.In(new[] {"NO OWNER", "NOT ON FILE", null, ""}))
                return;

            NoOwnerFound = "No Owner information is on file.The ticket will be voided.";
            NovMaster.NovInformation.TicketStatus = "V";
            NotifyPropertyChanged(nameof(Title));
        }
        
        public override List<string> CancelMenuItems
        {
            get { return new List<string> { "Void" }; }
        }

    }
}
