﻿using DSNY.Novas.ViewModels.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class PickerSearchScreenViewModel : ViewModelBase
    {
        private string _title;
        private List<string> _dataSource;
        private List<string> _searchResults;
        private string _selectedItem;
        private string _initiallySelectedItem;
        private string _searchString;

        public override string Title => _title;

        public ICommand ItemSelectedCommand { get; set; }

        public PickerSearchScreenViewModel(string title, List<string> dataSource, string selectedItem, ICommand itemSelectedCommand)
        {
            _title = title;
            _dataSource = dataSource;
            _searchResults = dataSource;
            _initiallySelectedItem = selectedItem;
            ItemSelectedCommand = itemSelectedCommand;
        }

        public override void OnAppearing()
        {
            base.OnAppearing();

            _selectedItem = SearchResults?.FirstOrDefault(item => item.Equals(_initiallySelectedItem));
            NotifyPropertyChanged(SelectedItem);
        }

        public string SearchString
        {
            get => _searchString;
            set
            {
                _searchString = value;
                NotifyPropertyChanged();

                Search();
            }
        }

        public List<string> SearchResults
        {
            get => _searchResults;
            set { _searchResults = value; NotifyPropertyChanged(); }
        }

        public string SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                NotifyPropertyChanged();

                if (_selectedItem != null)
                {
                    OnSelectedCommand.Execute(null);
                }
            }
        }

        public void Search()
        {
            if (!string.IsNullOrEmpty(SearchString))
            {
                SearchResults = _dataSource.FindAll((string item) => item.ToUpper().Contains(SearchString.ToUpper()));
            }
            else
            {
                SearchResults = _dataSource;
            }

            if (!SearchResults.Contains(SelectedItem))
            {
                SelectedItem = null;
            }
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;

        public override bool ShowNextButton => false;

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public ICommand OnSelectedCommand => new Command(async () =>
        {
            ItemSelectedCommand?.Execute(SelectedItem);
            await NavigationService.PopModalAsync();
        });
    }
}
