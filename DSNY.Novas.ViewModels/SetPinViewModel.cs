﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Linq;

namespace DSNY.Novas.ViewModels
{
    public class SetPinViewModel : ViewModelBase
    {
        private readonly INovasUserService _userService;
        private readonly ILoginService _loginService;
        private readonly INFCReader _nfcReader;
        private bool _hasCompletedInitialLoad;
        private string _instructionText;
        private string _currentCardId;
        private bool _isCardRead;
        private bool _isCardRecognized;
        private NovasUser _selectedUser;
        private string _userTitle;
        private string _isUserSupervisor;
        private string _pin;
        private string _pin2;
        private byte[] _signatureBitmap;
        private List<NovasUser> _userMasterList; //this represents DBNovasUserMaster
        private List<Novas_User> _userList; //this represents DBNovasUser

        public override string Title => "Set Password";

        public SetPinViewModel()
        {
            _userService = DependencyResolver.Get<INovasUserService>();
            _loginService = DependencyResolver.Get<ILoginService>();
            _nfcReader = DependencyResolver.Get<INFCReader>();
        }

        public override async Task LoadAsync()
        {
            await base.LoadAsync();

            if (_hasCompletedInitialLoad) { return; }

            InstructionText = "Place ID card here to scan.";
            PIN = "";
            PIN2 = "";
            Signature = null;

            _userMasterList = await _userService.GetUsers();
            NotifyPropertyChanged(nameof(Users));

            //TODO: Fix the flow here --need to understand if Reset should also prompt the user to rescan their card.
            if (!await _nfcReader.IsNFCAvailable())
            {
                await AlertService.DisplayAlert(new AlertViewModel(string.Empty, "NFC is not supported on this device.  You will not be able to log in."));
                return;
            }

            if (!await _nfcReader.IsNFCEnabled())
            {
                await AlertService.DisplayAlert(new AlertViewModel(string.Empty, "NFC is not enabled.  You must enable NFC reading / sharing in Settings."));
                await _nfcReader.NavigateToNFCSettings();
                return;
            }

            await _nfcReader.StartListening();
            _hasCompletedInitialLoad = true;

            _nfcReader.CardRecognizedCommand = new Command(async () =>
            {
                await ValidateCardId();

                _userList = await _userService.GetNovasUsers();
                if (_userList.Exists(e => e.NfcUid.Equals(_currentCardId)))
                {

                    Novas_User lUser = _userList.FirstOrDefault(e => e.NfcUid.Equals(_currentCardId));


                    NovasUser lUserMaster = _userMasterList.FirstOrDefault(e => e.UserId.Equals(lUser.UserID));

                    SelectedUser = lUserMaster;
                    IsCardRecognized = false;
                    var t = _currentCardId;

                }
            });

            var j = _currentCardId;
            string b = string.Empty;

        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            _nfcReader.StopListening();
        }


        public string InstructionText
        {
            get => _instructionText;
            set { _instructionText = value; NotifyPropertyChanged(); }
        }

        public bool IsCardRead
        {
            get => _isCardRead;
            set { _isCardRead = value; NotifyPropertyChanged(); }
        }
        public bool IsCardRecognized
        {
            get => _isCardRecognized;
            set { _isCardRecognized = value; NotifyPropertyChanged(); }
        }


        
        public string PIN
        {
            get => _pin;
            set { _pin = value; NotifyPropertyChanged(); }
        }

        public string PIN2
        {
            get => _pin2;
            set { _pin2 = value; NotifyPropertyChanged(); }
        }

        public List<NovasUser> Users
        {
            get => _userMasterList;
            set { _userMasterList = value; NotifyPropertyChanged(); }
        }

        public NovasUser SelectedUser
        {
            get => _selectedUser;
            set
            {
                _selectedUser = value;
                if (_selectedUser != null)
                {
                    UserTitle = _selectedUser.Title;
                    IsUserSupervisor = _selectedUser.IsSupervisor;
                }

                NotifyPropertyChanged();
            }
        }

        public string UserTitle
        {
            get => "Title: " + _userTitle;
            set { _userTitle = value; NotifyPropertyChanged(); }
        }

        public string IsUserSupervisor
        {
            get => _isUserSupervisor == "Y" ? "Supervisor" : "";
            set { _isUserSupervisor = value; NotifyPropertyChanged(); }
        }
        public byte[] Signature
        {
            get => _signatureBitmap;
            set { _signatureBitmap = value; NotifyPropertyChanged(); }
        }

        public ICommand SubmitCommand => new Command(async () =>
        {
            if (Signature == null)
            {
                await AlertService.DisplayAlert(new AlertViewModel("Update Failed", "Please enter your signature"));
            }
            else if (IsPasswordValid() && Signature != null)
            {
                bool isUpdateSucessful = await _userService.UpdateUser(SelectedUser.UserId, PIN, _currentCardId, _signatureBitmap);

                if (isUpdateSucessful)
                {
                    await AlertService.DisplayAlert(new AlertViewModel("Update Successful", "User updated complete."));
                    await NavigationService.PopModalAsync();
                }
                else
                    await AlertService.DisplayAlert(new AlertViewModel("Update Failed", "User update could not be completed at this time."));
            } else
            {
                await AlertService.DisplayAlert(new AlertViewModel("Update Failed", "Passwords do not match."));
            }
        });

        public ICommand ResetCommand => new Command(async () =>
        {
            _hasCompletedInitialLoad = false; 
            IsCardRead = false;
            IsCardRecognized = false;
            await LoadAsync();
        });

        private Task ValidateCardId()
        {
            SelectedUser = null;
            UserTitle = null;
            IsUserSupervisor = null;
            if (!String.IsNullOrEmpty(_nfcReader.CurrentCardUID))
            {
                _currentCardId = _nfcReader.CurrentCardUID;
                IsCardRead = true;
                IsCardRecognized = true;
                InstructionText = "ID Card Read Successfully";
            }
            return Task.CompletedTask;
        }

        public override ICommand BackCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });

        public override bool ShowBackButton => true;

        private bool IsPasswordValid()
        {
            if (PIN.Equals(PIN2) && !String.IsNullOrEmpty(PIN) && !String.IsNullOrEmpty(PIN2) && PIN.Length >= 4 && PIN.Length <= 10)
            {
                return true;
            }

            return false;
        }
    }
}
