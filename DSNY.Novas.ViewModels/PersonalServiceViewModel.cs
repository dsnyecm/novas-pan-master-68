﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class PersonalServiceViewModel : ViewModelBase
    {
        private bool _isAtChecked;
        private bool _isRespondentChecked;
        private bool _isSuitableAgeChecked;
        private bool _isOfficerChecked;
        private bool _isDesignatedChecked;
        private bool _areOptionsEnabled;
        private bool _isStateOptionVisible;
        private string _serviceLocation;
        private string _didPersonallyLabel;
        private readonly ILookupService _lookupService;
        private string locationService; 

        public override string Title => "Personal Service";

        private static readonly IDictionary<string, string> OptionToServiceToMappings = new Dictionary<string, string>()
        {
            {"IsRespondentChecked", "0"},
            {"IsSuitableAgeChecked", "1"},
            {"IsOfficerChecked", "2"},
            {"IsDesignatedChecked", "3"},
            {"IsAtChecked", "4"}
        };

        private static readonly IDictionary<string, PropertyInfo> OptionsDictionary = typeof(PersonalServiceViewModel)
            .GetProperties().Where(p => p.Name.In(OptionToServiceToMappings.Keys))
            .ToDictionary(p => p.Name);

        private static readonly IEnumerable<Tuple<string, string>> IllegalDumpingTypes = new[]
        {
            new Tuple<string, string>("S14", "Illegal dumping - owner"),
            new Tuple<string, string>("S15", "Illegal dumping - affidavit")
        };

        public PersonalServiceViewModel()
        {
            _lookupService = DependencyResolver.Get<ILookupService>();
            IsAtChecked = true;
        }

        public override async Task LoadAsync()
        {
            if (NovInformation?.ViolationTypeId == "A" 
                && IllegalDumpingTypes.Contains(new Tuple<string, string>(NovInformation?.ViolationCode, NovInformation?.HHTIdentifier))
                && AffidavitOfService != null
                && AffidavitOfService.ServiceTo.In(new []{null,"0"}))
            {
                IsAtChecked = false;
                IsRespondentChecked = true;
            }

            var novNumber = NovNumber;
            locationService = $"PersonalService_LocationOfService_{novNumber}".ToCrossSettingsString("");

            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                DidPersonallyLabel = "I did personally";
                IsStateOptionVisible = false;
            }
            else
            {
                DidPersonallyLabel = "did personally";
                IsStateOptionVisible = true;
            }
                

            if (AffidavitOfService?.ServiceTo != null)
            {
                var isAtChecked = $"PersonalService_IsAtChecked_{novNumber}".ToCrossSettingsString("");
                if (isAtChecked != "")
                    IsAtChecked = Convert.ToBoolean(isAtChecked);

                switch (NovMaster.AffidavitOfService.ServiceTo)
                {
                    case "0":
                        var isRespondentChecked = $"PersonalService_IsRespondentChecked_{novNumber}".ToCrossSettingsString("");
                        if (isRespondentChecked != "")
                            IsRespondentChecked = Convert.ToBoolean(isRespondentChecked);
                        break;
                    case "1":
                        var isSuitableAge = $"PersonalService_IsSuitableAgeChecked_{novNumber}".ToCrossSettingsString("");
                        if (isSuitableAge != "")
                            IsSuitableAgeChecked = Convert.ToBoolean(isSuitableAge);
                        break;
                    case "2":
                        var isOfficer = $"PersonalService_IsOfficerChecked_{novNumber}".ToCrossSettingsString("");
                        if (isOfficer != "")
                            IsOfficerChecked = Convert.ToBoolean(isOfficer);
                        break;
                    case "3":
                        var isDesignated = $"PersonalService_IsDesignatedChecked_{novNumber}".ToCrossSettingsString("");
                        if (isDesignated != "")
                            IsDesignatedChecked = Convert.ToBoolean(isDesignated);
                        break;
                    default:
                        break;
                }

                if(!IsAtChecked)
                    ServiceLocation = locationService;
            }
        }

        public bool IsAtChecked
        {
            get => _isAtChecked;
            set
            {
                if (_isAtChecked == value)
                    return;

                AreOptionsEnabled = !value;

                ServiceLocation = value ? "T/P/O" : "";

                _isAtChecked = value;

                if (value)
                    SetSelectedOption(nameof(IsAtChecked));


                NotifyPropertyChanged();
            }
        }

        public bool IsRespondentChecked
        {
            get => _isRespondentChecked;
            set
            {
                if (_isRespondentChecked == value)
                    return;

                _isRespondentChecked = value;

                if (value)
                    SetSelectedOption(nameof(IsRespondentChecked));
                
                NotifyPropertyChanged();
            }
        }

        public bool IsSuitableAgeChecked
        {
            get => _isSuitableAgeChecked;
            set
            {
                if (_isSuitableAgeChecked == value)
                    return;

                _isSuitableAgeChecked = value;

                if (value)
                    SetSelectedOption(nameof(IsSuitableAgeChecked));

                NotifyPropertyChanged();
            }
        }

        public bool IsOfficerChecked
        {
            get => _isOfficerChecked;
            set
            {
                if (_isOfficerChecked == value)
                    return;

                _isOfficerChecked = value;

                if (value)
                    SetSelectedOption(nameof(IsOfficerChecked));

                NotifyPropertyChanged();
            }
        }

        public bool IsStateOptionVisible
        {
            get => _isStateOptionVisible;
            set { _isStateOptionVisible = value; NotifyPropertyChanged(); }
        }

        public string DidPersonallyLabel
        {
            get => _didPersonallyLabel;
            set { _didPersonallyLabel = value; NotifyPropertyChanged(); }
        }

        public bool IsDesignatedChecked
        {
            get => _isDesignatedChecked;
            set
            {
                if (_isDesignatedChecked == value)
                    return;

                _isDesignatedChecked = value;

                if (value)
                    SetSelectedOption(nameof(IsDesignatedChecked));

                NotifyPropertyChanged();
            }
        }

        public bool AreOptionsEnabled
        {
            get => _areOptionsEnabled;
            set
            {
                if (_areOptionsEnabled == value)
                    return;

                _areOptionsEnabled = value;
                
                if (!value)
                {
                    IsDesignatedChecked = false;
                    IsOfficerChecked = false;
                    IsRespondentChecked = false;
                    IsSuitableAgeChecked = false;
                }

                NotifyPropertyChanged();
            }
        }

        public string ServiceLocation
        {
            get => _serviceLocation;
            set
            {
                _serviceLocation = value;
                NotifyPropertyChanged();
            }
        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;

        public bool AnyOptionChecked => new[] { IsRespondentChecked, IsDesignatedChecked, IsOfficerChecked, IsSuitableAgeChecked }.Any(option => option);

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (string.IsNullOrWhiteSpace(ServiceLocation))
                alerts.Add(new AlertViewModel("12053", WorkFlowMessages.DSNYMSG_NOV053, elementName: "ServiceLocation"));

            if (!AreOptionsEnabled)
                return Task.FromResult(alerts);

            if(!AnyOptionChecked)
                alerts.Add(new AlertViewModel("12077", WorkFlowMessages.DSNYMSG_NOV077));

            return Task.FromResult(alerts);
        }

        public override ViewModelBase NextViewModel
        {
            get
            {
                if (AffidavitOfService != null) {

                    AffidavitOfService.ServedLocation = ServiceLocation;

                    if (IsAtChecked)
                        SetSelectedOption(nameof(IsAtChecked));
                }
                


                var novNumber = NovNumber;

                CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsAtChecked_{novNumber}", IsAtChecked.ToString());
                CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsRespondentChecked_{novNumber}", IsRespondentChecked.ToString());
                CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsSuitableAgeChecked_{novNumber}", IsSuitableAgeChecked.ToString());
                CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsOfficerChecked_{novNumber}", IsOfficerChecked.ToString());
                CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsDesignatedChecked_{novNumber}", IsDesignatedChecked.ToString());
                CrossSettings.Current.AddOrUpdateValue($"PersonalService_LocationOfService_{novNumber}", ServiceLocation);

                if (IsOfficerChecked)
                    return new OfficerAgentOtherViewModel { NovMaster = NovMaster };

                if (IsSuitableAgeChecked || IsDesignatedChecked)
                {
                    //return base.NextViewModel;
                    if(AffidavitOfService != null)
                        AffidavitOfService.IsPersonBeingServedCleared = true;

                    return new PersonBeingServed2ViewModel { NovMaster = NovMaster };
                }

                if(AffidavitOfService != null)
                    AffidavitOfService.IsPersonBeingServedCleared = false;

                return new PersonBeingServed2ViewModel { NovMaster = NovMaster };
            }
        }

        public virtual ICommand BackCommand => new Command(async () =>
        {
            if(AffidavitOfService != null)
                AffidavitOfService.ServedLocation = ServiceLocation;

            var novNumber = NovNumber;

            CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsAtChecked_{novNumber}", IsAtChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsRespondentChecked_{novNumber}", IsRespondentChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsSuitableAgeChecked_{novNumber}", IsSuitableAgeChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsOfficerChecked_{novNumber}", IsOfficerChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"PersonalService_IsDesignatedChecked_{novNumber}", IsDesignatedChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"PersonalService_LocationOfService_{novNumber}", ServiceLocation);

            if (NavigationInProgress)
                return;

            NavigationInProgress = true;

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            WriteFieldValuesToNovMaster();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
            //NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;
        });


        public override void WriteFieldValuesToNovMaster()
        {
            if (AffidavitOfService == null)
                return;

            AffidavitOfService.ServedLocation = ServiceLocation;
        }

        private void DoNotLoadFromAffidavit()
        {
            if (AffidavitOfService == null)
                return;

            AffidavitOfService.ShouldLoadFromAffidavit = false;
            AffidavitOfService.ShouldLoadFromAffidavitPersonID = false;
        }

        private void SetSelectedOption(string optionName)
        {
            //DoNotLoadFromAffidavit();

            var selectedOption = OptionsDictionary[optionName];
            selectedOption.SetValue(this, true); 

            if (AffidavitOfService != null)
            {

                try
                {

                    AffidavitOfService.ServiceTo = OptionToServiceToMappings.Keys.Contains(optionName) ? OptionToServiceToMappings[optionName] : "4";
                }
                catch (Exception ex)
                {

                }

            }


            foreach(var p in OptionsDictionary.Values.Except(new []{selectedOption}))
                p.SetValue(this,false);
        }
    }
}
