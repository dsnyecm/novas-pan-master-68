﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.ViewModels
{
    class ConcreteWorkflowConstants
    {

        public static Dictionary<string, bool> IsSaveTicketNextCommandAfterPersonCommercialIDViewModel = new Dictionary<string, bool>() {
            { "Action", true }, { "Property", true }, { "Commercial", true }, { "Vacantlot", false }, { "Posting", true }, { "Dcodes/Const", false}, { "Tobacco", false}
        };
        public static Dictionary<string, string> DefaultLicenseTypePersonCommercialIDViewModel = new Dictionary<string, string>() { // see BS_IDMatrix
            { "Action", "Current Drivers License" }, { "Property", "Current Drivers License" }, { "Commercial", "--" }, { "Vacantlot", "--" }, { "Posting", "--"}, { "Dcodes/Const", "--"}, { "Tobacco", "--"}
        };

        // NH-1265
        public static Dictionary<string, bool> IsAllowBackToNovSummaryAfterPlaceOfOccurrenceViewModel = new Dictionary<string, bool>() {
            { "Action", true }, { "Property", false }, { "Commercial", false }, { "Vacantlot", false }, { "Posting", false }, { "Dcodes/Const", true}, { "Tobacco", true}
        };

        // NH-1381
        // key: GroupID  e.g. "4": Posting 
        public static Dictionary<string, bool> IsLockFirstStreetPlaceOfOccurrenceViewModel = new Dictionary<string, bool>() {
            { "1", true }, { "2", true }, { "3", true }, { "4", false }, { "5", true }, { "6", true}, { "7", false}
        };

        // NH-1289
        // key: GroupID  e.g. "4": Posting 
        public static Dictionary<string, bool> IsAllowMakeSimilarNovSummaryViewModel = new Dictionary<string, bool>() {
            { "1", false }, { "2", false }, { "3", false }, { "4", true }, { "5", false }, { "6", false}, { "7", false}
        };

    }
}
