﻿using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class ServiceTypeViewModel : ViewModelBase
    {
        private bool _isBackEnabled;
        private bool _isAlternativeEnabled;
        private bool _isLaterEnabled;
        private bool _personalServiceChecked;
        private bool _alternativeServiceChecked;
        private bool _laterServiceChecked;
        private string _alternativeServiceTitle;
        private bool _isShowBackButton;
        private bool _doResetToPersonalServiceOverride;

        private readonly IAffirmationService _affirmationService;
        private readonly IPersonBeingServedService _personBeingServedService;

        public override string Title => "Service Type";

        public bool IsBackEnabled
        {
            get => _isBackEnabled;
            set { _isBackEnabled = value; NotifyPropertyChanged(); }
        }

        

        public ServiceTypeViewModel()
        {
            _doResetToPersonalServiceOverride = false; //set to false by default
            PersonalServiceChecked = true;
            _affirmationService = DependencyResolver.Get<IAffirmationService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
        }

        public override Task LoadAsync()
        {
            if (NovMaster.AffidavitOfServiceTranList == null)
            {
                NovMaster.AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>();
            }

            NovMaster.NovInformation.IsMultipleAttempts = NovMaster.NovInformation.ViolationGroupId == 5;
            
            if (NovMaster.NovInformation.ViolationGroupId == 5 && NovMaster.AffidavitOfServiceTranList.Count >= 2)
            {
                if (CheckVioloationCodeList(NovMaster.NovInformation.ViolationCode))
                {
                    IsLaterEnabled = false;// Disable the later service option if this is the 3rd attempt on a Vacant Lot
                }
            }
            else
            {
                IsLaterEnabled = true;
            }

            VacantLotBackButtonOverride(NovMaster.NovInformation.ViolationCode);

            if ((NovMaster.NovInformation.IsMultipleAttempts || NovMaster.NovInformation.TicketStatus == "T") && NovMaster.AffidavitOfServiceTranList.Count != 2)
            {
                if (NovMaster.AffidavitOfServiceTranList.Count == 0)
                {
                    AlternativeServiceTitle = "1st Service Attempt";
                    IsLaterEnabled = true;
                }
                else if (NovMaster.AffidavitOfServiceTranList.Count == 1)
                {
                    AlternativeServiceTitle = "2nd Service Attempt";
                    if (CheckVioloationCodeList(NovMaster.NovInformation.ViolationCode))
                    {
                        _doResetToPersonalServiceOverride = true;
                        PersonalServiceChecked = true;
                        IsLaterEnabled = false;
                    }
                }
            }
            else
            {
                AlternativeServiceTitle = "Alternative Service";
                if (CheckVioloationCodeList(NovMaster.NovInformation.ViolationCode))
                {
                    PersonalServiceChecked = true;
                    IsLaterEnabled = false;
                    _doResetToPersonalServiceOverride = true;
                }

            }

            var checkedField =  CrossSettings.Current.GetValueOrDefault($"ServiceType_AlternativeService_{NovMaster.NovInformation.NovNumber}", string.Empty);

            if (checkedField != null)
            {
                switch (checkedField)
                {
                    case "P":
                        PersonalServiceChecked = true;
                        break;
                    case "A":
                        // this is vor override condition to set checkbox to personal service from alternative service. OOriginally Alternative Service is selected based on Data. This override re-sets Alternative Service to Personal Service by Default on Second screen only
                        if (!_doResetToPersonalServiceOverride)
                            AlternativeServiceChecked = true;
                        break;
                    case "L":
                        LaterServiceChecked = true;
                        break;
                    default:
                        PersonalServiceChecked = true;
                        AlternativeServiceChecked = false;
                        LaterServiceChecked = false;
                        break;
                }
            }
            else
            {
                PersonalServiceChecked = true;
                AlternativeServiceChecked = false;
                LaterServiceChecked = false;
            }

            if (NovMaster.ViolationGroup.TypeName == "Posting" && checkedField == "")
            {
                AlternativeServiceChecked = true;
            }

            IsAlternativeEnabled = NovMaster.NovInformation.ViolationCode != "S08";

            if(NovMaster.UserSession.Department== SHERIFF_DEP)
            {
                IsAlternativeEnabled = false;
                IsLaterEnabled = false;
            }

            return Task.CompletedTask;
        }

        private void VacantLotBackButtonOverride(string pViolationCode)
        {
            IsShowBackButton = CheckVioloationCodeList(pViolationCode);
            NotifyPropertyChanged(nameof(ShowBackButton));
        }

        private bool CheckVioloationCodeList(string pViolationCode)
        {
            // I am listing this for te ease of maintenance of adding or removing codes
            return pViolationCode.In(new[] {"S21", "S6V", "S7V", "S8V", "SC7"});
        }

        public bool PersonalServiceChecked
        {
            get => _personalServiceChecked;
            set
            {
                if (value)
                {
                    AlternativeServiceChecked = false;
                    LaterServiceChecked = false;
                }
                _personalServiceChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool AlternativeServiceChecked
        {
            get => _alternativeServiceChecked;
            set
            {
                if (value)
                {
                    PersonalServiceChecked = false;
                    LaterServiceChecked = false;
                }
                _alternativeServiceChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool LaterServiceChecked
        {
            get => _laterServiceChecked;
            set
            {
                if (value)
                {
                    PersonalServiceChecked = false;
                    AlternativeServiceChecked = false;
                }
                _laterServiceChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsLaterEnabled
        {
            get => _isLaterEnabled;
            set
            {
                _isLaterEnabled = value; NotifyPropertyChanged();
            }
        }

        public bool IsAlternativeEnabled
        {
            get => _isAlternativeEnabled;
            set
            {
                _isAlternativeEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsShowBackButton
        {
            get => _isShowBackButton;
            set { _isShowBackButton = value; NotifyPropertyChanged(); }
        }

        public override bool ShowBackButton
        {
            get
            {
                if (IsShowBackButton)
                {
                    return true;
                }
                return false;
            }
        }

        public string AlternativeServiceTitle
        {
            get => _alternativeServiceTitle;
            set { _alternativeServiceTitle = value; NotifyPropertyChanged(); }
        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;

        public override ICommand NextCommand => new Command(async () =>
        {
            try
            {

           
                if (LaterServiceChecked)
                {
                    if (NavigationInProgress) { return; }
                    NavigationInProgress = true;

                    NovMaster.NovInformation.TicketStatus = "L";
                    await SaveTicket();

                    await NavigationService.PopToSummaryScreenAsync();

                    NavigationInProgress = false;
                }
                else
                {
                    if (NovMaster.UserSession.Department != SHERIFF_DEP)
                    {
                        var courtLocation = await _affirmationService.GetCourtLocation(NovMaster.ViolationDetails.ViolationCode, NovMaster.NovInformation.PlaceBoroCode);
                        if (courtLocation == null)
                        {
                            NovMaster.AffidavitOfService.Comments = "at any OATH Location";
                        }
                        else
                        {
                            var boros = await _personBeingServedService.GetBoros();
                            NovMaster.AffidavitOfService.Comments = string.Format("at the {0} OATH Location", boros.Find(_ => _.BoroId.Equals(courtLocation.CourtBoroCode)).Name);
                        }
                    }

                    base.NextCommand.Execute(true);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        });

        public override void WriteFieldValuesToNovMaster()
        {

            /*
             ****** This moved to AffermativeService.SignNov Since it will be a part of signing nov. ******
             * */

            //if MDR is 0 then LicenseNumber and issue agency should be empty, this is to matht old handheld device
            // this change will take care  repoca as well.
            //if(!string.IsNullOrEmpty(NovMaster.NovInformation.LicenseNumber) || !string.IsNullOrEmpty(NovMaster.NovInformation.LicenseTypeDesc))
            //{ 
            //    if (NovMaster.NovInformation.LicenseTypeDesc.Trim().ToLower() == "multiple dwelling registration" && NovMaster.NovInformation.LicenseNumber.ToString() == "0")
            //    {
            //        NovMaster.NovInformation.LicenseTypeDesc = "";
            //        NovMaster.NovInformation.LicenseAgency = "";
            //        NovMaster.NovInformation.LicenseNumber = "";
            //    }
            //}
            if (PersonalServiceChecked)
            {
                NovMaster.NovInformation.AlternateService = "P";
            }
            else if (AlternativeServiceChecked)
            {
                NovMaster.NovInformation.AlternateService = "A";
            }
            else if (LaterServiceChecked)
            {
                NovMaster.NovInformation.AlternateService = "L";
            }
            NovMaster.AffidavitOfService.NovNumber = NovMaster.NovInformation.NovNumber;
            CrossSettings.Current.AddOrUpdateValue("ServiceType_AlternativeService_" + NovMaster.NovInformation.NovNumber.ToString(), NovMaster.NovInformation.AlternateService);
        }
    }
}




