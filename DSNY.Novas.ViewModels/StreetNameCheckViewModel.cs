﻿using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Services;
using System.Collections.ObjectModel;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class StreetNameCheckViewModel : ViewModelBase
    {
        private ObservableCollection<StreetCodeMaster> _searchResults;
        private StreetCodeMaster _selectedStreetName;
        private string _searchString;

        private readonly IPlaceOfOccurrenceService _placeOfOccurrenceService;

        public override string Title
        {
            get
            {
                var title = "Street Name Check";
                if (IsCancelled || NovMaster.NovInformation.TicketStatus == "C")
                {
                    title = "Street Name Check - Cancel";
                }
                else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    title = "Street Name Check - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;

            }
        }
        
        public ICommand StreetNameSearchCommand { get; set; }
        public ICommand StreetNameSelectedCommand { get; set; }

        public StreetNameCheckViewModel()
        {
            _placeOfOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
            StreetNameSearchCommand = new Command<StreetCodeMaster>(async _ => await ExecuteSearch());
        }
        
        public string SearchString
        {
            get => _searchString;
            set
            {
                _searchString = value;
                NotifyPropertyChanged();

                StreetNameSearchCommand.Execute(null);
            }
        }

        public string BoroId { get; set; }

        public ObservableCollection<StreetCodeMaster> SearchResults
        {
            get => _searchResults;
            set { _searchResults = value; NotifyPropertyChanged(); }
        }

        public StreetCodeMaster SelectedStreetName
        {
            get => _selectedStreetName;
            set
            {
                _selectedStreetName = value;
                NotifyPropertyChanged();

               // if (_selectedStreetName != null)
                //{
                 //   NextCommand.Execute(null);
               // }
            }
        }

        public async Task ExecuteSearch()
        {
            var boroId = CrossSettings.Current.GetValueOrDefault("PlaceOfOccurrence_BoroId_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);

            if (!string.IsNullOrEmpty(SearchString))
            {
                if (!string.IsNullOrEmpty(BoroId))
                {
                    var results = await _placeOfOccurrenceService.GetStreetSearchResults(SearchString, BoroId);
                    SearchResults = new ObservableCollection<StreetCodeMaster>(results);
                }
                else if(!string.IsNullOrEmpty(boroId))
                {
                    var results = await _placeOfOccurrenceService.GetStreetSearchResults(SearchString, boroId);
                    SearchResults = new ObservableCollection<StreetCodeMaster>(results);
                }
            }
            else
            {
                SearchResults = null;
            }
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;

        public override bool ShowNextButton => false;
        public override bool ShowConfirmButton => true;

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public override ICommand NextCommand => new Command(async () =>
        {
            StreetNameSelectedCommand?.Execute(SelectedStreetName);
            await NavigationService.PopModalAsync();
        });
    }
}
