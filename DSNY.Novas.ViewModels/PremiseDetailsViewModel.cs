﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class PremiseDetailsViewModel : ViewModelBase
    {
        private bool _isAtChecked;
        private string _serviceLocation;
        private bool _isNotAvailableChecked;
        private bool _isLockedChecked;
        private bool _isEnteredChecked;
        private bool _isServiceLocationEnabled;

        public PremiseDetailsViewModel()
        {
            IsAtChecked = true;
            IsServiceLocationEnabled = false;
            IsLockedChecked = true;
        }
        public override string Title => "Premise Details";

        public override async Task LoadAsync()
        {
            var isLocked1 = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsLockedChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
            var isEntered1 = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsEnteredChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
            if (!String.IsNullOrEmpty(isLocked1))
                IsLockedChecked = Convert.ToBoolean(isLocked1);
            if (!String.IsNullOrEmpty(isEntered1))
                IsEnteredChecked = Convert.ToBoolean(isEntered1);

            if (NovMaster?.AffidavitOfService?.AlternativeService1 != null && IsAtChecked == false)
            {
                var isAtChecked = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsAtChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
                var isNotAvailable = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsNotAvailableChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
                var isEntered = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsEnteredChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
                var isLocked = CrossSettings.Current.GetValueOrDefault("PremiseDetails_IsLockedChecked_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
                var locationOfService = CrossSettings.Current.GetValueOrDefault("PremiseDetails_LocationOfService_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);

                 switch (NovMaster.AffidavitOfService.AlternativeService1)
                  {
                      case "1":
                          IsNotAvailableChecked = Convert.ToBoolean(isNotAvailable);
                          break;
                      case "2":
                          IsLockedChecked = Convert.ToBoolean(isLocked); 
                          break;
                      case "3":
                          IsEnteredChecked = Convert.ToBoolean(isEntered); 
                          break;
                      default:
                          break;
                  }

                ServiceLocation = locationOfService;

                // return Task.CompletedTask;
            }
        }

        public bool IsAtChecked
        {
            get => _isAtChecked;
            set
            {
                _isAtChecked = value;
                if (value)
                {
                    ServiceLocation = "T/P/O";
                    IsNotAvailableChecked = false;
                    IsEnteredChecked = false;
                }
                else
                {
                    IsServiceLocationEnabled = true;
                    ServiceLocation = "";
                }
                NotifyPropertyChanged();
            }
        }
        public string ServiceLocation
        {
            get => _serviceLocation;
            set
            {
                _serviceLocation = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsNotAvailableChecked
        {
            get => _isNotAvailableChecked;
            set
            {
                _isNotAvailableChecked = value;
                if (value)
                {
                    IsLockedChecked = false;
                    IsEnteredChecked = false;
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsLockedChecked
        {
            get => _isLockedChecked;
            set
            {
                _isLockedChecked = value;
                if (value)
                {
                    IsNotAvailableChecked = false;
                    IsEnteredChecked = false;
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsEnteredChecked
        {
            get => _isEnteredChecked;
            set
            {
                _isEnteredChecked = value;
                if (value)
                {
                    IsNotAvailableChecked = false;
                    IsLockedChecked = false;
                }

                NotifyPropertyChanged();
            }
        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;

        public bool IsServiceLocationEnabled
        {
            get => _isServiceLocationEnabled;
            set
            {
                _isServiceLocationEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();
            if (String.IsNullOrWhiteSpace(ServiceLocation))
            {
                alerts.Add(new AlertViewModel("12053", WorkFlowMessages.DSNYMSG_NOV053, elementName: "ServiceLocation"));
            }

            return Task.FromResult(alerts);
        }

        public override ViewModelBase NextViewModel
        {
            get
            {
                if (IsEnteredChecked)
                {
                    return new PremisePersonViewModel { NovMaster = NovMaster };
                }
                return base.NextViewModel;
            }
        }
        
        public override void WriteFieldValuesToNovMaster()
        {
            NovMaster.AffidavitOfService.AlternativeService1 = IsNotAvailableChecked ? "1" : IsLockedChecked ? "2" : "3";
            
            NovMaster.AffidavitOfService.ServedLocation = ServiceLocation;

            // NH-1309
            resetPremisePersonFieldsInNovMasterAOS();

            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_AlternativeService1_" + NovMaster.NovInformation.NovNumber.ToString(), NovMaster.AffidavitOfService.AlternativeService1.ToString());
            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_IsAtChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsAtChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_IsNotAvailableChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsNotAvailableChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_IsEnteredChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsEnteredChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_IsLockedChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsLockedChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PremiseDetails_LocationOfService_" + NovMaster.NovInformation.NovNumber.ToString(), ServiceLocation);


        }
        // NH-1309
        private void resetPremisePersonFieldsInNovMasterAOS() {
            if (IsLockedChecked) {
                // flow1 : IsLockedChecked : screen PremiseDetails -> screen Alt. Affirmation
                // flow2 : IsEnteredChecked : screen PremiseDetails -> screen PremisePerson -> screen Alt. Affirmation
                // reset Fields on screen PremisePerson when IsLockedChecked 
                NovMaster.AffidavitOfService.AlternativeService2 = "";
                NovMaster.AffidavitOfService.PremiseFName = "";
                NovMaster.AffidavitOfService.PremiseLName = "";
                NovMaster.AffidavitOfService.PremiseMInit = "";
            }
        }
    }
}
