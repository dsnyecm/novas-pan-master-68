﻿using System;

namespace DSNY.Novas.ViewModels
{
    public class AlertViewModel
    {
        public AlertViewModel() { }

        public AlertViewModel(string title, string message, string okTitle = "Ok", Action okAction = null, string cancelTitle = null, Action cancelAction = null, bool shouldContinueOnOk = false, string elementName = null)
        {
            Title = title;
            Message = message?.Replace("\\n", "\n");
            OkTitle = okTitle;
            OkAction = okAction;
            CancelTitle = cancelTitle;
            CancelAction = cancelAction;
            ShouldContinueOnOk = shouldContinueOnOk;
            ElementName = elementName;
            


        }

        public string Title { get; set; }
        public string Message { get; set; }
        public string OkTitle { get; set; }
        public Action OkAction { get; set; }
        public string CancelTitle { get; set; }
        public Action CancelAction { get; set; }
        public bool ShouldContinueOnOk { get; set; }
        public string ElementName { get; set; }
    }
}
