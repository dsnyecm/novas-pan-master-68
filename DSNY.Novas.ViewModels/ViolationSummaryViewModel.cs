﻿using DSNY.Novas.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels
{
    public class ViolationSummaryViewModel : ViewModelBase
    {
        private string _novNumber;
        private string _dateOfOffense;
        private string _placeOfOccurrence;
        private string _violationScript;
        private string _serviceAttempts;
        private string _name;

        public override string Title => "NOV Summary";

        public ViolationSummaryViewModel()
        {

        }

        public override Task LoadAsync()
        {
            NovNumber = Convert.ToString(NovMaster.NovInformation.NovNumber);
            DateOfOffense = String.Format(NovMaster.NovInformation.IssuedTimestamp.ToString("MM/dd/yyyy hh:mm tt"));
            PlaceOfOccurrence = NovMaster.NovInformation.DisplayAddress;
            ViolationScript = NovMaster.NovInformation.ViolationScript;
            if (!String.IsNullOrEmpty(NovMaster.NovInformation.Resp1LastName) && String.IsNullOrEmpty(NovMaster.NovInformation.Resp1MiddleInitial))
            {
                Name = String.Format("{0} {1}", NovMaster.NovInformation.Resp1FirstName, NovMaster.NovInformation.Resp1LastName);
            }
            else if (!String.IsNullOrEmpty(NovMaster.NovInformation.Resp1LastName) && !String.IsNullOrEmpty(NovMaster.NovInformation.Resp1MiddleInitial))
            {
                Name = String.Format("{0} {1} {2}", NovMaster.NovInformation.Resp1FirstName, NovMaster.NovInformation.Resp1MiddleInitial, NovMaster.NovInformation.Resp1LastName);
            }
            else if (!String.IsNullOrEmpty(NovMaster.NovInformation.BusinessName))
            {
                Name = String.Format("{0}", NovMaster.NovInformation.BusinessName);
            }
            if (NovMaster.AffidavitOfServiceTranList.Count >= 1)
            {
                ServiceAttempts = String.Format("1st - {0}\n", NovMaster.AffidavitOfServiceTranList[0].SignDate.ToString("MM/dd/yyyy hh:mm tt"));
                if (NovMaster.AffidavitOfServiceTranList.Count == 2)
                {
                    ServiceAttempts += String.Format("2nd - {0}\n", NovMaster.AffidavitOfServiceTranList[1].SignDate.ToString("MM/dd/yyyy hh:mm tt"));
                }
            }

            return Task.CompletedTask;
        }

        public string NovNumber
        {
            get => _novNumber;
            set { _novNumber = value; NotifyPropertyChanged(); }
        }

        public string DateOfOffense
        {
            get => _dateOfOffense;
            set { _dateOfOffense = value; NotifyPropertyChanged(); }
        }

        public string PlaceOfOccurrence
        {
            get => _placeOfOccurrence;
            set { _placeOfOccurrence = value; NotifyPropertyChanged(); }
        }

        public string ViolationScript
        {
            get => _violationScript;
            set { _violationScript = value; NotifyPropertyChanged(); }
        }

        public string Name
        {
            get => _name;
            set { _name = value; NotifyPropertyChanged(); }
        }
        public string ServiceAttempts
        {
            get => _serviceAttempts;
            set { _serviceAttempts = value; NotifyPropertyChanged(); }
        }

        public override bool ShowCancelMenu
        {
            get
            { return false; }
        }
        public override ViewModelBase NextViewModel
        {
            get
            {
                return new ServiceTypeViewModel() { NovMaster = NovMaster };
            }
        }
    }
}
