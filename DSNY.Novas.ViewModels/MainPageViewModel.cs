﻿using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {

        private List<string> _syncServicePickerItems;
        private string _selectedSyncServiceItems;
        private bool _isSyncServicePickerVisible;
        public override bool ShowMenuButton => false;

        public override bool ShowNextButton => false;

        public MainPageViewModel()
        {
            try
            {
                var apiUrl = new ApiUrl();

                var urls=Task.Run(() => apiUrl.GetAllServicUrls()).Result;
                 SyncServicePickerItems = urls.ToList();
                string SyncServiceUrlLocal = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);
                var indx = SyncServicePickerItems.IndexOf(SyncServiceUrlLocal);
                if(indx ==-1 && !string.IsNullOrEmpty(SyncServiceUrlLocal))
                {
                    SyncServicePickerItems.Add(SyncServiceUrlLocal);
               
                }
                SelectedSyncServiceItems = SyncServiceUrlLocal;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        public override ICommand BackCommand => new Command(async () => {
            await NavigationService.PopModalAsync();
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });

        public List<string> SyncServicePickerItems
        {
            get => _syncServicePickerItems;
            set
            {
                if (_syncServicePickerItems?.Equals(value) == true)
                    return;

                _syncServicePickerItems = value;
                NotifyPropertyChanged();
               
            }
        }

        public string SelectedSyncServiceItems
        {
            get => _selectedSyncServiceItems;
            set { _selectedSyncServiceItems = value;
                CrossSettings.Current.AddOrUpdateValue("SyncServiceUrl", _selectedSyncServiceItems);
                NotifyPropertyChanged();
            }
        }
    }
}
