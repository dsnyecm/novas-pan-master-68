﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.ViewModels
{
    public class OfficerAgentOtherViewModel : ViewModelBase
    {
        private bool _isOfficerChecked;
        private bool _isDirectorChecked;
        private bool _isManagingAgentChecked;
        private bool _isOtherChecked;
        private string _otherServedText;

        public override string Title => "Officer/Agent/Other";

        public OfficerAgentOtherViewModel()
        {
            IsOfficerChecked = true;
        }
        public bool IsOfficerChecked
        {
            get => _isOfficerChecked;
            set
            {
                if (value)
                {
                    IsDirectorChecked = false;
                    IsManagingAgentChecked = false;
                    IsOtherChecked = false;
                    OtherServedText = null;
                }
                _isOfficerChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsDirectorChecked
        {
            get => _isDirectorChecked;
            set
            {
                if (value)
                {
                    IsOfficerChecked = false;
                    IsManagingAgentChecked = false;
                    IsOtherChecked = false;
                    OtherServedText = null;
                }
                _isDirectorChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsManagingAgentChecked
        {
            get => _isManagingAgentChecked;
            set
            {
                if (value)
                {
                    IsDirectorChecked = false;
                    IsOfficerChecked = false;
                    IsOtherChecked = false;
                    OtherServedText = null;
                }
                _isManagingAgentChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsOtherChecked
        {
            get => _isOtherChecked;
            set
            {
                if (value)
                {

                    IsDirectorChecked = false;
                    IsManagingAgentChecked = false;
                    IsOfficerChecked = false;
                    OtherServedText = null;
                }
                _isOtherChecked = value;
                NotifyPropertyChanged();
            }
        }

        public string OtherServedText
        {
            get => _otherServedText;
            set
            {
                if (_isOtherChecked)
                {
                    _otherServedText = value;
                    NotifyPropertyChanged();
                }
            }

        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;
        public override ViewModelBase NextViewModel
        {
            get
            {
                NovMaster.AffidavitOfService.IsPersonBeingServedCleared = true;
                return new PersonBeingServed2ViewModel() {NovMaster = NovMaster};
            }
        }


        public override void WriteFieldValuesToNovMaster()
        {
            NovMaster.AffidavitOfService.ServedTitle = IsOfficerChecked
                ? "0"
                : IsDirectorChecked
                    ? "1"
                    : IsManagingAgentChecked
                        ? "2"
                        : "3";
            NovMaster.AffidavitOfService.ServedTitleOther = OtherServedText;

        }

    }
}
