﻿
using System.IO;
using System.Reflection;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;

namespace DSNY.Novas.ViewModels
{
    public class HelpViewModel: ViewModelBase
    {
        private string _htmlString = "";
        public override string Title => "NOVAS Help";
        public HelpViewModel()
        {
            Assembly assembly = typeof(HelpViewModel).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("DSNY.Novas.ViewModels.Help.NovasHelp.htm");
            using (var reader = new StreamReader(stream))
            {
                HtmlString = reader.ReadToEnd();

            }
        }

        public string HtmlString
        {
            get => _htmlString;
            set { _htmlString = value; NotifyPropertyChanged(); }
        }

        public override bool ShowMenuButton => false;

        public override bool ShowNextButton => false;

        public override ICommand BackCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });
    }
}
