﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class GroupTypeViewModel : ViewModelBase
    {
        private List<ViolationGroups> _violationGroups;
        private List<ViolationDetails> _violationDetails;
        private List<ViolationTypes> _violationTypes;
        private ViolationTypes _violationTypesSelectedItem;
        private ViolationGroups _violationGroupsSelectedItem;
        private ViolationDetails _violationDetailsSelectedItem;
        private ViolationLawCodes _selectedViolationLawCode;
        private string _summaryOfLaw;
        private List<CodeLaw> _codeLaws;
        private bool _isPresentingViolationSearchModal;
        private bool _isEnabled;
        private bool _isShowBackButton;
        private string nextNovNum;
        private string showVoid = "";

        public override bool ShouldSaveTicketOnNext => false;

        public ICommand ViolationGroupSelectedCommand { get; set; }
        public ICommand ViolationTypeSelectedCommand { get; set; }
        public ICommand ViolationDetailsSelectedCommand { get; set; }

        public ICommand ViolationSearchPressedCommand => new Command(async () =>
        {
            _isPresentingViolationSearchModal = true;
            var novMast = NovMaster;
            ViolationCodeSearchViewModel vm = new ViolationCodeSearchViewModel { Nov = novMast, UserSession = novMast.UserSession, GroupId = ViolationGroupsSelectedItem?.ViolationGroupId ?? 0, GroupName = ViolationGroupsSelectedItem?.GroupName };
            await NavigationService.PushModalAsync(vm);
        }
        );

        private readonly INovService _novService;
        private DateTime _systemTime;
        private DateTime _occurrenceDate;
        private TimeSpan _occurrenceTime;

        private bool _isLoading;
        private bool _hasCompletedInitialLoad;
        private bool _hasCompletedInitialLoadForViolations;

        public GroupTypeViewModel()
        {
            _novService = DependencyResolver.Get<INovService>();
            ViolationGroupSelectedCommand = new Command<ViolationGroups>(async (_) => {
                _isLoading = true;

                _violationTypes = await _novService.GetViolationTypes(_violationGroupsSelectedItem.ViolationGroupId);
                NotifyPropertyChanged(nameof(ViolationTypes));

                _violationTypesSelectedItem = ViolationTypes.FirstOrDefault();
                NotifyPropertyChanged(nameof(ViolationTypesSelectedItem));

                _violationDetails = await _novService.GetViolationDetailsByGroupAndName(_violationGroupsSelectedItem.ViolationGroupId, _violationGroupsSelectedItem.GroupName);
                NotifyPropertyChanged(nameof(ViolationDetails));

                _violationDetailsSelectedItem = ViolationDetails.FirstOrDefault();
                NotifyPropertyChanged(nameof(ViolationDetailsSelectedItem));
                await SetSummaryOfLaw(_violationDetailsSelectedItem);

                _isLoading = false;
            });
            ViolationDetailsSelectedCommand = new Command<ViolationDetails>(async (_) => await SetSummaryOfLaw(_));
        }

        public override string Title
        {
            get
            {
                var title = "Group - Type - Code";
                if (IsCancelled || NovMaster.NovInformation.TicketStatus == "C")
                {
                    NovMaster.NovInformation.TicketStatus = "C";
                    title = "Group - Type - Code - Cancel";
                }
                else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    NovMaster.NovInformation.TicketStatus = "V";
                    title = "Group - Type - Code - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }
        }

        public override async Task LoadAsync()
        {
            nextNovNum = (await _novService.GetNextNovNumber(true)).ToString();

            if (nextNovNum != null)
            {
                showVoid = CrossSettings.Current.GetValueOrDefault("ShowVoid_" + nextNovNum, string.Empty);

            }

            NotifyPropertyChanged(nameof(Title));
            NotifyPropertyChanged(nameof(ShowCancelMenu));
            NotifyPropertyChanged(nameof(MenuItems));
            NotifyPropertyChanged(nameof(CancelMenuItems));
            _systemTime = TimeManager.Now;
            NotifyPropertyChanged(nameof(SystemDateTime));

            if (NovMaster.NovInformation.LockPreviousScreens || NovMaster.NovInformation.LockPlaceOfOccurrenceScreen)
            {
                IsEnabled = false;
                showVoid = "true";
            }
            else
            {
                IsEnabled = true;
                showVoid = "";
            }

            var completeViewStack = NavigationService.CompleteViewStack().ToList();
            //if (
            //    (NovMaster.NovInformation.NovNumber == new NovInformation().NovNumber && completeViewStack.Contains("PlaceOfOccurrenceScreen") && NovMaster.NovInformation.PlaceStreetId != new NovInformation().PlaceStreetId /* for New Violation scenario */) ||
            //    (completeViewStack.Contains("PlaceOfOccurrenceScreen") && completeViewStack.IndexOf("PlaceOfOccurrenceScreen") != completeViewStack.Count - 1 /* for Make Similar scenario */)
            //)
            if ((NovMaster.NovInformation.NovNumber == new NovInformation().NovNumber && NovMaster.NovInformation.PlaceStreetId != new NovInformation().PlaceStreetId /* for New Violation scenario */))
            {
                IsShowBackButton = false;
            }
            else
            {
                if (NovMaster.NovInformation.LockPreviousScreens)
                {
                    if ((completeViewStack.Contains("PlaceOfOccurrenceScreen") && completeViewStack.IndexOf("PlaceOfOccurrenceScreen") == completeViewStack.Count - 1))
                        IsShowBackButton = true;
                    else
                        IsShowBackButton = false;
                }
                else
                {
                    IsShowBackButton = true;
                }
            }
            // NH-1265
            if (!getIsAllowBackToNovSummary(true)) {
                IsShowBackButton = false;
            }

            NotifyPropertyChanged(nameof(ShowBackButton));


            if (_hasCompletedInitialLoad)
            {
                return;
            }

            _isLoading = true;

            if (ViolationGroups == null)
            {
                _violationGroups = await _novService.GetViolationGroups();
                if (NovMaster.UserSession.Department == SHERIFF_DEP)
                {
                    _violationGroups = ViolationGroups.Where(violationGroup => violationGroup.ViolationGroupId == 7).ToList();
                }
                else
                {
                    _violationGroups = ViolationGroups.Where(violationGroup => violationGroup.ViolationGroupId != 7).ToList();
                }
                NotifyPropertyChanged(nameof(ViolationGroups));
            }

            // Populate screen with current values from NovMaster
            var novInfo = NovMaster.NovInformation;
            if (NovMaster.NovInformation.TicketStatus != "I" || NovMaster.IsMakeSimilar)
            {
                OccurrenceDate = novInfo.IssuedTimestamp.Date;
                OccurrenceTime = novInfo.IssuedTimestamp.TimeOfDay;
            }
            else
            {
                OccurrenceDate = TimeManager.Now.Date;
                // NH-1434 2018-11-13
                OccurrenceTime = TimeManager.Now.TimeOfDay.Subtract(new TimeSpan(0, 1, 0));
                //OccurrenceTime = TimeManager.Now.TimeOfDay.Add(TimeManager.CurrentOffset);  // HHT timestamp
                //OccurrenceTime = TimeManager.Now.TimeOfDay;  // BoroServer timestamp
            }

            if (novInfo.ViolationGroupId != 0)
            {
                if(NovMaster.UserSession.Department == SHERIFF_DEP) //make similar 
                    _violationGroupsSelectedItem = ViolationGroups.Find((violationGroup) => violationGroup.ViolationGroupId == novInfo.ViolationGroupId && violationGroup.GroupName[0].Equals(novInfo.ViolationTypeId[0]));
                else
                    _violationGroupsSelectedItem = ViolationGroups.Find((violationGroup) => violationGroup.ViolationGroupId == novInfo.ViolationGroupId && violationGroup.GroupName[0].Equals(novInfo.ViolationCode[0]));

                _violationTypes = await _novService.GetViolationTypes(_violationGroupsSelectedItem.ViolationGroupId);
                NotifyPropertyChanged(nameof(ViolationGroupsSelectedItem));
                NotifyPropertyChanged(nameof(ViolationTypes));

                if (!string.IsNullOrEmpty(novInfo.ViolationTypeId))
                {
                    _violationTypesSelectedItem = ViolationTypes.Find((violationType) => violationType.ViolationTypeId.Equals(novInfo.ViolationTypeId));
                }
                else
                {
                    _violationTypesSelectedItem = ViolationTypes.FirstOrDefault();
                }
                NotifyPropertyChanged(nameof(ViolationTypesSelectedItem));

                _violationDetails = await _novService.GetViolationDetailsByGroupAndName(_violationGroupsSelectedItem.ViolationGroupId, _violationGroupsSelectedItem.GroupName);
                NotifyPropertyChanged(nameof(ViolationDetails));

                if (!string.IsNullOrEmpty(novInfo.ViolationCode))
                {
                    _violationDetailsSelectedItem = ViolationDetails.Find((violationDetails) => violationDetails.ViolationCode.Equals(novInfo.ViolationCode) && violationDetails.HHTIdentifier.Equals(novInfo.HHTIdentifier));
                    await SetSummaryOfLaw(_violationDetailsSelectedItem);
                }
                else
                {
                    _violationDetailsSelectedItem = ViolationDetails.FirstOrDefault();
                }
                NotifyPropertyChanged(nameof(ViolationDetailsSelectedItem));
            }

            _codeLaws = await _novService.GetCodeLaws();

            _isLoading = false;
            _hasCompletedInitialLoad = true;
        }

        public string SystemDateTime => $"System Time:  {_systemTime}";

        public List<ViolationGroups> ViolationGroups
        {
            get => _violationGroups;
            set { _violationGroups = value; NotifyPropertyChanged(); }
        }

        public List<ViolationTypes> ViolationTypes
        {
            get => _violationTypes;
            set { _violationTypes = value; NotifyPropertyChanged(); }
        }

        public List<ViolationDetails> ViolationDetails
        {
            get => _violationDetails;
            set { _violationDetails = value; NotifyPropertyChanged(); }
        }

        public DateTime OccurrenceDate
        {
            get => _occurrenceDate;
            set { _occurrenceDate = value; NotifyPropertyChanged(); }
        }

        public TimeSpan OccurrenceTime
        {
            get => _occurrenceTime;
            set { _occurrenceTime = value; NotifyPropertyChanged(); }
        }

        public string SummaryOfLaw
        {
            get => _summaryOfLaw;
            set
            {
                _summaryOfLaw = value;
                NotifyPropertyChanged();
            }
        }

        public ViolationGroups ViolationGroupsSelectedItem
        {
            get => _violationGroupsSelectedItem;

            set
            {
                if (!_isLoading)
                {
                    _violationGroupsSelectedItem = value;
                    ViolationGroupSelectedCommand.Execute(value);
                    if (NovMaster!=null && NovMaster.NovInformation!=null && NovMaster.NovInformation.ViolationGroupId != value.ViolationGroupId)
                        resetNovInformation();
                    NotifyPropertyChanged();
                }
            }
        }

        public ViolationTypes ViolationTypesSelectedItem
        {
            get => _violationTypesSelectedItem;

            set
            {
                if (!_isLoading)
                {
                    _violationTypesSelectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ViolationDetails ViolationDetailsSelectedItem
        {
            get => _violationDetailsSelectedItem;

            set
            {
                if (!_isLoading)
                {
                    _violationDetailsSelectedItem = value;
                    ViolationDetailsSelectedCommand.Execute(value);
                    NotifyPropertyChanged();
                }
            }
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set { _isEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsShowBackButton
        {
            get => _isShowBackButton;
            set { _isShowBackButton = value; NotifyPropertyChanged(); }
        }

        public override bool ShowBackButton
        {
            get
            {
                if (IsShowBackButton)
                {
                    return true;
                }
                return false;
            }
        }

        private async Task SetViolationTypes(ViolationGroups violationGroup)
        {
            if (violationGroup != null)
            {
                ViolationTypes = await _novService.GetViolationTypes(violationGroup.ViolationGroupId);
                ViolationTypesSelectedItem = ViolationTypes.FirstOrDefault();
            }
        }

        private async Task SetViolationDetails()
        {
            ViolationGroups selectedGroup = ViolationGroupsSelectedItem;

            if (selectedGroup != null)
            {
                ViolationDetails = await _novService.GetViolationDetailsByGroupAndName(selectedGroup.ViolationGroupId, selectedGroup.GroupName);
                ViolationDetailsSelectedItem = ViolationDetails.FirstOrDefault();
            }
        }

        private async Task SetSummaryOfLaw(ViolationDetails detail)
        {
            if (detail != null)
            {
                _selectedViolationLawCode = await _novService.GetSummaryOfLaw(detail.LawId);
                SummaryOfLaw = _selectedViolationLawCode.LawDescription;
                SummaryOfLaw = SummaryOfLaw.Replace("’", "\'");
            }
            else
            {
                _selectedViolationLawCode = null;
                SummaryOfLaw = null;
            }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (ViolationGroupsSelectedItem == null)
            {
                alerts.Add(new AlertViewModel("12079", WorkFlowMessages.DSNYMSG_NOV079, elementName: "ViolationGroups"));
            }

            if ((OccurrenceDate + OccurrenceTime) > DateTime.Now)
            {
                alerts.Add(new AlertViewModel("12079", WorkFlowMessages.DSNYMSG_NOV100, elementName: "ViolationGroups"));
            }

            return Task.FromResult(alerts);
        }

        public override List<string> CancelMenuItems
        {
            get
            {
                if (!String.IsNullOrEmpty(showVoid))
                {
                    if (showVoid == "true")
                    {
                        return new List<string> { "Void" };
                    }
                    else
                    {
                        return new List<string> { "Cancel" };
                    }
                }
                else
                {
                    return new List<string> { "Cancel" };
                }
            }
        }

        public override void WriteFieldValuesToNovMaster()
        {
            NovMaster.NovInformation.LoginTimestamp = NovMaster.UserSession.DutyHeader.LoginTimestamp;
            NovMaster.NovInformation.DeviceId = NovMaster.UserSession.DutyHeader.DeviceId;

            NovMaster.NovInformation.IssuedTimestamp = OccurrenceDate + OccurrenceTime;
            NovMaster.NovInformation.SystemTimestamp = DateTime.Now;
            NovMaster.ViolationGroup = ViolationGroupsSelectedItem;
            NovMaster.NovInformation.ViolationGroupId = ViolationGroupsSelectedItem != null ? ViolationGroupsSelectedItem.ViolationGroupId : 0;
            if (NovMaster.NovInformation.ViolationGroupId == 3)
            {
                NovMaster.NovInformation.IsBusiness = true;
            }

            NovMaster.NovInformation.ViolGroupName = ViolationGroupsSelectedItem?.GroupName;
            NovMaster.NovInformation.ViolationTypeId = ViolationTypesSelectedItem?.ViolationTypeId;

            if (ViolationDetailsSelectedItem != null)
            {
                NovMaster.ViolationDetails = ViolationDetailsSelectedItem;
                NovMaster.NovInformation.ViolationCode = ViolationDetailsSelectedItem.ViolationCode;
                NovMaster.NovInformation.MaximumAmount = ViolationDetailsSelectedItem.MaximumAmount;
                NovMaster.NovInformation.MailableAmount = ViolationDetailsSelectedItem.MailableAmount;
                NovMaster.NovInformation.HHTIdentifier = ViolationDetailsSelectedItem.HHTIdentifier;
                NovMaster.NovInformation.PlaceAddress1 = ViolationDetailsSelectedItem.InternalShortDescription; // In the old application, InternalShortDescription is stored in PlaceAddress1 so that it can be printed on the ticket later.
            }

            // Set the initial value of PrintViolationCode to match ViolationCode.  This is the value we display in the UI and on the ticket, but it could change later based on the repeat violator check.
            // The original value of ViolationCode still needs to be used to drive the logic of the app.
            NovMaster.NovInformation.PrintViolationCode = NovMaster.NovInformation.ViolationCode;

            NovMaster.NovInformation.LawSection = _selectedViolationLawCode?.LawSection;
            var law = _codeLaws.FirstOrDefault(_ => _.CodeLawId == ViolationDetailsSelectedItem?.CodeLaw);
            NovMaster.NovInformation.CodeLawDescription = law?.CodeLawDescription;

            // to print on ticket
            NovMaster.NovInformation.ViolationCodeShortDescription = ViolationDetailsSelectedItem?.ViolationCodeShortDescription;
            NovMaster.NovInformation.SummaryOfLaw = SummaryOfLaw;
            //

            if (ViolationDetailsSelectedItem?.ViolationCode == "S13"
                || ViolationDetailsSelectedItem?.ViolationCode == "S14"
                || ViolationDetailsSelectedItem?.ViolationCode == "S15"
                || ViolationDetailsSelectedItem?.ViolationCode == "GB1"
                || ViolationDetailsSelectedItem?.ViolationGroupId == 4)
            {
                NovMaster.NovInformation.IsPetitionerCourtAppear = "Y";
            }
            else
            {
                NovMaster.NovInformation.IsPetitionerCourtAppear = "N";
            }

            if (IsCancelled)
            {
                NovMaster.NovInformation.TicketStatus = "C";
            }
            else if (IsVoidAction)
            {
                NovMaster.NovInformation.TicketStatus = "V";
            }
        }

        // NH-1265 Property and Commerical only ; see PlaceOfOccurenceViewModel BackCommand
        // NH-1408 2018-11-05 Action ; Point of no return is PersonBeingServedID screen
        private bool getIsAllowBackToNovSummary(bool defaultChecked)
        {
            if (NovMaster != null && NovMaster.NovInformation != null && NovMaster.NovInformation.NovNumber > 0) {
                return false;
            }

            var isAllowBackToNovSummary = CrossSettings.Current.GetValueOrDefault("PlaceOfOccurrence_IsAllowBackToNovSummary_" + NovMaster.NovInformation.NovNumber.ToString(), defaultChecked.ToString());
            return bool.Parse(isAllowBackToNovSummary);

        }

        // NH-1417 2018-11-08
        private void resetNovInformation() {
            if (NovMaster != null && NovMaster.NovInformation!=null)
            {
                var newNovInfo = NovMaster.NovInformation;

                NovMaster.NovInformation.BusinessName = null;
                NovMaster.NovInformation.CodeLawDescription = null;
                NovMaster.NovInformation.HHTIdentifier = null;

                NovMaster.NovInformation.IsBusiness = false;
                NovMaster.NovInformation.IsBusinessNameRetained = false;
                NovMaster.NovInformation.IsIdChecked = false;
                NovMaster.NovInformation.IsIssuedByChecked = false;
                NovMaster.NovInformation.IsPersonBeingServedBusi = false;
                NovMaster.NovInformation.IsPetitionerCourtAppear = null;
                NovMaster.NovInformation.LawSection = null;

                NovMaster.NovInformation.LicenseAgency = null;
                NovMaster.NovInformation.LicenseNumber = null;
                NovMaster.NovInformation.LicenseType = null;
                NovMaster.NovInformation.LicenseTypeDesc = null;

                NovMaster.NovInformation.MailableAmount = 0;
                NovMaster.NovInformation.MaximumAmount = 0;

                // reset Place of occurrence fields
                NovMaster.NovInformation.PlaceAddress1 = null;
                NovMaster.NovInformation.PlaceAddress2 = null;
                NovMaster.NovInformation.PlaceAddressDescriptor = null;
                NovMaster.NovInformation.PlaceBBL = null;
                NovMaster.NovInformation.PlaceBoroCode = null;
                NovMaster.NovInformation.PlaceCross1StreetId = 0;
                NovMaster.NovInformation.PlaceCross2StreetId = 0;
                NovMaster.NovInformation.PlaceDistrictId = null;
                NovMaster.NovInformation.PlaceFirstName = null;
                NovMaster.NovInformation.PlaceHouseIndetifier = null;
                NovMaster.NovInformation.PlaceHouseNo = null;
                NovMaster.NovInformation.PlaceHouseNo1 = null;
                NovMaster.NovInformation.PlaceHouseNo2 = null;
                NovMaster.NovInformation.PlaceLastName = null;
                NovMaster.NovInformation.PlaceMiddleInitial = null;
                NovMaster.NovInformation.PlaceSectionId = null;
                NovMaster.NovInformation.PlaceSideOfStreet = null;
                NovMaster.NovInformation.PlaceStreetId = 0;

                NovMaster.NovInformation.PrintViolationCode = null;

                NovMaster.NovInformation.PropertyBBL = null;
                NovMaster.NovInformation.PublicKeyId = 0;

                // reset Person Being Served Address
                NovMaster.NovInformation.Resp1Address = null;
                NovMaster.NovInformation.Resp1Address1 = null;
                NovMaster.NovInformation.Resp1BoroCode = null;
                NovMaster.NovInformation.Resp1City = null;
                NovMaster.NovInformation.Resp1FirstName = null;
                NovMaster.NovInformation.Resp1HouseNo = null;
                NovMaster.NovInformation.Resp1LastName = null;
                NovMaster.NovInformation.Resp1MiddleInitial = null;
                NovMaster.NovInformation.Resp1Sex = null;
                NovMaster.NovInformation.Resp1State = null;
                NovMaster.NovInformation.Resp1Zip = null;

                NovMaster.NovInformation.SummaryOfLaw = null;
                NovMaster.NovInformation.ViolationCode = null;
                NovMaster.NovInformation.ViolationCodeShortDescription = null;

            }
        }
    }
}