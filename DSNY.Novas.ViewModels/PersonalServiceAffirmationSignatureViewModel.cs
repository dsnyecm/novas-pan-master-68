﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Services;
using DSNY.Novas.Common;
using System;
using System.Linq;
using DSNY.Novas.Models;

namespace DSNY.Novas.ViewModels
{
    public class PersonalServiceAffirmationSignatureViewModel : ViewModelBase
    {
        private bool _isNovAffirmed;
        private bool _isPetitionerVisible;
        private bool _isServiceTextVisible;
        private bool _isObserveTextVisible;
        private bool _isMailInPenaltyVisible;
        private byte[] _signatureImage;

        private string _code;

        private readonly IAffidavitService _affidavitService;
        private readonly IPersonBeingServedService _personBeingServedService;
        private readonly INovasUserService _userService;
        private readonly IPersonnelDataService _personnelDataService;
        private readonly ILookupService _lookupService;
        public override string Title => "Personal Service Affirmation";

        public PersonalServiceAffirmationSignatureViewModel()
        {
            _affidavitService = DependencyResolver.Get<IAffidavitService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
            _userService = DependencyResolver.Get<INovasUserService>();
            _personnelDataService = DependencyResolver.Get<IPersonnelDataService>();
            _lookupService = DependencyResolver.Get<ILookupService>();
            IsPetitionerVisible = false;
            IsServiceTextVisible = true;
            IsObserveTextVisible = false;
            IsMailInPenaltyVisible = false; 
        }

        public override async Task LoadAsync()
        {

            List<LookupTable> str;
            if (NovMaster.UserSession.Department == SHERIFF_DEP)
                str = await _lookupService.GetLookupTable("DOF");
            else
                str = await _lookupService.GetLookupTable("Dept of Sanitation");

            if (str != null)
            {
                NovMaster.NovInformation.AgencyId = str[0].Code;
                Code = str[0].Description;
            }

            //SignatureImage = (await _userService.GetUser(NovMaster.NovInformation.UserId)).SignatureBitmap;
            SignatureImage = (await _userService.GetUserNov(NovMaster.NovInformation.UserId)).SignatureBitmap;
            return;
        }

        public bool IsNovAffirmed
        {
            get => _isNovAffirmed;
            set
            {
                _isNovAffirmed = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsMailInPenaltyVisible
        {
            get => _isMailInPenaltyVisible;
            set
            {
                _isMailInPenaltyVisible = value;
                NotifyPropertyChanged();
            }
        }

        public string Code
        {
            get => _code;
            set { _code = value; NotifyPropertyChanged(); }
        }

        public bool IsPetitionerVisible
        {
            get => _isPetitionerVisible;
            set
            {
                _isPetitionerVisible = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsServiceTextVisible
        {
            get => _isServiceTextVisible;
            set
            {
                _isServiceTextVisible = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsObserveTextVisible
        {
            get => _isObserveTextVisible;
            set
            {
                _isObserveTextVisible = value;
                NotifyPropertyChanged();
            }
        }

        public byte[] SignatureImage
        {
            get => _signatureImage;
            set
            {
                _signatureImage = value;
                NotifyPropertyChanged();
            }
        }
        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;
        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (!IsNovAffirmed)
            {
                alerts.Add(new AlertViewModel("14001", WorkFlowMessages.DSNYMSG_26_EmptySignature, elementName: "IsNovAffirmed"));
            }

            return Task.FromResult(alerts);
        }

        public override void WriteFieldValuesToNovMaster()
        {
            //NovMaster.AffidavitOfService.NovNumber = NovMaster.NovInformation.NovNumber;
            NovMaster.AffidavitOfService.CheckSum = NovMaster.NovInformation.CheckSum; 
            NovMaster.AffidavitOfService.PersonallyServeFlag = "P";
            NovMaster.AffidavitOfService.UserId = NovMaster.NovInformation.UserId;
            NovMaster.AffidavitOfService.PublicKeyId = NovMaster.NovInformation.PublicKeyId;
            NovMaster.AffidavitOfService.DeviceId = NovMaster.NovInformation.DeviceId;
            NovMaster.AffidavitOfService.OfficerName = NovMaster.NovInformation.OfficerName;
            NovMaster.AffidavitOfService.AbbrevName = NovMaster.NovInformation.AbbrevName;
            NovMaster.AffidavitOfService.AgencyId = NovMaster.NovInformation.AgencyId;
            NovMaster.AffidavitOfService.Title = NovMaster.NovInformation.Title;
            NovMaster.AffidavitOfService.LoginTimestamp = NovMaster.NovInformation.LoginTimestamp;
            NovMaster.AffidavitOfService.ServedCounty = NovMaster.NovInformation.PrintViolationCode; // The PrintViolationCode is stored in the ServedCounty field for Affidavit of Service

            // AffidavitOfService.DigitalSignature is different  from Novinformation and it is being  set in  AffidavitService.cs of method SignAffadavit

            NovMaster.AffidavitOfService.DigitalSignature = NovMaster.NovInformation.DigitalSignature;
            NovMaster.AffidavitOfService.SignDate = NovMaster.NovInformation.IssuedTimestamp;
            // AffidavitOfService.ExpDate is not suppose to set from NovInformation
           // NovMaster.AffidavitOfService.ExpDate = NovMaster.NovInformation.LicenseExpDate;
        }

        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }
            
            WriteFieldValuesToNovMaster();
            await _affidavitService.SaveAffidavit(NovMaster);
            NovMaster.NovInformation.TicketStatus = " ";
            await SaveTicket();

            await NavigationService.PushAsync(new PrinterViewModel() { NovMaster = NovMaster });

            NavigationInProgress = false;
        });
    }
}