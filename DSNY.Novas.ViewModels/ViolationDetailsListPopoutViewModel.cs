﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using System.Threading.Tasks;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class ViolationDetailsListPopoutViewModel : ViewModelBase
    {
        private List<SelectableItem<ViolationScriptVariableData>> _values;
        private SelectableItem<ViolationScriptVariableData> _selectedValue;
        private ViolationScriptVariables _field;
        private string _otherFreeForm;
        private bool _isOtherVisible;
        private bool _isOtherEnabled;
        private bool _isMultiSelect;
        private string _title;

        public override string Title => _title;

        public ViolationDetailsListPopoutViewModel(NovMaster novMaster, ViolationScriptVariables field, List<ViolationScriptVariableData> values)
        {
            NovMaster = novMaster;
            Field = field;
            _title = FormatTitle(Field.Description);
            IsMultiSelect = Field.IsMultiplesAllowed.Equals("Y");

            if (Field.IsOtherAllowed.Equals("Y"))
            {
                IsOtherVisible = true;
                values.Add(new ViolationScriptVariableData { FieldName = Field.FieldName, Data = "Other" });
            }

            var items = values.Select((value) => new SelectableItem<ViolationScriptVariableData>(value)).ToList();
            var novDataItems = NovMaster.NovData.Find((novData) => novData.FieldName.Equals(Field.FieldName))?.Data?.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
            // If we have previously saved data, set those items to be initally selected
            if (novDataItems != null)
            {
                var novDataSet = new HashSet<string>(novDataItems, StringComparer.OrdinalIgnoreCase);
                foreach (var item in items)
                {
                    item.IsSelected = novDataSet.Contains(item.Data.Data.Trim());
                }

                if (IsOtherVisible)
                {
                    var itemData = items.Select((item) => item.Data.Data.Trim());
                    if (novDataItems.ToList().Count != 0 && !itemData.Contains(novDataItems.Last()))
                    {
                        var otherItem = items.Where((item) => item.Data.Data.Equals("Other")).First();
                        otherItem.IsSelected = true;
                        OtherFreeForm = novDataItems.Last();
                    }
                }
            }

            Values = items;
        }

        public List<SelectableItem<ViolationScriptVariableData>> Values
        {
            get => _values;
            set { _values = value; NotifyPropertyChanged(); }
        }

        public SelectableItem<ViolationScriptVariableData> SelectedValue
        {
            get => _selectedValue;
            set
            {
                _selectedValue = value;
                if (_selectedValue != null && !IsMultiSelect)
                {
                    foreach (var v in Values)
                    {
                        v.IsSelected = v == _selectedValue;
                    }
                }

                var otherEnabled = false;
                var selectedItems = Values.Where((item) => item.IsSelected);
                foreach (var item in selectedItems)
                {
                    if (item.Data.Data.Equals("Other"))
                    {
                        otherEnabled = true;
                    }
                }
                IsOtherEnabled = otherEnabled;

                NotifyPropertyChanged();
            }
        }

        public ViolationScriptVariables Field
        {
            get => _field;
            set { _field = value; NotifyPropertyChanged(); }
        }

        public bool IsOtherVisible
        {
            get => _isOtherVisible;
            set { _isOtherVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsOtherEnabled
        {
            get => _isOtherEnabled;
            set { _isOtherEnabled = value; NotifyPropertyChanged(); }
        }

        public string OtherFreeForm
        {
            get => _otherFreeForm;
            set { _otherFreeForm = value; NotifyPropertyChanged(); }
        }

        public bool IsMultiSelect
        {
            get => _isMultiSelect;
            set { _isMultiSelect = value; NotifyPropertyChanged(); }
        }

        private string FormatTitle(string originalTitle)
        {
            var wordSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "of", "on", "in", "and", "the", "for" };
            var separators = new List<string> { " ", "/", "-" };
            var title = originalTitle;
            foreach (var separator in separators)
            {
                var titleComponents = title.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
                var newComponents = new List<string>();
                foreach (var component in titleComponents)
                {
                    if (!wordSet.Contains(component))
                    {
                        if (component.Length > 1)
                        {
                            newComponents.Add(component.Substring(0, 1).ToUpper() + component.Substring(1));
                        }
                        else
                        {
                            newComponents.Add(component.ToUpper());
                        }
                    }
                    else
                    {
                        newComponents.Add(component);
                    }
                }

                title = string.Join(separator, newComponents);
            }

            return title;
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;

        public override bool ShowBackButton => false;

        public override bool ShowCancelButton => true;

        public override bool ShowNextButton => false;

        public override bool ShowConfirmButton => IsMultiSelect || IsOtherVisible;

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }
            WriteFieldValuesToNovMaster();
            await NavigationService.PopModalAsync();
            
           
        });

        public override async Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();
            
            if ((String.IsNullOrEmpty(OtherFreeForm) || String.IsNullOrWhiteSpace(OtherFreeForm)) && IsOtherEnabled)
            {
                alerts.Add(new AlertViewModel("12045", (String.Format(WorkFlowMessages.DSNYMSG_23_MissingVariable, Title)), elementName: "OtherFreeForm"));
            }

            return alerts;
        }

        public override void WriteFieldValuesToNovMaster()
        {
            //var novData = NovMaster.NovData;
            //NovData toDelete = novData.FirstOrDefault(_ => _.FieldName == Field.FieldName);
            //if (toDelete != null)
            //{
            //    novData.Remove(toDelete);
            //}

            // NH-1192
            NovData fieldExistsInNovData = NovMaster.NovData.FirstOrDefault(_ => _.FieldName == Field.FieldName);
            var dataIndex = NovMaster.NovData.IndexOf(fieldExistsInNovData);
            if (dataIndex != -1)
                NovMaster.NovData.RemoveAt(dataIndex);

            var selectedItems = Values.Where((item) => item.IsSelected);
            var dataValues = selectedItems.Select((item) => item.Data.Data.Equals("Other") ? OtherFreeForm : item.Data.Data.Trim());
            var displayString = string.Join(", ", dataValues);


            NovMaster.NovData.Add(new NovData { FieldName = Field.FieldName, Data = displayString , NovNumber = NovMaster.NovInformation.NovNumber });

            // NH-1192
            if ( Field.FieldName.Equals("(Routing Time)||0", StringComparison.OrdinalIgnoreCase))
            {
                CrossSettings.Current.AddOrUpdateValue("ViolationDetails_RoutingTimeOverride_" + NovMaster.NovInformation.NovNumber.ToString(), displayString);
            }
                

        }

    }
}

