﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Common.Interfaces;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading;

namespace DSNY.Novas.ViewModels
{
    public class PersonnelDataViewModel : ViewModelBase
    {
        private bool IS_DISPLAY_SUMMARY_OF_LAW=false;
        private int PRN_PRINT_STOP = 1482 - 300;
        private int PRN_DEFAULT_END = 3275 + 25;
        private ObservableCollection<Printer> _printerList;
        private List<ReportingLevel> _reportingLevelList;
        private ReportingLevel _reportingLevel;
        private string _name;
        private List<Agency> _agencyList;
        private Agency _agency;
        private List<HearingDateTime> _hearingDateTimesList;
        private HearingDateTime _hearingDate;
        private HearingDateTime _hearingTime;
        private string _printerSerialNumber;
        private List<string> _printerItemList;
        private string _printerSelectedItem;
        private string _messages;
        private List<HolidayMaster> _holidayList;
        private NovasUser _user;
        private readonly IPrinter _printer;
        private bool _isLoading;
        private bool _areControlsEnabled;
        private bool _hasCompletedInitialLoad;
        private List<DateTime> _validDates;

        public IObservable<Printer> PrinterObservable { get; }

        public IObservable<ObservableCollection<Printer>> PrinterListChangedObservable => 
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                h => (sender, args) => h(args),
                h => PrinterList.CollectionChanged += h,
                h => PrinterList.CollectionChanged -= h)
            .Select(args => PrinterList);
        
        public IDisposable PrinterListChangedSubscription { get; set; }
        public IDisposable PrinterSubscription { get; set; }
        
        public ICommand AgencySelectedCommand { get; set; }
        public ICommand TestPrintPressedCommand { get; set; }

        public override string Title => "NOVAS Personnel Data";

        public PersonnelDataViewModel()
        {
            #region Printers

            _printer = DependencyResolver.Get<IPrinter>();
            PrinterList = new ObservableCollection<Printer>();
            
            PrinterListChangedSubscription = PrinterListChangedObservable
                .ObserveOn(SynchronizationContext.Current)
                .SubscribeOn(NewThreadScheduler.Default)
                .Subscribe(
                    printer =>
                    {
                        NotifyPropertyChanged(nameof(PrinterList));
                        NotifyPropertyChanged(nameof(PrinterItemList));
                        NotifyPropertyChanged(nameof(PrinterSelectedItem));
                        NotifyPropertyChanged(nameof(SelectedPrinter));
                        NotifyPropertyChanged(nameof(SelectedPrinterIsInRange));
                    },
                    ex => { }, // add error handling here
                    () => { });

            PrinterObservable = LoadPrintersTask.ToObservable().SelectMany(printerList => printerList);

            PrinterSubscription = PrinterObservable
                .ObserveOn(SynchronizationContext.Current)
                .SubscribeOn(NewThreadScheduler.Default)
                .Subscribe(
                    printer =>
                    {
                        IsLoading = true;
                        PrinterList.Add(printer);
                    },
                    ex => { }, // add error handling here
                    () => IsLoading = false);

            #endregion

            BarcodeScanner.SetDataRecievedCommand(new Command(async scanResult =>
            {
                await AlertService.DisplayAlert(new AlertViewModel("Scan Successful", $"Attempting to pair with: {scanResult}"));
                BluetoothReader.FindDevices((string)scanResult);
            }));

            BluetoothReader.SetPairSuccessfulCommand(new Command(async pairedDevice =>
            {
                if (pairedDevice is bool)
                {
                    await AlertService.DisplayAlert(new AlertViewModel("Pairing Unsuccessful", "Unable to pair with the scanned printer."));
                    return;
                }

                var pairedPrinter = (IPrinter) pairedDevice;
                var printer = new Printer
                {
                    SerialNumber = pairedPrinter.GetSerialNumber(),
                    MacAddress = pairedPrinter.GetMacAddress()
                };

                await AlertService.DisplayAlert(new AlertViewModel("Pairing Successful", $"Paired with: {printer.SerialNumber}"));

                if (PrinterItemList?.Contains(printer.SerialNumber) == false)
                    PrinterList.Add(printer);

                PrinterSelectedItem = printer.SerialNumber;
            }));

            BluetoothReader.SetDeviceInfoListUpdate(new Command(async DeviceName =>
            {
                var printer = new Printer
                {
                    SerialNumber = ((IPrinter)DeviceName).GetSerialNumber(),
                    MacAddress = ((IPrinter)DeviceName).GetMacAddress()
                };

                if (PrinterItemList?.Contains(printer.SerialNumber) == false)
                    PrinterList.Add(printer);
            }));

            AgencySelectedCommand = new Command(async () => await SetHearingTimesAsync());
        }

        public override async Task LoadAsync()
        {
            if (UserSession != null)
                UserSession.TimeoutTimeStamp = DateTime.Now;
            
            if (User != null)
                User.BoroId = BoroId;

            NotifyPropertyChanged(PrinterSelectedItem);

            TestPrintPressedCommand = new Command(async () =>
            {
                if (PrinterSelectedItem != null && PrinterSelectedItem != "--")
                {
                    if (!SelectedPrinterIsInRange)
                    {
                        await AlertService.DisplayAlert(new AlertViewModel("", WorkFlowMessages.DSNYMSG_07_ScanChoosePrinter));
                    }
                    else
                    {
                        var img = User.SignatureBitmap;
                        if (img == null)
                        {
                            img = Encoding.UTF8.GetBytes("No Signature Found");
                        }

                        var signatureDictionary = new Dictionary<string, string>
                        {
                            {"<<Signature>>", Convert.ToBase64String(img)}
                        };

                        var testDictionaryWithSignature = MapAllFieldsToValuesByNovMaster(signatureDictionary);

                        var printSuccess = await _printer.InitiateConnection(SelectedPrinter?.MacAddress, true, testDictionaryWithSignature, UserSession?.Department);
                        if (!printSuccess)
                        {
                            await AlertService.DisplayAlert(new AlertViewModel("Connection Failed: ", "Unable to connect to: " + PrinterSelectedItem));
                        }
                    }
                }
                else
                {
                    await AlertService.DisplayAlert(new AlertViewModel("Connection Failed: ", "No Printer Selected"));
                }
            });

            //once device is selected from printer dropdown, then attempt to pair if not already paired.
            if (PrinterSelectedItem != null && PrinterSelectedItem != "--")
            {
                //Find printer that is selected
                if (SelectedPrinterIsInRange)
                {
                    IsLoading = true;

                    if (await BluetoothReader.IsPaired(PrinterSelectedItem) == false)
                        await BluetoothReader.Pair(PrinterSelectedItem);
                    
                    IsLoading = false;
                }
            }

            if (User == null)
                User = await NovasUserService.GetUser(UserSession.UserId);
            
            await Task.WhenAll(LoadReportingLevelsTask, LoadAgenciesTask, LoadHolidaysTask, LoadBoroSiteMastersTask)
                .ContinueWith(_ =>
                {
                    ReportingLevelList = ReportingLevelsByTitle?[UserTitle]?.ToList();
                    HolidayList = HolidayCollection?.ToList();
                    AgencyList = AgencyCollection?.ToList();
                });

            ReportingLevel = ReportingLevelList
                .FirstOrDefault(_ => _.ReportingLevelId == (UserSessionReportLevel ?? UserSiteId)
                                     && _.Title == (UserSessionTitle ?? UserTitle));

            await Task.WhenAll(LoadHearingDatesTask, LoadHearingDateTimesTask);

            if (_hasCompletedInitialLoad)
                return;

            IsLoading = true;
            
            Name = UserName;

            Agency = AgencyList?.FirstOrDefault(_ => _.AgencyId == AgencyId);

            // 1. find Hearing Time (not date) from database and compare the UserSession hearing-"Time", that match user AgencyId
            if (Agency != null)
            {
                IsLoading = true;

                HearingDateTimesList = (AgencyId == 0 ? HearingDateTimeCollection : HearingDateTimesByAgency[AgencyId]).ToList();

                HearingTime = HearingDateTimesList?.Find(_ => _.HearingTime.TimeOfDay.Equals(UserSession.HearingTimeStamp.TimeOfDay)) ??
                                  HearingDateTimesList?.FirstOrDefault();
            }

            // 2. find Hearing Date from database and compare the User SiteId to match > BoroSiteMaster.ReportLevel and BoroSiteMaster.BoroId to match > HolidayMaster.BoroId 
            if (UserSession.HearingTimeStamp != default(DateTime))
                HearingDate = new HearingDateTime { HearingFromDate = UserSession.HearingTimeStamp.Date };
            else
            {
                var holidaysWithinRange = (BoroId == 0 ? HolidayCollection : HolidaysByBoro[BoroId])
                    .Where(h => h.EffectiveDate.Between(ThirtyDaysFromToday, OneYearFromToday))
                    .Select(h => h.EndEffDate);

                var hearingDatesIncludeHolidays = (BoroId == 0 ? HearingDateCollection : HearingDatesByBoro[BoroId])
                    .Where(d => d.TimeStamp.Between(ThirtyDaysFromToday, OneYearFromToday))
                    .Select(d => d.TimeStamp);

                ValidDates = hearingDatesIncludeHolidays.Except(holidaysWithinRange).ToList();
                
                //if we find the hearing date, set it and move on,
                //otherwise try to construct it on our own
                HearingDate = new HearingDateTime { HearingFromDate = ValidDates.FirstOrDefault() };
            }

            if (BarcodeScanner != null && await BarcodeScanner?.IsScannerAvailable())
                await BarcodeScanner.InitiateScan();

            // NH-1264
            if (!string.IsNullOrEmpty(UserSessionUserId))
            {
                var hasAnotherUserIncompleteTicket = await NovService.HasAnotherUserIncompleteTicket(UserSession.UserId);
                if (hasAnotherUserIncompleteTicket)
                {
                    await AlertService.DisplayAlert(new AlertViewModel("12064", WorkFlowMessages.DSNYMSG_07_VoidAnotherUserIncompleteTicket));
                    await NovService.VoidIncompleteTicketExceptThisUser(UserSession.UserId);
                }
            }


            UserSessionTimeoutTimeStamp = DateTime.Now;

            await LoadPrintersTask.ContinueWith(async printerList =>
            {
                var printerResult = await printerList;
                if (printerResult?.Any() == false)
                    await AlertService.DisplayAlert(new AlertViewModel("Printers", "Printers not found"));
                else
                {
                    IsLoading = false;
                    _hasCompletedInitialLoad = true;
                }
            });
        }

        public List<ReportingLevel> ReportingLevelList
        {
            get => _reportingLevelList;
            set
            {
                _reportingLevelList = value;
                NotifyPropertyChanged();
            }
        }

        public ReportingLevel ReportingLevel
        {
            get => _reportingLevel;
            set
            {
                _reportingLevel = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(ReportingLevelId));
                BoroId = BoroSiteMastersByReportLevel?[ReportingLevelId]?.BoroId ?? 0;
            }
        }

        public string ReportingLevelId => ReportingLevel?.ReportingLevelId;
        
        private int _boroId;
        public int BoroId
        {
            get => _boroId;
            set
            {
                _boroId = value;
                NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        public List<Agency> AgencyList
        {
            get => _agencyList;
            set
            {
                _agencyList = value;
                NotifyPropertyChanged();
            }
        }

        public Agency Agency
        {
            get => _agency;
            set
            {
                _agency = value;
                NotifyPropertyChanged();
                HearingDateTimesList = (AgencyId == 0
                    ? HearingDateTimeCollection
                    : HearingDateTimesByAgency[AgencyId])?.ToList();
            }
        }

        public int AgencyId => Math.Max(UserSessionAgencyIdInt, UserAgencyIdInt);

        public List<HearingDateTime> HearingDateTimesList
        {
            get => _hearingDateTimesList;
            set
            {
                _hearingDateTimesList = value;
                NotifyPropertyChanged();
            }
        }

        public HearingDateTime HearingDate
        {
            get => _hearingDate;
            set
            {
                _hearingDate = value;
                NotifyPropertyChanged();
            }
        }

        public HearingDateTime HearingTime
        {
            get => _hearingTime;
            set
            {
                _hearingTime = value;
                NotifyPropertyChanged();
            }
        }

        public string PrinterSerialNumber
        {
            get => _printerSerialNumber;
            set
            {
                _printerSerialNumber = value;
                NotifyPropertyChanged();
            }
        }

        public List<string> PrinterItemList => PrinterList?.Select(printer => printer.SerialNumber).ToList();
        public Printer SelectedPrinter => PrinterList?.FirstOrDefault(printer => printer.SerialNumber == PrinterSelectedItem);
        public bool SelectedPrinterIsInRange => SelectedPrinter?.IsInRange == true;

        public string PrinterSelectedItem
        {
            get => _printerSelectedItem;
            set
            {
                if (value != _printerSelectedItem)
                {
                    _printerSelectedItem = value;
                    NotifyPropertyChanged();
                    NotifyPropertyChanged(nameof(SelectedPrinter));
                    NotifyPropertyChanged(nameof(SelectedPrinterIsInRange));
                }
            }
        }

        public ObservableCollection<Printer> PrinterList
        {
            get => _printerList;
            set
            {
                _printerList = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(PrinterItemList));
                NotifyPropertyChanged(nameof(PrinterSelectedItem));
                NotifyPropertyChanged(nameof(SelectedPrinter));
                NotifyPropertyChanged(nameof(SelectedPrinterIsInRange));
            }
        }

        public string Messages
        {
            get => _messages;
            set { _messages = value; NotifyPropertyChanged(); }
        }

        public List<HolidayMaster> HolidayList
        {
            get => _holidayList;
            set
            {
                _holidayList = value;
                NotifyPropertyChanged();
            }
        }

        public List<DateTime> ValidDates
        {
            get => _validDates;
            set { _validDates = value; NotifyPropertyChanged(); }
        }

        public NovasUser User
        {
            get => _user;
            set
            {
                _user = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(UserTitle));
                NotifyPropertyChanged(nameof(UserSiteId));
                NotifyPropertyChanged(nameof(UserName));
                NotifyPropertyChanged(nameof(UserAgencyId));
                NotifyPropertyChanged(nameof(UserAgencyIdInt));
            }
        }

        public string UserTitle => User?.Title?.Trim();
        public string UserSiteId => User?.SiteId?.Trim();
        public string UserName => User?.Name?.Trim();
        public string UserAgencyId => User?.AgencyId?.Trim();
        public int UserAgencyIdInt => string.IsNullOrWhiteSpace(UserAgencyId) ? 0 : int.Parse(UserAgencyId);

        public async Task SetHearingTimesAsync()
        {
            if (Agency != null)
            {
                HearingDateTimesList = await PersonnelDataService.GetHearingTimesAsync(Agency);
                HearingTime = HearingDateTimesList?.FirstOrDefault();
            }
        }

        public override bool ShowActionMenu => AreControlsEnabled;

        public override bool ShowHelpMenu => AreControlsEnabled;

        public override List<string> ActionMenuItems =>
            User != null && User.IsSupervisor == "Y"
                ? new List<string> {"Main Page", "Pin Tool", "Device Status", "Log Off"}
                : new List<string> {"Main Page", "Device Status", "Log Off"};

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;

                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(AreControlsEnabled));
                NotifyPropertyChanged(nameof(ShowHelpMenu));
                NotifyPropertyChanged(nameof(ShowActionMenu));
                NotifyPropertyChanged(nameof(MenuItems));
            }
        }

        public bool AreControlsEnabled => !IsLoading;

        public override async Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            /*if (!await VehicleService.isVehicleCurrent(UserSession.UserId)) //NH-965
                alerts.Add(new AlertViewModel("11004", WorkFlowMessages.DSNYMSG_24_VehicleRadioInfoNotEnterted));*/

            if (ValidDates == null || ValidDates.LongCount() == 0) {
                alerts.Add(new AlertViewModel("", "Please contact Novas Administrator. Error: ValidDates is invalid"));
                return alerts;
            }

            if (ReportingLevel == null)
            {
                alerts.Add(new AlertViewModel("14012", WorkFlowMessages.DSNYMSG_06_EmptyReportingLevel));
            }

            if (HearingDate == null || HearingTime == null)
            {
                alerts.Add(new AlertViewModel("11010", WorkFlowMessages.DSNYMSG_06_EmptyHearingTime, elementName: HearingDate == null ? "HearingDate" : "HearingTime"));
                return alerts;
            }

            if (HearingDate.HearingFromDate.CompareTo(DateTime.Today) < 0
                || ThirtyDaysFromToday.CompareTo(HearingDate.HearingFromDate) > 0
                || OneYearFromToday.CompareTo(HearingDate.HearingFromDate) < 0)
            {
                alerts.Add(new AlertViewModel("1082", WorkFlowMessages.DSNYMSG_06_InvalidHearingDate, elementName: "HearingDate"));
                return alerts;
            }
            
            // HolidayMasterTable contains more than just holidays, also includes "ECB Non-Working Day" values
            if (HearingDate.HearingFromDate.DayOfWeek.ToString().In("Saturday", "Sunday")
                || !ValidDates.Any(day => day.Equals(HearingDate.HearingFromDate.Date)))
            {
                var sb = new StringBuilder($"{WorkFlowMessages.DSNYMSG_06_InvalidHearingDateHoliday}\n\nThe next valid dates are:\n");
                ValidDates
                    .FindAll(day => day > HearingDate.HearingFromDate.Date && !day.DayOfWeek.ToString().In("Saturday", "Sunday"))
                    .Take(5)
                    .ToList()
                    .ForEach(d => sb.AppendLine($"{d:dddd, M/d/yyyy}"));

                if (UserSession.Department != SHERIFF_DEP)
                    alerts.Add(new AlertViewModel("11009", $"{sb}", elementName: "HearingDate"));
            }

            if (PrinterSelectedItem == null || PrinterSelectedItem == "--")
            {
                alerts.Add(new AlertViewModel("11002", WorkFlowMessages.DSNYMSG_06_ScanChoosePrinter, elementName: "Printer"));
            }

            if (!String.IsNullOrEmpty(PrinterSelectedItem) && PrinterSelectedItem != "--")
            {
                if (!SelectedPrinterIsInRange)
                {
                    alerts.Add(new AlertViewModel("", WorkFlowMessages.DSNYMSG_07_ScanChoosePrinter, elementName: "Printer"));
                }
            }

            return alerts;
        }

        public override ICommand BackCommand => new Command(() =>
        {
            UserSession.DutyHeader.LogoutTimestamp = DateTime.Now;
            MenuItemTapped("Log Off");
        });

        public override ViewModelBase NextViewModel
        {
            get
            {
                UserSession.AgencyId = Agency.AgencyId.ToString();
                UserSession.AbbrevName = User.AbbrevName;
                UserSession.OfficerName = User.Name;
                UserSession.Title = ReportingLevel.Title;
                UserSession.ReportLevel = ReportingLevel.ReportingLevelId;
                UserSession.HearingTimeStamp = HearingDate.HearingFromDate.Date + HearingTime.HearingTime.TimeOfDay;
                UserSession.DefaultBoroCode = User.BoroId.ToString();
                UserSession.IsSupervisor = User.IsSupervisor;

                UserSession.SelectedPrinter = PrinterList?.First(_ => _.SerialNumber.Equals(PrinterSelectedItem));
                UserSession.ValidHearingDates = ValidDates;

                //Saving DutyHeader Information
                UserSession.DutyHeader.UserId = UserSession.UserId;
                UserSession.DutyHeader.BoroId = UserSession.DefaultBoroCode;
                UserSession.DutyHeader.SiteId = UserSession.ReportLevel;
                UserSession.DutyHeader.Title = UserSession.Title;
                UserSession.DutyHeader.HearingDate = UserSession.HearingTimeStamp;
                UserSession.DutyHeader.IsActingSupervisor = UserSession.IsSupervisor;
                UserSession.DutyHeader.IsSent = "N";
                UserSession.DutyHeader.IsReceived = "N";
                UserSession.DutyHeader.SignatureBitmap = User.SignatureBitmap;

                // BluetoothReader.RemoveBluetoothWatchers();

                return new NovSummaryViewModel { UserSession = UserSession };
            }
        }

        public override bool ShouldSaveTicketOnNext => false;

        /*public override List<string> ScreenSpecificMenuItems => new List<string> { "Vehicle/Radio Information" }; //NH-965

        public override void MenuItemTapped(object item) //NH-965
        {
            if ((string) item == "Vehicle/Radio Information")
                VehicleRadioPressedCommand.Execute(null);

            base.MenuItemTapped(item);
        }*/

        public ICommand ListAllPrintersCommand => new Command(async () =>
        {
            IsLoading = true;

            // reset PrinterSelectedItem
            PrinterSelectedItem = "--";

            PrinterList = new ObservableCollection<Printer>(await BluetoothReader.ListAllPrinters());

            IsLoading = false;
        });

        private string GenerateTestValueForField(int len)
        {
            string testValue = "x";

            for (int i = 0; i < len; i++)
            {
                testValue += "x";
            }

            return testValue;
        }
        private Dictionary<string, string> MapAllFieldsToValuesByNovMaster(Dictionary<string, string> MapFieldsToNovInfo)
        {
            int LINE_SIZE = 70;
            int WORD_SIZE = 10;
            string LINE_X = GenerateTestValueForField(LINE_SIZE);
            string WORD_X = GenerateTestValueForField(WORD_SIZE);
            string CHAR_X = "x";
            string DATE_X = "XXXX X, XXXX";
            string TIME_X = "XX:XX XX";

            var CBNO = CHAR_X; // !String.IsNullOrEmpty(NovMaster.NovInformation.PlaceDistrictId) && !String.IsNullOrWhiteSpace(NovMaster.NovInformation.PlaceDistrictId) ? NovMaster.NovInformation.PlaceDistrictId.Substring(1, 2) : "";
            MapFieldsToNovInfo.Add("<<NovNumber>>", "xxxxxxxx"); // NovMaster.NovInformation.NovNumber.ToString());

            MapFieldsToNovInfo.Add("<<AgencyId>>", WORD_X); // NovMaster.NovInformation.AgencyId);
            MapFieldsToNovInfo.Add("<<ReportLevel>>", WORD_X); // NovMaster.NovInformation.ReportLevel);

            // Respondent
            MapFieldsToNovInfo.Add("<<Resp1LastName>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1FirstName>>", WORD_X);

            MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1Sex>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1Address>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1City>>", WORD_X);
            MapFieldsToNovInfo.Add("<<Resp1State>>", CHAR_X + CHAR_X);
            MapFieldsToNovInfo.Add("<<Resp1Zip>>", WORD_X);
            MapFieldsToNovInfo.Add("<<LicenseNumber>>", WORD_X);
            MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", WORD_X);
            MapFieldsToNovInfo.Add("<<LicenseAgency>>", WORD_X);
            MapFieldsToNovInfo.Add("<<LicenseExpDate>>", WORD_X);

            // 
            MapFieldsToNovInfo.Add("<<IssuedTimeStamp>>", DateTime.Now.ToString("M/d/yyyy hh:mm tt")); //NovMaster.NovInformation.IssuedTimestamp.ToString("M/d/yyyy hh:mm tt"));
            MapFieldsToNovInfo.Add("<<Borough>>", "xx");
            MapFieldsToNovInfo.Add("<<DisplayAddress>>", WORD_X); // <<PlaceAddressDescriptor>> <<PlaceBBL>> <<DisplayAddress>>

            MapFieldsToNovInfo.Add("<<HearingDate>>", DATE_X); // NovMaster.NovInformation.HearingTimestamp.ToString("MMMM d, yyyy"));
            MapFieldsToNovInfo.Add("<<HearingTime>>", TIME_X); // NovMaster.NovInformation.HearingTimestamp.ToString("hh:mm tt"));

            MapFieldsToNovInfo.Add("<<Comments>>", WORD_X);  // NovMaster.AffidavitOfService?.Comments);
            MapFieldsToNovInfo.Add("<<IsAppearInHearingChecked>>", "Petitioner Wishes to Appear If Hearing");

            MapFieldsToNovInfo.Add("<<CodeLawDescription>>", WORD_X); // NovMaster.NovInformation.CodeLawDescription);
            MapFieldsToNovInfo.Add("<<LawSection>>", WORD_X); // NovMaster.NovInformation.LawSection);

            //
            MapFieldsToNovInfo.Add("<<HHTIdentifier>>", WORD_X); // NovMaster.NovInformation.HHTIdentifier);
            MapFieldsToNovInfo.Add("<<PrintViolationCode>>", WORD_X); // NovMaster.NovInformation.PrintViolationCode);

            MapFieldsToNovInfo.Add("<<MailableAmount>>", WORD_X); // "No Mail-In Penalty. You Must Appear.");
            MapFieldsToNovInfo.Add("<<MaximumAmount>>", WORD_X); // NovMaster.NovInformation.MaximumAmount.ToString("C", new CultureInfo("en-US")));

            MapFieldsToNovInfo.Add("<<OfficerName>>", WORD_X); // NovMaster.NovInformation.OfficerName);
            MapFieldsToNovInfo.Add("<<AbbrevName>>", WORD_X); // NovMaster.NovInformation.AbbrevName);
            MapFieldsToNovInfo.Add("<<Title>>", WORD_X); // NovMaster.NovInformation.Title);
            MapFieldsToNovInfo.Add("<<Checksum>>", CHAR_X); // NovMaster.NovInformation.CheckSum);

            MapFieldsToNovInfo.Add("<<ViolationCodeShortDescription>>", WORD_X); // NovMaster.ViolationDetails.ViolationCode + " - " + NovMaster.ViolationDetails.InternalShortDescription);

            MapFieldsToNovInfo.Add("<<TicketPrintingType>>", "TEST");
            MapFieldsToNovInfo.Add("<<PropertyType>>", WORD_X); // PropertyType); // e.g. "Violation Type: Action";

            string ViolationScript = ""; // GenerateTestValueForField(410);
            ViolationScript += GenerateTestValueForField(76) + " ";
            ViolationScript += GenerateTestValueForField(76) + " ";
            ViolationScript += GenerateTestValueForField(76) + " ";
            ViolationScript += GenerateTestValueForField(76) + " ";
            ViolationScript += GenerateTestValueForField(76) + " ";
            var scriptLength = ViolationScript.Length;

            //var scriptLength = NovMaster.NovInformation.ViolationScript.Length;
            var firstLine = 87;
            var startIndexTotal = 0;

            for (int i = 0; i < 5; i++)
            {
                MapFieldsToNovInfo[String.Format("<<ViolationScript{0}>>", i + 1)] = "";
                if (scriptLength >= 78)
                {
                    firstLine = ViolationScript.Substring(startIndexTotal, 78).LastIndexOf(" ") + 1;
                    MapFieldsToNovInfo[String.Format("<<ViolationScript{0}>>", i + 1)] = ViolationScript.Substring(startIndexTotal, firstLine);
                    startIndexTotal += firstLine;
                }
                else if (scriptLength > 0)
                {
                    MapFieldsToNovInfo[String.Format("<<ViolationScript{0}>>", i + 1)] = ViolationScript.Substring(startIndexTotal, scriptLength);
                }
                scriptLength -= firstLine;
            }

            // building Summary of law print string/Lines            
            //var summaryOfLawLength = !String.IsNullOrEmpty(NovMaster.NovInformation.SummaryOfLaw) ? NovMaster.NovInformation.SummaryOfLaw.Length : 0;
            var startIndexTotal1 = 0;
            string SummaryOfLaw = ""; // GenerateTestValueForField(410);

            SummaryOfLaw += GenerateTestValueForField(76) + " ";
            SummaryOfLaw += GenerateTestValueForField(76) + " ";
            SummaryOfLaw += GenerateTestValueForField(76) + " ";
            SummaryOfLaw += GenerateTestValueForField(76) + " ";
            SummaryOfLaw += GenerateTestValueForField(76) + " ";
            var summaryOfLawLength = SummaryOfLaw.Length;

            // Start building dyanmic Summry of law rows/Lines

            int tLines = 0;
            int summeryOfLawPosition = 3325;
            int rowSpace = 25;
            string sLawString = "";
            int LinePosition = 0;
            string linePrefix = "";
            if (IS_DISPLAY_SUMMARY_OF_LAW)
            {
                if (summaryOfLawLength != 0)
                {
                    tLines = summaryOfLawLength / 78;

                    tLines += 2;

                    for (int j = 0; j <= tLines; j++)
                    {
                        int k = j + 1;
                        //string linePrefix = "@" + LinePosition + ",10:ZP04P| ";
                        if (summaryOfLawLength >= 78)
                        {
                            LinePosition = summeryOfLawPosition + k * rowSpace;

                            linePrefix = "@" + LinePosition + ",10:ZP04P| ";

                            firstLine = SummaryOfLaw.Substring(startIndexTotal1, 78).LastIndexOf(" ") + 1;
                            sLawString += linePrefix + SummaryOfLaw.Substring(startIndexTotal1, firstLine) + "| ";
                            startIndexTotal1 += firstLine;
                        }
                        else if (summaryOfLawLength > 0)
                        {
                            LinePosition = summeryOfLawPosition + k * rowSpace;
                            linePrefix = "@" + LinePosition + ",10:ZP04P| ";

                            sLawString += linePrefix + SummaryOfLaw.Substring(startIndexTotal1, summaryOfLawLength) + "| ";
                        }

                        summaryOfLawLength -= firstLine;
                    }
                }
                MapFieldsToNovInfo.Add("<<SummaryOfLawPlaceHolder>>", sLawString);
                LinePosition += 25;
            }
            else
            {
                LinePosition = PRN_DEFAULT_END;
            }

            MapFieldsToNovInfo.Add("<<LinePosition>>", Convert.ToString(LinePosition));
            LinePosition += 55;
            MapFieldsToNovInfo.Add("<<TicketPrintingTypePosition>>", Convert.ToString(LinePosition));
            //MapFieldsToNovInfo.Add("<<PrintStop>>", Convert.ToString(LinePosition + 139 - 1482)); // last section print stop line position.
            MapFieldsToNovInfo.Add("<<PrintStop>>", Convert.ToString(LinePosition + 139 - PRN_PRINT_STOP)); // last section print stop line position. after remove the Summary of Law


            //End building dyanmic Summry of law rows/Lines 

            return MapFieldsToNovInfo;
        }
    }
}
