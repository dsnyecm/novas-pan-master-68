﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Threading.Tasks;

using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Services;
using DSNY.Novas.Common.Interfaces;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class SyncStatusViewModel : ViewModelBase
    {
        private string _syncStatus;
        private string _currentTable;
        private string _currentTableProgress;
        private bool _syncInProgress;
        private string _syncResult;
        private ApiUrl _apiUtil;

        private SyncService _syncService;
        private LocalDeviceSettingsService _localSettingsService;
        private IDnsWatcher _dnsWatcher;
        private string networkName;
        private string ipAddress;
        private string aPICurrentUrl;
        public DateTime _lastSyncTime;

        public override string Title => "Sync Status ";

        public static string SYNC_TIME = "Syncing time...\r\n";
        public static string SYNC_UP_TO_SERVER = "Sync To Server...\r\n";
        public static string SYNC_USER = "Sync Users To Server...\r\n";
        public static string SYNC_CLIENT = "Sync Data To Client...\r\n";
        public static string SYNC_TICKET_TO_SERVER = "\r\nSyncing violations to Server...\r\n";

        public static string SYNC_TIME_COMPLETE = "\r\nSync Time is completed successfully!\r\n"; // message before NH-722 was "Time sync complete"
        public static string SYNC_UP_COMPLETE = "\r\nSync To Server is completed successfully!\r\n"; // message before NH-722 was "Sync to server complete!"
        public static string SYNC_CLIENT_COMPLETE = "\r\nSync Down is completed successfully!\r\n"; // message before NH-722 was "Sync to client complete!"
        public static string SYNC_FULL_COMPLETE = "\r\nFull Sync is completed successfully!\r\n"; // message before NH-722 was "Sync complete!"
        public static string SYNC_USER_COMPLETE = "\r\nSync Users to server is completed successfully!\r\n"; // message before NH-722 was "Sync to server complete!"

        public static string SYNC_TIME_FAIL = "\r\nSync Time failed!\r\n";
        public static string SYNC_UP_FAIL = "\r\nSync To Server failed!\r\n";
        public static string SYNC_USER_FAIL = "\r\nSync Users To Server failed!\r\n";
        public static string SYNC_CLIENT_FAIL = "\r\nSync Data To Client failed\r\n";
        public static string SYNC_FAIL = "\r\nSync Data failed unexpectedly!\r\n";
        public static string SYNC_FULL_FAIL = "\r\nFull Sync failed!\r\n";

        public static bool IS_ALERT_SYNC_COMPLETE = false;
        private LocalDeviceSettingsService _deviceSettingsService;

        public override async Task LoadAsync()
        {
            //_dnsWatcher = DependencyResolver.Get<IDnsWatcher>();
            _apiUtil = new ApiUrl();
            NetworkName = _apiUtil.NetworkName;
            IpAddress = _apiUtil.IpAddress?.ToString();
            _deviceSettingsService = new LocalDeviceSettingsService();
            var lastSyncTimeSetting = await _deviceSettingsService.GetLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey);

            if (lastSyncTimeSetting != null)
            {
                //LastSyncTime = SyncTime;
                //LastSyncTime = Convert.ToDateTime(lastSyncTimeSetting.Value);
                LastSyncTime = Convert.ToDateTime(lastSyncTimeSetting.Value);


            }
            string SyncServiceUrl = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);
            APICurrentUrl = SyncServiceUrl;

           _syncService = new SyncService(SyncServiceUrl);
          

           // string SyncServiceUrl = _apiUtil.SyncServiceUrl;

            _localSettingsService = new LocalDeviceSettingsService();
            // TODO: Move the base URL for the sync service into a config file
            //_syncService = new SyncService(SyncServiceUrl);
            _syncService.SyncStatusUpdatedCommand = new Command(SyncStatusUpdated);

            SyncStatus = "Ready to Sync";

            var SYNCFAILED = CrossSettings.Current.GetValueOrDefault("SYNCFAILED", string.Empty, "SYNCFAILED");
            if (SYNCFAILED == "1")
            {                
                await AlertService.DisplayAlert(new AlertViewModel("SYNC FAILED", "You must sync this device again before use"));  // "Sync complete!" + SyncStatus));
            }

            // return Task.CompletedTask;
        }

        public string IpAddress
        {
            get => ipAddress;
            set
            {
                ipAddress = value;
                NotifyPropertyChanged();
            }
        }
        public DateTime LastSyncTime
        {
            get => _lastSyncTime;
            set
            {
                _lastSyncTime = value;
                NotifyPropertyChanged();
            }
        }
        public string NetworkName
        {
            get => networkName;
            set
            {
                networkName = value;
                NotifyPropertyChanged();
            }
        }

        public string APICurrentUrl
        {
            get => aPICurrentUrl;
            set
            {
                aPICurrentUrl = value;
                NotifyPropertyChanged();
            }
        }
        public void SyncStatusUpdated(object updates)
        {
            var updateDict = updates as Dictionary<string, string>;
            if (updateDict != null)
            {
                string tableName;
                string currentRecord;
                string currentRangeEnd;
                string recordCount;

                updateDict.TryGetValue(SyncServiceConstants.TableName, out tableName);
                updateDict.TryGetValue(SyncServiceConstants.CurrentRecord, out currentRecord);
                updateDict.TryGetValue(SyncServiceConstants.CurrentRangeEnd, out currentRangeEnd);
                updateDict.TryGetValue(SyncServiceConstants.RecordCount, out recordCount);

                if (!string.IsNullOrEmpty(tableName))
                {
                    CurrentTable = "Syncing table: " + tableName;
                }
                else
                {
                    CurrentTable = null;
                }

                if (!string.IsNullOrEmpty(currentRecord))
                {
                    var progressString = "Record: " + currentRecord;
                    if (!string.IsNullOrEmpty(currentRangeEnd))
                    {
                        progressString += " - " + currentRangeEnd;
                    }

                    if (!string.IsNullOrEmpty(recordCount))
                    {
                        progressString += " out of " + recordCount;
                    }

                    CurrentTableProgress = progressString;
                }
                else if (!string.IsNullOrEmpty(recordCount))
                {
                    CurrentTableProgress = "(" + recordCount + "records)";
                }
                else
                {
                    CurrentTableProgress = null;
                }
            }
            else
            {
                CurrentTable = null;
                CurrentTableProgress = null;
            }
            SyncResult = null;
        }
        public ICommand SyncTimeCommand => new Command(async () =>
        {
            SyncStatus = SYNC_TIME; //"Syncing time...";
            resetSyncFields();
            SyncInProgress = true;

            var result = await _syncService.SyncTime();

            if (!result)
            {
                if (IS_ALERT_SYNC_COMPLETE)
                    await AlertService.DisplayAlert(new AlertViewModel("Error", SYNC_TIME_FAIL)); //"Syncing time failed."));
                SyncInProgress = false;
                SyncStatus = SYNC_TIME_FAIL; // "Syncing time failed.";
                return;
            }

            if (IS_ALERT_SYNC_COMPLETE)
                await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_TIME_COMPLETE)); //"Time sync complete!"));

            SyncInProgress = false;
            //SyncStatus = "Ready to Sync";
            SyncResult = SYNC_TIME_COMPLETE;
        });

        public ICommand SyncUpCommand => new Command(async () => {
            SyncStatus = SYNC_UP_TO_SERVER; //"Syncing to Server...";
            resetSyncFields();
            SyncInProgress = true;
            var result = await _syncService.SyncToServer();

            if (result.ToUpper().Contains("FAILED"))
            {
                if (IS_ALERT_SYNC_COMPLETE)
                    await AlertService.DisplayAlert(new AlertViewModel("Error", SYNC_UP_FAIL)); //"Sync to server failed."));
                SyncInProgress = false;
                if (result.ToUpper().Contains("APPLICATION LOGS FAILED"))
                {
                    SyncResult = result + SYNC_UP_FAIL;
                    return;
                }
                SyncStatus = result;
                return;
            }
            if (IS_ALERT_SYNC_COMPLETE)
                await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_UP_COMPLETE)); // + "Sync to server complete!"));

            SyncInProgress = false;
            //SyncStatus = "Ready to Sync";
            SyncResult = result + SYNC_UP_COMPLETE;
        });

        public ICommand SyncUsersCommand => new Command(async () => {
            SyncStatus = SYNC_USER; // "Syncing users to Server...";
            resetSyncFields();
            SyncInProgress = true;
            var result = await _syncService.SyncUsers();

            if (result.ToUpper().Contains("FAILED"))
            {
                if (IS_ALERT_SYNC_COMPLETE)
                    await AlertService.DisplayAlert(new AlertViewModel("Error", SYNC_USER_FAIL));
                SyncInProgress = false;
                SyncStatus = result;

                return;
            }
            if (IS_ALERT_SYNC_COMPLETE)
                await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_USER_COMPLETE));
            SyncInProgress = false;
            //SyncStatus = "Ready to Sync";
            SyncResult = result + SYNC_USER_COMPLETE;
        });

        public ICommand SyncDownCommand => new Command(async () => {
            SyncStatus = SYNC_TIME; //"Syncing time to device...\r\n";
            resetSyncFields();
            SyncInProgress = true;
            var syncTimeSuccessful = await _syncService.SyncTime();
            SyncStatus += syncTimeSuccessful + "\r\n";

            SyncStatus += SYNC_CLIENT; //"Syncing data to client...\r\n";
            SyncInProgress = true;
            var syncClientSuccessful = await _syncService.SyncToClient();
            SyncStatus += syncClientSuccessful + "\r\n";

            if (syncClientSuccessful.ToUpper().Contains("FAILED"))
            {
                if (IS_ALERT_SYNC_COMPLETE)
                    await AlertService.DisplayAlert(new AlertViewModel("Error", SYNC_CLIENT_FAIL));//"Sync to client failed."));
                SyncInProgress = false;
                SyncStatus = SYNC_CLIENT_FAIL; // "Sync to client failed.";
                return;
            }

            bool InsertOrUpdateResultSuccessful = await _localSettingsService.InsertOrUpdateLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey, DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss"));
            if (!InsertOrUpdateResultSuccessful)
            {
                SyncInProgress = false;
                SyncResult = SYNC_CLIENT_FAIL;
                return;
            }

            if (IS_ALERT_SYNC_COMPLETE)
                await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_CLIENT_COMPLETE)); // "Sync to client complete!"));
            SyncInProgress = false;
            SyncResult = SYNC_CLIENT_COMPLETE;
            //SyncStatus = "Ready to Sync";
        });

        public ICommand FullSyncCommand => new Command(async () => {
            CrossSettings.Current.AddOrUpdateValue("SyncStatus_FullSyncCalled_", true.ToString());
            ForceLogOff();

            SyncStatus = "";
            SyncStatus += SYNC_TICKET_TO_SERVER; // "\r\nSyncing violations to Server...\r\n";
            resetSyncFields();
            SyncInProgress = true;
            var syncToServerSuccessful = await _syncService.SyncToServer();

            SyncStatus += syncToServerSuccessful + "\r\n";

            SyncStatus += SYNC_TIME;// "Syncing time to device...\r\n";
            SyncInProgress = true;
            var syncTimeSuccessful = await _syncService.SyncTime();
            SyncStatus += syncTimeSuccessful + "\r\n";

            SyncStatus += SYNC_USER; // "Syncing users to Server...\r\n";
            SyncInProgress = true;
            var syncUserSuccessful = await _syncService.SyncUsers();

            SyncStatus += syncUserSuccessful + "\r\n";

            SyncStatus += SYNC_CLIENT; // "Syncing data to client...\r\n";
            SyncInProgress = true;
            var clientSyncSuccessful = await _syncService.SyncToClient();

            await PopulateCollections();

            SyncStatus += clientSyncSuccessful + "\r\n";

            if (clientSyncSuccessful.ToUpper().Contains("FAILED") || syncToServerSuccessful.ToUpper().Contains("FAILED") || syncToServerSuccessful.ToUpper().Contains("FAILED"))
            {
                SyncInProgress = false;
                SyncResult = SYNC_FULL_FAIL;

                var SYNCFAILED = CrossSettings.Current.GetValueOrDefault("SYNCFAILED", string.Empty, "SYNCFAILED");
                if(SYNCFAILED !="1")
                {
                    CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "1", "SYNCFAILED");
                    await AlertService.DisplayAlert(new AlertViewModel("SYNC FAILED", "You must sync this device again before use"));  // "Sync complete!" + SyncStatus));
                }
                
                return;
            }

            if (IS_ALERT_SYNC_COMPLETE)
                await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_FULL_COMPLETE));  // "Sync complete!" + SyncStatus));
            SyncInProgress = false;
            SyncResult = SYNC_FULL_COMPLETE;
            LastSyncTime = DateTime.Now;
            SyncTime = LastSyncTime;
            // http://jira.dsnyad.nycnet/browse/NH-1366 only set back to "0" when it SUCCEEDS.
            CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "0", "SYNCFAILED");
        });

        public string SyncStatus
        {
            get => _syncStatus;
            set { _syncStatus = value; NotifyPropertyChanged(); }
        }

        public string CurrentTable
        {
            get => _currentTable;
            set { _currentTable = value; NotifyPropertyChanged(); }
        }

        public string CurrentTableProgress
        {
            get => _currentTableProgress;
            set { _currentTableProgress = value; NotifyPropertyChanged(); }
        }

        public bool SyncInProgress
        {
            get => _syncInProgress;
            set { _syncInProgress = value; NotifyPropertyChanged(); }
        }

        public string SyncResult
        {
            get => _syncResult;
            set { _syncResult = value; NotifyPropertyChanged(); }
        }

        public override bool ShowNextButton => false;

        public override bool ShowMenuButton => false;

        public override ICommand BackCommand => new Command(async () =>
        {
            if (!SyncInProgress)
            {
                //await NavigationService.PopFromSyncToLoginIfTimeoutScreenPresentOtherwiseGoBack();
                await NavigationService.PopModalAsync();
            }
        });
        private void resetSyncFields()
        {
            CurrentTable = null;
            CurrentTableProgress = null;
            SyncResult = null;
        }


        public async Task serviecall()
        {
            //ping to server
            SyncStatus = "Pingging to servser...";
            string SyncServiceUrl = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);
            _syncService = new SyncService(SyncServiceUrl);
            var serverPing = await _syncService.SyncTime();

            if (serverPing)
            { 
                CrossSettings.Current.AddOrUpdateValue("SyncStatus_FullSyncCalled_", true.ToString());
                ForceLogOff();
                _apiUtil = new ApiUrl();
                NetworkName = _apiUtil.NetworkName;
                IpAddress = _apiUtil.IpAddress?.ToString();
           

                //SyncStatus = "";
                SyncStatus += SYNC_TICKET_TO_SERVER; // "\r\nSyncing violations to Server...\r\n";
                resetSyncFields();
                SyncInProgress = true;
                var syncToServerSuccessful = await _syncService.SyncToServer();

                SyncStatus += syncToServerSuccessful + "\r\n";

                SyncStatus += SYNC_TIME;// "Syncing time to device...\r\n";
                SyncInProgress = true;
                var syncTimeSuccessful = await _syncService.SyncTime();
                SyncStatus += syncTimeSuccessful + "\r\n";

                SyncStatus += SYNC_USER; // "Syncing users to Server...\r\n";
                SyncInProgress = true;
                var syncUserSuccessful = await _syncService.SyncUsers();

                SyncStatus += syncUserSuccessful + "\r\n";

                SyncStatus += SYNC_CLIENT; // "Syncing data to client...\r\n";
                SyncInProgress = true;
                var clientSyncSuccessful = await _syncService.SyncToClient();

                SyncStatus += clientSyncSuccessful + "\r\n";
                
                await PopulateCollections();

                if (IS_ALERT_SYNC_COMPLETE)
                    await AlertService.DisplayAlert(new AlertViewModel("Success", SYNC_FULL_COMPLETE));  // "Sync complete!" + SyncStatus));
                SyncInProgress = false;

                if (clientSyncSuccessful.ToUpper().Contains("FAILED") || syncToServerSuccessful.ToUpper().Contains("FAILED") || syncToServerSuccessful.ToUpper().Contains("FAILED"))
                {
                    SyncResult = SYNC_FAIL;

                    var SYNCFAILED = CrossSettings.Current.GetValueOrDefault("SYNCFAILED", string.Empty, "SYNCFAILED");
                    if (SYNCFAILED != "1")
                    {
                        CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "1", "SYNCFAILED");
                        await AlertService.DisplayAlert(new AlertViewModel("SYNC FAILED", "You must sync this device again before use"));  // "Sync complete!" + SyncStatus));
                    }

                    //await AlertService.DisplayAlert(new AlertViewModel("SYNC FAILED!", "You must sync this device again before use"));
                }
               
                else
                {
                    SyncResult = SYNC_FULL_COMPLETE;
                    LastSyncTime = DateTime.Now;
                    SyncTime = LastSyncTime;
                    // http://jira.dsnyad.nycnet/browse/NH-1366 only set back to "0" when it SUCCEEDS.
                    CrossSettings.Current.AddOrUpdateValue("SYNCFAILED", "0", "SYNCFAILED");
                }
            }
            //else
            //{
            //    SyncStatus = "Server not responding...";
            //}
        }

        public async Task PopulateCollections()
        {
            #region ReportingLevels

            ReportingLevelCollection.Clear();
            await PersonnelDataService.GetReportingLevelListAsync()
                .ContinueWith(async reportingLevels =>
                {
                    var reportingLevelsResult = await reportingLevels;

                    if (!reportingLevelsResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("ReportingLevels", "ReportingLevels not found"));
                    else
                        reportingLevelsResult?.ForEach(reportingLevel => ReportingLevelCollection.Add(reportingLevel));
                });

            #endregion
            
            #region Agencies

            AgencyCollection.Clear();
            await PersonnelDataService.GetAgencyListAsync()
                .ContinueWith(async agencies =>
                {
                    var agenciesResult = await agencies;

                    if (!agenciesResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("Agencies", "Agencies not found"));
                    else
                        agenciesResult?.ForEach(agency => AgencyCollection.Add(agency));
                });

            #endregion
            
            #region Holidays

            HolidayCollection.Clear();
            await PersonnelDataService.GetHolidaysAsync()
                .ContinueWith(async holidays =>
                {
                    var holidaysResult = await holidays;

                    if (!holidaysResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("Holidays", "Holidays not found"));
                    else
                        holidaysResult?.ForEach(holiday => HolidayCollection.Add(holiday));
                });

            #endregion

            #region HearingDates

            HearingDateCollection.Clear();
            await PersonnelDataService.GetHearingDatesAsync(0)
                .ContinueWith(async hearingDates =>
                {
                    var hearingDatesResult = await hearingDates;

                    if (!hearingDatesResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("HearingDates", "HearingDates not found"));
                    else
                        hearingDatesResult?.ForEach(hearingDate => HearingDateCollection.Add(hearingDate));
                });

            #endregion

            #region HearingDateTimes

            HearingDateTimeCollection.Clear();
            await PersonnelDataService.GetHearingTimesAsync(0)
                .ContinueWith(async hearingDateTimes =>
                {
                    var hearingDateTimesResult = await hearingDateTimes;

                    if (!hearingDateTimesResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("HearingDateTimes", "HearingDateTimes not found"));
                    else
                        hearingDateTimesResult?.ForEach(hearingDateTime => HearingDateTimeCollection.Add(hearingDateTime));
                });

            #endregion

            #region BoroSiteMaster

            BoroSiteMasterCollection.Clear();
            await PersonnelDataService.GetBoroSites()
                .ContinueWith(async boroSiteMasters =>
                {
                    var boroSiteMastersResult = await boroSiteMasters;

                    if (!boroSiteMastersResult?.Any() ?? false)
                        await AlertService.DisplayAlert(new AlertViewModel("BoroSiteMasters", "BoroSiteMasters not found"));
                    else
                        boroSiteMastersResult?.ForEach(boroSiteMaster => BoroSiteMasterCollection.Add(boroSiteMaster));
                });

            #endregion
        }
    }
}
