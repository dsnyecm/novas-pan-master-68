﻿using System;
using System.Collections.Generic;
using System.Text;
using DSNY.Novas.Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class ViolatorHitViewModel : ViewModelBase
    {
        private ObservableCollection<Violator> _violatorList;
        private Violator _selectedViolator;
        private bool _isBoxEnabled;
        private string _boxVal;

        public override string Title => "Violator Hit";

        public ViolatorHitViewModel(ObservableCollection<Violator> violatorList)
        {
            violatorList.Add(new Violator(){BusinessName="None of the Above"});
            ViolatorList = violatorList;
            var violatorBizName = getViolatorHitBusinessName();
            if (!string.IsNullOrEmpty(violatorBizName)) {
                if (IsBoxEnabled)
                    BoxVal = violatorBizName; //FindViolator(violatorBizName);
                else
                    SelectedViolator = FindViolator(violatorBizName);
            }
            else 
                SelectedViolator = ViolatorList.FirstOrDefault();
        }
        private Violator FindViolator(string businessName) {
            foreach (Violator v in ViolatorList)
            {
                if (v.BusinessName.Trim() == businessName.Trim())
                {
                    return v;
                }
            }
            return null;
        }

        public ObservableCollection<Violator> ViolatorList
        {
            get => _violatorList;
            set { _violatorList = value; NotifyPropertyChanged(); }
        }

        public Violator SelectedViolator
        {
            get => _selectedViolator;
            set
            {
                _selectedViolator = value;
                if (_selectedViolator?.BusinessName == "None of the Above")
                {
                    IsBoxEnabled = true;
                    BoxVal = null;
                }
                else
                {
                    IsBoxEnabled = false;
                    var violatorBizName = getViolatorHitBusinessName();
                    if (!string.IsNullOrEmpty(violatorBizName)) {
                        BoxVal = violatorBizName;
                        _selectedViolator = FindViolator(violatorBizName);
                    }
                    else 
                        BoxVal = _selectedViolator?.BusinessName;
                }                
                NotifyPropertyChanged();
            }
        }

        public bool IsBoxEnabled
        {
            get => _isBoxEnabled;
            set { _isBoxEnabled = value; NotifyPropertyChanged(); }
        }

        public string BoxVal
        {
            get => _boxVal;
            set { _boxVal = value; NotifyPropertyChanged(); }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (string.IsNullOrWhiteSpace(BoxVal))
            {
                alerts.Add(new AlertViewModel("12055", WorkFlowMessages.DSNYMSG_NOV055, elementName: "BoxVal"));
            }

            if (IsBoxEnabled)
            {
                foreach (Violator v in ViolatorList)
                {
                    if (v.BusinessName.Trim() == Convert.ToString(BoxVal).Trim())
                    {
                        alerts.Add(new AlertViewModel("12076", WorkFlowMessages.DSNYMSG_NOV076));
                        break;
                    }
                }
            }

            return Task.FromResult(alerts);
        }

        public override void WriteFieldValuesToNovMaster()
        {
            var novInfo = NovInformation;

            novInfo.IsPlaceAddressHit = "Y";

            var selectedViolatorIsNoneOfTheAbove = SelectedViolator?.BusinessName == "None of the Above";
            novInfo.IsMultipleOffences = selectedViolatorIsNoneOfTheAbove ? "N": "Y";
            novInfo.BusinessName = selectedViolatorIsNoneOfTheAbove ? BoxVal : SelectedViolator?.BusinessName;

            if (SelectedViolator == null || novInfo.IsMultipleOffences == "N")
                return;

            //For violations greater than thirteen than set the value to 13+ for violation code, for less, just use the regular number + ordinal
            var printViolationCode = $"{SelectedViolator.NextViolationCode.Trim()}-";
                
            // This is what will actually appear on the ticket and be used in the Affidavit table, but we still need to keep the original code in the ViolationCode field to drive other application logic
            novInfo.PrintViolationCode = SelectedViolator.NoOfTimes >= 13
                ? $"{printViolationCode}{13.DisplayWithSuffix()}+"
                : $"{printViolationCode}{SelectedViolator.NoOfTimes.DisplayWithSuffix()}";

            novInfo.MailableAmount = (decimal)SelectedViolator.MinFine;
            novInfo.MaximumAmount = (decimal)SelectedViolator.MaxFine;
        }

        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }
            WriteFieldValuesToNovMaster();
            WriteValueToCrossSettings();

            await SaveTicket();

            //TODO: Check if Repeat violator needs to be saved here
            

            await NavigationService.PushAsync(new ViolationDetailsViewModel() { NovMaster = NovMaster });

            NavigationInProgress = false;
        });

        public override void WriteValueToCrossSettings()
        {
            if (NovMaster!=null && NovMaster.NovInformation!=null && !string.IsNullOrEmpty(NovMaster.NovInformation.BusinessName) )
                CrossSettings.Current.AddOrUpdateValue("ViolatorHit_BusinessName_" + NovMaster.NovInformation.NovNumber.ToString(), NovMaster.NovInformation.BusinessName);
        }
        private string getViolatorHitBusinessName()
        {
            if (NovMaster != null && NovMaster.NovInformation != null )
            {
                var businessName = CrossSettings.Current.GetValueOrDefault("ViolatorHit_BusinessName_" + NovMaster.NovInformation.NovNumber.ToString(), "");
                return businessName;
            }
            return "";
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;
    }
}
