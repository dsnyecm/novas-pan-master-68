﻿using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class PersonBeingServed2ViewModel : ViewModelBase
    {
        private string _firstName;
        private string _lastName;
        private string _middleInitial;
        private string _streetName;
        private string _streetNumber;
        private string _apt;
        private string _licenseNumber;
        private string _licenseTypeDesc;
        private string _issuedBy; 
        private bool _isBoroEnabled;
        private List<LookupTable> _sexes;
        private LookupTable _sexSelectedItem;
        private List<BoroMaster> _boros;
        private BoroMaster _boroSelectedItem;
        private string _city;
        private List<LookupTable> _states;
        private LookupTable _stateSelectedItem;
        private string _zipCode;
        private string _businessName;
        private bool _isNameChecked;
        private bool _isBusinessNameVisible;
        private bool _isLicenseNumberVisible;
        private bool _areNameFieldsVisible;
        private bool _isBusinessCheckVisible;
        private bool _hasCompletedInitialLoad;
        private bool _isAptVisible;
        private readonly ILookupService _lookupService;
        private readonly IPersonBeingServedService _personBeingServedService;

        public PersonBeingServed2ViewModel()
        {
            IsNameChecked = true; // Default to checked here
            _lookupService = DependencyResolver.Get<ILookupService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
        }

        public override string Title => "Person Being Served";

        public override async Task LoadAsync()
        {
            IsLicenseNumberVisible = false;
            if (_hasCompletedInitialLoad)
            {
                return;
            }

            IsAptVisible = false;
            if (Boros == null) { Boros = await _personBeingServedService.GetBoros(); }
            if (Sexes == null) { Sexes = await _lookupService.GetLookupTable("SEX"); }
            if (States == null) { States = await _lookupService.GetLookupTable("STATE"); }

            var novInfo = NovMaster.NovInformation;
            IsBusinessCheckVisible = false;

            if (NovMaster.NovInformation.IsBusiness)
            {
                NovMaster.AffidavitOfService.IsPersonBeingServedCleared = true;
                IsNameChecked = true; // need to clear the data if it's a business and ask the User to enter the information of the respondent
            }

            var selectedNovNumber = CrossSettings.Current.GetValueOrDefault("MakeSimilar_SelectedViolation", string.Empty);
            string isMakeSimilar = "false";
            if (!String.IsNullOrEmpty(selectedNovNumber))
            {
                isMakeSimilar = CrossSettings.Current.GetValueOrDefault("IsMakeSimilar_" + selectedNovNumber, string.Empty);
            }

            if (!NovMaster.AffidavitOfService.IsPersonBeingServedCleared && !NovMaster.AffidavitOfService.ShouldLoadFromAffidavit 
                &&(NovMaster.NovInformation.ViolationTypeId == "A" || NovMaster.NovInformation.ViolationTypeId == "O"))
            {
                FirstName = novInfo.Resp1FirstName;
                MiddleInitial = novInfo.Resp1MiddleInitial;
                LastName = novInfo.Resp1LastName;
                SexSelectedItem = Sexes.Find((item) => item.Code.Equals(novInfo.Resp1Sex));

                StreetNumber = novInfo.Resp1HouseNo;
                StreetName = novInfo.Resp1Address;
                Apt = novInfo.Resp1Address1;
                BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(novInfo.Resp1BoroCode));
                City = novInfo.Resp1City;
                StateSelectedItem = States.Find((state) => state.Code.Equals(novInfo.Resp1State));
                ZipCode = novInfo.Resp1Zip;
                LicenseNumber = novInfo.LicenseNumber;
                IssuedBy = novInfo.LicenseAgency;
                LicenseTypeDesc = novInfo.LicenseTypeDesc;
            }
            else if (NovMaster.AffidavitOfService.ShouldLoadFromAffidavit || isMakeSimilar == "true")
            {
                FirstName = NovMaster.AffidavitOfService.ServedFName;
                MiddleInitial = NovMaster.AffidavitOfService.ServedMInit;
                LastName = NovMaster.AffidavitOfService.ServedLName;
                SexSelectedItem = Sexes.Find((item) => item.Code.Equals(NovMaster.AffidavitOfService.ServedSex));

                StreetNumber = NovMaster.AffidavitOfService.ServedHouseNo;
                StreetName = NovMaster.AffidavitOfService.ServedAddress;
                BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(NovMaster.AffidavitOfService.ServedBoroCode));
                City = NovMaster.AffidavitOfService.ServedCity;
                StateSelectedItem = States.Find((state) => state.Code.Equals(NovMaster.AffidavitOfService.ServedState));
                ZipCode = NovMaster.AffidavitOfService.ServedZip;
                LicenseNumber = NovMaster.AffidavitOfService.LicenseNumber;
                IssuedBy = NovMaster.AffidavitOfService.AgencyId;
                LicenseTypeDesc = NovMaster.AffidavitOfService.LicenseTypeDesc;
            }
            else
            {
               var firstName = CrossSettings.Current.GetValueOrDefault("FirstName_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var lastName = CrossSettings.Current.GetValueOrDefault("LastName_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var middleInitial = CrossSettings.Current.GetValueOrDefault("MiddleInitial_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var sex =  CrossSettings.Current.GetValueOrDefault("Sex_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var streetNumber = CrossSettings.Current.GetValueOrDefault("StreetNumber_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var streetName = CrossSettings.Current.GetValueOrDefault("StreetName_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var boro1 = CrossSettings.Current.GetValueOrDefault("Boro_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var city = CrossSettings.Current.GetValueOrDefault("City_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var state1 =  CrossSettings.Current.GetValueOrDefault("State_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var zipCode = CrossSettings.Current.GetValueOrDefault("ZipCode_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var licenseNumber = CrossSettings.Current.GetValueOrDefault("LicenseNumber_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var issuedBy = CrossSettings.Current.GetValueOrDefault("IssuedBy_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);
               var licenseTypeDesc = CrossSettings.Current.GetValueOrDefault("LicenseTypeDesc_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);

                FirstName = !String.IsNullOrEmpty(firstName) ? firstName : "";
                MiddleInitial = !String.IsNullOrEmpty(middleInitial) ? middleInitial : "";
                LastName = !String.IsNullOrEmpty(lastName) ? lastName : "";
                SexSelectedItem = Sexes.Find((item) => item.Code.Equals(sex));

                StreetNumber = !String.IsNullOrEmpty(streetNumber) ? streetNumber : "";
                StreetName = !String.IsNullOrEmpty(streetName) ? streetName : "";
                BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(boro1));
                City = !String.IsNullOrEmpty(city) ? city : "";
                StateSelectedItem = States.Find((state) => state.Code.Equals(state1));
                ZipCode = !String.IsNullOrEmpty(zipCode) ? zipCode : "";
                LicenseNumber = !String.IsNullOrEmpty(licenseNumber) ? licenseNumber : "";
                IssuedBy = !String.IsNullOrEmpty(issuedBy) ? issuedBy : "";
                LicenseTypeDesc = !String.IsNullOrEmpty(licenseTypeDesc) ? licenseTypeDesc : "";

            }

            if (String.IsNullOrEmpty(StateSelectedItem?.Code)) {
                StateSelectedItem = States.Find((state) => state.Code.Equals("NY"));
            };
            
            _hasCompletedInitialLoad = true;
        }

        public string BusinessName
        {
            get => _businessName;
            set { _businessName = value; NotifyPropertyChanged(); }
        }

        public bool IsBusinessNameVisible
        {
            get => _isBusinessNameVisible;
            set { _isBusinessNameVisible = value; NotifyPropertyChanged(); }
        }

        public string FirstName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                NotifyPropertyChanged();
            }
        }

        public string LastName
        {
            get => _lastName;
            set { _lastName = value; NotifyPropertyChanged(); }
        }

        public string MiddleInitial
        {
            get => _middleInitial;
            set { _middleInitial = value; NotifyPropertyChanged(); }
        }

        public bool IsLicenseNumberVisible
        {
            get => _isLicenseNumberVisible;
            set { _isLicenseNumberVisible = value;NotifyPropertyChanged(); }
        }
        public bool AreNameFieldsVisible
        {
            get => _areNameFieldsVisible;
            set { _areNameFieldsVisible = value; NotifyPropertyChanged(); }
        }

        public string StreetName
        {
            get => _streetName;
            set { _streetName = value; NotifyPropertyChanged(); }
        }

        public string StreetNumber
        {
            get => _streetNumber;
            set { _streetNumber = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Sexes
        {
            get => _sexes;
            set { _sexes = value; NotifyPropertyChanged(); }
        }

        public LookupTable SexSelectedItem
        {
            get => _sexSelectedItem;
            set { _sexSelectedItem = value; NotifyPropertyChanged(); }
        }

        public string Apt
        {
            get => _apt;
            set { _apt = value; NotifyPropertyChanged(); }
        }

        public bool IsBoroEnabled
        {
            get => _isBoroEnabled;
            set { _isBoroEnabled = value; NotifyPropertyChanged(); }
        }

        public List<BoroMaster> Boros
        {
            get => _boros;
            set { _boros = value; NotifyPropertyChanged(); }
        }

        public BoroMaster BoroSelectedItem
        {
            get => _boroSelectedItem;
            set { _boroSelectedItem = value; NotifyPropertyChanged(); }
        }

        public string City
        {
            get => _city;
            set { _city = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> States
        {
            get => _states;
            set { _states = value; NotifyPropertyChanged(); }
        }

        public LookupTable StateSelectedItem
        {
            get => _stateSelectedItem;
            set
            {
                _stateSelectedItem = value;
                if (_stateSelectedItem?.Code == "NY")
                {
                    IsBoroEnabled = true;
                }
                else
                {
                    BoroSelectedItem = null;
                    IsBoroEnabled = false;
                }
                NotifyPropertyChanged();
            }
        }

        public string ZipCode
        {
            get => _zipCode;
            set { _zipCode = value; NotifyPropertyChanged(); }
        }

        public string LicenseNumber
        {
            get => _licenseNumber;
            set { _licenseNumber = value; NotifyPropertyChanged(); }
        }

        public string IssuedBy
        {
            get => _issuedBy;
            set { _issuedBy = value; NotifyPropertyChanged(); }
        }
        public string LicenseTypeDesc
        {
            get => _licenseTypeDesc;
            set { _licenseTypeDesc = value; NotifyPropertyChanged(); }
        }
        public bool IsNameChecked
        {
            get => _isNameChecked;
            set
            {
                _isNameChecked = value;
                if (_isNameChecked)
                {
                    IsBusinessNameVisible = false;
                    AreNameFieldsVisible = true;
                }
                else
                {
                    IsBusinessNameVisible = true;
                    AreNameFieldsVisible = false;
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsBusinessCheckVisible
        {
            get => _isBusinessCheckVisible;
            set { _isBusinessCheckVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsAptVisible
        {
            get => _isAptVisible;
            set { _isAptVisible = value; NotifyPropertyChanged(); }
        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (IsNameChecked)
            {
                if (string.IsNullOrEmpty(FirstName))
                {
                    alerts.Add(new AlertViewModel("12047", WorkFlowMessages.DSNYMSG_NOV047, elementName: "FirstName"));
                }

                if (string.IsNullOrEmpty(LastName))
                {
                    alerts.Add(new AlertViewModel("12046", WorkFlowMessages.DSNYMSG_NOV046, elementName: "LastName"));
                }

                if (SexSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12048", WorkFlowMessages.DSNYMSG_NOV048, elementName: "Gender"));
                }
            }
            else
            {
                if (string.IsNullOrEmpty(BusinessName))
                {
                    alerts.Add(new AlertViewModel("12011", WorkFlowMessages.DSNYMSG_NOV011, elementName: "BusinessName"));
                }
            }

            if (string.IsNullOrEmpty(StreetName))
            {
                alerts.Add(new AlertViewModel("12049", WorkFlowMessages.DSNYMSG_NOV049, elementName: "StreetName"));
            }

            if (string.IsNullOrEmpty(StreetNumber))
            {
                alerts.Add(new AlertViewModel("12049", WorkFlowMessages.DSNYMSG_NOV049, elementName: "StreetNumber"));
            }

            if (string.IsNullOrEmpty(City))
            {
                alerts.Add(new AlertViewModel("12050", WorkFlowMessages.DSNYMSG_NOV050, elementName: "City"));
            }

            if (!string.IsNullOrEmpty(ZipCode))
            {
                if ((ZipCode.Length < 5 && ZipCode.Length > 1) || ZipCode.Length > 5 || ZipCode.Contains("."))
                {
                    alerts.Add(new AlertViewModel("12052", WorkFlowMessages.DSNYMSG_NOV052, elementName: "ZipCode"));
                }
            }
            
            return Task.FromResult(alerts);
        }

        public override ICommand BackCommand => new Command(async () =>
        {
            CrossSettings.Current.AddOrUpdateValue("FirstName_" + NovMaster.NovInformation.NovNumber.ToString(), FirstName);
            CrossSettings.Current.AddOrUpdateValue("LastName_" + NovMaster.NovInformation.NovNumber.ToString(), LastName);
            CrossSettings.Current.AddOrUpdateValue("MiddleInitial_" + NovMaster.NovInformation.NovNumber.ToString(), MiddleInitial);
            CrossSettings.Current.AddOrUpdateValue("Sex_" + NovMaster.NovInformation.NovNumber.ToString(), SexSelectedItem?.Code);
            CrossSettings.Current.AddOrUpdateValue("StreetNumber_" + NovMaster.NovInformation.NovNumber.ToString(), StreetNumber);
            CrossSettings.Current.AddOrUpdateValue("StreetName_" + NovMaster.NovInformation.NovNumber.ToString(), StreetName + (!String.IsNullOrWhiteSpace(Apt) ? ", Apt " + Apt + ",  ": ""));

            CrossSettings.Current.AddOrUpdateValue("Boro_" + NovMaster.NovInformation.NovNumber.ToString(), BoroSelectedItem?.BoroId);
            CrossSettings.Current.AddOrUpdateValue("City_" + NovMaster.NovInformation.NovNumber.ToString(), City);
            CrossSettings.Current.AddOrUpdateValue("State_" + NovMaster.NovInformation.NovNumber.ToString(), StateSelectedItem?.Code);
            CrossSettings.Current.AddOrUpdateValue("ZipCode_" + NovMaster.NovInformation.NovNumber.ToString(), ZipCode);
            CrossSettings.Current.AddOrUpdateValue("LicenseNumber_" + NovMaster.NovInformation.NovNumber.ToString(), LicenseNumber);
            CrossSettings.Current.AddOrUpdateValue($"IDNumber_{NovNumber}", LicenseNumber);
            CrossSettings.Current.AddOrUpdateValue("IssuedBy_" + NovMaster.NovInformation.NovNumber.ToString(), IssuedBy);
            CrossSettings.Current.AddOrUpdateValue("LicenseTypeDesc_" + NovMaster.NovInformation.NovNumber.ToString(), LicenseTypeDesc);

            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            WriteFieldValuesToNovMaster();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
        });

        public override void WriteFieldValuesToNovMaster()
        {
            NovMaster.AffidavitOfService.ServedFName = FirstName;
            NovMaster.AffidavitOfService.ServedMInit = MiddleInitial;
            NovMaster.AffidavitOfService.ServedLName = LastName;
            NovMaster.AffidavitOfService.ServedSex = SexSelectedItem?.Code;
            NovMaster.AffidavitOfService.ServedHouseNo = StreetNumber;
            NovMaster.AffidavitOfService.ServedAddress = StreetName + (!String.IsNullOrWhiteSpace(Apt) ? ", Apt " + Apt + ",  ": "");
            NovMaster.AffidavitOfService.ServedBoroCode = BoroSelectedItem?.BoroId;
            NovMaster.AffidavitOfService.ServedCity = City;
            NovMaster.AffidavitOfService.ServedState = StateSelectedItem?.Code;
            NovMaster.AffidavitOfService.ServedZip = ZipCode;

            NovMaster.AffidavitOfService.LicenseNumber = LicenseNumber;

            //  NovMaster.AffidavitOfService.IssuedBy = IssuedBy;
            // NovMaster.AffidavitOfService.LicenseTypeDesc = LicenseTypeDesc;
            NovMaster.AffidavitOfService.ShouldLoadFromAffidavit = true;
            NovMaster.AffidavitOfService.PersonallyServeFlag = "P";
            
        }
    }
}
