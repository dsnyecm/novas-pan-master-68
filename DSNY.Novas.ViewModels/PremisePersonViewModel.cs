﻿using DSNY.Novas.ViewModels.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class PremisePersonViewModel : ViewModelBase
    {
        private string _lastName;
        private string _firstName;
        private string _mI;
        private bool _respondentNotPresent;
        private bool _noOfficerPresent;
        private bool _unableToSecureIdentification;
        private bool _isEnabled;
        public PremisePersonViewModel()
        {
            IsEnabled = true;
            //Set Respondent not present initially so user can't navigate next without choosing an option
            RespondentNotPresent = true;
        }

        public override Task LoadAsync()
        {
            if (!String.IsNullOrEmpty(NovMaster.AffidavitOfService.PremiseLName))
            {
                LastName = NovMaster.AffidavitOfService.PremiseLName;
            }
            if (!String.IsNullOrEmpty(NovMaster.AffidavitOfService.PremiseFName))
            {
                FirstName = NovMaster.AffidavitOfService.PremiseFName;
            }
            if (!String.IsNullOrEmpty(NovMaster.AffidavitOfService.PremiseMInit))
            {
                MI = NovMaster.AffidavitOfService.PremiseMInit;
            }
            if (!String.IsNullOrEmpty(NovMaster.AffidavitOfService.AlternativeService2)) {
                RespondentNotPresent = true;
                NoOfficerPresent = false;
                UnableToSecureIdentification = false;
                switch (NovMaster.AffidavitOfService.AlternativeService2)
                {
                    case "1":
                        RespondentNotPresent = true;
                        break;
                    case "2":
                        NoOfficerPresent = true;
                        break;
                    case "3":
                        UnableToSecureIdentification = true;
                        break;
                    default:
                        {
                            RespondentNotPresent = true;
                            NoOfficerPresent = false;
                            UnableToSecureIdentification = false;
                            break;
                        }
                }
            }

            return Task.CompletedTask;
        }
        public override string Title => "Premise Person";

        public string LastName
        {
            get => _lastName;
            set { _lastName = value; NotifyPropertyChanged(); }
        }
        public string FirstName
        {
            get => _firstName;
            set { _firstName = value; NotifyPropertyChanged(); }
        }
        public string MI
        {
            get => _mI;
            set { _mI = value; NotifyPropertyChanged(); }
        }

        public bool RespondentNotPresent
        {
            get => _respondentNotPresent;
            set
            {
                _respondentNotPresent = value;
                if (value)
                {
                    NoOfficerPresent = false;
                    UnableToSecureIdentification = false;
                    IsEnabled = true;
                }
                NotifyPropertyChanged();
            }
        }

        public bool NoOfficerPresent
        {
            get => _noOfficerPresent;
            set
            {
                _noOfficerPresent = value;
                if (value)
                {
                    RespondentNotPresent = false;
                    UnableToSecureIdentification = false;
                    IsEnabled = true;
                }
                NotifyPropertyChanged();
            }
        }

        public bool UnableToSecureIdentification
        {
            get => _unableToSecureIdentification;
            set
            {
                _unableToSecureIdentification = value;
                if (value)
                {
                    NoOfficerPresent = false;
                    RespondentNotPresent = false;
                    IsEnabled = false;
                    LastName = "";
                    FirstName = "";
                    MI = "";
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set { _isEnabled = value; NotifyPropertyChanged(); }
        }
        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;
        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();
            if (RespondentNotPresent || NoOfficerPresent)
            {
                if (String.IsNullOrEmpty(FirstName)){
                    alerts.Add(new AlertViewModel("12054", WorkFlowMessages.DSNYMSG_NOV054, elementName: "FirstName"));
                }

                if (String.IsNullOrEmpty(LastName))
                {
                    alerts.Add(new AlertViewModel("12054", WorkFlowMessages.DSNYMSG_NOV054, elementName: "LastName"));
                }
            }

            return Task.FromResult(alerts);
        }
        public override ViewModelBase NextViewModel
        {
            get
            {
                return new AlternativeServiceAffirmationViewModel { NovMaster = NovMaster };
            }
        }
        public override ICommand BackCommand => new Command(async () =>
        {
            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            // NH-1309 save FName, LName, MInitial only when NEXT button is clicked. Not BACK button.
            //WriteFieldValuesToNovMaster();
            NovMaster.AffidavitOfService.AlternativeService2 = RespondentNotPresent ? "1" : NoOfficerPresent ? "2" : UnableToSecureIdentification ? "3" : "";

            WriteValueToCrossSettings();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
            //NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;
        });
        public override void WriteFieldValuesToNovMaster()
        {
            NovMaster.AffidavitOfService.AlternativeService2 = RespondentNotPresent ? "1" : NoOfficerPresent ? "2" : UnableToSecureIdentification ? "3" : "";
            NovMaster.AffidavitOfService.PremiseLName = LastName;
            NovMaster.AffidavitOfService.PremiseFName = FirstName;
            NovMaster.AffidavitOfService.PremiseMInit = MI;
        }
    }
}
