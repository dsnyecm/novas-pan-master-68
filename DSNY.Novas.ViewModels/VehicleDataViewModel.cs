﻿using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using System;

namespace DSNY.Novas.ViewModels
{
    public class VehicleDataViewModel : ViewModelBase
    {
        private string _vehicleNumber;
        private string _radioNumber;
        private int _startingMileage;
        private int _endingMileage;
        private bool _isLogOff;

        private readonly IVehicleService _vehicleService;

        private bool _isButtonsEnabled;
        private bool _isEndingEnabled;
        private bool _isVehicleEnabled;
        private bool _isNextEnabled;
        private bool _isRadioEnabled;
        private bool _isBackEnabled;
        private bool DoesDataExist;

        private List<LookupTable> _vehicleZeroMileage;

        private ICommand _newVehicleCommand;
        private ICommand _newRadioCommand;
        private readonly INovService _novService;
        private readonly ILookupService _lookupService;

        public override string Title => "Vehicle Data";

        public VehicleDataViewModel(bool existsData)
        {
            _vehicleService = DependencyResolver.Get<IVehicleService>();
            _novService = DependencyResolver.Get<INovService>();
            _lookupService = DependencyResolver.Get<ILookupService>();

            NewVehicleCommand = new Command(_ => EndVehicle(), _ => AreButtonsEnabled);
            NewRadioCommand = new Command(_ => NewRadio(), _ => AreButtonsEnabled);

            DoesDataExist = existsData;

        }

        public ICommand NewVehicleCommand
        {
            get => _newVehicleCommand;
            set
            {
                _newVehicleCommand = value;
                EndVehicle();
                NotifyPropertyChanged();
            }
        }

        public ICommand NewRadioCommand
        {
            get => _newRadioCommand;
            set {
                _newRadioCommand = value;
                NewRadio();
                NotifyPropertyChanged();
            }
        }

        public string VehicleNumber
        {
            get => _vehicleNumber;
            set { _vehicleNumber = value; NotifyPropertyChanged(); }
        }

        public string RadioNumber
        {
            get => _radioNumber;
            set { _radioNumber = value; NotifyPropertyChanged(); }
        }

        public int StartingMileage
        {
            get => _startingMileage;
            set { _startingMileage = value; NotifyPropertyChanged(); }
        }

        public int EndingMileage
        {
            get => _endingMileage;
            set { _endingMileage = value; NotifyPropertyChanged(); }
        }

        public bool AreButtonsEnabled
        {
            get => _isButtonsEnabled;
            set { _isButtonsEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsVehicleEnabled
        {
            get => _isVehicleEnabled;
            set { _isVehicleEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsEndingEnabled
        {
            get => _isEndingEnabled;
            set { _isEndingEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsNextEnabled
        {
            get => _isNextEnabled;
            set { _isNextEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsRadioEnabled
        {
            get => _isRadioEnabled;
            set { _isRadioEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsBackEnabled
        {
            get => _isBackEnabled;
            set { _isBackEnabled = value; NotifyPropertyChanged(); }
        }
        


        public bool IsLogOff
        {
            get => _isLogOff;
            set { _isLogOff = value; NotifyPropertyChanged(); }
        }

        public override bool ShowCancelMenu
        {
            get { return false; }
        }
        public override async Task LoadAsync()
        {
            if ((await _vehicleService.isVehicleCurrent(UserSession.UserId)))
            {
                VehicleRadioInfo v = await _vehicleService.GetCurrentVehicleRadioInfo(UserSession.UserId);
                if (!(IsVehicleEnabled && !IsRadioEnabled)) //this is the case where they are entering a new vehicle, so we don't load the old id
                {
                    VehicleNumber = v.VehicleId;
                }
                RadioNumber = v.RadioId;
                StartingMileage = v.StartMileage;
                EndingMileage = v.EndMileage;
            }

            //new session
            if (!DoesDataExist)
            {
                AreButtonsEnabled = false;
                IsEndingEnabled = false;
                IsNextEnabled = true;
                IsRadioEnabled = true;
                IsBackEnabled = true;
                IsVehicleEnabled = true;

            }
            else if (VehicleNumber == null && RadioNumber == null)
            {
                AreButtonsEnabled = false;
                IsEndingEnabled = false;
                IsNextEnabled = true;
                IsRadioEnabled = true;
                IsBackEnabled = true;
                IsVehicleEnabled = true;
            }
            else // not the first time visiting this screen
            {
                AreButtonsEnabled = true;
                IsEndingEnabled = false;
                IsNextEnabled = false;
                IsRadioEnabled = false;
                IsBackEnabled = true;
                IsVehicleEnabled = false;
            }

            //Potentially store in UserSession ?
            if (IsLogOff)
            {
                IsEndingEnabled = true;
                IsNextEnabled = true;
            }

            // Vehicle accept zero mileage
            VehicleZeroMileage = await _lookupService.GetLookupTable("VehicleZeroMileage");
        }

        public List<LookupTable> VehicleZeroMileage
        {
            get => _vehicleZeroMileage;
            set { _vehicleZeroMileage = value; NotifyPropertyChanged(); }
        }

        public void EndVehicle()
        {
            AreButtonsEnabled = false;
            IsEndingEnabled = true;
            IsVehicleEnabled = false;
            IsBackEnabled = true;
            IsNextEnabled = true;
            IsRadioEnabled = false;
        }

        public void NewRadio()
        {
            AreButtonsEnabled = false;
            IsEndingEnabled = false;
            IsVehicleEnabled = false;
            IsBackEnabled = true;
            IsNextEnabled = true;
            IsRadioEnabled = true;
        }


        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (string.IsNullOrWhiteSpace(VehicleNumber))
            {
                alerts.Add(new AlertViewModel("11004", WorkFlowMessages.DSNYMSG_LOG004, elementName: "VehicleNumber"));
            }

            if (string.IsNullOrWhiteSpace(RadioNumber))
            {
                alerts.Add(new AlertViewModel("11005", WorkFlowMessages.DSNYMSG_LOG005, elementName: "RadioNumber"));
            }

            if (!(StartingMileage >= 0))
            {
                alerts.Add(new AlertViewModel("11007", WorkFlowMessages.DSNYMSG_LOG006A, elementName: "StartingMileage"));
            }

            //if (IsEndingEnabled && !(EndingMileage > 0) && (char.ToUpper(VehicleNumber[VehicleNumber.Length-1]) != 'P' && StartingMileage == 0))
            if (IsEndingEnabled && !(EndingMileage > 0) && (!CanVehicleNumberHasZeroMileage() && StartingMileage == 0))
            {
                alerts.Add(new AlertViewModel("11007", WorkFlowMessages.DSNYMSG_LOG007A, elementName: "EndingMileage"));
            }

            if (IsEndingEnabled && (EndingMileage < StartingMileage))
            {
                alerts.Add(new AlertViewModel("11008", WorkFlowMessages.DSNYMSG_LOG008, elementName: "StartingMileage"));
            }

            return Task.FromResult(alerts);
        }

        // NH-1356
        private bool CanVehicleNumberHasZeroMileage() {

            var canZeroMileage = false;
            foreach (var allowVehicleName in VehicleZeroMileage) {

                if (string.Equals(VehicleNumber, allowVehicleName.Description, StringComparison.OrdinalIgnoreCase))
                {
                    canZeroMileage = true;
                    break;
                }
            }
            return (
                char.ToUpper(VehicleNumber[VehicleNumber.Length - 1]) != 'P'
                || canZeroMileage
                );

            //return (
            //    char.ToUpper(VehicleNumber[VehicleNumber.Length - 1]) != 'P'
            //    || string.Equals(VehicleNumber, "Foot", StringComparison.OrdinalIgnoreCase)
            //    || string.Equals(VehicleNumber, "InHouse", StringComparison.OrdinalIgnoreCase)
            //    || string.Equals(VehicleNumber, "Hse", StringComparison.OrdinalIgnoreCase)
            //    );

        }

        public override ICommand NextCommand => new Command(async () =>
        {
            await ExecuteNextCommand();
            //if this is the first time of creation
        });

        public async Task ExecuteNextCommand()
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }

            var list = await _vehicleService.GetAllVehicleRadioInfoByUser(UserSession.UserId);
            if (!IsLogOff)
            {
                if (!IsEndingEnabled)
                {
                    if (IsVehicleEnabled && !IsRadioEnabled) //Checks if it's they add a new vehicle after already having a record 
                    {

                        //short max = await _vehicleService.GetLatestVehicleRadioId();
                        short max = await _vehicleService.GetLatestVehicleRadioIdByUserId(UserSession.UserId);
                        await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                        {
                            UserId = UserSession.UserId,
                            LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                            VehicleRadioId = (short)(max + 1),
                            VehicleId = VehicleNumber,
                            RadioId = RadioNumber,
                            StartMileage = StartingMileage
                        });
                    }else if (!IsVehicleEnabled && IsRadioEnabled)
                    {
                        //short max = await _vehicleService.GetLatestVehicleRadioId();
                        short max = await _vehicleService.GetLatestVehicleRadioIdByUserId(UserSession.UserId);
                        await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                        {
                            UserId = UserSession.UserId,
                            LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                            VehicleRadioId = (short)(max + 1),
                            VehicleId = VehicleNumber,
                            RadioId = RadioNumber,
                            StartMileage = StartingMileage
                        });
                    } else if (IsVehicleEnabled && IsRadioEnabled)   // NH-1429 2018-11-09 scenario initial load screen VehicleData by User
                    {
                        //short max = await _vehicleService.GetLatestVehicleRadioId(VehicleNumber, RadioNumber);
                        //var currentVehicle = await _vehicleService.GetCurrentVehicleRadioInfo(UserSession.UserId);
                        var currentVehicle = await _vehicleService.GetCurrentVehicleRadioInfoByUser(UserSession.UserId);
                        if (currentVehicle.LoginTimestamp.Equals(UserSession.DutyHeader.LoginTimestamp))
                        {
                            await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                            {
                                UserId = UserSession.UserId,
                                LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                                VehicleRadioId = (short)(currentVehicle.VehicleRadioId + 1),
                                VehicleId = VehicleNumber,
                                RadioId = RadioNumber,
                                StartMileage = StartingMileage
                            });
                        }
                        else
                        {  // !currentVehicle.LoginTimestamp.Equals(UserSession.DutyHeader.LoginTimestamp)
                            await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                            {
                                UserId = UserSession.UserId,
                                LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                                VehicleRadioId = 1,
                                VehicleId = VehicleNumber,
                                RadioId = RadioNumber,
                                StartMileage = StartingMileage
                            });

                        }

                    }
                    else // (!IsVehicleEnabled && !IsRadioEnabled && !IsEndingEnabled) 
                    // DO NOTHING ----- waiting user clicking button "New Vehicle" or "New Radio" , four input fields are disable  
                    {
                        // NH-1429 2018-11-09 
                        //DEUBG await _vehicleService.GetAllVehicleRadioInfoByUser(UserSession.UserId);
                        //await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                        //{
                        //    UserId = UserSession.UserId,
                        //    LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                        //    VehicleRadioId = currentVehicle.VehicleRadioId,
                        //    VehicleId = VehicleNumber,
                        //    RadioId = RadioNumber,
                        //    StartMileage = StartingMileage
                        //});
                    }

                    await NavigationService.PopModalAsync();
                }
                else // IsEndingEnabled && !IsLogOff
                {
                    // NH-1429 2018-11-09 
                    var currentVehicle = await _vehicleService.GetCurrentVehicleRadioInfo(UserSession.UserId);
                    currentVehicle.EndMileage = EndingMileage;
                    await _vehicleService.UpdateVehicleRadio(currentVehicle);

                    //short max = await _vehicleService.GetLatestVehicleRadioId(VehicleNumber, RadioNumber);
                    //await _vehicleService.UpdateVehicleRadio(new VehicleRadioInfo
                    //{
                    //    UserId = UserSession.UserId,
                    //    LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                    //    VehicleRadioId = max,
                    //    VehicleId = VehicleNumber,
                    //    RadioId = RadioNumber,
                    //    StartMileage = StartingMileage,
                    //    EndMileage = EndingMileage
                    //});

                    await NavigationService.PopModalAsync();

                    VehicleDataViewModel vm = new VehicleDataViewModel(false) { UserSession = UserSession };
                    vm.RadioNumber = RadioNumber;
                    vm.IsBackEnabled = false;
                    vm.IsRadioEnabled = false;
                    await NavigationService.PushModalAsync(vm);
                }
            }
            else  // IsEndingEnabled && IsLogOff
            {
                var currentVehicle = await _vehicleService.GetCurrentVehicleRadioInfo(UserSession.UserId);
                currentVehicle.EndMileage = EndingMileage;
                UserSession.DutyHeader.LogoutTimestamp = DateTime.Now;
                await _vehicleService.UpdateVehicleRadio(currentVehicle);
                await _novService.SaveDutyHeader(UserSession);
                await NavigationService.PopModalAsync();
                await NavigationService.PopToLoginScreenAsync();
                UserSession = null;
            }

            NavigationInProgress = false;
        }

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public override List<string> ScreenSpecificMenuItems => AreButtonsEnabled ? new List<string> { "New Vehicle", "New Radio" } : null;

        public override void MenuItemTapped(object item)
        {
            if ((string)item == "New Vehicle")
            {
                EndVehicle();
            }
            else if ((string)item == "New Radio")
            {
                NewRadio();
            }
            else
            {
                base.MenuItemTapped(item);
            }
        }
    }
}