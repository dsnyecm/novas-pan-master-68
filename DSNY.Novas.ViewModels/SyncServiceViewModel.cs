﻿using DSNY.Novas.ViewModels.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class SyncServiceViewModel : ViewModelBase
    {
        public ICommand TableFinishedSyncingCommand { get; set; } //Called when table is finished syncing
        public ICommand DuplicateCountUpdatedCommand { get; set; } //called when duplicate count is updated 
        public ICommand ErrorCountUpdatedCommand { get; set; } //called when error count is updated 
        public ICommand RecordCountUpdatedCommand { get; set; } //called when record count is updated 

        public int DuplicateCount { get; private set; }
        public int ErrorCount { get; private set; }
        public int RecordCount { get; private set; }


        public SyncServiceViewModel()
        {
            TableFinishedSyncingCommand = new Command<string>(async _ => await TableFinishedSyncing());
            DuplicateCountUpdatedCommand = new Command<int>(async _ => await DuplicateCountUpdated());
            ErrorCountUpdatedCommand = new Command<int>(async _ => await ErrorCountUpdated());
            RecordCountUpdatedCommand = new Command<int>(async _ => await RecordCountUpdated());
        }

        async Task TableFinishedSyncing()
        {
           // OnPropertyChanged("TableName");
        }

        async Task DuplicateCountUpdated()
        {
            DuplicateCount = 0;
           // OnPropertyChanged("DuplicateCount");
        }

        async Task ErrorCountUpdated()
        {
            ErrorCount = 0;
           // OnPropertyChanged("ErrorCount");
        }

        async Task RecordCountUpdated()
        {
            RecordCount = 0;
           // OnPropertyChanged("RecordCount");
        }
        
    }
}
