﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Services;
using DSNY.Novas.Common;

namespace DSNY.Novas.ViewModels
{
    public class AlternativeServiceAffirmationViewModel : ViewModelBase
    {
        private bool _isNovAffirmed;
        private bool _isAlternativeVisible;
        private bool _isAlternative1Visible;
        private bool _isAlternative2Visible;
        private bool _isAlternative3Visible;
        private bool _isAlternative4Visible;
        private byte[] _signatureImage;

        private readonly IAffidavitService _affidavitService;
        private readonly IPersonBeingServedService _personBeingServedService;
        private readonly INovasUserService _userService;
        private readonly IPersonnelDataService _personnelDataService;

        public AlternativeServiceAffirmationViewModel()
        {
            _affidavitService = DependencyResolver.Get<IAffidavitService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
            _userService = DependencyResolver.Get<INovasUserService>();
            _personnelDataService = DependencyResolver.Get<IPersonnelDataService>();

            IsNovAffirmed = false;
        }

        public override string Title
        {
            get
            {
                if (IsAlternative1Visible)
                {
                    return "Statement of Compl. Service";
                }
                else if (IsAlternative2Visible)
                {
                    return "Statement of Compl. Service";
                }
                else if (IsAlternative3Visible)
                {
                    return "Statement of Compl. Service";
                }
                else if (IsAlternative4Visible && NovMaster.AffidavitOfServiceTranList.Count >= 1)
                {
                    return "2nd Attempt Affirmation";
                }
                else if (IsAlternative4Visible)
                {
                    return "1st Attempt Affirmation";
                }
                return "Alt. Service Affirmation";
            }
        }
        public override async Task LoadAsync()
        {
            // TODO: Should we be loading these from a table?
            if (NovMaster.NovInformation.ViolationCode.Equals("S30")
                || NovMaster.NovInformation.ViolationCode.Equals("S31")
                || NovMaster.NovInformation.ViolationCode.Equals("S32")
                || NovMaster.NovInformation.ViolationCode.Equals("S33")
                || NovMaster.NovInformation.ViolationCode.Equals("GB1")
                || NovMaster.NovInformation.ViolationCode.Equals("GB2")
                || NovMaster.NovInformation.ViolationCode.Equals("SJ1")
                || NovMaster.NovInformation.ViolationCode.Equals("SJ2")
                || NovMaster.NovInformation.ViolationCode.Equals("SJ3")
                || NovMaster.NovInformation.ViolationCode.Equals("SJ4")
                || NovMaster.NovInformation.ViolationCode.Equals("SH9")
                || NovMaster.NovInformation.ViolationCode.Equals("S3T")
                || NovMaster.NovInformation.ViolationCode.Equals("S3P"))
            {
                IsAlternative1Visible = true;
            }
            else if (NovMaster.NovInformation.ViolationCode.Equals("V02")
                || NovMaster.NovInformation.ViolationCode.Equals("V03"))
            {
                IsAlternative2Visible = true;
            }
            else if (NovMaster.NovInformation.ViolationCode.Equals("V01"))
            {
                IsAlternative3Visible = true;
            }
            else if ((NovMaster.NovInformation.IsMultipleAttempts || NovMaster.NovInformation.TicketStatus == "T") && NovMaster.AffidavitOfServiceTranList.Count < 2)
            {
                IsAlternative4Visible = true;
            }
            else
            {
                IsAlternativeVisible = true;
            }

            NotifyPropertyChanged(nameof(Title));
            SignatureImage = (await _userService.GetUser(NovMaster.NovInformation.UserId)).SignatureBitmap;
        }

        public bool IsNovAffirmed
        {
            get => _isNovAffirmed;
            set { _isNovAffirmed = value; NotifyPropertyChanged(); }
        }

        public bool IsAlternativeVisible
        {
            get => _isAlternativeVisible;
            set { _isAlternativeVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsAlternative1Visible
        {
            get => _isAlternative1Visible;
            set { _isAlternative1Visible = value; NotifyPropertyChanged(); }
        }

        public bool IsAlternative2Visible
        {
            get => _isAlternative2Visible;
            set { _isAlternative2Visible = value; NotifyPropertyChanged(); }
        }

        public bool IsAlternative3Visible
        {
            get => _isAlternative3Visible;
            set { _isAlternative3Visible = value; NotifyPropertyChanged(); }
        }

        public bool IsAlternative4Visible
        {
            get => _isAlternative4Visible;
            set { _isAlternative4Visible = value; NotifyPropertyChanged(); }
        }
        public byte[] SignatureImage
        {
            get => _signatureImage;
            set { _signatureImage = value; NotifyPropertyChanged(); }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();
            if (!IsNovAffirmed)
            {
                alerts.Add(new AlertViewModel("14001", WorkFlowMessages.DSNYMSG_26_EmptySignature, elementName: "IsNovAffirmed"));
            }

            return Task.FromResult(alerts);
        }

        public override void WriteFieldValuesToNovMaster()
        {
            // Save all data to the Affidavit Of Service object in memory.
            // Data will be written to the AffidavitOfService or AffidavitOfServiceTran table in the db based on the current ticket status / number of attempts that have been made
            try
            {
               // NovMaster.AffidavitOfService.NovNumber = NovMaster.NovInformation.NovNumber;
                NovMaster.AffidavitOfService.CheckSum = NovMaster.NovInformation.CheckSum;
                NovMaster.AffidavitOfService.PersonallyServeFlag = "A";
                NovMaster.AffidavitOfService.UserId = NovMaster.NovInformation.UserId;
                NovMaster.AffidavitOfService.PublicKeyId = NovMaster.NovInformation.PublicKeyId;
                NovMaster.AffidavitOfService.DeviceId = NovMaster.NovInformation.DeviceId;
                NovMaster.AffidavitOfService.OfficerName = NovMaster.NovInformation.OfficerName;
                NovMaster.AffidavitOfService.AbbrevName = NovMaster.NovInformation.AbbrevName;
                NovMaster.AffidavitOfService.AgencyId = NovMaster.NovInformation.AgencyId;
                NovMaster.AffidavitOfService.Title = NovMaster.NovInformation.Title;
                NovMaster.AffidavitOfService.LoginTimestamp = NovMaster.NovInformation.LoginTimestamp;
                NovMaster.AffidavitOfService.ServedCounty = NovMaster.NovInformation.PrintViolationCode; // ServedCounty is actually used to store the final "print" violation code (i.e. it could be different than the originally selected violation code for repeat violators)
                NovMaster.AffidavitOfService.DigitalSignature = NovMaster.NovInformation.DigitalSignature;
                NovMaster.AffidavitOfService.SignDate = DateTime.Now;

                //if (!NovMaster.NovInformation.IsMultipleAttempts || NovMaster.AffidavitOfServiceTranList.Count >= 2)
                //{
                //   // NovMaster.AffidavitOfService.ExpDate = NovMaster.NovInformation.LicenseExpDate;
                //    NovMaster.AffidavitOfService.ExpDate = new DateTime(1900, 1, 1);
                //}
                //else
                //{
                    //NovMaster.AffidavitOfService.ExpDate = new DateTime(1900, 1, 1);
                    NovMaster.AffidavitOfService.ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
                //}
            }catch(Exception ex)
            {

            }
        }

        public override bool ShowActionMenu => true;
        public override bool ShowCancelMenu => false;
        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }
            WriteFieldValuesToNovMaster();
            if ((!NovMaster.NovInformation.IsMultipleAttempts && NovMaster.NovInformation.TicketStatus != "T") || NovMaster.AffidavitOfServiceTranList.Count >= 2)
            {
                await _affidavitService.SaveAffidavit(NovMaster);
                NovMaster.NovInformation.TicketStatus = " ";
            }
            else
            {
                await _affidavitService.SaveAffidavitTran(NovMaster);
                if (NovMaster.NovInformation.ViolationGroupId.Equals(5))
                {
                    NovMaster.NovInformation.TicketStatus = "T";
                }
                else
                {
                    NovMaster.NovInformation.TicketStatus = "L";
                }
            }

            await SaveTicket(); // Need to persist Ticket Status to the db

            if (IsAlternative4Visible)
            {
                await NavigationService.PopToSummaryScreenAsync();
            }
            else
            {
                await NavigationService.PushAsync(new PrinterViewModel()
                {
                    NovMaster = NovMaster
                });
            }

            NavigationInProgress = false;
        });
    }
}
