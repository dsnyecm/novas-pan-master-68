﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.ViewModels
{
    public class SelectableItem : ViewModelBase
    {
        private object _data;
        private bool _isSelected;

        public object Data
        {
            get => _data;
            set { _data = value; NotifyPropertyChanged(); }
        }

        public bool IsSelected
        {
            get => _isSelected;
            set { _isSelected = value; NotifyPropertyChanged(); }
        }

        public DateTime LastSelectedTimestamp { get; set; }

        public SelectableItem(object data)
        {
            Data = data;
            IsSelected = false;
        }

        public SelectableItem(object data, bool isSelected)
        {
            Data = data;
            IsSelected = isSelected;
        }
    }

    public class SelectableItem<T> : SelectableItem
    {
        public SelectableItem(T data) : base(data)
        {
        }

        public SelectableItem(T data, bool isSelected) : base(data, isSelected)
        {
        }

        public new T Data
        {
            get { return (T)base.Data; }
            set { base.Data = value; }
        }
    }
}
