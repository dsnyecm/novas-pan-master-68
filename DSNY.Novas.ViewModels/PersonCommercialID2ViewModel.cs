﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class PersonCommercialID2ViewModel : ViewModelBase
    {
        #region Fields
        private bool _isIdChecked;
        private List<IdMatrix> _idTypes;
        private IdMatrix _idTypeSelectedItem;
        private string _idTypeFreeform;
        private List<IdMatrix> _issuedByItems;
        private List<IdMatrix> _states;
        private List<IdMatrix> _issued;
        private IdMatrix _issuedBySelectedItem;
        
        private string _businessName;
        private string _idNumber;
        private DateTime _expDate;
        private bool _isBusinessNameVisible;
        private bool _isIdNumberEnabled;

        private bool _isIssuedByEnabled;
        private bool _isExpDateEnabled;
        private bool _isExpDateChecked;
        private bool _isExpDateBoxEnabled;
        private bool _idTypeDropdownIsVisible;

        private string _issuedByFreeform;
        private bool _issuedByDropdownIsVisible;
        //private bool _issuedByFreeformIsVisible;
        private bool _isIssuedByChecked;
        //
        private bool _isBusinessNameEnabled;
        private bool _showPhysicalDescriptionFields;
        private List<LookupTable> _age;
        private LookupTable _ageSelectedItem;
        private List<LookupTable> _skin;
        private LookupTable _skinSelectedItem;
        private List<LookupTable> _weight;
        private LookupTable _weightSelectedItem;
        private string _features;
        private List<LookupTable> _haircolor;
        private LookupTable _haircolorSelectedItem;
        private List<LookupTable> _height;
        private LookupTable _heightSelectedItem;

        private bool _hasCompletedInitialLoad;

        private readonly IPersonCommercialIDService _personCommercialIDService;
        private readonly ILookupService _lookupService;
        private static readonly string ID_NOT_VALID = "NotValid";
        #endregion //Fields

        public override string Title => "Person Being Served ID";
        
        private static readonly IDictionary<string, Func<IdMatrix, bool>> IdFilters = new Dictionary<string, Func<IdMatrix, bool>>
            {
                {"C", id => id.IsPersonalSvc == "Y"}    // Commercial
                ,{"R", id => id.IsPersonalSvc == "Y"}   // Property Residential
                ,{"M", id => id.IsMultiOff == "Y"}      // Property Multiple
                ,{"A", id => id.IsActionOff == "Y"}     // Action And Dcodes
                ,{"O", id => id.IsActionOff == "Y"}     // Action And Dcodes
            };

        public PersonCommercialID2ViewModel()
        {
            _personCommercialIDService = DependencyResolver.Get<IPersonCommercialIDService>();
            _lookupService = DependencyResolver.Get<ILookupService>();

            ShowPhysicalDescriptionFields = true;
            ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
            //IssuedByDropdownIsVisible = false;
        }

        public override async Task LoadAsync()
        {
            if (_hasCompletedInitialLoad)
                return;

            var novInfo = NovMaster.NovInformation;
            var affInfo = NovMaster.AffidavitOfService;

            IsBusinessNameVisible = false; // When entering ID information during the Personal Service flow, it is *always* an individual being served
            IsIdChecked = true;
            IsIssuedByChecked = true;

            var selectedNovNumber = CrossSettings.Current.GetValueOrDefault("MakeSimilar_SelectedViolation", string.Empty);

            var isMakeSimilar = string.IsNullOrEmpty(selectedNovNumber)
                ? "false"
                : CrossSettings.Current.GetValueOrDefault($"IsMakeSimilar_{selectedNovNumber}", string.Empty);

            if (IdTypes == null)
            {
                IdTypes = await _personCommercialIDService.GetIDTypes();

                var violationTypeId = novInfo.ViolationTypeId;

                if (violationTypeId.In(new[] { "A", "O" }))  //For All OTHER than Posting and Action
                {
                    IdTypes = IdFilters.ContainsKey(violationTypeId)
                        ? IdTypes.Where(IdFilters[violationTypeId]).ToList()
                        : IdTypes.Where(IdFilters["A"]).ToList();
                }
                else
                {
                    IdTypes = IsBusinessNameVisible
                        ? (await _personCommercialIDService.GetIDTypes()).Where(_ => _.IsCommOff == "Y").ToList()
                        : (await _personCommercialIDService.GetIDTypes()).Where(_ => _.IsActionOff == "Y").ToList();
                }
            }

            if (IssuedByItems == null)
                IssuedByItems = await _personCommercialIDService.GetIssuedBy();

            Issued = IssuedByItems;

            if (States == null)
                States = await _personCommercialIDService.GetIssuedByStates();

            if (Ages == null)
                Ages = await _lookupService.GetLookupTable("AGE");

            if (Skins == null)
                Skins = await _lookupService.GetLookupTable("SKIN");

            if (Weights == null)
                Weights = await _lookupService.GetLookupTable("WEIGHT");

            if (HairColors == null)
                HairColors = await _lookupService.GetLookupTable("HAIR");

            if (Heights == null)
                Heights = await _lookupService.GetLookupTable("HEIGHT");


            if (novInfo.LicenseNumber != null)
                IDNumber = novInfo.LicenseNumber;

            if (novInfo.ViolationGroupId.In(new[] { 2, 3 })) {
                if (affInfo?.ExpDate != null)
                    ExpDate = affInfo.ExpDate;
                else
                    ExpDate = default(DateTime);

            } else {
                if (novInfo.LicenseExpDate != null)
                    ExpDate = novInfo.LicenseExpDate;
            }
            if (!affInfo.IsPersonBeingServedCleared && !affInfo.ShouldLoadFromAffidavitPersonID && novInfo.ViolationTypeId.In(new[] {"A", "O"}))
            {
                if (novInfo.LicenseType == "--" && !novInfo.IsIdChecked)
                {
                    IdTypeFreeform = novInfo.LicenseTypeDesc;
                    IdTypeSelectedItem = null;
                    IsIdChecked = false;
                }
                else
                {
                    string defaultLicenseType = getDefaultLicenseType();
                    if (affInfo == null || string.IsNullOrEmpty(affInfo.TypeOfLicense))
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc == defaultLicenseType);
                    else
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdType == affInfo.TypeOfLicense);
                }

                    if (novInfo.LicenseAgency != null && novInfo.LicenseAgency != "0")
                {
                    _issuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == novInfo.LicenseAgency);
                    NotifyPropertyChanged(nameof(IssuedBySelectedItem));
                    if (IssuedBySelectedItem != null)
                    {
                        IsIssuedByChecked = true;
                        IssuedByDropdownIsVisible = true;
                    }
                    else
                    {
                        IssuedByFreeform = novInfo.LicenseAgency;
                        IsIssuedByChecked = false;
                        IssuedByDropdownIsVisible = false;
                    }
                }
            }
            else if (affInfo.ShouldLoadFromAffidavitPersonID || isMakeSimilar == "true")
            {
                if (affInfo.TypeOfLicense == "--" && !affInfo.IsIdChecked)
                {
                    IdTypeFreeform = affInfo.LicenseTypeDesc;
                    IdTypeSelectedItem = null;
                    IsIdChecked = false;
                }
                else
                {
                    string defaultLicenseType = getDefaultLicenseType();
                    if (affInfo == null || string.IsNullOrEmpty(affInfo.TypeOfLicense))
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc == defaultLicenseType);
                    else
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdType == affInfo.TypeOfLicense);
                }

                if (affInfo.IssuedBy != null && affInfo.IssuedBy != "0")
                {
                    _issuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == affInfo.IssuedBy);
                    NotifyPropertyChanged(nameof(IssuedBySelectedItem));
                    if (IssuedBySelectedItem != null)
                    {
                        IsIssuedByChecked = true;
                        IssuedByDropdownIsVisible = true;
                    }
                    else
                    {
                        IssuedByFreeform = affInfo.IssuedBy;
                        IsIssuedByChecked = false;
                        IssuedByDropdownIsVisible = false;
                    }
                }

                if (!string.IsNullOrWhiteSpace(affInfo?.LicenseNumber))
                    IDNumber = affInfo.LicenseNumber;

                if (affInfo?.ExpDate != null)
                    ExpDate = affInfo.ExpDate;

                if (affInfo?.Age != null)
                    AgeSelectedItem = Ages.FirstOrDefault(_ => _.Code.Equals(affInfo.Age));

                if (affInfo?.HairColor != null)
                    HairColorSelectedItem = HairColors.FirstOrDefault(_ => _.Code.Equals(affInfo.HairColor));

                if (affInfo?.SkinColor != null)
                    SkinSelectedItem = Skins.FirstOrDefault(_ => _.Code.Equals(affInfo.SkinColor));

                if (affInfo?.Height != null)
                    HeightSelectedItem = Heights.FirstOrDefault(_ => _.Code.Equals(affInfo.Height));

                if (affInfo?.Weight != null)
                    WeightSelectedItem = Weights.FirstOrDefault(_ => _.Code.Equals(affInfo.Weight));

                if (affInfo?.OtherIdentifying != null)
                    OtherFeatures = affInfo.OtherIdentifying;

                var novInfoNovNumber = novInfo.NovNumber;
                var typeOfLicense = CrossSettings.Current.GetValueOrDefault($"TypeOfLicense_{novInfoNovNumber}", string.Empty);
                var licenseTypeDesc = CrossSettings.Current.GetValueOrDefault($"LicenseTypeDescCom_{novInfoNovNumber}", string.Empty);
                var issuedBy = CrossSettings.Current.GetValueOrDefault($"IssuedByCom_{novInfoNovNumber}", string.Empty);

                var isIdChecked = CrossSettings.Current.GetValueOrDefault($"IsIdChecked_{novInfoNovNumber}", string.Empty);
                var isIssuedByChecked = CrossSettings.Current.GetValueOrDefault($"IsIssuedByChecked_{novInfoNovNumber}", string.Empty);
                var expDate = CrossSettings.Current.GetValueOrDefault($"ExpDate_{novInfoNovNumber}", string.Empty);
                // Date: 10/26/2018 (NH-1333)Not requied to read from CrossSettings
                // var idNumber = CrossSettings.Current.GetValueOrDefault($"IDNumber_{novInfoNovNumber}", string.Empty);

                if (affInfo == null)
                {
                    IdTypeSelectedItem.IdType = !string.IsNullOrEmpty(typeOfLicense) ? typeOfLicense : "--";
                    IdTypeSelectedItem.IdDesc = !string.IsNullOrEmpty(licenseTypeDesc) ? licenseTypeDesc : "--";

                    if (IdTypeSelectedItem != null)
                        IdTypeSelectedItem.IssuedBy = !string.IsNullOrEmpty(issuedBy) ? issuedBy : "--";

                    IsIdChecked = !string.IsNullOrEmpty(isIdChecked) && Convert.ToBoolean(isIdChecked);
                    IsIssuedByChecked = !string.IsNullOrEmpty(isIssuedByChecked) && Convert.ToBoolean(isIssuedByChecked);
                    ExpDate = !string.IsNullOrEmpty(expDate) ? Convert.ToDateTime(expDate) : DateTime.Now;
                    // Date: 10/26/2018 (NH-1333)Not requied to read from CrossSettings
                    //IDNumber = !string.IsNullOrEmpty(idNumber) ? idNumber : "";
                }
                else {
                    if (string.IsNullOrEmpty(typeOfLicense))
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdType == "--");
                    else
                    {
                        IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdType == typeOfLicense);
                        if (typeOfLicense == "OTHER") {
                            IdTypeFreeform = licenseTypeDesc;
                        }
                    }
                    IsIssuedByChecked = !string.IsNullOrEmpty(isIssuedByChecked) && Convert.ToBoolean(isIssuedByChecked);
                    IssuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == issuedBy);
                }
            }
            else
            {
                IdTypeSelectedItem = IdTypes.FirstOrDefault();
            }
            
            _hasCompletedInitialLoad = true;
        }
        private string getDefaultLicenseType()
        {
            if (ConcreteWorkflowConstants.DefaultLicenseTypePersonCommercialIDViewModel != null
                && !String.IsNullOrEmpty(NovMaster.ViolationGroup.TypeName))
            {

                string defaultLicenseTypeValue;
                if (ConcreteWorkflowConstants.DefaultLicenseTypePersonCommercialIDViewModel.TryGetValue(NovMaster.ViolationGroup.TypeName, out defaultLicenseTypeValue))
                {
                    return defaultLicenseTypeValue;
                }

            }
            return null;
        }
        public bool IsIdChecked
        {
            get => _isIdChecked;
            set
            {
                _isIdChecked = value;

                if (_isIdChecked)
                {
                   // IsIssuedByEnabled = false;
                    // _issuedBySelectedItem = null;//race condition here (fixed after changing it to the var)
                    IssuedBySelectedItem = null;
                    IsExpDateEnabled = false;
                    IsExpDateChecked = false;
                    IsExpDateBoxEnabled = false;
                    IdTypeDropdownIsVisible = true;
                    IdTypeFreeform = "";
                }
                else
                {
                    IdTypeDropdownIsVisible = false;
                    IdTypeSelectedItem = null;
                    IsIssuedByEnabled = true;
                    IsExpDateEnabled = true;
                    IsExpDateBoxEnabled = true;
                    IsExpDateChecked = true;

                    IsExpDateChecked = NovMaster.AffidavitOfService.ShouldLoadFromAffidavitPersonID
                        ? NovMaster.AffidavitOfService?.ExpDate != new DateTime(1900, 1, 1)
                        : NovMaster.NovInformation?.LicenseExpDate != new DateTime(1900, 1, 1);
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsIssuedByChecked
        {
            get => _isIssuedByChecked;
            set
            {
                _isIssuedByChecked = value;
                if (_isIssuedByChecked)
                {
                    IssuedByDropdownIsVisible = true;
                    IssuedByFreeform = "";
                }
                else
                {
                    IssuedByDropdownIsVisible = false;
                    IssuedBySelectedItem = null;
                }
                NotifyPropertyChanged();
            }
        }

        public List<IdMatrix> IdTypes
        {
            get => _idTypes;
            set {
                // NH-1414 2018-11-07
                if (!IsBusinessNameVisible && value != null)
                {
                    List<IdMatrix> ids = value;
                    int index = ids.FindIndex(_ => _.IdType.Equals(ID_NOT_VALID));
                    if (index >= 0)
                        ids.RemoveAt(index);
                    _idTypes = ids;
                }
                else
                    _idTypes = value;
                NotifyPropertyChanged();
            }
        }

        public IdMatrix IdTypeSelectedItem
        {
            get => _idTypeSelectedItem;
            set
            {
                IssuedByDropdownIsVisible = true;
                _idTypeSelectedItem = value;

                if (_idTypeSelectedItem != null)
                {
                    IsIssuedByEnabled = _idTypeSelectedItem.AskIssuedBy.Equals("Y");

                    if (!string.IsNullOrEmpty(_idTypeSelectedItem.IssuedBy))
                    {
                        IssuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == _idTypeSelectedItem.IssuedBy);
                        // IsIssuedByEnabled = false;
                    }

                    if (_idTypeSelectedItem.AskExpDate.Equals("O"))
                    {
                        IsExpDateEnabled = true;
                        IsExpDateBoxEnabled = true;
                        IsExpDateChecked = true;
                    }
                    else
                    {
                        IsExpDateEnabled = false;
                        IsExpDateChecked = false;
                        IsExpDateBoxEnabled = false;
                        IsExpDateChecked = NovMaster.AffidavitOfService.ShouldLoadFromAffidavitPersonID
                            ? NovMaster.AffidavitOfService?.ExpDate != new DateTime(1900, 1, 1)
                            : NovMaster.NovInformation?.LicenseExpDate != new DateTime(1900, 1, 1);
                    }
                }

                if (_idTypeSelectedItem != null)
                {

                    if (_idTypeSelectedItem.IdType == "LP" || _idTypeSelectedItem.IdType == "CDL")
                    {
                        IssuedByItems = States;
                        IsIdNumberEnabled = true;
                        if (IDNumber == "--" || IDNumber == null)
                        {
                            IDNumber = "";
                        }
                    }
                    else
                    {
                        IssuedByItems = Issued;
                        if (_idTypeSelectedItem.IdDesc == "--")
                        {
                            IsIdNumberEnabled = false;
                            IDNumber = "--";
                        }
                        else
                        {
                            IsIdNumberEnabled = true;

                            if (_idTypeSelectedItem.IdDesc != "--")
                            {
                                if (IDNumber == "--" || IDNumber == null)
                                {
                                    IDNumber = "";
                                }
                            }
                        }

                    }                 
                }

                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(IsOtherIdTypeSectionVisible));
                NotifyPropertyChanged(nameof(IsIssuedByEnabled));
                NotifyPropertyChanged(nameof(IsIdNumberEnabled));
            }
        }

        public bool IsOtherIdTypeSectionVisible => IdTypeSelectedItem != null && IdTypeSelectedItem.IdDesc.ToLower().Equals("other");

        public string IdTypeFreeform
        {
            get => _idTypeFreeform;
            set { _idTypeFreeform = value; NotifyPropertyChanged(); }
        }

        

        public List<IdMatrix> IssuedByItems
        {
            get => _issuedByItems;
            set { _issuedByItems = value;

                if (IssuedBySelectedItem != null)
                {
                    if (_issuedBySelectedItem.IssuedBy.Trim() == "")
                    {
                        IssuedByDropdownIsVisible = false;
                    }
                    else
                    {
                        IssuedByDropdownIsVisible = true;
                    }
                }

                NotifyPropertyChanged(); 
            }
        }
        
        public bool IsBusinessNameVisible
        {
            get => _isBusinessNameVisible;
            set { _isBusinessNameVisible = value; NotifyPropertyChanged(); }
        }
        public bool IsIdNumberEnabled
        {
            get => _isIdNumberEnabled;
            set {
                // NH-1414 2018-11-07
                if (_idTypeSelectedItem != null && (_idTypeSelectedItem?.IdType == "--"))
                {
                    _isIdNumberEnabled = true;
                }
                else if (_idTypeSelectedItem != null && _idTypeSelectedItem?.IdType == ID_NOT_VALID)
                {
                    _isIdNumberEnabled = false;
                }
                else
                    _isIdNumberEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public List<IdMatrix> States
        {
            get => _states;
            set { _states = value; NotifyPropertyChanged(); }
        }

        public List<IdMatrix> Issued
        {
            get => _issued;
            set { _issued = value; NotifyPropertyChanged(); }
        }


        public bool IsExpDateEnabled
        {
            get => _isExpDateEnabled;
            set
            {
                _isExpDateEnabled = value;

                if (_isExpDateEnabled)
                {
                    if (NovMaster.NovInformation.ViolationGroupId.In(new[] { 2, 3 })) {
                        if (NovMaster.AffidavitOfService.ExpDate != default(DateTime) && NovMaster.AffidavitOfService.ExpDate != new DateTime(1900, 1, 1))
                            ExpDate = NovMaster.AffidavitOfService.ExpDate;
                        else
                            ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
                    } else
                    if (!NovMaster.AffidavitOfService.ShouldLoadFromAffidavitPersonID)
                    {
                        if (NovMaster.NovInformation.LicenseExpDate != default(DateTime) && NovMaster.NovInformation.LicenseExpDate != new DateTime(1900, 1, 1))
                        {
                            ExpDate = NovMaster.NovInformation.LicenseExpDate;
                        }
                        else
                        {
                            ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
                        }
                    }
                    else
                    {
                        if (NovMaster.AffidavitOfService.ExpDate != default(DateTime) && NovMaster.AffidavitOfService.ExpDate != new DateTime(1900, 1, 1))
                        {
                            ExpDate = NovMaster.AffidavitOfService.ExpDate;
                        }
                        else
                        {
                            ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
                        }
                    }
                }
                else
                {
                    ExpDate = new DateTime(1900, 1, 1);
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsExpDateBoxEnabled
        {
            get => _isExpDateBoxEnabled;
            set { _isExpDateBoxEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsExpDateChecked
        {
            get => _isExpDateChecked;
            set
            {
                _isExpDateChecked = value;
                IsExpDateEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsIssuedByEnabled
        {
            get => _isIssuedByEnabled;
            set {
                // NH-1414 2018-11-07
                if (_idTypeSelectedItem != null && (_idTypeSelectedItem?.IdType == ID_NOT_VALID || _idTypeSelectedItem?.IdType == "--"))
                {
                    _isIssuedByEnabled = false;
                }
                else
                    _isIssuedByEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool IdTypeDropdownIsVisible
        {
            get => _idTypeDropdownIsVisible;
            set { _idTypeDropdownIsVisible = value; NotifyPropertyChanged(); }
        }
        
        public IdMatrix IssuedBySelectedItem
        {
            get => _issuedBySelectedItem;
            set
            {
                _issuedBySelectedItem = value;
                IsIdNumberEnabled = true;

                IDNumber = NovMaster.AffidavitOfService.LicenseNumber;
                NotifyPropertyChanged();
            }
        }

        public string IssuedByFreeform
        {
            get => _issuedByFreeform;
            set { _issuedByFreeform = value; NotifyPropertyChanged(); }
        }

        public bool IssuedByDropdownIsVisible
        {
            get => _issuedByDropdownIsVisible;
            set { _issuedByDropdownIsVisible = value; NotifyPropertyChanged(); }
        }

        public DateTime ExpDate
        {
            get => _expDate;
            set { _expDate = value; NotifyPropertyChanged(); }
        }

        public string BusinessName
        {
            get => _businessName;
            set { _businessName = value; NotifyPropertyChanged(); }
        }
   
        public bool IsBusinessNameEnabled
        {
            get => _isBusinessNameEnabled;
            set { _isBusinessNameEnabled = value; NotifyPropertyChanged(); }
        }

        public string IDNumber
        {
            get => _idNumber;
            set { _idNumber = value; NotifyPropertyChanged(); }
        }

        public bool ShowPhysicalDescriptionFields
        {
            get => _showPhysicalDescriptionFields;
            set { _showPhysicalDescriptionFields = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Ages
        {
            get => _age;
            set { _age = value; NotifyPropertyChanged(); }
        }

        public LookupTable AgeSelectedItem
        {
            get => _ageSelectedItem;
            set { _ageSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Skins
        {
            get => _skin;
            set { _skin = value; NotifyPropertyChanged(); }
        }

        public LookupTable SkinSelectedItem
        {
            get => _skinSelectedItem;
            set { _skinSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Weights
        {
            get => _weight;
            set { _weight = value; NotifyPropertyChanged(); }
        }

        public LookupTable WeightSelectedItem
        {
            get => _weightSelectedItem;
            set { _weightSelectedItem = value; NotifyPropertyChanged(); }
        }
        
        public string OtherFeatures
        {
            get => _features;
            set { _features = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> HairColors
        {
            get => _haircolor;
            set { _haircolor = value; NotifyPropertyChanged(); }
        }

        public LookupTable HairColorSelectedItem
        {
            get => _haircolorSelectedItem;
            set { _haircolorSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Heights
        {
            get => _height;
            set { _height = value; NotifyPropertyChanged(); }
        }

        public LookupTable HeightSelectedItem
        {
            get => _heightSelectedItem;
            set { _heightSelectedItem = value; NotifyPropertyChanged(); }
        }

        public override bool ShowActionMenu => true;

        public override bool ShowCancelMenu => false;
        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (string.IsNullOrEmpty(IdTypeFreeform) && IdTypeSelectedItem == null)
            {
                alerts.Add(new AlertViewModel("12008", WorkFlowMessages.DSNYMSG_NOV008, elementName: "IdType"));
            }

            if (IssuedBySelectedItem == null && string.IsNullOrEmpty(IssuedByFreeform) && IsIssuedByEnabled)
            {
                alerts.Add(new AlertViewModel("12009", WorkFlowMessages.DSNYMSG_NOV009, elementName: "IssuedBy"));
            }
            if (!String.IsNullOrEmpty(IDNumber) && IsIdNumberEnabled)
            { 
                if (IsExpDateEnabled && ExpDate.Date < DateTime.Today)
                {
                    alerts.Add(new AlertViewModel("1081", WorkFlowMessages.DSNYMSG_NOV014, okTitle: "Yes", cancelTitle: "No", shouldContinueOnOk: true, elementName: "ExpDate"));
                }
            }
            if (string.IsNullOrEmpty(IDNumber) && IsIdNumberEnabled)
            {
                alerts.Add(new AlertViewModel("12010", WorkFlowMessages.DSNYMSG_NOV010, elementName: "IDNumber"));
            }

            if (IsIdChecked)
            {
                if (IdTypeSelectedItem?.IdType != null)
                {
                    if (IdTypeSelectedItem.IdType == "NotValid")
                    {
                        alerts.Add(new AlertViewModel("12062", WorkFlowMessages.DSNYMSG_NOV062, okTitle: "Yes", cancelTitle: "No", shouldContinueOnOk: true, elementName: "IdTypeSelected"));
                    }
                }

                if (IsBusinessNameVisible && string.IsNullOrEmpty(BusinessName))
                {
                    alerts.Add(new AlertViewModel("12011", WorkFlowMessages.DSNYMSG_NOV011, elementName: "BusinessName"));
                }
            }

            if (ShowPhysicalDescriptionFields)
            {
                if (AgeSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12056", WorkFlowMessages.DSNYMSG_NOV056, elementName: "Age"));
                }

                if (SkinSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12057", WorkFlowMessages.DSNYMSG_NOV057, elementName: "Skin"));
                }

                if (WeightSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12058", WorkFlowMessages.DSNYMSG_NOV058, elementName: "Weight"));
                }

                if (HairColorSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12059", WorkFlowMessages.DSNYMSG_NOV059, elementName: "Hair"));
                }

                if (HeightSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12061", WorkFlowMessages.DSNYMSG_NOV061, elementName: "Height"));
                }
            }

            return Task.FromResult(alerts);
        }
        private bool isIdTypeOther() {
            if (IdTypeSelectedItem== null || string.IsNullOrEmpty(IdTypeSelectedItem.IdType))
                return false;
            if (IdTypeSelectedItem.IdType == "OTHER")
                return true;
            return false;
        }

        public override ICommand BackCommand => new Command(async () =>
        {
            var novInfoNovNumber = NovMaster.NovInformation.NovNumber;

            CrossSettings.Current.AddOrUpdateValue($"TypeOfLicense_{novInfoNovNumber}", IsIdChecked? IdTypeSelectedItem?.IdType : "--");
            if (isIdTypeOther())
            {
                CrossSettings.Current.AddOrUpdateValue($"LicenseTypeDescCom_{novInfoNovNumber}", IdTypeFreeform);
            }
            else {
                CrossSettings.Current.AddOrUpdateValue($"LicenseTypeDescCom_{novInfoNovNumber}", IdTypeSelectedItem?.IdDesc );
            }
            //CrossSettings.Current.AddOrUpdateValue($"LicenseTypeDescCom_{novInfoNovNumber}", IsIdChecked?  IdTypeSelectedItem?.IdDesc : IdTypeFreeform);
            CrossSettings.Current.AddOrUpdateValue($"IssuedByCom_{novInfoNovNumber}", IsIssuedByChecked?  IssuedBySelectedItem?.IssuedBy : IssuedByFreeform);

            CrossSettings.Current.AddOrUpdateValue($"IsIdChecked_{novInfoNovNumber}", IsIdChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"IsIssuedByChecked_{novInfoNovNumber}", IsIssuedByChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue($"ExpDate_{novInfoNovNumber}", ExpDate.ToString());
            CrossSettings.Current.AddOrUpdateValue($"IDNumber_{novInfoNovNumber}", IDNumber);
           
            
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            WriteFieldValuesToNovMaster();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
        });


        public override void WriteFieldValuesToNovMaster()
        {
            if (IsIdChecked)
            {
                NovMaster.AffidavitOfService.TypeOfLicense = IdTypeSelectedItem?.IdType;
                NovMaster.AffidavitOfService.LicenseTypeDesc =
                    IdTypeSelectedItem != null && IdTypeSelectedItem.IdDesc.ToLower().Equals("other")
                        ? IdTypeFreeform
                        : IdTypeSelectedItem?.IdDesc;
            }
            else
            {
                NovMaster.AffidavitOfService.TypeOfLicense = "--";
                NovMaster.AffidavitOfService.LicenseTypeDesc = IdTypeFreeform;
            }

            if (IsIssuedByChecked)
            {
                NovMaster.AffidavitOfService.IssuedBy = IssuedBySelectedItem?.IssuedBy;
                //NovMaster.NovInformation.LicenseTypeDesc = IdTypeSelectedItem?.IdDesc;
            }
            else
            {
                //NovMaster.NovInformation.LicenseAgency = "--";
                NovMaster.AffidavitOfService.IssuedBy = IssuedByFreeform;
            }

            NovMaster.AffidavitOfService.IsIdChecked = IsIdChecked;
            NovMaster.AffidavitOfService.IsIssuedByChecked = IsIssuedByChecked;
            NovMaster.AffidavitOfService.ExpDate = ExpDate;
            //NovMaster.AffidavitOfService.IssuedBy = IssuedBySelectedItem?.IssuedBy;
            NovMaster.AffidavitOfService.LicenseNumber = IDNumber;

            NovMaster.AffidavitOfService.Age = AgeSelectedItem?.Code;
            NovMaster.AffidavitOfService.SkinColor = SkinSelectedItem?.Code;
            NovMaster.AffidavitOfService.Weight = WeightSelectedItem?.Code;
            NovMaster.AffidavitOfService.Height = HeightSelectedItem?.Code;
            NovMaster.AffidavitOfService.HairColor = HairColorSelectedItem?.Code;

            if (!string.IsNullOrWhiteSpace(OtherFeatures))
            {
                NovMaster.AffidavitOfService.OtherIdentifying = OtherFeatures;
            }

            NovMaster.AffidavitOfService.ShouldLoadFromAffidavitPersonID = true;

            if (IdTypeSelectedItem?.IdType == "NYCIDC")
                NovMaster.AffidavitOfService.IssuedBy = " ";
        }

    }
}
