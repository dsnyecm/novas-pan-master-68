﻿using System;
using DSNY.Novas.Models;
using System.Collections.Generic;

namespace DSNY.Novas.ViewModels
{
    public interface IWorkflowFactory
    {
        ViewModelBase NextViewModel(string viewModel, NovMaster novMaster);
    }

    public abstract class WorkflowFactory
    {
        public abstract IWorkflowFactory GetViewModel(string violationType);
    }

    public class ConcreteWorkflowFactory : WorkflowFactory
    {      
        public override IWorkflowFactory GetViewModel(string violationType)
        {
            switch (violationType)
            {
                case "Action":
                    return new ActionWorkflow();
                case "Property":
                    return new PropertyWorkflow();
                case "Commercial":
                    return new CommercialNovWorkflow();
                case "Vacantlot":
                    return new VacantNovWorkflow();
                case "Posting":
                    return new PostingNovWorkflow();
                case "Dcodes/Const":
                    return new DcodesNovWorkflow();
                case "Tobacco":
                    return new TobaccoNovWorkflow();
            }

            return null;
        }
    }

    public class ActionWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PersonBeingServedViewModel { NovMaster = novMaster };
                case "PersonBeingServedViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        // TODO: Move these violation codes into a table? (Misc. table?)  Evidentally these values change from time to time.
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                        || novMaster.NovInformation.ViolationCode.Equals("S31")
                        || novMaster.NovInformation.ViolationCode.Equals("S32")
                        || novMaster.NovInformation.ViolationCode.Equals("S33")
                        || novMaster.NovInformation.ViolationCode.Equals("GB1")
                        || novMaster.NovInformation.ViolationCode.Equals("GB2")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                        || novMaster.NovInformation.ViolationCode.Equals("SH9")
                        || novMaster.NovInformation.ViolationCode.Equals("S3T")
                        || novMaster.NovInformation.ViolationCode.Equals("S3P")
                        || novMaster.NovInformation.ViolationCode.Equals("V01"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class PropertyWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new ResidentialOwnerViewModel { NovMaster = novMaster };
                case "ResidentialOwnerViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    if (novMaster.NovInformation.TicketStatus != "V")
                    {
                        return new ServiceTypeViewModel { NovMaster = novMaster };
                    }
                    else
                    {
                        return new NovSummaryViewModel { NovMaster = novMaster };
                    }
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                            || novMaster.NovInformation.ViolationCode.Equals("S31")
                            || novMaster.NovInformation.ViolationCode.Equals("S32")
                            || novMaster.NovInformation.ViolationCode.Equals("S33")
                            || novMaster.NovInformation.ViolationCode.Equals("GB1")
                            || novMaster.NovInformation.ViolationCode.Equals("GB2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                            || novMaster.NovInformation.ViolationCode.Equals("SH9")
                            || novMaster.NovInformation.ViolationCode.Equals("S3T")
                            || novMaster.NovInformation.ViolationCode.Equals("S3P"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class CommercialNovWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                            || novMaster.NovInformation.ViolationCode.Equals("S31")
                            || novMaster.NovInformation.ViolationCode.Equals("S32")
                            || novMaster.NovInformation.ViolationCode.Equals("S33")
                            || novMaster.NovInformation.ViolationCode.Equals("GB1")
                            || novMaster.NovInformation.ViolationCode.Equals("GB2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                            || novMaster.NovInformation.ViolationCode.Equals("SH9")
                            || novMaster.NovInformation.ViolationCode.Equals("S3T")
                            || novMaster.NovInformation.ViolationCode.Equals("S3P"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class VacantNovWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PersonBeingServedViewModel { NovMaster = novMaster };
                case "PersonBeingServedViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                            || novMaster.NovInformation.ViolationCode.Equals("S31")
                            || novMaster.NovInformation.ViolationCode.Equals("S32")
                            || novMaster.NovInformation.ViolationCode.Equals("S33")
                            || novMaster.NovInformation.ViolationCode.Equals("GB1")
                            || novMaster.NovInformation.ViolationCode.Equals("GB2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                            || novMaster.NovInformation.ViolationCode.Equals("SH9")
                            || novMaster.NovInformation.ViolationCode.Equals("S3T")
                            || novMaster.NovInformation.ViolationCode.Equals("S3P"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class PostingNovWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PersonBeingServedViewModel { NovMaster = novMaster };
                case "PersonBeingServedViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P") 
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        if (
                            (new List<string> { "S30", "S31", "S32", "S33", "GB1", "GB2", "SJ1", "SJ2", "SJ3", "SJ4", "SH9", "S3T", "S3P", "V02", "V03" }).Contains(novMaster.NovInformation.ViolationCode) ||
                            (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        )
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }

                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class DcodesNovWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PersonBeingServedViewModel { NovMaster = novMaster };
                case "PersonBeingServedViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                            || novMaster.NovInformation.ViolationCode.Equals("S31")
                            || novMaster.NovInformation.ViolationCode.Equals("S32")
                            || novMaster.NovInformation.ViolationCode.Equals("S33")
                            || novMaster.NovInformation.ViolationCode.Equals("GB1")
                            || novMaster.NovInformation.ViolationCode.Equals("GB2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                            || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                            || novMaster.NovInformation.ViolationCode.Equals("SH9")
                            || novMaster.NovInformation.ViolationCode.Equals("S3T")
                            || novMaster.NovInformation.ViolationCode.Equals("S3P"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

    public class TobaccoNovWorkflow : IWorkflowFactory
    {
        public ViewModelBase NextViewModel(string viewModel, NovMaster novMaster)
        {
            switch (viewModel)
            {
                case "GroupTypeViewModel":
                    return new PersonBeingServedViewModel { NovMaster = novMaster };
                case "PersonBeingServedViewModel":
                    return new PersonCommercialIDViewModel { NovMaster = novMaster };
                case "PersonCommercialIDViewModel":
                    return new PlaceOfOccurrenceViewModel { NovMaster = novMaster };
                case "PlaceOfOccurrenceViewModel":
                    return new ViolationDetailsViewModel { NovMaster = novMaster };
                case "ViolationDetailsViewModel":
                    return new AffirmationSignatureViewModel { NovMaster = novMaster };
                case "AffirmationSignatureViewModel":
                    return new ServiceTypeViewModel { NovMaster = novMaster };
                case "ServiceTypeViewModel":
                    //we'll need to check what the input from previous screen
                    if (novMaster.NovInformation.AlternateService == "P")
                    {
                        return new PersonalServiceViewModel { NovMaster = novMaster };
                    }
                    else if (novMaster.NovInformation.AlternateService == "A")
                    {
                        // TODO: Move these violation codes into a table? (Misc. table?)  Evidentally these values change from time to time.
                        if (novMaster.NovInformation.ViolationCode.Equals("S30")
                        || novMaster.NovInformation.ViolationCode.Equals("S31")
                        || novMaster.NovInformation.ViolationCode.Equals("S32")
                        || novMaster.NovInformation.ViolationCode.Equals("S33")
                        || novMaster.NovInformation.ViolationCode.Equals("GB1")
                        || novMaster.NovInformation.ViolationCode.Equals("GB2")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ1")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ2")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ3")
                        || novMaster.NovInformation.ViolationCode.Equals("SJ4")
                        || novMaster.NovInformation.ViolationCode.Equals("SH9")
                        || novMaster.NovInformation.ViolationCode.Equals("S3T")
                        || novMaster.NovInformation.ViolationCode.Equals("S3P"))
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        else if (novMaster.NovInformation.IsMultipleAttempts || novMaster.NovInformation.TicketStatus == "T")
                        {
                            return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
                        }
                        return new PremiseDetailsViewModel { NovMaster = novMaster };
                    }
                    break;
                case "PersonalServiceViewModel":
                    return new PersonBeingServed2ViewModel { NovMaster = novMaster };
                case "PersonBeingServed2ViewModel":
                    return new PersonCommercialID2ViewModel { NovMaster = novMaster };
                case "PersonCommercialID2ViewModel":
                    return new PersonalServiceAffirmationSignatureViewModel { NovMaster = novMaster };
                case "PremiseDetailsViewModel":
                    return new AlternativeServiceAffirmationViewModel { NovMaster = novMaster };
            }

            return null;
        }
    }

}
