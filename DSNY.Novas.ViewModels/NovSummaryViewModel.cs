﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;
using System.Linq;
using System.Collections;
using Plugin.Settings;
using static DSNY.Novas.Services.SyncService;
using DSNY.Novas.Entities;
using DSNY.Novas.Data;
using System.Threading;

namespace DSNY.Novas.ViewModels
{
    public class NovSummaryViewModel : ViewModelBase
    {
        private List<NovInformation> _violations;
        private NovInformation _selectedViolation;
        private readonly IVehicleService _vehicleService;
        private readonly IPlaceOfOccurrenceService _placeOfOccurrenceService;
        private readonly IAffidavitService _affidavitService;
        private readonly INovService _novService;
        private readonly IPrinter _printer; 
        private bool _isLaterServiceEnabled;
        private bool _IsMakeSimilarAndRePrintEnabled;
        private bool IsGroupIdAllowMakeSimilar;  // value set at ConcreteWorkflowConstant.IsAllowMakeSimilarNovSummaryViewModel


        private bool _isNewNovSelected;
        private bool _isLaterServiceSelected;
        private bool _isMakeSimilarSelected;
        private bool _isRePrintNovSelected;
        private bool _isPrintDuplicateSelected;
        private bool _isPrintReportSelected;
        private bool _isPrintAllSelected;
        private bool _isLoading;
        private ViewModelBase _nextViewModel;


        public override string Title => "NOVAS Summary";

        public ICommand VehicleRadioPressedCommand => new Command(async () =>
        {
            bool existsData = await _vehicleService.isVehicleCurrent(UserSession.UserId);
            VehicleDataViewModel vm = new VehicleDataViewModel(existsData) { UserSession = UserSession };
            await NavigationService.PushModalAsync(vm);
        });

        public ICommand SyncPressedCommand => new Command(async () =>
        {
            var syncStatusViewModel = new SyncStatusViewModel() { UserSession = UserSession, _isTimeOutWithLogin=true };
            await NavigationService.PushModalAsync(syncStatusViewModel);
        });

        public ICommand PrintReportPressedCommand => new Command(async () =>
        {
            var novMaster = new NovMaster
            {
                UserSession = UserSession
            };
            await NavigationService.PushAsync(new PrinterViewModel() { NovMaster = novMaster });
        });

        public ICommand PrintAllPressedCommand => new Command(async () =>
        {
            var _violations = await _novService.GetViolations(UserSession?.UserId);

            List<NovMaster> novMasterList = new List<NovMaster>();
            foreach (var violation in _violations)
            {
                var novMaster = await BuildNovMaster(violation);
                var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);

                violation.SummaryOfLaw = summaryOfLaw.LawDescription;
                violation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;
                novMasterList.Add(novMaster);
            }

            await NavigationService.PushAsync(new PrinterViewModel() { NovMasterList = novMasterList });
        });

        public ICommand LongPressCommand => new Command((object item) =>
        {
            SelectedViolation = item as NovInformation;
            NotifyPropertyChanged(nameof(LongPressMenuItemSource));
        });

        public ICommand LongPressMenuItemTappedCommand => new Command(async (object item) =>
        {
            if ((string) item == "Later Service")
            {
                IsLaterServiceSelected = true;
                CrossSettings.Current.AddOrUpdateValue("MakeSimilar_SelectedViolation", "");
                NextCommand.Execute(null);
                
            }
            else if ((string) item == "Make Similar")
            {
                IsMakeSimilarSelected = true;

                var novMaster = await BuildNovMaster(SelectedViolation);
                var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);

                novMaster.NovInformation.SummaryOfLaw = summaryOfLaw.LawDescription;
                novMaster.NovInformation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;
                
                if (SelectedViolation.TicketStatus == " ")
                {
                    CrossSettings.Current.AddOrUpdateValue("MakeSimilar_SelectedViolation", SelectedViolation.NovNumber.ToString());
                    CrossSettings.Current.AddOrUpdateValue("IsMakeSimilar_" + SelectedViolation.NovNumber.ToString(), "true");
                }
                NextCommand.Execute(null);
            }
            else if ((string) item == "Re-Print NOV")
            {
                var novMaster = await BuildNovMaster(SelectedViolation);
                var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);

                novMaster.NovInformation.TicketPrintingType = "Re-Print";
                novMaster.NovInformation.SummaryOfLaw = summaryOfLaw.LawDescription;
                novMaster.NovInformation.ViolationCodeShortDescription= novMaster.ViolationDetails.ViolationCode +" - "+ novMaster.ViolationDetails.InternalShortDescription;
                
                await NavigationService.PushAsync(new PrinterViewModel() { NovMaster = novMaster });
            }else if ((string)item == "Print Duplicate")
            {
                var novMaster = await BuildNovMaster(SelectedViolation);
                var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);

                novMaster.NovInformation.TicketPrintingType = "DUPLICATE";
                novMaster.NovInformation.SummaryOfLaw = summaryOfLaw.LawDescription;
                novMaster.NovInformation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;

                await NavigationService.PushAsync(new PrinterViewModel() { NovMaster = novMaster });
            }
        });

        private async Task<NovMaster> BuildNovMaster(NovInformation violation)
        {
            var novMaster = new NovMaster
            {
                UserSession = UserSession,
                NovData = await _novService.GetNovData(violation.NovNumber),
                ViolationGroup = (await _novService.GetViolationGroups()).Find(_ => _.ViolationGroupId.Equals(violation.ViolationGroupId) && _.GroupName.Equals(violation.ViolGroupName)),
                AffidavitOfService = await _affidavitService.GetAffidavit(violation.NovNumber) ?? new AffidavitOfService(),
                AffidavitOfServiceTranList = await _affidavitService.GetAffidavitTranList(violation.NovNumber) ?? new List<AffidavitOfServiceTran>(),
                ViolationDetails = (await _novService.GetViolationDetailsByGroupAndName(violation.ViolationGroupId, violation.ViolGroupName)).Find(_ => _.ViolationCode.Equals(violation.ViolationCode.Trim())),
                NovInformation = violation
            };

            return novMaster;
        }

        public NovSummaryViewModel()
        {
            _vehicleService = DependencyResolver.Get<IVehicleService>();
            _placeOfOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
            _affidavitService = DependencyResolver.Get<IAffidavitService>();
            _novService = DependencyResolver.Get<INovService>();
            _printer = DependencyResolver.Get<IPrinter>();
            _violations = new List<NovInformation>();
            _isNewNovSelected = true;
            NotifyPropertyChanged(nameof(IsNewNovSelected));
        }

        public override async Task LoadAsync()
        {
            _isLoading = true;
            await GetTickets();
            _isNewNovSelected = true;
            NotifyPropertyChanged(nameof(IsNewNovSelected));
            _isLoading = false;
            IsGroupIdAllowMakeSimilar = false;

            NavigationService.CompleteViewStack(new List<string>());
        
            //await ProcessViolations(); //initiates the resume functionality; NH-551

            var inProgressViolations = _violations.Where(v => v.TicketStatus == "I");


            if (inProgressViolations.Any())
            {
                var latestViolation = inProgressViolations.Where(v => v.NovNumber != 0).OrderByDescending(t => t.IssuedTimestamp).First();
                var novId = CrossSettings.Current.GetValueOrDefault("ErroNo_" + latestViolation.NovNumber.ToString(), "");

                if (latestViolation != null && latestViolation.NovNumber != 0)
                    await NavigationService.SetLatestNovNumberAndAttemptNumber(latestViolation.NovNumber.ToString());

                if (latestViolation != null && (novId == "" || novId == "1" ))
                {
                    SelectedViolation = latestViolation;

                    var novMaster = await BuildNovMaster(SelectedViolation);
                    var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);

                    latestViolation.SummaryOfLaw = summaryOfLaw.LawDescription;
                    latestViolation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;

                    await NavigationService.PopToCachedScreenAsync(new ViewModelBase { NovMaster = novMaster });
                    // await Task.Run(() => NavigationService.PopToCachedScreenAsync(new ViewModelBase { NovMaster = novMaster }));
                }
            }
        }

        private async Task ProcessViolations()
        {
            var inProgressViolations = _violations.Where(v => v.TicketStatus == "I");
            
            if (inProgressViolations.Any())
            {
                var latestViolation = inProgressViolations.OrderByDescending(t => t.IssuedTimestamp).First();

                if (latestViolation != null)
                {
                    SelectedViolation = latestViolation;
                    
                    var novMaster = await BuildNovMaster(SelectedViolation);
                    var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);
                    
                    latestViolation.SummaryOfLaw = summaryOfLaw.LawDescription;
                    latestViolation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;
                    
                    await NavigationService.PopToCachedScreenAsync(new ViewModelBase { NovMaster = novMaster });
                   // await Task.Run(() => NavigationService.PopToCachedScreenAsync(new ViewModelBase { NovMaster = novMaster }));
                }
            }
        }
        public async Task GetTickets()
        {
            //Ideally, these two table would be joined in the repo for performance
            // deleter recrod having novnumber =0 -- some time it insert 0 in novinofmation table
            //await _novService.DeleteNovInfo();
            var cancelledViolations = await _novService.GetCancelNovInformation(UserSession.UserId);
            var techUser = TechResource.TechUser.Split(',');
            if(techUser.Contains(UserSession.UserId))
            {
                _violations = await _novService.GetViolations();
                cancelledViolations = await _novService.GetCancelNovInformation();
            }
            else
            {
                _violations = await _novService.GetViolations(UserSession.UserId);
            }

            _violations = _violations.Where(m => m.TicketStatus != "C").ToList();

            var cancelledNovsToNovInformation = cancelledViolations.Select(_ => new NovInformation
            {
                NovNumberAsString = _.CancelNo,
                DeviceId = _.DeviceId,
                TicketStatus = _.TicketStatus,
                IssuedTimestamp = _.SystemTimestamp,
                SystemTimestamp = _.SystemTimestamp,
                LoginTimestamp = _.LoginTimestamp,
                ReportLevel = _.ReportLevel,
                IsResp1AddressHit = _.IsResp1AddressHit,
                Resp1LastName = _.Resp1LastName,
                Resp1FirstName = _.Resp1FirstName,
                Resp1MiddleInitial = _.Resp1MiddleInitial,
                Resp1Sex = _.Resp1Sex,
                PropertyBBL = _.PropertyBBL,
                Resp1DistrictId = _.Resp1DistrictId,
                Resp1SectionId = _.Resp1SectionId,
                Resp1StreetId = _.Resp1StreetId,
                Resp1HouseNo = _.Resp1HouseNo,
                Resp1Address1 = _.Resp1Address1,
                Resp1City = _.Resp1City,
                Resp1State = _.Resp1State,
                Resp1Zip = _.Resp1Zip,
                Resp1BoroCode = _.Resp1BoroCode,
                LicenseNumber = _.LicenseNumber,
                LicenseAgency = _.LicenseAgency,
                LicenseType = _.LicenseType,
                LicenseTypeDesc = _.LicenseTypeDesc,
                IsPlaceAddressHit = _.IsPlaceAddressHit,
                PlaceLastName = _.PlaceLastName,
                PlaceFirstName = _.PlaceFirstName,
                PlaceMiddleInitial = _.PlaceMiddleInitial,
                PlaceBBL = _.PlaceBBL,
                PlaceDistrictId = _.PlaceDistrictId,
                PlaceSectionId = _.PlaceSectionId,
                PlaceStreetId = _.PlaceStreetId,
                PlaceAddress1 = _.PlaceAddress1,
                PlaceAddress2 = _.PlaceAddress2,
                PlaceAddressDescriptor = _.PlaceAddressDescriptor,
                PlaceSideOfStreet = _.PlaceSideOfStreet,
                PlaceCross1StreetId = _.PlaceCross1StreetId,
                PlaceCross2StreetId = _.PlaceCross2StreetId,
                MailableAmount = _.MailableAmount,
                MaximumAmount = _.MaximumAmount,
                HearingTimestamp = _.HearingTimestamp,
                IsAppearRequired = _.IsAppearRequired,
                AlternateService = _.AlternateService,
                BuildingType = _.BuildingType,
                IsMultipleOffences = _.IsMultipleOffences,
                IsSent = _.IsSent,
                SentTimestamp = _.SentTimestamp,
                IsReceived = _.IsReceived,
                ReceivedTimestamp = _.ReceivedTimestamp,
                ViolGroupName = _.ViolGroupName,
                ViolationCode = _.ViolationCode,
                HHTIdentifier = _.HHTIdentifier,
                UserId = _.UserId,
                VoidCancelScreen = _.VoidCancelScreen,
                PlaceBoroCode = _.PlaceBoroCode,
                PlaceHouseNo = _.PlaceHouseNo,
                ViolationGroupId = Convert.ToInt32(_.ViolationGroupId),
                ViolationTypeId = _.ViolationTypeId,
                MDRNumber = Convert.ToInt32(_.MDRNumber),
                LicenseExpDate = _.LicenseExpDate,
                BusinessName = _.BusinessName,
                CheckSum = _.CheckSum,
                FreeAddrees = _.FreeAddress,
                PrintViolationCode = _.PrintViolationCode,
                CodeLawDescription = _.CodeLawDescription,
                OfficerName = _.OfficerName,
                LawSection = _.LawSection,
                AbbrevName = _.AbbrevName,
                AgencyId = _.AgencyId,
                Title = _.Title
            });
            _violations.AddRange(cancelledNovsToNovInformation);

            var _deviceService = DependencyResolver.Get<IDeviceService>();

            LoginViewModel loginViewModel = new LoginViewModel();

            var deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            var deviceId = await _deviceService.GetDeviceIdByIMEI(deviceIMEI);


            var laterServiceNovInformation = await _novService.GetLaterViolations(deviceId);
            //laterServiceNovInformation = laterServiceNovInformation.Where(x=>x.DLDeviceId)
            _violations.AddRange(laterServiceNovInformation.ToList());
            //fix to NH-1109
            if (UserSession.Department == SHERIFF_DEP)
            {
                _violations = _violations.Where(m => m.ViolationGroupId == 7).ToList();
            }
            else
            {
                _violations = _violations.Where(m => m.ViolationGroupId != 7).ToList();
            }

            //Ideally, the sort would be done in the repo for performance
            _violations = _violations
               // .OrderByDescending(v => v.IssuedTimestamp).ThenByDescending(v => v.NovNumber)
                //.Where(n=>n.NovNumber != 0)
                .OrderByDescending(v=>v.NovNumber)
                .Distinct().ToList();

            foreach (var violation in _violations)
            {
                if (violation.PlaceAddressDescriptor != null)
                {
                    if (violation.PlaceAddressDescriptor.Equals("C"))
                    {
                        var ofStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                        var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} CORNER OF {1} AND {2}", violation.PlaceSideOfStreet, ofStreetName?.StreetName, andStreetName?.StreetName);

                    }
                    else if (violation.PlaceAddressDescriptor.Equals("B"))
                    {
                        var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        var betweenStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                        var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} {1} BETWEEN {2} AND {3}", violation.PlaceSideOfStreet, onStreetName?.StreetName, betweenStreetName?.StreetName, andStreetName?.StreetName);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("V"))
                    {
                        violation.DisplayAddress = String.Format("Vacant Lot BBL # {0}", violation.PlaceBBL);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("N"))
                    {
                        var closestStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("NEAR {0}", closestStreetName?.StreetName);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("A"))
                    {
                        violation.DisplayAddress = String.Format("{0}", violation.FreeAddrees);
                    }
                    else
                    {
                        var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} {1}", violation.PlaceHouseNo, onStreetName?.StreetName);
                    }
                }

                //fixed for 868
                if (violation.TicketStatus.Equals("C") && String.IsNullOrEmpty(violation.NovNumberAsString))
                {
                    violation.NovNumberAsString = "C"+violation.NovNumber.ToString();
                }
                //end fixed for 868

                if (String.IsNullOrEmpty(violation.NovNumberAsString))
                {
                    violation.NovNumberAsString = violation.NovNumber.ToString() + violation.CheckSum;
                }
            }

       

     

            NotifyPropertyChanged(nameof(Violations));
        }

        public List<NovInformation> Violations
        {
            get => _violations;
            set { _violations = value; NotifyPropertyChanged(); }
        }

        public NovInformation SelectedViolation
        {
            get => _selectedViolation;
            set
            {
                if (!_isLoading)
                {
                    _selectedViolation = value;
                    if (_selectedViolation != null)
                    {

                        // NH-1289
                        if (ConcreteWorkflowConstants.IsAllowMakeSimilarNovSummaryViewModel != null && _selectedViolation.ViolationGroupId > 0)
                        {
                            IsGroupIdAllowMakeSimilar = false;
                            ConcreteWorkflowConstants.IsAllowMakeSimilarNovSummaryViewModel.TryGetValue(_selectedViolation.ViolationGroupId.ToString(), out IsGroupIdAllowMakeSimilar);

                        }

                        var ticketStatus = _selectedViolation.TicketStatus;
                        if (ticketStatus == "L" || ticketStatus == "T")
                        {
                            IsLaterServiceEnabled = true;
                            IsMakeSimilarAndRePrintEnabled = true;
                                                            
                        }
                        else if (ticketStatus == "I")
                        {
                            IsLaterServiceEnabled = false;
                            IsMakeSimilarAndRePrintEnabled = true;                            
                        }
                        else if (_selectedViolation.TicketStatus == "C")
                        {
                            IsNewNovSelected = true;
                            IsLaterServiceEnabled = false;
                            IsMakeSimilarAndRePrintEnabled = false;
                        }
                        else
                        {
                            IsLaterServiceEnabled = false;
                            IsMakeSimilarAndRePrintEnabled = true;                            
                        }
                    }
                    else
                    {
                        IsNewNovSelected = true;
                        IsLaterServiceEnabled = false;
                        IsMakeSimilarAndRePrintEnabled = false;
                    }

                    NotifyPropertyChanged();
                }
            }
        }

        public bool IsLaterServiceEnabled
        {
            get => _isLaterServiceEnabled;
            set
            {
                _isLaterServiceEnabled = value;
                if (!value && IsLaterServiceSelected)
                {
                    IsNewNovSelected = true;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsMakeSimilarAndRePrintEnabled
        {
            get => _IsMakeSimilarAndRePrintEnabled;
            set
            {
                _IsMakeSimilarAndRePrintEnabled = value;
                if (!value)
                {
                    IsNewNovSelected = true;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsNewNovSelected
        {
            get => _isNewNovSelected;
            set
            {
                _isNewNovSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsLaterServiceSelected = false;
                    IsRePrintNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsPrintReportSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsMakeSimilarSelected
        {
            get => _isMakeSimilarSelected;
            set
            {
                _isMakeSimilarSelected = value;
                if (value)
                {
                    IsNewNovSelected = false;
                    IsLaterServiceSelected = false;
                    IsRePrintNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsPrintReportSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsLaterServiceSelected
        {
            get => _isLaterServiceSelected;
            set
            {
                _isLaterServiceSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsNewNovSelected = false;
                    IsRePrintNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsPrintReportSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsRePrintNovSelected
        {
            get => _isRePrintNovSelected;
            set
            {
                _isRePrintNovSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsLaterServiceSelected = false;
                    IsNewNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsPrintReportSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsPrintDuplicateSelected
        {
            get => _isPrintDuplicateSelected;
            set
            {
                _isPrintDuplicateSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsLaterServiceSelected = false;
                    IsRePrintNovSelected = false;
                    IsNewNovSelected = false;
                    IsPrintReportSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsPrintReportSelected
        {
            get => _isPrintReportSelected;
            set
            {
                _isPrintReportSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsLaterServiceSelected = false;
                    IsRePrintNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsNewNovSelected = false;
                    IsPrintAllSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public bool IsPrintAllSelected
        {
            get => _isPrintAllSelected;
            set
            {
                _isPrintAllSelected = value;
                if (value)
                {
                    IsMakeSimilarSelected = false;
                    IsLaterServiceSelected = false;
                    IsRePrintNovSelected = false;
                    IsPrintDuplicateSelected = false;
                    IsPrintReportSelected = false;
                    IsNewNovSelected = false;
                }
                NotifyPropertyChanged();
            }
        }

        public IList LongPressMenuItemSource
        {
            get
            {
                var longPressItems = new List<string>();
                if (IsLaterServiceEnabled)
                {
                    longPressItems.Add("Later Service");
                }
                if (IsMakeSimilarAndRePrintEnabled)
                {
                    if (IsGroupIdAllowMakeSimilar)
                    {
                        longPressItems.Add("Make Similar");
                    }
                    longPressItems.Add("Re-Print NOV");
                    longPressItems.Add("Print Duplicate");
                }
                return (longPressItems);
            }
        }

        public override bool ShowNextButton
        {
            get => false;
        }

        public override bool ShowPlusButton
        {
            get => true;
        }

        public override ICommand PlusCommand => new Command(_ =>
        {
            IsNewNovSelected = true;
            CrossSettings.Current.AddOrUpdateValue("MakeSimilar_SelectedViolation", "");
            NextCommand.Execute(null);
        });

        public override List<string> ActionMenuItems
        {
            get
            {
                if (UserSession != null && (UserSession.IsSupervisor == "Y"))
                {
                    return new List<string> { "Main Page", "Pin Tool", "Device Status", "Log Off" };
                }
                else
                {
                    return new List<string> { "Main Page", "Device Status", "Log Off" };
                }
            }
        }

        public override List<string> ScreenSpecificMenuItems
        {
            get { return new List<string> { "Vehicle/Radio Information", "Print Report", "Print All", "Sync" }; }
        }

        public override void MenuItemTapped(object item)
        {
            if ((string)item == "Vehicle/Radio Information")
            {
                VehicleRadioPressedCommand.Execute(null);
                return;
            }
            else if ((string)item == "Sync")
            {
                SyncPressedCommand.Execute(null);
                return;
            }else if ((string)item == "Print Report")
            {
                PrintReportPressedCommand.Execute(null);
                return;
            }
            else if ((string)item == "Print All")
            {
                PrintAllPressedCommand.Execute(null);
                return;
            }

            base.MenuItemTapped(item);
        }


        public override async Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (!await _vehicleService.isVehicleCurrent(UserSession.UserId) && UserSession.Department != SHERIFF_DEP)
            {
                alerts.Add(new AlertViewModel("11004", WorkFlowMessages.DSNYMSG_24_VehicleRadioInfoNotEnterted));
            }
            else
            {
                if (IsNewNovSelected)
                {
                    var novNumber = await _novService.CheckDeviceTicketRange();

                    if (!novNumber)
                    {
                        alerts.Add(new AlertViewModel("11004", WorkFlowMessages.DSNYMSG_24_NoMoreNovNum));
                    }

                    var novMaster = new NovMaster
                    {
                        UserSession = UserSession,
                        NovData = new List<NovData>(),
                        ViolationGroup = new ViolationGroups(),
                        AffidavitOfService = new AffidavitOfService(),
                        NovInformation = new NovInformation
                        {
                            UserId = UserSession.UserId,
                            OfficerName = UserSession.OfficerName,
                            AbbrevName = UserSession.AbbrevName,
                            AgencyId = UserSession.AgencyId,
                            Title = UserSession.Title,
                            ReportLevel = UserSession.ReportLevel,
                            DeviceId = UserSession.DutyHeader.DeviceId,
                            LoginTimestamp = LoginTimestamp,
                            IssuedTimestamp = TimeManager.Now,
                            SystemTimestamp = TimeManager.Now,
                            HearingTimestamp = UserSession.HearingTimeStamp,
                            TicketStatus = "I"
                        }
                    };

                    _nextViewModel = new GroupTypeViewModel { NovMaster = novMaster };
                }
                else if (IsLaterServiceSelected && SelectedViolation != null && (SelectedViolation.TicketStatus == "T" || SelectedViolation.TicketStatus == "L"))
                {
                    var novMaster = await BuildNovMaster(SelectedViolation);

                    var summaryOfLaw = await _novService.GetSummaryOfLaw(novMaster.ViolationDetails.LawId);
                    
                    novMaster.NovInformation.SummaryOfLaw = summaryOfLaw.LawDescription;
                    novMaster.NovInformation.ViolationCodeShortDescription = novMaster.ViolationDetails.ViolationCode + " - " + novMaster.ViolationDetails.InternalShortDescription;

                    await _novService.DeleteLaterSrvNovInfo(SelectedViolation.NovNumber);


                     _nextViewModel = new ViolationSummaryViewModel() { NovMaster = novMaster };
                }
                else if (IsMakeSimilarSelected && SelectedViolation != null)
                {
                    var hasNovNumber = await _novService.CheckDeviceTicketRange();

                    if (!hasNovNumber)
                    {
                        alerts.Add(new AlertViewModel("11004", WorkFlowMessages.DSNYMSG_24_NoMoreNovNum));
                    }
                    var novMaster = await BuildNovMaster(SelectedViolation);

                    novMaster.IsMakeSimilar = true;
                    novMaster.NovInformation.TicketStatus = "I";
                    //novMaster.NovInformation.IssuedTimestamp = TimeManager.Now;
                    novMaster.NovInformation.IssuedTimestamp = SelectedViolation.IssuedTimestamp;
                    novMaster.NovInformation.NovNumber = (await _novService.GetNextNovNumber(false)).GetValueOrDefault(); 
                    
                    // For Make Similar, update NovData to use the new Nov Number right away
                    foreach (var novData in novMaster.NovData)
                    {
                         novData.NovNumber = novMaster.NovInformation.NovNumber;
                    }

                    _nextViewModel = new GroupTypeViewModel() { NovMaster = novMaster };
                }
            }

            if(UserSession.Department == SHERIFF_DEP)
            {
                await _vehicleService.SaveVehicleRadio(new VehicleRadioInfo
                {
                    UserId = UserSession.UserId,
                    LoginTimestamp = UserSession.DutyHeader.LoginTimestamp,
                    VehicleRadioId = 1,
                    VehicleId = "N/A",
                    RadioId = "N/A",
                    StartMileage = 0,
                    EndMileage =  0
                });
            }

            return await Task.FromResult(alerts);
        }

        public override ViewModelBase NextViewModel
        {
            get
            {
                if (IsNewNovSelected || IsMakeSimilarSelected || IsLaterServiceSelected || IsRePrintNovSelected || IsPrintDuplicateSelected || IsPrintReportSelected || IsPrintAllSelected)
                {
                    return _nextViewModel;
                }

                return null;
            }
        }
        public override bool ShouldSaveTicketOnNext => false;
    }
}