﻿using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class DeviceStatusViewModel : ViewModelBase
    {
        private string _loggedIn;
        private string _nextNovNumber;
        private string _lastNovNumber;
        private string _remainingNovs;
        private string _novRangeBox1;
        private string _novRangeBox2;
        private string _personalActionNovs;
        private string _personalMultipleNovs;
        private string _personalResidentialNovs;
        private string _personalCommercialNovs;
        private string _alternativeActionNovs;
        private string _alternativeMultipleNovs;
        private string _alternativeResidentialNovs;
        private string _alternativeCommercialNovs;
        private string _ticketCount;
        private string _voidCount;
        private string _cancelCount;

        private readonly INovService _novService;
        public override string Title => "Device Status";

        public DeviceStatusViewModel()
        {
            _novService = DependencyResolver.Get<INovService>();
        }
        public override async Task LoadAsync()
        {
            LoggedIn = UserSession.DutyHeader.LoginTimestamp.ToString("MM/dd/yyyy hh:mm tt");

            var novRange = await _novService.GetTicketRange();
            if (novRange != null)
            {
                NovRangeBox1 = novRange.TicketStartNumber.ToString();
                NovRangeBox2 = novRange.TicketEndNumber.ToString();
                LastNovNumber = novRange.LastTicketNumber.ToString();
                bool isLastTicketNumberInRange = (novRange.LastTicketNumber >= novRange.TicketStartNumber && novRange.LastTicketNumber <= novRange.TicketEndNumber);
                if (isLastTicketNumberInRange)
                    RemainingNovs = (Convert.ToInt32(NovRangeBox2) - Convert.ToInt32(LastNovNumber)).ToString();
                else
                    RemainingNovs = (Convert.ToInt32(novRange.TicketEndNumber - novRange.TicketStartNumber)).ToString();
            }
            else
            {
                NovRangeBox1 = "N/A";
                NovRangeBox2 = "N/A";
                LastNovNumber = "N/A";
                RemainingNovs = "N/A";
            }

            var nextNovNum = await _novService.GetNextNovNumber(true);
            NextNovNumber = nextNovNum.HasValue ? nextNovNum.Value.ToString() : "N/A";

            var violations = await _novService.GetViolations(UserSession.UserId);
            var nonVoidViolations = violations.FindAll(v => v.TicketStatus != "V");

            var personalViolations = nonVoidViolations.FindAll(v => v.AlternateService == "P");
            PersonalActionNovs = personalViolations.Count(v => v.ViolationTypeId == "A").ToString();
            PersonalMultipleNovs = personalViolations.Count(v => v.ViolationTypeId == "M").ToString();
            PersonalResidentialNovs = personalViolations.Count(v => v.ViolationTypeId == "R").ToString();
            PersonalCommercialNovs = personalViolations.Count(v => v.ViolationTypeId.In(new[] { "C", "O", "T" })).ToString();

            var alternativeViolations = nonVoidViolations.Where(v => v.AlternateService == "A").ToList();
            AlternativeActionNovs = alternativeViolations.Count(v => v.ViolationTypeId == "A").ToString();
            AlternativeMultipleNovs = alternativeViolations.Count(v => v.ViolationTypeId == "M").ToString();
            AlternativeResidentialNovs = alternativeViolations.Count(v => v.ViolationTypeId == "R").ToString();
            AlternativeCommercialNovs = alternativeViolations.Count(v => v.ViolationTypeId.In(new[] { "C", "O" })).ToString();

            TicketCount = violations.Count(x => !x.TicketStatus.In(new[] { "C", "V" })).ToString(); //Consider everything except V and C tickets 
            VoidCount = violations.Count(x => x.TicketStatus == "V").ToString();
            CancelCount = (await _novService.GetCancelNovInformation(UserSession.UserId)).Count.ToString();
        }
        
        public string LoggedIn
        {
            get => _loggedIn;
            set { _loggedIn = value; NotifyPropertyChanged(); }
        }

        public string NextNovNumber
        {
            get => _nextNovNumber;
            set { _nextNovNumber = value; NotifyPropertyChanged(); }
        }

        public string LastNovNumber
        {
            get => _lastNovNumber;
            set { _lastNovNumber = value; NotifyPropertyChanged(); }
        }

        public string RemainingNovs
        {
            get => _remainingNovs;
            set { _remainingNovs = value; NotifyPropertyChanged(); }
        }

        public string NovRangeBox1
        {
            get => _novRangeBox1;
            set { _novRangeBox1 = value; NotifyPropertyChanged(); NotifyPropertyChanged(nameof(NovRange)); }
        }

        public string NovRangeBox2
        {
            get => _novRangeBox2;
            set { _novRangeBox2 = value; NotifyPropertyChanged(); NotifyPropertyChanged(nameof(NovRange)); }
        }

        public string NovRange => NovRangeBox1 + "    -    " + NovRangeBox2;

        public string PersonalActionNovs
        {
            get => _personalActionNovs;
            set { _personalActionNovs = value; NotifyPropertyChanged(); }
        }

        public string PersonalMultipleNovs
        {
            get => _personalMultipleNovs;
            set { _personalMultipleNovs = value; NotifyPropertyChanged(); }
        }

        public string PersonalResidentialNovs
        {
            get => _personalResidentialNovs;
            set { _personalResidentialNovs = value; NotifyPropertyChanged(); }
        }

        public string PersonalCommercialNovs
        {
            get => _personalCommercialNovs;
            set { _personalCommercialNovs = value; NotifyPropertyChanged(); }
        }

        public string AlternativeActionNovs
        {
            get => _alternativeActionNovs;
            set { _alternativeActionNovs = value; NotifyPropertyChanged(); }
        }

        public string AlternativeMultipleNovs
        {
            get => _alternativeMultipleNovs;
            set { _alternativeMultipleNovs = value; NotifyPropertyChanged(); }
        }

        public string AlternativeResidentialNovs
        {
            get => _alternativeResidentialNovs;
            set { _alternativeResidentialNovs = value; NotifyPropertyChanged(); }
        }

        public string AlternativeCommercialNovs
        {
            get => _alternativeCommercialNovs;
            set { _alternativeCommercialNovs = value; NotifyPropertyChanged(); }
        }

        public string TicketCount
        {
            get => _ticketCount;
            set { _ticketCount = value; NotifyPropertyChanged(); }
        }

        public string VoidCount
        {
            get => _voidCount;
            set { _voidCount = value; NotifyPropertyChanged(); }
        }

        public string CancelCount
        {
            get => _cancelCount;
            set { _cancelCount = value; NotifyPropertyChanged(); }
        }

        public override bool ShowMenuButton => false;

        public override bool ShowNextButton => false;

        public override ICommand BackCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });
    }
}
