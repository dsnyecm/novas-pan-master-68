﻿using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels.Interfaces
{
    public interface ILoadableViewModel
    {
        Task LoadAsync();
    }
}