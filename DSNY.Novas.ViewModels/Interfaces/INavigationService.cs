﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels.Interfaces
{
    public interface INavigationService
    {
        Task PopAsync();
        Task PopModalAsync();
        Task PushAsync(ViewModelBase viewModel);
        Task PushModalAsync(ViewModelBase viewModel);
        Task PopToRootAsync();
        Task PopToSummaryScreenAsync();
        Task PopFromSyncToLoginIfTimeoutScreenPresentOtherwiseGoBack();
        Task PopToLoginScreenAsync();
        Task PopToCachedScreenAsync(ViewModelBase baseModel);
        Task SetLatestNovNumberAndAttemptNumber(string latestIncompleteNovNumber);
        
        IEnumerable<string> CompleteViewStack(List<string> completeViewStack = null);
    }
}
