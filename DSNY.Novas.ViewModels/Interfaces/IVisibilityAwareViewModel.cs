﻿namespace DSNY.Novas.ViewModels.Interfaces
{
    public interface IVisibilityAwareViewModel
    {
        void OnAppearing();
        void OnDisappearing();
    }
}