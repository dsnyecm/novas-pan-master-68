﻿using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels.Interfaces
{
    public interface IAlertService
    {
        Task<bool> DisplayAlert(AlertViewModel alert);
    }
}
