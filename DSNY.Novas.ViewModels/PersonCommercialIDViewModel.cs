﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using Plugin.Settings;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;
namespace DSNY.Novas.ViewModels
{
    public class PersonCommercialIDViewModel : ViewModelBase
    {
        private bool _isIdChecked;
        private List<IdMatrix> _idTypes;
        private IdMatrix _idTypeSelectedItem;
        private string _idTypeFreeform;
        private List<IdMatrix> _issuedByItems;
        private IdMatrix _issuedBySelectedItem;
        private string _businessName;
        private string _idNumber;
        private DateTime _expDate;
        private bool _isBusinessNameVisible;
        private bool _isIssuedByEnabled;
        private bool _isExpDateEnabled;
        private bool _isExpDateChecked;
        private bool _isExpCheckBoxEnabled;
        private bool _idTypeDropdownIsVisible;
        private bool _isBusinessNameEnabled;
        private bool _isIdNumberEnabled;

        private string _issuedByFreeform;
        private bool _issuedByDropdownIsVisible;
        private bool _issuedByFreeformIsVisible;
        private bool _isIssuedByChecked;
        private string nextNovNum;

        private bool _showPhysicalDescriptionFields;
        private bool _hasCompletedInitialLoad;

        private List<IdMatrix> _states;
        private List<IdMatrix> _issued;

        private List<LookupTable> _age;
        private LookupTable _ageSelectedItem;
        private List<LookupTable> _skin;
        private LookupTable _skinSelectedItem;
        private List<LookupTable> _weight;
        private LookupTable _weightSelectedItem;
        private string _features;
        private List<LookupTable> _haircolor;
        private LookupTable _haircolorSelectedItem;
        private List<LookupTable> _height;
        private LookupTable _heightSelectedItem;
        private string showVoid = "";

        private readonly IPersonCommercialIDService _commercialIdService;
        private readonly ILookupService _lookupService;
        private readonly IPlaceOfOccurrenceService _placeofOccurrenceService;
        private readonly INovService _novService;

        private static readonly string ID_NOT_VALID = "NotValid";

        public override string Title
        {
            get
            {
                var title = "";
                if (IsBusinessNameVisible)
                {
                    title = "Commercial Business ID";
                    if (IsCancelled || NovMaster.NovInformation.TicketStatus == "C")
                    {
                        title = "Commercial Business ID - Cancel";
                    }
                    else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                    {
                        title = "Commercial Business ID - Void";
                    }
                }
                else
                {
                    title = "Person Being Served ID";
                    if (IsCancelled || NovMaster.NovInformation.TicketStatus == "C")
                    {
                        title = "Person Being Served ID - Cancel";
                    }
                    else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                    {
                        title = "Person Being Served ID - Void";
                    }
                }

                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }
        }

        private static readonly IDictionary<string, Func<IdMatrix, bool>> IdFilters = new Dictionary<string, Func<IdMatrix, bool>>
        {
            {"C", id => id.IsCommOff == "Y"}
            ,{"R", id => id.IsPersonalSvc == "Y"}
            ,{"M", id => id.IsMultiOff == "Y"}
            ,{"A", id => id.IsActionOff == "Y"}
            ,{"O", id => id.IsCommOff == "Y"}
        };

        public PersonCommercialIDViewModel()
        {
            _commercialIdService = DependencyResolver.Get<IPersonCommercialIDService>();
            _lookupService = DependencyResolver.Get<ILookupService>();
            _placeofOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
            _novService = DependencyResolver.Get<INovService>();

            IssuedByDropdownIsVisible = false;
            IssuedByFreeformVisible = true;
        }

        public override async Task LoadAsync()
        {
            nextNovNum = (await _novService.GetNextNovNumber(true)).ToString();
            if (nextNovNum != null)
            {
                showVoid = CrossSettings.Current.GetValueOrDefault("ShowVoid_" + nextNovNum, string.Empty);
            }

            NotifyPropertyChanged(nameof(Title));
            NotifyPropertyChanged(nameof(ShowCancelMenu));
            NotifyPropertyChanged(nameof(CancelMenuItems));

            if (IdTypeSelectedItem != null)
                if (IdTypeSelectedItem?.IdType == ID_NOT_VALID)
                {
                    IsIssuedByEnabled = false;
                    IsIdNumberEnabled = false;
                    IsExpDateChecked = false;
                    IsExpCheckBoxEnabled = false;
                    BusinessName = NovMaster.NovInformation.BusinessName;
                    IsBusinessNameEnabled = true;
                }
                else
                {
                    if (NovMaster.NovInformation.IsBusinessNameRetained == true || NovMaster.NovInformation.IsPersonBeingServedBusi == true)
                    {
                       // Added 10 / 29 / 2018 to fix ticket issue NH - 1357 in Jira
                        if (NovMaster.NovInformation.ViolationTypeId == "A")
                        {
                            if (IdTypeSelectedItem?.AskIssuedBy == "N")
                            {
                                 IsIssuedByEnabled = false; 
                             }
                            else
                            {
                                IsIssuedByEnabled = true;
                            }

                        }
                        else
                        {
                            IsIssuedByEnabled = true; // this was original line
                        }

                        IsIdNumberEnabled = true;
                        IsExpCheckBoxEnabled = true;
                        BusinessName = NovMaster.NovInformation.BusinessName;
                        if (NovMaster.NovInformation.ViolationTypeId == "O")
                        {
                            IsBusinessNameEnabled = false;
                        }
                        else
                        {
                            IsBusinessNameEnabled = true;
                        }
                    }
                    else
                    {
                        IsIssuedByEnabled = true;
                        IsIdNumberEnabled = true;
                        IsExpCheckBoxEnabled = true;
                        //BusinessName = string.Empty;
                        IsBusinessNameEnabled = true;
                    }
                }

            if (_hasCompletedInitialLoad)
            {
                return;
            }

            var novInfo = NovMaster.NovInformation;
            ShowPhysicalDescriptionFields = false; // These should only be shown the 2nd time around, as part of the Personal Service flow
            IsIdChecked = true;
            IsIssuedByChecked = true;
            IsBusinessNameVisible = novInfo.IsBusiness;

            if (IsBusinessNameVisible)
            {
                BusinessName = novInfo.BusinessName;
                //IsBusinessNameEnabled = false;
                if (NovMaster.NovInformation.ViolationGroupId == 3)
                {
                    //BusinessName = (await _placeofOccurrenceService.GetPropertyDetails(NovMaster.NovInformation.PlaceStreetId, NovMaster.NovInformation.PlaceHouseNo, NovMaster.NovInformation.PlaceBoroCode))?.FirstName;
                    if (NovMaster.NovInformation.IsBusinessNameRetained == true)
                    {
                        BusinessName = NovMaster.NovInformation.BusinessName;
                    }
                    //if (String.IsNullOrEmpty(BusinessName))
                    //{
                    IsBusinessNameEnabled = true;
                    //}
                }
                else if (NovMaster.NovInformation.ViolationTypeId == "O")
                {
                    BusinessName = NovMaster.NovInformation.BusinessName;
                    if (String.IsNullOrEmpty(BusinessName))
                    {
                        IsBusinessNameEnabled = true;
                    }
                }
                else if (NovMaster.NovInformation.ViolationGroupId == 1)
                {
                    BusinessName = NovMaster.NovInformation.BusinessName;
                    if (String.IsNullOrEmpty(BusinessName))
                    {
                        IsBusinessNameEnabled = true;
                    }
                }
            }

            if (IdTypes == null)
            {
                //if (IsBusinessNameVisible)
                //{
                //    IdTypes = (await _commercialIdService.GetIDTypes()).Where(_ => _.IsCommOff == "Y").ToList();
                //}
                //else
                //{
                //    IdTypes = (await _commercialIdService.GetIDTypes()).Where(_ => _.IsActionOff == "Y").ToList();
                //}

                IdTypes = await _commercialIdService.GetIDTypes();

                var violationTypeId = NovMaster.NovInformation.ViolationTypeId;

                if (NovMaster.NovInformation.ViolationTypeId != "O" && NovMaster.NovInformation.ViolationTypeId != "A")  //For All OTHER than Posting and Action
                {
                    IdTypes = IdFilters.ContainsKey(violationTypeId)
                    ? IdTypes.Where(IdFilters[violationTypeId]).ToList()
                    : IdTypes.Where(IdFilters["A"]).ToList();
                }
                else
                {
                    if (IsBusinessNameVisible)
                    {
                        IdTypes = (await _commercialIdService.GetIDTypes()).Where(_ => _.IsCommOff == "Y").ToList();
                    }
                    else
                    {
                        IdTypes = (await _commercialIdService.GetIDTypes()).Where(_ => _.IsActionOff == "Y").ToList();
                    }
                }
            }

            if (!string.IsNullOrEmpty(NovMaster.NovInformation.BusinessName))
                BusinessName = NovMaster.NovInformation.BusinessName;

            if (IssuedByItems == null) { IssuedByItems = await _commercialIdService.GetIssuedBy(); }
            Issued = IssuedByItems;
            if (States == null) { States = await _commercialIdService.GetIssuedByStates(); }

            //if (novInfo.LicenseType != null)
            //{
            if (NovMaster.NovInformation.LicenseType == "--" && !NovMaster.NovInformation.IsIdChecked)
            {
                IdTypeFreeform = novInfo.LicenseTypeDesc;
                IdTypeSelectedItem = null;
                IsIdChecked = false;
            }
            else
            {
                // NH-917 comment the following to retain value for the following flow:
                // PersonBeingServed (Next) > PersonBeingServedID (Next) > PlaceOfOccurrence (Back) > PersonBeingServedID (Back) > PersonBeingServed (Next)
                //novInfo.LicenseTypeDesc = null; //This is fresh load scenario. This variable is NULL on initial load. This re-sets value just in case user clicked back button. 
                if (NovMaster.NovInformation.IsBusinessNameRetained == true)
                {
                    novInfo.LicenseTypeDesc = NovMaster.NovInformation.LicenseTypeDesc;
                }
                string defaultLicenseType = getDefaultLicenseType();
                //IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc == novInfo.LicenseTypeDesc);
                IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc == ((novInfo.LicenseTypeDesc == null) ? defaultLicenseType : novInfo.LicenseTypeDesc)); // NH-1195

                if (IdTypeSelectedItem == null)
                {
                    // set the default value of ID Type when Business Name is checked in the screen PersonBeingServed
                    if (IsBusinessNameVisible)
                    {
                        if (NovMaster.NovInformation.IsBusinessNameRetained != true)
                        {
                            IdTypeSelectedItem = null;  //IdTypes.FirstOrDefault(_ => _.IdDesc.Equals("--"));
                        }
                    }
                    // comment out the following to allow default value get from the getDefaultLicenseType() when Person is checked in the screen PersonBeingServed.
                    //else
                    //{
                    //    IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc.Equals("Current Drivers License"));
                    //}

                }

            }

            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                IdTypes.Add(new IdMatrix
                {
                    IdType = "CCAL",
                    AskExpDate = "Y",
                    AskIdNo = "Y",
                    AskIssuedBy = "Y",
                    IdDesc = "Current Consumer Affairs License",
                    IsActionOff = "Y",
                    IsCommOff = "Y",
                    IsMultiOff = "Y",
                    IsPersonalSvc = "Y",
                    IssuedBy = "NYC"
                });
                IsIdChecked = true;
                IsIssuedByChecked = true;
                IsExpDateChecked = true;
                IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc.Equals("Current Consumer Affairs License"));
                IDNumber = NovMaster.NovInformation.LicenseNumber;
                ExpDate = NovMaster.NovInformation.LicenseExpDate;
            }

            if (novInfo.LicenseNumber != null) { IDNumber = novInfo.LicenseNumber; }
            if (novInfo.LicenseExpDate != null) { ExpDate = novInfo.LicenseExpDate; }


            if (novInfo.LicenseAgency != null)
            {
                IssuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == novInfo.LicenseAgency);
                if (IssuedBySelectedItem != null && IdTypeSelectedItem != null)
                {
                    IdTypeSelectedItem.IssuedBy = IssuedBySelectedItem?.IssuedBy;
                }

                IsIdChecked = getFormIsIdChecked(IsIdChecked);
                IsIssuedByChecked = getFormIsIssuedByChecked(IsIssuedByChecked);


                // Date:  10/26/2018  if License is Other type it will be handle seperately as below.
                if (novInfo.LicenseType == "OTHER")
                {
                    IdTypeSelectedItem = IdTypes.FirstOrDefault(_ => _.IdDesc.ToLower() == novInfo.LicenseType.ToLower());
                    IdTypeFreeform= novInfo.LicenseTypeDesc;
                    IdTypeSelectedItem.IssuedBy = novInfo.LicenseAgency;
                    ExpDate = novInfo.LicenseExpDate;
                }
                else
                {
                    IssuedByFreeform = novInfo.LicenseAgency;
                    //IsIssuedByChecked = false;
                }
            }
            //IsIdChecked = getFormIsIdChecked(IsIdChecked);
            //IsIssuedByChecked = getFormIsIssuedByChecked(IsIssuedByChecked);

            _hasCompletedInitialLoad = true;
        }
        private string getDefaultLicenseType()
        {
            if (ConcreteWorkflowConstants.DefaultLicenseTypePersonCommercialIDViewModel != null
                && !String.IsNullOrEmpty(NovMaster.ViolationGroup.TypeName))
            {

                string defaultLicenseTypeValue;
                if (ConcreteWorkflowConstants.DefaultLicenseTypePersonCommercialIDViewModel.TryGetValue(NovMaster.ViolationGroup.TypeName, out defaultLicenseTypeValue))
                {
                    return defaultLicenseTypeValue;
                }

            }
            return null;
        }


        public bool IsIdChecked
        {
            get => _isIdChecked;
            set
            {
                _isIdChecked = value;
                if (_isIdChecked)
                {
                    // IsIssuedByEnabled = false;
                    //   _issuedBySelectedItem = null; //race condition here (fixed after changing it to the var)
                    IsExpDateEnabled = false;
                    IsExpDateChecked = false;
                    IsExpCheckBoxEnabled = false;
                    IdTypeDropdownIsVisible = true;
                    IdTypeFreeform = "";
                }
                else
                {
                    IdTypeDropdownIsVisible = false;
                    IdTypeSelectedItem = null;
                    IsIssuedByEnabled = true;
                    IsExpDateChecked = true;
                    IsExpCheckBoxEnabled = true;
                    IsExpDateEnabled = true;
                    if (NovMaster.NovInformation.LicenseExpDate == new DateTime(1900, 1, 1))
                    {
                        IsExpDateChecked = false;
                    }
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsIssuedByChecked
        {
            get => _isIssuedByChecked;
            set
            {
                _isIssuedByChecked = value;
                if (_isIssuedByChecked)
                {
                    IssuedByDropdownIsVisible = true;
                    IssuedByFreeform = "";
                    //_issuedBySelectedItem = null;
                }
                else
                {
                    IssuedByDropdownIsVisible = false;
                    IssuedBySelectedItem = null;
                }
                NotifyPropertyChanged();
            }
        }
        public List<IdMatrix> IdTypes
        {
            get => _idTypes;
            set {
                // NH-1414 2018-11-07
                if (!IsBusinessNameVisible && value!=null) {
                    List<IdMatrix> ids = value;
                    int index = ids.FindIndex( _ => _.IdType.Equals(ID_NOT_VALID));
                    if (index >= 0)
                        ids.RemoveAt(index);
                    _idTypes = ids;
                } else
                    _idTypes = value;
                NotifyPropertyChanged(); }
        }

        public IdMatrix IdTypeSelectedItem
        {
            get => _idTypeSelectedItem;
            set
            {
                _idTypeSelectedItem = value;
                if (value != null)
                {
                    IsIssuedByEnabled = value.AskIssuedBy.Equals("Y");

                    if (!string.IsNullOrEmpty(value.IssuedBy))
                    {
                        IssuedBySelectedItem = IssuedByItems.FirstOrDefault(_ => _.IssuedBy == value.IssuedBy);
                        // IsIssuedByEnabled = false;
                    }
                    else
                    {
                        IssuedBySelectedItem = null;
                    }

                    //NH-917
                    resetExpDate();
                }

                if (_idTypeSelectedItem != null)
                {

                    if (_idTypeSelectedItem.IdType == "LP" || _idTypeSelectedItem.IdType == "CDL")
                    {
                        IssuedByItems = States;
                        IsIdNumberEnabled = true;
                        if (IDNumber == "--" || IDNumber == null)
                        {
                            IDNumber = "";
                        }
                    }
                    else
                    {
                        IssuedByItems = Issued;
                        if (_idTypeSelectedItem.IdDesc == "--")
                        {
                            IsIdNumberEnabled = false;
                            IDNumber = "--";
                        }
                        else
                        {
                            IsIdNumberEnabled = true;

                            if (_idTypeSelectedItem.IdDesc != "--")
                            {
                                if (IDNumber == "--" || IDNumber == null)
                                {
                                    IDNumber = "";
                                }
                            }
                        }

                    }

                }

                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(IsOtherIdTypeSectionVisible));
                NotifyPropertyChanged(nameof(IsIssuedByEnabled));
                NotifyPropertyChanged(nameof(IsIdNumberEnabled));
            }
        }

        public bool IsOtherIdTypeSectionVisible => IdTypeSelectedItem != null && IdTypeSelectedItem.IdDesc.ToLower().Equals("other");

        public string IdTypeFreeform
        {
            get => _idTypeFreeform;
            set { _idTypeFreeform = value; NotifyPropertyChanged(); }
        }

        public List<IdMatrix> IssuedByItems
        {
            get => _issuedByItems;
            set
            {
                _issuedByItems = value;

                if (IssuedBySelectedItem != null)
                {
                    if (_issuedBySelectedItem.IssuedBy.Trim() == "")
                    {
                        IssuedByDropdownIsVisible = false;
                        IssuedByFreeformVisible = true;
                    }
                    else
                    {
                        IssuedByDropdownIsVisible = true;
                        IssuedByFreeformVisible = false;
                    }
                }


                NotifyPropertyChanged();
            }
        }


        public bool IsBusinessNameVisible
        {
            get => _isBusinessNameVisible;
            set { _isBusinessNameVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsExpDateEnabled
        {
            get => _isExpDateEnabled;
            set
            {
                _isExpDateEnabled = value;
                if (_isExpDateEnabled)
                {
                    if (NovMaster.NovInformation.LicenseExpDate != default(DateTime) && NovMaster.NovInformation.LicenseExpDate != new DateTime(1900, 1, 1))
                    {
                        ExpDate = NovMaster.NovInformation.LicenseExpDate;
                    }
                    else
                    {
                        ExpDate = new DateTime(DateTime.Now.Year, 1, 1);
                    }
                }
                else
                {
                    ExpDate = new DateTime(1900, 1, 1);
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsExpCheckBoxEnabled
        {
            get => _isExpCheckBoxEnabled;
            set
            {
                _isExpCheckBoxEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsExpDateChecked
        {
            get => _isExpDateChecked;
            set
            {
                _isExpDateChecked = value;
                IsExpDateEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsIssuedByEnabled
        {
            get => _isIssuedByEnabled;
            set {
                // NH-1414 2018-11-07
                if (_idTypeSelectedItem != null && (_idTypeSelectedItem?.IdType == ID_NOT_VALID || _idTypeSelectedItem?.IdType == "--") )
                {
                    _isIssuedByEnabled = false;
                }
                else
                    _isIssuedByEnabled = value;
                NotifyPropertyChanged(); }
        }

        public bool IdTypeDropdownIsVisible
        {
            get => _idTypeDropdownIsVisible;
            set { _idTypeDropdownIsVisible = value; NotifyPropertyChanged(); }
        }

        public IdMatrix IssuedBySelectedItem
        {
            get => _issuedBySelectedItem;
            set
            {
                _issuedBySelectedItem = value;

                NotifyPropertyChanged();
            }
        }

        //added by chaaran to accmodate
        public string IssuedByFreeform
        {
            get => _issuedByFreeform;
            set { _issuedByFreeform = value; NotifyPropertyChanged(); }
        }

        public bool IssuedByDropdownIsVisible
        {
            get => _issuedByDropdownIsVisible;
            set { _issuedByDropdownIsVisible = value; NotifyPropertyChanged(); }
        }

        public bool IssuedByFreeformVisible
        {
            get => _issuedByFreeformIsVisible;
            set { _issuedByFreeformIsVisible = value; NotifyPropertyChanged(); }
        }

        //End Charan changes end


        public List<IdMatrix> States
        {
            get => _states;
            set { _states = value; NotifyPropertyChanged(); }
        }

        public List<IdMatrix> Issued
        {
            get => _issued;
            set { _issued = value; NotifyPropertyChanged(); }
        }

        public DateTime ExpDate
        {
            get => _expDate;
            set { _expDate = value; NotifyPropertyChanged(); }
        }

        public string BusinessName
        {
            get => _businessName;
            set { _businessName = value; NotifyPropertyChanged(); }
        }

        public bool IsBusinessNameEnabled
        {
            get => _isBusinessNameEnabled;
            set { _isBusinessNameEnabled = value; NotifyPropertyChanged(); }
        }
        public bool IsIdNumberEnabled
        {
            get => _isIdNumberEnabled;
            set {
                // NH-1414 2018-11-07
                if (_idTypeSelectedItem != null && (_idTypeSelectedItem?.IdType == "--"))
                {
                    _isIdNumberEnabled = true;
                }
                else if (_idTypeSelectedItem != null && _idTypeSelectedItem?.IdType == ID_NOT_VALID)
                {
                    _isIdNumberEnabled = false;
                }
                else
                    _isIdNumberEnabled = value;
                NotifyPropertyChanged();
            }
        }

        public string IDNumber
        {
            get => _idNumber;
            set { _idNumber = value; NotifyPropertyChanged(); }
        }

        public bool ShowPhysicalDescriptionFields
        {
            get => _showPhysicalDescriptionFields;
            set { _showPhysicalDescriptionFields = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Ages
        {
            get => _age;
            set { _age = value; NotifyPropertyChanged(); }
        }

        public LookupTable AgeSelectedItem
        {
            get => _ageSelectedItem;
            set { _ageSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Skins
        {
            get => _skin;
            set { _skin = value; NotifyPropertyChanged(); }
        }

        public LookupTable SkinSelectedItem
        {
            get => _skinSelectedItem;
            set { _skinSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Weights
        {
            get => _weight;
            set { _weight = value; NotifyPropertyChanged(); }
        }

        public LookupTable WeightSelectedItem
        {
            get => _weightSelectedItem;
            set { _weightSelectedItem = value; NotifyPropertyChanged(); }
        }

        public String OtherFeatures
        {
            get => _features;
            set { _features = value; NotifyPropertyChanged(); }
        }
        public List<LookupTable> HairColors
        {
            get => _haircolor;
            set { _haircolor = value; NotifyPropertyChanged(); }
        }

        public LookupTable HairColorSelectedItem
        {
            get => _haircolorSelectedItem;
            set { _haircolorSelectedItem = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Heights
        {
            get => _height;
            set { _height = value; NotifyPropertyChanged(); }
        }

        public LookupTable HeightSelectedItem
        {
            get => _heightSelectedItem;
            set { _heightSelectedItem = value; NotifyPropertyChanged(); }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (String.IsNullOrEmpty(IdTypeFreeform) && IdTypeSelectedItem == null)
            {
                alerts.Add(new AlertViewModel("12008", WorkFlowMessages.DSNYMSG_NOV008, elementName: "IdType"));
            }

            if (string.IsNullOrEmpty(IssuedByFreeform) && IssuedBySelectedItem == null && IsIssuedByEnabled)
            {
                alerts.Add(new AlertViewModel("12009", WorkFlowMessages.DSNYMSG_NOV009, elementName: "IssuedBy"));
            }

            if (!String.IsNullOrEmpty(IDNumber) && IsIdNumberEnabled)
            {

                if (IsExpDateEnabled && ExpDate < DateTime.Today)
                {
                    alerts.Add(new AlertViewModel("1081", WorkFlowMessages.DSNYMSG_NOV014, okTitle: "Yes", cancelTitle: "No", shouldContinueOnOk: true, elementName: "ExpDate"));
                }

            }
            if (String.IsNullOrEmpty(IDNumber) && IsIdNumberEnabled)
            {
                alerts.Add(new AlertViewModel("12010", WorkFlowMessages.DSNYMSG_NOV010, elementName: "IDNumber"));
            }

            if (IsIdChecked)
            {
                if (IdTypeSelectedItem != null)
                {
                    if (IdTypeSelectedItem.IdType == ID_NOT_VALID)
                    {
                        alerts.Add(new AlertViewModel("12062", WorkFlowMessages.DSNYMSG_NOV062, okTitle: "Yes", cancelTitle: "No", shouldContinueOnOk: true, elementName: "IdTypeSelected"));
                    }
                }

                if (string.IsNullOrEmpty(BusinessName) && IsBusinessNameVisible)
                {
                    alerts.Add(new AlertViewModel("12011", WorkFlowMessages.DSNYMSG_NOV011, elementName: "BusinessName"));
                }
            }

            return Task.FromResult(alerts);
        }

        public override void WriteFieldValuesToNovMaster()
        {
            if (IsIdChecked)
            {
                NovMaster.NovInformation.LicenseType = IdTypeSelectedItem?.IdType;
                NovMaster.NovInformation.LicenseTypeDesc =
                    IdTypeSelectedItem != null && IdTypeSelectedItem.IdDesc.ToLower().Equals("other")
                        ? IdTypeFreeform
                        : IdTypeSelectedItem?.IdDesc;
            }
            else
            {
                NovMaster.NovInformation.LicenseType = "--";
                NovMaster.NovInformation.LicenseTypeDesc = IdTypeFreeform;
            }

            if (IsIssuedByChecked)
            {
                NovMaster.NovInformation.LicenseAgency = IssuedBySelectedItem?.IssuedBy;
                //NovMaster.NovInformation.LicenseTypeDesc = IdTypeSelectedItem?.IdDesc;
            }
            else
            {
                //NovMaster.NovInformation.LicenseAgency = "--";
                NovMaster.NovInformation.LicenseAgency = IssuedByFreeform;
            }

            NovMaster.NovInformation.IsIssuedByChecked = IsIssuedByChecked;
            NovMaster.NovInformation.IsIdChecked = IsIdChecked;
            NovMaster.NovInformation.LicenseExpDate = ExpDate;
            //NovMaster.NovInformation.LicenseAgency = IssuedBySelectedItem?.IssuedBy;
            NovMaster.NovInformation.LicenseNumber = IDNumber;

            if (!string.IsNullOrWhiteSpace(BusinessName)) { NovMaster.NovInformation.BusinessName = BusinessName; }
            if (!string.IsNullOrWhiteSpace(OtherFeatures)) { NovMaster.AffidavitOfService.OtherIdentifying = OtherFeatures; }

            if (AgeSelectedItem != null) { NovMaster.AffidavitOfService.Age = AgeSelectedItem.Code; }
            if (SkinSelectedItem != null) { NovMaster.AffidavitOfService.SkinColor = SkinSelectedItem.Code; }
            if (WeightSelectedItem != null) { NovMaster.AffidavitOfService.Weight = WeightSelectedItem.Code; }
            if (HeightSelectedItem != null) { NovMaster.AffidavitOfService.Height = HeightSelectedItem.Code; }
            if (HairColorSelectedItem != null) { NovMaster.AffidavitOfService.HairColor = HairColorSelectedItem.Code; }
            if (IsCancelled)
            {
                NovMaster.NovInformation.TicketStatus = "C";
            }
            else if (IsVoidAction)
            {
                NovMaster.NovInformation.TicketStatus = "V";
            }
        }
        public override bool ShouldLockPreviousScreensOnNext => true;

        // NH-1408 2018-11-05 Action ; Point of no return is PersonBeingServedID screen

        // TODO: S6V and S7V should verify the No Point of Return and when will the NOV number is generated
        //public override List<string> CancelMenuItems
        //{
        //    get
        //    {
        //        if (NovMaster.NovInformation.ViolationCode == "S6V" || NovMaster.NovInformation.ViolationCode == "S7V")
        //        {
        //            return new List<string> { "Cancel" };
        //        }
        //        if (!String.IsNullOrEmpty(showVoid))
        //        {
        //            if (showVoid == "true")
        //            {
        //                return new List<string> { "Void" };
        //            }
        //            else
        //            {
        //                return new List<string> { "Cancel" };
        //            }
        //        }
        //        else
        //        {
        //            return new List<string> { "Cancel" };
        //        }
        //    }
        //}
        public virtual ICommand NextCommand => new Command(async () =>
        {

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }

            WriteFieldValuesToNovMaster();
            WriteValueToCrossSettings();
            if (ConcreteWorkflowConstants.IsSaveTicketNextCommandAfterPersonCommercialIDViewModel != null
                && !String.IsNullOrEmpty(NovMaster.ViolationGroup.TypeName))
            {

                bool hasSaveTicketValue = false;
                if (ConcreteWorkflowConstants.IsSaveTicketNextCommandAfterPersonCommercialIDViewModel.TryGetValue(NovMaster.ViolationGroup.TypeName, out hasSaveTicketValue))
                {
                    if (hasSaveTicketValue)
                    {
                        if (NovMaster.NovInformation.NovNumber == 0) {
                            NovMaster.NovInformation.NovNumber = (await _novService.GetNextNovNumber(false)).GetValueOrDefault();
                        }
                        await SaveTicket();
                    }
                }

            }
            //if (ShouldSaveTicketOnNext)
            //{
            //    await SaveTicket();
            //}

            if (ShouldLockPreviousScreensOnNext)
            {
                NovMaster.NovInformation.LockPreviousScreens = true;
            }

            if (ShouldLockPlaceOfOccurrenceOnNext)
            {
                NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;
            }

            var nextVM = NextViewModel;
            if (nextVM != null)
            {
                await NavigationService.PushAsync(nextVM);
            }

            NavigationInProgress = false;


        });


        public override ICommand BackCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            WriteFieldValuesToNovMaster();
            WriteValueToCrossSettings();
            await NavigationService.PopAsync();
            NavigationInProgress = false;

            NovMaster.NovInformation.IsBusinessNameRetained = true;
        });

        private void resetExpDate()
        {
            //NH-917 IsExpDateChecked = false;
            if (NovMaster.NovInformation.LicenseExpDate == default(DateTime)
                || NovMaster.NovInformation.LicenseExpDate == new DateTime(1900, 1, 1))
            {
                IsExpDateEnabled = false;
                IsExpCheckBoxEnabled = false;
                IsExpDateChecked = getFormIsExpDateChecked(false);

                if (_idTypeSelectedItem.AskExpDate == "O")
                {
                    IsExpDateEnabled = true;
                    IsExpCheckBoxEnabled = true;
                    IsExpDateChecked = getFormIsExpDateChecked(true);
                }
            }
            else
            {
                IsExpDateEnabled = true;
                IsExpCheckBoxEnabled = true;
                IsExpDateChecked = getFormIsExpDateChecked(true);
            }
        }

        private bool getFormIsIdChecked(bool defaultChecked)
        {
            var isIdCheckedLocal = CrossSettings.Current.GetValueOrDefault("PersonalComID1_IsIdChecked_" + NovMaster.NovInformation.NovNumber.ToString(), defaultChecked.ToString());
            return bool.Parse(isIdCheckedLocal);
        }
        private bool getFormIsIssuedByChecked(bool defaultChecked)
        {
            var isIssuedByCheckedLocal = CrossSettings.Current.GetValueOrDefault("PersonalComID1_IsIssuedByChecked_" + NovMaster.NovInformation.NovNumber.ToString(), defaultChecked.ToString());
            return bool.Parse(isIssuedByCheckedLocal);
        }
        private bool getFormIsExpDateChecked(bool defaultChecked)
        {
            var isExpDateCheckedLocal = CrossSettings.Current.GetValueOrDefault("PersonalComID1_IsExpDateChecked_" + NovMaster.NovInformation.NovNumber.ToString(), defaultChecked.ToString());
            return bool.Parse(isExpDateCheckedLocal);
        }
        public override void WriteValueToCrossSettings()
        {
            CrossSettings.Current.AddOrUpdateValue("PersonalComID1_IsIdChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsIdChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PersonalComID1_IsIssuedByChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsIssuedByChecked.ToString());
            CrossSettings.Current.AddOrUpdateValue("PersonalComID1_IsExpDateChecked_" + NovMaster.NovInformation.NovNumber.ToString(), IsExpDateChecked.ToString());
        }

    }
}