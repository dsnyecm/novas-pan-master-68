﻿using System;

namespace DSNY.Novas.ViewModels.Utils
{
    public enum InputType
    {
        Default,
        Any,
        Alphanumeric,
        Numeric,
        Email,
        Phone,
        Currency
    }
}
