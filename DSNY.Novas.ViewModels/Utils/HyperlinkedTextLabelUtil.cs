﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.ViewModels.Utils
{
    public static class HyperlinkedTextLabelUtil
    {
        public enum TokenType { Static, Dynamic }

        public static List<Tuple<TokenType, string, string>> GetScriptTokens(string scriptText, TokenType? tokenType = null)
        {
            var tokenList = new List<Tuple<TokenType, string, string>>();
            if (string.IsNullOrEmpty(scriptText))
                return tokenList;

            var markedText = scriptText.Replace("(", "^(").Replace(")", ")^");
            var tokens = markedText.Split('^');

            var tokenIndex = 0;
            foreach (var t in tokens)
            {
                var type = t.Contains("(") && !t.Contains("(s)") ? TokenType.Dynamic : TokenType.Static;

                if (tokenType != null && type != tokenType)
                    continue;

                string tokenId = null;

                if (type == TokenType.Dynamic)
                {
                    var previousTokens = tokenList.GetRange(0, tokenIndex).FindAll(li => li.Item3 == t).Count;
                    tokenId = string.Format("{0}||{1}", t, previousTokens);
                }

                var listItem = new Tuple<TokenType, string, string>(type, tokenId, t);
                tokenList.Add(listItem);
                tokenIndex++;
            }

            return tokenList;
        }
    }
}
