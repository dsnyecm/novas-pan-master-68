﻿using System.Linq;
using DSNY.Novas.Models;
using DSNY.Novas.Models.Enums;

namespace DSNY.Novas.ViewModels.Utils
{
    public static class NovInformationExtensions
    {
        #region ViolationGroupIds

        /// <summary>
        /// [2, 3]
        /// </summary>
        public static int[] PropertyViolationGroupIds { get; } = new[]
            {ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty}.Cast<int>().ToArray();

        /// <summary>
        /// [2, 3, 4]
        /// </summary>
        public static int[] PropertyAndPostingViolationGroupIds { get; } = new[]
            {ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty, ViolationGroupId.Posting}.Cast<int>().ToArray();

        /// <summary>
        /// [1,2,3]
        /// </summary>
        public static int[] ActionAndPropertyViolationGroupIds { get; } =
            new[] { ViolationGroupId.Action, ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty }
                .Cast<int>().ToArray();

        /// <summary>
        /// [1, 4, 7]
        /// </summary>
        public static int[] ActionPostingAndTobaccoViolationGroupIds { get; } =
            new[] { ViolationGroupId.Action, ViolationGroupId.Posting, ViolationGroupId.Tobacco }
                .Cast<int>().ToArray();

        /// <summary>
        /// [4, 6]
        /// </summary>
        public static int[] PostingAndDcodeViolationGroupIds { get; } =
            new[] { ViolationGroupId.Posting, ViolationGroupId.DCodesAndPosting }
                .Cast<int>().ToArray();

        #endregion

        #region ViolationTypeIds

        /// <summary>
        /// ["A", "O"]
        /// </summary>
        public static string[] ActionPostingDcodeAndVacantLotViolationTypeIds { get; } =
            new[] { ViolationTypeId.Action, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["A", "O", "T"]
        /// </summary>
        public static string[] ActionPostingDcodeVacantLotAndTobaccoViolationTypeIds { get; } =
            new[] { ViolationTypeId.Action, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot, ViolationTypeId.Tobacco }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["C", "M", "R"]
        /// </summary>
        public static string[] PropertyViolationTypeIds { get; } =
            new[] { ViolationTypeId.CommercialProperty, ViolationTypeId.MultipleDwellingProperty, ViolationTypeId.ResidentialProperty }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["M", "R"]
        /// </summary>
        public static string[] NonCommercialPropertyViolationTypeIds { get; } =
            new[] { ViolationTypeId.MultipleDwellingProperty, ViolationTypeId.ResidentialProperty }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();


        /// <summary>
        /// ["C", "O"]
        /// </summary>
        public static string[] CommercialPropertyDcodePostingOrVacantLotViolationTypeIds { get; } =
            new[] { ViolationTypeId.CommercialProperty, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        #endregion

        /// <summary>
        /// ["C", "V"]
        /// </summary>
        public static string[] CancelledAndVoidTicketStatuses { get; } =
            new[] { ((char)TicketStatus.Cancelled).ToString(), ((char)TicketStatus.Void).ToString() }.ToArray();

        #region NovInformation
        
        /// <summary>
        /// ViolationGroupId == 5
        /// </summary>
        /// <returns></returns>
        public static bool IsVacantLot(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.VacantLot;

        /// <summary>
        /// ViolationGroupId == 1 Or obj?.ViolationTypeId == 'A'
        /// </summary>
        /// <returns></returns>
        public static bool IsAction(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.Action || obj?.ViolationTypeId?.ToUpper()?.Equals("A") == true;

        /// <summary>
        /// ViolationTypeId.In("A", "O")
        /// </summary>
        /// <returns></returns>
        public static bool IsActionPostingDcodeOrVacantLot(this NovInformation obj) => obj?.ViolationTypeId.In(ActionPostingDcodeAndVacantLotViolationTypeIds) == true;

        /// <summary>
        /// ViolationTypeId.In("A", "O", "T")
        /// </summary>
        /// <returns></returns>
        public static bool IsActionPostingDcodeVacantLotOrTobacco(this NovInformation obj) => obj?.ViolationTypeId.In(ActionPostingDcodeVacantLotAndTobaccoViolationTypeIds) == true;

        /// <summary>
        /// ViolationGroupId.In(1, 4, 7)
        /// </summary>
        /// <returns></returns>
        public static bool IsActionPostingOrTobacco(this NovInformation obj) => obj?.ViolationGroupId.In(ActionPostingAndTobaccoViolationGroupIds) == true;

        /// <summary>
        /// ViolationGroupId == 7 Or obj?.ViolationTypeId == "T"
        /// </summary>
        /// <returns></returns>
        public static bool IsTobacco(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.Tobacco || obj?.ViolationTypeId == ((char)ViolationTypeId.Tobacco).ToString();

        /// <summary>
        /// ViolationGroupId.In(2, 3) Or ViolationTypeId.In("C", "M", "R")
        /// </summary>
        /// <returns></returns>
        public static bool IsProperty(this NovInformation obj) => obj?.ViolationGroupId.In(PropertyViolationGroupIds) == true || obj?.ViolationTypeId.In(PropertyViolationTypeIds) == true;

        /// <summary>
        /// ViolationGroupId == 3 Or obj?.ViolationTypeId == "C"
        /// </summary>
        /// <returns></returns>
        public static bool IsCommercialProperty(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.CommercialProperty || obj?.ViolationTypeId == ((char)ViolationTypeId.CommercialProperty).ToString();

        /// <summary>
        /// ViolationTypeId.In("C", "O")
        /// </summary>
        /// <returns></returns>
        public static bool IsCommercialPropertyDcodePostingOrVacantLot(this NovInformation obj) => obj?.ViolationTypeId.In(CommercialPropertyDcodePostingOrVacantLotViolationTypeIds) == true;

        /// <summary>
        /// ViolationGroupId == 2 Or ViolationTypeId.In("M", "R")
        /// </summary>
        /// <returns></returns>
        public static bool IsNonCommercialProperty(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.NonCommercialProperty || obj?.ViolationTypeId.In(NonCommercialPropertyViolationTypeIds) == true;

        /// <summary>
        /// ViolationGroupId.In(1, 2, 3)
        /// </summary>
        /// <returns></returns>
        public static bool IsActionOrProperty(this NovInformation obj) => obj?.ViolationGroupId.In(ActionAndPropertyViolationGroupIds) == true;

        /// <summary>
        /// ViolationGroupId.In(2, 3, 4)
        /// </summary>
        /// <returns></returns>
        public static bool IsPropertyOrPosting(this NovInformation obj) => obj?.ViolationGroupId.In(PropertyAndPostingViolationGroupIds) == true;

        /// <summary>
        /// ViolationGroupId == 4
        /// </summary>
        /// <returns></returns>
        public static bool IsPosting(this NovInformation obj) => obj?.ViolationGroupId == (int)ViolationGroupId.Posting;

        /// <summary>
        /// ViolationGroupId.In(4, 6)
        /// </summary>
        /// <returns></returns>
        public static bool IsPostingOrDcode(this NovInformation obj) => obj?.ViolationGroupId.In(PostingAndDcodeViolationGroupIds) == true;

        
        /// <summary>
        /// TicketStatus == "C"
        /// </summary>
        /// <returns></returns>
        public static bool IsCancelled(this NovInformation obj) => obj?.TicketStatus == ((char)TicketStatus.Cancelled).ToString();

        /// <summary>
        /// TicketStatus == "V"
        /// </summary>
        /// <returns></returns>
        public static bool IsVoid(this NovInformation obj) => obj?.TicketStatus == ((char)TicketStatus.Void).ToString();

        /// <summary>
        /// TicketStatus.In("C","V")
        /// </summary>
        /// <returns></returns>
        public static bool IsVoidOrCancelled(this NovInformation obj) => obj?.TicketStatus.In(CancelledAndVoidTicketStatuses) ?? false;

        #endregion
    }
}
