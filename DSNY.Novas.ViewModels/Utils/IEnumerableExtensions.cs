﻿using System.Collections.Generic;
using System.Linq;

namespace DSNY.Novas.ViewModels.Utils
{
    public static class IEnumerableExtensions
    {
        public static bool In<T>(this T value, IEnumerable<T> values) => values?.Contains(value) ?? false;
        public static bool In<T>(this T value, params T[] values) => values?.Contains(value) ?? false;
    }
}
