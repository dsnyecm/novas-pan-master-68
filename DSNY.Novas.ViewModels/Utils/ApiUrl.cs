﻿using DSNY.Novas.Models;
using DSNY.Novas.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Common;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels.Utils
{

    public class ApiUrl
    {
        private IDnsWatcher _dnsWatcher;
        private string networkName;
        private string ipAddress;
        private string _aPIServiceUrl;

        private readonly ILookupService _lookupService;

        private List<LookupTable> _endUrl;
        private static string SyncServiceBaseUrl => ServiceUrlResources.ResourceManager.GetString("SyncServiceBaseUrl");
        private static string SyncServiceEndpoint => ServiceUrlResources.ResourceManager.GetString("SyncServiceEndpoint");
        protected static Uri SyncServiceBaseUri => !string.IsNullOrWhiteSpace(SyncServiceBaseUrl) ? new Uri(SyncServiceBaseUrl) : null;

        protected static Uri SyncServiceUri => SyncServiceBaseUri != null && !string.IsNullOrWhiteSpace(SyncServiceEndpoint)
            ? new Uri(SyncServiceBaseUri, SyncServiceEndpoint)
            : null;


       
        public ApiUrl()
        {
            _lookupService = new LookupService();
            _dnsWatcher = DependencyResolver.Get<IDnsWatcher>();
            NetworkName = _dnsWatcher.GetNetworkName();
            IpAddress = _dnsWatcher.GetIpAddress()?.ToString();





            string SyncServiceUrl = CrossSettings.Current.GetValueOrDefault("SyncServiceUrl", string.Empty);

            if(string.IsNullOrEmpty(SyncServiceUrl))
            {
                Task.Run(async () =>
                {
               
                    var urlLocal = await ServicUrl();
                    await Task.Delay(500);
                    SyncServiceUrl = urlLocal;

                    CrossSettings.Current.AddOrUpdateValue("SyncServiceUrl", SyncServiceUrl);
                });
            }


            //SyncServiceUrl = ServicUrl().ToString();
        }

       // public  string SyncServiceUrl => SyncServiceUri?.ToString();
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                ipAddress = value;
            }
        }
        public string NetworkName
        {
            get => networkName;
            set
            {
                networkName = value;
            }
        }
        public string SyncServiceUrl
        {
            get => _aPIServiceUrl;
            set
            {
                _aPIServiceUrl = value;
            }
        }
        // public static string SyncServiceUrl { get; set; }

        public async Task<string> ServicUrl()
        {

            string strApiUrl = string.Empty;

            if (!string.IsNullOrEmpty(IpAddress))
            {
                IList<LookupTable> urls = await _lookupService.GetLookupCode("uri");

                string hostSubnet = getSubNet(IpAddress);

               
                foreach (var item in urls)
                {
                    if (hostSubnet == item.Code)
                    {
                        strApiUrl = item.Description;
                    }
                }

            }

            if(string.IsNullOrEmpty(strApiUrl))
            {
                strApiUrl = SyncServiceUri.ToString();
            }
            return strApiUrl;
        }
        public async Task<List<string>> GetAllServicUrls()
        {

            var lstUrls = new List<string>();
            IList<LookupTable> urls = await _lookupService.GetLookupCode("uri");
            foreach(var item in urls)
            {
                lstUrls.Add(item.Description);
            }
            lstUrls.Add(SyncServiceUri.ToString());

            return lstUrls;
        }
        private string getSubNet(string ip)
        {
            string subNet = string.Empty;
            var ipPatrs = ip.Split('.');
            if(ipPatrs.Length>=3)
            {
                subNet = ipPatrs[0] + "." + ipPatrs[1] + "." + ipPatrs[2];
            }

            return subNet;
        }
}
}
