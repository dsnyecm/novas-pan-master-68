﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels.Utils
{
    public static class StringExtensions
    {
        public static string WithMaxLength(this string value, int maxLength)
        {
            return value?.Substring(0, Math.Min(value.Length, maxLength));
        }

        public static string TruncateAtWord(this string value, int length)
        {
            if (value == null || value.Length < length || value.IndexOf(" ", length) == -1)
                return value;

            return value.Substring(0, value.IndexOf(" ", length));
        }

        public static string GetUntilOrEmpty(this string text, string stopAt = "|")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }

        public static string ToCrossSettingsString(this string key, string defaultValue)
        {
            return CrossSettings.Current.GetValueOrDefault(key, defaultValue);
        }
    }
}
