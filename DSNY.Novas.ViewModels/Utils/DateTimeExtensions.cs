﻿using System;

namespace DSNY.Novas.ViewModels.Utils
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Returns whether or not a TimeSpan falls within the supplied range
        /// </summary>
        /// <param name="value">The TimeSpan to be checked</param>
        /// <param name="from">The start of the range, inclusive</param>
        /// <param name="to">The end of the range, inclusive or exclusive as specified by <paramref name="isToInclusive"/></param>
        /// <param name="isToInclusive">Whether or not <paramref name="to"/> should be considered inclusive or exclusive. Defaults to 'true'.</param>
        /// <returns></returns>
        public static bool Between(this TimeSpan value, TimeSpan from, TimeSpan to, bool isToInclusive = true) =>
            value >= from && (isToInclusive ? value <= to : value < to);

        /// <summary>
        /// Returns whether or not a DateTime falls within the supplied range
        /// </summary>
        /// <param name="value">The TimeSpan to be checked</param>
        /// <param name="from">The start of the range, inclusive</param>
        /// <param name="to">The end of the range, inclusive or exclusive as specified by <paramref name="isToInclusive"/></param>
        /// <param name="isToInclusive">Whether or not <paramref name="to"/> should be considered inclusive or exclusive. Defaults to 'true'.</param>
        /// <returns></returns>
        public static bool Between(this DateTime value, DateTime from, DateTime to, bool isToInclusive = true) =>
            value >= from && (isToInclusive ? value <= to : value < to);

        /// <summary>
        /// Returns whether or not the given date's value is equal to 'default(DateTime)' or 1900-JAN-01
        /// </summary>
        /// <param name="date">The date to be interrogated</param>
        /// <returns></returns>
        public static bool IsDefaultDate(this DateTime date) => date.In(default(DateTime), new DateTime(1900, 1, 1));
    }
}
