﻿using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class PrinterViewModel : ViewModelBase
    {
        private bool IS_DISPLAY_SUMMARY_OF_LAW = false;
        private int PRN_PRINT_STOP = 1482 - 300;
        private int PRN_DEFAULT_END = 3275 + 25;
        private bool _isRetrievingChecked;
        private bool _isFormattingTicketChecked;
        private bool _isConnectingChecked;
        private bool _isSendingChecked;
        private bool _isCloseConnectionChecked;
        private string _printerStatus;
        private readonly IPrinter _printer;
        private readonly IPersonBeingServedService _personBeingServedService;
        private readonly IPlaceOfOccurrenceService _placeOfOccurrenceService;
        private readonly IPersonnelDataService _personnelDataService;
        private readonly ILookupService _lookupService;
        private readonly INovService _novService;
        private Dictionary<string, string> _mapFieldsToNovInfo;
        private bool _isLoading;
        private bool printReport;
        private bool printAll;

        public override string Title => "Printing";
        public PrinterViewModel()
        {
            _printer = DependencyResolver.Get<IPrinter>();
            _placeOfOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
            _personnelDataService = DependencyResolver.Get<IPersonnelDataService>();
            _lookupService = DependencyResolver.Get<ILookupService>();
            _novService = DependencyResolver.Get<INovService>();
        }

        public override async Task LoadAsync()
        {
            try
            {
                _printer.SetConnectionOpenSuccessCommand(new Command(async ConnectionIsOpen =>
                {
                    try
                    {
                        if ((bool)ConnectionIsOpen)
                            IsConnectingChecked = true;
                        else
                            await AlertService.DisplayAlert(new AlertViewModel("Connection Failed: ", "Unable to Pair With: " + NovMaster?.UserSession?.SelectedPrinter?.SerialNumber));
                    }
                    catch (Exception ex)
                    {
                        var stack = ex.StackTrace.ToString();
                    }
                }));

                _printer.SetSendCommandSuccess(new Command(async SendingSuccessful =>
                {
                    try
                    {
                        if ((bool)SendingSuccessful)
                            IsSendingChecked = true;
                        else
                            await AlertService.DisplayAlert(new AlertViewModel("Sending Data Failed: ", "Unable to Send Data to: " + NovMaster?.UserSession?.SelectedPrinter?.SerialNumber));
                    }
                    catch (Exception ex)
                    {
                        var stack = ex.StackTrace.ToString();
                    }
                }));

                _printer.SetConnectionClosedSuccessCommand(new Command(async ConnectionIsClosed =>
                {
                    try
                    {
                        if ((bool)ConnectionIsClosed)
                            IsCloseConnectionChecked = true;
                        else
                            await AlertService.DisplayAlert(new AlertViewModel("Connection Closing Failed: ", "Unable to Close Connection with: " + NovMaster?.UserSession?.SelectedPrinter?.SerialNumber));
                    }
                    catch (Exception ex)
                    {
                        var stack = ex.StackTrace.ToString();
                    }
                }));

                IsLoading = true;
                IsRetrievingChecked = true;
                MapFieldsToNovInfo = new Dictionary<string, string>();

                printReport = NovMaster?.NovInformation == null && NovMasterList == null;
                printAll = NovMasterList != null;

                if (!printReport)
                {
                    if (!printAll)
                    {
                        MapFieldsToNovInfo = await MapAllFieldsToValues(MapFieldsToNovInfo);
                        IsFormattingTicketChecked = true;
                        PrinterStatus = String.Format("Connecting to {0}", NovMaster?.UserSession?.SelectedPrinter?.SerialNumber);
                        await _printer.InitiateConnection(NovMaster?.UserSession?.SelectedPrinter?.MacAddress, false, MapFieldsToNovInfo, NovMaster?.UserSession?.Department);

                        IsConnectingChecked = true;
                        IsSendingChecked = true;
                        await Task.Delay(500);
                        IsLoading = false;
                        NextCommand?.Execute(null);

                    }
                    else
                    {
                        var novMasterList = NovMasterList.FindAll(x => x.NovInformation.TicketStatus == " " || x.NovInformation.TicketStatus == "L"); //TODO check for L too
                        PrintAllConfirmation(novMasterList.Count);
                    }
                }
                else
                {
                    MapFieldsToNovInfo = await MapAllFieldsToValuesForPrintReport(MapFieldsToNovInfo, NovMaster);
                    IsFormattingTicketChecked = true;
                    PrinterStatus = String.Format("Connecting to {0}", NovMaster?.UserSession?.SelectedPrinter?.SerialNumber);
                    await _printer.InitiateConnectionForPrintReport(NovMaster?.UserSession?.SelectedPrinter?.MacAddress, MapFieldsToNovInfo, NovMaster?.UserSession?.Department);

                    IsConnectingChecked = true;
                    IsSendingChecked = true;
                    await Task.Delay(500);
                    IsLoading = false;
                    NextCommand?.Execute(null);
                }
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace;
            }
        }

        public bool IsRetrievingChecked
        {
            get => _isRetrievingChecked;
            set
            {
                _isRetrievingChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsFormattingTicketChecked
        {
            get => _isFormattingTicketChecked;
            set
            {
                _isFormattingTicketChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsConnectingChecked
        {
            get => _isConnectingChecked;
            set
            {
                _isConnectingChecked = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsSendingChecked
        {
            get => _isSendingChecked;
            set
            {
                _isSendingChecked = value;
                NotifyPropertyChanged();
            }
        }
        public bool IsCloseConnectionChecked
        {
            get => _isCloseConnectionChecked;
            set
            {
                _isCloseConnectionChecked = value;
                NotifyPropertyChanged();
            }
        }

        public string PrinterStatus
        {
            get => _printerStatus;
            set
            {
                _printerStatus = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                _isLoading = value;
                NotifyPropertyChanged();
            }
        }

        public Dictionary<string, string> MapFieldsToNovInfo
        {
            get => _mapFieldsToNovInfo;
            set { _mapFieldsToNovInfo = value;  }
        }

        private async Task<Dictionary<string, string>> MapAllFieldsToValuesForPrintReport(Dictionary<string, string> MapFieldsToNovInfo, NovMaster novMaster)
        {
            var _violations = await _novService.GetViolations(novMaster.UserSession.UserId);

            //NH-865
            _violations = _violations.Where(m => m.TicketStatus != "C").ToList();
            
            var cancelledViolations = await _novService.GetCancelNovInformation(novMaster.UserSession.UserId);
            var cancelledNovsToNovInformation = cancelledViolations.Select(_ => new NovInformation
            {
                NovNumberAsString = _.CancelNo,
                DeviceId = _.DeviceId,
                TicketStatus = _.TicketStatus,
                IssuedTimestamp = _.SystemTimestamp,
                SystemTimestamp = _.SystemTimestamp,
                LoginTimestamp = _.LoginTimestamp,
                ReportLevel = _.ReportLevel,
                IsResp1AddressHit = _.IsResp1AddressHit,
                Resp1LastName = _.Resp1LastName,
                Resp1FirstName = _.Resp1FirstName,
                Resp1MiddleInitial = _.Resp1MiddleInitial,
                Resp1Sex = _.Resp1Sex,
                PropertyBBL = _.PropertyBBL,
                Resp1DistrictId = _.Resp1DistrictId,
                Resp1SectionId = _.Resp1SectionId,
                Resp1StreetId = _.Resp1StreetId,
                Resp1HouseNo = _.Resp1HouseNo,
                Resp1Address1 = _.Resp1Address1,
                Resp1City = _.Resp1City,
                Resp1State = _.Resp1State,
                Resp1Zip = _.Resp1Zip,
                Resp1BoroCode = _.Resp1BoroCode,
                LicenseNumber = _.LicenseNumber,
                LicenseAgency = _.LicenseAgency,
                LicenseType = _.LicenseType,
                LicenseTypeDesc = _.LicenseTypeDesc,
                IsPlaceAddressHit = _.IsPlaceAddressHit,
                PlaceLastName = _.PlaceLastName,
                PlaceFirstName = _.PlaceFirstName,
                PlaceMiddleInitial = _.PlaceMiddleInitial,
                PlaceBBL = _.PlaceBBL,
                PlaceDistrictId = _.PlaceDistrictId,
                PlaceSectionId = _.PlaceSectionId,
                PlaceStreetId = _.PlaceStreetId,
                PlaceAddress1 = _.PlaceAddress1,
                PlaceAddress2 = _.PlaceAddress2,
                PlaceAddressDescriptor = _.PlaceAddressDescriptor,
                PlaceSideOfStreet = _.PlaceSideOfStreet,
                PlaceCross1StreetId = _.PlaceCross1StreetId,
                PlaceCross2StreetId = _.PlaceCross2StreetId,
                MailableAmount = _.MailableAmount,
                MaximumAmount = _.MaximumAmount,
                HearingTimestamp = _.HearingTimestamp,
                IsAppearRequired = _.IsAppearRequired,
                AlternateService = _.AlternateService,
                BuildingType = _.BuildingType,
                IsMultipleOffences = _.IsMultipleOffences,
                IsSent = _.IsSent,
                SentTimestamp = _.SentTimestamp,
                IsReceived = _.IsReceived,
                ReceivedTimestamp = _.ReceivedTimestamp,
                ViolGroupName = _.ViolGroupName,
                ViolationCode = _.ViolationCode,
                HHTIdentifier = _.HHTIdentifier,
                UserId = _.UserId,
                VoidCancelScreen = _.VoidCancelScreen,
                PlaceBoroCode = _.PlaceBoroCode,
                PlaceHouseNo = _.PlaceHouseNo,
                ViolationGroupId = Convert.ToInt32(_.ViolationGroupId),
                ViolationTypeId = _.ViolationTypeId,
                MDRNumber = Convert.ToInt32(_.MDRNumber),
                LicenseExpDate = _.LicenseExpDate,
                BusinessName = _.BusinessName,
                CheckSum = _.CheckSum,
                FreeAddrees = _.FreeAddress,
                PrintViolationCode = _.PrintViolationCode,
                CodeLawDescription = _.CodeLawDescription,
                OfficerName = _.OfficerName,
                LawSection = _.LawSection,
                AbbrevName = _.AbbrevName,
                AgencyId = _.AgencyId,
                Title = _.Title
            });
            _violations.AddRange(cancelledNovsToNovInformation);
            
            _violations = _violations
               .OrderByDescending(v => v.NovNumber)
               .ToList();

            var ynumber = 350;
            var NovSummary = "";
            StringBuilder sb = new StringBuilder();

            foreach (var violation in _violations)
            {
                if (violation.PlaceAddressDescriptor != null)
                {
                    if (violation.PlaceAddressDescriptor.Equals("C"))
                    {
                        var ofStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                        var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} CORNER OF {1} AND {2}", violation.PlaceSideOfStreet, ofStreetName?.StreetName, andStreetName?.StreetName);

                    }
                    else if (violation.PlaceAddressDescriptor.Equals("B"))
                    {
                        var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        var betweenStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                        var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} {1} BETWEEN {2} AND {3}", violation.PlaceSideOfStreet, onStreetName?.StreetName, betweenStreetName?.StreetName, andStreetName?.StreetName);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("V"))
                    {
                        violation.DisplayAddress = String.Format("BBL # {0}", violation.PlaceBBL);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("N"))
                    {
                        var closestStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("NEAR {0}", closestStreetName?.StreetName);
                    }
                    else if (violation.PlaceAddressDescriptor.Equals("A"))
                    {
                        violation.DisplayAddress = String.Format("{0}", violation.FreeAddrees);
                    }
                    else
                    {
                        var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                        violation.DisplayAddress = String.Format("{0} {1}", violation.PlaceHouseNo, onStreetName?.StreetName);
                    }
                }
            }

            for (int i = 0; i < _violations.Count; i++)
            {
                sb.Append($"@{ynumber},20:ZP04P|{_violations[i].TicketStatus}|");
                sb.Append(Environment.NewLine);

                //NH-865
                if (_violations[i].TicketStatus=="C")
                {
                    sb.Append($"@{ynumber},80:ZP04P|{_violations[i].NovNumberAsString}|");
                }
                else
                {
                    sb.Append($"@{ynumber},80:ZP04P|{_violations[i].NovNumber}{_violations[i].CheckSum}|");
                }
               
                sb.Append(Environment.NewLine);
                sb.Append($"@{ynumber},200:ZP04P|{_violations[i].ViolationCode}|");
                sb.Append(Environment.NewLine);
                sb.Append($"@{ynumber},270:ZP04P|{_violations[i].DisplayAddress.TruncateAtWord(26)}|");
                sb.Append(Environment.NewLine);
                sb.Append($"@{ynumber},620:ZP04P|{_violations[i].IssuedTimestamp}|");
                sb.Append(Environment.NewLine);

                NovSummary = sb.ToString();

                ynumber = ynumber + 30;
            }
            
            MapFieldsToNovInfo.Add("<<DynamicNovSummaryPlaceholder>>", NovSummary);
            MapFieldsToNovInfo.Add("<<LinesBeforeStopping>>", ynumber.ToString());

            return MapFieldsToNovInfo;
        }
        
        private string ReplaceVariablesInViolationScript()
        {
            var token = "SAVED_TOKEN";
            //replace (s) with a token string that we can replace later
            if (!string.IsNullOrEmpty(NovInformation.ViolationScript))
            {
                var tokenized = Regex.Replace(NovInformation.ViolationScript, "\\((s)\\)", token);
                //remove all remaining paranthesis, then replace our token with (s)
                NovInformation.ViolationScript = Regex.Replace(tokenized, "\\(|\\)", "").Replace(token, "(s)");
            }

            if (!String.IsNullOrEmpty(AffidavitOfService?.ServedLocation) && (NovInformation.ViolationScript.Contains("T/P/O") || NovInformation.ViolationScript.Contains("t/p/o")))
            {
                NovInformation.ViolationScript = Regex.Replace(NovInformation.ViolationScript, "T/P/O", AffidavitOfService?.ServedLocation, RegexOptions.IgnoreCase);
            }

            return NovInformation.ViolationScript;
                       
        }

        private string FindPlaceAddressDescriptor()
        {
            //changes the letter to value for printing
            if (Descriptors.ContainsKey(NovInformation.PlaceAddressDescriptor))
                NovInformation.PlaceAddressDescriptor = Descriptors[NovInformation.PlaceAddressDescriptor];

            return NovInformation.PlaceAddressDescriptor;
        }

        private async Task<string> SetLawSectionWithLegalPrefix()
        {
            var legalPrefix = await _lookupService.GetLookupTable("Legal Prefix");
            var match = legalPrefix.Find(_ => _.Description.GetUntilOrEmpty() == NovInformation.LawSection);
            if (match != null)
            {
                var stringToRemove = match.Description.GetUntilOrEmpty();

                var legalPrefixForLawSection = match.Description.Replace($"{stringToRemove}|", "");
                NovInformation.LawSection = $"{legalPrefixForLawSection} {NovInformation.LawSection}"; 
            }

            return NovInformation.LawSection;
        }

        /// <summary>
        /// Method: MapAllFieldsToValues
        /// Summary: Replaces the placeholders in the UniversalSummon.txt prn file with values from values from ticket object
        /// </summary>
        /// <param name="MapFieldsToNovInfo"></param>
        /// <returns>Dictionary<string,string></returns>
        private async Task<Dictionary<string, string>> MapAllFieldsToValues(Dictionary<string, string> MapFieldsToNovInfo)
        {
            //Gets a copy of ViolationScript without the paranthesis for printing
            NovInformation.ViolationScript = ReplaceVariablesInViolationScript();

            //Finds the First Descriptor of an Address 
            NovInformation.PlaceAddressDescriptor = FindPlaceAddressDescriptor();

            //Finds the Legal Prefix for Law Section Code 
            NovInformation.LawSection = await SetLawSectionWithLegalPrefix();

            //Calculate CBNO number based on PlaceDistrictId
            var CBNO = NovInformation.ViolationGroupId == 7
                ? NovInformation.PlaceDistrictId
                : !string.IsNullOrWhiteSpace(NovInformation.PlaceDistrictId)
                    ? NovInformation.PlaceDistrictId.Substring(1, 2)
                    : "";

            MapFieldsToNovInfo.Add("<<NovNumber>>", NovInformation.NovNumber.ToString());
            // TODO: "Enforcement Agency Name" is currently set to Department of Sanitation in the PRN file - this may need to be dynamic once we have multiple agencies set up
            MapFieldsToNovInfo.Add("<<AgencyId>>", NovInformation.AgencyId);
            MapFieldsToNovInfo.Add("<<ReportLevel>>", NovInformation.ReportLevel);
            // TODO: There is an "Agency Address and Phone Number" field on the ticket - this may need to be dynamic, as well

            if (AffidavitOfService.PersonallyServeFlag == "A") //Alternative Service 
            {
                MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");

                if(NovInformation.ViolationGroupId==4)
                {
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>",
                    NovInformation.IsBusiness
                        ? NovInformation.BusinessName
                        : NovInformation.Resp1LastName + ", " + NovInformation.Resp1FirstName + " " + NovInformation.Resp1MiddleInitial);
                }
                else
                {
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>",
                    NovInformation.IsBusiness
                        ? NovInformation.BusinessName
                        : NovInformation.Resp1FirstName);
                }
                

                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
               
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                //MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--"))? NovInformation.LicenseAgency:"--");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");

                MapFieldsToNovInfo.Add("<<PlaceBBL>>",
                    NovInformation.ViolationGroupId == 5
                        ? $" BBL: {NovInformation.PlaceBBL}"
                        : "");
                
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  ": "");
                
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);

                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else if (AffidavitOfService.PersonallyServeFlag == "P" && NovInformation.ViolationTypeId == "A" && NovInformation.TicketStatus == "L")
            {
                if (!NovInformation.IsBusiness)
                {
                        MapFieldsToNovInfo.Add("<<Resp1LastName>>", NovInformation.Resp1LastName + ",  ");
                        MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1MiddleInitial) ? NovInformation.Resp1MiddleInitial + ".  ": "");
                        MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.Resp1FirstName + "  ");                   
                }
                else
                {
                    MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
                }

                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<PlaceBBL>>", "");
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else if (AffidavitOfService.PersonallyServeFlag == "P" && NovInformation.ViolationTypeId == "A" && NovInformation.TicketStatus != "L")
            {
                if (!NovInformation.IsBusiness)
                {
                    MapFieldsToNovInfo.Add("<<Resp1LastName>>", !string.IsNullOrEmpty(NovInformation.Resp1LastName)? NovInformation.Resp1LastName + ",  ":"");
                    MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1MiddleInitial) ? NovInformation.Resp1MiddleInitial + ".  " : "");
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.Resp1FirstName + "  ");
                }
                else
                {
                    MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
                }

                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<PlaceBBL>>", "");
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else if (AffidavitOfService.PersonallyServeFlag == "P" && (NovInformation.ViolationTypeId == "R" || NovInformation.ViolationTypeId == "C"))
            {
                MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");
                MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<PlaceBBL>>", "");
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else if (AffidavitOfService.PersonallyServeFlag == "P" && NovInformation.ViolationGroupId == 5) 
            {
                MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");
                MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<PlaceBBL>>", " BBL: " + NovInformation.PlaceBBL);
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else if (AffidavitOfService.PersonallyServeFlag == "P" && (NovInformation.ViolationGroupId == 4 || NovInformation.ViolationGroupId == 6))
            {
                if (NovInformation.IsBusiness)
                {
                    MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
                }
                else
                {
                    MapFieldsToNovInfo.Add("<<Resp1LastName>>", !string.IsNullOrEmpty(NovInformation.Resp1LastName) ? $"{NovInformation.Resp1LastName}, " : "");
                    MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", !string.IsNullOrWhiteSpace(NovInformation.Resp1MiddleInitial) ? $"{NovInformation.Resp1MiddleInitial}.  " : "");
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", $"{NovInformation.Resp1FirstName}  ");
                }

                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<PlaceBBL>>", "");
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", ((!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--")) ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            else
            {
                MapFieldsToNovInfo.Add("<<Resp1LastName>>", "");
                MapFieldsToNovInfo.Add("<<Resp1MiddleInitial>>", "");

                if (NovInformation.ViolationGroupId == 4 || NovInformation.ViolationGroupId == 1) {
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>",
                    NovInformation.IsBusiness
                        ? NovInformation.BusinessName
                        : NovInformation.Resp1LastName + ", " + NovInformation.Resp1FirstName + " " + NovInformation.Resp1MiddleInitial);
                }
                else
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.IsBusiness ? NovInformation.BusinessName : NovInformation.Resp1FirstName);

                MapFieldsToNovInfo.Add("<<PlaceBBL>>",
                    NovInformation.ViolationGroupId == 5
                        ? $" BBL: {NovInformation.PlaceBBL}"
                        : "");

                var placeAddressDescriptor = ShowOrHidePlaceAddressDescriptor();
                MapFieldsToNovInfo.Add("<<PlaceAddressDescriptor>>", placeAddressDescriptor);
                MapFieldsToNovInfo.Add("<<Resp1HouseNo>>", NovInformation.Resp1HouseNo);
                MapFieldsToNovInfo.Add("<<Resp1Apt>>", !String.IsNullOrWhiteSpace(NovInformation.Resp1Address1) ? ", Apt " + NovInformation.Resp1Address1 + ",  " : "");
                MapFieldsToNovInfo.Add("<<Resp1Sex>>", NovInformation.Resp1Sex);
                MapFieldsToNovInfo.Add("<<Resp1Address>>", NovInformation.Resp1Address);
                MapFieldsToNovInfo.Add("<<Resp1City>>", NovInformation.Resp1City + ",  ");
                MapFieldsToNovInfo.Add("<<Resp1State>>", NovInformation.Resp1State);
                MapFieldsToNovInfo.Add("<<Resp1Zip>>", NovInformation.Resp1Zip);
                MapFieldsToNovInfo.Add("<<LicenseNumber>>", NovInformation.LicenseNumber);
                MapFieldsToNovInfo.Add("<<LicenseTypeDesc>>", !String.IsNullOrWhiteSpace(NovInformation.LicenseTypeDesc) ? NovInformation.LicenseTypeDesc + ",  " : "");
                MapFieldsToNovInfo.Add("<<LicenseAgency>>", (!String.IsNullOrWhiteSpace(NovInformation.LicenseNumber) && NovInformation.LicenseNumber != "--") ? NovInformation.LicenseAgency : "--");
                MapFieldsToNovInfo.Add("<<CBNO>>", CBNO);
                MapFieldsToNovInfo.Add("<<LicenseExpDate>>", !String.IsNullOrEmpty(NovInformation.LicenseExpDate.ToString("d")) && NovInformation.LicenseExpDate != default(DateTime) && NovInformation.LicenseExpDate.Date != new DateTime(1900, 1, 1) ? "  EXP: " + NovInformation.LicenseExpDate.ToString("d") : "");
            }
            
            if (NovInformation.ViolationGroupId == 2 || NovInformation.ViolationGroupId == 3)
            {
                if (MapFieldsToNovInfo.ContainsKey("<<Resp1FirstName>>"))
                    MapFieldsToNovInfo["<<Resp1FirstName>>"] = NovInformation.BusinessName;
                else
                    MapFieldsToNovInfo.Add("<<Resp1FirstName>>", NovInformation.BusinessName);
            }

            MapFieldsToNovInfo.Add("<<IssuedTimeStamp>>", NovInformation.IssuedTimestamp.ToString("M/d/yyyy hh:mm tt"));

            var displayAddress = await FormatDisplayAddress(NovInformation);
            string displayAddress1 = "";
            string displayAddress2 = "";

            if (displayAddress.Length > 67)
            {
                displayAddress1 = displayAddress.TruncateAtWord(67);

                string toBeSearched = displayAddress1;
                int ix = displayAddress.IndexOf(toBeSearched);

                if (ix != -1)
                {
                    string remainingString = displayAddress.Substring(ix + toBeSearched.Length);
                    displayAddress2 = remainingString;
                }
                
                MapFieldsToNovInfo.Add("<<DisplayAddress>>", displayAddress1);
                MapFieldsToNovInfo.Add("<<DisplayAddress2>>", displayAddress2);
            }
            else
            {
                MapFieldsToNovInfo.Add("<<DisplayAddress>>", displayAddress);
                MapFieldsToNovInfo.Add("<<DisplayAddress2>>", "");
            }

            MapFieldsToNovInfo.Add("<<Borough>>", (await _personBeingServedService.GetBoros()).Find(_ => _.BoroId.Equals(NovInformation.PlaceBoroCode)).ShortName);

            MapFieldsToNovInfo.Add("<<HearingDate>>", NovInformation.HearingTimestamp.ToString("MMMM d, yyyy"));
            MapFieldsToNovInfo.Add("<<HearingTime>>", NovInformation.HearingTimestamp.ToString("hh:mm tt"));
            MapFieldsToNovInfo.Add("<<Comments>>", AffidavitOfService?.Comments);

            if(NovInformation.ViolationGroupId != 7)
                MapFieldsToNovInfo.Add("<<IsAppearInHearingChecked>>",
                    NovInformation.IsPetitionerCourtAppear == "Y"
                        ? "Petitioner Wishes to Appear If Hearing"
                        : "");

            MapFieldsToNovInfo.Add("<<CodeLawDescription>>", NovInformation.CodeLawDescription);
            MapFieldsToNovInfo.Add("<<LawSection>>", NovInformation.LawSection);
            MapFieldsToNovInfo.Add("<<HHTIdentifier>>", NovInformation.PlaceAddress1);
            MapFieldsToNovInfo.Add("<<PrintViolationCode>>", NovInformation.PrintViolationCode);
            
            MapFieldsToNovInfo.Add("<<MailableAmount>>",
                NovInformation.IsMailInPenaltyChecked
                    ? "No Mail-In Penalty. You Must Appear."
                    : NovInformation.MailableAmountString);

            MapFieldsToNovInfo.Add("<<MaximumAmount>>", NovInformation.MaximumAmountString);
            
            var signatureBitmap = NovMaster.UserSession.DutyHeader.SignatureBitmap;
            if (signatureBitmap != null)
                MapFieldsToNovInfo.Add("<<Signature>>", Convert.ToBase64String(signatureBitmap));

            MapFieldsToNovInfo.Add("<<OfficerName>>", NovInformation.OfficerName);
            MapFieldsToNovInfo.Add("<<AbbrevName>>", NovInformation.AbbrevName);
            MapFieldsToNovInfo.Add("<<Title>>", NovInformation.Title);
            MapFieldsToNovInfo.Add("<<Checksum>>",NovInformation.CheckSum);

            MapFieldsToNovInfo.Add("<<ViolationCodeShortDescription>>", $"{ViolationDetails.ViolationCode} - {ViolationDetails.InternalShortDescription}");

            var ticketPrintingType = "";

            if (NovInformation.TicketPrintingType == "DUPLICATE")
                ticketPrintingType = "DUPLICATE";
            else if (TicketStatusTicketPrintingTypeMap.ContainsKey(NovInformation.TicketStatus))
                ticketPrintingType = TicketStatusTicketPrintingTypeMap[NovInformation.TicketStatus];

            MapFieldsToNovInfo.Add("<<TicketPrintingType>>", ticketPrintingType);

            string PropertyType = null;

            if(NovInformation.ViolationTypeId == "R")
            {
                PropertyType = "Property: 1-2 Family";
            }
            else if (NovInformation.ViolationTypeId == "C")
            {
                PropertyType = "Property: Commercial";
            }else if(NovInformation.ViolationTypeId  =="M")
            {
                PropertyType = "Property: Multiple Dwelling";
            }
            else if(NovInformation.ViolationGroupId == 4)
            {
                PropertyType = "Violation Type: Posting";
            }
            else if(NovInformation.ViolationGroupId == 5)
            {
                PropertyType = "Property: Vacant Lot";
            }
            else if (NovInformation.ViolationGroupId == 1)
            {
                PropertyType = "Violation Type: Action";
            }
            else if(NovInformation.ViolationGroupId == 7)
            {
                PropertyType = "Violation Type: Tobacco";
            }
            else
            {
                PropertyType = "Violation Type: Dcodes / Const";
            }
            MapFieldsToNovInfo.Add("<<PropertyType>>", PropertyType);
            
            if(string.IsNullOrWhiteSpace(NovInformation.ViolationScript))
                NovInformation.ViolationScript = "";

            string[] lines = NovInformation.ViolationScript.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.None
            );
            string Offense = "";
            string line0 = "";

            if (lines.Length>1)
            {
                if(lines[0].Contains("[x] Respondent must appear in person"))
                {

                    line0 = lines[0].Replace("[x] Respondent must appear in person", "");
                    NovInformation.IsAppearRequired = "Y";
                    MapFieldsToNovInfo.Add("<<IsAppearInHearingChecked>>", "x");
                }
                else
                {
                    line0 =lines[0].Replace("[`] Respondent must appear in person", "");
                    MapFieldsToNovInfo.Add("<<IsAppearInHearingChecked>>", "");
                }

                Offense = $"[x] Offense: {line0}";
                NovInformation.ViolationScript = lines[1];
            }

            MapFieldsToNovInfo.Add("<<Offense>>", Offense);

            var scriptLength = NovInformation.ViolationScript.Trim().Length;
            var firstLine = 87;
            var startIndexTotal = 0;

            for (int i = 0; i < 5; i++)
            {
                MapFieldsToNovInfo[string.Format("<<ViolationScript{0}>>", i + 1)] = "";

                if (scriptLength >= 78)
                {
                    firstLine = NovInformation.ViolationScript.Substring(startIndexTotal, 78).LastIndexOf(" ") + 1;
                    MapFieldsToNovInfo[$"<<ViolationScript{i + 1}>>"] = NovInformation.ViolationScript.Substring(startIndexTotal, firstLine);
                    startIndexTotal += firstLine;
                }
                else if(scriptLength > 0)
                {
                    MapFieldsToNovInfo[$"<<ViolationScript{i + 1}>>"] = NovInformation.ViolationScript.Substring(startIndexTotal, scriptLength);
                }

                scriptLength -= firstLine;
            }

            // building Summary of law print string/Lines
            var summaryOfLawLength = !String.IsNullOrEmpty(NovInformation.SummaryOfLaw) ? NovInformation.SummaryOfLaw.Length : 0;
            int totalLines = 0;
            var startIndexTotal1 = 0;

            // Start building dyanmic Summry of law rows/Lines

            int tLines = 0;
            int summeryOfLawPosition = 3325;
            int rowSpace = 25;
            string sLawString = "";
            int LinePosition = 0;
            string linePrefix = "";
            if (IS_DISPLAY_SUMMARY_OF_LAW)
            {
                if (summaryOfLawLength != 0)
                {
                    tLines = (summaryOfLawLength / 78) + 2;

                    for (int j = 0; j <= tLines; j++)
                    {
                        int k = j + 1;

                        LinePosition = summeryOfLawPosition + k * rowSpace;
                        linePrefix = $"@{LinePosition},10:ZP04P| ";

                        if (summaryOfLawLength >= 78)
                        {
                            firstLine = NovInformation.SummaryOfLaw.Substring(startIndexTotal1, 78).LastIndexOf(" ") + 1;
                            sLawString += linePrefix + NovInformation.SummaryOfLaw.Substring(startIndexTotal1, firstLine) + "| ";
                            startIndexTotal1 += firstLine;
                        }
                        else if (summaryOfLawLength > 0)
                        {
                            sLawString += linePrefix + NovInformation.SummaryOfLaw.Substring(startIndexTotal1, summaryOfLawLength) + "| ";
                        }

                        summaryOfLawLength -= firstLine;
                    }
                }

                MapFieldsToNovInfo.Add("<<SummaryOfLawPlaceHolder>>", sLawString);
                LinePosition += 25;
            }
            else
            {
                LinePosition = PRN_DEFAULT_END;
            }

            MapFieldsToNovInfo.Add("<<LinePosition>>", Convert.ToString(LinePosition));
            LinePosition += 55;
            MapFieldsToNovInfo.Add("<<TicketPrintingTypePosition>>", Convert.ToString(LinePosition));
            MapFieldsToNovInfo.Add("<<PrintStop>>", Convert.ToString(LinePosition - 50- PRN_PRINT_STOP)); // last section print stop line position. after remove the Summary of Law

            //End building dyanmic Summry of law rows/Lines 

            /**
             * Sheriff
             * */
            // fields exists in Sheriff only
            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                // TODO: ADD CELLPHONE
                MapFieldsToNovInfo.Add("<<CellPhone>>", " ");                
                MapFieldsToNovInfo.Add("<<OathCode>>", NovInformation.ViolationCode);
                // MapFieldNamesToValues.Add("<<TaxRegistryNumber>>", "642741");
                MapFieldsToNovInfo.Add("<<IsAlternativeServiceChecked>>", " ");
                MapFieldsToNovInfo.Add("<<IsOffenseSeekingRevocationChecked>>", " ");
            }

            return MapFieldsToNovInfo;
        }

        public async Task<string> FormatDisplayAddress(NovInformation violation)
        {
            if (violation.PlaceAddressDescriptor.Equals("At Corner Of"))
            {
                var ofStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
                
                return $"{violation.PlaceSideOfStreet} Corner of {ofStreetName.StreetName} and {andStreetName.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Equals("On/Between"))
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                var betweenStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);
               
                return $"On {violation.PlaceSideOfStreet} Of {onStreetName?.StreetName} Between {betweenStreetName?.StreetName} and {andStreetName?.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Equals("Near"))
            {
                var closestStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
               
                return $"Near {closestStreetName.StreetName} {violation.FreeAddrees}";
            }
            else if (violation.PlaceAddressDescriptor.Equals("At"))
            {
                if(string.IsNullOrEmpty(violation.FreeAddrees)){
                    var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                    return $"{violation.PlaceHouseNo} {onStreetName?.StreetName}";
                }

                return violation.FreeAddrees;
            }
            else if (violation.PlaceAddressDescriptor.Equals("At Side Of"))
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                return $"Side Of {violation.PlaceHouseNo} {onStreetName?.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Equals("At Rear Of"))
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                return $"Rear Of {violation.PlaceHouseNo} {onStreetName?.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Contains("On/Bet C/M"))
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                var betweenStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross1StreetId, violation.PlaceBoroCode);
                var andStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceCross2StreetId, violation.PlaceBoroCode);

                return $"On the Center Median On {onStreetName?.StreetName} Between {betweenStreetName?.StreetName} and {andStreetName?.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Contains("Front Of C/M"))
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                return $"On the Center Median in Front of {violation.PlaceHouseNo} {onStreetName?.StreetName}";
            }
            else if (violation.PlaceAddressDescriptor.Equals("At") && violation.ViolationTypeId == "T")
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                return $"Inside of {violation.PlaceHouseNo} {onStreetName?.StreetName}";
            }
            else
            {
                var onStreetName = await _placeOfOccurrenceService.LookUpByStreetCode(violation.PlaceStreetId, violation.PlaceBoroCode);
                return $"{violation.PlaceHouseNo} {onStreetName?.StreetName}";
            }
        }

        public string ShowOrHidePlaceAddressDescriptor() =>
            NovInformation.PlaceAddressDescriptor.In(new[] {"Front Of", "Opposite Of", "Vacant Lot"})
                ? NovInformation.PlaceAddressDescriptor
                : "";

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();
            if (!IsRetrievingChecked)
            {
                alerts.Add(new AlertViewModel("Retrieving NOV Information Failed", "Would you like to try again?", okTitle:"Retry", cancelTitle:"Abort", okAction:RetryPrint, cancelAction:ReturnToNOVSummary, elementName: "Retrieving"));
                return Task.FromResult(alerts);
            }
            if (!IsFormattingTicketChecked)
            {
                alerts.Add(new AlertViewModel("Formatting NOV Information Failed", "Would you like to try again?", okTitle: "Retry", cancelTitle: "Abort", okAction: RetryPrint, cancelAction: ReturnToNOVSummary, elementName: "Formatting"));
                return Task.FromResult(alerts);
            }
            if (!IsConnectingChecked)
            {
                alerts.Add(new AlertViewModel("Connecting To Printer Failed", "Would you like to try again?", okTitle: "Retry", cancelTitle: "Abort", okAction: RetryPrint, cancelAction: ReturnToNOVSummary, elementName: "Connecting"));
                return Task.FromResult(alerts);
            }
            if (!IsSendingChecked)
            {
                alerts.Add(new AlertViewModel("Sending Data To Printer Failed", "Would you like to try again?", okTitle: "Retry", cancelTitle: "Abort", okAction: RetryPrint, cancelAction: ReturnToNOVSummary, elementName: "Sending"));
                return Task.FromResult(alerts);
            }
            if (IsSendingChecked)
            {
                alerts.Add(new AlertViewModel("7003", "Did the ticket print successfully?", okTitle: "Yes", cancelTitle: "No", okAction: ReturnToNOVSummary, cancelAction: RetryPrintingWithOptions, elementName: "Printing"));
                return Task.FromResult(alerts);
            }

            return Task.FromResult(alerts);
        }

        public void ReturnToNOVSummary()
        {
            GoToNovSummary?.Execute(null);
        }

        public void PrintAllTickets()
        {
            PrintAllNovs?.Execute(null);
        }

        public async void RetryPrintingWithOptions()
        {
            var alert = new AlertViewModel("Connecting To Printer Failed", "Would you like to try again?", okTitle: "Retry", cancelTitle: "Abort", okAction: RetryPrint, cancelAction: ReturnToNOVSummary, elementName: "Connecting");
            await AlertService.DisplayAlert(alert);
        }

        public async void PrintAllConfirmation(int NovNumbersCount)
        {
            var alert = new AlertViewModel("1206", String.Format("Are you sure you wish to print all {0} NOVs?", NovNumbersCount), okTitle: "Yes", cancelTitle: "No", okAction: PrintAllTickets, cancelAction: ReturnToNOVSummary, shouldContinueOnOk: true, elementName: "PrintAll");
            await AlertService.DisplayAlert(alert);
        }
        
        public void RetryPrint()
        {
            try
            { 
                ReloadScreen?.Execute(null);
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace;
            }
        }

        public List<NovMaster> NovMasterList { get; set; }

        public ICommand GoToNovSummary => new Command(async () =>
        {
            try
            { 
                await NavigationService.PopToSummaryScreenAsync();
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace;
            }
        });

        public ICommand PrintAllNovs => new Command(async () =>
        {
            try
            {
                foreach (var violation in NovMasterList)
                {
                    if (violation.NovInformation.TicketStatus == " " || violation.NovInformation.TicketStatus == "L")
                    {
                        NovMaster = violation;
                        if (MapFieldsToNovInfo.Count != 0)
                        {
                            MapFieldsToNovInfo.Clear();
                        }
                        MapFieldsToNovInfo = await MapAllFieldsToValues(MapFieldsToNovInfo);
                        IsFormattingTicketChecked = true;
                        PrinterStatus = String.Format("Connecting to {0}", NovMaster?.UserSession?.SelectedPrinter?.SerialNumber);
                        await _printer.InitiateConnection(NovMaster?.UserSession?.SelectedPrinter?.MacAddress, false, MapFieldsToNovInfo, UserSession.Department);
                    }
                }

                IsConnectingChecked = true;
                IsSendingChecked = true;
                await Task.Delay(500);
                IsLoading = false;
                NextCommand?.Execute(null);
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace.ToString();
            }
        });

        public ICommand ReloadScreen => new Command(async () =>
        {
            try
            {
                await LoadAsync();
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace.ToString();
            }
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            try
            {
                var alerts = await ValidateScreen();
                foreach (AlertViewModel alert in alerts)
                {
                    if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                    {
                        return;
                    }
                }

                await NavigationService.PopToSummaryScreenAsync();
            }
            catch (Exception ex)
            {
                var stack = ex.StackTrace.ToString();
            }
        });
    }
}
