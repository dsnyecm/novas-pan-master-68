﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.ViewModels.Utils;
using DSNY.Novas.Services;

namespace DSNY.Novas.ViewModels
{
    public class AboutNovasViewModel : ViewModelBase
    {
        private string _unitId;
        private string _userId;
        private string _softwareVersion;
        private string _lastSyncTime;
        private ILocalDeviceSettingsService _localDeviceSettingsService;
        private string _DeviceIMEI;
        private string _DeviceSerialNumber;

        public override string Title => "About NOVAS";
        public AboutNovasViewModel() { _lastSyncTime = "1900-01-01 00:00:00"; }
        public override async Task LoadAsync()
        {
            _localDeviceSettingsService = DependencyResolver.Get<ILocalDeviceSettingsService>();

            var syncTimeSetting = await _localDeviceSettingsService.GetLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey);
            if (syncTimeSetting != null)
                LastSyncTime = syncTimeSetting.Value;

            UnitID = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            UserID = UserSession.UserId;
            SoftwareVersion = DependencyResolver.Get<IAppRuntimeSettings>().Version;
            DeviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            DeviceSerialNumber = DependencyResolver.Get<IAppRuntimeSettings>().DeviceSerialNumber;
            return;
        }
        public string Date
        {
            get => String.Format(DateTime.Now.ToString("MM/dd/yyyy"));
        }

        public string Time
        {
            get => String.Format(DateTime.Now.ToString("hh:mm tt"));
        }

        public string LastSyncTime
        {
            get => String.Format(_lastSyncTime);
            set
            {
                _lastSyncTime = value; NotifyPropertyChanged();
            }
        }

        public string UnitID
        {
            get => _unitId;
            set { _unitId = value; NotifyPropertyChanged(); }
        }

        public string UserID
        {
            get => _userId;
            set { _userId = value; NotifyPropertyChanged(); }
        }

        public string SoftwareVersion
        {
            get => _softwareVersion;
            set { _softwareVersion = value; NotifyPropertyChanged(); }
        }

        public string BatteryPercentage
        {
            get => Plugin.Battery.CrossBattery.Current.RemainingChargePercent + "%";
        }

        public string DeviceIMEI
        {
            get => _DeviceIMEI;
            set { _DeviceIMEI = value; NotifyPropertyChanged(); }
        }
        public string DeviceSerialNumber
        {
            get => _DeviceSerialNumber;
            set { _DeviceSerialNumber = value; NotifyPropertyChanged(); }
        }
        public override bool ShowMenuButton => false;

        public override bool ShowNextButton => false;

        public override ICommand BackCommand => new Command(async () => {
            await NavigationService.PopModalAsync();
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            await NavigationService.PopModalAsync();
        });
    }
}
