﻿using System;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;

namespace DSNY.Novas.ViewModels
{
    public class ViolationDetailsViewModel : ViewModelBase
    {
        private string _violationScript;
        private Dictionary<string, string> _violationScriptTextReplacements;
        private Dictionary<string, string> _novDataFieldMappings;
        private readonly IViolationDetailsService _violationDetailsService;
        private string _violationReplacedScript;
        private string _property;
        private readonly INovService _novService;
        private List<ViolationTypes> _violationTypes;
        private ViolationTypes violationType;
        private readonly IPlaceOfOccurrenceService _placeofOccurrenceService;
        private string _residentialOwner;

        public ViolationDetailsViewModel()
        {
          _violationDetailsService = DependencyResolver.Get<IViolationDetailsService>();
          _novService = DependencyResolver.Get<INovService>();
          _placeofOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
        }

        public override string Title
        {
            get
            {
                var title = "Violation Details";
                if (NovMaster.NovInformation.TicketStatus == "C")
                {
                    title = "Violation Details - Cancel";
                }
                else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    title = "Violation Details - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }

        }

        private async Task<string> GetPropertyName()
        {
            ViolationTypes = await _novService.GetViolationTypes(NovMaster.ViolationGroup.ViolationGroupId);
            violationType = ViolationTypes.Where(v => v.ViolationTypeId == (NovMaster.NovInformation.ViolationTypeId ?? v.ViolationTypeId)).FirstOrDefault();

            return $"Property: {(violationType != null ? violationType.Name : NovMaster.ViolationGroup.TypeName)}";
        }

        public override async Task LoadAsync()
        {
            try
            {

            
                ViolationScript = await GetCodeScript();
            

                ViolationScript = ViolationScript.Replace("\r\n", string.Empty);

                string repScript = ". (hidden alignment command)";
                string repScript2 = ".(hidden alignment command)";

                string formatedScript = ViolationScript.Replace(repScript, "").Replace(repScript2, "").Replace("(hidden carriage rtn command)", Environment.NewLine);
                //string formatedScript = ViolationScript.Replace(repScript, "").Replace("(hidden carriage rtn command)", "");


                ViolationScript = formatedScript;
                // string ss = ". (hidden alignment command) (number of repeat offenses) offenses (revocation or suspension)"+ Environment.NewLine +"At T/P/O I did observe respondent, (Licensed UnLicensed Dealer) retail dealer, in possession of (number amount) cigarettes in (number amount) packages/ (number amount)  loose cigarettes which are not in packages that bear valid tax stamps in violation of Ad. Code §17-703.2 [a].";


                _novDataFieldMappings = await _violationDetailsService.GetNovDataFieldMappings();
                Property = await GetPropertyName();

                var replacementStrings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

                var tokens = HyperlinkedTextLabelUtil.GetScriptTokens(ViolationScript, HyperlinkedTextLabelUtil.TokenType.Dynamic);
                var routingTimeToken = tokens.FirstOrDefault(t => t.Item3 == "(Routing Time)");
                //if (routingTimeToken != null && !String.IsNullOrEmpty(NovMaster.NovInformation.RoutingTime))
                // BuildViolationDetails(routingTimeToken.Item2, NovMaster.NovInformation.RoutingTime);
                // NH-1192
                if (routingTimeToken != null)
                {
                    var newPopOutRoutingTime = CrossSettings.Current.GetValueOrDefault("ViolationDetails_RoutingTimeOverride_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);

                    if (string.IsNullOrEmpty(newPopOutRoutingTime) && !String.IsNullOrEmpty(NovMaster.NovInformation.RoutingTime)) {
                        BuildViolationDetails(routingTimeToken.Item2, NovMaster.NovInformation.RoutingTime);
                    } else {
                        BuildViolationDetails(routingTimeToken.Item2, newPopOutRoutingTime);
                    }
                    
                }

                foreach (var novData in NovMaster.NovData)
                {
                    if (!replacementStrings.ContainsKey(novData.FieldName))
                    {
                        replacementStrings.Add(novData.FieldName, novData.Data);
                    }
                }

                ViolationScriptTextReplacements = replacementStrings;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

    public string Group => $"Group: {NovMaster.ViolationGroup.GroupAndType}";

    // public string Property => $"Property: {NovMaster.ViolationGroup.TypeName}"; //To add ViolationType.Name to this Property value 

    public string SecSub => $"Sec/Sub: {NovMaster.NovInformation.LawSection}";

    public string Short => $"Short: {NovMaster.NovInformation.HHTIdentifier}";

    public string Code => $"Code: {NovMaster.NovInformation.ViolationCode}";  //1-23-18 According to Tamara, ViolationCode should be used and not PrintViolationCode 

    public string ViolationScript
    {
      get => _violationScript;
      set { _violationScript = value; NotifyPropertyChanged(); }
    }

    public string ViolationReplacedScript
    {
      get => _violationReplacedScript;
      set { _violationReplacedScript = value; NotifyPropertyChanged(); }
    }

    public string Property
    {
      get => _property;
      set { _property = value ?? GetPropertyName().Result; NotifyPropertyChanged(); }
    }

    public List<ViolationTypes> ViolationTypes
    {
      get => _violationTypes;
      set { _violationTypes = value; NotifyPropertyChanged(); }
    }

    public string ResidentialOwner
    {
      get => _residentialOwner;
      set { _residentialOwner = value; NotifyPropertyChanged(); }
    }

    public Dictionary<string, string> ViolationScriptTextReplacements
    {
      get => _violationScriptTextReplacements;
      set { _violationScriptTextReplacements = value; NotifyPropertyChanged(); }
    }

    public ICommand ViolationTextClickedCommand
    {
      get => new Command(async (_) => await ShowDetailsPopup(_ as string));
    }

    public async Task ShowDetailsPopup(string placeholder)
    {
        ViewModelBase popout = null;

        var placeholderKey = placeholder.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[0].Replace("(", "").Replace(")", "");
        var fieldInfo = await _violationDetailsService.GetViolationScriptVariables(placeholderKey);

        placeholderKey = fieldInfo.FieldName;
        fieldInfo.FieldName = placeholder;

        if (fieldInfo.IsFreeForm.Equals("Y"))
        {
            popout = new ViolationDetailsFreeformPopoutViewModel(fieldInfo) { NovMaster = NovMaster };
        }
        else if (fieldInfo.IsFreeForm.Equals("M"))
        {
            popout = new ViolationDetailsFreeformPopoutViewModel(fieldInfo) { NovMaster = NovMaster };
        }
        else
        {
            var choices = await _violationDetailsService.GetViolationScriptVariableData(placeholderKey.Trim());
            popout = new ViolationDetailsListPopoutViewModel(NovMaster, fieldInfo, choices);
        }

        await NavigationService.PushModalAsync(popout);
    }

    public async Task<string> GetCodeScript()
    {
        return await _violationDetailsService.GetCodeScript(NovMaster.NovInformation.ViolationCode,
          NovMaster.NovInformation.HHTIdentifier, NovMaster.NovInformation.ViolationGroupId);
    }

    public async Task<ViolationScriptVariables> GetViolationScriptVariables(string description)
    {
      return await _violationDetailsService.GetViolationScriptVariables(description);
    }

    public async Task<List<ViolationScriptVariableData>> GetViolationScriptVariableData(string fieldName)
    {
      return await _violationDetailsService.GetViolationScriptVariableData(fieldName);
    }

    public override List<string> CancelMenuItems
    {
      get
      {
        return new List<string> { "Void" };
      }
    }

    public override async Task<List<AlertViewModel>> ValidateScreen()
    {
        var tokens = HyperlinkedTextLabelUtil.GetScriptTokens(ViolationScript, HyperlinkedTextLabelUtil.TokenType.Dynamic);
        var maxLengthForScriptString = 410;

        var violationScriptTextReplacementKeys = ViolationScriptTextReplacements.Select(_ => _.Key).ToList();
        var indices = tokens.Where(t => !violationScriptTextReplacementKeys.Any(r => r == t.Item2)).ToList();

        var alerts = new List<AlertViewModel>();
        foreach (KeyValuePair<string, string> replacements in ViolationScriptTextReplacements)
        {
        if (String.IsNullOrEmpty(replacements.Value))
        {
            alerts.Add(new AlertViewModel("12045", (String.Format(WorkFlowMessages.DSNYMSG_23_MissingVariable, replacements.Key)), elementName: "ViolationScript"));
        }
        }
        if (indices.Count != 0)
        {
            alerts.Add(new AlertViewModel("12045", (String.Format(WorkFlowMessages.DSNYMSG_23_MissingVariable, indices.First().Item3)), elementName: "ViolationScript"));
        }

        if (ViolationReplacedScript.Count() > maxLengthForScriptString)
        {
            alerts.Add(new AlertViewModel("12074", (String.Format(WorkFlowMessages.DSNYMSG_23_SentenceLength, ViolationReplacedScript.Count() - maxLengthForScriptString, maxLengthForScriptString)), elementName: "ViolationScript"));
        }

            // fix for address validataion-- Address can  be in range of lowhouseno and highhuseno  and  will be considered all three input form address screen.
            //ResidentialOwner = (await _placeofOccurrenceService.GetPropertyDetails(NovMaster.NovInformation.PlaceStreetId, NovMaster.NovInformation.PlaceHouseNo, NovMaster.NovInformation.PlaceBoroCode))?.FirstName;
            // bug #518 to get propety details based on BBL

            string nonNumericHousesString = "S/W/C,N/E/C,N/W/C,S/E/C";
            var nonNumericHouses = nonNumericHousesString.Split(',');

            if (NovMaster.NovInformation.PlaceAddressDescriptor == "V")
            {
                var bblAddresss = (await _placeofOccurrenceService.GetAllBBLNumbers(NovMaster.NovInformation.PlaceBBL)).Find(_ => _.VacantLot.Equals("V"));
                if (bblAddresss != null)
                {
                    ResidentialOwner = bblAddresss.FirstName;
                }
            }
            //else if (!NovMaster.NovInformation.PlaceHouseNo1.Equals("S/W/C") && !NovMaster.NovInformation.PlaceHouseNo1.Equals("N/E/C") && !NovMaster.NovInformation.PlaceHouseNo1.Equals("N/W/C") && !NovMaster.NovInformation.PlaceHouseNo1.Equals("S/E/C"))
            else if (Convert.ToString(NovMaster.NovInformation.PlaceHouseNo1).All(Char.IsDigit))
            {
                ResidentialOwner = (await _placeofOccurrenceService.GetPropertyDetails(NovMaster.NovInformation.PlaceStreetId, NovMaster.NovInformation.PlaceHouseNo1, NovMaster.NovInformation.PlaceHouseNo2, NovMaster.NovInformation.PlaceHouseIndetifier, NovMaster.NovInformation.PlaceBoroCode))?.FirstName;
            }

            return alerts;
    }

    public override void WriteFieldValuesToNovMaster()
    {
        NovMaster.NovInformation.ViolationScript = ViolationReplacedScript;
  
        //NH-1406
        if ((!string.IsNullOrEmpty(ResidentialOwner) && NovMaster.NovInformation.ViolationGroupId != 1)  && ( NovMaster.NovInformation.ViolationGroupId != 4))
        {
            NovMaster.NovInformation.Resp1FirstName = ResidentialOwner;
        }

        _novService.DeleteNovInfo();
        _novService.UpdateNovData(NovMaster);

        // NH-1192
        var newPopOutRoutingTime = CrossSettings.Current.GetValueOrDefault("ViolationDetails_RoutingTimeOverride_" + NovMaster.NovInformation.NovNumber.ToString(), string.Empty);            
        if (!string.IsNullOrEmpty(newPopOutRoutingTime)) {
            NovMaster.NovInformation.RoutingTime = newPopOutRoutingTime;
        }

        if (IsVoidAction)
        {
            NovMaster.NovInformation.TicketStatus = "V";
        }

        NovMaster.NovInformation.VoidSet = true;
    }
    public void BuildViolationDetails(string fieldName, string data)
    {
        var novData = new NovData
        {
            NovNumber = NovMaster.NovInformation.NovNumber,
            Data = data,
            FieldName = fieldName
        };

            // NH-1192
            NovData fieldExistsInNovData = NovMaster.NovData.FirstOrDefault(_ => _.FieldName == fieldName);
            var dataIndex = NovMaster.NovData.IndexOf(fieldExistsInNovData);
            if (dataIndex != -1)
                NovMaster.NovData.RemoveAt(dataIndex);

            NovMaster.NovData.Add(novData);
    }
    public override void WriteValueToCrossSettings()
        {
            CrossSettings.Current.AddOrUpdateValue("ViolationDetails_IsVisited_" + NovMaster.NovInformation.NovNumber.ToString(), Boolean.TrueString);
        }
    }
}
