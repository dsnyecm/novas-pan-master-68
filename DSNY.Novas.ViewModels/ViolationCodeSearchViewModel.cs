﻿using System;
using System.Collections.Generic;
using System.Linq;
using DSNY.Novas.Services;
using System.Collections.ObjectModel;
using DSNY.Novas.Models;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.ViewModels.Utils;
using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels
{
    public class ViolationCodeSearchViewModel : ViewModelBase
    {
        //Vars
        private List<ViolationDetails> _searchResults;
        private string _searchWord;
        private ViolationDetails _selectedViolationDetail;
        private NovMaster _novMaster;

        private readonly INovService _violationDetailsService;
        //Commands
        public ICommand ViolationDetailsSearchCommand { get; set; }
        public ICommand ViolationDetailsSelectedCommand { get; set; }
        //Property
        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public override string Title
        {
            get
            {
                var title = "Violation Code Search";
                if (IsCancelled || Nov?.NovInformation.TicketStatus == "C")
                {
                    title = "Violation Code Search - Cancel";
                }
                else if (NovMaster?.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    title = "Violation Code Search - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }
        }
        public ViolationCodeSearchViewModel()
        {
            _violationDetailsService = DependencyResolver.Get<INovService>();
            ViolationDetailsSearchCommand = new Command<ViolationDetails>(async _ => await ExecuteSearch());
        }

        public override Task LoadAsync()
        {
            NotifyPropertyChanged(nameof(Title));
            return Task.CompletedTask;
        }

        public async Task ExecuteSearch()
        {

            if (!string.IsNullOrEmpty(SearchWord))
            {
                var results = await _violationDetailsService.GetViolationDetailsByGroupAndNameAndSearch(GroupId, GroupName, SearchWord);
                SearchResults = new List<ViolationDetails>(results);
            }
            else
            {
                SearchResults = null;
            }

            //reset option to null, since no item could currently be selected from list
            SelectedViolationDetail = null;
        }

        public List<ViolationDetails> SearchResults
        {
            get => _searchResults; 
            set { _searchResults = value; NotifyPropertyChanged(); }
        }

        public string SearchWord
        {
            get => _searchWord;
            set { _searchWord = value; NotifyPropertyChanged();  }
        }

        public ViolationDetails SelectedViolationDetail
        {
            get => _selectedViolationDetail;
            set { _selectedViolationDetail = value; NotifyPropertyChanged(); }
        }

        public NovMaster Nov
        {
            get => _novMaster;
            set { _novMaster = value; NotifyPropertyChanged(); }
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public override ICommand NextCommand => new Command(async () =>
        {
            if (SelectedViolationDetail == null)
                return;

            Nov.NovInformation.ViolationGroupId = (int) SelectedViolationDetail.ViolationGroupId;
            Nov.NovInformation.ViolGroupName = SelectedViolationDetail.CodeLaw;


            Nov.NovInformation.ViolationCode = SelectedViolationDetail?.ViolationCode;
            Nov.NovInformation.HHTIdentifier = SelectedViolationDetail?.HHTIdentifier;
            await NavigationService.PopModalAsync();
        });
    }
}
