﻿using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DSNY.Novas.ViewModels
{
    public class TimeoutViewModel : ViewModelBase
    {
        private readonly ILoginService _loginService;
        private readonly INFCReader _nfcReader;
        private bool _isPinEntryVisible;
        private bool _isInstructionTextVisible;
        private string _pin;
        private string _userName; //UserAuthenticationStatus
        private string _userAuthenticationStatus;
        public override string Title => "Timeout Screen";

        public TimeoutViewModel()
        {
            _loginService = DependencyResolver.Get<ILoginService>();
            _nfcReader = DependencyResolver.Get<INFCReader>();

            IsInstructionTextVisible = false;
            IsPinEntryVisible = false;

            UserName = "To Unlock Screen:";
            UserAuthenticationStatus = "";
        }

        public override async Task LoadAsync()
        {
            // NH-1314
            //var previousLoginUserId = getLoginUser();
            //if (!String.IsNullOrEmpty(previousLoginUserId))
            //{
            //    UserName = "User Id : " + previousLoginUserId;
            //}
            await base.LoadAsync();


            UserName = "User Id : " + UserSession.UserId;

            IsPinEntryVisible = false;
            PIN = "";
            if (!await _nfcReader.IsNFCAvailable())
            {
                await AlertService.DisplayAlert(new AlertViewModel(string.Empty, "NFC is not supported on this device.  You will not be able to log in."));
                return;
            }

            if (!await _nfcReader.IsNFCEnabled())
            {
                await AlertService.DisplayAlert(new AlertViewModel(string.Empty, "NFC is not enabled.  You must enable NFC reading / sharing in Settings."));
                await _nfcReader.NavigateToNFCSettings();
                return;
            }

            await _nfcReader.StartListening();

            IsInstructionTextVisible = true;
            IsPinEntryVisible = true;

            _nfcReader.CardRecognizedCommand = new Command(async () =>
            {
                IsInstructionTextVisible = false;
                IsPinEntryVisible = true;

                UserName = await _loginService.GetUserNameFromCardID(_nfcReader.CurrentCardUID);

            });
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            _nfcReader.StopListening();
        }


        public bool IsPinEntryVisible
        {
            get => _isPinEntryVisible;
            set { _isPinEntryVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsInstructionTextVisible
        {
            get => _isInstructionTextVisible;
            set { _isInstructionTextVisible = value; NotifyPropertyChanged(); }
        }


        public string PIN
        {
            get => _pin;
            set { _pin = value; NotifyPropertyChanged(); }
        }


        public ICommand DepartmentSelectedCommand => new Command(async () =>
        {

        });
        //ExitEnterPressed
        public ICommand ExitEnterPressed => new Command(async () =>
        {
                ForceLogOff();
                NavigationService.PopModalAsync();
            
        });
        public ICommand PINEnteredCommand => new Command(async () =>
        {
            var userID = await AuthenticateUser(PIN, _nfcReader.CurrentCardUID);

            IsInstructionTextVisible = true;
            IsPinEntryVisible = true;

            //UserName = await _loginService.GetUserNameFromCardID(_nfcReader.CurrentCardUID);
            var ScannedUserName = await _loginService.GetUserNameFromCardID(_nfcReader.CurrentCardUID);
            var ScannedUserID = await _loginService.GetUserIDFromCardID(_nfcReader.CurrentCardUID);

            // NH-1314
            bool isScannedUserSamePreviousUser = false;
            //var previousLoginUserId = getLoginUser();

          
            if (!String.IsNullOrEmpty(UserSession.UserId))
            {                
                // DEBUG - uncomment the following line
                //UserName = "User Id : " + previousLoginUserId + " ; Card User Id : " + ScannedUserName;
                if (UserSession.UserId.Equals(ScannedUserID, StringComparison.OrdinalIgnoreCase))
                    isScannedUserSamePreviousUser = true;

            }

            if (!String.IsNullOrEmpty(userID) && isScannedUserSamePreviousUser)
            {
                //UserSession.TimeoutTimeStamp = DateTime.Now;
                if (UserSession != null)
                {
                    UserSession.TimeoutTimeStamp = DateTime.Now;
                }
                else if (NovMaster != null && NovMaster?.UserSession != null)
                {
                    NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
                }
                await NavigationService.PopModalAsync();
            }
            else
            {
                PIN = string.Empty;
                await AlertService.DisplayAlert(new AlertViewModel(string.Empty, "Invalid password."));
                if (!isScannedUserSamePreviousUser)
                    UserAuthenticationStatus = "User Id : " + UserSession.UserId;
                else if (String.IsNullOrEmpty(UserName))
                    UserAuthenticationStatus = "Unable to find user. Please try again.";
                else
                    UserAuthenticationStatus = "Invalid password. Please try again.";
            }
        });

        public async Task<bool> IsValidUser(string userId)
        {
            var isValid = await _loginService.IsValidUser(userId);
            return isValid;
        }

        public async Task<string> AuthenticateUser(string pin, string nfcUid)
        {
            var userIdReturned = await _loginService.GetUserIdFromCardID(pin, nfcUid);
            if (!String.IsNullOrEmpty(userIdReturned))
            {
                LoginTimestamp = DateTime.Now;
                NeedNotCheckLogOff = false;
                return userIdReturned;
            }
            return "";
        }
        public string UserName
        {
            get => _userName;
            set
            {
                if (_userName?.Equals(value) == true)
                    return;

                _userName = value;
                NotifyPropertyChanged();
            }
        }

        public string UserAuthenticationStatus
        {
            get => _userAuthenticationStatus;
            set
            {
                if (_userAuthenticationStatus?.Equals(value) == true)
                    return;

                _userAuthenticationStatus = value;
                NotifyPropertyChanged();
            }
        }
        //private String getLoginUser()
        //{
        //    var loginUserId = CrossSettings.Current.GetValueOrDefault("Login_UserId_", "");
        //    return loginUserId;
        //}
    }
}
