﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;

using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels
{
    public class ViolationDetailsFreeformPopoutViewModel : ViewModelBase
    {
        private ViolationScriptVariables _field;
        private string _enteredValue = "";
        private DateTime _enteredDate = DateTime.Now;
        private TimeSpan _enteredTime = DateTime.Now.TimeOfDay;
        private bool _showTextField;
        private bool _showDatePicker;
        private bool _showTimePicker;
        private bool _showInputDescription;
        private bool _shouldDisplayInputFormat;
        private InputType _inputType;
        private string _title;

        public override string Title => _title;
        public string InputDescription => ShouldDisplayInputFormat ? "Format: " + Field.Mask : Field.Mask.Length > 0 ? "Character Limit: " + (Field.Mask.Length - EnteredValue.Length) : "";

        public ViolationDetailsFreeformPopoutViewModel(ViolationScriptVariables field)
        {
            Field = field;
            _title = FormatTitle(Field.Description);
            ShowInputDescription = true;
            ShowTextField = true;
            // NovMaster.NovData
           

            if (field.Mask == null) // Some of the freeform values in DBViolationScriptVariables don't have a mask set...
            {
                field.Mask = "";
                ShowInputDescription = false;
            }

            ShouldDisplayInputFormat = Field.Mask == "##/##/####" || Field.Mask == "####" || Field.Mask == "##:## AA";
            InputType = Field.Mask.Contains("#") && !Field.Mask.Contains("A") ? InputType.Numeric : InputType.Alphanumeric;

            if (Field.Mask == "##/##/####")
            {
                ShowDatePicker = true;
                ShowTextField = false;
                ShowInputDescription = false;
            }

            if (field.Mask == "##:## AA")
            {
                ShowTimePicker = true;
                ShowTextField = false;
                ShowInputDescription = false;
            }
        }

        public override async Task LoadAsync()
        {
            if (NovMaster.NovData.Count() > 0 && (Field.IsFreeForm=="Y" || Field.IsFreeForm == "M"))
            {
                var oldValue = NovMaster.NovData.Where(m => m.FieldName.Contains(Field.Description)).ToList();
                if(oldValue.Count()>0)
                    EnteredValue = oldValue.FirstOrDefault().Data;
            }

            
        }

        public ViolationScriptVariables Field
        {
            get => _field;
            set { _field = value; NotifyPropertyChanged(); }
        }

        public string EnteredValue
        {
            get => _enteredValue;
            set
            {
                _enteredValue = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(InputDescription));
            }
        }

        public DateTime EnteredDate
        {
            get => _enteredDate;
            set { _enteredDate = value; NotifyPropertyChanged(); }
        }

        public TimeSpan EnteredTime
        {
            get => _enteredTime;
            set { _enteredTime = value; NotifyPropertyChanged(); }
        }

        public bool ShowTextField
        {
            get => _showTextField;
            set { _showTextField = value; NotifyPropertyChanged(); }
        }

        public bool ShowDatePicker
        {
            get => _showDatePicker;
            set { _showDatePicker = value; NotifyPropertyChanged(); }
        }

        public bool ShowTimePicker
        {
            get => _showTimePicker;
            set { _showTimePicker = value; NotifyPropertyChanged(); }
        }

        public bool ShowInputDescription
        {
            get => _showInputDescription;
            set { _showInputDescription = value; NotifyPropertyChanged(); }
        }

        public bool ShouldDisplayInputFormat
        {
            get => _shouldDisplayInputFormat;
            set { _shouldDisplayInputFormat = value; NotifyPropertyChanged(); }
        }

        public InputType InputType
        {
            get => _inputType;
            set { _inputType = value; NotifyPropertyChanged(); }
        }

        public void TextChanged(string newVal, string oldVal)
        {
            if (string.IsNullOrWhiteSpace(Field.Mask))
            {
                return; // We can't do any validation on freeform values that don't have a mask
            }

            if (oldVal == null)
            {
                oldVal = "";
            }

            if (newVal.Length > Field.Mask.Length)
            {
                EnteredValue = newVal.Remove(Field.Mask.Length);
                return;
            }

            switch (Field.Mask)
            {
                case "##/##/####":
                    newVal = Regex.Replace(newVal, @"[^\d|/]", string.Empty);
                    if (oldVal.Length < newVal.Length)
                    {
                        if (Regex.IsMatch(newVal, @"^\d\d$") || Regex.IsMatch(newVal, @"^\d\d/\d\d$"))
                        {
                            newVal += '/';
                        }
                    }
                    else
                    {
                        if ((Regex.IsMatch(newVal, @"^\d\d/$") && Regex.IsMatch(oldVal, @"^\d\d/\d$"))
                            || (Regex.IsMatch(newVal, @"^\d\d/\d\d/$") && Regex.IsMatch(oldVal, @"^\d\d/\d\d/\d$")))
                        {
                            newVal = newVal.Remove(newVal.Length - 1);
                        }
                    }

                    EnteredValue = newVal;
                    return;

                case "####":
                    newVal = Regex.Replace(newVal, @"[^\d]", string.Empty);
                    EnteredValue = newVal;
                    return;

                case "##:## AA":
                    newVal = Regex.Replace(newVal, @"[^\d|a|A|p|P|m|M|\s|:]", string.Empty);
                    if (oldVal.Length < newVal.Length)
                    {
                        if (Regex.IsMatch(newVal, @"^\d\d$"))
                        {
                            newVal += ':';
                        }
                        if (Regex.IsMatch(newVal, @"^\d\d:\d\d$"))
                        {
                            newVal += ' ';
                        }
                    }
                    else
                    {
                        if (Regex.IsMatch(newVal, @"^\d\d:$") && Regex.IsMatch(oldVal, @"\d\d:\d$"))
                        {
                            newVal = newVal.Remove(newVal.Length - 1);
                        }
                        if (Regex.IsMatch(newVal, @"^\d\d:\d\d $") && Regex.IsMatch(oldVal, @"\d\d:\d\d [A|a|P|p]"))
                        {
                            newVal = newVal.Remove(newVal.Length - 1);
                        }
                    }

                    EnteredValue = newVal;
                    return;
            }
        }

        private string FormatTitle(string originalTitle)
        {
            var wordSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "of", "on", "in", "and", "the", "for" };
            var separators = new List<string> { " ", "/", "-" };
            var title = originalTitle;
            foreach (var separator in separators)
            {
                var titleComponents = title.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
                var newComponents = new List<string>();
                foreach (var component in titleComponents)
                {
                    if (!wordSet.Contains(component))
                    {
                        if (component.Length > 1)
                        {
                            newComponents.Add(component.Substring(0, 1).ToUpper() + component.Substring(1));
                        }
                        else
                        {
                            newComponents.Add(component.ToUpper());
                        }
                    }
                    else
                    {
                        newComponents.Add(component);
                    }
                }

                title = string.Join(separator, newComponents);
            }

            return title;
        }

        public override bool ShowActionMenu => false;

        public override bool ShowCancelMenu => false;

        public override bool ShowBackButton => false;

        public override bool ShowCancelButton => true;

        public override bool ShowNextButton => false;

        public override bool ShowConfirmButton => true;

        public override ICommand BackCommand => new Command(async () => await NavigationService.PopModalAsync());

        public override ICommand NextCommand => new Command(async () =>
        {
            WriteFieldValuesToNovMaster();
            await NavigationService.PopModalAsync();
        });

        public override void WriteFieldValuesToNovMaster()
        {
            //var novData = NovMaster.NovData;
            //NovData toDelete = novData.FirstOrDefault(_ => _.FieldName == Field.FieldName);
            //if (toDelete != null)
            //{
            //    novData.Remove(toDelete);
            //}            
            NovData fieldExistsInNovData = NovMaster.NovData.FirstOrDefault(_ => _.FieldName == Field.FieldName);
            var dataIndex = NovMaster.NovData.IndexOf(fieldExistsInNovData);
            if (dataIndex != -1)
                NovMaster.NovData.RemoveAt(dataIndex);


            if (ShowDatePicker && EnteredDate != null)
            {
                NovMaster.NovData.Add(new NovData { FieldName = Field.FieldName, Data = EnteredDate.ToString("MM/dd/yyyy") });
            }
            else if (ShowTimePicker && EnteredTime != null)
            {
                NovMaster.NovData.Add(new NovData { FieldName = Field.FieldName, Data = DateTime.Today.Add(EnteredTime).ToString("hh:mm tt") });
            }
            else if (EnteredValue != null)
            {
                NovMaster.NovData.Add(new NovData { FieldName = Field.FieldName, Data = EnteredValue });
            }
        }
    }
}
