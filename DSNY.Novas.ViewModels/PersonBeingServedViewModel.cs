﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using System;
using Plugin.Settings;
using System.Windows.Input;
using DSNY.Novas.ViewModels.Utils;

namespace DSNY.Novas.ViewModels
{
    public class PersonBeingServedViewModel : ViewModelBase
    {
        private string _firstName;
        private string _lastName;
        private string _licenseId;
        private string _tradeName;
        private string _middleInitial;
        private string _streetName;
        private string _streetNumber;
        private string _apt;
        private List<LookupTable> _sexes;
        private LookupTable _sexSelectedItem;
        private List<BoroMaster> _boros;
        private BoroMaster _boroSelectedItem;
        private bool _isBoroEnabled;
        private string _city;
        private List<LookupTable> _states;
        private LookupTable _stateSelectedItem;
        private string _zipCode;
        private string _businessName;
        private bool _isNameChecked;
        private bool _isBusinessCheckVisible;
        private bool _isBusinessNameVisible;
        private bool _isLicenseNumberVisibile;
        private bool _areNameFieldsVisible;
        private readonly ILookupService _lookupService;
        private readonly IPersonBeingServedService _personBeingServedService;
        private readonly INovService _novService;
        private bool _isEnabled;
        private bool _hasCompletedInitialLoad;
        private string nextNovNum;
        private string showVoid = "";
        private bool _tradeNameVisible;

        private readonly IPlaceOfOccurrenceService _placeOfOccurrenceService;

        public PersonBeingServedViewModel()
        {
            IsNameChecked = true; // Default to checked here
            _lookupService = DependencyResolver.Get<ILookupService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();
            _novService = DependencyResolver.Get<INovService>();

            _placeOfOccurrenceService = DependencyResolver.Get<IPlaceOfOccurrenceService>();
        }

        public override string Title
        {
            get
            {
                var title = "Person Being Served";
                if (IsCancelled || NovMaster.NovInformation.TicketStatus == "C")
                {
                    title = "Person Being Served - Cancel";
                }
                else if (NovMaster.NovInformation.TicketStatus == "V" || IsVoidAction)
                {
                    title = "Person Being Served - Void";
                }
                NotifyPropertyChanged(nameof(ShowCancelMenu));
                NotifyPropertyChanged(nameof(MenuItems));
                return title;
            }
        }

        public override async Task LoadAsync()
        {
            nextNovNum = (await _novService.GetNextNovNumber(true)).ToString();
            if (nextNovNum != null)
            {
                showVoid = CrossSettings.Current.GetValueOrDefault("ShowVoid_" + nextNovNum, string.Empty);
            }

            NotifyPropertyChanged(nameof(Title));
            NotifyPropertyChanged(nameof(ShowCancelMenu));
            NotifyPropertyChanged(nameof(CancelMenuItems));

            if (NovMaster.NovInformation.LockPreviousScreens)
            {
                IsEnabled = false;
                IsBoroEnabled = false;
            }
            else
            {
                IsEnabled = true;
            }

            if (NovMaster.UserSession.Department == SHERIFF_DEP)
                TradeNameVisible = true;
            else
                TradeNameVisible = false;

            if (_hasCompletedInitialLoad)
            {
                return;
            }

            if (Boros == null) { Boros = await _personBeingServedService.GetBoros(); }
            if (Sexes == null) { Sexes = await _lookupService.GetLookupTable("SEX"); }
            if (States == null) { States = await _lookupService.GetLookupTable("STATE"); }

            var novInfo = NovMaster.NovInformation;

            FirstName = novInfo.Resp1FirstName;
            MiddleInitial = novInfo.Resp1MiddleInitial;
            LastName = novInfo.Resp1LastName;
            SexSelectedItem = Sexes.Find((item) => item.Code.Equals(novInfo.Resp1Sex));

            if(NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                IsLicenseNumberVisible = true;
                IsBusinessCheckVisible = false;
            }
            else
            {
                IsBusinessCheckVisible = true;
                IsLicenseNumberVisible = false;
            }
            
            //default behavior is to have it checked; except for VacantLot, Posting, and DCodes
            IsNameChecked = novInfo.ViolationTypeId != "O";
            //If a Make Similar or returning from the next screen, it should match how it was saved
            if (!String.IsNullOrWhiteSpace(novInfo.BusinessName)) //check if we already have information about the ticket
            {
                IsNameChecked = !novInfo.IsBusiness;
            }
            BusinessName = novInfo.BusinessName;

            if (novInfo.ViolationGroupId == 4) // Posting
            {
                if (!novInfo.IsBusiness && !String.IsNullOrWhiteSpace(novInfo.Resp1FirstName)) {
                    IsNameChecked = true;
                }
            }
            
            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                IsNameChecked = false;
                if (!string.IsNullOrEmpty(novInfo.LicenseNumber))
                    LicenseId = novInfo.LicenseNumber;
            }

            StreetNumber = novInfo.Resp1HouseNo;
            StreetName = novInfo.Resp1Address;
            Apt = novInfo.Resp1Address1;

            var boroCode = string.IsNullOrWhiteSpace(novInfo.Resp1BoroCode)
                ? novInfo.PlaceBoroCode
                : novInfo.Resp1BoroCode;

            // NH-1465 2018-11-21
           // if (string.IsNullOrWhiteSpace(boroCode))
           //     boroCode = NovMaster.UserSession.DefaultBoroCode;

            if (boroCode!=null)
                BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(boroCode));
            City = novInfo.Resp1City;
            if (novInfo.Resp1State != null)
            {
                StateSelectedItem = States.Find((state) => state.Code.Equals(novInfo.Resp1State));
            }
            else
            {
                StateSelectedItem = States.Find((state) => state.Code.Equals("NY"));
            }
            ZipCode = novInfo.Resp1Zip;

            _hasCompletedInitialLoad = true;
        }


        public bool IsLicenseNumberVisible
        {
            get => _isLicenseNumberVisibile;
            set { _isLicenseNumberVisibile = value; NotifyPropertyChanged(); }
        }

        public string BusinessName
        {
            get => _businessName;
            set { _businessName = value; NotifyPropertyChanged(); }
        }

        private async void getLicDetails()
        {
            var prop = await _placeOfOccurrenceService.GetPropertyDetailsByLicenseId(_licenseId);

            if (prop != null)
            {
                BusinessName = prop.BusinessName;
                TradeName = prop.TradeName;
                StreetNumber = prop.MailingBuildingNumber;
                StreetName = GetStreetName(prop.MailingStreetName);
                City = prop.MailingCity;
                
                var B = prop.Bbl.ToString();
                if (!string.IsNullOrEmpty(B))
                    NovMaster.NovInformation.PlaceBoroCode = B.Substring(0, 1);
                ZipCode = Convert.ToString(prop.MailingZipCode);
                StateSelectedItem = States.Find((state) => state.Code.Equals(prop.MailingState));
                if (StateSelectedItem.Code == "NY")
                {
                    BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(B.Substring(0, 1)));
                    var bc = await _placeOfOccurrenceService.GetBoroCodeByStreet(StreetName);
                    if (bc != null)
                    {
                        foreach (var item in bc)
                        {
                            var value = await _placeOfOccurrenceService.GetBBLNumber(item.StreetCode, prop.MailingBuildingNumber);
                            if (value != null)
                            {
                                B = value.Bbl.ToString();
                                BoroSelectedItem = Boros.Find((boro) => boro.BoroId.Equals(B.Substring(0, 1)));
                                break;
                            }
                        }
                            
                    }
                }
                else
                {
                    BoroSelectedItem = null;
                    IsBoroEnabled = false;
                }
                NovMaster.NovInformation.PlaceHouseNo = prop.PremiseBuildingNumber.ToString();
                NovMaster.NovInformation.PlaceSideOfStreet = prop.PremiseStreetName;
                NovMaster.NovInformation.LicenseExpDate = !string.IsNullOrEmpty(prop.ExpirationDate) ? DateTime.ParseExact(prop.ExpirationDate, "yyyyMMdd", null) : new DateTime(1900, 1, 1);
                NovMaster.NovInformation.LicenseNumber = prop.LicenseId;
                NovMaster.NovInformation.PlaceBBL = Convert.ToString(prop.Bbl);
                NovMaster.NovInformation.PlaceDistrictId = Convert.ToString(prop.DistrictId);
                if (Convert.ToString(prop.PremiseCommunityDist).Length>1)
                {
                    NovMaster.NovInformation.PlaceBoroCode = prop.PremiseCommunityDist.ToString().Substring(0, 1);
                }
                if(!string.IsNullOrWhiteSpace(NovMaster.NovInformation.PlaceBBL))
                {
                    var value2 = await _placeOfOccurrenceService.GetAllBBLNumbers(NovMaster.NovInformation.PlaceBBL);
                    if(value2.Count>0)
                    {
                        NovMaster.NovInformation.PlaceStreetId = Convert.ToInt32(value2[0].StreetCode);
                    }
                    
                }
                
            }

        }

        private string GetStreetName(string s)
        {

            var str = "";
            var n = 0;
            int i = 0;
            if (s[0] == 'W' && s[1] == ' ')
            {
                str = str + "WEST";
                i++;
            }
            if (s[0] == 'E' && s[1] == ' ')
            {
                str = str + "EAST";
                i++;
            }
            if (s[0] == 'N' && s[1] == ' ')
            {
                str = str + "NORTH";
                i++;
            }
            if (s[0] == 'S' && s[1] == ' ')
            {
                str = str + "SOUTH";
                i++;
            }

            for (; i < s.Length; i++)
            {
                if (!Char.IsNumber(s[i]))
                {
                    if (n == 1)
                    {
                        i = i + 1;
                        n = 0;
                    }
                    else
                        str = str + s[i];
                }
                else
                {
                    str = str + s[i];
                    n = 1;
                }

            }

            string[] words = str.Split(' ');
            var s_str = "";
            if (words.Length == 1)
            {
                s_str = str;
            }
            else
            {
                for (i = 0; i < words.Length - 1; i++)
                {
                    s_str += words[i] + ' ';
                }

                switch (words[i].ToUpper())
                {
                    case "AVE":
                        s_str += "AVENUE";
                        break;
                    case "ST":
                        s_str += "STREET";
                        break;
                    case "BLVD":
                        s_str += "BOULEVARD";
                        break;
                    case "RD":
                        s_str += "ROAD";
                        break;
                    case "PL":
                        s_str += "PLACE";
                        break;
                    case "DR":
                        s_str += "DRIVE";
                        break;
                }
            }

            return s_str;
        }

        public string LicenseId
        {
            get => _licenseId;
            set
            {
                _licenseId = value.ToUpper();
                if (_licenseId.Length >= 11)
                {
                    getLicDetails();
                    NotifyPropertyChanged();
                }

            }
        }

        public string TradeName
        {
            get => _tradeName;
            set { _tradeName = value; NotifyPropertyChanged(); }
        }


        public bool IsBusinessNameVisible
        {
            get => _isBusinessNameVisible;
            set { _isBusinessNameVisible = value; NotifyPropertyChanged(); }
        }

        public string FirstName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                NotifyPropertyChanged();
            }
        }

        public string LastName
        {
            get => _lastName;
            set { _lastName = value; NotifyPropertyChanged(); }
        }

        public string MiddleInitial
        {
            get => _middleInitial;
            set { _middleInitial = value; NotifyPropertyChanged(); }
        }
        public bool TradeNameVisible
        {
            get => _tradeNameVisible;
            set { _tradeNameVisible = value; NotifyPropertyChanged(); }
        }

        public bool AreNameFieldsVisible
        {
            get => _areNameFieldsVisible;
            set { _areNameFieldsVisible = value; NotifyPropertyChanged(); }
        }

        public string StreetName
        {
            get => _streetName;
            set { _streetName = value; NotifyPropertyChanged(); }
        }

        public string StreetNumber
        {
            get => _streetNumber;
            set { _streetNumber = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> Sexes
        {
            get => _sexes;
            set { _sexes = value; NotifyPropertyChanged(); }
        }

        public LookupTable SexSelectedItem
        {
            get => _sexSelectedItem;
            set { _sexSelectedItem = value; NotifyPropertyChanged(); }
        }

        public string Apt
        {
            get => _apt;
            set { _apt = value; NotifyPropertyChanged(); }
        }

        public List<BoroMaster> Boros
        {
            get => _boros;
            set { _boros = value; NotifyPropertyChanged(); }
        }

        public bool IsBusinessCheckVisible
        {
            get => _isBusinessCheckVisible;
            set { _isBusinessCheckVisible = value; NotifyPropertyChanged(); }
        }
        public BoroMaster BoroSelectedItem
        {
            get => _boroSelectedItem;
            set { _boroSelectedItem = value; NotifyPropertyChanged(); }
        }

        public string City
        {
            get => _city;
            set { _city = value; NotifyPropertyChanged(); }
        }

        public List<LookupTable> States
        {
            get => _states;
            set { _states = value; NotifyPropertyChanged(); }
        }

        public LookupTable StateSelectedItem
        {
            get => _stateSelectedItem;
            set
            {
                _stateSelectedItem = value;
                if (_stateSelectedItem?.Code == "NY")
                {
                    IsBoroEnabled = IsEnabled ? true : false;
                }
                else
                {
                    BoroSelectedItem = null;
                    IsBoroEnabled = false;
                }

                NotifyPropertyChanged();
            }
        }

        public string ZipCode
        {
            get => _zipCode;
            set { _zipCode = value; NotifyPropertyChanged(); }
        }

        public bool IsNameChecked
        {
            get => _isNameChecked;
            set
            {
                _isNameChecked = value;
                if (_isNameChecked)
                {
                    IsBusinessNameVisible = false;
                    AreNameFieldsVisible = true;
                }
                else
                {
                    IsBusinessNameVisible = true;
                    AreNameFieldsVisible = false;
                }

                NotifyPropertyChanged();
            }
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set { _isEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsBoroEnabled
        {
            get => _isBoroEnabled;
            set { _isBoroEnabled = value; NotifyPropertyChanged(); }
        }

        public override Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (IsNameChecked)
            {
                if (string.IsNullOrEmpty(FirstName))
                {
                    alerts.Add(new AlertViewModel("12047", WorkFlowMessages.DSNYMSG_NOV047, elementName: "FirstName"));
                }

                if (string.IsNullOrEmpty(LastName))
                {
                    alerts.Add(new AlertViewModel("12046", WorkFlowMessages.DSNYMSG_NOV046, elementName: "LastName"));
                }

                if (SexSelectedItem == null)
                {
                    alerts.Add(new AlertViewModel("12048", WorkFlowMessages.DSNYMSG_NOV048, elementName: "Gender"));
                }
            }
            else
            {
                if (string.IsNullOrEmpty(BusinessName))
                {
                    alerts.Add(new AlertViewModel("12011", WorkFlowMessages.DSNYMSG_NOV011, elementName: "BusinessName"));
                }
            }

            if (string.IsNullOrEmpty(StreetName))
            {
                alerts.Add(new AlertViewModel("12049", WorkFlowMessages.DSNYMSG_NOV049, elementName: "StreetName"));
            }

            if (string.IsNullOrEmpty(StreetNumber))
            {
                alerts.Add(new AlertViewModel("12049", WorkFlowMessages.DSNYMSG_NOV049, elementName: "StreetNumber"));
            }

            if (string.IsNullOrEmpty(City))
            {
                alerts.Add(new AlertViewModel("12050", WorkFlowMessages.DSNYMSG_NOV050, elementName: "City"));
            }

            if (!string.IsNullOrEmpty(ZipCode))
            {
                if ((ZipCode.Length < 5 && ZipCode.Length > 1) || ZipCode.Length > 5 || ZipCode.Contains("."))
                {
                    alerts.Add(new AlertViewModel("12052", WorkFlowMessages.DSNYMSG_NOV052, elementName: "ZipCode"));
                }
            }

            return Task.FromResult(alerts);
        }

        public override void WriteFieldValuesToNovMaster()
        {
            if (IsNameChecked)
            {
                NovInformation.Resp1FirstName = FirstName;
                NovInformation.Resp1MiddleInitial = MiddleInitial;
                NovInformation.Resp1LastName = LastName;
                NovInformation.Resp1Sex = SexSelectedItem?.Code;
            }
            else
            {
                NovInformation.Resp1FirstName = null;
                NovInformation.Resp1MiddleInitial = null;
                NovInformation.Resp1LastName = null;
                NovInformation.Resp1Sex = null;
            }

            NovInformation.IsBusiness = IsBusinessNameVisible;

            // NH-1168
            //NovInformation.LicenseNumber = IsLicenseNumberVisible ? LicenseId : null;
            if (IsLicenseNumberVisible)
                NovInformation.LicenseNumber = LicenseId;

            NovInformation.BusinessName = IsBusinessNameVisible
                ? BusinessName
                : IsAction || IsPosting
                    ? ""
                    : string.IsNullOrWhiteSpace(LastName)
                        ? $"{FirstName} {MiddleInitial}".Trim()
                        : $"{LastName}, {FirstName} {MiddleInitial}".Trim();
            
            NovInformation.Resp1HouseNo = StreetNumber;
            NovInformation.Resp1Address = StreetName;
            NovInformation.Resp1Address1 = Apt;
            NovInformation.Resp1BoroCode = IsBoroEnabled ? BoroSelectedItem?.BoroId : "1";
            NovInformation.Resp1City = City;
            NovInformation.Resp1State = StateSelectedItem?.Code;
            NovInformation.Resp1Zip = ZipCode;
            NovInformation.Resp1Sex = SexSelectedItem?.Code;

            if (IsCancelled)
            {
                NovInformation.TicketStatus = "C";
            }
            else if (IsVoidAction)
            {
                NovInformation.TicketStatus = "V";
            }
        }

        // NH-1408 2018-11-05 Action ; Point of no return is PersonBeingServedID screen

        // TODO: S6V and S7V should verify the No Point of Return and when will the NOV number is generated

        //public override List<string> CancelMenuItems
        //{
        //    get
        //    {
        //        if (NovMaster.NovInformation.ViolationCode == "S6V" || NovMaster.NovInformation.ViolationCode == "S7V")
        //        {
        //            return new List<string> { "Cancel" };
        //        }

        //        if (!String.IsNullOrEmpty(showVoid))
        //        {
        //            if (showVoid == "true")
        //            {
        //                return new List<string> { "Void" };
        //            }
        //            else
        //            {
        //                return new List<string> { "Cancel" };
        //            }
        //        }
        //        else
        //        {
        //            return new List<string> { "Cancel" };
        //        }


        //    }
        //}

        public override ICommand BackCommand => new Command(async () =>
        {
            CrossSettings.Current.AddOrUpdateValue($"IDNumber_{NovNumber}", LicenseId);

            if (NavigationInProgress)
                return;

            NavigationInProgress = true;

            if (UserSession != null)
                UserSession.TimeoutTimeStamp = DateTime.Now;
            else if (NovMaster?.UserSession != null)
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            
            WriteFieldValuesToNovMaster();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
        });

        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress)
                return;

            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }

            if (UserSession != null)
                UserSession.TimeoutTimeStamp = DateTime.Now;
            else if (NovMaster?.UserSession != null)
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;

            WriteFieldValuesToNovMaster();

            //if (ShouldSaveTicketOnNext)
            //{
            //await SaveTicket();
            //}

            if(!NovInformation.LockPreviousScreens)
                NovInformation.LockPreviousScreens = ShouldLockPreviousScreensOnNext;

            if(!NovInformation.LockPlaceOfOccurrenceScreen)
                NovInformation.LockPlaceOfOccurrenceScreen = ShouldLockPlaceOfOccurrenceOnNext;

            NovMaster.NovInformation.IsPersonBeingServedBusi = true;

            var nextVM = NextViewModel;
            if (nextVM != null)
            {
                await NavigationService.PushAsync(nextVM);
            }

            NavigationInProgress = false;
        });
    }
}