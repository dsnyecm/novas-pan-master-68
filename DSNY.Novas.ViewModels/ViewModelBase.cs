﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Interfaces;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using DSNY.Novas.Entities;
using DSNY.Novas.Data;
using DSNY.Novas.Models.Enums;

namespace DSNY.Novas.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged, ILoadableViewModel, IVisibilityAwareViewModel
    {

        public const string SHERIFF_DEP_KEY = "Department of Sanitation";
        public const string SHERIFF_DEP = "Sheriff's Department";
        public event PropertyChangedEventHandler PropertyChanged;

        #region Services

        protected static INavigationService NavigationService { get; } = DependencyResolver.Get<INavigationService>();
        protected static ILoginService LoginService { get; } = DependencyResolver.Get<ILoginService>();
        protected static ILookupService LookupService { get; } = DependencyResolver.Get<ILookupService>();
        protected static IDeviceService DeviceService { get; } = DependencyResolver.Get<IDeviceService>();
        protected static ILocalDeviceSettingsService DeviceSettingsService { get; } = DependencyResolver.Get<ILocalDeviceSettingsService>();
        protected static INFCReader NfcReader { get; } = DependencyResolver.Get<INFCReader>();
        protected static IBluetoothReader BluetoothReader { get; } = DependencyResolver.Get<IBluetoothReader>();
        protected static IBarcodeScanner BarcodeScanner { get; } = DependencyResolver.Get<IBarcodeScanner>();
        protected static IDnsWatcher DnsWatcher { get; } = DependencyResolver.Get<IDnsWatcher>();
        protected static IAlertService AlertService { get; } = DependencyResolver.Get<IAlertService>();
        protected static INovService NovService { get; } = DependencyResolver.Get<INovService>();
        protected static IVehicleService VehicleService { get; } = DependencyResolver.Get<IVehicleService>();
        protected static IPlaceOfOccurrenceService PlaceOfOccurrenceService { get; } = DependencyResolver.Get<IPlaceOfOccurrenceService>();
        protected static IPersonBeingServedService PersonBeingServedService { get; } = DependencyResolver.Get<IPersonBeingServedService>();
        protected static IPersonnelDataService PersonnelDataService { get; } = DependencyResolver.Get<IPersonnelDataService>();
        protected static INovasUserService NovasUserService { get; } = DependencyResolver.Get<INovasUserService>();

        protected static ISyncService SyncService;

        #endregion

        #region Repositories

        protected static IRepository<DBBoroSiteMaster> BoroSiteMasterRepo { get; } = DependencyResolver.Get<IRepository<DBBoroSiteMaster>>();
        protected static IRepository<DBLookup> LookupRepo { get; } = DependencyResolver.Get<IRepository<DBLookup>>();

        #endregion

        protected static readonly Task<List<Printer>> LoadPrintersTask = BluetoothReader.ListAllPrinters();


        #region ReportingLevels

        protected static readonly Task<Task> LoadReportingLevelsTask = PersonnelDataService.GetReportingLevelListAsync()
            .ContinueWith(async reportingLevels =>
            {
                var reportingLevelsResult = await reportingLevels;

                if (!reportingLevelsResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("ReportingLevels", "ReportingLevels not found"));
                else
                    reportingLevelsResult?.Distinct().ToList().ForEach(reportingLevel => ReportingLevelCollection.Add(reportingLevel));
            });

        protected static readonly ObservableCollection<ReportingLevel> _reportingLevelCollection = new ObservableCollection<ReportingLevel>();
        protected static ObservableCollection<ReportingLevel> ReportingLevelCollection => _reportingLevelCollection;

        protected static readonly IObservable<ObservableCollection<ReportingLevel>> _reportingLevelCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                    h => (sender, args) => h(args),
                    h => ReportingLevelCollection.CollectionChanged += h,
                    h => ReportingLevelCollection.CollectionChanged -= h)
                .Select(args => ReportingLevelCollection);

        protected static IObservable<ObservableCollection<ReportingLevel>> ReportingLevelCollectionChangedObservable => _reportingLevelCollectionChangedObservable;

        protected static IDisposable ReportingLevelCollectionChangedSubscription { get; set; }

        protected IDictionary<string, IEnumerable<ReportingLevel>> ReportingLevelsByTitle =>
            ReportingLevelCollection?
                .GroupBy(obj => obj.Title.Trim(), obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable().Distinct());

        protected IDictionary<string, ReportingLevel> ReportingLevelsByIdAndTitle =>
            ReportingLevelCollection?
                .ToDictionary(reportingLevel => reportingLevel.ReportingLevelAndTitle, reportingLevel => reportingLevel);

        #endregion

        #region Agencies

        protected static readonly Task<Task> LoadAgenciesTask = PersonnelDataService.GetAgencyListAsync()
            .ContinueWith(async agencies =>
            {
                var agenciesResult = await agencies;

                if (!agenciesResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("Agencies", "Agencies not found"));
                else
                    agenciesResult?.ForEach(agency => AgencyCollection.Add(agency));
            });

        protected static readonly ObservableCollection<Agency> _agencyCollection = new ObservableCollection<Agency>();
        protected static ObservableCollection<Agency> AgencyCollection => _agencyCollection;

        protected static readonly IObservable<ObservableCollection<Agency>> _agencyCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                    h => (sender, args) => h(args),
                    h => AgencyCollection.CollectionChanged += h,
                    h => AgencyCollection.CollectionChanged -= h)
                .Select(args => AgencyCollection);
        public IObservable<ObservableCollection<Agency>> AgencyCollectionChangedObservable => _agencyCollectionChangedObservable;

        protected static IDisposable AgencyCollectionChangedSubscription { get; set; }

        protected static IDictionary<int, Agency> Agencies =>
            AgencyCollection?
                .GroupBy(obj => obj.AgencyId, obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable().Distinct().FirstOrDefault());

        #endregion

        #region HolidayMaster
        
        protected static readonly Task<Task> LoadHolidaysTask = PersonnelDataService.GetHolidaysAsync()
            .ContinueWith(async holidays =>
            {
                var holidaysResult = await holidays;

                if (!holidaysResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("Holidays", "Holidays not found"));
                else
                    holidaysResult?.ForEach(holiday => HolidayCollection.Add(holiday));
            });

        protected static readonly ObservableCollection<HolidayMaster> _holidayCollection = new ObservableCollection<HolidayMaster>();
        protected static ObservableCollection<HolidayMaster> HolidayCollection => _holidayCollection;

        protected static readonly IObservable<ObservableCollection<HolidayMaster>> _holidayCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                h => (sender, args) => h(args),
                h => HolidayCollection.CollectionChanged += h,
                h => HolidayCollection.CollectionChanged -= h)
            .Select(args => HolidayCollection);
        public IObservable<ObservableCollection<HolidayMaster>> HolidayCollectionChangedObservable => _holidayCollectionChangedObservable;
        
        protected static IDisposable HolidayCollectionChangedSubscription { get; set; }

        protected static IDictionary<int, IEnumerable<HolidayMaster>> HolidaysByBoro =>
            HolidayCollection?
                .GroupBy(obj => int.Parse(obj.BoroCode), obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable().Distinct());

        #endregion

        #region HearingDates

        protected static readonly Task<Task> LoadHearingDatesTask = PersonnelDataService.GetHearingDatesAsync(0)
            .ContinueWith(async hearingDates =>
            {
                var hearingDatesResult = await hearingDates;

                if (!hearingDatesResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("HearingDates", "HearingDates not found"));
                else
                    hearingDatesResult?.ForEach(hearingDate => HearingDateCollection.Add(hearingDate));
            });

        protected static readonly ObservableCollection<HearingDate> _hearingDateCollection = new ObservableCollection<HearingDate>();
        protected static ObservableCollection<HearingDate> HearingDateCollection => _hearingDateCollection;

        protected static readonly IObservable<ObservableCollection<HearingDate>> _hearingDateCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                    h => (sender, args) => h(args),
                    h => HearingDateCollection.CollectionChanged += h,
                    h => HearingDateCollection.CollectionChanged -= h)
                .Select(args => HearingDateCollection);
        public IObservable<ObservableCollection<HearingDate>> HearingDateCollectionChangedObservable => _hearingDateCollectionChangedObservable;

        protected static IDisposable HearingDateCollectionChangedSubscription { get; set; }

        protected static IDictionary<int, IEnumerable<HearingDate>> HearingDatesByBoro =>
            HearingDateCollection?
                .GroupBy(obj => int.Parse(obj.BoroCode), obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable());

        #endregion

        #region HearingDateTimes

        protected static readonly Task<Task> LoadHearingDateTimesTask = PersonnelDataService.GetHearingTimesAsync(0)
            .ContinueWith(async hearingDateTimes =>
            {
                var hearingDateTimesResult = await hearingDateTimes;

                if (!hearingDateTimesResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("HearingDateTimes", "HearingDateTimes not found"));
                else
                    hearingDateTimesResult?.ForEach(hearingDateTime => HearingDateTimeCollection.Add(hearingDateTime));
            });

        protected static readonly ObservableCollection<HearingDateTime> _hearingDateTimeCollection = new ObservableCollection<HearingDateTime>();
        protected static ObservableCollection<HearingDateTime> HearingDateTimeCollection => _hearingDateTimeCollection;

        protected static readonly IObservable<ObservableCollection<HearingDateTime>> _hearingDateTimeCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                    h => (sender, args) => h(args),
                    h => HearingDateTimeCollection.CollectionChanged += h,
                    h => HearingDateTimeCollection.CollectionChanged -= h)
                .Select(args => HearingDateTimeCollection);
        public IObservable<ObservableCollection<HearingDateTime>> HearingDateTimeCollectionChangedObservable => _hearingDateTimeCollectionChangedObservable;

        protected static IDisposable HearingDateTimeCollectionChangedSubscription { get; set; }

        protected static IDictionary<int, IEnumerable<HearingDateTime>> HearingDateTimesByAgency =>
            HearingDateTimeCollection?
                .GroupBy(obj => obj.AgencyId, obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable());

        #endregion

        #region BoroSiteMaster

        protected static readonly Task<Task> LoadBoroSiteMastersTask = PersonnelDataService.GetBoroSites()
            .ContinueWith(async boroSiteMasters =>
            {
                var boroSiteMastersResult = await boroSiteMasters;

                if (!boroSiteMastersResult?.Any() ?? false)
                    await AlertService.DisplayAlert(new AlertViewModel("BoroSiteMasters", "BoroSiteMasters not found"));
                else
                    boroSiteMastersResult?.ForEach(boroSiteMaster => BoroSiteMasterCollection.Add(boroSiteMaster));
            });

        protected static readonly ObservableCollection<DBBoroSiteMaster> _boroSiteMasterCollection = new ObservableCollection<DBBoroSiteMaster>();
        protected static ObservableCollection<DBBoroSiteMaster> BoroSiteMasterCollection => _boroSiteMasterCollection;

        protected static readonly IObservable<ObservableCollection<DBBoroSiteMaster>> _boroSiteMasterCollectionChangedObservable =
            Observable.FromEvent<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                    h => (sender, args) => h(args),
                    h => BoroSiteMasterCollection.CollectionChanged += h,
                    h => BoroSiteMasterCollection.CollectionChanged -= h)
                .Select(args => BoroSiteMasterCollection);
        public IObservable<ObservableCollection<DBBoroSiteMaster>> BoroSiteMasterCollectionChangedObservable => _boroSiteMasterCollectionChangedObservable;

        protected static IDisposable BoroSiteMasterCollectionChangedSubscription { get; set; }

        protected static IDictionary<int, IEnumerable<DBBoroSiteMaster>> BoroSiteMastersByBoro =>
            BoroSiteMasterCollection?
                .GroupBy(obj => int.Parse(obj.BoroCode), obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable().Distinct());

        protected static IDictionary<string, DBBoroSiteMaster> BoroSiteMastersByReportLevel =>
            BoroSiteMasterCollection?
                .GroupBy(obj => obj.ReportLevel, obj => obj)
                .ToDictionary(obj => obj.Key, obj => obj.AsEnumerable().FirstOrDefault());

        #endregion

        #region ApiUrl

        private static ApiUrl _apiUrl;
        protected ApiUrl ApiUrl
        {
            get => _apiUrl;
            private set
            {
                _apiUrl = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(NetworkName));
                NotifyPropertyChanged(nameof(IpAddress));
            }
        }

        protected string NetworkName => ApiUrl?.NetworkName;
        protected string IpAddress => ApiUrl?.IpAddress;

        #endregion

        #region AppRuntimeSettings

        private static IAppRuntimeSettings _appRuntimeSettings;
        protected IAppRuntimeSettings AppRuntimeSettings
        {
            get => _appRuntimeSettings;
            set
            {
                _appRuntimeSettings = value;
                NotifyPropertyChanged(nameof(PlatformDeviceName));
                NotifyPropertyChanged(nameof(HardwareIdentifier));
                NotifyPropertyChanged(nameof(Version));
                NotifyPropertyChanged(nameof(DeviceIMEI));
                NotifyPropertyChanged(nameof(DeviceIdAndIMEI));
                NotifyPropertyChanged(nameof(DeviceSerialNumber));
            }
        }

        protected string PlatformDeviceName => AppRuntimeSettings?.PlatformDeviceName;
        protected string HardwareIdentifier => AppRuntimeSettings?.HardwareIdentifier;
        protected string Version => AppRuntimeSettings?.Version;
        protected string DeviceIMEI => AppRuntimeSettings?.DeviceIMEI;
        protected string DeviceSerialNumber => AppRuntimeSettings?.DeviceSerialNumber;
        protected string DeviceIdAndIMEI => new StringBuilder()
            .AppendLine($"Platform DeviceId: {PlatformDeviceName}")
            .AppendLine($"IMEI: {DeviceIMEI}")
            .ToString();

        #endregion

        private bool _isCancelled;
        private bool _isVoidAction;
        private int autoLogoutTime = 15 * 60; // TIMEOUT
        //private int autoLogoutTime = 1 * 60;
        public static DateTime propertyChangedTime { get; set; }
        protected static bool NeedNotCheckLogOff { get; set; }
        public static DateTime SyncTime { get; set; }
        private bool autoTimeOutLooping { get; set; }
        protected bool isForeground { get; set; }
        protected DateTime LoginTimestamp { get; set; }
        public bool _isTimeOutWithLogin { get; set; }
        protected static ConcreteWorkflowFactory ConcreteWorkflowFactory;

        protected bool NavigationInProgress { get; set; }

        //protected static Task<List<Printer>> LoadPrintersTask;

        protected static readonly IDictionary<string, string> TicketStatusTicketPrintingTypeMap =
            new Dictionary<string, string>
            {
                {"L", "TO BE SERVED"},
                {"T", "TO BE SERVED"},
                {"I", "FAILED" },
                {"V", "VOID" }
            };

        protected static readonly IDictionary<string, string> Descriptors =
            new Dictionary<string, string>
            {
                {"V", "Vacant Lot"},
                {"S_MDR", "At Side Of"},
                {"F_MDR", "Front Of"},
                {"R_MDR", "At Rear Of"},
                {"F", "Front Of"},
                {"O", "Opposite Of"},
                {"R", "At Rear Of"},
                {"C", "At Corner Of"},
                {"S", "At Side Of"},
                {"Q", "Front Of C/M"},
                {"N", "Near"},
                {"A", "At"},
                {"P", "On/Bet C/M"},
                {"B", "On/Between"},
                {"T", "At"},
                {"T_MDR", "At"},
            };

        static ViewModelBase()
        {
            ConcreteWorkflowFactory = new ConcreteWorkflowFactory();
        }

        public ViewModelBase()
        {
            /// TO DO this is to test properly.
            /*
            var codeEnforceRepo = DependencyResolver.Get<IRepository<DBCodeEnforceControl>>();
    
            Task.Run(async () =>
            {
                var codeEnf=await codeEnforceRepo.GetAsync();
                if (codeEnf.Count() > 0)
                {
                    if(codeEnf.FirstOrDefault().InactivityTimeoutMinutes>0)
                    {
                        autoLogoutTime = Convert.ToInt32(codeEnf.FirstOrDefault().InactivityTimeoutMinutes);
                    }
                }
            });
            */
            //AutoLogOff();

            #region ReportingLevels

            if(ReportingLevelCollectionChangedSubscription == null)
                ReportingLevelCollectionChangedSubscription = ReportingLevelCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(
                        _ =>
                        {
                            NotifyPropertyChanged(nameof(ReportingLevelsByTitle));
                            NotifyPropertyChanged(nameof(ReportingLevelsByIdAndTitle));
                        });

            #endregion

            #region HolidayMaster

            if (HolidayCollectionChangedSubscription == null)
                HolidayCollectionChangedSubscription = HolidayCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(_ => NotifyPropertyChanged(nameof(HolidaysByBoro)));

            #endregion

            #region Agencies

            if(AgencyCollectionChangedSubscription == null)
                AgencyCollectionChangedSubscription = AgencyCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(_ => NotifyPropertyChanged(nameof(Agencies)));

            #endregion

            #region HearingDates

            if(HearingDateCollectionChangedSubscription == null)
                HearingDateCollectionChangedSubscription = HearingDateCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(_ => NotifyPropertyChanged(nameof(HearingDatesByBoro)));

            #endregion

            #region HearingDateTimes

            if (HearingDateTimeCollectionChangedSubscription == null)
                HearingDateTimeCollectionChangedSubscription = HearingDateTimeCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(_ => NotifyPropertyChanged(nameof(HearingDateTimesByAgency)));

            #endregion

            #region BoroSiteMasters

            if (BoroSiteMasterCollectionChangedSubscription == null)
                BoroSiteMasterCollectionChangedSubscription = BoroSiteMasterCollectionChangedObservable
                    .ObserveOn(SynchronizationContext.Current)
                    .SubscribeOn(NewThreadScheduler.Default)
                    .Subscribe(_ =>
                    {
                        NotifyPropertyChanged(nameof(BoroSiteMastersByBoro));
                        NotifyPropertyChanged(nameof(BoroSiteMastersByReportLevel));
                    });

            #endregion

        }

        public virtual string Title => "";

        public NovMaster NovMaster { get; set; }

        #region UserSession

        private static UserSession _userSession = new UserSession();
        public UserSession UserSession
        {
            get => _userSession;
            set
            {
                _userSession = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(UserSessionUserId));
                NotifyPropertyChanged(nameof(UserSessionReportLevel));
                NotifyPropertyChanged(nameof(UserSessionTitle));
                NotifyPropertyChanged(nameof(UserSessionAgencyId));
                NotifyPropertyChanged(nameof(UserSessionAgencyIdInt));
                NotifyPropertyChanged(nameof(UserSessionTimeoutTimeStamp));
            }
        }

        public string UserSessionUserId => UserSession?.UserId;
        public string UserSessionReportLevel => UserSession?.ReportLevel?.Trim();
        public string UserSessionTitle => UserSession?.Title?.Trim();
        public string UserSessionAgencyId => UserSession?.AgencyId;
        public int UserSessionAgencyIdInt =>
            string.IsNullOrWhiteSpace(UserSessionAgencyId) ? 0 : int.Parse(UserSessionAgencyId);

        public DateTime? UserSessionTimeoutTimeStamp
        {
            get => UserSession?.TimeoutTimeStamp;
            set
            {
                if (UserSession == null || !value.HasValue)
                    return;
                
                UserSession.TimeoutTimeStamp = value.Value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region AffidavitOfService

        protected AffidavitOfService AffidavitOfService => NovMaster?.AffidavitOfService;

        /// <summary>
        /// AffidavitOfService.PersonallyServeFlag == "P"
        /// </summary>
        protected bool IsPersonallyServed => AffidavitOfService?.PersonallyServeFlag == "P";

        /// <summary>
        /// AffidavitOfService.PersonallyServeFlag == "A"
        /// </summary>
        protected bool IsAlternativelyServed => AffidavitOfService?.PersonallyServeFlag == "A";

        #endregion

        #region ViolationGroupIds

        /// <summary>
        /// [2, 3]
        /// </summary>
        protected static int[] PropertyViolationGroupIds { get; } = new[]
            {ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty}.Cast<int>().ToArray();

        /// <summary>
        /// [2, 3, 4]
        /// </summary>
        protected static int[] PropertyAndPostingViolationGroupIds { get; } = new[]
            {ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty, ViolationGroupId.Posting}.Cast<int>().ToArray();

        /// <summary>
        /// [1,2,3]
        /// </summary>
        protected static int[] ActionAndPropertyViolationGroupIds { get; } =
            new[] { ViolationGroupId.Action, ViolationGroupId.NonCommercialProperty, ViolationGroupId.CommercialProperty }
                .Cast<int>().ToArray();

        /// <summary>
        /// [1, 4, 7]
        /// </summary>
        protected static int[] ActionPostingAndTobaccoViolationGroupIds { get; } =
            new[] { ViolationGroupId.Action, ViolationGroupId.Posting, ViolationGroupId.Tobacco }
                .Cast<int>().ToArray();

        /// <summary>
        /// [4, 6]
        /// </summary>
        protected static int[] PostingAndDcodeViolationGroupIds { get; } =
            new[] { ViolationGroupId.Posting, ViolationGroupId.DCodesAndPosting }
                .Cast<int>().ToArray();

        #endregion

        #region ViolationTypeIds

        /// <summary>
        /// ["A", "O"]
        /// </summary>
        protected static string[] ActionPostingDcodeAndVacantLotViolationTypeIds { get; } =
            new[] { ViolationTypeId.Action, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["A", "O", "T"]
        /// </summary>
        protected static string[] ActionPostingDcodeVacantLotAndTobaccoViolationTypeIds { get; } =
            new[] { ViolationTypeId.Action, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot, ViolationTypeId.Tobacco }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["C", "M", "R"]
        /// </summary>
        protected static string[] PropertyViolationTypeIds { get; } =
            new[] { ViolationTypeId.CommercialProperty, ViolationTypeId.MultipleDwellingProperty, ViolationTypeId.ResidentialProperty }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        /// <summary>
        /// ["M", "R"]
        /// </summary>
        protected static string[] NonCommercialPropertyViolationTypeIds { get; } =
            new[] { ViolationTypeId.MultipleDwellingProperty, ViolationTypeId.ResidentialProperty }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();


        /// <summary>
        /// ["C", "O"]
        /// </summary>
        protected static string[] CommercialPropertyDcodePostingOrVacantLotViolationTypeIds { get; } =
            new[] { ViolationTypeId.CommercialProperty, ViolationTypeId.DCode, ViolationTypeId.Posting, ViolationTypeId.VacantLot }
                .Select(vtId => ((char)vtId).ToString()).Distinct().ToArray();

        #endregion

        /// <summary>
        /// ["C", "V"]
        /// </summary>
        protected static string[] CancelledAndVoidTicketStatuses { get; } =
            new[] { ((char)TicketStatus.Cancelled).ToString(), ((char)TicketStatus.Void).ToString() }.ToArray();
        
        #region NovInformation

        /// <summary>
        /// NovMaster?.NovInformation
        /// </summary>
        protected NovInformation NovInformation => NovMaster?.NovInformation;

        /// <summary>
        /// NovInformation?.NovNumber ?? 0
        /// </summary>
        protected long NovNumber => NovInformation?.NovNumber ?? 0;

        /// <summary>
        /// NovInformation?ViolationTypeId
        /// </summary>
        protected string NovViolationTypeId => NovInformation?.ViolationTypeId;

        /// <summary>
        /// NovInformation?.ViolationGroupId ?? 0
        /// </summary>
        protected int NovViolationGroupId => NovInformation?.ViolationGroupId ?? 0;

        /// <summary>
        /// NovViolationGroupId == 5
        /// </summary>
        /// <returns></returns>
        protected bool IsVacantLot => NovViolationGroupId == (int)ViolationGroupId.VacantLot;

        /// <summary>
        /// NovViolationGroupId == 1 Or NovViolationTypeId == 'A'
        /// </summary>
        /// <returns></returns>
        protected bool IsAction => NovViolationGroupId == (int)ViolationGroupId.Action || NovViolationTypeId?.ToUpper()?.Equals("A") == true;

        /// <summary>
        /// NovViolationTypeId.In("A", "O")
        /// </summary>
        /// <returns></returns>
        protected bool IsActionPostingDcodeOrVacantLot => NovViolationTypeId.In(ActionPostingDcodeAndVacantLotViolationTypeIds);

        /// <summary>
        /// NovViolationTypeId.In("A", "O", "T")
        /// </summary>
        /// <returns></returns>
        protected bool IsActionPostingDcodeVacantLotOrTobacco => NovViolationTypeId.In(ActionPostingDcodeVacantLotAndTobaccoViolationTypeIds);

        /// <summary>
        /// NovViolationGroupId.In(1, 4, 7)
        /// </summary>
        /// <returns></returns>
        protected bool IsActionPostingOrTobacco => NovViolationGroupId.In(ActionPostingAndTobaccoViolationGroupIds);

        /// <summary>
        /// NovViolationGroupId == 7 Or NovViolationTypeId == "T"
        /// </summary>
        /// <returns></returns>
        protected bool IsTobacco => NovViolationGroupId == (int)ViolationGroupId.Tobacco || NovViolationTypeId == ((char)ViolationTypeId.Tobacco).ToString();

        /// <summary>
        /// ViolationGroupId.In(2, 3) Or ViolationTypeId.In("C", "M", "R")
        /// </summary>
        /// <returns></returns>
        protected bool IsProperty => NovViolationGroupId.In(PropertyViolationGroupIds) || NovViolationTypeId.In(PropertyViolationTypeIds);

        /// <summary>
        /// NovViolationGroupId == 3 Or NovViolationTypeId == "C"
        /// </summary>
        /// <returns></returns>
        protected bool IsCommercialProperty => NovViolationGroupId == (int)ViolationGroupId.CommercialProperty || NovViolationTypeId == ((char)ViolationTypeId.CommercialProperty).ToString();

        /// <summary>
        /// NovViolationTypeId == "R"
        /// </summary>
        protected bool IsResidentialProperty => NovViolationTypeId == ((char) ViolationTypeId.ResidentialProperty).ToString();

        /// <summary>
        /// NovViolationTypeId == "M"
        /// </summary>
        protected bool IsMultipleDwellingProperty => NovViolationTypeId == ((char)ViolationTypeId.MultipleDwellingProperty).ToString();

        /// <summary>
        /// NovViolationTypeId.In("C", "O")
        /// </summary>
        /// <returns></returns>
        protected bool IsCommercialPropertyDcodePostingOrVacantLot => NovViolationTypeId.In(CommercialPropertyDcodePostingOrVacantLotViolationTypeIds);

        /// <summary>
        /// NovViolationGroupId == 2 Or NovViolationTypeId.In("M", "R")
        /// </summary>
        /// <returns></returns>
        protected bool IsNonCommercialProperty => NovViolationGroupId == (int)ViolationGroupId.NonCommercialProperty || NovViolationTypeId.In(NonCommercialPropertyViolationTypeIds);

        /// <summary>
        /// NovViolationGroupId.In(1, 2, 3)
        /// </summary>
        /// <returns></returns>
        protected bool IsActionOrProperty => NovViolationGroupId.In(ActionAndPropertyViolationGroupIds);

        /// <summary>
        /// NovViolationGroupId.In(2, 3, 4)
        /// </summary>
        /// <returns></returns>
        protected bool IsPropertyOrPosting => NovViolationGroupId.In(PropertyAndPostingViolationGroupIds);

        /// <summary>
        /// NovViolationGroupId == 4
        /// </summary>
        /// <returns></returns>
        protected bool IsPosting => NovViolationGroupId == (int)ViolationGroupId.Posting;

        /// <summary>
        /// NovViolationGroupId.In(4, 6)
        /// </summary>
        /// <returns></returns>
        protected bool IsPostingOrDcode => NovViolationGroupId.In(PostingAndDcodeViolationGroupIds);


        protected string NovTicketStatus => NovInformation?.TicketStatus;
        /// <summary>
        /// NovTicketStatus == "C"
        /// </summary>
        /// <returns></returns>
        protected bool NovIsCancelled => NovTicketStatus == ((char)TicketStatus.Cancelled).ToString();

        /// <summary>
        /// NovTicketStatus == "V"
        /// </summary>
        /// <returns></returns>
        protected bool NovIsVoid => NovTicketStatus == ((char)TicketStatus.Void).ToString();

        /// <summary>
        /// NovTicketStatus.In("C","V")
        /// </summary>
        /// <returns></returns>
        protected bool IsVoidOrCancelled => NovInformation?.TicketStatus.In(CancelledAndVoidTicketStatuses) ?? false;

        /// <summary>
        /// NovTicketStatus == "L"
        /// </summary>
        protected bool IsLaterService => NovTicketStatus == "L";

        #endregion

        public DateTime ThirtyDaysFromToday => DateTime.Today.AddDays(30).Date;
        public DateTime OneYearFromToday => DateTime.Today.AddYears(1).Date;

        protected ViolationDetails ViolationDetails => NovMaster?.ViolationDetails;
        protected ViolationGroups ViolationGroup => NovMaster?.ViolationGroup;

        public bool IsCancelled
        {
            get => _isCancelled;
            set
            {
                _isCancelled = value;
                NotifyPropertyChanged(nameof(Title));
            }
        }

        public bool IsVoidAction
        {
            get => _isVoidAction;
            set
            {
                _isVoidAction = value;
                NotifyPropertyChanged(nameof(Title));
            }
        }

        public virtual ICommand BackCommand => new Command(async () =>
        {
            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            WriteFieldValuesToNovMaster();
            WriteValueToCrossSettings();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
            //NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;
        });

        public virtual ICommand NextCommand => new Command(async () =>
        {

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }

            WriteFieldValuesToNovMaster();
            WriteValueToCrossSettings();
            if (ShouldSaveTicketOnNext)
            {
                await SaveTicket();
            }

            if (ShouldLockPreviousScreensOnNext)
            {
                NovMaster.NovInformation.LockPreviousScreens = true;
            }

            if (ShouldLockPlaceOfOccurrenceOnNext)
            {
                NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;
            }

            var nextVM = NextViewModel;
            if (nextVM != null)
            {
                await NavigationService.PushAsync(nextVM);
            }

            NavigationInProgress = false;


        });

        public virtual Task<List<AlertViewModel>> ValidateScreen()
        {
            return Task.FromResult(new List<AlertViewModel>());
        }

        public virtual ViewModelBase NextViewModel
        {
            get
            {
                var locator = ConcreteWorkflowFactory.GetViewModel(NovMaster.ViolationGroup.TypeName);
                var nextVM = locator.NextViewModel(this.GetType().Name, NovMaster);

                return nextVM;
            }
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                var args = new PropertyChangedEventArgs(propertyName);
                OnPropertyChanged(args);
                PropertyChanged(this, args);
                propertyChangedTime = DateTime.Now;
            }
        }

        protected void RaiseCanExecuteChanged(ICommand command)
        {
            var changedCommand = command as Command;

            if (changedCommand != null)
            {
                changedCommand.ChangeCanExecute();
            }
        }

        protected async Task<bool> DisplayAlert(string errorNumber, string workflowMessage, string okAction = "Ok")
        {
            AlertViewModel vm = new AlertViewModel(errorNumber, workflowMessage, okAction);
            await AlertService.DisplayAlert(vm);

            return false;
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            // Fill in any behavior that needs to happen for all Property Changed events
        }

        public virtual void OnAppearing()
        {
            // Can be overriden in a derived class to add behavior when a view becomes active
            isForeground = true;
            if (autoTimeOutLooping == false && NeedNotCheckLogOff == false)
            {
                AutoLogOff();
            }
        }

        public virtual void OnDisappearing()
        {
            // Can be overriden in a derived class to add behavior when a view becomes inactive

            isForeground = false;
        }

        // Can be overriden in a derived class to add loading behavior when a view becomes active
        public virtual Task LoadAsync()
        {
            return Task.CompletedTask;
        }

        public virtual void WriteFieldValuesToNovMaster()
        {
            // Override in derived classes
        }
        public virtual void WriteValueToCrossSettings()
        {
            // Override in derived classes
        }

        public virtual bool ShouldSaveTicketOnNext => true;

        public virtual bool ShouldLockPreviousScreensOnNext => false;

        public virtual bool ShouldLockPlaceOfOccurrenceOnNext => false;

        public async Task SaveTicket()
        {
            try
            {
                await NovService.SaveViolation(NovMaster);
            }
            catch (Exception exc)
            {
                MessageCenter.Send("Error saving violation.");
            }
        }

        public virtual bool ShowBackButton => true;

        public virtual bool ShowCancelButton => false;

        public virtual bool ShowNextButton => true;

        public virtual bool ShowConfirmButton => false;

        public virtual bool ShowPlusButton => false;

        public virtual ICommand PlusCommand => null;

        public virtual bool ShowMenuButton => true;

        public virtual List<string> MenuItems
        {
            get
            {
                var menuItems = new List<string>();
                if (ScreenSpecificMenuItems != null)
                {
                    menuItems.AddRange(UserSession.Department == SHERIFF_DEP
                        ? ScreenSpecificMenuItems.Where(i => i != "Vehicle/Radio Information")
                        : ScreenSpecificMenuItems);

                    menuItems.Add("——————————");
                }

                if (ShowHelpMenu) { menuItems.AddRange(HelpMenuItems); }
                if (ShowActionMenu) { menuItems.AddRange(ActionMenuItems); }
                if (ShowCancelMenu) { menuItems.AddRange(CancelMenuItems); }

                return menuItems;
            }
        }

        public virtual ICommand MenuItemTappedCommand => new Command(MenuItemTapped);

        public virtual bool ShowHelpMenu => true;

        public virtual List<string> ScreenSpecificMenuItems => null;

        public virtual List<string> HelpMenuItems => new List<string> { "NOVAS Help", "About NOVAS" };

        public virtual bool ShowActionMenu => true;

        public virtual List<string> ActionMenuItems => new List<string> {"Device Status" };

        public virtual bool ShowCancelMenu => NovMaster != null && NovMaster.NovInformation?.TicketStatus != "C" && NovMaster.NovInformation?.TicketStatus != "V" && !IsCancelled && !IsVoidAction;

        //public virtual List<string> CancelMenuItems => (!NovMaster?.NovInformation?.VoidSet ?? false)
        //    ? new List<string> {"Cancel"}
        //    : new List<string> {"Void"};
        public virtual List<string> CancelMenuItems => (NovMaster==null || NovMaster.NovInformation==null || NovMaster.NovInformation.NovNumber==0)?
            new List<string> {"Cancel"} : new List<string> {"Void"};

    public async virtual void MenuItemTapped(object item)
        {
            if ((string)item == "Log Off")
            {
                var alert = new AlertViewModel("11001", WorkFlowMessages.DSNYMSG_06_ConfirmLogOff, okTitle: "Yes", okAction:LogOff, cancelTitle: "No");
                await AlertService.DisplayAlert(alert);
            }
            else if ((string)item == "Cancel")
            {
                var alert = new AlertViewModel("12001", WorkFlowMessages.DSNYMSG_ConfirmCancel, okTitle: "Yes", okAction:Cancel, cancelTitle: "No");
                await AlertService.DisplayAlert(alert);
            }
            else if ((string)item == "Void")
            {
                var alert = new AlertViewModel("12002", WorkFlowMessages.DSNYMSG_ConfirmVoid, okTitle: "Yes", okAction:VoidAction, cancelTitle: "No");
                await AlertService.DisplayAlert(alert);
            }
            else if ((string)item == "About NOVAS")
            {
                AboutAction();
            }
            else if ((string)item == "Device Status")
            {
                DeviceStatusAction();
            }
            else if ((string) item == "NOVAS Help")
            {
                NovasHelp();
            }
            else if ((string) item == "Pin Tool")
            {
                PinTool();
            }
            else if ((string)item == "Main Page")
            {
                MainPage();
            }
        }

        public async Task AutoLogOff()
        {
            var isFullSyncCalled = CrossSettings.Current.GetValueOrDefault("SyncStatus_FullSyncCalled_", string.Empty);

            if (string.IsNullOrEmpty(isFullSyncCalled))
                isFullSyncCalled = "false";

            if (Convert.ToBoolean(isFullSyncCalled)) {
                ForceLogOff();
                return;
            } else
            if (GetType() == typeof(SyncStatusViewModel)) {
                ForceLogOff();
                return;
            }
            //if (GetType() != typeof(LoginViewModel)&&GetType() != typeof(TimeoutViewModel) && (GetType()==typeof(SyncStatusViewModel)? _isTimeOutWithLogin:true))
            if (GetType() != typeof(LoginViewModel) && GetType() != typeof(TimeoutViewModel) )
            {               
                while (!NeedNotCheckLogOff)
                {
                    autoTimeOutLooping = true;
                    await Task.Delay(10000);
                    if (isForeground == true)
                    {
                        if (DateTime.Now.Subtract(propertyChangedTime).TotalSeconds > autoLogoutTime)
                        {
                            if (!NeedNotCheckLogOff)
                            {
                                await NavigationService.PushModalAsync(new TimeoutViewModel()
                                {
                                    UserSession = UserSession
                                });
                            }
                            NeedNotCheckLogOff = true;
                            autoTimeOutLooping = false;
                        }

                    }

                }
            }
        }

        public virtual async void ForceLogOff()
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            // without User Login
            if (UserSession == null)
            {
                await NavigationService.PopToLoginScreenAsync();
                UserSession = null;
                NavigationInProgress = false;
            }
            else // User has Login
            {

                // ForceLogOff skip the screen vehicle
                //bool existsData = await VehicleService.isVehicleCurrent(UserSession.UserId);
                bool existsData = false;

                if (UserSession.Department == SHERIFF_DEP)
                {
                    existsData = false; ;
                    UserSession.DutyHeader.LogoutTimestamp = DateTime.Now;
                    await NovService.SaveDutyHeader(UserSession);
                    await NavigationService.PopToLoginScreenAsync();
                }
                //if (existsData)
                //{
                //    VehicleDataViewModel vm = new VehicleDataViewModel(existsData) { UserSession = UserSession, IsLogOff = true };
                //    //await NavigationService.PushModalAsync(vm);
                //    //User Session is set to null inside the Vehicle/Radio ViewModel

                //    await NavigationService.PopToLoginScreenAsync();
                //    UserSession = null;
                //}
                //else
                if (!existsData)
                {
                    await NavigationService.PopToLoginScreenAsync();
                    UserSession = null;
                }
            }
            NavigationInProgress = false;

        }
        public virtual async void LogOff()
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            bool existsData = await VehicleService.isVehicleCurrent(UserSession.UserId);

            if (UserSession.Department == SHERIFF_DEP)
            {
                existsData = false; ;
                UserSession.DutyHeader.LogoutTimestamp = DateTime.Now;
                await NovService.SaveDutyHeader(UserSession);
            }
            if (existsData)
            {
                VehicleDataViewModel vm = new VehicleDataViewModel(existsData) { UserSession = UserSession, IsLogOff = true };
                await NavigationService.PushModalAsync(vm);
                //User Session is set to null inside the Vehicle/Radio ViewModel
            }
            else
            {
                await NavigationService.PopToLoginScreenAsync();
                UserSession = null;
            }

            NavigationInProgress = false;
        }

        public virtual void Cancel()
        {
            IsCancelled = true;
        }

        public virtual void VoidAction()
        {
            //Need to override void in PoO and Violation Details
            IsVoidAction = true;
        }

        public virtual async void AboutAction()
        {
            await NavigationService.PushModalAsync(new AboutNovasViewModel() {
                UserSession = NovMaster?.UserSession ?? UserSession
            });
        }

        public virtual async void PinTool()
        {
            await NavigationService.PushModalAsync(new SetPinViewModel()
            {
                UserSession = NovMaster?.UserSession ?? UserSession
            });
        }

        public virtual async void MainPage()
        {
            await NavigationService.PushModalAsync(new MainPageViewModel()
            {
                UserSession = NovMaster?.UserSession ?? UserSession
            });
        }

        public virtual async void DeviceStatusAction()
        {
            await NavigationService.PushModalAsync(new DeviceStatusViewModel()
            {
                UserSession = NovMaster?.UserSession ?? UserSession
            });
        }

        public virtual async void NovasHelp()
        {
            await NavigationService.PushModalAsync(new HelpViewModel()
            {
                UserSession = NovMaster?.UserSession ?? UserSession
            });
        }

        public async Task SaveViolator()
        {
            try
            {
                await PlaceOfOccurrenceService.SaveViolator(NovMaster);
            }
            catch (Exception exc)
            {
                MessageCenter.Send("Error saving violator");
            }
        }
        
        public static ViewModelBase GetViewModelBase(string viewModelName)
        {
            var t = Type.GetType(viewModelName);
            
            if (t == null)
                return null;

            try
            {
                var instance = Activator.CreateInstance(t);

                return (ViewModelBase)instance;
            } catch (Exception ex)
            {
                return null;
            }
        }
        //private string SyncServiceBaseUrl => ServiceUrlResources.ResourceManager.GetString("SyncServiceBaseUrl");
        //private string SyncServiceEndpoint => ServiceUrlResources.ResourceManager.GetString("SyncServiceEndpoint");
        //protected Uri SyncServiceBaseUri => !string.IsNullOrWhiteSpace(SyncServiceBaseUrl) ? new Uri(SyncServiceBaseUrl) : null;
        //protected Uri SyncServiceUri => SyncServiceBaseUri != null && !string.IsNullOrWhiteSpace(SyncServiceEndpoint)
        //    ? new Uri(SyncServiceBaseUri, SyncServiceEndpoint)
        //    : null;

        //protected string SyncServiceUrl => SyncServiceUri?.ToString();
        // protected string SyncServiceUrl => ApiUrl.SyncServiceUrl;

        //protected string SyncServiceUrl => "";

        //ServicUrl

        public void getAppRuntimeMemory()
        {
            //var deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            var memory = DependencyResolver.Get<IAppRuntimeSettings>().DeviceMemory;

        }
    }
}
