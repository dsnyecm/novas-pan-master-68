﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

using DSNY.Novas.Common;
using DSNY.Novas.Services;
using DSNY.Novas.ViewModels.Utils;
using Plugin.Settings;
using DSNY.Novas.Models;

namespace DSNY.Novas.ViewModels
{
    public class AffirmationSignatureViewModel : ViewModelBase
    {
        private bool _isAppearInHearingEnabled;
        private bool _isAppearInHearingChecked;
        private bool _isNovAffirmed;
        private bool _isPetitionerVisible;
        private bool _isServiceTextVisible;
        private bool _isObserveTextVisible;
        private bool _isMailInPenaltyVisible;
        private bool _isMailInPenaltyChecked;
        private byte[] _signatureImage;
        private string _code;
        private string _petitionerHearinglabel;

        private readonly INovService _novService;
        private readonly INovasUserService _userService;
        private readonly IPersonnelDataService _personnelDataService;
        private readonly IAffirmationService _affirmationService;
        private readonly ILookupService _lookupService;
        private readonly IPersonBeingServedService _personBeingServedService;

        public override string Title
        {
            get
            {
                var title = "NOV Affirmation Signature";

                if (NovInformation.TicketStatus == "C")
                    title += " - Cancel";
                else if (NovInformation.TicketStatus == "V" || IsVoidAction)
                    title += " - Void";

                return title;
            }
        }

        public AffirmationSignatureViewModel()
        {
            _novService = DependencyResolver.Get<INovService>();
            _userService = DependencyResolver.Get<INovasUserService>();
            _personnelDataService = DependencyResolver.Get<IPersonnelDataService>();
            _affirmationService = DependencyResolver.Get<IAffirmationService>();
            _lookupService = DependencyResolver.Get<ILookupService>();
            _personBeingServedService = DependencyResolver.Get<IPersonBeingServedService>();

            IsPetitionerVisible = true;
            IsObserveTextVisible = true;
            IsServiceTextVisible = false;
            IsMailInPenaltyVisible = true;
        }

        public override async Task LoadAsync()
        {
            var petitionerWishestoAppearTable = await _lookupService.GetLookupTable("Petitioner wishes to appear");
            var match = petitionerWishestoAppearTable.Find(_ => _.Code == NovMaster.NovInformation.ViolationCode);

            var mailInPenaltyTable = await _lookupService.GetLookupTable("No Mail-In Penalty");
            var mailInPenaltyMatch = mailInPenaltyTable.Find(_ => _.Code == NovMaster.NovInformation.ViolationCode);

            List<LookupTable> str;
            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                PetitionerHearingLabel = "Petitioner wishes to appear if there were to be a hearing";
                str = await _lookupService.GetLookupTable("DOF");
            }
            else
            {
                PetitionerHearingLabel = "Petitioner wishes to appear if hearing";
                str = await _lookupService.GetLookupTable("Dept of Sanitation");
            }

            if (str != null && str.Count > 0)
            {
                NovMaster.NovInformation.AgencyId = str[0].Code;
                Code = str[0].Description;
            }

            IsMailInPenaltyChecked = mailInPenaltyMatch != null;

            IsAppearInHearingChecked = false;
            IsAppearInHearingEnabled = true;

            // Petitioner Wishes To Appear if Hearing box checked by default for two conditions 

            // 1. Dated: 08/28/2018-- All violations in PAN BS MISC table (e.g: SL7, SL5) that has Field 1 "petitioner wishes to appear" will have box checked by default; see below also
            if (match != null)
            {
               // NovMaster.NovInformation.IsPetitionerCourtAppear = "Y";
                IsAppearInHearingChecked = true;
            }

           
            // 2.  Dated: 08/28/2018-- all posting violations  except "GB1" will have checkbox checked by default; see above also 
            if(NovMaster.ViolationGroup.ViolationGroupId == 4 && !NovMaster.NovInformation.ViolationCode.Equals("GB1"))
            {

                IsAppearInHearingChecked = true;
            }
            //IsAppearInHearingEnabled = !(NovMaster.NovInformation.ViolationCode.In(new []{ "GB1", "S33" }) || NovMaster.ViolationGroup.ViolationGroupId == 4);

            IsMailInPenaltyVisible = NovMaster.UserSession.Department != SHERIFF_DEP;

            //SignatureImage = (await _userService.GetUser(NovMaster.NovInformation.UserId)).SignatureBitmap;
           SignatureImage = (await _userService.GetUserNov(NovMaster.NovInformation.UserId)).SignatureBitmap;
        }

        public string Code
        {
            get => _code;
            set { _code = value; NotifyPropertyChanged(); }
        }

        public bool IsAppearInHearingEnabled
        {
            get => _isAppearInHearingEnabled;
            set { _isAppearInHearingEnabled = value; NotifyPropertyChanged(); }
        }

        public bool IsAppearInHearingChecked
        {
            get => _isAppearInHearingChecked;
            set { _isAppearInHearingChecked = value; NotifyPropertyChanged(); }
        }

        public bool IsMailInPenaltyChecked
        {
            get => _isMailInPenaltyChecked;
            set { _isMailInPenaltyChecked = value; NotifyPropertyChanged(); }
        }

        public bool IsNovAffirmed
        {
            get => _isNovAffirmed;
            set { _isNovAffirmed = value; NotifyPropertyChanged(); }
        }

        public byte[] SignatureImage
        {
            get => _signatureImage;
            set { _signatureImage = value; NotifyPropertyChanged(); }
        }

        public bool IsPetitionerVisible
        {
            get => _isPetitionerVisible;
            set { _isPetitionerVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsServiceTextVisible
        {
            get => _isServiceTextVisible;
            set { _isServiceTextVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsMailInPenaltyVisible
        {
            get => _isMailInPenaltyVisible;
            set { _isMailInPenaltyVisible = value; NotifyPropertyChanged(); }
        }

        public bool IsObserveTextVisible
        {
            get => _isObserveTextVisible;
            set { _isObserveTextVisible = value; NotifyPropertyChanged(); }
        }

        public string PetitionerHearingLabel
        {
            get => _petitionerHearinglabel;
            set { _petitionerHearinglabel = value; NotifyPropertyChanged(); }
        }

        public override List<string> CancelMenuItems
        {
            get => new List<string> { "Void" };
        }

        public override ICommand NextCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            var alerts = await ValidateScreen();
            foreach (AlertViewModel alert in alerts)
            {
                if (!await AlertService.DisplayAlert(alert) || !alert.ShouldContinueOnOk)
                {
                    NavigationInProgress = false;
                    return;
                }
            }

            await CalculateHearingDate();

            WriteFieldValuesToNovMaster();

            if (NovMaster.NovInformation.TicketStatus != "C")
            {
               await _affirmationService.SignNov(NovMaster);
               await SaveTicket();
            }
            else
            {
                await _novService.SaveCancelNovInformation(NovMaster); // It doesn't appear that the old app actually saves these values, even though they have a table for it
            }

            if (NovMaster.NovInformation.TicketStatus == "C" || NovMaster.NovInformation.TicketStatus == "V")
            {
                await NavigationService.PopToSummaryScreenAsync();
            }
            else
            {
                var nextVM = NextViewModel;
                if (nextVM != null)
                {
                    await NavigationService.PushAsync(nextVM);
                }
            }

            NavigationInProgress = false;
        });
        
        private async Task CalculateHearingDate()
        {
            //await CalculateHearingDate_Dof();
            CourtLocations courtLocation = new CourtLocations();
            if (NovMaster.UserSession.Department == SHERIFF_DEP)
            {
                //courtLocation.CourtBoroCode = "4";
                courtLocation.CourtBoroCode = NovMaster.NovInformation.PlaceBoroCode;
            }
            else
            {
                courtLocation = await _affirmationService.GetCourtLocation(NovMaster.ViolationDetails.ViolationCode, NovMaster.NovInformation.PlaceBoroCode);
            }
            //var courtLocation = await _affirmationService.GetCourtLocation(NovMaster.ViolationDetails.ViolationCode, NovMaster.NovInformation.PlaceBoroCode);
            var hearingTime = NovMaster.UserSession.HearingTimeStamp.TimeOfDay;

            // TODO: Verify this logic - this is probably why we're seeing a different value on the ticket in NH-472
            if (courtLocation == null)
            {
                var newHearingDate = default(DateTime);

                if (NovMaster.NovInformation.ViolationGroupId == 4) // If Posting, find the next Friday that isn't a Holiday
                {
                    newHearingDate = NovMaster.UserSession.ValidHearingDates?.FindAll(_ => _.DayOfWeek.ToString() == "Friday").FirstOrDefault() ?? default(DateTime);
                }

                // If we found a new hearing date, use the hearing time that was selected on Personnel Data  (Hearing times are determined by Agency)
                if (newHearingDate != default(DateTime))
                {
                    NovMaster.NovInformation.HearingTimestamp = newHearingDate + hearingTime;
                }
                else // Otherwise just use the initially selected date / time from Personnel Data
                {
                    NovMaster.NovInformation.HearingTimestamp = NovMaster.UserSession.HearingTimeStamp;
                }
            }
            else // If we have a CourtBoroCode, caclulate a valid hearing date for that court
            {
                //NH-1026
                //var allDates = await _personnelDataService.GetHearingDatesAsync(Convert.ToInt32(courtLocation.CourtBoroCode));
                
                // To fix hearing date issue: take difference only days --- Do not consider time part.
                //TimeSpan d = NovInformation.HearingTimestamp - DateTime.Now;
                TimeSpan d = NovInformation.HearingTimestamp.Date - DateTime.Now.Date;
                var hearingFromDate = new DateTime();
                if (NovMaster.NovInformation.ViolationGroupId == 4 || NovMaster.NovInformation.ViolationGroupId == 1|| NovMaster.NovInformation.ViolationGroupId == 5 || NovMaster.UserSession.Department == SHERIFF_DEP)
                    hearingFromDate = DateTime.Today.AddDays(d.Days).Date;
                else
                    hearingFromDate = DateTime.Today.AddDays(30).Date;
                var hearingLastDate = DateTime.Today.AddYears(1).Date;

                //NH-1026
                //var totalDatesAfterCurrent = allDates.FindAll(_ => _.TimeStamp >= hearingFromDate
                //                                                   && _.TimeStamp <= hearingLastDate).Select(_ => _.TimeStamp).ToList();
                //var holidayList = await _personnelDataService.GetHolidaysAsync();
                //var daysInHolidayEnum = holidayList.FindAll
                //(_ => _.EffectiveDate >= hearingFromDate
                //      && _.EffectiveDate <= hearingLastDate
                //      && (courtLocation.CourtBoroCode.Equals(_.BoroCode) || courtLocation.CourtBoroCode.Equals("0"))).Select(_ => _.EffectiveDate.Date).ToList();
                //var validDates = totalDatesAfterCurrent.Except(daysInHolidayEnum).ToList();



                var holidaysWithinRange = await _personnelDataService.GetHolidaysBetweenAsync(Convert.ToInt32(courtLocation.CourtBoroCode), hearingFromDate, hearingLastDate);
                var hearingDatesIncludeHolidays = await _personnelDataService.GetHearingDatesBetweenAsync(Convert.ToInt32(courtLocation.CourtBoroCode), hearingFromDate, hearingLastDate);

                List<DateTime> validDates = hearingDatesIncludeHolidays.Except(holidaysWithinRange).ToList();

                var firstValidDate = new DateTime();
                if (NovMaster.UserSession.Department == SHERIFF_DEP)
                {
                    firstValidDate = validDates.FindAll
                      (_ => _.DayOfWeek.ToString() != "Saturday"
                      && _.DayOfWeek.ToString() != "Sunday").FirstOrDefault();

                    var validDays = await _lookupService.GetLookupTable("Court Days");
                    validDays = validDays.Where(validDay => validDay.Description.Equals(firstValidDate.DayOfWeek.ToString())).ToList();
                    if(validDays.Count>0)
                    {
                        courtLocation.CourtBoroCode = validDays[0].Code;
                    }
                    else
                    {
                        courtLocation.CourtBoroCode = "";
                    }
                   

                    var boros = await _personBeingServedService.GetBoros();
                    NovMaster.AffidavitOfService.Comments = string.Format("at the {0} ", boros.Find(_ => _.BoroId.Equals(courtLocation.CourtBoroCode)).Name);
                }
                else
                {
                    if (NovMaster.NovInformation.ViolationGroupId == 4)
                    {
                        firstValidDate = validDates.FindAll
                        (_ => _.DayOfWeek.ToString() == "Friday").FirstOrDefault();
                    }
                    else
                    {
                        firstValidDate = validDates.FindAll
                        (_ => _.DayOfWeek.ToString() != "Saturday"
                        && _.DayOfWeek.ToString() != "Sunday").FirstOrDefault();
                    }
                }
                
                
                if (firstValidDate != default(DateTime))
                {
                    NovMaster.NovInformation.HearingTimestamp = firstValidDate + hearingTime;
                }
                else
                {
                    NovMaster.NovInformation.HearingTimestamp = NovMaster.UserSession.HearingTimeStamp;
                }
            }
        }
        private async Task CalculateHearingDate_Dof()
        {
            //courtLocation = await _affirmationService.GetCourtLocation(NovMaster.ViolationDetails.ViolationCode, NovMaster.NovInformation.PlaceBoroCode);
           var dt= NovMaster.UserSession.HearingTimeStamp;

            var validDays = await _lookupService.GetLookupTable("Court Days");

            var hearingday=validDays.Where(m => m.Code == NovMaster.NovInformation.PlaceBoroCode).FirstOrDefault().Description; ;

            var holidayList = await _personnelDataService.GetHolidaysAsync();

            //find the hearing day for hearing date if selected date is not right find out next right date.
            if(dt.DayOfWeek.ToString() != hearingday)
            {
                dt = GetNextWeekday(hearingday);
                //if(holidayList.Where(m=>m.EffectiveDate.ToString()=="")
            }
            
        }

         DateTime GetNextWeekday(string day)
        {
            DateTime result = DateTime.Now.AddDays(1);
            while (result.DayOfWeek.ToString() != day)
                result = result.AddDays(1);
            return result;
        }

        public override async Task<List<AlertViewModel>> ValidateScreen()
        {
            var alerts = new List<AlertViewModel>();

            if (!IsNovAffirmed)
            {
                alerts.Add(new AlertViewModel("14001", WorkFlowMessages.DSNYMSG_26_EmptySignature, elementName: "IsNovAffirmed"));
            }

           /* else if (NovMaster.NovInformation.TicketStatus == "C")
            {
                await _novService.DeleteNovInfo();
                NovMaster.NovInformation.NovNumber = (await _novService.GetNextNovNumber(false)).GetValueOrDefault();

                CalculateCheckSum();
            }
            else
            {
                await _novService.DeleteNovInfo();
                await _novService.UpdateNovData(NovMaster);

                CalculateCheckSum();

                if (NovMaster.NovInformation.TicketStatus == "V")
                {
                    NovMaster.UserSession.DutyHeader.VoidCount += 1;
                }
                else
                {
                    NovMaster.UserSession.DutyHeader.TicketCount += 1;
                }
                
                await _novService.SaveDutyHeader(NovMaster.UserSession);
            }*/

            return alerts;
        }

        public override void WriteFieldValuesToNovMaster()
        {
            NovInformation.IsPetitionerCourtAppear = IsAppearInHearingChecked ? "Y" : "N";

            //Bug #: NH-1251
            //1. Date: 09/10/2018 As per Navi comments
            //2.	Resp1FirstName should be empty for ALL property ticket and Commercial tickets.
            //3.	Resp1BoroCode should be same as PlaceBoroCode for ALL property ticket and Commercial tickets.
            if (NovInformation.ViolationGroupId.In(new []{ 2 , 3 }))
            {
                NovInformation.Resp1LastName = "";
                NovInformation.Resp1FirstName = "";
                NovInformation.Resp1MiddleInitial = "";
                NovInformation.Resp1BoroCode = Convert.ToString(NovInformation.PlaceBoroCode);

                NovInformation.IsResp1AddressHit = Convert.ToString(NovInformation.IsResp1AddressHit);
                NovInformation.Resp1Sex = "";
                NovInformation.PropertyBBL = "";
                NovInformation.Resp1DistrictId = "";
                NovInformation.Resp1SectionId = "";

                NovInformation.PlaceLastName = "";
                NovInformation.PlaceFirstName = "";
                NovInformation.PlaceMiddleInitial = "";

                //NovInformation.PlaceBBL = "";
                NovInformation.PlaceSideOfStreet = Convert.ToString(NovInformation.PlaceSideOfStreet);
                NovInformation.BuildingType = Convert.ToString(NovInformation.BuildingType);

                NovInformation.IsSent = "N";
                NovInformation.FreeAddrees = Convert.ToString(NovInformation.FreeAddrees);
            }

            NovInformation.IsMailInPenaltyChecked = IsMailInPenaltyChecked;

            if (IsVoidAction)
                NovInformation.TicketStatus = "V";
        }

        public override ICommand BackCommand => new Command(async () =>
        {
            if (NavigationInProgress) { return; }
            NavigationInProgress = true;

            if (UserSession != null)
            {
                UserSession.TimeoutTimeStamp = DateTime.Now;
            }
            else if (NovMaster?.UserSession != null)
            {
                NovMaster.UserSession.TimeoutTimeStamp = DateTime.Now;
            }

            WriteFieldValuesToNovMaster();
            await NavigationService.PopAsync();
            NavigationInProgress = false;
            //NovMaster.NovInformation.LockPlaceOfOccurrenceScreen = true;

            NovMaster.NovInformation.IsBusinessNameRetained = true;
        });
    }
}
