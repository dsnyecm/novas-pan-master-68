﻿using System;

namespace DSNY.Novas.Models
{
    public class DeviceTicketRanges
    {
        public string DeviceId { get; set; }
        public int NovRangeId { get; set; }
        public string RangeStatus { get; set; }
        public Int64 TicketStartNumber { get; set; }
        public Int64 TicketEndNumber { get; set; }
        public Int64? LastTicketNumber { get; set; }
    }
}