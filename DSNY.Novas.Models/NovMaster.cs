﻿using System.Collections.Generic;

namespace DSNY.Novas.Models
{
    public class NovMaster
    {
        public UserSession UserSession { get; set; }
        public List<NovData> NovData { get; set; }
        public NovInformation NovInformation { get; set; }
        public NovCertificate NovCertificate { get; set; }
        public CancelNovInformation CancelNovInformation { get; set; }
        public CancelNovData CancelNovData { get; set; }
        public ViolationGroups ViolationGroup { get; set; }
        public ViolationDetails ViolationDetails { get; set;  }
        public AffidavitOfService AffidavitOfService { get; set; }
        public List<AffidavitOfServiceTran> AffidavitOfServiceTranList { get; set; }
        public ViolationTypes ViolationTypes { get; set; }
        public Violator Violator { get; set; }
        public bool IsMakeSimilar { get; set; }
    }
}