﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class LocalDeviceSettings
    {
        public int LocalDeviceSettingKey { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
