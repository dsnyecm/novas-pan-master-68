﻿namespace DSNY.Novas.Models
{
    public class StreetCodeMaster
    {
        public string BoroCode { get; set; }
        public int StreetCode { get; set; }
        public string StreetName { get; set; }
    }
}
