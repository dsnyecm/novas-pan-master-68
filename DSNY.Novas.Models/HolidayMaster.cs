﻿using System;

namespace DSNY.Novas.Models
{
    public class HolidayMaster
    {
        public string BoroCode { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime EndEffDate { get; set; }

        public string Comments { get; set; }
    }
}