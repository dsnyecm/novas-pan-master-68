﻿namespace DSNY.Novas.Models.Enums
{
    public enum ViolationGroupId
    {
        Action = 1,
        NonCommercialProperty = 2,
        CommercialProperty = 3,
        Posting = 4,
        VacantLot = 5,
        DCodesAndPosting = 6,
        Tobacco = 7
    }
}
