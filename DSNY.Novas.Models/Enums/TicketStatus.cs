﻿namespace DSNY.Novas.Models.Enums
{
    public enum TicketStatus
    {
        Cancelled = 'C',
        Void = 'V'
    }
}
