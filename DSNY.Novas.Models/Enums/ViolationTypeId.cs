﻿namespace DSNY.Novas.Models.Enums
{
    public enum ViolationTypeId
    {
        Action = 'A',
        CommercialProperty = 'C',
        MultipleDwellingProperty = 'M',
        DCode = 'O',
        Posting = 'O',
        VacantLot = 'O',
        ResidentialProperty = 'R',
        Tobacco = 'T'
    }
}
