﻿namespace DSNY.Novas.Models
{
    public class ViolationLawCodes
    {
        public string LawSection { get; set; }
        public string LawDescription { get; set; }
    }
}