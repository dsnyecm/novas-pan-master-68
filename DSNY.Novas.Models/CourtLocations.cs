﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class CourtLocations
    {
        public string ViolationCode { get; set; }
        public string PlaceBoroCode { get; set; }
        public string CourtBoroCode { get; set; }
    }
}
