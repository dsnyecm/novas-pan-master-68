﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class Violator
    {
        public string BoroCode { get; set; }
        public int StreetCode { get; set; }
        public string LowHouseNo { get; set; }
        public DateTime BatchDate { get; set; }
        public string BusinessName { get; set; }
        public string ViolationCode { get; set; }
        public string NextViolationCode { get; set; }
        public float MinFine { get; set; }
        public float MaxFine { get; set; }
        public int NoOfTimes { get; set; }
        public string DisplaySign { get; set; }

        public string ViolatorToString => BusinessName != "None of the Above" ? BusinessName + " ((" + ViolationCode.Trim() + "->" + NextViolationCode.Trim() + "))" : BusinessName;
    }
}
