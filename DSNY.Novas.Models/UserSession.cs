﻿using System;
using System.Collections.Generic;

namespace DSNY.Novas.Models
{
    public class UserSession
    {
        public string UserId { get; set; }
        public string OfficerName { get; set; }
        public string AbbrevName { get; set; }
        public string AgencyId { get; set; }
        public string Title { get; set; }
        public string ReportLevel { get; set; }
        public DateTime HearingTimeStamp { get; set; }
        public string DefaultBoroCode { get; set; }
        public string IsSupervisor { get; set; }
        public DutyHeader DutyHeader { get; set; }
        public Printer SelectedPrinter { get; set; }
        public List<DateTime> ValidHearingDates { get; set; }
        public DateTime TimeoutTimeStamp { get; set; }

        public string Department { get; set; }
    }
}