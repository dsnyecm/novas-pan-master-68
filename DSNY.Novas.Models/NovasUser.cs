﻿namespace DSNY.Novas.Models
{
    public class NovasUser  
    {
        //this class represents NovasUserMaster
        public string UserId { get; set; }
        public string AgencyId { get; set; }
        public string AbbrevName { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string SiteId { get; set; }
        public int BoroId { get; set; }
        public string IsSupervisor { get; set; }
        public byte[] SignatureBitmap { get; set; }
    }
}