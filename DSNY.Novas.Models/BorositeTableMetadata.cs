﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class BorositeTableMetadata
    {
        public string TableName { get; set; }
        public DateTime ActivityTimestamp { get; set; }
        public string Module { get; set; }
        public string SqlStatement { get; set; }
    }
}
