﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class Printer
    {
        public string SerialNumber { get; set; }
        public string MacAddress { get; set; }
        public bool IsInRange { get; set; }
    }
}
