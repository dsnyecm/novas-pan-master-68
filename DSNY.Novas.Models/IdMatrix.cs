﻿namespace DSNY.Novas.Models
{
    public class IdMatrix
    {
        public string IdType { get; set; }
        public string IdDesc { get; set; }
        public string IsActionOff { get; set; }
        public string IsCommOff { get; set; }
        public string IsMultiOff { get; set; }
        public string IsPersonalSvc { get; set; }
        public string AskIdNo { get; set; }
        public string AskExpDate { get; set; }
        public string AskIssuedBy { get; set; }
        public string IssuedBy { get; set; }

        public string TableName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}