﻿namespace DSNY.Novas.Models
{
    public class Novas_User
    {
        //this class represents actual NovasUser
        public int DBNovasUserID { get; set; }
        public string UserID { get; set; }
        public string Salt { get; set; }
        public string UserHash { get; set; }
        public string NfcUid { get; set; }
        public string Pin { get; set; }
        public byte[] SignatureBitmap { get; set; }
    }
}
