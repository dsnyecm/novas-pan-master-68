﻿using System;
using System.Numerics;

namespace DSNY.Novas.Models
{
    public class RoutingTime
    {
        public int BoroCode { get; set; }
        public string DistrictID { get; set; }
        public string SectionId { get; set; }
        public DateTime EffectDate { get; set; }
        public string RoutingTimeAM { get; set; }
        public string RoutingTimePM { get; set; }
        public string ViolationType { get; set; }
    }
}
