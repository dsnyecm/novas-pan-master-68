﻿using System;

namespace DSNY.Novas.Models
{
    public class PropertyDetails
    {
        public Int64 Bbl { get; set; }
        public string LicenseId { get; set; }
        public int? StreetCode { get; set; }
        public int? LowHouseNo { get; set; }
        public string LowHouseSfx { get; set; }
        public string LowHouseInd { get; set; }
        public string LowHouseKey { get; set; }
        public int? HighHouseNo { get; set; }
        public string HighHouseSfx { get; set; }
        public string HighHouseInd { get; set; }
        public int? MdrNo { get; set; }
        public int? DistrictId { get; set; }
        public int? SectionId { get; set; }
        public string Pop { get; set; }
        public string ContinuousParity { get; set; }
        public string VacantLot { get; set; }
        public string OwnStatus { get; set; }
        public string FirstName { get; set; }
        public int? ZipCode { get; set; }

        public string BusinessName { get; set; }
        public string TradeName { get; set; }
        public string PremiseBuildingNumber { get; set; }
        public string PremiseStreetName { get; set; }
        public string PremsieLocation { get; set; }
        public string PremsieCity { get; set; }
        public string PremiseState { get; set; }
        public string PremiseZipCode { get; set; }
        public int PremiseCouncilDist { get; set; }
        public string PremiseHouseNumber { get; set; }
        public int PremiseCommunityDist { get; set; }
        public string MailingBuildingNumber { get; set; }
        public string MailingStreetName { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string ExpirationDate { get; set; }
    }
}
