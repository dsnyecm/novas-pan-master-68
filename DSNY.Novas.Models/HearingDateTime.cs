﻿using System;

namespace DSNY.Novas.Models
{
    public class HearingDateTime
    {
        public int AgencyId { get; set; }
        public DateTime HearingFromDate { get; set; }
        public DateTime HearingToDate { get; set; }
        public DateTime HearingTime { get; set; }
        
        public string OnlyTime => HearingTime.ToString("hh:mm tt");
    }
}