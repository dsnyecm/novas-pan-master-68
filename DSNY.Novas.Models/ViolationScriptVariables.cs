﻿namespace DSNY.Novas.Models
{
    public class ViolationScriptVariables
    {
        public string FieldName { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Mask { get; set; }
        public string MapField { get; set; }
        public string IsMultiplesAllowed { get; set; }
        public string IsOtherAllowed { get; set; }
        public string IsFreeForm { get; set; }
    }
}