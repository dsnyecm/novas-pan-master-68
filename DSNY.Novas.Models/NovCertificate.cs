﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class NovCertificate
    {
        public Int64 NovNumber { get; set; }
        public string Certificate { get; set; }
    }
}
