﻿using System;

namespace DSNY.Novas.Models
{
    public class ViolationDetails
    {
        public int ViolationDetailsKey { get; set; }
        public string ViolationCode { get; set; }
        public Int64 ViolationGroupId { get; set; }
        public string InternalShortDescription { get; set; }
        public decimal MailableAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        public string RoutingFlag { get; set; }
        public string HHTIdentifier { get; set; }
        public string ViolationCodeShortDescription { get; set; }
        public int LawId { get; set; }
        public string CodeLaw { get; set; }
    }
}