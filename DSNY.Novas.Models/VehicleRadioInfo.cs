﻿using System;

namespace DSNY.Novas.Models
{
    public class VehicleRadioInfo
    {
        public string UserId { get; set; }
        public DateTime LoginTimestamp { get; set; }
        public Int16 VehicleRadioId { get; set; }
        public string VehicleId { get; set; }
        public string RadioId { get; set; }
        public int StartMileage { get; set; }
        public int EndMileage { get; set; }
    }
}