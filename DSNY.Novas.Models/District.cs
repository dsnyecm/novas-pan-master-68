﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class District
    {
        public string BoroCode { get; set; }
        public string DistrictId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
