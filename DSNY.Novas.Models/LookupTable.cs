﻿namespace DSNY.Novas.Models
{
    public class LookupTable
    {
        public int MiscKey { get; set; }
        public string TableName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
