﻿using System;

namespace DSNY.Novas.Models
{
    public class HearingDate
    {
        public string BoroCode { get; set; }
        public DateTime CurrentDate { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}