﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class ViolationTypes
    {
        public int ViolationGroupId { get; set; }
        public string ViolationTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
    }
}
