﻿namespace DSNY.Novas.Models
{
    public class FirstDescriptors
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string ViolationTypeDesc { get; set; }
        public string GroupName { get; set; }

        public int Sort { get; set; }
        public int GoElement { get; set; }
        public string ViolationTypes { get; set; }
    }
}
