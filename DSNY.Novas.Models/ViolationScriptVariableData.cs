﻿namespace DSNY.Novas.Models
{
    public class ViolationScriptVariableData
    {
        public string FieldName { get; set; }
        public string Data { get; set; }
        public int? Sequence { get; set; }
    }
}