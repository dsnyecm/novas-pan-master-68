﻿namespace DSNY.Novas.Models
{
    public class Sections
    {
        public string DistrictId { get; set; }
        public string SectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Sequence { get; set; }
    }
}