﻿namespace DSNY.Novas.Models
{
    public class BoroMaster
    {
        public string BoroId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}