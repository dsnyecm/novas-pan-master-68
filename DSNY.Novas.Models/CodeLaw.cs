﻿namespace DSNY.Novas.Models
{
    public class CodeLaw
    {
        public string CodeLawId { get; set; }
        public string CodeLawDescription { get; set; }
    }
}