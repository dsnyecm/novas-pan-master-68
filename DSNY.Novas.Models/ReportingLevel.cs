﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class ReportingLevel
    {
        public string ReportingLevelId { get; set; }

        public string Title { get; set; }

        public short Rank { get; set; }

        public string ReportingLevelAndTitle => $"{ReportingLevelId}-{Title}";
    }
}
