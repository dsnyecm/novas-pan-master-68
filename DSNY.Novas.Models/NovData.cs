﻿using System;

namespace DSNY.Novas.Models
{
    public class NovData
    {
        public Int64 NovNumber { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
    }
}