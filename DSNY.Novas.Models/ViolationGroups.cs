﻿using System;
using System.Collections.Generic;

namespace DSNY.Novas.Models
{
    public class ViolationGroups
    {
        public int ViolationGroupId { get; set; }
        public string GroupName { get; set; }
        public string TypeName { get; set; }
        public int Sequence { get; set; }
        public string GroupAndType => $"{GroupName}-{TypeName}";
    }
}