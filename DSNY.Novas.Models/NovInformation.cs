﻿using System;
using System.Globalization;

namespace DSNY.Novas.Models
{
    public class NovInformation
    {
        public static readonly CultureInfo NovasCulture = new CultureInfo("en-US");
        public int NovInformationKey { get; set; }
        public Int64 NovNumber { get; set; }
        public string TicketStatus { get; set; }
        public DateTime IssuedTimestamp { get; set; }
        public DateTime SystemTimestamp { get; set; }
        public DateTime LoginTimestamp { get; set; }
        public string ReportLevel { get; set; }
        public string IsResp1AddressHit { get; set; }
        public string Resp1LastName { get; set; }
        public string Resp1FirstName { get; set; }
        public string Resp1MiddleInitial { get; set; }
        public string Resp1Sex { get; set; }
        public string PropertyBBL { get; set; }
        public string Resp1DistrictId { get; set; }
        public string Resp1SectionId { get; set; }
        public int Resp1StreetId { get; set; }
        public string Resp1Address { get; set; }
        public string Resp1Address1 { get; set; }
        public string Resp1City { get; set; }
        public string Resp1State { get; set; }
        public string Resp1Zip { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseAgency { get; set; }
        public string LicenseType { get; set; }
        public string IsPlaceAddressHit { get; set; }
        public string PlaceLastName { get; set; }
        public string PlaceFirstName { get; set; }
        public string PlaceMiddleInitial { get; set; }
        public string PlaceBBL { get; set; }
        public string PlaceDistrictId { get; set; }
        public string PlaceSectionId { get; set; }
        public int PlaceStreetId { get; set; }
        public string PlaceAddress1 { get; set; }
        public string PlaceAddress2 { get; set; }
        public string PlaceAddressDescriptor { get; set; }
        public string PlaceSideOfStreet { get; set; }
        public int PlaceCross1StreetId { get; set; }
        public int PlaceCross2StreetId { get; set; }
        public decimal MailableAmount { get; set; }
        /// <summary>
        /// MailableAmount.ToString("C", new CultureInfo("en-US"))
        /// </summary>
        public string MailableAmountString => MailableAmount.ToString("C", NovasCulture);
        public decimal MaximumAmount { get; set; }
        /// <summary>
        /// MaximumAmount.ToString("C", new CultureInfo("en-US"))
        /// </summary>
        public string MaximumAmountString => MaximumAmount.ToString("C", NovasCulture);
        public DateTime HearingTimestamp { get; set; }
        public string IsAppearRequired { get; set; }
        public string AlternateService { get; set; }
        public string BuildingType { get; set; }
        public string IsMultipleOffences { get; set; }
        public string DigitalSignature { get; set; }
        public string IsSent { get; set; }
        public DateTime SentTimestamp { get; set; }
        public string IsReceived { get; set; }
        public DateTime ReceivedTimestamp { get; set; }
        public string ViolationCode { get; set; }
        public string HHTIdentifier { get; set; }
        public string ViolationScript { get; set; }
        public string UserId { get; set; }
        public string VoidCancelScreen { get; set; }
        public string PlaceBoroCode { get; set; }
        public string PlaceHouseNo { get; set; }
        public string PlaceHouseNo1 { get; set; }
        public string PlaceHouseNo2 { get; set; }
        public string PlaceHouseIndetifier { get; set; }
        public string Resp1BoroCode { get; set; }
        public string Resp1HouseNo { get; set; }
        public int ViolationGroupId { get; set; }
        public string ViolationTypeId { get; set; }
        public int MDRNumber { get; set; }
        public DateTime LicenseExpDate { get; set; }
        public string BusinessName { get; set; }
        public string CheckSum { get; set; }
        public string FreeAddrees { get; set; }
        public string DeviceId { get; set; }
        public int PublicKeyId { get; set; }
        public string PrintViolationCode { get; set; }
        public string IsPetitionerCourtAppear { get; set; }
        public string LicenseTypeDesc { get; set; }
        public string ViolGroupName { get; set; }
        public string CodeLawDescription { get; set; }
        public string OfficerName { get; set; }
        public string LawSection { get; set; }
        public string AbbrevName { get; set; }
        public string AgencyId { get; set; }
        public string Title { get; set; }
        public int OrigViolationGroupId { get; set; }
        public string OrigViolationTypeId { get; set; }
        public string OrigViolationCode { get; set; }
        public string OrigHHTIdentifier { get; set; }
        public bool IsBusiness { get; set; }
        public string DisplayAddress { get; set; }
        public bool LockPreviousScreens { get; set; }
        public bool LockPlaceOfOccurrenceScreen { get; set; }
        public bool IsAddressOverridden { get; set; }
        public string RoutingTime { get; set; }
        public bool IsMultipleAttempts { get; set; }
        public bool VoidSet { get; set; }
        public string NovNumberAsString { get; set; }
        public bool IsIdChecked { get; set; }
        public bool IsIssuedByChecked { get; set; }

        public string ViolationCodeShortDescription { get; set; }
        public string SummaryOfLaw { get; set; }
        public string TicketPrintingType { get; set; }
        public bool IsBusinessNameRetained { get; set; }

        public bool IsPersonBeingServedBusi { get; set; }
        public string DLDeviceId { get; set; }

        public bool IsMailInPenaltyChecked { get; set; }
    }
}