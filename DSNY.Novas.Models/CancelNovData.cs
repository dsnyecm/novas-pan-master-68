﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Models
{
    public class CancelNovData
    {
        public string DeviceId { get; set; }
        public DateTime SystemTimestamp { get; set; }
        public string FieldName { get; set; }
        public string Data { get; set; }
    }
}
