﻿using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class WorkflowTests : UnitTestBase
    {
        private ConcreteWorkflowFactory _concreteWorkflowFactory;
        private NovMaster _novMaster;
        

        [TestInitialize]
        public void WorkflowTestsInit()
        {
            _concreteWorkflowFactory = new ConcreteWorkflowFactory();
            _novMaster = new NovMaster();
        }

        [TestMethod]
        public void ActionWorkflowTest()
        {
            var locator = _concreteWorkflowFactory.GetViewModel("Action");
            var nextVM = locator.NextViewModel("GroupTypeViewModel", _novMaster);
            Assert.IsTrue(nextVM.GetType() == typeof(PersonBeingServedViewModel));

            nextVM = locator.NextViewModel("PersonBeingServedViewModel", _novMaster);
            Assert.IsTrue(nextVM.GetType() == typeof(PersonCommercialIDViewModel));

            nextVM = locator.NextViewModel("PersonCommercialIDViewModel", _novMaster);
            Assert.IsTrue(nextVM.GetType() == typeof(PlaceOfOccurrenceViewModel));

            nextVM = locator.NextViewModel("PlaceOfOccurrenceViewModel", _novMaster);
            Assert.IsTrue(nextVM.GetType() == typeof(ViolationDetailsViewModel));

            nextVM = locator.NextViewModel("ViolationDetailsViewModel", _novMaster);
            Assert.IsTrue(nextVM.GetType() == typeof(AffirmationSignatureViewModel));
        }

        [TestMethod]
        public void PropertyWorkflowTest()
        {
            
        }
    }
}