﻿using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class AlternativeServiceAffirmationViewModelTests: UnitTestBase
    {
        [TestMethod]
        public async Task ValidateAffidavitTranSaveTest()
        {
            var novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 123456789,
                    UserId = "SEA1095",
                    PublicKeyId = 3,
                    PlaceBoroCode = "1",
                    DigitalSignature = "098409ADSF7U3MCJRURIALKJDF82930MVKA",
                    ViolationCode = "S7V",
                    CheckSum = "N",
                    DeviceId = "TestDevice",
                    OfficerName = "Alvin Joe",
                    AbbrevName = "520167",
                    AgencyId = "827",
                    Title = "SEA",
                    LoginTimestamp = new System.DateTime(2009, 1, 1),
                    IssuedTimestamp = new System.DateTime(2010, 1, 1),
                    LicenseExpDate = new System.DateTime(2010, 1, 1),
                },
                AffidavitOfService = new AffidavitOfService()
                {
                    AlternativeService1 = "1",
                    Comments = "ECB",
                    AlternativeService2 = "2",
                    ServedLocation = "T/P/O",
                },
                ViolationDetails = new ViolationDetails()
                {
                    ViolationCode = "123"
                }
               
            };
            var vm = new AlternativeServiceAffirmationViewModel() { NovMaster = novMaster, IsNovAffirmed = true };
            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
            vm.NextCommand.Execute(true);
        }

        [TestMethod]
        public async Task ValidateAffidavitTranScreenPassTest()
        {
            var vm = new AlternativeServiceAffirmationViewModel() { IsNovAffirmed = true};
            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public async Task ValidateAffidavitTranScreenFailTest()
        {
            var vm = new AlternativeServiceAffirmationViewModel() { IsNovAffirmed = false };
            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 1);
        }
    }
}
