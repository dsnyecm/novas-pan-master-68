﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PersonalServiceViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidateScreenPassTest()
        {
            var vm = new PersonalServiceViewModel();
            var alerts = await vm.ValidateScreen();

            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public async Task ValidateScreenFailTest()
        {
            var vm = new PersonalServiceViewModel();
            vm.IsAtChecked = false;
            vm.ServiceLocation = "  ";
            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault();
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "You must enter the Location of Service before continuing.");
        }

        [TestMethod]
        public void ValidateStatesTest()
        {
            var vm = new PersonalServiceViewModel();
            vm.NovMaster = new Models.NovMaster
            {
                AffidavitOfService = new Models.AffidavitOfService()
            };
            Assert.IsTrue(vm.IsAtChecked);
            Assert.IsFalse(vm.AreOptionsEnabled);
            Assert.IsTrue(vm.ServiceLocation == "T/P/O");

            vm.IsAtChecked = false;
            Assert.IsTrue(vm.AreOptionsEnabled);

            vm.IsDesignatedChecked = true;
            vm.IsAtChecked = true;
            Assert.IsFalse(vm.IsDesignatedChecked);
        }
    }
}
