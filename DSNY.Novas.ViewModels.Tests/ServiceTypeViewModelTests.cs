﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ServiceTypeViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidateNonVacantLotLoadAsync()
        {
            ServiceTypeViewModel vm = new ServiceTypeViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() {ViolationGroupId = 1},
                    AffidavitOfService = new AffidavitOfService()
                }
            };
            await vm.LoadAsync();
            Assert.IsFalse(vm.NovMaster.NovInformation.IsMultipleAttempts);
            Assert.IsNotNull(vm.NovMaster.AffidavitOfServiceTranList);
            Assert.IsTrue(vm.IsLaterEnabled);
            Assert.AreEqual(vm.AlternativeServiceTitle, "Alternative Service");
        }

        [TestMethod]
        public async Task ValidateVacantLotFirstAttemptLoadAsync()
        {
            ServiceTypeViewModel vm = new ServiceTypeViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() { ViolationGroupId = 5 },
                    AffidavitOfService = new AffidavitOfService(),
                    AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
                }
            };
            vm.NovMaster.AffidavitOfServiceTranList.Add(new AffidavitOfServiceTran() { AbbrevName = "test1" });
            await vm.LoadAsync();
            Assert.IsTrue(vm.NovMaster.NovInformation.IsMultipleAttempts);
            Assert.IsTrue(vm.IsLaterEnabled);
            Assert.AreEqual(vm.AlternativeServiceTitle, "2nd Service Attempt");
        }

        [TestMethod]
        public async Task ValidateVacantLotLastAttemptLoadAsync()
        {
            ServiceTypeViewModel vm = new ServiceTypeViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() { ViolationGroupId = 5 },
                    AffidavitOfService = new AffidavitOfService(),
                    AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
                }
            };
            vm.NovMaster.AffidavitOfServiceTranList.Add(new AffidavitOfServiceTran() { AbbrevName = "test1" });
            vm.NovMaster.AffidavitOfServiceTranList.Add(new AffidavitOfServiceTran() { AbbrevName = "test2" });
            await vm.LoadAsync();
            Assert.IsTrue(vm.NovMaster.NovInformation.IsMultipleAttempts);
            Assert.IsFalse(vm.IsLaterEnabled);
            Assert.AreEqual(vm.AlternativeServiceTitle, "Alternative Service");
        }

        [TestMethod]
        public void RadioButtonTest()
        {
            ServiceTypeViewModel vm = new ServiceTypeViewModel(){IsLaterEnabled = true};
            vm.PersonalServiceChecked = true;
            Assert.IsFalse(vm.AlternativeServiceChecked|| vm.LaterServiceChecked);
            vm.AlternativeServiceChecked = true;
            Assert.IsFalse(vm.PersonalServiceChecked || vm.LaterServiceChecked);
            vm.LaterServiceChecked = true;
            Assert.IsFalse(vm.PersonalServiceChecked || vm.AlternativeServiceChecked);
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTestAsync()
        {
            //non later service
            ServiceTypeViewModel vm = new ServiceTypeViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() { ViolationGroupId = 1 },
                    AffidavitOfService = new AffidavitOfService()
                }
            };
            vm.PersonalServiceChecked = true;
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("P", vm.NovMaster.NovInformation.AlternateService, "P");
        }
    }
}
