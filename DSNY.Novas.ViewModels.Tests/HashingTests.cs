﻿using System.Text;
using DSNY.Novas.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class HashingTests
    {
        [TestMethod]
        public void VerifyHashTest()
        {
            string correctPassword = "123456";
            string wrongPassword = "234567";
            var nfcUid = "95-93-8E-28";
            var salt = Encoding.UTF8.GetBytes("4E6AC09C59BD317D");
            var plainText = string.Format("{0}{1}", correctPassword, nfcUid);
            var sha512Hash = SimpleHash.ComputeHash(plainText, salt);

            var isValid = SimpleHash.VerifyHash(plainText, sha512Hash);
            Assert.IsTrue(isValid, "Invalid Hash");

            isValid = SimpleHash.VerifyHash(wrongPassword, sha512Hash);
            Assert.IsFalse(isValid);
        }
    }
}