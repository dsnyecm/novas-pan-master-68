﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using DSNY.Novas.Services;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.Sec;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class AffirmationSignatureViewModelTests : UnitTestBase
    {
        private string _serializedPrivate;
        private string _serializedPublic;
        private NovMaster _novMaster;

        [TestInitialize]
        public void TestInit()
        {
            AsymmetricCipherKeyPair keyPair = GenerateKeys();

            PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(keyPair.Private);
            byte[] serializedPrivateBytes = privateKeyInfo.ToAsn1Object().GetDerEncoded();
            _serializedPrivate = Convert.ToBase64String(serializedPrivateBytes);

            SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(keyPair.Public);
            byte[] serializedPublicBytes = publicKeyInfo.ToAsn1Object().GetDerEncoded();
            _serializedPublic = Convert.ToBase64String(serializedPublicBytes);

            _novMaster = new NovMaster
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 1234567,
                    Resp1FirstName = "John",
                    Resp1LastName = "Doe",
                    Resp1DistrictId = "District1",
                    Resp1SectionId = "Section1",
                    Resp1StreetId = 4,
                    IssuedTimestamp = DateTime.Now,
                    PlaceDistrictId = "District1",
                    PlaceSectionId = "Section1",
                    PlaceStreetId = 4,
                    ViolationGroupId = 1,
                    ViolationTypeId = "Action",
                    ViolationCode = "D09",
                    ViolationScript =
                        "Respondent, a registered scrap metal processor failed to report quarterly to DSNY on the total amount of metal and other recyclable materials by type, calculated by weight in tons, that has been transported out from the facility to a destination by state and county for the reporting peried_______due on ________.",
                    IsMultipleOffences = "N",
                    MailableAmount = new decimal(2.34),
                    MaximumAmount = new decimal(2.34),
                    DeviceId = "TestDevice",
                    UserId = "TestUser"
                },
                NovData = new List<NovData>()
            };
        }

        [TestMethod]
        public async Task ValidateScreenPassTest()
        {
            var vm = new AffirmationSignatureViewModel
            {
                NovMaster = new NovMaster() {
                    NovInformation = new NovInformation() { NovNumber = 123 },
                    UserSession = new UserSession
                    {
                        DutyHeader = new DutyHeader()
                    },
                    NovData = new List<NovData>()
                },
                IsNovAffirmed = true
            };

            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public async Task ValidateScreenFailTest()
        {
            var vm = new AffirmationSignatureViewModel { IsNovAffirmed = false };

            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault();
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please sign before continuing.");
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTest()
        {
            var vm = new AffirmationSignatureViewModel
            {
                IsAppearInHearingChecked = true,
                NovMaster = _novMaster
            };

            vm.WriteFieldValuesToNovMaster();
            Assert.IsTrue(vm.NovMaster.NovInformation.IsPetitionerCourtAppear == "Y");
        }

        [TestMethod]
        public async Task IsValidSignature()
        {
            var affirmationService = DependencyResolver.Get<IAffirmationService>();

            var novRepo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            var dbNovs = await novRepo.GetAsync();

            foreach (var n in dbNovs)
            {
                await novRepo.DeleteAsync(n);
            }

            var plainText = "I love Boston MA."; // hash of violation
            var deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            var dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == "TestDevice");
            var plainTextHash = SimpleHash.ComputeHash(plainText, Encoding.UTF8.GetBytes(dbDevice.Salt));
            byte[] plainHashInBytes = Encoding.UTF8.GetBytes(plainTextHash);
            dbDevice.SerializedPrivateKey = _serializedPrivate;
            await deviceRepo.UpdateAsync(dbDevice);

            var vm = new AffirmationSignatureViewModel
            {
                IsAppearInHearingChecked = true,
                NovMaster = _novMaster
            };

            vm.WriteFieldValuesToNovMaster();
            await affirmationService.SignNov(_novMaster);
            await vm.SaveTicket();
            
            try
            {
                DBNovInformation dbNov = await novRepo.GetAsync(_ => _.NovNumber == 1234567);

                byte[] signature = Convert.FromBase64String(dbNov.DigitalSignature);
                byte[] publicKeyInBytes = Convert.FromBase64String(_serializedPublic);
                AsymmetricKeyParameter key = PublicKeyFactory.CreateKey(publicKeyInBytes);
                ISigner bVerifier = SignerUtilities.GetSigner("SHA-256withECDSA");
                bVerifier.Init(false, key);
                bVerifier.BlockUpdate(plainHashInBytes, 0, plainHashInBytes.Length);
                bool passed = bVerifier.VerifySignature(signature);

                Assert.IsTrue(passed, "Violation incorrectly signed.");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public async Task DutyHeaderSaveTest()
        {
            var vm = new AffirmationSignatureViewModel() { IsNovAffirmed = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {

                },
                UserSession = new UserSession()
                {
                    DutyHeader = new DutyHeader()
                    {
                        UserId = "dsny00065",
                        LoginTimestamp = new DateTime(1900, 1, 1),
                        BoroId = "1",
                        DeviceId = "12345",
                        SiteId = "1",
                        Title = "SEA",
                        VehicleRadioId = 1,
                        //LogoutTimestamp = new DateTime(1900, 1, 1),
                        HearingDate = new DateTime(2005, 1, 1),
                        IsActingSupervisor = "Y",
                        TicketCount = 4,
                        VoidCount = 0,
                        IsSent = "N",
                        SentTimestamp = new DateTime(1900, 1, 1),
                        IsReceived = "N",
                        ReceivedTimestamp = new DateTime(1900, 1, 1),
                        ReportingTimestamp = new DateTime(1900, 1, 1),
                    }
                },
                NovData = new List<NovData>()
            };
            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        
        private AsymmetricCipherKeyPair GenerateKeys()
        {
            ECKeyPairGenerator gen = new ECKeyPairGenerator("ECDSA");
            SecureRandom secureRandom = new SecureRandom();
            X9ECParameters ecp = SecNamedCurves.GetByName("secp256k1");
            ECDomainParameters ecSpec = new ECDomainParameters(ecp.Curve, ecp.G, ecp.N, ecp.H, ecp.GetSeed());
            ECKeyGenerationParameters ecgp = new ECKeyGenerationParameters(ecSpec, secureRandom);
            gen.Init(ecgp);
            AsymmetricCipherKeyPair eckp = gen.GenerateKeyPair();

            return eckp;
        }
    }
}