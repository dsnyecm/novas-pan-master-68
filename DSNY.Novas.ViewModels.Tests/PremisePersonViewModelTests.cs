﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PremisePersonViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task LoadAsyncTestAsync()
        {
            PremisePersonViewModel vm = new PremisePersonViewModel()
            {
                NovMaster = new NovMaster()
                {
                   AffidavitOfService = new AffidavitOfService()
                   {
                       PremiseLName = "Garvey",
                       PremiseFName = "Kevin",
                       PremiseMInit = "J"
                   }
                }
            };
            await vm.LoadAsync();
            var aff = vm.NovMaster.AffidavitOfService;
            Assert.AreEqual("Garvey", aff.PremiseLName);
            Assert.AreEqual("Kevin", aff.PremiseFName);
            Assert.AreEqual("J", aff.PremiseMInit);
        }

        [TestMethod]
        public void RadioButtonTest()
        {
            PremisePersonViewModel vm = new PremisePersonViewModel();
            vm.RespondentNotPresent = true;
            Assert.IsFalse(vm.NoOfficerPresent || vm.UnableToSecureIdentification);
            vm.NoOfficerPresent = true;
            Assert.IsFalse(vm.RespondentNotPresent || vm.UnableToSecureIdentification);
            vm.FirstName = "John";
            vm.MI = "E";
            vm.LastName = "Doe";
            vm.UnableToSecureIdentification = true;
            Assert.IsFalse(vm.IsEnabled || vm.NoOfficerPresent || vm.RespondentNotPresent);
            Assert.AreEqual("", vm.FirstName);
            Assert.AreEqual("", vm.LastName);
            Assert.AreEqual("", vm.MI);
        }

        [TestMethod]
        public async Task ValidateScreenFailTest()
        {
            PremisePersonViewModel vm = new PremisePersonViewModel(){ NovMaster = new NovMaster() { AffidavitOfService = new AffidavitOfService() } };
            await vm.LoadAsync();
            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault(a => a.Title == "12054");
            Assert.IsNotNull(alert);
            vm.FirstName = "John";
            vm.LastName = "Doe";
            alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTestAsync()
        {
            PremisePersonViewModel vm = new PremisePersonViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation(),
                    AffidavitOfService = new AffidavitOfService()
                }
            };
            vm.NoOfficerPresent = true;
            vm.FirstName = "John";
            vm.LastName = "Doe";
            vm.MI = "E";
            vm.WriteFieldValuesToNovMaster();
            var aff = vm.NovMaster.AffidavitOfService;
            Assert.AreEqual(aff.AlternativeService2, "2");
            Assert.AreEqual(aff.PremiseFName, "John");
            Assert.AreEqual(aff.PremiseLName, "Doe");
            Assert.AreEqual(aff.PremiseMInit, "E");
        }
    }
}
