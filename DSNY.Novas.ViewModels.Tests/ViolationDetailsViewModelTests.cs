﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolationDetailsViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void GroupTest()
        {
            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new Models.NovMaster
                {
                    ViolationGroup = new Models.ViolationGroups()
                    {
                        GroupName = "Groupname"
                    }
                }
            };

            vm.NovMaster.ViolationGroup = new Models.ViolationGroups()
            {
                TypeName = "Typename"
            };
            Assert.IsTrue($"Group: {vm.NovMaster.ViolationGroup.GroupAndType}" == vm.Group);
        }

        [TestMethod]
        public void PropertyTest()
        {
            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new Models.NovMaster
                {
                    ViolationGroup = new Models.ViolationGroups()
                    {
                        TypeName = "Type"
                    }
                }
            };

            Assert.IsTrue($"Property: {vm.NovMaster.ViolationGroup.TypeName}" == vm.Property);
        }

        [TestMethod]
        public void SecSubTest()
        {
            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new Models.NovMaster
                {
                    NovInformation = new Models.NovInformation()
                    {
                        LawSection = "This is the law"
                    }
                }
            };

            Assert.IsTrue($"Sec/Sub: {vm.NovMaster.NovInformation.LawSection}" == vm.SecSub);
        }

        [TestMethod]
        public void ShortTest()
        {
            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new Models.NovMaster
                {
                    NovInformation = new Models.NovInformation()
                    {
                        HHTIdentifier = "Penalty for throwing doritos on the street"
                    }
                }
            };


            Assert.IsTrue($"Short: {vm.NovMaster.NovInformation.HHTIdentifier}" == vm.Short);
        }


        [TestMethod]
        public void BuildViolationDetailsTest()
        {
            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { NovNumber = 12345 },
                    NovData = new List<NovData>()
                }
            };

            vm.BuildViolationDetails("xxxx", "yyyy");

            var novData =
                vm.NovMaster.NovData.FirstOrDefault(_ => _.NovNumber == 12345 && _.FieldName == "xxxx" &&
                                                         _.Data == "yyyy");

            Assert.IsNotNull(novData);
        }

        [TestMethod]
        public async Task ShouldSaveTicketOnNextTest()
        {
            var repo = DependencyResolver.Get<IRepository<DBNovData>>();
            await repo.QueryAsync("delete from DBNovData where NovNumber = 12345");

            var vm = new ViolationDetailsViewModel
            {
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { NovNumber = 12345, Resp1Address1 = "ljkljlkj", PlaceDistrictId = "lkjlkj", PlaceSectionId = "lkjlkj", HHTIdentifier = "ljlkj", OrigHHTIdentifier = "lkjlkj" },
                    NovData = new List<NovData>()
                }
            };

            vm.BuildViolationDetails("xxxx", "yyyy");
            vm.BuildViolationDetails("aaaa", "bbbb");
            vm.BuildViolationDetails("cccc", "dddd");

            await vm.SaveTicket();
            Assert.IsNotNull(await repo.GetAsync(_ => _.NovNumber == 12345 && _.FieldName == "xxxx" && _.Data == "yyyy"));
            Assert.IsNotNull(await repo.GetAsync(_ => _.NovNumber == 12345 && _.FieldName == "aaaa" && _.Data == "bbbb"));
            Assert.IsNotNull(await repo.GetAsync(_ => _.NovNumber == 12345 && _.FieldName == "cccc" && _.Data == "dddd"));
        }
    }
}