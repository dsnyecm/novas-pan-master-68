using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class LoginViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task IsValidUserTest()
        {
            var vm = new LoginViewModel();
            string userId = "Dsny00021";
            var isValidUser = await vm.IsValidUser(userId);
            Assert.IsTrue(isValidUser, "Invalid UserId");
        }

        [TestMethod]
        public async Task InValidUserTest()
        {
            var vm = new LoginViewModel();
            string userId = "invaliduser";
            var isValidUser = await vm.IsValidUser(userId);
            Assert.IsFalse(isValidUser, "Invalid UserId");
        }

        [TestMethod]
        public async Task AuthenticateUserValidHashTest()
        {
            var vm = new LoginViewModel();
            string nfcUid = "95-93-8E-28";
            string pin = "596244";
            var userID = await vm.AuthenticateUser(pin, nfcUid);

            Assert.IsTrue(userID == "TestUser");
        }

        [TestMethod]
        public async Task AuthenticateUserInValidHashTest()
        {
            var vm = new LoginViewModel();
            string nfcUid = "95-93-8E-30";
            string pin = "596244";
            var userId = await vm.AuthenticateUser(pin, nfcUid);

            Assert.IsTrue(userId == "");
        }
    }
}
