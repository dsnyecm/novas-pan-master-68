﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using DSNY.Novas.Services;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class VehicleDataViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationAddVehicle()
        {
            VehicleDataViewModel vm = new VehicleDataViewModel(true)
            {
                VehicleNumber = "123",
                RadioNumber = "312",
                StartingMileage = 101,
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { UserId = "jes418"},
                    NovData = new List<NovData>()
                },
                UserSession = new UserSession() { UserId = "jes418"}
            };

            await vm.ExecuteNextCommand();
            IVehicleService s = new VehicleService();
            VehicleRadioInfo vehicle = await s.GetCurrentVehicleRadioInfo("jes418");
            Assert.AreEqual(vehicle.RadioId, "312");
            Assert.AreEqual(vehicle.VehicleId, "123");
            Assert.AreEqual(vehicle.StartMileage, 101);
            Assert.AreEqual(vehicle.EndMileage, 0);
        }

        
        [TestMethod]
        public async Task ValidationUpdateVehicle()
        {
            VehicleDataViewModel vm = new VehicleDataViewModel(true)
            {
                VehicleNumber = "123",
                RadioNumber = "312",
                StartingMileage = 101,
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { UserId = "jes418" },
                    NovData = new List<NovData>()
                },
                UserSession = new UserSession { UserId = "jes418"}
            };

            await vm.ExecuteNextCommand();

            VehicleDataViewModel vm2 = new VehicleDataViewModel(false)
            {
                VehicleNumber = "123",
                RadioNumber = "321",
                StartingMileage = 101,
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { UserId = "jes418" },
                    NovData = new List<NovData>()
                },
                UserSession = new UserSession { UserId = "jes418"}
            };

            vm2.NewVehicleCommand.Execute(null);

            Assert.IsTrue(vm2.IsEndingEnabled);
            Assert.IsTrue(vm2.IsBackEnabled); //documentation says back should be disabled, but in actual app it is enabled

            vm2.EndingMileage = 200;
            await vm2.ExecuteNextCommand();

            IVehicleService s = new VehicleService();
            VehicleRadioInfo vehicle = await s.GetFinishedVehicleRadioInfo("jes418");
            Assert.AreEqual(vehicle.StartMileage, 101);
            Assert.AreEqual(vehicle.EndMileage, 200);
        }
    }
}
