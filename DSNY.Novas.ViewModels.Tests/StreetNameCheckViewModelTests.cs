﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class StreetNameCheckViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationExecuteSearchSuccess()
        {
            StreetNameCheckViewModel vm = new StreetNameCheckViewModel() { SearchString = "AN", BoroId = "1"};

            await vm.ExecuteSearch();
            Assert.IsNotNull(vm.SearchResults);

            Assert.IsTrue(vm.SearchResults.Any(scm => scm.StreetName.Equals("ANDES ROAD")));
            Assert.IsTrue(vm.SearchResults.Any(scm => scm.StreetName.Equals("ANN STREET")));
            Assert.IsTrue(vm.SearchResults.Any(scm => scm.StreetName.Equals("ANTHONY & CADY STANTON CRNR")));
            Assert.IsTrue(vm.SearchResults.Any(scm => scm.StreetName.Equals("ANIBAL AVILES PLAYGROUND")));
            Assert.IsTrue(vm.SearchResults.Any(scm => scm.StreetName.Equals("ANNUNCIATION PARK")));
            Assert.IsTrue(!vm.SearchResults.Any(scm => scm.StreetName.Equals("HOLYOKE ROAD")));
        }

        [TestMethod]
        public async Task ValidationExecuteSearchFail()
        {
            StreetNameCheckViewModel vm = new StreetNameCheckViewModel() { SearchString = null, BoroId = "1"};
            await vm.ExecuteSearch();
            Assert.IsNull(vm.SearchResults);

            vm = new StreetNameCheckViewModel() { SearchString = "xyz", BoroId = "1"};
            await vm.ExecuteSearch();
            Assert.AreEqual(vm.SearchResults.Count, 0);
        }
    }
}
