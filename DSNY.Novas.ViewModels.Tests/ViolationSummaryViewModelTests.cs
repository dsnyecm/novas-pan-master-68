﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolationSummaryViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task TestBusinessLoadAsync()
        {
            NovMaster novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 12345,
                    IssuedTimestamp = new DateTime(2017, 3, 15, 8, 30, 0),
                    DisplayAddress = "1800 21st Street",
                    ViolationScript = "This is a test script",
                    BusinessName = "Mod Pizza"
                },
                AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
            };
            ViolationSummaryViewModel vm = new ViolationSummaryViewModel() { NovMaster = novMaster };
            await vm.LoadAsync();
            Assert.AreEqual("12345", vm.NovNumber);
            Assert.AreEqual("03/15/2017 08:30 AM", vm.DateOfOffense);
            Assert.AreEqual("Mod Pizza", vm.Name);
            Assert.AreEqual("1800 21st Street", vm.PlaceOfOccurrence);
            Assert.AreEqual("This is a test script", vm.ViolationScript);
            Assert.IsNull(vm.ServiceAttempts);
        }

        [TestMethod]
        public async Task TestFirstLastNameLoadAsync()
        {
            NovMaster novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 12345,
                    IssuedTimestamp = new DateTime(2017, 3, 15, 8, 30, 0),
                    DisplayAddress = "1800 21st Street",
                    ViolationScript = "This is a test script",
                    Resp1FirstName = "John",
                    Resp1MiddleInitial = "E",
                    Resp1LastName = "Doe"
                },
                AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
            };
            ViolationSummaryViewModel vm = new ViolationSummaryViewModel() { NovMaster = novMaster };
            await vm.LoadAsync();
            Assert.AreEqual("12345", vm.NovNumber);
            Assert.AreEqual("03/15/2017 08:30 AM", vm.DateOfOffense);
            Assert.AreEqual("John E Doe", vm.Name);
            Assert.AreEqual("1800 21st Street", vm.PlaceOfOccurrence);
            Assert.AreEqual("This is a test script", vm.ViolationScript);
            Assert.IsNull(vm.ServiceAttempts);

            novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 12345,
                    IssuedTimestamp = new DateTime(2017, 3, 15, 8, 30, 0),
                    DisplayAddress = "1800 21st Street",
                    ViolationScript = "This is a test script",
                    Resp1FirstName = "John",
                    Resp1LastName = "Doe"
                },
                AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
            };
            vm = new ViolationSummaryViewModel() { NovMaster = novMaster };
            await vm.LoadAsync();
            Assert.AreEqual("12345", vm.NovNumber);
            Assert.AreEqual("03/15/2017 08:30 AM", vm.DateOfOffense);
            Assert.AreEqual("John Doe", vm.Name);
            Assert.AreEqual("1800 21st Street", vm.PlaceOfOccurrence);
            Assert.AreEqual("This is a test script", vm.ViolationScript);
            Assert.IsNull(vm.ServiceAttempts);
        }

        [TestMethod]
        public async Task TestMultipleAttemptsLoadAsync()
        {
            NovMaster novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 12345,
                    IssuedTimestamp = new DateTime(2017, 3, 15, 8, 30, 0),
                    DisplayAddress = "1800 21st Street",
                    ViolationScript = "This is a test script",
                    BusinessName = "Mod Pizza"
                },
                AffidavitOfServiceTranList = new List<AffidavitOfServiceTran>()
            };
            novMaster.AffidavitOfServiceTranList.Add(new AffidavitOfServiceTran()
            {
                SignDate = new DateTime(2015, 2, 13, 7, 45, 0)
            });
            novMaster.AffidavitOfServiceTranList.Add(new AffidavitOfServiceTran()
            {
                SignDate = new DateTime(2016, 1, 11, 5, 00, 0)
            });
            ViolationSummaryViewModel vm = new ViolationSummaryViewModel() { NovMaster = novMaster };
            await vm.LoadAsync();
            Assert.AreEqual("12345", vm.NovNumber);
            Assert.AreEqual("03/15/2017 08:30 AM", vm.DateOfOffense);
            Assert.AreEqual("Mod Pizza", vm.Name);
            Assert.AreEqual("1800 21st Street", vm.PlaceOfOccurrence);
            Assert.AreEqual("This is a test script", vm.ViolationScript);
            Assert.AreEqual("1st - 02/13/2015 07:45 AM\n2nd - 01/11/2016 05:00 AM\n", vm.ServiceAttempts);
        }
    }
}
