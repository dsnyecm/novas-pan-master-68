﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;

namespace DSNY.Novas.ViewModels.Tests
{
    public class UnitTestBase
    {
        [TestInitialize]
        public void UnitTestBaseInit()
        {
            var unitTestDependancyProvider = new UnitTestDependancyProvider();
            unitTestDependancyProvider.Register();
            DependencyResolver.SetDependencyProvider(unitTestDependancyProvider);

        }
    }

    public class MoqNavService : INavigationService
    {
        public Task PopAsync()
        {
            return Task.CompletedTask;
        }

        public Task PopModalAsync()
        {
            return Task.CompletedTask;
        }

        public Task PushAsync(ViewModelBase viewModel)
        {
            return Task.CompletedTask;
        }

        public Task PushModalAsync(ViewModelBase viewModel)
        {
            return Task.CompletedTask;
        }

        public Task PopToRootAsync()
        {
            return Task.CompletedTask;
        }

        public Task PopToSummaryScreenAsync()
        {
            return Task.CompletedTask;
        }

        public Task PopToLoginScreenAsync()
        {
            return Task.CompletedTask;
        }

        public Task PopToCachedScreenAsync(ViewModelBase viewModel)
        {
            return Task.CompletedTask;
        }
        public Task SetLatestNovNumberAndAttemptNumber(string latestIncompleteNovNumberInSqlLite)
        {
            return Task.CompletedTask;
        }

        public IEnumerable<string> CompleteViewStack(List<string> completeViewStack = null)
        {
            return new List<string>();
        }

        public Task PopToBeforeTimeoutScreenAsync()
        {
            return Task.CompletedTask;
        }

        Task INavigationService.PopAsync()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopModalAsync()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PushAsync(ViewModelBase viewModel)
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PushModalAsync(ViewModelBase viewModel)
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopToRootAsync()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopToSummaryScreenAsync()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopFromSyncToLoginIfTimeoutScreenPresentOtherwiseGoBack()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopToLoginScreenAsync()
        {
            throw new NotImplementedException();
        }

        Task INavigationService.PopToCachedScreenAsync(ViewModelBase baseModel)
        {
            throw new NotImplementedException();
        }

        IEnumerable<string> INavigationService.CompleteViewStack(List<string> completeViewStack)
        {
            throw new NotImplementedException();
        }
    }

    public class MoqAlertService : IAlertService
    {
        public Task<bool> DisplayAlert(AlertViewModel alert)
        {
            return Task.FromResult(true);
        }
    }

    public class MoqNfcReader : INFCReader
    {
        public Task<bool> IsNFCAvailable()
        {
            return Task.FromResult(true);
        }

        public Task<bool> IsNFCEnabled()
        {
            return Task.FromResult(true);
        }

        public Task<bool> NavigateToNFCSettings()
        {
            return Task.FromResult(true);
        }

        public Task<bool> StartListening()
        {
            return Task.FromResult(true);
        }

        public void StopListening()
        {
            
        }

        public ICommand CardRecognizedCommand { get; set; }
        public ICommand CardRemovedCommand { get; set; }
        public string CurrentCardUID { get; }
    }

    public class MoqBarcodeScanner : IBarcodeScanner
    {
        public MoqBarcodeScanner()
        {
        }

        public Task InitiateScan()
        {
            return Task.CompletedTask;
        }

        public Task<bool> IsScannerAvailable()
        {
            return Task.FromResult(true);
        }

        public void SetDataRecievedCommand(ICommand dataRecievedCommand)
        {
            DataRecievedCommand = dataRecievedCommand;
        }

        public ICommand DataRecievedCommand { get; set; }
    }

    public class MoqBluetoothReader : IBluetoothReader
    {
        public void FindDevices(string scanResult)
        {
            return;
        }

        public void FindPairedDevices()
        {
            return;
        }

        public async Task PairAsync()
        {
            return;
        }

        public async Task FromIdAsync()
        {
            return;
        }

        public void RemoveBluetoothWatchers()
        {
            return;
        }

        public void SetDeviceInfoListUpdate(ICommand deviceInfoListUpdate)
        {
            DeviceInfoListUpdate = deviceInfoListUpdate;
        }

        public void SetPairSuccessfulCommand(ICommand pairSuccessfulCommand)
        {
            PairSuccessfulCommand = pairSuccessfulCommand;
        }

        public Task<DeviceInformation> PairAsync(string name)
        {
            throw new NotImplementedException();
        }

        public Task<BluetoothDevice> FromIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task Pair(string name)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsPaired(string name)
        {
            throw new NotImplementedException();
        }

        public Task<List<Printer>> ListAllPrinters()
        {
            throw new NotImplementedException();
        }

        public ICommand DeviceInfoListUpdate { get; set; }
        public ICommand PairSuccessfulCommand { get; set; }
        public IEnumerable<Printer> Printers { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }

    public class MoqPrinter : IPrinter
    {
        public string GetMacAddress()
        {
            return "";
        }

        public Task<List<string>> GetPairedDevices()
        {
            return Task.FromResult(new List<string>());
        }

        public string GetSerialNumber()
        {
            return "";
        }

        public Task<bool> InitiateConnection(string macAddress, bool testPrint, Dictionary<string, string> MapFieldsToNovInfo, string summonType)
        {
            return Task.FromResult(true);
        }

        public Task<bool> InitiateConnectionForPrintReport(string macAddress, Dictionary<string, string> MapFieldsToNovInfo, string summonType)
        {
            return Task.FromResult(true);
        }

        public void SetConnectionClosedSuccessCommand(ICommand connectionClosedSuccessCommand)
        {
            ConnectionClosedSuccessCommand = connectionClosedSuccessCommand;
        }

        public void SetConnectionOpenSuccessCommand(ICommand connectionOpenSuccessCommand)
        {
            ConnectionOpenSuccessCommand = connectionOpenSuccessCommand;
        }

        public void SetSendCommandSuccess(ICommand sendCommandSuccess)
        {
            SendCommandSuccess = sendCommandSuccess;
        }

        public Task<bool> StartPrintJob()
        {
            return Task.FromResult(true);
        }

        public ICommand ConnectionOpenSuccessCommand { get; set; }
        public ICommand ConnectionClosedSuccessCommand { get; set; }
        public ICommand SendCommandSuccess { get; set; }
    }

    public class MoqAppRuntimeSettings : AppRuntimeSettingsBase
    {
        public override string GetSqlitePath()
        {
            return string.Empty;
        }
        public override string HardwareIdentifier => "TestDevice";

        public override string Version => "1.0.0.1";

        public override string HHTNumber => "PAN999";

        public override string DeviceIMEI => "777222888111999";

        public override string DeviceSerialNumber => "PAN12ABC12345";

        public override string PlatformDeviceName => "PAN12ABC12345";

        public override string DeviceMemory => "0";
    }
}
