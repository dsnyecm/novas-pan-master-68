﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class GroupTypeViewModelTests : UnitTestBase
    {
        [TestInitialize]
        public void GroupTypeViewModelTestsInit()
        {
            
        }

        [TestMethod]
        public async Task ValidationFailTest()
        {
            GroupTypeViewModel vm = new GroupTypeViewModel();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12079");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a Violation Group.");
        }

        [TestMethod]
        public async Task ValidationPassTest()
        {
            GroupTypeViewModel vm = new GroupTypeViewModel
            {
                ViolationGroupsSelectedItem = new ViolationGroups()
                {
                    ViolationGroupId = 2,
                    GroupName = "B",
                    TypeName = "Property",
                    Sequence = 1
                }
            };

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12079");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.ViolationGroupsSelectedItem.GroupName == "B");
            Assert.IsTrue(vm.ViolationGroupsSelectedItem.ViolationGroupId == 2);
            Assert.IsTrue(vm.ViolationGroupsSelectedItem.TypeName == "Property");
            Assert.IsTrue(vm.ViolationGroupsSelectedItem.Sequence == 1);
        }

        [TestMethod]
        public async Task CascadePassPropertyTest()
        {
            GroupTypeViewModel vm = new GroupTypeViewModel();
            var vg = new ViolationGroups()
            {
                ViolationGroupId = 2,
                GroupName = "B",
                TypeName = "Property",
                Sequence = 1
            };
            vm.ViolationGroupsSelectedItem = vg;

            var nov = new NovMaster
            {
                NovInformation = new NovInformation()
                {
                    DeviceId = "TestDevice",
                    ViolationGroupId = 2,
                    ViolGroupName = "B",
                    ViolationCode = "B01",
                    HHTIdentifier = "Improper Lead Acid Battery Disposal"
                }
            };

            vm.NovMaster = nov;
            await vm.LoadAsync();
            Assert.IsTrue(vm.ViolationTypesSelectedItem.Name == "Property Multiple");
            Assert.IsTrue(vm.ViolationDetailsSelectedItem.ViolationCodeShortDescription == "B01 - Improper Lead Acid Battery Disposal");
        }

        [TestMethod]
        public async Task CascadePassActionTest()
        {
            GroupTypeViewModel vm = new GroupTypeViewModel();
            var vg = new ViolationGroups()
            {
                ViolationGroupId = 1,
                GroupName = "D",
                TypeName = "Action",
                Sequence = 3
            };
            vm.ViolationGroupsSelectedItem = vg;

            var nov = new NovMaster
            {
                NovInformation = new NovInformation()
                {
                    DeviceId = "TestDevice",
                    ViolationGroupId = 1,
                    ViolGroupName = "D",
                    ViolationCode = "D09",
                    HHTIdentifier = "Construc Mat/ Equip with No permit"
                }
            };

            vm.NovMaster = nov;
            await vm.LoadAsync();
            Assert.IsTrue(vm.ViolationTypesSelectedItem.Name == "Action");
            Assert.IsTrue(vm.ViolationDetailsSelectedItem.ViolationCodeShortDescription == "D09 - Construc Mat/ Equip with No permit");
        }

        [TestMethod]
        public async Task SearchButtonTest()
        {
            GroupTypeViewModel vm = new GroupTypeViewModel();
            var vg = new ViolationGroups()
            {
                ViolationGroupId = 1,
                GroupName = "D",
                TypeName = "Action",
                Sequence = 3
            };
            vm.ViolationGroupsSelectedItem = vg;

            var nov = new NovMaster
            {
                NovInformation = new NovInformation()
                {
                    DeviceId = "TestDevice"
                }
            };

            vm.NovMaster = nov;
            await vm.LoadAsync();
            vm.ViolationSearchPressedCommand.Execute(null); //just checking to see if this command will return an exception
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12079");
            Assert.IsNull(alert);
        }
    }
}