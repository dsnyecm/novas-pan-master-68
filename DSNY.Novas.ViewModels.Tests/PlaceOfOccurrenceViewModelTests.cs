﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PlaceOfOccurrenceViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationIsBBLNumberVisibleTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationGroupId = 5, ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsBBLVisible);
        }

        [TestMethod]
        public async Task ValidationIsMDRNumberVisibleSideTest()
        {
            var descriptor = new FirstDescriptors() { Code = "S_MDR", Description = "At Side Of" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 2329301,
                BoroCode = "5",
                StreetName = "CLEVELAND ALLEY"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsMDRVisible);
        }

        [TestMethod]
        public async Task ValidationIsMDRNumberVisibleRearTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R_MDR", Description = "At Rear Of" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 2329301,
                BoroCode = "5",
                StreetName = "CLEVELAND ALLEY"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsMDRVisible);
        }

        [TestMethod]
        public async Task ValidationIsMDRNumberVisibleFrontTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F_MDR", Description = "Front Of" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 2329301,
                BoroCode = "5",
                StreetName = "CLEVELAND ALLEY"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();


            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsMDRVisible);
        }

        [TestMethod]
        public async Task ValidationFrontOfMDRNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F_MDR", Description = "Front Of", GroupName = "R" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter an MDR Number.");
        }

        [TestMethod]
        public async Task ValidationFrontOfHouseNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F", Description = "Front Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { HouseNumberBox1 = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12019");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a House Number.");
        }

        [TestMethod]
        public async Task ValidationFrontOfFirstStreetNameFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F", Description = "Front Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { FirstStreet = null, HouseNumberBox1 = "14" };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12027");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the First Street Name.");
        }

        [TestMethod]
        public async Task ValidationOppositeOfFirstStreetNameFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "O", Description = "Opposite Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { FirstStreet = null, HouseNumberBox1 = "14" };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12027");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the First Street Name.");
        }

        [TestMethod]
        public async Task ValidationOppositeOfHouseNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "O", Description = "Opposite Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { FirstStreet = null, HouseNumberBox1 = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12019");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a House Number.");
        }

        [TestMethod]
        public async Task ValidationAtRearOfHouseNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R", Description = "At Rear Of" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A R C O", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12019");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a House Number.");
        }

        [TestMethod]
        public async Task ValidationAtRearOfFirstStreetNameFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R", Description = "At Rear Of" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A R C O", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12027");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the First Street Name.");
        }

        [TestMethod]
        public async Task ValidationAtSideOfMDRNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "S_MDR", Description = "At Side Of" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter an MDR Number.");
        }

        [TestMethod]
        public async Task ValidationAtSideOfHouseNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "S", Description = "At Side Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { HouseNumberBox1 = null };

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A R C O", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12019");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a House Number.");
        }

        [TestMethod]
        public async Task ValidationAtSideOfFirstStreetNameFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "S", Description = "At Side Of" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { HouseNumberBox1 = "13", FirstStreet = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12027");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the First Street Name.");
        }

        [TestMethod]
        public async Task ValidationAtFrontOfCMHouseNumberFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "Q", Description = "Front Of C/M" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { HouseNumberBox1 = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12019");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a House Number.");
        }

        [TestMethod]
        public async Task ValidationAtFrontOfCMOnStreetNameFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "Q", Description = "Front Of C/M" };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { HouseNumberBox1 = "13", FirstStreet = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12021");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the ON Street Name.");
        }

        [TestMethod]
        public async Task ValidationAtCornerOfDistrictFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "C", Description = "At Corner Of" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12017");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a District.");
        }

        [TestMethod]
        public async Task ValidationAtCornerOfSectionFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "C", Description = "At Corner Of" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12018");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a Section.");
        }
        [TestMethod]
        public async Task ValidationOnBetweenDistrictFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "B", Description = "On/Between" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12017");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a District.");
        }

        [TestMethod]
        public async Task ValidationOnBetweenSectionFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "B", Description = "On/Between" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12018");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a Section.");
        }

        [TestMethod]
        public async Task ValidationOnBetweenFirstStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "B", Description = "On/Between" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12021");
            Assert.IsNotNull(alert);
        }
        [TestMethod]
        public async Task ValidationOnBetweenCrossStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "B", Description = "On/Between" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12026");
            Assert.IsNotNull(alert);
        }
        [TestMethod]
        public async Task ValidationOnBetweenCrossStreet2FailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "B", Description = "On/Between" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12023");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationNearDistrictFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "N", Description = "Near" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12017");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a District.");
        }

        [TestMethod]
        public async Task ValidationNearLocationFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "N", Description = "Near" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12032");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the Description of Location.");
        }

        [TestMethod]
        public async Task ValidationNearSectionFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "N", Description = "Near" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12018");
            Assert.IsNotNull(alert);
        }
        [TestMethod]
        public async Task ValidationAtDistrictFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "A", Description = "At" };
            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel { DistrictSelectedItem = null };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12017");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a District.");
        }

        [TestMethod]
        public async Task ValidationAtLocationFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "A", Description = "At" };
            var district = new District() { BoroCode = "1", Description = "MN01", DistrictId = "101" };
            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel
                {
                    DistrictSelectedItem = district,
                    LocationDescription = null
                };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12032");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the Description of Location.");
        }

        [TestMethod]
        public async Task ValidationOnBetCMDistrictFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "P", Description = "On/Bet C/M" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12017");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a District.");
        }


        [TestMethod]
        public async Task ValidationOnBetCMSectionFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "P", Description = "On/Bet C/M" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12018");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select a Section.");
        }

        [TestMethod]
        public async Task ValidationOnBetCMOnStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "P", Description = "On/Bet C/M" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12021");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the ON Street Name.");
        }

        [TestMethod]
        public async Task ValidationOnBetCMOnCenterMedianStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "P", Description = "On/Bet C/M" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12026A");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the CENTER MEDIAN BETWEEN Street Name.");
        }

        [TestMethod]
        public async Task ValidationOnBetCMAndStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "P", Description = "On/Bet C/M" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12023");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter the AND Street Name.");
        }
        [TestMethod]
        public async Task ValidationPassTest()
        {
            var descriptor = new FirstDescriptors() { Code = "O", Description = "Opposite Of" };
            var district = new District() { BoroCode = "1", Description = "MN01", DistrictId = "101" };
            var boro = new BoroMaster() { BoroId = "1", Name = "Manhattan", ShortName = "MN" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 2329301,
                BoroCode = "5",
                StreetName = "CLEVELAND ALLEY"
            };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            vm.FirstStreet = streetCodeMaster;
            vm.HouseNumberBox1 = "13";
            vm.DistrictSelectedItem = district;
            vm.BoroSelectedItem = boro;

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12079");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.HouseNumberBox1 == "13");
            Assert.IsTrue(vm.BoroSelectedItem.BoroId == "1");
        }

        [TestMethod]
        public async Task ValidationAtCornerOfCrossStreet1FailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "C", Description = "At Corner of" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12020");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationAtCornerOfCrossStreet2FailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "C", Description = "At Corner of" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12023");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationNearFirstStreetFailTest()
        {
            var descriptor = new FirstDescriptors() { Code = "N", Description = "Near" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel();
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "A", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12031");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationFrontOfMDRNumberPropertyNotFoundTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F_MDR", Description = "Front Of" };
            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 2329301,
                BoroCode = "5",
                StreetName = "CLEVELAND ALLEY"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel();

            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();


            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsMDRVisible);
        }

        [TestMethod]
        public async Task ValidationAtRearOfMDRNumberPropertyNotFoundTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R_MDR", Description = "At Rear Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2222222", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12040");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationAtSideOfMDRNumberPropertyNotFoundTest()
        {
            var descriptor = new FirstDescriptors() { Code = "S_MDR", Description = "At Side Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12037");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationMDRNumberNotMatchTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R_MDR", Description = "At Rear Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", MDRNumber = 111111, PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12039");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationNotPOPTest()
        {
            var descriptor = new FirstDescriptors() { Code = "R_MDR", Description = "At Rear Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm =
                new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "162", FirstStreet = streetCodeMaster };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "M", MDRNumber = 123456, PlaceAddressDescriptor = descriptor.Code, PlaceStreetId = streetCodeMaster.StreetCode },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12038");
            Assert.IsNull(alert);
        }

        [TestMethod]
        public async Task ValidationBBLWhiteSpaceTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12033");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationBBLLengthIncorrectTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "12345678" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12034");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationBBLNotStartWithValidNumberTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "6789012349" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12035");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationBBLNumberNotInSelectedBoroTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "2789012349" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };
            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12036");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationPropertyNotFoundTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "1789012349" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12042");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationPropertyNotVacantLotTest()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "1000030002" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12041");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationPropertyIsAPOP()
        {
            var descriptor = new FirstDescriptors() { Code = "V", Description = "Vacant Lot" };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { ViolationTypeId = "O", PlaceAddressDescriptor = descriptor.Code, PlaceBBL = "1000060001" },
                UserSession = new UserSession() { DefaultBoroCode = "1" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12038");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationDistrictAndSectionOverrideRoutingTimeTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F", Description = "Front Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2", IsAddressOverridden = true };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    ViolationTypeId = "C",
                    PlaceAddressDescriptor = descriptor.Code,
                    PlaceBBL = "1000890012",
                    PlaceStreetId = streetCodeMaster.StreetCode,
                    IssuedTimestamp = new System.DateTime(2007, 1, 1)
                },
                UserSession = new UserSession() { DefaultBoroCode = "1" },
                ViolationDetails = new ViolationDetails { ViolationCode = "S06", RoutingFlag = "Y" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12043");
            Assert.IsNotNull(alert);
        }

        [TestMethod]
        public async Task ValidationCorrectHouseNumberRoutingTimeTest()
        {
            var descriptor = new FirstDescriptors() { Code = "F", Description = "Front Of" };

            var streetCodeMaster = new StreetCodeMaster()
            {
                StreetCode = 1175001,
                BoroCode = "1",
                StreetName = "ANN STREET"
            };

            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2" };
            vm.NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    ViolationTypeId = "C",
                    PlaceAddressDescriptor = descriptor.Code,
                    PlaceBBL = "1000890012",
                    PlaceStreetId = streetCodeMaster.StreetCode,
                    IssuedTimestamp = new System.DateTime(2007, 1, 1)
                },
                UserSession = new UserSession() { DefaultBoroCode = "1" },
                ViolationDetails = new ViolationDetails { ViolationCode = "S06", RoutingFlag = "Y" }
            };

            await vm.LoadAsync();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12044");
            Assert.IsNotNull(alert);
        }

        [TestMethod]

        public void CheckSumTest()
        {
            var NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 41280001,
                    TicketStatus = "C"
                },
                NovData = new List<NovData>(),
                UserSession = new UserSession()
                {
                    DutyHeader = new DutyHeader()
                    {
                        TicketCount = 0
                    }
                }
            };
            PlaceOfOccurrenceViewModel vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2" };

            vm.CalculateCheckSum();
            Assert.IsTrue(vm.NovMaster.NovInformation.CheckSum == "X");

            NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 41280003
                },
                NovData = new List<NovData>(),
                UserSession = new UserSession()
                {
                    DutyHeader = new DutyHeader()
                    {
                        TicketCount = 0
                    }
                }
            };
            vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2" };

            vm.CalculateCheckSum();
            Assert.IsTrue(vm.NovMaster.NovInformation.CheckSum == "J");

            NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 41280005
                },
                NovData = new List<NovData>(),
                UserSession = new UserSession()
                {
                    DutyHeader = new DutyHeader()
                    {
                        TicketCount = 0
                    }
                }
            };
            vm = new PlaceOfOccurrenceViewModel() { FieldsEnabled = true, HouseNumberBox1 = "2" };

            vm.CalculateCheckSum();
            Assert.IsTrue(vm.NovMaster.NovInformation.CheckSum == "N");
        }
    }
}
