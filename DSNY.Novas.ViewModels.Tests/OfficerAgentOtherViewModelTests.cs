﻿using System;
using System.Collections.Generic;
using System.Text;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class OfficerAgentOtherViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void RadioButtonSelectionTest()
        {
            OfficerAgentOtherViewModel vm = new OfficerAgentOtherViewModel()
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation() }
            };
            vm.IsOfficerChecked = true;
            Assert.IsFalse(vm.IsDirectorChecked || vm.IsManagingAgentChecked || vm.IsOtherChecked);
            vm.IsManagingAgentChecked = true;
            Assert.IsFalse(vm.IsDirectorChecked || vm.IsOfficerChecked || vm.IsOtherChecked);
            vm.IsDirectorChecked = true;
            Assert.IsFalse(vm.IsOfficerChecked || vm.IsManagingAgentChecked || vm.IsOtherChecked);
            vm.IsOtherChecked = true;
            Assert.IsFalse(vm.IsDirectorChecked || vm.IsManagingAgentChecked || vm.IsOfficerChecked);
            Assert.IsNull(vm.OtherServedText);
        }

        [TestMethod]
        public void SaveToNovMasterTest()
        {
            OfficerAgentOtherViewModel vm = new OfficerAgentOtherViewModel()
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation(), AffidavitOfService = new AffidavitOfService() }
            };
            vm.IsOfficerChecked = true;
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("0", vm.NovMaster.AffidavitOfService.ServedTitle);

            vm = new OfficerAgentOtherViewModel()
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation(), AffidavitOfService = new AffidavitOfService() }
            };
            vm.IsDirectorChecked = true;
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("1", vm.NovMaster.AffidavitOfService.ServedTitle);

            vm = new OfficerAgentOtherViewModel()
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation(), AffidavitOfService = new AffidavitOfService() }
            };
            vm.IsManagingAgentChecked = true;
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("2", vm.NovMaster.AffidavitOfService.ServedTitle);

            vm = new OfficerAgentOtherViewModel()
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation(), AffidavitOfService = new AffidavitOfService() }
            };
            vm.IsOtherChecked = true;
            vm.OtherServedText = "Seargent";
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("3", vm.NovMaster.AffidavitOfService.ServedTitle);
            Assert.AreEqual("Seargent", vm.NovMaster.AffidavitOfService.ServedTitleOther);


        }
    }
}
