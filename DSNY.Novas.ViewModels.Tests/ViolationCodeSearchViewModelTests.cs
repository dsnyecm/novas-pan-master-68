﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolationCodeSearchViewModelTests : UnitTestBase
    {
        [TestInitialize]
        public void ViolationCodeSearchViewModelTestsInit()
        {

        }

        [TestMethod]
        public async Task IDAndNameNullTest()
        {
            ViolationCodeSearchViewModel vm = new ViolationCodeSearchViewModel();
            vm.SearchWord = "s";
            vm.ViolationDetailsSearchCommand.Execute(null);
            await Task.Delay(200); //needed this because the rest of the code is being executed before the Thread awaiting the query comes back 
            vm.SelectedViolationDetail = vm.SearchResults[0];
            vm.Nov = new NovMaster
            {
                NovInformation = new NovInformation()
                {
                    DeviceId = "TestDevice"
                },
                UserSession = new UserSession()
            };
            vm.NextCommand.Execute(null);
            Assert.IsTrue(vm.Nov.NovInformation.ViolationGroupId.Equals(3));
            Assert.IsTrue(vm.Nov.NovInformation.ViolGroupName.Equals("B"));
            Assert.IsTrue(vm.Nov.NovInformation.ViolationCode.Equals("B01"));
            Assert.IsTrue(vm.Nov.NovInformation.HHTIdentifier.Equals("Improper Lead Acid Battery Disposal"));
        }

        [TestMethod]
        public async Task IDAndNameNotNullTest()
        {
            ViolationCodeSearchViewModel vm = new ViolationCodeSearchViewModel();
            vm.SearchWord = "swe";
            vm.Nov = new NovMaster();
            vm.Nov.NovInformation = new NovInformation()
            {
                DeviceId = "TestDevice",
                ViolationGroupId = 1,
                ViolGroupName = "S",
                ViolationCode = "S03",
                HHTIdentifier = "Littering - From Vehicle"
            };
            vm.ViolationDetailsSearchCommand.Execute(null);
            await Task.Delay(200); //needed this because the rest of the code is being executed before the Thread awaiting the query comes back 
            vm.SelectedViolationDetail = vm.SearchResults[0];
            vm.NextCommand.Execute(null);
            Assert.IsTrue(vm.Nov.NovInformation.ViolationGroupId.Equals(1));
            Assert.IsTrue(vm.Nov.NovInformation.ViolGroupName.Equals("S"));
            Assert.IsTrue(vm.Nov.NovInformation.ViolationCode.Equals("S04"));
            Assert.IsTrue(vm.Nov.NovInformation.HHTIdentifier.Equals("Sweep Out Lawn Res"));
        }
    }
}
