﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PersonBeingServed2ViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationNamedCheckedFirstNameFailTest()
        {
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel { IsNameChecked = true };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12047");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a First Name.");
        }

        [TestMethod]
        public async Task ValidationNamedCheckedLastNameFailTest()
        {
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel { IsNameChecked = true, FirstName = "test" };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12046");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a Last Name.");
        }

        [TestMethod]
        public async Task ValidationNamedCheckedSexFailTest()
        {
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel { IsNameChecked = true, FirstName = "test", LastName = "test" };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12048");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Choose a gender from the Drop down list.");
        }

        [TestMethod]
        public async Task ValidationNamedNotCheckedBusinessNameFailTest()
        {
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel { IsNameChecked = false };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12011");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a Business Name.");
        }

        [TestMethod]
        public async Task ValidationStreetNameFailTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = true,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetNumber = "5",
                    City = "Test",
                    ZipCode = "12345"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12049");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a Street Address.");
        }

        [TestMethod]
        public async Task ValidationStreetNumberFailTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = true,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetName = "Test Drive",
                    City = "Test",
                    ZipCode = "12345"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12049");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a Street Address.");
        }

        [TestMethod]
        public async Task ValidationCityFailTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = true,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetName = "Test Drive",
                    StreetNumber = "5",
                    ZipCode = "12345"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12050");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a City.");
        }

        [TestMethod]
        public async Task ValidationZipFailTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = true,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetName = "Test Drive",
                    StreetNumber = "5",
                    City = "Test"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12052");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please Enter a 5 Digit Zip Code.");
        }

        [TestMethod]
        public async Task ValidationIsNameCheckedVisibilityTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = true,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetName = "Test Drive",
                    StreetNumber = "5",
                    City = "Test",
                    ZipCode = "12345"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12052");
            Assert.IsNull(alert);
            Assert.IsTrue(vm.AreNameFieldsVisible);
            Assert.IsFalse(vm.IsBusinessNameVisible);
        }

        [TestMethod]
        public async Task ValidationIsNameNotCheckedVisibilityTest()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    IsNameChecked = false,
                    FirstName = "test",
                    LastName = "test",
                    SexSelectedItem = sex,
                    StreetName = "Test Drive",
                    StreetNumber = "5",
                    City = "Test",
                    ZipCode = "12345"
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12052");
            Assert.IsNull(alert);
            Assert.IsFalse(vm.AreNameFieldsVisible);
            Assert.IsTrue(vm.IsBusinessNameVisible);
        }

        [TestMethod]
        public async Task WriteFieldValuesToNovMasterTestAsync()
        {
            var sex = new LookupTable() { TableName = "SEX", Code = "F", Description = "Female" };
            PersonBeingServed2ViewModel vm =
                new PersonBeingServed2ViewModel
                {
                    NovMaster = new NovMaster()
                    {
                        AffidavitOfService = new AffidavitOfService(),
                        NovInformation = new NovInformation()
                    }
                };
            await vm.LoadAsync();
            vm.IsNameChecked = false;
            vm.FirstName = "Jack";
            vm.MiddleInitial = "L";
            vm.LastName = "Bauer";
            vm.SexSelectedItem = sex;
            vm.StreetName = "Vine Drive";
            vm.StreetNumber = "5";
            vm.City = "Bethlehem";
            vm.ZipCode = "12345";
            vm.StateSelectedItem = vm.States.FirstOrDefault(state=> state.Code.Equals("NY"));
            vm.BoroSelectedItem = vm.Boros.FirstOrDefault(boro=> boro.BoroId == "1");
            vm.WriteFieldValuesToNovMaster();
            AffidavitOfService aff = vm.NovMaster.AffidavitOfService;
            Assert.AreEqual(aff.ServedFName, "Jack");
            Assert.AreEqual(aff.ServedMInit, "L");
            Assert.AreEqual(aff.ServedLName, "Bauer");
            Assert.AreEqual(aff.ServedSex, "F");
            Assert.AreEqual(aff.ServedHouseNo, "5");
            Assert.AreEqual(aff.ServedAddress, "Vine Drive");
            Assert.AreEqual(aff.ServedBoroCode, "1");
            Assert.AreEqual(aff.ServedCity, "Bethlehem");
            Assert.AreEqual(aff.ServedState, "NY");
            Assert.AreEqual(aff.ServedZip, "12345");
            Assert.IsTrue(aff.ShouldLoadFromAffidavit);
        }
    }
}
