﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PremiseDetailsViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void RadioButtonSelectionTest() {
            PremiseDetailsViewModel vm = new PremiseDetailsViewModel()
            {
                NovMaster = new NovMaster() {AffidavitOfService = new AffidavitOfService()}
            };
            vm.IsAtChecked = true;
            Assert.IsFalse(vm.IsEnteredChecked | vm.IsNotAvailableChecked | vm.IsLockedChecked);
            vm.IsEnteredChecked = true;
            Assert.IsFalse(vm.IsLockedChecked | vm.IsNotAvailableChecked);
            vm.IsLockedChecked = true;
            Assert.IsFalse(vm.IsEnteredChecked | vm.IsNotAvailableChecked);
            vm.IsNotAvailableChecked = true;
            Assert.IsFalse(vm.IsEnteredChecked | vm.IsLockedChecked);
        }

        [TestMethod]
        public async Task ValidateScreenTestAsync()
        {
            PremiseDetailsViewModel vm = new PremiseDetailsViewModel()
            {
                NovMaster = new NovMaster() { AffidavitOfService = new AffidavitOfService() }
            };
            vm.ServiceLocation = "";
            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault(a => a.Title == "12053");
            Assert.IsNotNull(alert);

            vm = new PremiseDetailsViewModel()
            {
                NovMaster = new NovMaster() { AffidavitOfService = new AffidavitOfService() }
            };
            vm.ServiceLocation = "T/P/O";
            alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTest()
        {
            PremiseDetailsViewModel vm = new PremiseDetailsViewModel()
            {
                NovMaster = new NovMaster() { AffidavitOfService = new AffidavitOfService() }
            };
            vm.ServiceLocation = "T/P/O";
            vm.IsEnteredChecked = true;
            vm.WriteFieldValuesToNovMaster();
            Assert.AreEqual("3", vm.NovMaster.AffidavitOfService.AlternativeService1);

        }
    }
}
