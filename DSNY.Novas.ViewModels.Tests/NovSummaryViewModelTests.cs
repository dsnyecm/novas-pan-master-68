﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class NovSummaryViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task CreateTicketTest()
        {
            IRepository<DBDeviceTicketRanges> repo =
                DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticket = await CreateTicket(100, 200, 0);
            Assert.IsNotNull(ticket);
            var vm = new NovSummaryViewModel();
            vm.UserSession = new UserSession() { DutyHeader = new DutyHeader() { DeviceId = "TestDevice" } };
            await vm.ValidateScreen();
            var createdTicket = await repo.GetAsync(_ => _.DeviceId == ticket.DeviceId);
            Assert.IsTrue(createdTicket.LastTicketNumber == 0);
        }

        [TestMethod]
        public async Task NextTicketTest()
        {
            IRepository<DBDeviceTicketRanges> repo =
                DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            await CreateTicket(100, 200, 100);
            var vm = new NovSummaryViewModel();
            vm.UserSession = new UserSession() { DutyHeader = new DutyHeader() { DeviceId = "TestDevice" } };
            await vm.ValidateScreen();
            var nextTicket = await repo.GetAsync(_ => _.DeviceId == "TestDevice");
            Assert.IsTrue(nextTicket.LastTicketNumber == 100);
        }

        [TestMethod]
        public async Task NoMoreTicketNumbersTest()
        {
            //load vehicle data into the DB so we can check the NOV numbers
            VehicleDataViewModel vvm = new VehicleDataViewModel(true)
            {
                VehicleNumber = "123",
                RadioNumber = "312",
                StartingMileage = 101,
                NovMaster = new NovMaster
                {
                    NovInformation = new NovInformation() { UserId = "jes418" },
                    NovData = new List<NovData>()
                },
                UserSession = new UserSession() { UserId = "jes418", DutyHeader = new DutyHeader() { DeviceId = "TestDevice" } }
            };
            vvm.NextCommand.Execute(null);


            IRepository<DBDeviceTicketRanges> repo =
                DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            await CreateTicket(100, 200, 200);
            var ticket = await repo.GetAsync(_ => _.DeviceId == "TestDevice");
            Assert.IsNotNull(ticket, "Error creating ticket.");
            var vm = new NovSummaryViewModel { NovMaster = new NovMaster { NovInformation = new NovInformation { UserId = "Test" } } };
            vm.UserSession = new UserSession() { UserId = "Test", DutyHeader = new DutyHeader() { DeviceId = "TestDevice" } };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Any(_ => _.Message == "No more NOV numbers."));
        }

        private async Task<DBDeviceTicketRanges> CreateTicket(long start, long end, long last)
        {
            IRepository<DBDeviceTicketRanges> repo =
                DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticketToDelete = await repo.GetAsync(_ => _.DeviceId.Equals("TestDevice"));
            if (ticketToDelete != null) await repo.DeleteAsync(ticketToDelete);

            var dbTicketRange = new DBDeviceTicketRanges
            {
                DeviceId = "TestDevice",
                NovRangeId = 1,
                RangeStatus = "A",
                TicketStartNumber = start,
                TicketEndNumber = end,
                LastTicketNumber = last
            };

            await repo.InsertAsync(dbTicketRange);
            var ticket = await repo.GetAsync(_ => _.DeviceId == dbTicketRange.DeviceId);
            return ticket;
        }
    }
}