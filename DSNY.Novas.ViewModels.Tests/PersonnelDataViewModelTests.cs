﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Models;
using DSNY.Novas.Services;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PersonnelDataViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationHearingTimesTest()
        {
            IPersonnelDataService personnelDataService = DependencyResolver.Get<IPersonnelDataService>();

            PersonnelDataViewModel vm = new PersonnelDataViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation(){ UserId = "PO540128" },
                    NovData = new List<NovData>(),
                },

                Agency = new Agency { AgencyId = 827 },
                ReportingLevel = new ReportingLevel() { ReportingLevelId = "BEQH", Title = "Sgt." },
                HearingDate = new HearingDateTime() { HearingFromDate = new DateTime(636316128000000000) },
                HearingTime = new HearingDateTime() { HearingTime = new DateTime().AddHours(6) },
                UserSession = new UserSession { UserId = "PO540128" }
            };

            await vm.LoadAsync();

            Assert.AreEqual(vm.HearingDateTimesList.Count, 4);
        }

        [TestMethod]
        public async Task ValidationHolidayListTest()
        {
            PersonnelDataViewModel vm = new PersonnelDataViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation(){ UserId = "po538053" },
                    NovData = new List<NovData>()
                },
                Agency = new Agency { AgencyId = 827 },
                ReportingLevel = new ReportingLevel() { ReportingLevelId = "BEQH", Title = "Sgt." },
                HearingDate = new HearingDateTime() { HearingFromDate = new DateTime(636316128000000000) },
                HearingTime = new HearingDateTime() { HearingTime = new DateTime().AddHours(6) },
                UserSession = new UserSession { UserId = "po538053" }
            };

            await vm.LoadAsync();

            //Checks if there isn't a day in the holiday list that matches the hearing date that's calculated
            Assert.IsFalse(vm.HolidayList.Any(_ => _.EffectiveDate == vm.HearingDate.HearingFromDate));

            PersonnelDataViewModel vm2 = new PersonnelDataViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation() { UserId = "po538053" },
                    NovData = new List<NovData>()
                },
                Agency = new Agency { AgencyId = 827 },
                ReportingLevel = new ReportingLevel() { ReportingLevelId = "BEQH", Title = "Sgt." },
                HearingDate = new HearingDateTime() { HearingFromDate = new DateTime() },
                HearingTime = new HearingDateTime() { HearingTime = new DateTime().AddHours(6) },
                UserSession = new UserSession { UserId = "po538053" }
            };

            await vm2.LoadAsync();

            //Checks if there isn't a day in the holiday list that matches the calculated hearing date
            Assert.IsFalse(vm2.HolidayList.Any(_ => _.EffectiveDate == vm2.HearingDate.HearingFromDate
                                                    && !_.Comments.Trim().Equals("ECB Non-Working Day")));
        }

        [TestMethod]
        public async Task ValidateUserRetrievalTest()
        {
            PersonnelDataViewModel vm = new PersonnelDataViewModel(){NovMaster = new NovMaster()
            {
                NovInformation = new NovInformation() { UserId = "PO540128" },
            }};
            vm.UserSession = new UserSession() { UserId = "PO540128" };
            await vm.LoadAsync();

            Assert.AreEqual(828, vm.Agency.AgencyId);
            Assert.AreEqual("KKTF", vm.ReportingLevel.ReportingLevelId);
            Assert.AreEqual("PO", vm.ReportingLevel.Title);
        }
    }
}
