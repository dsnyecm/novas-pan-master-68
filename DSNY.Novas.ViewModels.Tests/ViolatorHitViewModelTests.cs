﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolatorHitViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void TestConstructor()
        {
            ObservableCollection<Violator> violators = new ObservableCollection<Violator>();
            violators.Add(new Violator()
            {
                BoroCode = "1",
                BusinessName = "Mod Pizza",
                MaxFine = 300, MinFine = 100,
                ViolationCode = "S80",
                NextViolationCode = "S81"
            });
            violators.Add(new Violator()
            {
                BoroCode = "2",
                BusinessName = "Pie Craft",
                MaxFine = 200,
                MinFine = 50,
                ViolationCode = "S70",
                NextViolationCode = "S71"
            });

            ViolatorHitViewModel vm = new ViolatorHitViewModel(violators);
            Assert.IsNotNull(vm.ViolatorList.FirstOrDefault(v => v.BusinessName.Equals("None of the Above")));
            string selectedName = vm.SelectedViolator.BusinessName;
            Assert.AreEqual("Mod Pizza", selectedName);
        }

        [TestMethod]
        public async Task ValidateScreenTestAsync()
        {
            ObservableCollection<Violator> violators = new ObservableCollection<Violator>();
            violators.Add(new Violator()
            {
                BoroCode = "1",
                BusinessName = "Mod Pizza",
                MaxFine = 300,
                MinFine = 100,
                ViolationCode = "S80",
                NextViolationCode = "S81"
            });
            violators.Add(new Violator()
            {
                BoroCode = "2",
                BusinessName = "Pie Craft",
                MaxFine = 200,
                MinFine = 50,
                ViolationCode = "S70",
                NextViolationCode = "S71"
            });

            ViolatorHitViewModel vm = new ViolatorHitViewModel(violators);
            vm.SelectedViolator = vm.ViolatorList.FirstOrDefault(v => v.BusinessName.Equals("None of the Above"));
            vm.BoxVal = "Pie Craft";
            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault(
                a => a.Message.Equals("The name you have entered is available in provided list"));
            Assert.IsNotNull(alert);
            vm.BoxVal = "Papa John's";
            alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTestAsync()
        {
            ObservableCollection<Violator> violators = new ObservableCollection<Violator>();
            violators.Add(new Violator()
            {
                BoroCode = "1",
                BusinessName = "Mod Pizza",
                MaxFine = 300,
                MinFine = 100,
                ViolationCode = "S80",
                NextViolationCode = "S81"
            });
            violators.Add(new Violator()
            {
                BoroCode = "2",
                BusinessName = "Pie Craft",
                MaxFine = 200,
                MinFine = 50,
                ViolationCode = "S70",
                NextViolationCode = "S71"
            });
            ViolatorHitViewModel vm = new ViolatorHitViewModel(violators)
            {
                NovMaster = new NovMaster() { NovInformation = new NovInformation() }
            };
            vm.SelectedViolator = vm.ViolatorList.FirstOrDefault(v => v.BusinessName.Equals("Pie Craft"));
            vm.WriteFieldValuesToNovMaster();
            NovInformation novInfo = vm.NovMaster.NovInformation;
            Assert.AreEqual("Y", novInfo.IsPlaceAddressHit);
            Assert.AreEqual("Y", novInfo.IsMultipleOffences);
            Assert.AreEqual(novInfo.PrintViolationCode, "S71");
            Assert.AreEqual(50, novInfo.MailableAmount);
            Assert.AreEqual(200, novInfo.MaximumAmount);
        }
    }
}
