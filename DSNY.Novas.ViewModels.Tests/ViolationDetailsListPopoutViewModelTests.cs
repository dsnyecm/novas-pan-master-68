﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolationDetailsListPopoutViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void TestConstructor()
        {
           NovMaster novMaster = new NovMaster(){NovData = new List<NovData>()};
            novMaster.NovData.Add(new NovData(){Data = "red, blue, orange", FieldName = "VEHICLE COLOR"});
           ViolationScriptVariables field = new ViolationScriptVariables()
           {
               Description = "color of the vehicle",
               FieldName = "VEHICLE COLOR",
               IsFreeForm = "N",
               IsMultiplesAllowed = "Y",
               IsOtherAllowed = "Y"              
           };
            List<ViolationScriptVariableData> vars = new List<ViolationScriptVariableData>();
            vars.Add(new ViolationScriptVariableData(){Data="red", FieldName = "VEHICLE COLOR"});
            vars.Add(new ViolationScriptVariableData() { Data = "blue", FieldName = "VEHICLE COLOR" });
            vars.Add(new ViolationScriptVariableData() { Data = "green", FieldName = "VEHICLE COLOR" });
            vars.Add(new ViolationScriptVariableData() { Data = "black", FieldName = "VEHICLE COLOR" });

            ViolationDetailsListPopoutViewModel vm = new ViolationDetailsListPopoutViewModel(novMaster, field, vars);
            Assert.AreEqual("Color of the Vehicle", vm.Title);
            Assert.IsTrue(vm.IsMultiSelect);
            Assert.IsTrue(vm.IsOtherVisible);
            var blue = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("blue"));
            var red = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("red"));
            var other = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("Other"));
            var green = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("green"));
            Assert.IsNull(green);
            Assert.IsNotNull(blue);
            Assert.IsNotNull(red);
            Assert.IsNotNull(other);
            Assert.AreEqual("orange", vm.OtherFreeForm);
        }

        [TestMethod]
        public void TestSingleSelect()
        {
            NovMaster novMaster = new NovMaster() { NovData = new List<NovData>() };
            novMaster.NovData.Add(new NovData() { Data = "red", FieldName = "VEHICLE COLOR" });
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "color of the vehicle",
                FieldName = "VEHICLE COLOR",
                IsFreeForm = "N",
                IsMultiplesAllowed = "N",
                IsOtherAllowed = "N"
            };
            List<ViolationScriptVariableData> vars = new List<ViolationScriptVariableData>();
            vars.Add(new ViolationScriptVariableData() { Data = "red", FieldName = "VEHICLE COLOR" });
            vars.Add(new ViolationScriptVariableData() { Data = "blue", FieldName = "VEHICLE COLOR" });
            vars.Add(new ViolationScriptVariableData() { Data = "green", FieldName = "VEHICLE COLOR" });
            vars.Add(new ViolationScriptVariableData() { Data = "black", FieldName = "VEHICLE COLOR" });

            ViolationDetailsListPopoutViewModel vm = new ViolationDetailsListPopoutViewModel(novMaster, field, vars);
            var red = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("red"));
            var black = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("black"));
            Assert.IsNotNull(red);
            Assert.IsNull(black);
            vm.SelectedValue = vm.Values.FirstOrDefault(v => v.Data.Data.Equals("black"));
            red = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("red"));
            black = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("black"));
            Assert.IsNull(red);
            Assert.IsNotNull(black);
        }

        [TestMethod]
        public void TestMultiSelect()
        {
            NovMaster novMaster = new NovMaster() {NovData = new List<NovData>()};
            novMaster.NovData.Add(new NovData() {Data = "red, black", FieldName = "VEHICLE COLOR"});
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "color of the vehicle",
                FieldName = "VEHICLE COLOR",
                IsFreeForm = "N",
                IsMultiplesAllowed = "Y",
                IsOtherAllowed = "N"
            };
            List<ViolationScriptVariableData> vars = new List<ViolationScriptVariableData>();
            vars.Add(new ViolationScriptVariableData() {Data = "red", FieldName = "VEHICLE COLOR"});
            vars.Add(new ViolationScriptVariableData() {Data = "blue", FieldName = "VEHICLE COLOR"});
            vars.Add(new ViolationScriptVariableData() {Data = "green", FieldName = "VEHICLE COLOR"});
            vars.Add(new ViolationScriptVariableData() {Data = "black", FieldName = "VEHICLE COLOR"});

            ViolationDetailsListPopoutViewModel vm = new ViolationDetailsListPopoutViewModel(novMaster, field, vars);
            var red = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("red"));
            var black = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("black"));
            var green = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("green"));
            Assert.IsNotNull(red);
            Assert.IsNotNull(black);
            Assert.IsNull(green);
            vm.Values.FirstOrDefault(v => v.Data.Data.Equals("green")).IsSelected = true;
            red = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("red"));
            black = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("black"));
            green = vm.Values.FirstOrDefault(color => color.IsSelected && color.Data.Data.Equals("green"));
            Assert.IsNotNull(red);
            Assert.IsNotNull(black);
            Assert.IsNotNull(green);
        }

    }
}
