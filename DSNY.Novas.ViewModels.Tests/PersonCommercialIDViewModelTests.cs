﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PersonCommercialIDViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidationIdTypeFailTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel {IdTypeSelectedItem = null};
            vm.IsIdChecked = true;
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12008");
            Assert.IsNotNull(alert);

            Assert.IsTrue(alert.Message == "Please select an ID Type.");
        }

        [TestMethod]
        public async Task ValidationIssuedByFailTest()
        {
            var idMatrix = new IdMatrix()
            {
                IdType = "Current Drivers License",
                AskExpDate = "N",
                AskIdNo = "Y",
                AskIssuedBy = "",
                IdDesc = "",
                IssuedBy = ""
            };
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel {IdTypeSelectedItem = idMatrix, IssuedBySelectedItem = null, IsIssuedByEnabled = true, IDNumber = "123"};
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12009");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please select the Issuing Agency.");
        }

        [TestMethod]
        public async Task ValidationExpDateFailTest()
        {
            var idMatrix = new IdMatrix()
            {
                IdType = "CDL",
                AskExpDate = "N",
                AskIdNo = "Y",
                AskIssuedBy = "",
                IdDesc = "Current Drivers License",
                IssuedBy = ""
            };
            PersonCommercialIDViewModel vm = new PersonCommercialIDViewModel
                {
                    NovMaster = new NovMaster()
                    {
                        NovInformation = new NovInformation()
                        {
                            LicenseExpDate = new DateTime()
                        }
                    },
                    IdTypeSelectedItem = idMatrix,
                    IssuedBySelectedItem = null,
                    IsExpDateEnabled = true
                };
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "1081");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Id expiration date is less than today.\nDo you wish to continue?");
        }

        [TestMethod]
        public async Task ValidationIdNumberFailTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel() { NovMaster = new NovMaster() { NovInformation = new NovInformation() } };
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault();
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12010");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter an ID Number.");
        }

        [TestMethod]
        public async Task ValidationNoValidIdTypeTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel() { NovMaster = new NovMaster() { NovInformation = new NovInformation() } };
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault(_ => _.IdType == "--");
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12062");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == 
                "Are you certain that no valid form of ID can be discovered?\nYES - Continue\nNO - Select valid ID Type");
        }

        [TestMethod]
        public async Task ValidationBusinessNameFailTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel(){NovMaster = new NovMaster(){NovInformation = new NovInformation()}};
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault();
            vm.IsBusinessNameVisible = true;
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault(_ => _.Title == "12011");
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please enter a Business Name.");
        }

        [TestMethod]
        public async Task ValidationIdCheckedTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel() { NovMaster = new NovMaster() { NovInformation = new NovInformation() } };
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault(_ => _.IdType == "SSC");
            vm.IssuedBySelectedItem = vm.IssuedByItems.FirstOrDefault();
            vm.IDNumber = "123";
            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault();
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsIdChecked);
            Assert.IsTrue(vm.IdTypeFreeform == "");
            Assert.IsFalse(vm.IsIssuedByEnabled);
            Assert.IsTrue(vm.IdTypeDropdownIsVisible);
            Assert.IsFalse(vm.IsExpDateEnabled);
        }

        [TestMethod]
        public async Task ValidationIdNotCheckedTest()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel() { NovMaster = new NovMaster() { NovInformation = new NovInformation() } };
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault();
            vm.IsIdChecked = false;
            vm.IdTypeFreeform = "Lehigh ID Card";
            vm.IssuedBySelectedItem = new IdMatrix(){IssuedBy = "LU"};
            vm.IDNumber = "1185";
            vm.ExpDate = new DateTime(2100, 3, 15);

            List<AlertViewModel> alerts = await vm.ValidateScreen();
            AlertViewModel alert = alerts.FirstOrDefault();
            Assert.IsNull(alert);
            Assert.IsTrue(vm.IsIssuedByEnabled);
            Assert.IsFalse(vm.IdTypeDropdownIsVisible);
            Assert.IsTrue(vm.IsExpDateEnabled);
        }

        [TestMethod]
        public async Task WriteFieldValuesToNovMasterTestAsync()
        {
            PersonCommercialIDViewModel vm =
                new PersonCommercialIDViewModel
                {
                    NovMaster = new NovMaster()
                    {
                        NovInformation = new NovInformation(),
                        AffidavitOfService = new AffidavitOfService()
                    },
                    BusinessName = "Evil Corp",
                    OtherFeatures = "None",
                    AgeSelectedItem = new LookupTable { Code = "1", Description = "Elder", TableName = "AgeTable" },
                    SkinSelectedItem = new LookupTable { Code = "2", Description = "White", TableName = "SkinTable" },
                    WeightSelectedItem =
                        new LookupTable { Code = "3", Description = "Thin", TableName = "WeightTable" },
                    HeightSelectedItem =
                        new LookupTable { Code = "4", Description = "Tall", TableName = "HeightTable" },
                    HairColorSelectedItem =
                        new LookupTable { Code = "5", Description = "Brown", TableName = "HairTable" }

                };
            await vm.LoadAsync();
            vm.IdTypeSelectedItem = vm.IdTypes.FirstOrDefault(_ => _.IdType == "CDL");
            vm.IDNumber = "1234567";
            vm.IssuedBySelectedItem = vm.IssuedByItems.FirstOrDefault(_ => _.IssuedBy == "NYS");
            vm.ExpDate = new DateTime(2100, 3, 15);

            vm.WriteFieldValuesToNovMaster();

            Assert.AreEqual("Evil Corp", vm.NovMaster.NovInformation.BusinessName);
            Assert.AreEqual("None", vm.NovMaster.AffidavitOfService.OtherIdentifying);
            Assert.AreEqual("1", vm.NovMaster.AffidavitOfService.Age);
            Assert.AreEqual("2", vm.NovMaster.AffidavitOfService.SkinColor);
            Assert.AreEqual("3", vm.NovMaster.AffidavitOfService.Weight);
            Assert.AreEqual("4", vm.NovMaster.AffidavitOfService.Height);
            Assert.AreEqual("5", vm.NovMaster.AffidavitOfService.HairColor);

            Assert.AreEqual(vm.NovMaster.NovInformation.LicenseType, "CDL");
            Assert.AreEqual(vm.NovMaster.NovInformation.LicenseTypeDesc, "Current Drivers License");
            Assert.AreEqual(vm.NovMaster.NovInformation.LicenseAgency, "NYS");
            Assert.AreEqual(vm.NovMaster.NovInformation.LicenseNumber, "1234567");
            Assert.AreEqual(vm.NovMaster.NovInformation.LicenseExpDate, new DateTime(2100, 3, 15));

        }
    }
}
