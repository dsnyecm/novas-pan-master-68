﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using DSNY.Novas.ViewModels.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ViolationDetailsFreeformPopoutViewModelTests : UnitTestBase
    {
        [TestMethod]
        public void TestConstruction()
        {
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "day of occurrence",
                Mask = "##/##/####"
            };
            ViolationDetailsFreeformPopoutViewModel vm = new ViolationDetailsFreeformPopoutViewModel(field);
            Assert.AreEqual(vm.InputType, InputType.Numeric);
            Assert.IsTrue(vm.ShouldDisplayInputFormat);
            Assert.AreEqual("Day of Occurrence", vm.Title);
            Assert.AreEqual("Format: ##/##/####", vm.InputDescription);

            ViolationScriptVariables field2 = new ViolationScriptVariables()
            {
                Description = "vehicle-color",
                Mask = "AAAAAAAAAA"
            };
            vm = new ViolationDetailsFreeformPopoutViewModel(field2);
            Assert.AreEqual(vm.InputType, InputType.Alphanumeric);
            Assert.IsFalse(vm.ShouldDisplayInputFormat);
            Assert.AreEqual("Vehicle-Color", vm.Title);
            Assert.AreEqual("Character Limit: 10", vm.InputDescription);

            ViolationScriptVariables field3 = new ViolationScriptVariables()
            {
                Description = "NullMaskCase",
                Mask = null
            };
            vm = new ViolationDetailsFreeformPopoutViewModel(field3);
            Assert.AreEqual(vm.InputType, InputType.Alphanumeric);
            Assert.IsFalse(vm.ShowInputDescription);
            Assert.AreEqual("", vm.Field.Mask);
        }

        [TestMethod]
        public void DateTextChangedTest()
        {
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "day of occurrence",
                Mask = "##/##/####"
            };
            ViolationDetailsFreeformPopoutViewModel vm = new ViolationDetailsFreeformPopoutViewModel(field);
            vm.EnteredValue = "";
            vm.TextChanged("1", vm.EnteredValue);
            Assert.AreEqual("1", vm.EnteredValue);
            vm.TextChanged("12", vm.EnteredValue);
            Assert.AreEqual("12/", vm.EnteredValue);
            vm.TextChanged("12/3", vm.EnteredValue);
            Assert.AreEqual("12/3", vm.EnteredValue);
            vm.TextChanged("12/30", vm.EnteredValue);
            Assert.AreEqual("12/30/", vm.EnteredValue);
            vm.TextChanged("12/30/2", vm.EnteredValue);
            vm.TextChanged("12/30/20", vm.EnteredValue);
            vm.TextChanged("12/30/201", vm.EnteredValue);
            vm.TextChanged("12/30/2017", vm.EnteredValue);
            vm.TextChanged("12/30/20178", vm.EnteredValue);
            Assert.AreEqual("12/30/2017", vm.EnteredValue);
        }

        [TestMethod]
        public void TimeTextChangedTest()
        {
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "time of occurrence",
                Mask = "##:## AA"
            };
            ViolationDetailsFreeformPopoutViewModel vm = new ViolationDetailsFreeformPopoutViewModel(field);
            vm.EnteredValue = "";
            vm.TextChanged("1", vm.EnteredValue);
            Assert.AreEqual("1", vm.EnteredValue);
            vm.TextChanged("12", vm.EnteredValue);
            Assert.AreEqual("12:", vm.EnteredValue);
            vm.TextChanged("12:3", vm.EnteredValue);
            Assert.AreEqual("12:3", vm.EnteredValue);
            vm.TextChanged("12:30", vm.EnteredValue);
            Assert.AreEqual("12:30 ", vm.EnteredValue);
            vm.TextChanged("12:30 S", vm.EnteredValue);
            Assert.AreEqual("12:30 ", vm.EnteredValue);
            vm.TextChanged("12:30 P", vm.EnteredValue);
            vm.TextChanged("12:30 PM", vm.EnteredValue);
            vm.TextChanged("12:30 PMA", vm.EnteredValue);
            Assert.AreEqual("12:30 PM", vm.EnteredValue);            
        }

        [TestMethod]
        public void YearTextChangedTest()
        {
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "year of occurrence",
                Mask = "####"
            };
            ViolationDetailsFreeformPopoutViewModel vm = new ViolationDetailsFreeformPopoutViewModel(field);
            vm.EnteredValue = "";
            vm.TextChanged("a", vm.EnteredValue);
            Assert.AreEqual("", vm.EnteredValue);
            vm.TextChanged("1", vm.EnteredValue);
            vm.TextChanged("19", vm.EnteredValue);
            vm.TextChanged("199", vm.EnteredValue);
            vm.TextChanged("1999", vm.EnteredValue);
            vm.TextChanged("19999", vm.EnteredValue);
            Assert.AreEqual("1999", vm.EnteredValue);
        }

        [TestMethod]
        public void WriteFieldValuesToNovMasterTestAsync()
        {
            ViolationScriptVariables field = new ViolationScriptVariables()
            {
                Description = "color of vehicle",
                Mask = "AAAAAA",
                FieldName = "vehicle color"
            };
            ViolationDetailsFreeformPopoutViewModel vm = new ViolationDetailsFreeformPopoutViewModel(field)
            {
                NovMaster = new NovMaster() { NovData = new List<NovData>()}
            };

            vm.EnteredValue = "red";
            vm.WriteFieldValuesToNovMaster();
            NovData entry = vm.NovMaster.NovData.FirstOrDefault(e => e.FieldName.Equals("vehicle color"));
            Assert.IsNotNull(entry);
            Assert.AreEqual("red", entry.Data);
        }
    }
}
