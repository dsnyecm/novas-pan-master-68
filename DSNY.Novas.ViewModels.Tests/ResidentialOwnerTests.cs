﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class ResidentialOwnerTests : UnitTestBase
    {
        //Database dependent test
        [TestMethod]
        public async Task ResidentialOwnerTest()
        {
            var vm = new ResidentialOwnerViewModel()
            {
                NovMaster = new NovMaster()
                {
                    NovInformation = new NovInformation()
                    {
                        PlaceHouseNo = "184",
                        PlaceStreetId = 3383001,
                        PlaceBoroCode = "3"
                    }
                }
            };

            await vm.LoadAsync();

            Assert.AreEqual("Ziff , Richard", vm.ResidentialOwner);
        }
    }
}
