﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.ViewModels.Tests
{
    [TestClass]
    public class PersonalServiceAffirmationSignatureViewModelTests : UnitTestBase
    {
        [TestMethod]
        public async Task ValidateScreenPassTest()
        {
            NovMaster novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    PlaceBoroCode = "1"
                },
                ViolationDetails = new ViolationDetails()
                {
                    ViolationCode = "123"
                }
            };

            var vm = new PersonalServiceAffirmationSignatureViewModel(){ IsNovAffirmed = true, NovMaster = novMaster};

            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
        }

        [TestMethod]
        public async Task ValidateScreenFailTest()
        {
            NovMaster novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    PlaceBoroCode = "1"
                },
                ViolationDetails = new ViolationDetails()
                {
                    ViolationCode = "123"
                }
            };

            var vm = new PersonalServiceAffirmationSignatureViewModel(){ NovMaster = novMaster, IsNovAffirmed = false };

            var alerts = await vm.ValidateScreen();
            var alert = alerts.FirstOrDefault();
            Assert.IsNotNull(alert);
            Assert.IsTrue(alert.Message == "Please sign before continuing.");
        }

        [TestMethod]
        public async Task ValidateAffidavitSaveTest()
        {
            var novMaster = new NovMaster()
            {
                NovInformation = new NovInformation()
                {
                    NovNumber = 123456789,
                    UserId = "SEA1095",
                    PublicKeyId = 3,
                    DigitalSignature = "098409ADSF7U3MCJRURIALKJDF82930MVKA",
                    ViolationCode = "S7V",
                    PlaceBoroCode = "1",
                    CheckSum = "N",
                    DeviceId = "HHT157",
                    OfficerName = "Albert Joseph",
                    AbbrevName = "520167",
                    AgencyId = "827",
                    Title = "SEA",
                    LoginTimestamp = new System.DateTime(2009, 1, 1),
                    IssuedTimestamp = new System.DateTime(2010, 1, 1),
                    LicenseExpDate = new System.DateTime(2010, 1, 1),
                },
                AffidavitOfService = new AffidavitOfService()
                {
                    Sex = "2",
                    Age = "1",
                    HairColor = "4",
                    SkinColor = "2",
                    Height = "1",
                    Weight = "3",
                    LicenseNumber = "12345",
                    ExpDate = new System.DateTime(1, 2, 3),
                    TypeOfLicense = "CDL",
                    IssuedBy = "NY",
                    Comments = "ECB",
                    SignDate = new System.DateTime(2001, 1, 1),
                    ServiceTo = "4",
                    ServedLName = "Joe",
                    ServedFName = "Allan",
                    ServedMInit = "J",
                    ServedSex = "F",
                    ServedAddress = "1 Oak Drive",
                    ServedCity = "Mauville",
                    ServedState = "Jhoto",
                    ServedZip = "12345",
                    AbbrevName = "12344",
                    AgencyId = "827",
                    Title = "SEA",
                    LoginTimestamp = new System.DateTime(2001, 2, 2),
                    ServedHouseNo = "1",
                    ServedBoroCode = "5"
                },
                ViolationDetails = new ViolationDetails()
                {
                    ViolationCode = "123"
                }
            };
            var vm = new PersonalServiceAffirmationSignatureViewModel() { NovMaster = novMaster, IsNovAffirmed = true};
            var alerts = await vm.ValidateScreen();
            Assert.IsTrue(alerts.Count == 0);
            vm.NextCommand.Execute(true);
        }
    }
}