﻿using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
namespace DSNY.Novas.Services
{
    public class LoginService : BaseService, ILoginService
    {
        private readonly IRepository<DBNovasUser> _novasUserRepository;
        private readonly IRepository<DBNovasUserMaster> _novasUserMasterRepository;

        public LoginService()
        {
            _novasUserRepository = DependencyResolver.Get<IRepository<DBNovasUser>>();
            _novasUserMasterRepository = DependencyResolver.Get<IRepository<DBNovasUserMaster>>();
        }

        public async Task<bool> AuthenticateUser(string userId, string pin, string nfcUid)
        {
            var user = await _novasUserRepository.GetAsync(_ => _.UserId == userId);
            var salt = Encoding.UTF8.GetBytes(user.Salt);
            var plainText = string.Format("{0}{1}", pin, nfcUid);
            var computedHash = SimpleHash.ComputeHash(plainText, salt);

            var userMasterRecord = await _novasUserMasterRepository.GetAsync(_ => _.UserId == userId);
            userMasterRecord.LastLoginAttempt = TimeManager.Now;
            userMasterRecord.LoginAttempts = userMasterRecord.LoginAttempts++;

            if (computedHash == user.UserHash)
            {
                userMasterRecord.LoginAttempts = 0;
                return true;
            }

            return false;
        }

        public async Task<string> GetUserIdFromCardID(string pin, string nfcUid)
        {
            var userTable = await _novasUserRepository.GetAsync();
            for (int i = 0; i < userTable.Count; i++)
            {
                var salt = Encoding.UTF8.GetBytes(userTable[i].Salt);
                var plainText = string.Format("{0}{1}", pin, nfcUid);
                var computedHash = SimpleHash.ComputeHash(plainText, salt);
                if (computedHash == userTable[i].UserHash)
                {
                    return userTable[i].UserId;
                }
            }
            return "";
        }
        public async Task<string> GetUserIDFromCardID(string nfcUid)
        {
            var novasUser = await _novasUserRepository.GetAsync(_ => _.NfcUid == nfcUid);

            if (novasUser == null)
                return "";

            var user = await _novasUserMasterRepository.GetAsync(_ => _.UserId == novasUser.UserId);

            if (user == null)
                return "";

            return user.UserId;
        }
        public async Task<string> GetUserNameFromCardID (string nfcUid)
        {
            var novasUser = await _novasUserRepository.GetAsync(_ => _.NfcUid == nfcUid);

            if (novasUser == null)
                return "";

            var user = await _novasUserMasterRepository.GetAsync(_ => _.UserId == novasUser.UserId);

            if (user == null)
                return "";

            return user.Name;
        }
        public async Task<string> GetUserAgencyFromCardID(string nfcUid)
        {
            var novasUser = await _novasUserRepository.GetAsync(_ => _.NfcUid == nfcUid);

            if (novasUser == null)
                return "";

            var user = await _novasUserMasterRepository.GetAsync(_ => _.UserId == novasUser.UserId);

            if (user == null)
                return "";

            return user.AgencyId;
        }

        public async Task<bool> IsValidUser(string userId)
        {
            var user = await _novasUserRepository.GetAsync(_ => _.UserId == userId);

            if (user != null)
                return true;

            return false;
        }
    }
}