﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class NovService : BaseService, INovService
    {
        public async Task<List<ViolationGroups>> GetViolationGroups()
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationGroups>>();
            var dbGroups = await repo.GetAsync();

            var violationGroups = dbGroups.Select(_ => new ViolationGroups
            {
                GroupName = _.GroupName,
                Sequence = _.Sequence,
                TypeName = _.TypeName,
                ViolationGroupId = _.ViolationGroupId
            }).OrderBy(_ => _.Sequence).ToList();

            return violationGroups;
        }

        public async Task<List<ViolationTypes>> GetViolationTypes(Int64 violationGroupId)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationTypes>>();

            var dbViolationTypes =
                await repo.GetAsync<List<ViolationTypes>>(
                    _ => _.ViolationGroupId == violationGroupId);

            return dbViolationTypes.Select(_ => new ViolationTypes
            {
                Description = _.Description,
                Name = _.Name,
                Sequence = _.Sequence,
                ViolationGroupId = _.ViolationGroupId,
                ViolationTypeId = _.ViolationTypeId
            }).ToList();
        }

        public async Task<List<ViolationDetails>> GetViolationDetailsByGroupAndName(Int64 violationGroupId,
            string groupName)
        {
            var detailsRepo = DependencyResolver.Get<IRepository<DBViolationDetails>>();

            List<DBViolationDetails> dbViolationDetails =
                await detailsRepo.GetAsync<List<DBViolationDetails>>(
                    _ => _.ViolationGroupId == violationGroupId &&
                         _.IsMobileRecord == "Y");
            IList<DBViolationDetails> dbViolationDetailsLimited;
            if (violationGroupId==7)
            {
                dbViolationDetailsLimited = dbViolationDetails.FindAll(_ => _.ViolationGroupId==violationGroupId);
            }
            else
            {
                dbViolationDetailsLimited = dbViolationDetails.FindAll(_ => _.ViolationCode[0].Equals(groupName[0]));
           
            }
           

            return dbViolationDetailsLimited.Select(_ => new ViolationDetails
            {
                ViolationDetailsKey = _.DBViolationDetailsKey,
                ViolationCode = _.ViolationCode.Trim(),
                InternalShortDescription = _.InternalShortDescription,
                MailableAmount = _.MailableAmount,
                MaximumAmount = _.MaximumAmount,
                RoutingFlag = _.RoutingFlag,
                HHTIdentifier = _.HHTIdentifier,
                ViolationCodeShortDescription =
                    getSortViolationDetail($"{_.ViolationCode.Trim()} - {_.HHTIdentifier}"),
                LawId = _.LawId,
                CodeLaw = _.CodeLaw
            }).ToList();
        }


        private string getSortViolationDetail(string strvio)
        {

            if (strvio.Length > 37)
            {
                strvio = strvio.Substring(0, 34) + "...";
            }
            return strvio;
        }

        public async Task<List<ViolationDetails>> GetViolationDetailsByGroupAndNameAndSearch(Int64 violationGroupId,
            string groupName, string search)
        {
            if (violationGroupId == 0 || string.IsNullOrEmpty(groupName))
            {
                var detailsRepositoryWithoutBounds = DependencyResolver.Get<IRepository<DBViolationDetails>>();
                var groupsRepositoryWithoutBounds = DependencyResolver.Get<IRepository<DBViolationGroups>>();
                var dbViolationDetailsWithoutBounds = await detailsRepositoryWithoutBounds.GetAsync();
                var dbViolationGroupsWithoutBounds = await groupsRepositoryWithoutBounds.GetAsync();

                var violationDetailsFiltered = dbViolationDetailsWithoutBounds.FindAll((violationDetails) => (violationDetails.ViolationCode.ToLower()).Contains(search.ToLower()) || (violationDetails.HHTIdentifier.ToLower()).Contains(search.ToLower()));
                return violationDetailsFiltered.Select(_ => new ViolationDetails
                {
                    ViolationGroupId = _.ViolationGroupId,
                    ViolationDetailsKey = _.DBViolationDetailsKey,
                    ViolationCode = _.ViolationCode.Trim(),
                    InternalShortDescription = _.InternalShortDescription,
                    MailableAmount = _.MailableAmount,
                    MaximumAmount = _.MaximumAmount,
                    RoutingFlag = _.RoutingFlag,
                    HHTIdentifier = _.HHTIdentifier,
                    ViolationCodeShortDescription =
                    $"{_.ViolationCode.Trim()}-{dbViolationGroupsWithoutBounds.Find(group => group.ViolationGroupId == _.ViolationGroupId).TypeName}-{_.HHTIdentifier}",
                    LawId = _.LawId,
                    CodeLaw = _.CodeLaw
                }).OrderBy((_) => _.ViolationCodeShortDescription).ToList();

            }

            var detailsRepo = DependencyResolver.Get<IRepository<DBViolationDetails>>();
            var groupsRepo = DependencyResolver.Get<IRepository<DBViolationGroups>>();

            List<DBViolationDetails> dbViolationDetails =
                await detailsRepo.GetAsync<List<DBViolationDetails>>(
                _ => _.ViolationGroupId == violationGroupId &&
                _.IsMobileRecord == "Y");

            dbViolationDetails = dbViolationDetails.FindAll(_ => _.ViolationCode[0].Equals(groupName[0]));
            var dbViolationGroups = await groupsRepo.GetAsync();

            dbViolationDetails = dbViolationDetails.FindAll((violationDetails) => (violationDetails.ViolationCode.ToLower()).Contains(search.ToLower()) || (violationDetails.HHTIdentifier.ToLower()).Contains(search.ToLower()));

            return dbViolationDetails.Select(_ => new ViolationDetails
            {
                ViolationGroupId = _.ViolationGroupId,
                ViolationDetailsKey = _.DBViolationDetailsKey,
                ViolationCode = _.ViolationCode.Trim(),
                InternalShortDescription = _.InternalShortDescription,
                MailableAmount = _.MailableAmount,
                MaximumAmount = _.MaximumAmount,
                RoutingFlag = _.RoutingFlag,
                HHTIdentifier = _.HHTIdentifier,
                ViolationCodeShortDescription =
                    $"{_.ViolationCode.Trim()}-{dbViolationGroups.Find(group => group.ViolationGroupId == _.ViolationGroupId).TypeName}-{_.HHTIdentifier}",
                LawId = _.LawId,
                CodeLaw = _.CodeLaw
            }).OrderBy((_) => _.ViolationCodeShortDescription).ToList();
        }

        public async Task<ViolationLawCodes> GetSummaryOfLaw(int lawId)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationLawCodes>>();
            var dbLawCode = await repo.GetAsync(_ => _.LawId == lawId);

            var lawCode = new ViolationLawCodes
            {
                LawDescription = dbLawCode.LawDescription,
                LawSection = dbLawCode.LawSection
            };

            return lawCode;
        }

        public async Task<bool> CheckDeviceTicketRange()
        {
            var deviceService = DependencyResolver.Get<IDeviceService>();
            var repo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticketRanges = await repo.GetAsync();
            string deviceId;
#if TEST
            deviceId = "TestDevice";
#else
            //NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //deviceId = await deviceService.GetDeviceIdForHardware(hardwareId);
            string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);
#endif

            var blnResult = await AssignNewTicketRange(deviceId);

            return blnResult;

            #region oldCode

            ////var ticketRange = ticketRanges.FirstOrDefault(_ => _.DeviceId.Trim() == deviceId?.Trim());
            //var ticketRange = ticketRanges.Where(_ => _.DeviceId.Trim() == deviceId?.Trim());

            //var currentRange = ticketRange.Where(m => m.RangeStatus == "A").FirstOrDefault();

            //if (currentRange?.LastTicketNumber == currentRange?.TicketEndNumber)
            //{
            //    //Current ticket range is over and
            //    //try to see if there are any unused ranges for the device 
            //    var nextRange = ticketRange.FirstOrDefault(_ => _.RangeStatus == "U");
            //    if (nextRange != null)
            //    {
            //        currentRange.RangeStatus = "C";
            //        nextRange.RangeStatus = "A";
            //        //nextTicketNumber = nextRange.TicketStartNumber;
            //        //nextRange.LastTicketNumber = (long)nextTicketNumber;

            //        await repo.UpdateAsync(currentRange);
            //        await repo.UpdateAsync(nextRange);

            //        return true;

            //    }

            //    return false;
            //}

            //return true;
#endregion 
        }
        private async Task<bool> AssignNewTicketRange(string deviceId)
        {
            bool isTicketAssigned = true;
            var repo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticketRanges = await repo.GetAsync();
            var ticketRange = ticketRanges.Where(_ => _.DeviceId.Trim() == deviceId?.Trim());

            var currentRange = ticketRange.Where(m => m.RangeStatus == "A").FirstOrDefault();

            if (currentRange?.LastTicketNumber == currentRange?.TicketEndNumber)
            {
                //Current ticket range is over and
                //try to see if there are any unused ranges for the device 
                isTicketAssigned = false;
                long? nextTicketNumber = null;
                var nextRange = ticketRange.FirstOrDefault(_ => _.RangeStatus == "U");
                if (nextRange != null)
                {
                    currentRange.RangeStatus = "C";
                    nextRange.RangeStatus = "A";
                    //nextTicketNumber = nextRange.TicketStartNumber;
                    //nextRange.LastTicketNumber = (long)nextTicketNumber;
                    await repo.UpdateAsync(currentRange);
                    await repo.UpdateAsync(nextRange);

                    isTicketAssigned= true;

                }

            }
            return isTicketAssigned;
        }
        public async Task<bool> HasAnotherUserIncompleteTicket(String userID) {
            if (string.IsNullOrEmpty(userID))
                return false;

            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            var dbNovInfo = await repo.GetAsync(_ => !_.UserId.Equals(userID) && (_.TicketStatus.Equals("I")));
            if (dbNovInfo != null)
            {
                return true;
            }

            return false;
        }
        public async Task<long?> IncrementTicketValue()
        {
            var deviceService = DependencyResolver.Get<IDeviceService>();
            var repo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticketRanges = await repo.GetAsync();
            string deviceId;
#if TEST
            deviceId = "TestDevice";
#else
            //NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //deviceId = await deviceService.GetDeviceIdForHardware(hardwareId);
            string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);
#endif
            var ticketRange = ticketRanges.FirstOrDefault(_ => _.DeviceId.Trim() == deviceId?.Trim() && _.RangeStatus=="A");
            if (ticketRange == null)
            {
                // TODO: Log
                return null;
            }

            long? nextTicketNumber = null;
            if (ticketRange.LastTicketNumber == 0)
            {
                nextTicketNumber = ticketRange.TicketStartNumber;
            }
            else
            {
                long lastTicketNumber = ticketRange.LastTicketNumber + 1;

                if (!(lastTicketNumber > ticketRange.TicketEndNumber))
                {
                    nextTicketNumber = lastTicketNumber;
                }
            }

            if (!nextTicketNumber.HasValue) return null;
            ticketRange.LastTicketNumber = (long)nextTicketNumber;
            await repo.UpdateAsync(ticketRange);
            return ticketRange.LastTicketNumber;
        }

        public async Task<DeviceTicketRanges> GetTicketRange()
        {
            var deviceService = DependencyResolver.Get<IDeviceService>();
            var repo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            //var dbticketRanges = await repo.GetAsync();
            string deviceId;
#if TEST
            deviceId = "TestDevice";
#else
            //NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //deviceId = await deviceService.GetDeviceIdForHardware(hardwareId);
            string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);
#endif

            //To checke if current ticket range is over then assign new range
            var blnResult = await AssignNewTicketRange(deviceId);

            var dbticketRanges = await repo.GetAsync();
            var dbDeviceTicketRanges = dbticketRanges.FindAll(_ => _.DeviceId.Trim() == deviceId?.Trim());
            if (dbDeviceTicketRanges != null)
            {
                var currentRange = dbDeviceTicketRanges.FirstOrDefault(_ => _.RangeStatus == "A");



                if (currentRange != null)
                {
                    var ticketRange = new DeviceTicketRanges();
                    ticketRange.TicketStartNumber = currentRange.TicketStartNumber;
                    ticketRange.TicketEndNumber = currentRange.TicketEndNumber;
                    ticketRange.LastTicketNumber = currentRange.LastTicketNumber;
                    return ticketRange;
                }
            }

            return null;
        }
        public async Task<long?> GetNextNovNumber(bool isPeek)
        {
            var deviceService = DependencyResolver.Get<IDeviceService>();
            var repo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
            var ticketRanges = await repo.GetAsync();
            string deviceId;
#if TEST
            deviceId = "TestDevice";
#else
            //NH-979
            //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
            //deviceId = await deviceService.GetDeviceIdForHardware(hardwareId);
            string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
            deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);
#endif
            var deviceTicketRanges = ticketRanges.FindAll(_ => _.DeviceId.Trim() == deviceId?.Trim());
            if (ticketRanges == null)
            {
                // TODO: Log
                return null;
            }

            long? nextTicketNumber = null;
            var currentRange = deviceTicketRanges.FirstOrDefault(_ => _.RangeStatus == "A");

            if (currentRange != null)
            {
                bool isLastTicketNumberInRange = (currentRange.LastTicketNumber >= currentRange.TicketStartNumber && currentRange.LastTicketNumber <= currentRange.TicketEndNumber);
                //if we're using a brand new range, then use start number 
                if (currentRange.LastTicketNumber == 0 || !isLastTicketNumberInRange)
                {
                    nextTicketNumber = currentRange.TicketStartNumber;
                    currentRange.LastTicketNumber = (long)nextTicketNumber;
                    if (!isPeek)
                        await repo.UpdateAsync(currentRange);
                }
                //if we're approaching the end of the range (less than 25 tickets remaining in the range), use the next range
                //else if ((currentRange.LastTicketNumber + 1) > (currentRange.TicketEndNumber - 25))
                //{
                //    var nextRange = deviceTicketRanges.FirstOrDefault(_ => _.RangeStatus == "U");
                //    if (nextRange != null)
                //    {
                //        currentRange.RangeStatus = "C";
                //        nextRange.RangeStatus = "A";
                //        nextTicketNumber = nextRange.TicketStartNumber;
                //        nextRange.LastTicketNumber = (long)nextTicketNumber;
                //        if (!isPeek)
                //        {
                //            await repo.UpdateAsync(currentRange);
                //            await repo.UpdateAsync(nextRange);
                //        }
                //    }
                //}
                //otherwise, simply grab the next ticket number.
                else
                {
                    nextTicketNumber = currentRange.LastTicketNumber + 1;
                    currentRange.LastTicketNumber = (long)nextTicketNumber;
                    if (!isPeek)
                        await repo.UpdateAsync(currentRange);
                }
                
            }
            else
            {
                //try to see if there are any unused ranges for the device
                var nextRange = deviceTicketRanges.FirstOrDefault(_ => _.RangeStatus == "U");
                if (nextRange != null)
                {
                    currentRange.RangeStatus = "C";
                    nextRange.RangeStatus = "A";
                    nextTicketNumber = nextRange.TicketStartNumber;
                    nextRange.LastTicketNumber = (long)nextTicketNumber;
                    if (!isPeek)
                    {
                        await repo.UpdateAsync(currentRange);
                        await repo.UpdateAsync(nextRange);
                    }
                }
            }

            return nextTicketNumber;
        }

        public async Task SaveViolation(NovMaster novMaster)
        {
            //TODO: Add transaction support
            await SaveNovInfo(novMaster);
            await SaveNovData(novMaster);
            await SaveNovCertificate(novMaster);
        }

        public async Task<List<NovData>> GetNovData(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovData>>();
            var dbNovData = await repo.GetAsync<NovData>(_ => _.NovNumber.Equals(novNumber));
            return dbNovData.Select(_ => new NovData
            {
                NovNumber = _.NovNumber,
                FieldName = _.FieldName,
                Data = _.Data
            }).ToList();
        }

        public async Task  DeleteNovData(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovData>>();
            var dbNovData = await repo.GetAsync<NovData>(_ => _.NovNumber.Equals(novNumber));

            foreach(var item in dbNovData)
            {
                await repo.DeleteAsync(item);
            }
        }

        public async Task<List<NovInformation>> GetViolations()
        {
            var tickets = await GetViolations(null);

            return tickets.ToList();
        }

        public async Task<List<NovInformation>> GetViolations(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            var qTickets = repo.AsQueryable();

            if (!String.IsNullOrEmpty(userId))
                qTickets = qTickets
                    .Where(_ => _.UserId == userId);

            var _tickets = await qTickets.ToListAsync();
            var tickets = _tickets
                .Select(_ => new NovInformation
            {
                NovNumber = _.NovNumber,
                TicketStatus = _.TicketStatus,
                IssuedTimestamp = _.IssuedTimestamp,
                SystemTimestamp = _.SystemTimestamp,
                LoginTimestamp = _.LoginTimestamp,
                ReportLevel = _.ReportLevel,
                IsResp1AddressHit = _.IsResp1AddressHit,
                Resp1LastName = _.Resp1LastName,
                Resp1FirstName = _.Resp1FirstName,
                Resp1MiddleInitial = _.Resp1MiddleInitial,
                Resp1Sex = _.Resp1Sex,
                PropertyBBL = _.PropertyBBL,
                Resp1DistrictId = _.Resp1DistrictId,
                Resp1SectionId = _.Resp1SectionId,
                Resp1StreetId = _.Resp1StreetId,
                Resp1Address = _.Resp1Address,
                Resp1Address1 = _.Resp1Address1,
                Resp1City = _.Resp1City,
                Resp1State = _.Resp1State,
                Resp1Zip = _.Resp1Zip,
                LicenseNumber = _.LicenseNumber,
                LicenseAgency = _.LicenseAgency,
                LicenseType = _.LicenseType,
                IsPlaceAddressHit = _.IsPlaceAddressHit,
                PlaceLastName = _.PlaceLastName,
                PlaceFirstName = _.PlaceFirstName,
                PlaceMiddleInitial = _.PlaceMiddleInitial,
                PlaceBBL = _.PlaceBBL,
                PlaceDistrictId = _.PlaceDistrictId,
                PlaceSectionId = _.PlaceSectionId,
                PlaceStreetId = _.PlaceStreetId,
                PlaceAddress1 = _.PlaceAddress1,
                PlaceAddress2 = _.PlaceAddress2,
                PlaceAddressDescriptor = _.PlaceAddressDescriptor,
                PlaceSideOfStreet = _.PlaceSideOfStreet,
                PlaceCross1StreetId = _.PlaceCross1StreetId,
                PlaceCross2StreetId = _.PlaceCross2StreetId,
                MailableAmount = _.MailableAmount,
                MaximumAmount = _.MaximumAmount,
                HearingTimestamp = _.HearingTimestamp,
                IsAppearRequired = _.IsAppearRequired,
                AlternateService = _.AlternateService,
                BuildingType = _.BuildingType,
                IsMultipleOffences = _.IsMultipleOffences,
                DigitalSignature = _.DigitalSignature,
                IsSent = _.IsSent,
                SentTimestamp = _.SentTimestamp,
                IsReceived = _.IsReceived,
                ReceivedTimestamp = _.ReceivedTimestamp,
                ViolationCode = _.ViolationCode,
                HHTIdentifier = _.HHTIdentifier,
                ViolationScript = _.ViolationScript,
                UserId = _.UserId,
                VoidCancelScreen = _.VoidCancelScreen,
                PlaceBoroCode = _.PlaceBoroCode,
                PlaceHouseNo = _.PlaceHouseNo,
                Resp1BoroCode = _.Resp1BoroCode,
                Resp1HouseNo = _.Resp1HouseNo,
                ViolationGroupId = _.ViolationGroupId,
                ViolationTypeId = _.ViolationTypeId,
                MDRNumber = _.MDRNumber,
                LicenseExpDate = _.LicenseExpDate,
                BusinessName = _.BusinessName,
                CheckSum = _.CheckSum,
                FreeAddrees = _.FreeAddrees,
                DeviceId = _.DeviceId,
                PublicKeyId = _.PublicKeyId,
                PrintViolationCode = _.PrintViolationCode,
                IsPetitionerCourtAppear = _.IsPetitionerCourtAppear,
                LicenseTypeDesc = _.LicenseTypeDesc,
                ViolGroupName = _.ViolGroupName,
                CodeLawDescription = _.CodeLawDescription,
                OfficerName = _.OfficerName,
                LawSection = _.LawSection,
                AbbrevName = _.AbbrevName,
                AgencyId = _.AgencyId,
                Title = _.Title,
                OrigViolationGroupId = _.OrigViolationGroupId,
                OrigViolationTypeId = _.OrigViolationTypeId,
                OrigViolationCode = _.OrigViolationCode,
                OrigHHTIdentifier = _.OrigHHTIdentifier,
                IsBusiness = _.IsBusiness
            });

            return tickets.ToList();
        }
        public async Task<List<NovInformation>> GetLaterViolations(string deviceId)
        {          
            var repo1 = DependencyResolver.Get<IRepository<DBLaterSvcInfo>>();

            //var alltickets = repo1.GetAsync<DBLaterSvcInfo>();

            var qTickets = repo1.AsQueryable();

            var sss= await qTickets.ToListAsync();

            if (!String.IsNullOrEmpty(deviceId))
                qTickets = qTickets
                    .Where(_ => _.DLDeviceId == deviceId);

            var _tickets = await qTickets.ToListAsync();
            var tickets = _tickets
                .Select(_ => new NovInformation
                {
                    NovNumber = _.NovNumber,
                    TicketStatus = _.TicketStatus,
                    IssuedTimestamp = _.IssuedTimestamp,
                    SystemTimestamp = _.SystemTimestamp,
                    LoginTimestamp = _.LoginTimestamp,
                    ReportLevel = _.ReportLevel,
                    IsResp1AddressHit = _.IsResp1AddressHit,
                    Resp1LastName = _.Resp1LastName,
                    Resp1FirstName = _.Resp1FirstName,
                    Resp1MiddleInitial = _.Resp1MiddleInitial,
                    Resp1Sex = _.Resp1Sex,
                    PropertyBBL = _.PropertyBBL,
                    Resp1DistrictId = _.Resp1DistrictId,
                    Resp1SectionId = _.Resp1SectionId,
                    Resp1StreetId = _.Resp1StreetId,
                    Resp1Address = _.Resp1Address,
                    Resp1Address1 = _.Resp1Address1,
                    Resp1City = _.Resp1City,
                    Resp1State = _.Resp1State,
                    Resp1Zip = _.Resp1Zip,
                    LicenseNumber = _.LicenseNumber,
                    LicenseAgency = _.LicenseAgency,
                    LicenseType = _.LicenseType,
                    IsPlaceAddressHit = _.IsPlaceAddressHit,
                    PlaceLastName = _.PlaceLastName,
                    PlaceFirstName = _.PlaceFirstName,
                    PlaceMiddleInitial = _.PlaceMiddleInitial,
                    PlaceBBL = _.PlaceBBL,
                    PlaceDistrictId = _.PlaceDistrictId,
                    PlaceSectionId = _.PlaceSectionId,
                    PlaceStreetId = _.PlaceStreetId,
                    PlaceAddress1 = _.PlaceAddress1,
                    PlaceAddress2 = _.PlaceAddress2,
                    PlaceAddressDescriptor = _.PlaceAddressDescriptor,
                    PlaceSideOfStreet = _.PlaceSideOfStreet,
                    PlaceCross1StreetId = _.PlaceCross1StreetId,
                    PlaceCross2StreetId = _.PlaceCross2StreetId,
                    MailableAmount = _.MailableAmount,
                    MaximumAmount = _.MaximumAmount,
                    HearingTimestamp = _.HearingTimestamp,
                    IsAppearRequired = _.IsAppearRequired,
                    AlternateService = _.AlternateService,
                    BuildingType = _.BuildingType,
                    IsMultipleOffences = _.IsMultipleOffences,
                    DigitalSignature = _.DigitalSignature,
                    IsSent = _.IsSent,
                    SentTimestamp = _.SentTimestamp,
                    IsReceived = _.IsReceived,
                    ReceivedTimestamp = _.ReceivedTimestamp,
                    ViolationCode = _.ViolationCode,
                    HHTIdentifier = _.HHTIdentifier,
                    ViolationScript = _.ViolationScript,
                    UserId = _.UserId,
                    VoidCancelScreen = _.VoidCancelScreen,
                    PlaceBoroCode = _.PlaceBoroCode,
                    PlaceHouseNo = _.PlaceHouseNo,
                    Resp1BoroCode = _.Resp1BoroCode,
                    Resp1HouseNo = _.Resp1HouseNo,
                    ViolationGroupId = _.ViolationGroupId,
                    ViolationTypeId = _.ViolationTypeId,
                    MDRNumber = _.MDRNumber,
                    LicenseExpDate = _.LicenseExpDate,
                    BusinessName = _.BusinessName,
                    CheckSum = _.CheckSum,
                    FreeAddrees = _.FreeAddrees,
                    DeviceId = _.DeviceId,
                    PublicKeyId = _.PublicKeyId,
                    PrintViolationCode = _.PrintViolationCode,
                    IsPetitionerCourtAppear = _.IsPetitionerCourtAppear,
                    LicenseTypeDesc = _.LicenseTypeDesc,
                    ViolGroupName = _.ViolGroupName,
                    CodeLawDescription = _.CodeLawDescription,
                    OfficerName = _.OfficerName,
                    LawSection = _.LawSection,
                    AbbrevName = _.AbbrevName,
                    AgencyId = _.AgencyId,
                    Title = _.Title,
                    OrigViolationGroupId = _.OrigViolationGroupId,
                    OrigViolationTypeId = _.OrigViolationTypeId,
                    OrigViolationCode = _.OrigViolationCode,
                    OrigHHTIdentifier = _.OrigHHTIdentifier,
                    DLDeviceId = _.DLDeviceId
                });

            return tickets.ToList();
        }

        public async Task<List<CodeLaw>> GetCodeLaws()
        {
            var repo = DependencyResolver.Get<IRepository<DBCodeLaw>>();
            var codeLaws = await repo.GetAsync();

            return codeLaws.Select(_ => new CodeLaw
            {
                CodeLawId = _.CodeLawId,
                CodeLawDescription = _.CodeLawDescription
            }).ToList();
        }

        private async Task SaveNovInfo(DBNovInformation dbNovInfo)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            await repo.UpdateAsync(dbNovInfo);
        }

        private async Task SaveNovInfo(NovMaster novMaster)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            var isNewNov = false;
            var dbNovInfo = await repo.GetAsync(_ => _.NovNumber == novMaster.NovInformation.NovNumber);
            if (dbNovInfo == null)
            {
                isNewNov = true;
                dbNovInfo = new DBNovInformation();
            }

            MapViolation(dbNovInfo, novMaster);

            if (isNewNov)
                await repo.InsertAsync(dbNovInfo);
            else
                await repo.UpdateAsync(dbNovInfo);
        }

        private async Task SaveNovCertificate (NovMaster novMaster)
        {
            //because we save the NOV incrementally, it's possible that we may not have signed the affidavit or AOS,
            //which is when the certificate is associated. Therefore, if we haven't filled in cert, just jump back.
            if (novMaster.NovCertificate == null)
                return;

            var repo = DependencyResolver.Get<IRepository<DBNovPublicKey>>();
            var dbCert = await repo.GetAsync(_ => _.NovNumber == novMaster.NovInformation.NovNumber);

            var mappedCert = new DBNovPublicKey()
            {
                NovNumber = novMaster.NovCertificate.NovNumber,
                Certificate = novMaster.NovCertificate.Certificate
            };

            if (dbCert == null)
                await repo.InsertAsync(mappedCert);
            else
                await repo.UpdateAsync(mappedCert);
        }

        public async Task DeleteNovInfo()
        {
            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            //var dbNovInfo = await repo.GetAsync(_ => _.NovNumber.Equals(0) || (_.TicketStatus.Equals("C")));
            var dbNovInfo = await repo.GetAsync(_ => _.NovNumber.Equals(0));
            if (dbNovInfo != null)
            {
                await repo.DeleteAsync(dbNovInfo);
            }
        }

        public async Task DeleteNovInfo(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            var dbNovInfo = await repo.GetAsync(_ => _.NovNumber.Equals(novNumber) || (_.TicketStatus.Equals("C")));
            if (dbNovInfo != null)
            {
                await repo.DeleteAsync(dbNovInfo);
            }
        }

        public async Task DeleteLaterSrvNovInfo(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBLaterSvcInfo>>();
            var dbNovInfo = await repo.GetAsync(_ => _.NovNumber.Equals(novNumber));
            if (dbNovInfo != null)
            {
                await repo.DeleteAsync(dbNovInfo);
            }
        }


        public async Task VoidIncompleteTicketExceptThisUser(String userID) {
            if (string.IsNullOrEmpty(userID))
                return ;

            var repo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            //var dbNovData = await repo.GetAsync(_ => !_.UserId.Equals(userID) && (_.TicketStatus.Equals("I")) );
            List<DBNovInformation> dbNovData = await repo.GetAsync<DBNovInformation>();

            var incompleteTicketsByAnotherUser = dbNovData.FindAll(_ => !_.UserId.Equals(userID) && (_.TicketStatus.Equals("I")) );

            if (incompleteTicketsByAnotherUser != null && incompleteTicketsByAnotherUser.Count>0) {
                foreach (var novInfo in incompleteTicketsByAnotherUser) {
                    novInfo.TicketStatus = "V";
                    SaveNovInfo(novInfo);
                }
            }

        }

        public async Task UpdateNovData(NovMaster novMaster)
        {

            var repo = DependencyResolver.Get<IRepository<DBNovData>>();
            var novNumber = novMaster.NovInformation.NovNumber;
            var dbNovData = await repo.GetAsync<DBNovData>(_ => _.NovNumber.Equals(novNumber));

            if (novNumber == 0) // For new tickets, we will not have a NovNumber assigned in the db yet
            {
                novNumber = await IncrementTicketValue() ?? 0;
                novMaster.NovInformation.NovNumber = novNumber;
            }
            
            for (var i = 0; i < novMaster.NovData?.Count; i++)
            {
                novMaster.NovData[i].NovNumber = (long)novNumber;
            }

            //Fix for NH-875
            //if (dbNovData.Count != 0)
            //{
            //    novMaster.NovData = dbNovData.Select(_ => new NovData
            //    {
            //        NovNumber = novMaster.NovInformation.NovNumber,
            //        FieldName = _.FieldName,
            //        Data = _.Data
            //    }).ToList();
            //}
            
            if (dbNovData.Count != 0 && dbNovData[0].NovNumber == 0)
            {
                for (var i = 0; i < dbNovData.Count; i++)
                {
                    await repo.DeleteAsync(dbNovData[i]);
                }
            }
            await SaveNovData(novMaster);
            return;
        }

        public async Task UpdateCancelNovData(NovMaster novMaster)
        {
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBCancelNovData>>();

                var dbCancelNovDatas = new List<DBCancelNovData>();
                //var dbNovData = await repo.GetAsync<DBNovData>(_ => _.NovNumber.Equals(novMaster.NovInformation.NovNumber));
                MapCancelNovData(dbCancelNovDatas, novMaster);
                await repo.InsertAllAsync(dbCancelNovDatas);
            }
            catch(Exception ex)
            {
                //Charan wrote on: 04/04/2018
                // the table is not designed properly and DBCancelNovData has primay key on DeviceId and if  this the case you can not insert muliple child of Same device
                string error = ex.Message;
            }

            
        }
        private void MapViolation(DBNovInformation dbNovInfo, NovMaster novMaster)
        {
            dbNovInfo.NovNumber = novMaster.NovInformation.NovNumber;
            dbNovInfo.TicketStatus = novMaster.NovInformation.TicketStatus;
            dbNovInfo.IssuedTimestamp = novMaster.NovInformation.IssuedTimestamp;
            dbNovInfo.SystemTimestamp = novMaster.NovInformation.SystemTimestamp;
            dbNovInfo.LoginTimestamp = novMaster.NovInformation.LoginTimestamp;
            dbNovInfo.ReportLevel = novMaster.NovInformation.ReportLevel;
            dbNovInfo.DeviceId = novMaster.NovInformation.DeviceId;
            dbNovInfo.AbbrevName = novMaster.NovInformation.AbbrevName;
            dbNovInfo.AgencyId = novMaster.NovInformation.AgencyId;
            dbNovInfo.AlternateService = string.IsNullOrWhiteSpace(novMaster.NovInformation.AlternateService)
                ? " "
                : novMaster.NovInformation.AlternateService;
            dbNovInfo.LawSection = novMaster.NovInformation.LawSection;
            dbNovInfo.Title = novMaster.NovInformation.Title;
            dbNovInfo.OrigViolationGroupId = novMaster.NovInformation.OrigViolationGroupId;
            dbNovInfo.OrigViolationTypeId = novMaster.NovInformation.OrigViolationTypeId;
            dbNovInfo.OrigViolationCode = novMaster.NovInformation.OrigViolationCode;
            dbNovInfo.OrigHHTIdentifier = string.IsNullOrWhiteSpace(novMaster.NovInformation.OrigHHTIdentifier)
                ? " "
                : novMaster.NovInformation.OrigHHTIdentifier;
            dbNovInfo.IsResp1AddressHit = NullToString(novMaster.NovInformation.IsResp1AddressHit);
            dbNovInfo.Resp1LastName = NullToString(novMaster.NovInformation.Resp1LastName);
            dbNovInfo.Resp1FirstName = NullToString(novMaster.NovInformation.Resp1FirstName);
            dbNovInfo.Resp1MiddleInitial = NullToString(novMaster.NovInformation.Resp1MiddleInitial);
            dbNovInfo.Resp1Sex = NullToString(novMaster.NovInformation.Resp1Sex);
            dbNovInfo.PropertyBBL = NullToString(novMaster.NovInformation.PropertyBBL);
            dbNovInfo.Resp1DistrictId = NullToString(novMaster.NovInformation.Resp1DistrictId);
            dbNovInfo.Resp1SectionId = NullToString(novMaster.NovInformation.Resp1SectionId);
            dbNovInfo.Resp1StreetId = novMaster.NovInformation.Resp1StreetId;
            dbNovInfo.Resp1Address = novMaster.NovInformation.Resp1Address;
            dbNovInfo.Resp1Address1 = string.IsNullOrWhiteSpace(novMaster.NovInformation.Resp1Address1)
                ? " "
                : novMaster.NovInformation.Resp1Address1;
            dbNovInfo.Resp1City = novMaster.NovInformation.Resp1City;
            dbNovInfo.Resp1State = novMaster.NovInformation.Resp1State;
            dbNovInfo.Resp1Zip = novMaster.NovInformation.Resp1Zip;
            dbNovInfo.LicenseNumber = novMaster.NovInformation.LicenseNumber;
            dbNovInfo.LicenseAgency = novMaster.NovInformation.LicenseAgency;
            //dbNovInfo.LicenseType = novMaster.NovInformation.LicenseType;  //NH-917 move 
            dbNovInfo.IsPlaceAddressHit = novMaster.NovInformation.IsPlaceAddressHit;
            dbNovInfo.PlaceLastName = NullToString(novMaster.NovInformation.PlaceLastName);
            dbNovInfo.PlaceFirstName = NullToString(novMaster.NovInformation.PlaceFirstName);
            dbNovInfo.PlaceMiddleInitial = NullToString(novMaster.NovInformation.PlaceMiddleInitial);
            dbNovInfo.PlaceBBL = NullToString(novMaster.NovInformation.PlaceBBL);
            dbNovInfo.PlaceDistrictId = string.IsNullOrWhiteSpace(novMaster.NovInformation.PlaceDistrictId)
                ? " "
                : novMaster.NovInformation.PlaceDistrictId;
            dbNovInfo.PlaceSectionId = string.IsNullOrWhiteSpace(novMaster.NovInformation.PlaceSectionId)
                ? " "
                : novMaster.NovInformation.PlaceSectionId;
            dbNovInfo.VoidCancelScreen = NullToString(novMaster.NovInformation.VoidCancelScreen);
            dbNovInfo.PlaceBoroCode = novMaster.NovInformation.PlaceBoroCode;
            dbNovInfo.PlaceHouseNo = NullToString(novMaster.NovInformation.PlaceHouseNo);
            dbNovInfo.Resp1BoroCode = novMaster.NovInformation.Resp1BoroCode;
            dbNovInfo.Resp1HouseNo = novMaster.NovInformation.Resp1HouseNo;
            dbNovInfo.ViolationGroupId = novMaster.NovInformation.ViolationGroupId;
            dbNovInfo.ViolationTypeId = novMaster.NovInformation.ViolationTypeId;
            dbNovInfo.MDRNumber = novMaster.NovInformation.MDRNumber;
            
            //NH-917 
            dbNovInfo.LicenseExpDate = novMaster.NovInformation.LicenseExpDate;
            dbNovInfo.LicenseType = novMaster.NovInformation.LicenseType;
            dbNovInfo.LicenseTypeDesc = novMaster.NovInformation.LicenseTypeDesc;
            // NH-917 end

            dbNovInfo.BusinessName = novMaster.NovInformation.BusinessName;
            dbNovInfo.CheckSum = novMaster.NovInformation.CheckSum;
            dbNovInfo.FreeAddrees = novMaster.NovInformation.FreeAddrees;
            dbNovInfo.PublicKeyId = novMaster.NovInformation.PublicKeyId;
            dbNovInfo.PrintViolationCode = novMaster.NovInformation.PrintViolationCode;
            dbNovInfo.IsPetitionerCourtAppear = novMaster.NovInformation.IsPetitionerCourtAppear;
            //dbNovInfo.LicenseTypeDesc = novMaster.NovInformation.LicenseTypeDesc;  // NH-917 move
            dbNovInfo.ViolGroupName = novMaster.NovInformation.ViolGroupName;
            dbNovInfo.CodeLawDescription = novMaster.NovInformation.CodeLawDescription;
            dbNovInfo.OfficerName = novMaster.NovInformation.OfficerName;
            dbNovInfo.PlaceStreetId = novMaster.NovInformation.PlaceStreetId;
            dbNovInfo.PlaceAddress1 = novMaster.NovInformation.PlaceAddress1;
            dbNovInfo.PlaceAddress2 = novMaster.NovInformation.PlaceAddress2;
            dbNovInfo.PlaceAddressDescriptor = novMaster.NovInformation.PlaceAddressDescriptor;
            dbNovInfo.PlaceSideOfStreet = NullToString(novMaster.NovInformation.PlaceSideOfStreet);
            dbNovInfo.PlaceCross1StreetId = novMaster.NovInformation.PlaceCross1StreetId;
            dbNovInfo.PlaceCross2StreetId = novMaster.NovInformation.PlaceCross2StreetId;
            dbNovInfo.MailableAmount = novMaster.NovInformation.MailableAmount;
            dbNovInfo.MaximumAmount = novMaster.NovInformation.MaximumAmount;
            dbNovInfo.HearingTimestamp = novMaster.NovInformation.HearingTimestamp;
            dbNovInfo.IsAppearRequired = string.IsNullOrWhiteSpace(novMaster.NovInformation.IsAppearRequired)
                ? " "
                : novMaster.NovInformation.IsAppearRequired;
            dbNovInfo.BuildingType = NullToString(novMaster.NovInformation.BuildingType);
            dbNovInfo.IsMultipleOffences = novMaster.NovInformation.IsMultipleOffences;
            dbNovInfo.DigitalSignature = novMaster.NovInformation.DigitalSignature;
            dbNovInfo.IsSent = novMaster.NovInformation.IsSent;
            dbNovInfo.SentTimestamp = novMaster.NovInformation.SentTimestamp;
            dbNovInfo.IsReceived = novMaster.NovInformation.IsReceived;
            dbNovInfo.ReceivedTimestamp = novMaster.NovInformation.ReceivedTimestamp;
            dbNovInfo.ViolationCode = novMaster.NovInformation.ViolationCode;
            dbNovInfo.HHTIdentifier = string.IsNullOrWhiteSpace(novMaster.NovInformation.HHTIdentifier)
                ? " "
                : novMaster.NovInformation.HHTIdentifier;
            dbNovInfo.ViolationScript = novMaster.NovInformation.ViolationScript;
            dbNovInfo.UserId = novMaster.NovInformation.UserId;
            dbNovInfo.IsBusiness = novMaster.NovInformation.IsBusiness;
        }

        private async Task SaveNovData(NovMaster novMaster)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovData>>();
            var dbNovData = await repo.GetAsync<NovData>(_ => _.NovNumber == novMaster.NovInformation.NovNumber);
            if (dbNovData.Count != 0)
            {
                for (var i = 0; i < dbNovData.Count; i++)
                {
                    await repo.DeleteAsync(dbNovData[i]);
                }
            }
            foreach (var novData in novMaster.NovData)
            {
                var dbNovDataNewEntry = new DBNovData();
                MapViolationDetails(dbNovDataNewEntry, novData);
                await repo.InsertAsync(dbNovDataNewEntry);
            }
        }

        public async Task SaveCancelNovInformation(NovMaster novMaster)
        {
            var repo = DependencyResolver.Get<IRepository<DBCancelNovInformation>>();
            var dbCancelNovInformation = new DBCancelNovInformation();
            var dbDBNovInformation = new DBNovInformation();
            MapCancelNovInformation(dbCancelNovInformation, novMaster);

            try
            {
                await repo.InsertAsync(dbCancelNovInformation);
                await UpdateCancelNovData(novMaster);
                //delete Novdata entries which is already saved with Nov information
                await DeleteNovData(novMaster.NovInformation.NovNumber);

                //delete the  nov form NovInformation which is already strord and  same nov is get cancelled
                //await DeleteNovInfo(novMaster.NovInformation.NovNumber);
            }
            catch(Exception ex)
            {
                string strError = ex.Message;
            }
            
        }


        private void MapCancelNovInformation(DBCancelNovInformation dbCancelNovInformation, NovMaster novMaster)
        {
            
            // cancel nov number should be last 7 digit of actual nov number
            string canNov = novMaster.NovInformation.NovNumber.ToString().Substring(1, 7); ;

            dbCancelNovInformation.CancelNo = String.Format("C" + canNov);
            dbCancelNovInformation.DeviceId = novMaster.NovInformation.DeviceId;
            dbCancelNovInformation.TicketStatus = novMaster.NovInformation.TicketStatus;
            dbCancelNovInformation.SystemTimestamp = novMaster.NovInformation.SystemTimestamp;
            //dbCancelNovInformation.LoginTimestamp = new DateTime(1900, 1, 1);
            dbCancelNovInformation.LoginTimestamp = novMaster.UserSession.DutyHeader.LoginTimestamp;
            dbCancelNovInformation.ReportLevel = novMaster.NovInformation.ReportLevel.Substring(0, 4);
            dbCancelNovInformation.IsResp1AddressHit = "";
            dbCancelNovInformation.Resp1LastName = novMaster.NovInformation.Resp1LastName;
            dbCancelNovInformation.Resp1FirstName = novMaster.NovInformation.Resp1FirstName;
            dbCancelNovInformation.Resp1MiddleInitial = novMaster.NovInformation.Resp1MiddleInitial;
            dbCancelNovInformation.Resp1Sex = novMaster.NovInformation.Resp1Sex;
            dbCancelNovInformation.PropertyBBL = "";
            dbCancelNovInformation.Resp1DistrictId = "";
            dbCancelNovInformation.Resp1SectionId = "";
            dbCancelNovInformation.Resp1StreetId = 0;
            dbCancelNovInformation.Resp1HouseNo = "";
            dbCancelNovInformation.Resp1Address1 = "";
            dbCancelNovInformation.Resp1City = novMaster.NovInformation.Resp1City;
            dbCancelNovInformation.Resp1State = novMaster.NovInformation.Resp1State;
            dbCancelNovInformation.Resp1Zip = novMaster.NovInformation.Resp1Zip;
            dbCancelNovInformation.Resp1BoroCode = "";
            dbCancelNovInformation.LicenseNumber = novMaster.NovInformation.LicenseNumber;
            dbCancelNovInformation.LicenseAgency = novMaster.NovInformation.LicenseAgency;
            dbCancelNovInformation.LicenseType = novMaster.NovInformation.LicenseType;
            dbCancelNovInformation.LicenseTypeDesc = novMaster.NovInformation.LicenseTypeDesc;
            dbCancelNovInformation.IsPlaceAddressHit = novMaster.NovInformation.IsPlaceAddressHit;
            dbCancelNovInformation.PlaceLastName = "";
            dbCancelNovInformation.PlaceFirstName = "";
            dbCancelNovInformation.PlaceMiddleInitial = "";
            dbCancelNovInformation.PlaceBBL = novMaster.NovInformation.PlaceBBL;
            dbCancelNovInformation.PlaceDistrictId = novMaster.NovInformation.PlaceDistrictId;
            dbCancelNovInformation.PlaceSectionId = novMaster.NovInformation.PlaceSectionId;
            dbCancelNovInformation.PlaceStreetId = novMaster.NovInformation.PlaceStreetId;
            dbCancelNovInformation.PlaceAddress1 = "";
            dbCancelNovInformation.PlaceAddress2 = "";
            dbCancelNovInformation.PlaceAddressDescriptor = novMaster.NovInformation.PlaceAddressDescriptor;
            dbCancelNovInformation.PlaceSideOfStreet = NullToString(novMaster.NovInformation.PlaceSideOfStreet);
            dbCancelNovInformation.PlaceCross1StreetId = novMaster.NovInformation.PlaceCross1StreetId;
            dbCancelNovInformation.PlaceCross2StreetId = novMaster.NovInformation.PlaceCross2StreetId;
            dbCancelNovInformation.MailableAmount = novMaster.NovInformation.MailableAmount;
            dbCancelNovInformation.MaximumAmount = novMaster.NovInformation.MaximumAmount;
            dbCancelNovInformation.HearingTimestamp = novMaster.NovInformation.HearingTimestamp;
            dbCancelNovInformation.IsAppearRequired = "";
            dbCancelNovInformation.AlternateService = "";
            dbCancelNovInformation.BuildingType = "";
            dbCancelNovInformation.IsMultipleOffences = novMaster.NovInformation.IsMultipleAttempts ? "Y" : "N";
            dbCancelNovInformation.DigitalSignature = novMaster.NovInformation.DigitalSignature;
            dbCancelNovInformation.IsSent = "N";
            dbCancelNovInformation.SentTimestamp = new DateTime(1900, 1, 1);
            dbCancelNovInformation.IsReceived = "N";
            dbCancelNovInformation.ReceivedTimestamp = new DateTime(1900, 1, 1);
            dbCancelNovInformation.ViolGroupName = novMaster.NovInformation.ViolGroupName;
            dbCancelNovInformation.ViolationCode = novMaster.NovInformation.ViolationCode;
            dbCancelNovInformation.HHTIdentifier = novMaster.NovInformation.HHTIdentifier;
            dbCancelNovInformation.ViolationScript = novMaster.NovInformation.ViolationScript;
            dbCancelNovInformation.UserId = novMaster.NovInformation.UserId;
            dbCancelNovInformation.VoidCancelScreen = novMaster.NovInformation.VoidCancelScreen;
            dbCancelNovInformation.PlaceBoroCode = novMaster.NovInformation.PlaceBoroCode;
            dbCancelNovInformation.PlaceHouseNo = novMaster.NovInformation.PlaceHouseNo;
            dbCancelNovInformation.ViolationGroupId = novMaster.NovInformation.ViolationGroupId.ToString();
            dbCancelNovInformation.ViolationTypeId = novMaster.NovInformation.ViolationTypeId;
            dbCancelNovInformation.MDRNumber = novMaster.NovInformation.MDRNumber.ToString();
            dbCancelNovInformation.LicenseExpDate = novMaster.NovInformation.LicenseExpDate;
            dbCancelNovInformation.BusinessName = novMaster.NovInformation.BusinessName;
            dbCancelNovInformation.CheckSum = novMaster.NovInformation.CheckSum;
            dbCancelNovInformation.FreeAddress = novMaster.NovInformation.FreeAddrees;
            dbCancelNovInformation.PrintViolationCode = novMaster.NovInformation.PrintViolationCode;
            dbCancelNovInformation.CodeLawDescription = novMaster.NovInformation.CodeLawDescription;
            dbCancelNovInformation.OfficerName = novMaster.NovInformation.OfficerName;
            dbCancelNovInformation.LawSection = novMaster.NovInformation.LawSection;
            dbCancelNovInformation.AbbrevName = novMaster.NovInformation.AbbrevName;
            dbCancelNovInformation.AgencyId = novMaster.NovInformation.AgencyId;
            dbCancelNovInformation.Title = novMaster.NovInformation.Title;

        }

        private void MapCancelNovData(IList<DBCancelNovData> canList, NovMaster novMaster)
        {
            //canList = new List<DBCancelNovData>();
            foreach(var item in novMaster.NovData)
            {
                canList.Add(new DBCancelNovData
                {
                    DeviceId = novMaster.NovInformation.DeviceId,
                    FieldName = item.FieldName,
                    Data = item.Data,
                    SystemTimestamp = DateTime.Now
                });
            }
        }

        public async Task<List<CancelNovInformation>> GetCancelNovInformation()
        {
            var cancelNovInformation = await GetCancelNovInformation(null);

            return cancelNovInformation;
        }

        public async Task<List<CancelNovInformation>> GetCancelNovInformation(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBCancelNovInformation>>();
            var dbCancelNovInformation = repo.AsQueryable();
            
            if (!String.IsNullOrEmpty(userId))
                dbCancelNovInformation = dbCancelNovInformation
                    .Where(_ => _.UserId == userId);

            var _cancelNovInformation = await dbCancelNovInformation.ToListAsync();
            var cancelNovInformation = _cancelNovInformation
                .Select(_ => new CancelNovInformation
                {
                    CancelNo = _.CancelNo,
                    DeviceId = _.DeviceId,
                    TicketStatus = _.TicketStatus,
                    SystemTimestamp = _.SystemTimestamp,
                    LoginTimestamp = _.LoginTimestamp,
                    ReportLevel = _.ReportLevel,
                    IsResp1AddressHit = _.IsResp1AddressHit,
                    Resp1LastName = _.Resp1LastName,
                    Resp1FirstName = _.Resp1FirstName,
                    Resp1MiddleInitial = _.Resp1MiddleInitial,
                    Resp1Sex = _.Resp1Sex,
                    PropertyBBL = _.PropertyBBL,
                    Resp1DistrictId = _.Resp1DistrictId,
                    Resp1SectionId = _.Resp1SectionId,
                    Resp1StreetId = _.Resp1StreetId,
                    Resp1HouseNo = _.Resp1HouseNo,
                    Resp1Address1 = _.Resp1Address1,
                    Resp1City = _.Resp1City,
                    Resp1State = _.Resp1State,
                    Resp1Zip = _.Resp1Zip,
                    Resp1BoroCode = _.Resp1BoroCode,
                    LicenseNumber = _.LicenseNumber,
                    LicenseAgency = _.LicenseAgency,
                    LicenseType = _.LicenseType,
                    LicenseTypeDesc = _.LicenseTypeDesc,
                    IsPlaceAddressHit = _.IsPlaceAddressHit,
                    PlaceLastName = _.PlaceLastName,
                    PlaceFirstName = _.PlaceFirstName,
                    PlaceMiddleInitial = _.PlaceMiddleInitial,
                    PlaceBBL = _.PlaceBBL,
                    PlaceDistrictId = _.PlaceDistrictId,
                    PlaceSectionId = _.PlaceSectionId,
                    PlaceStreetId = _.PlaceStreetId,
                    PlaceAddress1 = _.PlaceAddress1,
                    PlaceAddress2 = _.PlaceAddress2,
                    PlaceAddressDescriptor = _.PlaceAddressDescriptor,
                    PlaceSideOfStreet = _.PlaceSideOfStreet,
                    PlaceCross1StreetId = _.PlaceCross1StreetId,
                    PlaceCross2StreetId = _.PlaceCross2StreetId,
                    MailableAmount = _.MailableAmount,
                    MaximumAmount = _.MaximumAmount,
                    HearingTimestamp = _.HearingTimestamp,
                    IsAppearRequired = _.IsAppearRequired,
                    AlternateService = _.AlternateService,
                    BuildingType = _.BuildingType,
                    IsMultipleOffences = _.IsMultipleOffences,
                    IsSent = _.IsSent,
                    SentTimestamp = _.SentTimestamp,
                    IsReceived = _.IsReceived,
                    ReceivedTimestamp = _.ReceivedTimestamp,
                    ViolGroupName = _.ViolGroupName,
                    ViolationCode = _.ViolationCode,
                    HHTIdentifier = _.HHTIdentifier,
                    UserId = _.UserId,
                    VoidCancelScreen = _.VoidCancelScreen,
                    PlaceBoroCode = _.PlaceBoroCode,
                    PlaceHouseNo = _.PlaceHouseNo,
                    ViolationGroupId = _.ViolationGroupId,
                    ViolationTypeId = _.ViolationTypeId,
                    MDRNumber = _.MDRNumber,
                    LicenseExpDate = _.LicenseExpDate,
                    BusinessName = _.BusinessName,
                    CheckSum = _.CheckSum,
                    FreeAddress = _.FreeAddress,
                    PrintViolationCode = _.PrintViolationCode,
                    CodeLawDescription = _.CodeLawDescription,
                    OfficerName = _.OfficerName,
                    LawSection = _.LawSection,
                    AbbrevName = _.AbbrevName,
                    AgencyId = _.AgencyId,
                    Title = _.Title
                });
            
            return cancelNovInformation.ToList();
        }

        public async Task SaveDutyHeader_old(UserSession userSession)
        {

            //await SaveDutyHeader_new(userSession);

            //TODO: Figure out why this logic could be broken 
            var _vehicleService= DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
            var veh = await _vehicleService.GetAsync();
            var currentVhe = veh.Where(m => m.UserId == userSession.DutyHeader.UserId).OrderByDescending(n=>n.LoginTimestamp).FirstOrDefault();
            if (currentVhe != null)
                userSession.DutyHeader.VehicleRadioId = currentVhe.VehicleRadioId;
            var repo = DependencyResolver.Get<IRepository<DBDutyHeader>>();
            var dbDutyHeader = new DBDutyHeader();
            var logoutDateToString = (new DateTime(1, 1, 1)).ToString();
            // var dbDutyHeaderRepo = await repo.GetAsync(_ => _.UserId.Equals(userSession.DutyHeader.UserId) && _.LogoutTimestamp.Equals(0));
            var dbDutyHeaderRepo2 = await repo.GetAsync();
            var dbDutyHeaderRepo = await repo.GetAsync(_ => _.UserId.Equals(userSession.DutyHeader.UserId) && _.LogoutTimestamp.Equals(0));
            MapDutyHeader(dbDutyHeader, userSession);
            if (dbDutyHeaderRepo != null)
            {
                //need to add a query for the existing data to modify that
                await repo.DeleteAsync(dbDutyHeaderRepo);
                await repo.InsertAsync(dbDutyHeader);
                //2-1-2018: Added query to modify existing data, not sure why Update was not used in the first place. I will leave it as is for now
                //await repo.UpdateAsync(dbDutyHeader);
            }
            else
            {
                await repo.InsertAsync(dbDutyHeader);
            }
        }


        public async Task SaveDutyHeader(UserSession userSession)
        {

            //update VehicleRadioId
            var _vehicleService = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
            var veh = await _vehicleService.GetAsync();
            var currentVhe = veh.Where(m => m.UserId == userSession.DutyHeader.UserId).OrderByDescending(n => n.LoginTimestamp).FirstOrDefault();
            if (currentVhe != null)
                userSession.DutyHeader.VehicleRadioId = currentVhe.VehicleRadioId;


            var repo = DependencyResolver.Get<IRepository<DBDutyHeader>>();
            var dbDutyHeader = new DBDutyHeader();
            var dbDutyHeaderRepo2 = await repo.GetAsync();

            var logoutDate = (new DateTime(1, 1, 1));
            string sql = string.Format("select * from DBDutyHeader where userId='{0}'", userSession.UserId);

            var dutys=await repo.QueryAsync(sql);

            var currentDuty = dutys.Where(m => m.LogoutTimestamp == logoutDate);


            if (currentDuty.Count()>0)
            {
                //update dutyheader

                try
                {
                    var dty = currentDuty.ToList().FirstOrDefault();
                    MapDutyHeader(dty, userSession);
                    await repo.UpdateAsync(dty);

                }
                catch(Exception ex)
                {
                    string str = ex.Message;
                }
            }
            else
            {
                //inset new dutyheader
                MapDutyHeader(dbDutyHeader, userSession);
                await repo.InsertAsync(dbDutyHeader);
            }


        }


        private void MapDutyHeader(DBDutyHeader dbDutyHeader, UserSession userSession)
        {
           

            dbDutyHeader.UserId = userSession.DutyHeader.UserId;
            dbDutyHeader.LoginTimestamp = userSession.DutyHeader.LoginTimestamp;
            dbDutyHeader.BoroId = userSession.DutyHeader.BoroId;
            dbDutyHeader.DeviceId = userSession.DutyHeader.DeviceId;
            dbDutyHeader.SiteId = userSession.DutyHeader.SiteId;
            dbDutyHeader.Title = userSession.DutyHeader.Title;
            dbDutyHeader.VehicleRadioId = userSession.DutyHeader.VehicleRadioId;
           // dbDutyHeader.VehicleRadioId = userSession.VehicleRadioId;
            dbDutyHeader.LogoutTimestamp = userSession.DutyHeader.LogoutTimestamp;
            dbDutyHeader.HearingDate = userSession.DutyHeader.HearingDate;
            dbDutyHeader.IsActingSupervisor = userSession.DutyHeader.IsActingSupervisor;
            dbDutyHeader.TicketCount = userSession.DutyHeader.TicketCount;
            dbDutyHeader.VoidCount = userSession.DutyHeader.VoidCount;
            dbDutyHeader.IsSent = userSession.DutyHeader.IsSent;
            dbDutyHeader.SentTimestamp = userSession.DutyHeader.SentTimestamp;
            dbDutyHeader.IsReceived = userSession.DutyHeader.IsReceived;
            dbDutyHeader.ReceivedTimestamp = userSession.DutyHeader.ReceivedTimestamp;
            dbDutyHeader.ReportingTimestamp = userSession.DutyHeader.ReportingTimestamp;
            dbDutyHeader.SignatureBitmap = userSession.DutyHeader.SignatureBitmap;
        }

        private void MapViolationDetails(DBNovData dbNovData, NovData novData)
        {
            dbNovData.NovNumber = novData.NovNumber;
            dbNovData.Data = novData.Data;
            dbNovData.FieldName = novData.FieldName;
        }

        private string NullToString(object Value)
        {

            return Value == null ? "" : Value.ToString();

        }
    }
}