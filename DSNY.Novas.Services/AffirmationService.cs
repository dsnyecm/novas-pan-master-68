﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;

using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class AffirmationService : IAffirmationService
    {
        public async Task<CourtLocations> GetCourtLocation(string violationCode, string boroCode)
        {
            string query = String.Format("Select * From DBCourtLocations Where RTrim(ViolationCode) = '{0}' and PlaceBoroCode = '{1}'", violationCode, boroCode);
            var repo = DependencyResolver.Get<IRepository<DBCourtLocations>>();
            var dbCourtLocations = await repo.QueryAsync(query);
            var courtLocation = dbCourtLocations.Select(_ => new CourtLocations
            {
                CourtBoroCode = _.CourtBoroCode
            }).FirstOrDefault();

            return courtLocation;
        }

        public async Task SignNov(NovMaster novMaster)
        {
            IRepository<DBNovInformation> novRepo = DependencyResolver.Get<IRepository<DBNovInformation>>();
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();

            try
            {
                DBNovInformation dbNovInfo = await novRepo.GetAsync(_ => _.NovNumber == novMaster.NovInformation.NovNumber);
                DBDevices dbDevice = await deviceRepo.GetAsync(_ => _.DeviceId == novMaster.NovInformation.DeviceId);
                byte[] salt = Encoding.UTF8.GetBytes(dbDevice.Salt);


                //if MDR is 0 then LicenseNumber and issue agency should be empty, this is to match old handheld device
                // this change will take care  replica as well.


                if (!string.IsNullOrEmpty(novMaster.NovInformation.LicenseNumber) || !string.IsNullOrEmpty(novMaster.NovInformation.LicenseTypeDesc))
                {
                    if (novMaster.NovInformation.LicenseTypeDesc.Trim().ToLower() == "multiple dwelling registration" && novMaster.NovInformation.LicenseNumber.ToString() == "0")
                    {
                        novMaster.NovInformation.LicenseTypeDesc = "";
                        novMaster.NovInformation.LicenseAgency = "";
                        novMaster.NovInformation.LicenseNumber = "";
                    }
                }

                var plainText = string.Empty;
#if TEST

            plainText = "I love Boston MA.";
#else
                var novInfo = novMaster.NovInformation;

                DateTime dt=DateTime.Parse("1/1/0001 12:00:00 AM");

                //****OLD code***
                //plainText = novInfo.NovNumber + novInfo.IssuedTimestamp.ToString("MM/dd/yyyy hh:mm:ss") + novInfo.Resp1LastName + novInfo.Resp1FirstName +
                //    novInfo.Resp1MiddleInitial + novInfo.Resp1Sex + novInfo.PropertyBBL + novInfo.Resp1DistrictId +
                //    novInfo.Resp1SectionId + novInfo.Resp1StreetId + novInfo.Resp1Address + novInfo.Resp1Address1 +
                //    novInfo.Resp1City + novInfo.Resp1State + novInfo.Resp1Zip + novInfo.LicenseNumber +
                //    novInfo.LicenseAgency + novInfo.LicenseType + novInfo.PlaceLastName +
                //    novInfo.PlaceFirstName + novInfo.PlaceMiddleInitial + novInfo.PlaceBBL + novInfo.PlaceDistrictId +
                //    novInfo.PlaceSectionId + novInfo.PlaceStreetId + novInfo.PlaceAddress1 + novInfo.PlaceAddress2 +
                //    novInfo.PlaceAddressDescriptor + novInfo.PlaceSideOfStreet + novInfo.PlaceCross1StreetId + novInfo.PlaceCross2StreetId +
                //    novInfo.MailableAmount.ToString("F2").PadLeft(9, '0') + novInfo.MaximumAmount.ToString("F2").PadLeft(9, '0') + novInfo.HearingTimestamp.ToString("MM/dd/yyyy hh:mm:ss") + novInfo.IsAppearRequired +
                //    novInfo.BuildingType + novInfo.IsMultipleOffences + novInfo.ViolationCode + novInfo.HHTIdentifier +
                //    novInfo.ViolationScript + novInfo.UserId + novInfo.PlaceBoroCode + novInfo.PlaceHouseNo +
                //    novInfo.ViolationGroupId + novInfo.ViolationTypeId + novInfo.MDRNumber + novInfo.LicenseExpDate.ToString("MM/dd/yyyy hh:mm:ss") +
                //    novInfo.BusinessName + novInfo.CheckSum + novInfo.FreeAddrees + novInfo.DeviceId + novInfo.PrintViolationCode +
                //    novInfo.CodeLawDescription + novInfo.OfficerName + novInfo.LawSection + novInfo.AbbrevName + novInfo.AgencyId + novInfo.Title;

                //****NEW code***
                plainText = novInfo.NovNumber.ToString().TrimEnd() + novInfo.IssuedTimestamp.ToString("MM/dd/yyyy HH:mm:ss").TrimEnd() +
                    (String.IsNullOrEmpty(novInfo.Resp1LastName) ? "" : Truncate(novInfo.Resp1LastName.TrimEnd(), 30)) + 
                    (String.IsNullOrEmpty(novInfo.Resp1FirstName) ? "" : Truncate(novInfo.Resp1FirstName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(novInfo.Resp1MiddleInitial) ? "" : Truncate(novInfo.Resp1MiddleInitial.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.Resp1Sex) ? "" : Truncate(novInfo.Resp1Sex.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.PropertyBBL) ? "" : Truncate(novInfo.PropertyBBL.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.Resp1DistrictId) ? "" : Truncate(novInfo.Resp1DistrictId.TrimEnd(), 3)) +
                    (String.IsNullOrEmpty(novInfo.Resp1SectionId) ? "" : Truncate(novInfo.Resp1SectionId.TrimEnd(), 2)) +
                    (String.IsNullOrEmpty(novInfo.Resp1StreetId.ToString()) ? "" : novInfo.Resp1StreetId.ToString().TrimEnd()) + //Int
                    (String.IsNullOrEmpty(novInfo.Resp1Address) ? "" : Truncate(novInfo.Resp1Address.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(novInfo.Resp1Address1) ? "" : Truncate(novInfo.Resp1Address1.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(novInfo.Resp1City) ? "" : Truncate(novInfo.Resp1City.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(novInfo.Resp1State) ? "" : Truncate(novInfo.Resp1State.TrimEnd(), 2)) +
                    (String.IsNullOrEmpty(novInfo.Resp1Zip) ? "" : Truncate(novInfo.Resp1Zip.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.LicenseNumber) ? "" : Truncate(novInfo.LicenseNumber.TrimEnd(), 45)) +
                    (String.IsNullOrEmpty(novInfo.LicenseAgency) ? "" : Truncate(novInfo.LicenseAgency.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.LicenseType) ? "" : Truncate(novInfo.LicenseType.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.PlaceLastName) ? "" : Truncate(novInfo.PlaceLastName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(novInfo.PlaceFirstName) ? "" : Truncate(novInfo.PlaceFirstName.TrimEnd(), 20)) +
                    (String.IsNullOrEmpty(novInfo.PlaceMiddleInitial) ? "" : Truncate(novInfo.PlaceMiddleInitial.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.PlaceBBL) ? "" : Truncate(novInfo.PlaceBBL.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.PlaceDistrictId) ? "" : Truncate(novInfo.PlaceDistrictId.TrimEnd(), 3)) +
                    (String.IsNullOrEmpty(novInfo.PlaceSectionId) ? "" : Truncate(novInfo.PlaceSectionId.TrimEnd(), 2)) +
                    (String.IsNullOrEmpty(novInfo.PlaceStreetId.ToString()) ? "" : novInfo.PlaceStreetId.ToString().TrimEnd()) + //Int
                    (String.IsNullOrEmpty(novInfo.PlaceAddress1) ? "" : Truncate(novInfo.PlaceAddress1.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(novInfo.PlaceAddress2) ? "" : Truncate(novInfo.PlaceAddress2.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(novInfo.PlaceAddressDescriptor) ? "" : Truncate(novInfo.PlaceAddressDescriptor.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.PlaceSideOfStreet) ? "" : Truncate(novInfo.PlaceSideOfStreet.TrimEnd(), 4)) +
                    (String.IsNullOrEmpty(novInfo.PlaceCross1StreetId.ToString()) ? "" : novInfo.PlaceCross1StreetId.ToString().TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.PlaceCross2StreetId.ToString()) ? "" : novInfo.PlaceCross2StreetId.ToString().TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.MailableAmount.ToString()) ? "" : novInfo.MailableAmount.ToString("F2").PadLeft(9, '0').TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.MaximumAmount.ToString()) ? "" : novInfo.MaximumAmount.ToString("F2").PadLeft(9, '0').TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.HearingTimestamp.ToString()) ? "" : novInfo.HearingTimestamp.ToString("MM/dd/yyyy HH:mm:ss").TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.IsAppearRequired) ? "" : Truncate(novInfo.IsAppearRequired.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.BuildingType) ? "" : Truncate(novInfo.BuildingType.TrimEnd(), 2)) +
                    (String.IsNullOrEmpty(novInfo.IsMultipleOffences) ? "" : Truncate(novInfo.IsMultipleOffences.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.ViolationCode) ? "" : Truncate(novInfo.ViolationCode.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.HHTIdentifier) ? "" : Truncate(novInfo.HHTIdentifier.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(novInfo.ViolationScript) ? "" : Truncate(novInfo.ViolationScript.TrimEnd(), 512)) +
                    (String.IsNullOrEmpty(novInfo.UserId) ? "" : Truncate(novInfo.UserId.TrimEnd(),16)) +
                    (String.IsNullOrEmpty(novInfo.PlaceBoroCode) ? "" : Truncate(novInfo.PlaceBoroCode.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.PlaceHouseNo) ? "" : Truncate(novInfo.PlaceHouseNo.TrimEnd(), 16)) +
                    (String.IsNullOrEmpty(novInfo.ViolationGroupId.ToString()) ? "" : novInfo.ViolationGroupId.ToString().TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.ViolationTypeId) ? "" : Truncate(novInfo.ViolationTypeId.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.MDRNumber.ToString()) ? "" : novInfo.MDRNumber.ToString().TrimEnd()) +
                    ((novInfo.LicenseExpDate == dt ? "" : novInfo.LicenseExpDate.ToString("MM/dd/yyyy HH:mm:ss").TrimEnd()))+
                    //(String.IsNullOrEmpty(novInfo.LicenseExpDate.ToString()) ? "" : novInfo.LicenseExpDate.ToString("MM/dd/yyyy HH:mm:ss").TrimEnd()) +
                    (String.IsNullOrEmpty(novInfo.BusinessName) ? "" : Truncate(novInfo.BusinessName.TrimEnd(), 70)) +
                    (String.IsNullOrEmpty(novInfo.CheckSum) ? "" : Truncate(novInfo.CheckSum.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(novInfo.FreeAddrees) ? "" : Truncate(novInfo.FreeAddrees.TrimEnd(), 80)) +
                    (String.IsNullOrEmpty(novInfo.DeviceId) ? "" : Truncate(novInfo.DeviceId.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.PrintViolationCode) ? "" : Truncate(novInfo.PrintViolationCode.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(novInfo.CodeLawDescription) ? "" : Truncate(novInfo.CodeLawDescription.TrimEnd(), 100)) +
                    (String.IsNullOrEmpty(novInfo.OfficerName) ? "" : Truncate(novInfo.OfficerName.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(novInfo.LawSection) ? "" : Truncate(novInfo.LawSection.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(novInfo.AbbrevName) ? "" : Truncate(novInfo.AbbrevName.TrimEnd(), 30)) +
                    (String.IsNullOrEmpty(novInfo.AgencyId) ? "" : Truncate(novInfo.AgencyId.TrimEnd(),3)) +
                    (String.IsNullOrEmpty(novInfo.Title) ? "" : Truncate(novInfo.Title.TrimEnd(), 30));                    
                    
#endif
                string novHash = SimpleHash.ComputeHash(plainText, salt);
                string digitalSignature = GenerateDigitalSignature(novHash, dbDevice.SerializedPrivateKey);
                novMaster.NovInformation.DigitalSignature = digitalSignature;
                novMaster.NovCertificate = new NovCertificate()
                {
                    NovNumber = novMaster.NovInformation.NovNumber,
                    Certificate = dbDevice.Certificate
                };
            }
            catch(Exception ex)
            {

            }

  
        }
        private  string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }
        private string GenerateDigitalSignature(string novHash, string serializedPrivate)
        {
            byte[] keyInBytes = Convert.FromBase64String(serializedPrivate);
            AsymmetricKeyParameter key = PrivateKeyFactory.CreateKey(keyInBytes);
            byte[] data = Encoding.UTF8.GetBytes(novHash);
            ISigner bSigner = SignerUtilities.GetSigner("SHA-256withECDSA");
            bSigner.Init(true, key);
            bSigner.BlockUpdate(data, 0, data.Length);
            byte[] signature = bSigner.GenerateSignature();

            return Convert.ToBase64String(signature);
        }
    }
}