﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IViolationDetailsService
    {
        Task<string> GetCodeScript(string violationCode, string hhtIdentifier, int violationGroupId);
        Task<Dictionary<string, string>> GetNovDataFieldMappings();
        Task<ViolationScriptVariables> GetViolationScriptVariables(string description);
        Task<List<ViolationScriptVariableData>> GetViolationScriptVariableData(string fieldName);
    }
}