﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class PersonCommercialIDService : BaseService, IPersonCommercialIDService
    {
        public async Task<List<IdMatrix>> GetIssuedBy()
        {
            string query = "select distinct IssuedBy from DBIdMatrix where IssuedBy is not null and IssuedBy <> '' order by IssuedBy";
            //string query = "select ' ' as IssuedBy Union select distinct IssuedBy from DBIdMatrix where IssuedBy is not null order by IssuedBy";
            var repo = DependencyResolver.Get<IRepository<DBIdMatrix>>();
            var dbIdMatrices = await repo.QueryAsync(query);

            var matrices = dbIdMatrices.Select(_ => new IdMatrix
            {
                IssuedBy = _.IssuedBy
            }).ToList();


            return matrices;
        }


        public async Task<List<IdMatrix>> GetIssuedByStates()
        {
            string query = "select * from DBLookup where TableName is 'STATE' ";
            var repo = DependencyResolver.Get<IRepository<DBLookup>>();
            var dbLookup = await repo.QueryAsync(query);

            var matrices = dbLookup.Select(_ => new IdMatrix
            {
                IssuedBy = _.Code,
                Description = _.Description
            }).ToList();

            return matrices;
        }

        public async Task<List<IdMatrix>> GetIDTypes()
        {
            string query = "SELECT '' AS IdType, '' AS IdDesc, 'Y' AS IsActionOff, 'Y' AS IsCommOff, 'Y' AS IsMultiOff, 'Y' AS IsPersonalSvc, " +
                " 'N' AS AskIdNo,  'N' AS AskExpDate, 'N' AS AskIssuedBy, 'N' AS IssuedBy UNION SELECT IdType, IdDesc, IsActionOff, IsCommOff, " + 
                "IsMultiOff, IsPersonalSvc, AskIdNo, AskExpDate, AskIssuedBy, IssuedBy FROM DBIdMatrix ORDER BY IdDesc";

            var repo = DependencyResolver.Get<IRepository<DBIdMatrix>>();
            var dbIdMatrices = await repo.QueryAsync(query);

            /*
            foreach (var id in dbIdMatrices)
            {
                if (id.IdType == "--")
                {
                    id.IdType = "No Valid ID";
                    id.IdDesc = "No Valid ID";
                }
            }
            */

            var matrices = dbIdMatrices.Select(dbIdType => new IdMatrix
            {
                IdType = dbIdType.IdType,
                AskExpDate = dbIdType.AskExpDate,
                AskIdNo =  dbIdType.AskIdNo,
                AskIssuedBy = dbIdType.AskIssuedBy,
                IdDesc = dbIdType.IdDesc,
                IsActionOff = dbIdType.IsActionOff,
                IsCommOff = dbIdType.IsCommOff,
                IsMultiOff = dbIdType.IsMultiOff,
                IsPersonalSvc = dbIdType.IsPersonalSvc,
                IssuedBy = dbIdType.IssuedBy
            }).Where(_ => _.IdType != "").ToList();

            matrices.Add(
                    new IdMatrix {
                    IdType = "NotValid",
                    AskExpDate = "N",
                    AskIdNo = "Y",
                    AskIssuedBy = "Y",
                    IdDesc = "No Valid ID",
                    IsActionOff = "Y",
                    IsCommOff = "Y",
                    IsMultiOff = "Y",
                    IsPersonalSvc = "Y",
                    IssuedBy = ""
                    }
                );

            return matrices;
        }
    }
}
