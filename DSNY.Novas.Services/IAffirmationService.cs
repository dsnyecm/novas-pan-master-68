﻿using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IAffirmationService
    {
        Task<CourtLocations> GetCourtLocation(string violationCode, string boroCode);
        Task SignNov(NovMaster novMaster);
    }
}