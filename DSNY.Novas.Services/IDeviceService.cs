﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services
{
    public interface IDeviceService
    {
        Task<string> GetDeviceIdForHardware(string hardwareId);

        Task<string> GetDeviceIdByIMEI(string deviceIMEI);
        Task<string> GetDeviceIdByIMEIAndBoro(string deviceIMEI, string boroLocation);

        Task<List<DBDevices>> GetAllDevices();

        Task<List<DBDevices>> GetAllPrinters();
    }
}
