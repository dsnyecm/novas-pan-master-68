﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;

using DSNY.Novas.Models;
using DSNY.Novas.Entities;
using DSNY.Novas.Common;
using DSNY.Novas.Data;

namespace DSNY.Novas.Services
{
    public class AffidavitService : BaseService, IAffidavitService
    {
        public async Task SaveAffidavit(NovMaster novMaster)
        {
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBAffidavitOfService>>();
                var dbAffidavit = new DBAffidavitOfService();
                await SignAffadavit(novMaster);
                MapAffidavit(dbAffidavit, novMaster);
                await repo.InsertAsync(dbAffidavit);
            }
            catch (Exception ex)
            {

            }
        }

        public async Task SaveAffidavitTran(NovMaster novMaster)
        {
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBAffidavitOfServiceTran>>();
                var dbAffidavitTran = new DBAffidavitOfServiceTran();
                await SignAffadavit(novMaster);
                MapAffidavitTran(dbAffidavitTran, novMaster);
                await repo.InsertAsync(dbAffidavitTran);

            }
            catch (Exception ex)
            {

            }
        }

        public async Task<List<AffidavitOfServiceTran>> GetAffidavitTranList(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBAffidavitOfServiceTran>>();
            var dbAffidavitOfServiceTran = await repo.GetAsync();
            var affidavitOfServiceTran = dbAffidavitOfServiceTran.Select(_ => new AffidavitOfServiceTran
            {
                NovNumber = _.NovNumber,
                PersonallyServeFlag = _.PersonallyServeFlag,
                ExpDate = _.ExpDate,
                AlternativeService1 = _.AlternativeService1,
                Comments = _.Comments,
                SignDate = _.SignDate,
                UserId = _.UserId,
                PublicKeyId = _.PublicKeyId,
                DigitalSignature = _.DigitalSignature,
                ServedCounty = _.ServedCounty,
                AlternativeService2 = _.AlternativeService2,
                CheckSum = _.CheckSum,
                DeviceId = _.DeviceId,
                OfficerName = _.OfficerName,
                AbbrevName = _.AbbrevName,
                AgencyId = _.AgencyId,
                Title = _.Title,
                LoginTimestamp = _.LoginTimestamp,
                IssuedBy = _.IssuedNy

            }).Where(_ => _.NovNumber.Equals(novNumber)).ToList();
            return affidavitOfServiceTran;
        }

        public async Task<AffidavitOfService> GetAffidavit(long novNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBAffidavitOfService>>();
            var dbAffidavitOfService = await repo.GetAsync();
            var affidavitOfService = dbAffidavitOfService.Select(_ => new AffidavitOfService
            {
                NovNumber = _.NovNumber,
                PersonallyServeFlag = _.PersonallyServeFlag,
                ExpDate = _.ExpDate,
                AlternativeService1 = _.AlternativeService1,
                Comments = _.Comments,
                SignDate = _.SignDate,
                UserId = _.UserId,
                PublicKeyId = _.PublicKeyId,
                DigitalSignature = _.DigitalSignature,
                ServedCounty = _.ServedCounty,
                AlternativeService2 = _.AlternativeService2,
                CheckSum = _.CheckSum,
                DeviceId = _.DeviceId,
                OfficerName = _.OfficerName,
                AbbrevName = _.AbbrevName,
                AgencyId = _.AgencyId,
                Title = _.Title,
                LoginTimestamp = _.LoginTimestamp,
                ServedLocation=_.ServedLocation,
                ServedAddress = _.ServedAddress,
                ServedBoroCode =  _.ServedBoroCode,
                ServedCity = _.ServedCity,
                ServedFName = _.ServedFName,
                ServedLName = _.ServedLName,
                ServedMInit = _.ServedMInit,
                ServedHouseNo = _.ServedHouseNo,
                ServedSex = _.ServedSex,
                ServedState = _.ServedState,
                ServedTitle = _.ServedTitle,
                ServedZip = _.ServedZip,
                LicenseNumber = _.LicenseNumber,
                LicenseTypeDesc = _.LicenseTypeDesc,
                Age = _.Age,
                HairColor = _.HairColor,
                SkinColor = _.SkinColor,
                Sex = _.Sex,
                PremiseFName = _.PremiseFName,
                PremiseMInit = _.PremiseMInit,
                PremiseLName = _.PremiseLName,
                TypeOfLicense = _.TypeOfLicense,
                Weight = _.Weight,
                Height = _.Height,
                ServiceTo = _.ServiceTo,
                ServedTitleOther = _.ServedTitleOther,
                IssuedBy = _.IssuedNy

            }).Where(_ => _.NovNumber.Equals(novNumber)).SingleOrDefault();
            return affidavitOfService;
        }

        private void MapAffidavit(DBAffidavitOfService dbAffidavit, NovMaster novMaster)
        {
            dbAffidavit.NovNumber = novMaster.AffidavitOfService.NovNumber;
            dbAffidavit.PersonallyServeFlag = novMaster.AffidavitOfService.PersonallyServeFlag;
            dbAffidavit.Sex = NullToString(novMaster.AffidavitOfService.Sex);
            dbAffidavit.Age = novMaster.AffidavitOfService.Age;
            dbAffidavit.HairColor = novMaster.AffidavitOfService.HairColor;
            dbAffidavit.SkinColor = novMaster.AffidavitOfService.SkinColor;
            dbAffidavit.Height = novMaster.AffidavitOfService.Height;
            dbAffidavit.Weight = novMaster.AffidavitOfService.Weight;
            dbAffidavit.OtherIdentifying = NullToString(novMaster.AffidavitOfService.OtherIdentifying);
            dbAffidavit.LicenseNumber = novMaster.AffidavitOfService.LicenseNumber;
            dbAffidavit.ExpDate = novMaster.AffidavitOfService.ExpDate;
            dbAffidavit.TypeOfLicense = novMaster.AffidavitOfService.TypeOfLicense;
            dbAffidavit.IssuedNy = novMaster.AffidavitOfService.IssuedBy;
            dbAffidavit.AlternativeService1 = NullToString(novMaster.AffidavitOfService.AlternativeService1);
            dbAffidavit.Comments = novMaster.AffidavitOfService.Comments;
            dbAffidavit.SignDate = novMaster.AffidavitOfService.SignDate;
            dbAffidavit.UserId = novMaster.AffidavitOfService.UserId;
            dbAffidavit.PublicKeyId = novMaster.AffidavitOfService.PublicKeyId;
            dbAffidavit.DigitalSignature = novMaster.AffidavitOfService.DigitalSignature;
            dbAffidavit.ServedCounty = novMaster.AffidavitOfService.ServedCounty;
            dbAffidavit.ServiceTo = novMaster.AffidavitOfService.ServiceTo;
            dbAffidavit.ServedTitle = NullToString(novMaster.AffidavitOfService.ServedTitle);
            dbAffidavit.ServedTitleOther = NullToString(novMaster.AffidavitOfService.ServedTitleOther);
            dbAffidavit.ServedLName = novMaster.AffidavitOfService.ServedLName;
            dbAffidavit.ServedFName = novMaster.AffidavitOfService.ServedFName;
            dbAffidavit.ServedMInit = NullToString(novMaster.AffidavitOfService.ServedMInit);
            dbAffidavit.ServedSex = novMaster.AffidavitOfService.ServedSex;
            dbAffidavit.ServedAddress = novMaster.AffidavitOfService.ServedAddress;
            dbAffidavit.ServedCity = novMaster.AffidavitOfService.ServedCity;
            dbAffidavit.ServedState = novMaster.AffidavitOfService.ServedState;
            dbAffidavit.ServedZip = novMaster.AffidavitOfService.ServedZip;
            dbAffidavit.AlternativeService2 = NullToString(novMaster.AffidavitOfService.AlternativeService2);
            dbAffidavit.PremiseLName = NullToString(novMaster.AffidavitOfService.PremiseLName);
            dbAffidavit.PremiseFName = NullToString(novMaster.AffidavitOfService.PremiseFName);
            dbAffidavit.PremiseMInit = NullToString(novMaster.AffidavitOfService.PremiseMInit);
            dbAffidavit.ServedLocation = novMaster.AffidavitOfService.ServedLocation;
            dbAffidavit.CheckSum = novMaster.AffidavitOfService.CheckSum;
            dbAffidavit.DeviceId = novMaster.AffidavitOfService.DeviceId;
            dbAffidavit.LicenseTypeDesc = novMaster.AffidavitOfService.LicenseTypeDesc;
            dbAffidavit.OfficerName = novMaster.AffidavitOfService.OfficerName;
            dbAffidavit.AbbrevName = novMaster.AffidavitOfService.AbbrevName;
            dbAffidavit.AgencyId = novMaster.AffidavitOfService.AgencyId;
            dbAffidavit.Title = novMaster.AffidavitOfService.Title;
            dbAffidavit.LoginTimestamp = novMaster.AffidavitOfService.LoginTimestamp;
            dbAffidavit.ServedHouseNo = novMaster.AffidavitOfService.ServedHouseNo;
            dbAffidavit.ServedBoroCode = novMaster.AffidavitOfService.ServedBoroCode;
        }

        private void MapAffidavitTran(DBAffidavitOfServiceTran dbAffidavitTran, NovMaster novMaster)
        {
            dbAffidavitTran.NovNumber = novMaster.AffidavitOfService.NovNumber;
            dbAffidavitTran.PersonallyServeFlag = novMaster.AffidavitOfService.PersonallyServeFlag;
            dbAffidavitTran.Sex = novMaster.AffidavitOfService.Sex;
            dbAffidavitTran.Age = novMaster.AffidavitOfService.Age;
            dbAffidavitTran.HairColor = novMaster.AffidavitOfService.HairColor;
            dbAffidavitTran.SkinColor = novMaster.AffidavitOfService.SkinColor;
            dbAffidavitTran.Height = novMaster.AffidavitOfService.Height;
            dbAffidavitTran.Weight = novMaster.AffidavitOfService.Weight;
            dbAffidavitTran.OtherIdentifying = NullToString(novMaster.AffidavitOfService.OtherIdentifying);
            dbAffidavitTran.LicenseNumber = novMaster.AffidavitOfService.LicenseNumber;
            dbAffidavitTran.ExpDate = novMaster.AffidavitOfService.ExpDate;
            dbAffidavitTran.TypeOfLicense = novMaster.AffidavitOfService.TypeOfLicense;
            dbAffidavitTran.IssuedNy = novMaster.AffidavitOfService.IssuedBy;
            dbAffidavitTran.AlternativeService1 = NullToString(novMaster.AffidavitOfService.AlternativeService1);
            dbAffidavitTran.Comments = novMaster.AffidavitOfService.Comments;
            dbAffidavitTran.SignDate = novMaster.AffidavitOfService.SignDate;
            dbAffidavitTran.UserId = novMaster.AffidavitOfService.UserId;
            dbAffidavitTran.PublicKeyId = novMaster.AffidavitOfService.PublicKeyId;
            dbAffidavitTran.DigitalSignature = novMaster.AffidavitOfService.DigitalSignature;
            dbAffidavitTran.ServedCounty = novMaster.AffidavitOfService.ServedCounty;
            dbAffidavitTran.ServiceTo = novMaster.AffidavitOfService.ServiceTo;
            dbAffidavitTran.ServedTitle = NullToString(novMaster.AffidavitOfService.ServedTitle);
            dbAffidavitTran.ServedTitleOther = NullToString(novMaster.AffidavitOfService.ServedTitleOther);
            dbAffidavitTran.ServedLName = novMaster.AffidavitOfService.ServedLName;
            dbAffidavitTran.ServedFName = novMaster.AffidavitOfService.ServedFName;
            dbAffidavitTran.ServedMInit = NullToString(novMaster.AffidavitOfService.ServedMInit);
            dbAffidavitTran.ServedSex = novMaster.AffidavitOfService.ServedSex;
            dbAffidavitTran.ServedAddress = novMaster.AffidavitOfService.ServedAddress;
            dbAffidavitTran.ServedCity = novMaster.AffidavitOfService.ServedCity;
            dbAffidavitTran.ServedState = novMaster.AffidavitOfService.ServedState;
            dbAffidavitTran.ServedZip = novMaster.AffidavitOfService.ServedZip;
            dbAffidavitTran.AlternativeService2 = NullToString(novMaster.AffidavitOfService.AlternativeService2);
            dbAffidavitTran.PremiseLName = NullToString(novMaster.AffidavitOfService.PremiseLName);
            dbAffidavitTran.PremiseFName = NullToString(novMaster.AffidavitOfService.PremiseFName);
            dbAffidavitTran.PremiseMInit = NullToString(novMaster.AffidavitOfService.PremiseMInit);
            dbAffidavitTran.ServedLocation = novMaster.AffidavitOfService.ServedLocation;
            dbAffidavitTran.CheckSum = novMaster.AffidavitOfService.CheckSum;
            dbAffidavitTran.DeviceId = novMaster.AffidavitOfService.DeviceId;
            dbAffidavitTran.LicenseTypeDesc = novMaster.AffidavitOfService.LicenseTypeDesc;
            dbAffidavitTran.OfficerName = novMaster.AffidavitOfService.OfficerName;
            dbAffidavitTran.AbbrevName = novMaster.AffidavitOfService.AbbrevName;
            dbAffidavitTran.AgencyId = novMaster.AffidavitOfService.AgencyId;
            dbAffidavitTran.Title = novMaster.AffidavitOfService.Title;
            dbAffidavitTran.LoginTimestamp = novMaster.AffidavitOfService.LoginTimestamp;
            dbAffidavitTran.ServedHouseNo = novMaster.AffidavitOfService.ServedHouseNo;
            dbAffidavitTran.ServedBoroCode = novMaster.AffidavitOfService.ServedBoroCode;
        }

        public async Task SignAffadavit(NovMaster novMaster)
        {
            try
            {
                IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();

                DBDevices dbDevice = await deviceRepo.GetAsync(_ => _.DeviceId == novMaster.NovInformation.DeviceId);

                byte[] salt = Encoding.UTF8.GetBytes(dbDevice.Salt);
                var plainText = string.Empty;
#if TEST
            plainText = "I love Boston MA.";
#else
                var aos = novMaster.AffidavitOfService;

                //****OLD code***
                //plainText = aos.NovNumber + aos.PersonallyServeFlag + aos.Sex + aos.Age + aos.HairColor + aos.SkinColor + aos.Height +
                //    aos.Weight + aos.OtherIdentifying + aos.LicenseNumber + aos.ExpDate.ToString("MM/dd/yyyy hh:mm:ss") + aos.TypeOfLicense + aos.IssuedBy +
                //    aos.AlternativeService1 + aos.Comments + aos.UserId + aos.PublicKeyId + aos.ServedCounty +
                //    aos.ServiceTo + aos.ServedTitle + aos.ServedTitleOther + aos.ServedLName + aos.ServedFName +
                //    aos.ServedMInit + aos.ServedSex + aos.ServedAddress + aos.ServedCity + aos.ServedState + aos.ServedZip +
                //    aos.AlternativeService2 + aos.PremiseLName + aos.PremiseFName + aos.PremiseMInit +
                //    aos.ServedLocation + aos.CheckSum + aos.DeviceId +
                //    aos.OfficerName + aos.AbbrevName + aos.AgencyId + aos.Title;

                //****NEW code***
                plainText = aos.NovNumber.ToString().TrimEnd() +
                    (String.IsNullOrEmpty(aos.PersonallyServeFlag) ? "" : Truncate(aos.PersonallyServeFlag.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.Sex) ? "" : Truncate(aos.Sex.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.Age) ? "" : Truncate(aos.Age.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(aos.HairColor) ? "" : Truncate(aos.HairColor.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.SkinColor) ? "" : Truncate(aos.SkinColor.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.Height) ? "" : Truncate(aos.Height.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(aos.Weight) ? "" : Truncate(aos.Weight.TrimEnd(), 15)) +
                    (String.IsNullOrEmpty(aos.OtherIdentifying) ? "" : Truncate(aos.OtherIdentifying.TrimEnd(), 30)) +
                    (String.IsNullOrEmpty(aos.LicenseNumber) ? "" : aos.LicenseNumber.TrimEnd()) +
                    (String.IsNullOrEmpty(aos.ExpDate.ToString()) ? "" : aos.ExpDate.ToString("MM/dd/yyyy HH:mm:ss").TrimEnd()) +
                    (String.IsNullOrEmpty(aos.TypeOfLicense) ? "" : Truncate(aos.TypeOfLicense.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(aos.IssuedBy) ? "" : Truncate(aos.IssuedBy.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(aos.AlternativeService1) ? "" : Truncate(aos.AlternativeService1.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.Comments) ? "" : Truncate(aos.Comments.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(aos.UserId) ? "" : Truncate(aos.UserId.TrimEnd(), 16)) +
                    (String.IsNullOrEmpty(aos.PublicKeyId.ToString()) ? "" : Truncate(aos.PublicKeyId.ToString().TrimEnd(), 30)) +
                    (String.IsNullOrEmpty(aos.ServedCounty) ? "" : Truncate(aos.ServedCounty.TrimEnd(), 20)) +
                    (String.IsNullOrEmpty(aos.ServiceTo) ? "" : Truncate(aos.ServiceTo.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.ServedTitle) ? "" : Truncate(aos.ServedTitle.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.ServedTitleOther) ? "" : Truncate(aos.ServedTitleOther.TrimEnd(), 15)) +
                    (String.IsNullOrEmpty(aos.ServedLName) ? "" : Truncate(aos.ServedLName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(aos.ServedFName) ? "" : Truncate(aos.ServedFName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(aos.ServedMInit) ? "" : Truncate(aos.ServedMInit.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.ServedSex) ? "" : Truncate(aos.ServedSex.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.ServedAddress) ? "" : Truncate(aos.ServedAddress.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(aos.ServedCity) ? "" : Truncate(aos.ServedCity.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(aos.ServedState) ? "" : Truncate(aos.ServedState.TrimEnd(), 2)) +
                    (String.IsNullOrEmpty(aos.ServedZip) ? "" : Truncate(aos.ServedZip.TrimEnd(), 5)) +
                    (String.IsNullOrEmpty(aos.AlternativeService2) ? "" : Truncate(aos.AlternativeService2.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.PremiseLName) ? "" : Truncate(aos.PremiseLName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(aos.PremiseFName) ? "" : Truncate(aos.PremiseFName.TrimEnd(), 25)) +
                    (String.IsNullOrEmpty(aos.PremiseMInit) ? "" : Truncate(aos.PremiseMInit.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.ServedLocation) ? "" : Truncate(aos.ServedLocation.TrimEnd(), 20)) +
                    (String.IsNullOrEmpty(aos.CheckSum) ? "" : Truncate(aos.CheckSum.TrimEnd(), 1)) +
                    (String.IsNullOrEmpty(aos.DeviceId) ? "" : Truncate(aos.DeviceId.TrimEnd(), 10)) +
                    (String.IsNullOrEmpty(aos.OfficerName) ? "" : Truncate(aos.OfficerName.TrimEnd(), 50)) +
                    (String.IsNullOrEmpty(aos.AbbrevName) ? "" : Truncate(aos.AbbrevName.TrimEnd(), 30)) +
                    (String.IsNullOrEmpty(aos.AgencyId) ? "" : Truncate(aos.AgencyId.TrimEnd(), 3)) +
                    (String.IsNullOrEmpty(aos.Title) ? "" : Truncate(aos.Title.TrimEnd(), 30));                
#endif
                string affadavitHash = SimpleHash.ComputeHash(plainText, salt);
                string digitalSignature = GenerateDigitalSignature(affadavitHash, dbDevice.SerializedPrivateKey);
                novMaster.AffidavitOfService.DigitalSignature = digitalSignature;
                novMaster.NovCertificate = new NovCertificate()
                {
                    NovNumber = novMaster.NovInformation.NovNumber,
                    Certificate = dbDevice.Certificate
                };
            }
            catch(Exception ex)
            {

            }
        }
        private string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }
        private string GenerateDigitalSignature(string novHash, string serializedPrivateKey)
        {
            byte[] keyInBytes = Convert.FromBase64String(serializedPrivateKey);
            AsymmetricKeyParameter key = PrivateKeyFactory.CreateKey(keyInBytes);
            byte[] data = Encoding.UTF8.GetBytes(novHash);
            ISigner bSigner = SignerUtilities.GetSigner("SHA-256withECDSA");
            bSigner.Init(true, key);
            bSigner.BlockUpdate(data, 0, data.Length);
            byte[] signature = bSigner.GenerateSignature();

            return Convert.ToBase64String(signature);
        }

        private string NullToString(object Value)
        {

            return Value == null ? "" : Value.ToString();

        }
    }
}