﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DSNY.Novas.Services
{
    public class HttpClientService
    {
        static readonly HttpClient _client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.GZip });
        private static string _basePath;

        public HttpClientService(Uri serviceBaseUri = null)
        {
            if (_client.BaseAddress == null) // Only initialize the first time we are setting up the http client
            {
                _client.BaseAddress = serviceBaseUri;
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("UTF-8"));
            }

            _basePath = "api/sync/";
        }

        public async Task<string> GetServerTimeAsync (string deviceId)
        {
            string uri = $"{_basePath}time?deviceId={deviceId}";
            int retryCount = 0;
            HttpResponseMessage response = null;

            while (response == null && retryCount < 3)
            {
                try
                {
                    response = await _client.GetAsync(uri);

                    response.EnsureSuccessStatusCode();
                }
                catch (Exception e)
                {
                    if (retryCount == 2)
                    {
                        throw e;
                    }
                }

                retryCount++;
            }

            string data = await response.Content.ReadAsStringAsync();
            return data;
        }

        public async Task<string> GetTablesToSync(string lastSyncTime)
        {
            string uri = $"{_basePath}config?lastsync={lastSyncTime}";

            int retryCount = 0;
            HttpResponseMessage response = null;

            while (response == null && retryCount < 3)
            {
                try
                {
                    response = await _client.GetAsync(uri);
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception e)
                {
                    if (retryCount == 2)
                    {
                        throw e;
                    }
                }

                retryCount++;
            }

            string data = await response.Content.ReadAsStringAsync();
            return data;
        }

        public async Task<string> GetAsync(string key, int? start, int? end)
        {
            string uri = $"{_basePath}borosite?table={key}&start={start}&end={end}";

            int retryCount = 0;
            HttpResponseMessage response = null;

            while (response == null && retryCount < 3)
            {
                try
                {
                    response = await _client.GetAsync(uri);
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception e)
                {
                    if (retryCount == 2)
                    {
                        throw e;
                    }
                }

                retryCount++;
            }

            string data = await response.Content.ReadAsStringAsync();
            return data;

        }

        public async Task<bool> PostAsync(string endpoint, JObject jsonData)
        {
            bool isSuccess = false;
            try
            {
                string uri = "";
                if (endpoint == "users" || endpoint == "logs")
                    uri = $"{"api/"}{endpoint}";
                else
                    uri = $"{_basePath}{endpoint}";
                StringContent content = new StringContent(jsonData.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await _client.PostAsync(uri, content);

                isSuccess = response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                string error = "I am eating remote exception: " + ex.Message;
            }

            return isSuccess;
        }

        public async Task<string> PostAsyncForDeviceSetup(JObject jsonData)
        {
            bool isSuccess = false;
            string responseMessage = "";
            try
            {
                string basePathDeviceSetup = "api/setupdevice/";
                string endpointDeviceSetup = "saveToCentral";
                string uri = $"{basePathDeviceSetup}{endpointDeviceSetup}";
                StringContent content = new StringContent(jsonData.ToString(), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await _client.PostAsync(uri, content);

                isSuccess = response.IsSuccessStatusCode;

                if (isSuccess)
                    responseMessage = "SUCCESS!!";
                else
                    responseMessage = "FAIL!! " + response.ReasonPhrase;
            }
            catch (Exception ex)
            {
                string error = "I am eating remote exception: " + ex.Message;
            }

            return responseMessage;
        }
    }
}