﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IPersonnelDataService
    {
        Task<List<HolidayMaster>> GetHolidaysAsync();
        
        Task<List<ReportingLevel>> GetReportingLevelListAsync();

        Task<List<Agency>> GetAgencyListAsync();

        Task<List<HearingDateTime>> GetHearingTimesAsync(Agency agency);
        Task<List<HearingDateTime>> GetHearingTimesAsync(int agencyId);

        Task<List<HearingDate>> GetHearingDatesAsync(int boroId);
        Task<List<DateTime>> GetHearingDatesBetweenAsync(int boroId, DateTime hearingFromDate, DateTime hearingToDate);
        //Task<List<DateTime>> GetHearingDatesBetweenAsync(int boroId, DateTime hearingFromDate, DateTime hearingToDate, bool isDofDate);
        Task<List<DateTime>> GetHolidaysBetweenAsync(int boroId, DateTime hearingFromDate, DateTime hearingToDate);

        Task<List<DBBoroSiteMaster>> GetBoroSites();
        Task<int> GetBoroIdForCurrentDevice();

        Task<int> GetBoroIdForReportLevel(string reportingLevel);

        

    }
}