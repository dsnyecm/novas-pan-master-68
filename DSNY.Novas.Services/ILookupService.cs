﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface ILookupService
    {
        Task<List<LookupTable>> GetLookupTable(string lookupTable);
        Task<List<LookupTable>> GetLookupCode(string lookupCode);

        
    }
}
