﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface ILocalDeviceSettingsService
    {
        Task<LocalDeviceSettings> GetLocalDeviceSetting(string key);
        Task<bool> InsertOrUpdateLocalDeviceSetting(string key, string value);
    }
}
