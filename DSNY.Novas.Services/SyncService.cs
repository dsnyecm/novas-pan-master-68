﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DSNY.Novas.Common.Interfaces;
using System.Text;
using System.Collections;

namespace DSNY.Novas.Services
{
    public static class SyncServiceConstants
    {
        public static string TableName = "Table Name";
        public static string CurrentRecord = "Current Record";
        public static string CurrentRangeEnd = "Current Range End";
        public static string RecordCount = "Record Count";
        public static string UserTableName = "\r\n" + "NovasUser";
        public static string FullSyncTableName = "\r\n" + "BS_DutyHeader" + "\r\n" + "BS_DeviceTicketRanges" + "\r\n" + "BS_VehicleRadioInfo" + "\r\n"
            + "BS_HHTNOVInformation" + "\r\n" + "BS_HHNOVData" + "\r\n" + "BS_NOVPublicKey" + "\r\n" + "BS_AffidavitOfService" + "\r\n"
            + "BS_AffidavitOfServiceTran" + "\r\n" + "BS_CancelHHTNOVInformation" + "\r\n" + "BS_CancelHHNOVData" + "\r\n";
        public static string SYNC_TICKET_SUCCESS = "All violations have been successfully synced\r\n";
        public static string SYNC_TICKET_SUCCESS_LOG_FAIL = "All violations have been successfully synced. \r\nApplication logs failed to sync." + "\r\n";
        public static string SYNC_TICKET_FAIL = "The following violations failed to sync: {0}" + "\r\n";
        public static string SYNC_BORO_TO_DEVICE = "Successfully synced data from Boro to Handheld\r\n";

    }

    public class SyncService : BaseService, ISyncService
    {
        private readonly List<string> _tables;
        private readonly HttpClientService _httpClientService;
        private readonly ViolationDetailsService _violationDetailsService;
        private readonly LocalDeviceSettingsService _deviceSettingsService;
        private readonly INovService _novService;


        public ICommand SyncStatusUpdatedCommand { get; set; }

        public SyncService(string baseUrl)
        {
            _violationDetailsService = new ViolationDetailsService();
            _httpClientService = new HttpClientService(new Uri(baseUrl));
            _deviceSettingsService = new LocalDeviceSettingsService();
            _novService = new NovService();
        }

        public async Task Sync()
        {
            try
            {
                DependencyResolver.Get<IToastMessage>().DoToast("Sync time Started" + DateTime.Now);
                SyncInProgress = true;
                DisplayMessage("Sync in progress");
                await ContinuousPromptMsg();
                await SyncTime(); //Sync device time

                DisplayMessage("Sync in progress-Sync to server...");
                var ss = await SyncToServer(); //Sync Up 
                DisplayMessage("Current Status:---" + ss); 
                DisplayMessage("Sync in progress-Sync to client...");
                ss = await SyncToClient(); //Sync Down
                DisplayMessage("Current Status:---" + ss);
                SyncInProgress = false;
                DependencyResolver.Get<IToastMessage>().DoToast("Sync Complete" + DateTime.Now);
            }
            catch (Exception exc)
            {
                //TODO: Log
                //MessageCenter.Send(MessageCenterConstants.SyncFailed);
                var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
                await appLogRepo.InsertAsync(new DBApplicationLog { AddTimestamp = DateTime.Now, Data = exc.Message, LogTimestamp = DateTime.Now, Source = exc.StackTrace });
                string str = exc.Message;
                // logService.LogError(String.Format("Error with Sync: {0}", exc));
            }
        }

        private bool SyncInProgress { get; set; }
        private string continousMessage { get; set; }

        public void DisplayMessage(string msg)
        {
            DependencyResolver.Get<IToastMessage>().DoToast(msg);
            continousMessage = msg;
        }
        public async Task ContinuousPromptMsg()
        {
            while (SyncInProgress)
            {
                DependencyResolver.Get<IToastMessage>().DoToast(continousMessage);
                await Task.Delay(10000);
            }

        }

        public async Task<string> SyncToServer()
        {
          
        string deviceId = "";
            string jsonText = "";
            string userId = "";
            List<long> errorNovs = new List<long>();
            long currentNovNo = 0;

            try
            {
#if TEST
                var hardwareId = "TestDevice";
#else
                //NH-979
                //var hardwareId = DependencyResolver.Get<IAppRuntimeSettings>().HardwareIdentifier;
#endif
                //var deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
                //DBDevices dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == hardwareId);
                //deviceId = dbDevice.DeviceId.Trim();

                string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
                deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);

                var ticketRangeRepo = DependencyResolver.Get<IRepository<DBDeviceTicketRanges>>();
                var allRanges = await ticketRangeRepo.GetAsync<DBDeviceTicketRanges>();
                List<DBDeviceTicketRanges> rangesToSync2 = allRanges.FindAll((range) => range.DeviceId.Trim() == deviceId && (range.RangeStatus == "A" || range.RangeStatus == "C"));

                if(rangesToSync2.FindAll((range) => range.DeviceId.Trim() == deviceId && (range.RangeStatus == "A")).Count > 0)
                {
                    var maxNovs = await _novService.GetViolations();
                    var novs = new ArrayList();
                    foreach (var item in maxNovs)
                    {
                        novs.Add(item.NovNumber);
                    }
                    novs.Sort();
                    novs.Reverse();
                    if(novs.Count>0)
                    {
                        var maxNov = (long)novs[0];

                        var lastTicketUsed = rangesToSync2.FindAll((range) => range.DeviceId.Trim() == deviceId && (range.RangeStatus == "A"))[0].LastTicketNumber;
                        if (maxNov != lastTicketUsed)
                        {
                            var toUpdate = rangesToSync2.FindAll((range) => range.DeviceId.Trim() == deviceId && (range.RangeStatus == "A"))[0];
                            toUpdate.LastTicketNumber = maxNov;
                            await ticketRangeRepo.UpdateAsync(toUpdate);
                        }
                    }
                    
                }

                List<DBDeviceTicketRanges> rangesToSync = allRanges.FindAll((range) => range.DeviceId.Trim() == deviceId && (range.RangeStatus == "A" || range.RangeStatus == "C"));

                var dutyHeaderRepo = DependencyResolver.Get<IRepository<DBDutyHeader>>();
                List<DBDutyHeader> dutyHeaders = await dutyHeaderRepo.GetAsync<DBDutyHeader>();

               
               //var ss= maxTicketNumber.Sort()
                foreach (var dh in dutyHeaders)
                {
                    //dh.UserId
                    var violations = await _novService.GetViolations(dh.UserId);
                    dh.TicketCount= violations.FindAll(v => v.TicketStatus != "V" && v.TicketStatus != "C" && v.LoginTimestamp== dh.LoginTimestamp).Count;
                    dh.VoidCount = violations.FindAll(v => v.TicketStatus == "V" && v.LoginTimestamp == dh.LoginTimestamp).Count;
                }
                
                
                //userId = dutyHeaders?[0].UserId;

                var vehicleRadioInfoRepo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
                List<DBVehicleRadioInfo> vehicleRadioInfo = await vehicleRadioInfoRepo.GetAsync();

                var novInfoRepo = DependencyResolver.Get<IRepository<DBNovInformation>>();
                List<DBNovInformation> novInfo = await novInfoRepo.GetAsync();


                //start running through each ticket to sync
                foreach (var nov in novInfo)
                {
                    userId = nov.UserId;
                    currentNovNo = nov.NovNumber;
                    nov.SentTimestamp = TimeManager.Now;
                    var novDataRepo = DependencyResolver.Get<IRepository<DBNovData>>();

                    var lnov = nov.NovNumber;

                    var novData = await novDataRepo.GetAsync<DBNovData>(m => m.NovNumber == lnov);
                    List<DBNovData> currentNovDataItems = new List<DBNovData>();

                    foreach (var novDataItem in novData)
                    {
                        if (novDataItem.NovNumber == currentNovNo)
                        {
                            var placeholderKey = novDataItem.FieldName.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[0].Replace("(", "").Replace(")", "");
                            var fieldInfo = await _violationDetailsService.GetViolationScriptVariables(placeholderKey);
                            if (fieldInfo != null)
                            {
                                novDataItem.FieldName = fieldInfo.FieldName;
                            }
                            currentNovDataItems.Add(novDataItem);
                        }
                    }

                    var certRepo = DependencyResolver.Get<IRepository<DBNovPublicKey>>();
                    DBNovPublicKey cert = await certRepo.GetAsync(_ => _.NovNumber == nov.NovNumber);

                    var affidavitRepo = DependencyResolver.Get<IRepository<DBAffidavitOfService>>();

                    var aos2 = await affidavitRepo.GetAsync();

                    DBAffidavitOfService aos = await affidavitRepo.GetAsync(_ => _.NovNumber == nov.NovNumber);

                    var affidavitTranRepo = DependencyResolver.Get<IRepository<DBAffidavitOfServiceTran>>();
                    DBAffidavitOfServiceTran affidavitsTran = await affidavitTranRepo.GetAsync(_ => _.NovNumber == nov.NovNumber);


                    /* -- CANCEL NOV TABLES -- */
                    DBCancelNovInformation cancelNovInfo = null;
                    IList<DBCancelNovData> cancelNovData = null;
                    var cancelNovInfoRepo = DependencyResolver.Get<IRepository<DBCancelNovInformation>>();
                    //IList<DBCancelNovData> cancelNovDataRepo = DependencyResolver.Get<IRepository<DBCancelNovData>>();

                    var cancelNovDataRepo = DependencyResolver.Get<IRepository<DBCancelNovData>>();



                    if (nov.TicketStatus.Equals("C"))
                    {


                        if (nov.NovNumber.ToString().Length >= 8)
                        {
                            string cancelNo = String.Format("C{0}", nov.NovNumber.ToString().Substring(1, 7));

                            cancelNovInfo = await cancelNovInfoRepo.GetAsync(_ => _.CancelNo == cancelNo);
                            if (cancelNovInfo != null)
                            {
                                cancelNovInfo.Resp1SectionId = "0";
                                cancelNovInfo.IsResp1AddressHit = "false";

                                cancelNovData = await cancelNovDataRepo.GetAsync<DBCancelNovData>(_ => _.DeviceId == nov.DeviceId);
                            }

                        }

                    }

                    /* -- END CANCEL NOV TABLES -- */


                    JObject jsonData = new JObject();
                    JProperty dutyHeaderJson = new JProperty("DutyHeaders", JToken.FromObject(dutyHeaders));
                    jsonData.Add(dutyHeaderJson);

                    JProperty vehicleRadioInfoJson = new JProperty("VehicleRadioInfo", JToken.FromObject(vehicleRadioInfo));
                    jsonData.Add(vehicleRadioInfoJson);

                    JProperty ticketRangeJson = new JProperty("DeviceTicketRanges", JToken.FromObject(rangesToSync));
                    jsonData.Add(ticketRangeJson);
                    if (nov.TicketStatus != "C")
                    {
                        JProperty novInfoJson = new JProperty("NovInformation", JToken.FromObject(nov));
                        jsonData.Add(novInfoJson);
                    }


                    if (currentNovDataItems.Count > 0)
                    {
                        JProperty novDataJson = new JProperty("NovData", JToken.FromObject(currentNovDataItems));
                        jsonData.Add(novDataJson);
                    }

                    if (cert != null)
                    {
                        JProperty novCertJson = new JProperty("NovCertificate", JToken.FromObject(cert));
                        jsonData.Add(novCertJson);
                    }

                    if (aos != null)
                    {
                        JProperty affidavitsJson = new JProperty("AffidavitOfService", JToken.FromObject(aos));
                        jsonData.Add(affidavitsJson);
                    }

                    if (cancelNovData != null && cancelNovData.Count > 0)
                    {
                        JProperty cancelNovDataJson = new JProperty("CancelNovData", JToken.FromObject(cancelNovData));
                        jsonData.Add(cancelNovDataJson);
                    }

                    if (cancelNovInfo != null)
                    {
                        JProperty cancelNovInfoJson = new JProperty("CancelNovInfo", JToken.FromObject(cancelNovInfo));
                        jsonData.Add(cancelNovInfoJson);
                    }

                    if (affidavitsTran != null)
                    {
                        JProperty affidavitsTranJson = new JProperty("AffidavitOfServiceTran", JToken.FromObject(affidavitsTran));
                        jsonData.Add(affidavitsTranJson);
                    }

                    jsonText = jsonData.ToString();
                    var isTicketSaved = await _httpClientService.PostAsync("save", jsonData);

                    if (!isTicketSaved)
                    {
                        if (nov.TicketStatus == "C")
                        {
                            long cancelNo2 = Convert.ToInt64(nov.NovNumber.ToString().Substring(1, 7));
                            errorNovs.Add(cancelNo2);
                        }
                        else
                        {
                            errorNovs.Add(nov.NovNumber);
                        }

                    }
                    //Delete the nov data from the device
                    else
                    {

                        await novInfoRepo.DeleteAsync(nov);
                        if (aos != null)
                            await affidavitRepo.DeleteAsync(aos);

                        foreach (var novDataItem in currentNovDataItems)
                        {
                            if (novDataItem != null)
                                await novDataRepo.DeleteAsync(novDataItem);

                        }

                        if (affidavitsTran != null)
                            await affidavitTranRepo.DeleteAsync(affidavitsTran);

                        if (cancelNovInfo != null)
                            await cancelNovInfoRepo.DeleteAsync(cancelNovInfo);

                        if (cancelNovData != null)
                        {
                            foreach (var item in cancelNovData)
                            {
                                if (item != null)
                                    await cancelNovDataRepo.DeleteAsync(item);
                            }

                        }

                    }
                }

                if (errorNovs.Count == 0)
                {
                    foreach (var dutyheader in dutyHeaders)
                    {
                        if (dutyheader != null)
                            await dutyHeaderRepo.DeleteAsync(dutyheader);

                    }


                    foreach (var radio in vehicleRadioInfo)
                    {
                        if (radio != null)
                            await vehicleRadioInfoRepo.DeleteAsync(radio);
                    }




                }

                var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
                List<DBApplicationLog> applicationLog = await appLogRepo.GetAsync();
                foreach (var item in applicationLog)
                {
                    item.ComputerName = "Handheld Device";
                    item.ProcessTimestamp = DateTime.Now;
                    item.AddTimestamp = DateTime.Now;
                    //AddTimestamp

                }

                bool isAppLogSaved = true;
                if (applicationLog != null && applicationLog.Count > 0)
                {
                    JObject appLogJson = new JObject();
                    JProperty applicationLogJson = new JProperty("ApplicationLogs", JToken.FromObject(applicationLog));
                    appLogJson.Add(applicationLogJson);



                    isAppLogSaved = await _httpClientService.PostAsync("logs", appLogJson);
                }

                string statusMessage = "";
                if (errorNovs.Count == 0 && isAppLogSaved)
                {
                    //statusMessage = "All voilations have been successfully synced";
                    foreach (var item in applicationLog)
                    {
                        if (item != null)
                            await appLogRepo.DeleteAsync(item);
                    }

                    statusMessage = SyncServiceConstants.FullSyncTableName + "\r\n" + SyncServiceConstants.SYNC_TICKET_SUCCESS;
                }
                else if (errorNovs.Count == 0 && !isAppLogSaved)
                {
                    //statusMessage = "All violations have been successfully synced. Application logs failed to sync.";
                    statusMessage = SyncServiceConstants.FullSyncTableName + "\r\n" + SyncServiceConstants.SYNC_TICKET_SUCCESS_LOG_FAIL;
                }
                else
                {
                    //statusMessage = string.Format("The following voilations failed to sync: {0}", string.Join(",", errorNovs));
                    statusMessage = string.Format(SyncServiceConstants.SYNC_TICKET_FAIL, string.Join(",", errorNovs));
                }


                // DisplayMessage("statusMessage: "+ statusMessage);
                if (novInfo.Count == 0)
                {
                    statusMessage = "No Violation Record found to Sync to boro server.";
                }

                return statusMessage;
            }
            catch (Exception exc)
            {
                var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
                await appLogRepo.InsertAsync(new DBApplicationLog()
                {
                    Source = "SyncService.SyncToServer()",
                    Description = String.Format("{0}{1}", exc.Message, exc.StackTrace),
                    LogTimestamp = DateTime.Now,
                    DeviceId = deviceId,
                    Data = jsonText,
                    UserId = userId
                });

                errorNovs.Add(currentNovNo);
                string errorMessage = string.Format("The following violations failed to sync: {0}", string.Join(",", errorNovs));
                return errorMessage;
            }
        }

        public async Task<string> SyncUsers()
        {
            var userMasterRepo = DependencyResolver.Get<IRepository<DBNovasUserMaster>>();
            List<DBNovasUserMaster> updatedUserMasterEntries = await userMasterRepo.GetAsync<DBNovasUserMaster>();

            var userRepo = DependencyResolver.Get<IRepository<DBNovasUser>>();
            var novasUsers = await userRepo.GetAsync<List<DBNovasUser>>();

            var updatedUsers = updatedUserMasterEntries.FindAll(_ => _.LastActivityTimestamp > DateTime.Now.AddDays(-1));
            List<DBNovasUser> updatedNovasUsers = new List<DBNovasUser>();
            if (updatedUsers != null && updatedUsers.Count > 0)
            {
                foreach (var user in updatedUsers)
                {
                    var novasUser = novasUsers.Find(_ => _.UserId == user.UserId);
                    if (novasUser != null)
                    {
                        updatedNovasUsers.Add(novasUser);
                    }
                }
            }
            JObject jsonData = new JObject();
            JObject jsonData2 = new JObject();

            JProperty novasUserMasterJson = new JProperty("NovasUserMaster", JToken.FromObject(updatedUsers));
            jsonData2.Add(novasUserMasterJson);
            var jsonTxt2 = jsonData2.ToString();


            JProperty novasUserJson = new JProperty("NovasUser", JToken.FromObject(updatedNovasUsers));
            jsonData.Add(novasUserJson);
            //jsonData.Add(novasUserMasterJson);


            var jsonTxt = jsonData.ToString();

            var isUsersSaved = await _httpClientService.PostAsync("users", jsonData);

            var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
            List<DBApplicationLog> applicationLog = await appLogRepo.GetAsync();

            foreach (var item in applicationLog)
            {
                item.ComputerName = "Handheld Device";
                item.ProcessTimestamp = DateTime.Now;
                item.AddTimestamp = DateTime.Now;
                //AddTimestamp

            }

            JObject appLogJson = new JObject();
            JProperty applicationLogJson = new JProperty("ApplicationLogs", JToken.FromObject(applicationLog));


            appLogJson.Add(applicationLogJson);

            var isAppLogSaved = await _httpClientService.PostAsync("logs", appLogJson);

            if (isUsersSaved && isAppLogSaved)
                return "All users successfully synced" + SyncServiceConstants.UserTableName;

            return "Failed to sync users";

        }
        public async Task<string> SyncToClient()
        {

            var db = new DatabaseSynchronizer();
            await db.SetDescriptors();

            var tableNames = ""; 
            try
            {
                //get the last time we synced this device
                var lastSyncTimeSetting = await _deviceSettingsService.GetLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey);
                string lastSyncTime = "1970-01-01 00:00:00 AM";
                if (lastSyncTimeSetting != null)
                    lastSyncTime = lastSyncTimeSetting.Value;

                DateTime dt = Convert.ToDateTime(lastSyncTime);

                // Grab tables to sync from server based on last sync time on device
                var tablesToPull = await _httpClientService.GetTablesToSync(dt.ToString("yyyy-MM-dd h:mm:ss tt"));

                //if the api errored, fail
                if (tablesToPull == null)
                {
                    return "FAILED: Could not fetch tables from API";
                }

                //Deserialize the response
                var tableData = (JObject)JsonConvert.DeserializeObject(tablesToPull);
                var tableCount = tableData["RecordCount"].Value<int>();

                var tableList = new List<BorositeTableMetadata>();
                //Manually adding this table to sync every time because we cannot add to the database without 
                //breaking the Intermec sync.
                //TODO: Remove this when all Intermecs are decommissioned
                tableList.Add(new BorositeTableMetadata()
                {
                    TableName = "HH_NovasUser",
                    ActivityTimestamp = DateTime.Now,
                    Module = "C",
                    SqlStatement = "select * from [dbo].[BS_NovasUser]"
                });
                tableList.Add(new BorositeTableMetadata()
                {
                    TableName = "HH_Devices",
                    ActivityTimestamp = DateTime.Now,
                    Module = "C",
                    SqlStatement = "select * from [Masterdata].[BS_HHDevices]"
                });
                tableList.Add(new BorositeTableMetadata()
                {
                    TableName = "HH_DeviceTicketRanges",
                    ActivityTimestamp = DateTime.Now,
                    Module = "C",
                    SqlStatement = "[sync].[GetDeviceTicketRanges]"
                });

                //if we have some tables to sync from the results of the query of the API, add those to the list
                if (tableCount != 0)
                {
                    var dbTables = JsonConvert.DeserializeObject<List<BorositeTableMetadata>>(tableData["BorositeData"].ToString());
                    tableList.AddRange(dbTables);
                }

                //ensure that we deserialized correctly
                if (tableList != null && tableList.Count > 0)
                {
                    var repo = DependencyResolver.Get<IDatabaseSynchronizer>();

                    //await repo.SetUpNovasDb();

                    var statusDictionary = new Dictionary<string, string>();

                    foreach (var table in tableList)
                    {
                        if (1 == 1)
                        //if (table.TableName == "HH_Users")
                        {

                            tableNames += "\r\n" + table.TableName;

                            //TODO: From line 334 to line 345 can be removed when all Intermecs are decommissioned.
                            //You will have to update the table names in BS_MobileDataPull table match the table names in SQLite
                            //Remove the HH_ from the table name from the database
                            string tableName = table.TableName.Substring(3);
                            string sqlLiteTableName = tableName;
                            switch (tableName)
                            {
                                case "HHViolator": sqlLiteTableName = "Violator"; break;
                                case "Users": sqlLiteTableName = "NovasUserMaster"; break;
                                case "HHTPropertyDetails": sqlLiteTableName = "PropertyDetails"; break;
                                case "IDMatrix": sqlLiteTableName = "IdMatrix"; break;
                                case "Titlereportlevel": sqlLiteTableName = "TitleReportLevel"; break;
                                case "Miscellaneous": sqlLiteTableName = "Lookup"; break;
                                default: break;
                            }

                            statusDictionary[SyncServiceConstants.TableName] = tableName;
                            statusDictionary.Remove(SyncServiceConstants.CurrentRecord);
                            statusDictionary.Remove(SyncServiceConstants.RecordCount);
                            SyncStatusUpdatedCommand?.Execute(statusDictionary);


                            if(tableName== "DCALicenseDetails")
                            {
                                string stopme= "";
                            }

                            var json = await _httpClientService.GetAsync(tableName, null, null);
                            if (json != null)
                            {
                                var data = (JObject)JsonConvert.DeserializeObject(json);
                                var recordCount = data["RecordCount"].Value<int>();

                                //if we have data, truncate the table and start inserting
                                await repo.SetUpNovasDbTable(sqlLiteTableName);
                                await repo.SyncAsync(sqlLiteTableName, data["BorositeData"].ToString());

                                if (recordCount <= 5000) continue; // Already synced all records in the first batch

                                statusDictionary[SyncServiceConstants.RecordCount] = recordCount.ToString();
                                for (var i = 5001; i <= recordCount; i += 5000)
                                {
                                    statusDictionary[SyncServiceConstants.CurrentRecord] = i.ToString();
                                    var rangeEnd = i + 4999 > recordCount ? recordCount : i + 4999;
                                    statusDictionary[SyncServiceConstants.CurrentRangeEnd] = rangeEnd.ToString();
                                    SyncStatusUpdatedCommand?.Execute(statusDictionary);

                                    // DisplayMessage("Currentely processsing:" + tableName);

                                    json = await _httpClientService.GetAsync(tableName, i, i + 4999);
                                    data = (JObject)JsonConvert.DeserializeObject(json);
                                    await repo.SyncAsync(sqlLiteTableName, data["BorositeData"].ToString());
                                }
                            }
                        }
                    }
                    //full sync was successful, update the last sync time locally
                    await _deviceSettingsService.InsertOrUpdateLocalDeviceSetting(LocalDeviceSettingsService.LastSyncTimeKey, DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt"));
                    // update sync time to Applicatian log table.
                    await SyncTime();
                }
                else
                {
                    return "FAILED: Could not deserialize table response"; //we couldn't deserialize the response, fail.
                }

            }
            catch (Exception exc)
            {
                // logService.LogError(String.Format("Error with Sync to Client: {0}", exc));

                return "FAILED: " + tableNames + "\r\n" + exc.Message;
            }

            return SyncServiceConstants.SYNC_BORO_TO_DEVICE + tableNames;
        }

        public async Task<bool> SyncTime()
        {
            try
            {
                var statusDictionary = new Dictionary<string, string>();
                statusDictionary[SyncServiceConstants.TableName] = "System Time";
                statusDictionary.Remove(SyncServiceConstants.CurrentRecord);
                statusDictionary.Remove(SyncServiceConstants.RecordCount);
                SyncStatusUpdatedCommand?.Execute(statusDictionary);

                string deviceIMEI = DependencyResolver.Get<IAppRuntimeSettings>().DeviceIMEI;
                string deviceId = await DependencyResolver.Get<IDeviceService>().GetDeviceIdByIMEI(deviceIMEI);

                var json = await _httpClientService.GetServerTimeAsync(deviceId);

                if (json != null)
                {

                    DateTime serverTime = JsonConvert.DeserializeObject<DateTime>(json);
                    TimeManager.UpdateOffset(serverTime);
                    //await _deviceSettingsService.InsertOrUpdateLocalDeviceSetting(LocalDeviceSettingsService.DeviceOffsetTimeKey,
                    //    TimeManager.CurrentOffset.ToString());

                    return true;
                }
            }
            catch (Exception ex)
            {
                //Failed to sync with server time
                return false;
            }

            return false;

        }

        public async Task<string> SyncDeviceId(string deviceIMEI, string deviceSerialNum, string platformDeviceName, string hhtNumber, string boroSiteCode)
        {
            string deviceId = "";
            string jsonText = "";
            string userId = "";
            try
            {

                DeviceSetupInfo deviceSetupInfo = new DeviceSetupInfo();
                deviceSetupInfo.DeviceIMEI = deviceIMEI;
                deviceSetupInfo.DeviceSerialNumber = deviceSerialNum;
                deviceSetupInfo.PlatformDeviceName = platformDeviceName;
                deviceSetupInfo.HHTNumber = hhtNumber;
                deviceSetupInfo.BoroSiteCode = boroSiteCode;
                deviceSetupInfo.IsPendingSetup = true;
                deviceSetupInfo.createdate = DateTime.Now;

                JObject jsonData = new JObject();
                //JProperty deviceSetupInfoJson = new JProperty("DeviceSetupInfo", JToken.FromObject(deviceSetupInfo));
                //jsonData.Add(deviceSetupInfoJson);                
                jsonData.Add(new JProperty("DeviceIMEI", JToken.FromObject(deviceSetupInfo.DeviceIMEI)));
                jsonData.Add(new JProperty("PlatformDeviceName", JToken.FromObject(deviceSetupInfo.PlatformDeviceName)));
                jsonData.Add(new JProperty("SerialNumber", JToken.FromObject(deviceSetupInfo.DeviceSerialNumber)));
                jsonData.Add(new JProperty("HHTNumber", JToken.FromObject(deviceSetupInfo.HHTNumber)));
                jsonData.Add(new JProperty("BoroSiteCode", JToken.FromObject(deviceSetupInfo.BoroSiteCode)));
                jsonData.Add(new JProperty("IsPendingSetup", JToken.FromObject(deviceSetupInfo.IsPendingSetup)));
                jsonData.Add(new JProperty("createdate", JToken.FromObject(deviceSetupInfo.createdate)));
                jsonText = jsonData.ToString();
                var isDeviceIdSaved = await _httpClientService.PostAsyncForDeviceSetup(jsonData);

                /**
                 * // TODO: logs
                var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
                List<DBApplicationLog> applicationLog = await appLogRepo.GetAsync();
                foreach (var item in applicationLog)
                {
                    item.ComputerName = "Handheld Device";
                    item.ProcessTimestamp = DateTime.Now;
                    item.AddTimestamp = DateTime.Now;
                    //AddTimestamp

                }

                bool isAppLogSaved = true;
                if (applicationLog != null && applicationLog.Count > 0)
                {
                    JObject appLogJson = new JObject();
                    JProperty applicationLogJson = new JProperty("ApplicationLogs", JToken.FromObject(applicationLog));
                    appLogJson.Add(applicationLogJson);
                    isAppLogSaved = await _httpClientService.PostAsync("logs", appLogJson);
                }
                

                string statusMessage = "";
                if (errorNovs.Count == 0 && isAppLogSaved)
                {
                    //statusMessage = "All voilations have been successfully synced";
                    statusMessage = SyncServiceConstants.FullSyncTableName + "\r\n" + SyncServiceConstants.SYNC_TICKET_SUCCESS;
                }
                else if (errorNovs.Count == 0 && !isAppLogSaved)
                {
                    //statusMessage = "All violations have been successfully synced. Application logs failed to sync.";
                    statusMessage = SyncServiceConstants.FullSyncTableName + "\r\n" + SyncServiceConstants.SYNC_TICKET_SUCCESS_LOG_FAIL;
                }
                else
                {
                    //statusMessage = string.Format("The following voilations failed to sync: {0}", string.Join(",", errorNovs));
                    statusMessage = string.Format(SyncServiceConstants.SYNC_TICKET_FAIL, string.Join(",", errorNovs));
                }


                // DisplayMessage("statusMessage: "+ statusMessage);

                return statusMessage;
    */
                if (isDeviceIdSaved != null && isDeviceIdSaved.ToUpper().Contains("SUCCESS"))
                    return "SyncDeviceId is completed Successfully!!";
                else
                    return "SyncDeviceId is Failed!";
            }
            catch (Exception exc)
            {
                var appLogRepo = DependencyResolver.Get<IRepository<DBApplicationLog>>();
                await appLogRepo.InsertAsync(new DBApplicationLog()
                {
                    Source = "SyncService.SyncDeviceId()",
                    Description = String.Format("{0}{1}", exc.Message, exc.StackTrace),
                    LogTimestamp = DateTime.Now,
                    DeviceId = deviceId,
                    Data = jsonText,
                    UserId = userId
                });

                return "SyncDeviceId Failed!!";
            }

        }

        class DeviceSetupInfo
        {

            public string DeviceIMEI { get; set; }
            public string DeviceSerialNumber { get; set; }
            public string PlatformDeviceName { get; set; }
            public string HHTNumber { get; set; }
            public string BoroSiteCode { get; set; }

            public bool IsPendingSetup { get; set; }

            public DateTime createdate { get; set; }

            //TODO: add additional columns e.g. ipaddress , device model
        }
    }
}

