﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;


namespace DSNY.Novas.Services
{
    public class LocalDeviceSettingsService : BaseService, ILocalDeviceSettingsService
    {
        public static readonly string DeviceOffsetTimeKey = "time";
        public static readonly string LastSyncTimeKey = "sync";
        public async Task<LocalDeviceSettings> GetLocalDeviceSetting(string key)
        {
            var repository = DependencyResolver.Get<IRepository<DBLocalDeviceSettings>>();

            List<DBLocalDeviceSettings> dbSettings =
                await repository.GetAsync<List<DBLocalDeviceSettings>>(_ => _.Key == key);

            if (dbSettings.Count == 1)
            {
                var setting = dbSettings?.Select(_ => new LocalDeviceSettings()
                {
                    LocalDeviceSettingKey = _.DBLocalDeviceSettingKey,
                    Key = _.Key,
                    Value = _.Value
                }).First();

                return setting;
            }

            return null;
        }

        public async Task<bool> InsertOrUpdateLocalDeviceSetting(string key, string value)
        {
            var repository = DependencyResolver.Get<IRepository<DBLocalDeviceSettings>>();
            List<DBLocalDeviceSettings> dbSettings =
                await repository.GetAsync<List<DBLocalDeviceSettings>>(_ => _.Key == key);

            if (dbSettings == null || dbSettings.Count == 0)
            {
                await repository.InsertAsync(new DBLocalDeviceSettings() { Key = key, Value = value });
            }
            else
            {
                var settingId = dbSettings.First().DBLocalDeviceSettingKey;
                await repository.UpdateAsync(new DBLocalDeviceSettings() { DBLocalDeviceSettingKey = settingId, Key = key, Value = value });
            }

            return true;
        }
    }
}
