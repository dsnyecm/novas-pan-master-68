﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IVehicleService
    {
        Task<int> SaveVehicleRadio(VehicleRadioInfo vehicleRadio);
        Task<VehicleRadioInfo> GetCurrentVehicleRadioInfo(string vehicleId);
        Task<bool> isVehicleCurrent(string userId);
        Task<int> UpdateVehicleRadio(VehicleRadioInfo vehicleRadio);
        Task<short> GetLatestRecordEntry(string vehicleId, string radioId);
        Task<short> GetLatestVehicleRadioId();
        Task<VehicleRadioInfo> GetFinishedVehicleRadioInfo(string v);
        Task<List<VehicleRadioInfo>> GetAllVehicleRadioInfoByUser(string userId);
        Task<VehicleRadioInfo> GetCurrentVehicleRadioInfoByUser(string userId);
        Task<short> GetLatestVehicleRadioIdByUserId(string userId);
        
    }
}