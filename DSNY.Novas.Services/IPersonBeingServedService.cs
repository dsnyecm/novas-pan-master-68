﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IPersonBeingServedService
    {
        Task<List<BoroMaster>> GetBoros();
        Task<List<IdMatrix>> GetIdMatrices();
    }
}
