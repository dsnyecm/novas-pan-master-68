﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class PersonBeingServedService : BaseService, IPersonBeingServedService
    {
        public async Task<List<BoroMaster>> GetBoros()
        {
            var repo = DependencyResolver.Get<IRepository<DBBoroMaster>>();
            var dbBoros = await repo.GetAsync();

            var boros = dbBoros.Select(_ => new BoroMaster
            {
                BoroId = _.BoroId,
                Name = _.Name,
                ShortName = _.ShortName
            }).OrderBy(_ => _.BoroId).ToList();

            boros = boros.Where(b => b.BoroId != "0").ToList();

            return boros;
        }

        public async Task<List<IdMatrix>> GetIdMatrices()
        {
            var repo = DependencyResolver.Get<IRepository<DBIdMatrix>>();
            var dbIdMatrices = await repo.GetAsync();

            var matrices = dbIdMatrices.Select(dbMatrix => new IdMatrix
            {
                
            }).ToList();

            return matrices;
        }
    }
}
