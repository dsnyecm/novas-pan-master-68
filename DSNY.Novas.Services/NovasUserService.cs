﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using System.Text;
using System;

namespace DSNY.Novas.Services
{
    public class NovasUserService : INovasUserService
    {
        public async Task<NovasUser> GetUser(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovasUserMaster>>();

            var dbUser = await repo.GetAsync(_ => _.UserId == userId);

            NovasUser user = null;
            if (dbUser != null)
            {
                user = new NovasUser
                {
                    UserId = dbUser.UserId,
                    AgencyId = dbUser.AgencyId,
                    AbbrevName = dbUser.AbbrevName,
                    Name = dbUser.Name,
                    Title = dbUser.Title.ToString().Trim(),
                    IsSupervisor = dbUser.IsSupervisor,
                    BoroId = dbUser.BoroId,
                    SiteId = dbUser.SiteId,
                    //SignatureBitmap = dbUser.SignatureBitmap
                };
            }

            var luser = await GetUserNov(userId);

            user.SignatureBitmap = luser.SignatureBitmap;

            return user;
        }

        public async Task<List<NovasUser>> GetUsers()
        {
            var repo = DependencyResolver.Get<IRepository<DBNovasUserMaster>>();
            var users = await repo.GetAsync();

            var userList = users.Select(_ => new NovasUser //this represents NovasUserMaster table
            {
                AgencyId = _.AgencyId,
                Title = _.Title,
                UserId = _.UserId,
                Name = _.Name,
                SiteId = _.SiteId,
                BoroId = _.BoroId,
                IsSupervisor = _.IsSupervisor,
                //SignatureBitmap = _.SignatureBitmap,
            }).OrderBy(_ => _.Name).ToList();

            return userList;
        }
        public async Task<NovasUser> GetUserNov(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBNovasUser>>();

            var dbUser = await repo.GetAsync(_ => _.UserId == userId);

            NovasUser user = null;
            if (dbUser != null)
            {
                user = new NovasUser
                {
                    UserId = dbUser.UserId,                    
                    SignatureBitmap = dbUser.SignatureBitmap
                };
            }

            return user;
        }

        public async Task<List<Novas_User>> GetNovasUsers()
        {
            var repo = DependencyResolver.Get<IRepository<DBNovasUser>>(); 
            var users = await repo.GetAsync();

            List<Novas_User> userList = users.Select(_ => new Novas_User //this represents NovasUsers table
            {
                DBNovasUserID = _.DBNovasUserId,
                UserID = _.UserId,
                Salt = _.Salt,
                UserHash = _.UserHash,
                NfcUid = _.NfcUid,
                Pin = _.Pin,
                SignatureBitmap= _.SignatureBitmap,
            }).OrderBy(_ => _.UserID).ToList();

            return userList;
        }

        public async Task<bool> UpdateUser(string userId, string password, string nfcId, byte[] signatureBitmap)
        {
            string saltString = SimpleHash.GenerateRandomHexString(16);
            byte[] saltBytes = Encoding.UTF8.GetBytes(saltString);
            string passHash = SimpleHash.ComputeHash(password + nfcId, saltBytes);

            var masterRepo = DependencyResolver.Get<IRepository<DBNovasUserMaster>>();
            var hashRepo = DependencyResolver.Get<IRepository<DBNovasUser>>();

            var dbUserMasterRecord = await masterRepo.GetAsync(_ => _.UserId == userId);
            var dbUserHashRecord = await hashRepo.GetAsync(_ => _.UserId == userId);

            if (dbUserMasterRecord != null && signatureBitmap != null)
            {
                //dbUserMasterRecord.SignatureBitmap = signatureBitmap;
                dbUserMasterRecord.LastActivityTimestamp = DateTime.Now;
                int retVal = await masterRepo.UpdateAsync(dbUserMasterRecord);


               // dbUserHashRecord.SignatureBitmap = signatureBitmap;
                //int retVal = await hashRepo.UpdateAsync(dbUserHashRecord);

                
                if (retVal == 0) { return false; }
            }

            if (dbUserHashRecord == null)
            {
                dbUserHashRecord = new DBNovasUser()
                {
                    UserId = userId,
                    Pin = password,
                    NfcUid = nfcId,
                    Salt = saltString,
                    UserHash = passHash,
                    SignatureBitmap=signatureBitmap
                };

                int retVal = await hashRepo.InsertAsync(dbUserHashRecord);
                if (retVal == 0) { return false; }
            }
            else
            {
                dbUserHashRecord.Pin = password;
                dbUserHashRecord.NfcUid = nfcId;
                dbUserHashRecord.Salt = saltString;
                dbUserHashRecord.UserHash = passHash;
                dbUserHashRecord.SignatureBitmap = signatureBitmap;

                int retVal = await hashRepo.UpdateAsync(dbUserHashRecord);
                if (retVal == 0) { return false; }
            }

            return true;
        }
    }
}