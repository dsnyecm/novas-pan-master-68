﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services
{
    public class PersonnelDataService : IPersonnelDataService
    {
        public async Task<List<HolidayMaster>> GetHolidaysAsync()
        {
            var repo = DependencyResolver.Get<IRepository<DBHolidayMaster>>();
            var dbHoliday = await repo.GetAsync();

            List<HolidayMaster> holidays = dbHoliday.Select(_ => 
            new HolidayMaster()
            {
                BoroCode = _.BoroCode,
                EffectiveDate = _.EffectiveDate,
                EndEffDate = _.EndEffDate,
                Comments = _.Comments
            }).ToList();

            return holidays;
        }

        public async Task<List<ReportingLevel>> GetReportingLevelListAsync()
        {
            var repo = DependencyResolver.Get<IRepository<DBTitleReportLevel>>();
            var dbRL = await repo.GetAsync();

            List<ReportingLevel> reportingLevels = dbRL.Select(_ => new ReportingLevel()
            {
                ReportingLevelId = _.ReportLevel.Trim(),
                Title = _.Title.Trim(),
                Rank = _.Rank
            }).ToList();

            return reportingLevels;
        }

        public async Task<List<Agency>> GetAgencyListAsync()
        {
            var repo = DependencyResolver.Get<IRepository<DBAgencies>>();
            var dbAgencies = await repo.GetAsync();

            List<Agency> agencies = dbAgencies.Select(_ => new Agency()
            {
                AgencyId = _.AgencyId,
                Name = _.Name,
                Description = _.Description,
                CreatedDate = _.CreatedDate
            }).OrderBy(x => x.AgencyId).ToList();

            return agencies;
        }

        public async Task<int> GetBoroIdForCurrentDevice()
        {
            var codeEnforceRepo = DependencyResolver.Get<IRepository<DBCodeEnforceControl>>();
            var codeEnforce = (await codeEnforceRepo.GetAsync()).FirstOrDefault();

            var boroSiteMasterRepo = DependencyResolver.Get<IRepository<DBBoroSiteMaster>>();
            var boroSiteMasterList = await boroSiteMasterRepo.GetAsync();
            var currentBoro = boroSiteMasterList.FirstOrDefault(_ => _.BoroSiteCode.Equals(codeEnforce?.LocalSite, StringComparison.OrdinalIgnoreCase));

            return Convert.ToInt32(currentBoro?.BoroCode ?? "0");
        }

        public async Task<int> GetBoroIdForReportLevel(string reportLevel)
        {
            var boroSiteMasterRepo = DependencyResolver.Get<IRepository<DBBoroSiteMaster>>();
            var boroSiteMasterList = await boroSiteMasterRepo.GetAsync();
            var matchingBoro = boroSiteMasterList.FirstOrDefault(_ => _.ReportLevel.Equals(reportLevel, StringComparison.OrdinalIgnoreCase));

            return Convert.ToInt32(matchingBoro?.BoroCode ?? "0");
        }

        public async Task<List<DBBoroSiteMaster>> GetBoroSites() => await DependencyResolver.Get<IRepository<DBBoroSiteMaster>>().GetAsync();

        public async Task<List<HearingDateTime>> GetHearingTimesAsync(Agency agency)
        {
            var repo = DependencyResolver.Get<IRepository<DBHearingDateTime>>();
            var dbTimes = await repo.GetAsync();
            dbTimes = dbTimes.Where(_ => _.AgencyId == agency.AgencyId).ToList();


            List<HearingDateTime> hearingTimes = dbTimes.Select(_ => new HearingDateTime()
            {
                AgencyId = _.AgencyId,
                HearingFromDate = _.HearingFromDate,
                HearingToDate = _.HearingToDate,
                HearingTime = _.HearingTime
            }).ToList();

            return hearingTimes;
        }

        public async Task<List<HearingDateTime>> GetHearingTimesAsync(int agencyId) =>
            (await DependencyResolver.Get<IRepository<DBHearingDateTime>>().GetAsync())
            .Where(_ => agencyId == 0 || _.AgencyId == agencyId)
            .Select(_ => new HearingDateTime
            {
                AgencyId = _.AgencyId,
                HearingFromDate = _.HearingFromDate,
                HearingToDate = _.HearingToDate,
                HearingTime = _.HearingTime
            }).ToList();

        public async Task<List<HearingDate>> GetHearingDatesAsync(int boroId)
        {
            var repo = DependencyResolver.Get<IRepository<DBHearingDates>>();
            var dbDates = await repo.GetAsync();
            dbDates = dbDates.Where(_ => boroId == 0 || _.BoroCode == boroId.ToString()).ToList();
            return dbDates.Select(_ => new HearingDate()
            {
                BoroCode = _.BoroCode,
                CurrentDate = _.CurrentDate,
                TimeStamp = _.TimeStamp
            }).OrderBy(_ => _.TimeStamp).ToList();
        }

        public async Task<List<DateTime>> GetHearingDatesBetweenAsync(int boroId, DateTime hearingFromDate, DateTime hearingToDate)
        {
            bool IsAnyBoroCode = (boroId == 0) ? true : false;
            var repo = DependencyResolver.Get<IRepository<DBHearingDates>>();
            var dbDates = await repo.GetAsync();

            var dbDateTimes = dbDates.Where(d => d.BoroCode == boroId.ToString() 
                                        && d.TimeStamp >= hearingFromDate 
                                        && d.TimeStamp <= hearingToDate                                        
                                        && (Convert.ToInt32(d.BoroCode) == boroId || IsAnyBoroCode))
                             .OrderBy(d => d.TimeStamp)
                             .Select(d => d.TimeStamp)
                             .ToList();

            return dbDateTimes;
        }
        public async Task<List<DateTime>> GetHolidaysBetweenAsync(int boroId, DateTime hearingFromDate, DateTime hearingToDate)
        {
            bool IsAnyBoroCode = (boroId == 0) ? true : false;
            var repo = DependencyResolver.Get<IRepository<DBHolidayMaster>>();
            var dbHoliday = await repo.GetAsync();

            List<DateTime> holidays = dbHoliday.Where(h => h.EffectiveDate >= hearingFromDate
                                                            && h.EffectiveDate <= hearingToDate
                                                            && ( Convert.ToInt32(h.BoroCode) == boroId || IsAnyBoroCode))
                                               .Select(h => h.EndEffDate).ToList();

            return holidays;
        }

       
    }
}