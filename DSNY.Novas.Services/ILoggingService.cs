﻿using System;

namespace DSNY.Novas.Services
{
    public interface ILoggingService
    {
        void LogError(string message);
        void LogDebug(string message);
        void LogInfo(string message);
    }
}