﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSNY.Novas.Services
{
    public interface IToastMessage
    {
        void DoToast(string stringContent);
    }
}
