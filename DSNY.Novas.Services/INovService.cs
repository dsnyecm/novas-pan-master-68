﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface INovService
    {
        Task<List<ViolationGroups>> GetViolationGroups();
        Task<List<ViolationTypes>> GetViolationTypes(Int64 violationGroupId);
        Task<List<ViolationDetails>> GetViolationDetailsByGroupAndName(Int64 violationGroupId, string groupName);
        Task<List<ViolationDetails>> GetViolationDetailsByGroupAndNameAndSearch(Int64 violationGroupId,
            string groupName, string search);
        Task<ViolationLawCodes> GetSummaryOfLaw(int lawId);
        Task<bool> CheckDeviceTicketRange();


        Task<long?> IncrementTicketValue();
        Task<bool> HasAnotherUserIncompleteTicket(String userID);
        Task VoidIncompleteTicketExceptThisUser(String userID);

        Task<DeviceTicketRanges> GetTicketRange();
        Task<long?> GetNextNovNumber(bool isPeek);
        Task SaveViolation(NovMaster novMaster);
        Task SaveCancelNovInformation(NovMaster novMaster);
        Task<List<CancelNovInformation>> GetCancelNovInformation();
        Task<List<CancelNovInformation>> GetCancelNovInformation(string userId);
        Task SaveDutyHeader(UserSession userSession);
        Task DeleteNovInfo();
        Task UpdateNovData(NovMaster novMaster);
        Task<List<NovData>> GetNovData(long novNumber);
        Task<List<NovInformation>> GetViolations();
        Task<List<NovInformation>> GetViolations(string userId);
        Task<List<NovInformation>> GetLaterViolations(string userId);
        Task<List<CodeLaw>> GetCodeLaws();
        Task DeleteLaterSrvNovInfo(long novNumber);

    }
}