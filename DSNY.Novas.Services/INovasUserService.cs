﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DSNY.Novas.Models;
using System;

namespace DSNY.Novas.Services
{
    public interface INovasUserService
    {
        Task<NovasUser> GetUser(string userId);
        Task<List<NovasUser>> GetUsers(); //this represents NovasUserMaster
        Task<List<Novas_User>> GetNovasUsers(); //this represents NovasUser
        Task<NovasUser> GetUserNov(string userId);
        Task<bool> UpdateUser(string userId, string password, string nfcId, byte[] signatureBitmap);


    }
}