﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using System.Text.RegularExpressions;

namespace DSNY.Novas.Services
{
    public class ViolationDetailsService : IViolationDetailsService
    {
        public async Task<string> GetCodeScript(string violationCode, string hhtIdentifier, int violationGroupId)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationDetails>>();
            var dbViolationDetails = await repo.GetAsync(_ => _.ViolationCode.Contains(violationCode) &&
                                                              _.HHTIdentifier.Equals(hhtIdentifier) &&
                                                              _.ViolationGroupId == violationGroupId);

            //return dbViolationDetails != null ? dbViolationDetails.CodeScript.Replace("\r\n", string.Empty) : string.Empty;
            return dbViolationDetails != null ? Regex.Replace(dbViolationDetails.CodeScript, @"[^0-9a-zA-Z\./#( )-_|]", string.Empty) : string.Empty;
        }

        public async Task<Dictionary<string, string>> GetNovDataFieldMappings()
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationScriptVariables>>();
            var dbResult = await repo.GetAsync();

            /*var lookup = dbResult.ToLookup(_ => _.FieldName, _ => _.Description);
            var all = lookup.SelectMany(l => l);
            lookup.Select(l => new { Key = l.Key, Value = l.ToArray() });*/

            var dbResultDistinct = dbResult.Select(_ => new { FieldName = _.FieldName, Description = _.Description }).Distinct();

            //return dbResult.ToDictionary(_ => _.FieldName, _ => _.Description);
            return dbResultDistinct.ToDictionary(_ => _.FieldName, _ => _.Description);
        }

        public async Task<ViolationScriptVariables> GetViolationScriptVariables(string description)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationScriptVariables>>();

            //var dbResult = await repo.GetAsync(_ => _.Description.Equals(description, System.StringComparison.OrdinalIgnoreCase)); // Apparently case-insensitive compare isn't supported in this sqlite implementation (?)
            var dbResult2 = await repo.GetAsync();
            var dbResult = await repo.GetAsync(_ => _.Description.ToUpper() == description.ToUpper());

            return new ViolationScriptVariables
            {
                Type = dbResult.Type,
                Description = dbResult.Description,
                FieldName = dbResult.FieldName,
                IsFreeForm = dbResult.IsFreeForm,
                IsMultiplesAllowed = dbResult.IsMultiplesAllowed,
                IsOtherAllowed = dbResult.IsOtherAllowed,
                MapField = dbResult.MapField,
                Mask = dbResult.Mask
            };
        }

        public async Task<List<ViolationScriptVariableData>> GetViolationScriptVariableData(string fieldName)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolationScriptVariableData>>();
            var dbResult2 = await repo.GetAsync<List<DBViolationScriptVariableData>>();
            var dbResult = await repo.GetAsync<List<DBViolationScriptVariableData>>(_ => _.FieldName == fieldName);

            return dbResult.Select(_ => new ViolationScriptVariableData
            {
                Data = _.Data,
                FieldName = _.FieldName,
                Sequence = _.Sequence
            }).ToList();
        }
    }
}