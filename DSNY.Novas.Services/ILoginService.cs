﻿using System.Threading.Tasks;

namespace DSNY.Novas.Services
{
    public interface ILoginService
    {
        Task<bool> AuthenticateUser(string userId, string pin, string nfcUid);
        Task<bool> IsValidUser(string userId);
        Task<string> GetUserIdFromCardID(string pin, string nfcUid);
        Task<string> GetUserNameFromCardID(string nfcUid);
        Task<string> GetUserIDFromCardID(string nfcUid);
        Task<string> GetUserAgencyFromCardID(string nfcUid);
    }
}