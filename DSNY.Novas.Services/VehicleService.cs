﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;
using System;

namespace DSNY.Novas.Services
{
    public class VehicleService : IVehicleService
    {
        public async Task<int> SaveVehicleRadio(VehicleRadioInfo vehicleRadio)
        {
            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
            var dbVRInfo = new DBVehicleRadioInfo
            {
                UserId = vehicleRadio.UserId,
                LoginTimestamp = vehicleRadio.LoginTimestamp,
                VehicleRadioId = vehicleRadio.VehicleRadioId,
                VehicleId = vehicleRadio.VehicleId,
                RadioId = vehicleRadio.RadioId,
                StartMileage = vehicleRadio.StartMileage,
                EndMileage = vehicleRadio.EndMileage
            };

            return await repo.InsertAsync(dbVRInfo);
        }

        // NH-1307
        public async Task<List<VehicleRadioInfo>> GetAllVehicleRadioInfoByUser(string userId) {
            List<VehicleRadioInfo> resultVRInfo = new List<VehicleRadioInfo>();
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
                List<DBVehicleRadioInfo> dbVRInfo = await repo.GetAsync();

                if (dbVRInfo.Count != 0)
                {
                    dbVRInfo = dbVRInfo.Where(_ => _.UserId == userId).ToList();

                    if (dbVRInfo.Count != 0)
                    {
                        foreach(DBVehicleRadioInfo dbvri in dbVRInfo)
                        {
                            var vrInfo = new VehicleRadioInfo
                            {
                                UserId = dbvri.UserId,
                                LoginTimestamp = dbvri.LoginTimestamp,
                                VehicleRadioId = dbvri.VehicleRadioId,
                                VehicleId = dbvri.VehicleId,
                                RadioId = dbvri.RadioId,
                                StartMileage = dbvri.StartMileage,
                                EndMileage = dbvri.EndMileage
                            };
                            resultVRInfo.Add(vrInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return resultVRInfo;
        }
        public async Task<VehicleRadioInfo> GetCurrentVehicleRadioInfoByUser(string userId)
        {
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
                List<DBVehicleRadioInfo> dbVRInfo = await repo.GetAsync();

                //Remove test VR Info from dbVRInfo
                //  var VRInfoWithoutTest = dbVRInfo.Where(x => x.UserId != "dsny00065").ToList();

                if (dbVRInfo.Count != 0)
                {
                    DateTime maxLoginDT = dbVRInfo.Max(_ => _.LoginTimestamp);
                    dbVRInfo = dbVRInfo.Where(_ => _.UserId == userId && _.LoginTimestamp == maxLoginDT).ToList();
                    
                    if (dbVRInfo.Count != 0)
                    {//dbVRInfo can be empty... what happens in this scenario? 
                        int max = dbVRInfo.Max(_ => _.VehicleRadioInfoKey);
                        DBVehicleRadioInfo vr = dbVRInfo.Find(_ => _.VehicleRadioInfoKey == max);

                        var vrInfo = new VehicleRadioInfo
                        {
                            UserId = vr.UserId,
                            LoginTimestamp = vr.LoginTimestamp,
                            VehicleRadioId = vr.VehicleRadioId,
                            VehicleId = vr.VehicleId,
                            RadioId = vr.RadioId,
                            StartMileage = vr.StartMileage,
                            EndMileage = vr.EndMileage
                        };

                        return vrInfo;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new VehicleRadioInfo();
        }

        public async Task<VehicleRadioInfo> GetCurrentVehicleRadioInfo(string userId)
        {
            try
            {
                var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
                List<DBVehicleRadioInfo> dbVRInfo = await repo.GetAsync();

                if (dbVRInfo.Count != 0)
                {
                    dbVRInfo = dbVRInfo.Where(_ => _.UserId == userId && _.EndMileage == 0).ToList();

                    if (dbVRInfo.Count != 0)
                    {//dbVRInfo can be empty... what happens in this scenario? 
                        int max = dbVRInfo.Max(_ => _.VehicleRadioInfoKey);
                        DBVehicleRadioInfo vr = dbVRInfo.Find(_ => _.VehicleRadioInfoKey == max);

                        var vrInfo = new VehicleRadioInfo
                        {
                            UserId = vr.UserId,
                            LoginTimestamp = vr.LoginTimestamp,
                            VehicleRadioId = vr.VehicleRadioId,
                            VehicleId = vr.VehicleId,
                            RadioId = vr.RadioId,
                            StartMileage = vr.StartMileage,
                            EndMileage = vr.EndMileage
                        };

                    return vrInfo;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new VehicleRadioInfo(); 
        }

        //Just checks the most recent entry to see if it has an ending mileage
        public async Task<bool> isVehicleCurrent(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();

            List<DBVehicleRadioInfo> dbVRInfo = await repo.GetAsync();

            if (!dbVRInfo.Where(x=>x.UserId == userId).Any())
            {
                return false;
            }

            var latestEntry = dbVRInfo.ElementAt(dbVRInfo.Count - 1);
            return latestEntry.EndMileage == 0;
        }

        public async Task<int> UpdateVehicleRadio(VehicleRadioInfo vehicleRadio)
        {
            if (vehicleRadio == null) //NH-965
                return 0;

            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
            
            var dbVRInfo = await repo.GetAsync(_ => _.UserId == vehicleRadio.UserId
                                                 && _.VehicleId == vehicleRadio.VehicleId
                                                 && _.StartMileage == vehicleRadio.StartMileage
                                                 && _.EndMileage == 0);

            if (dbVRInfo == null) //NH-965
                return 0;
            
            dbVRInfo.EndMileage = vehicleRadio.EndMileage;

            return await repo.UpdateAsync(dbVRInfo);
        }

        public async Task<short> GetLatestRecordEntry(string vehicleId, string radioId) {
            return 0;
        }
        public async Task<short> GetLatestVehicleRadioId()
        {
            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();

            List<DBVehicleRadioInfo> db = await repo.GetAsync();
            //TODO: Shouldn't this be incrementing (VehicleRadioId)
            if (db.Count > 0)
            {
                var max = db.Max(x => x.VehicleRadioId);

                return max;
            }
            return 0;
        }
        public async Task<short> GetLatestVehicleRadioIdByUserId(string userId) {
            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();
            List<DBVehicleRadioInfo> dbVRInfo = await repo.GetAsync();

            if (dbVRInfo.Count != 0)
            {
                dbVRInfo = dbVRInfo.Where(_ => _.UserId == userId && _.EndMileage == 0).ToList();

                if (dbVRInfo.Count != 0)
                {//dbVRInfo can be empty... what happens in this scenario? 
                    int max = dbVRInfo.Max(_ => _.VehicleRadioInfoKey);

                    return (short)max;
                }
            }
            return 0;
        }

        public async Task<VehicleRadioInfo> GetFinishedVehicleRadioInfo(string userId)
        {
            var repo = DependencyResolver.Get<IRepository<DBVehicleRadioInfo>>();

            var dbVRInfo = await repo.GetAsync();
            dbVRInfo = dbVRInfo.Where(_ => _.UserId == userId && _.EndMileage > 0).ToList();
            int max = dbVRInfo.Max(_ => _.VehicleRadioInfoKey);
            DBVehicleRadioInfo vr = dbVRInfo.Find(_ => _.VehicleRadioInfoKey == max);

            var vrInfo = new VehicleRadioInfo
            {
                UserId = vr.UserId,
                LoginTimestamp = vr.LoginTimestamp,
                VehicleRadioId = vr.VehicleRadioId,
                VehicleId = vr.VehicleId,
                RadioId = vr.RadioId,
                StartMileage = vr.StartMileage,
                EndMileage = vr.EndMileage
            };

            return vrInfo;
        }
    }
}