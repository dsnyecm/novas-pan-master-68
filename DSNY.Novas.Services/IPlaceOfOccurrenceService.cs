﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IPlaceOfOccurrenceService
    {
        Task<List<StreetCodeMaster>> GetBoroCodeByStreet(string street);
        Task<PropertyDetails> GetBBLNumber(int streetCode, string houseNumber);
        Task<List<FirstDescriptors>> GetFirstDescriptors(string violationType);
        Task<List<string>> GetIdentifiers();
        Task<List<BoroMaster>> GetBoros();
        Task<List<District>> GetDistricts(string boroId);
        Task<List<Sections>> GetSections(string districtId);
        Task<StreetCodeMaster> LookUpByStreetCode(int streetId, string boroId);
        Task<StreetCodeMaster> FindStreet(string streetName, string boroId);
        Task<List<StreetCodeMaster>> GetStreetSearchResults(string searchWord, string boroId);
        Task<PropertyDetails> GetPropertyDetails(int streetCode, string houseNumber, string boroId);
        Task<PropertyDetails> GetPropertyDetails(int streetCode, string houseNumberBox1, string houseNumberBox2, string ddentifier, string boroId);
        Task<List<PropertyDetails>> GetAllBBLNumbers(string bblNumber);
        Task<RoutingTime> GetRoutingTimes(string districtId, string sectionId, string boroId, string violationType);
        Task<RoutingTime> GetRoutingTimes(string violationType);
        Task<List<Violator>> GetViolators(string boroCode, int streetCode, string lowHouseNo, string violationCode, string firstNameOfProperty);
        Task<List<Violator>> GetViolators(string boroCode, int streetCode, string lowHouseNo, string violationCode);
        Task<List<Violator>> GetViolatorsForVacantLot(string violationCode, string bblnumber);
        Task SaveViolator(NovMaster novMaster);

        // added for DOF
        Task<PropertyDetails> GetPropertyDetailsByLicenseId(string licenseId);

       
    }
}
