﻿using System.Threading.Tasks;
using DSNY.Novas.Data;

namespace DSNY.Novas.Services
{
    public interface ISyncService
    {
        Task Sync();
    }
}