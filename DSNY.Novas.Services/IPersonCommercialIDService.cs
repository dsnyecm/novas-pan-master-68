﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public interface IPersonCommercialIDService
    {
        Task<List<IdMatrix>> GetIssuedBy();
        Task<List<IdMatrix>> GetIDTypes();
        Task<List<IdMatrix>> GetIssuedByStates();
    }
}
