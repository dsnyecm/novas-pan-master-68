﻿using System;
using System.Collections.Generic;
using System.IO;
using DSNY.Novas.Common;
using DSNY.Novas.Common.Interfaces;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services
{
    public class BaseService
    {
        static BaseService()
        {
#if !TEST
            DependencyResolver.Set<SQLiteRepository<DBViolationGroups>>();
            DependencyResolver.Set<SQLiteRepository<DBViolationTypes>>();
            DependencyResolver.Set<SQLiteRepository<DBViolationDetails>>();
            DependencyResolver.Set<SQLiteRepository<DBViolationScriptVariables>>();
            DependencyResolver.Set<SQLiteRepository<DBViolationScriptVariableData>>();
            DependencyResolver.Set<SQLiteRepository<DBViolationLawCodes>>();
            DependencyResolver.Set<SQLiteRepository<DBBoroMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBFirstDescriptors>>();
            DependencyResolver.Set<SQLiteRepository<DBDistricts>>();
            DependencyResolver.Set<SQLiteRepository<DBLookup>>();
            DependencyResolver.Set<SQLiteRepository<DBPropertyDetails>>();
            DependencyResolver.Set<SQLiteRepository<DBDCALicenseDetails>>();
            DependencyResolver.Set<SQLiteRepository<DBIdMatrix>>();
            DependencyResolver.Set<SQLiteRepository<DBNovasUser>>();
            DependencyResolver.Set<SQLiteRepository<DBStreetCodeMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBNovData>>();
            DependencyResolver.Set<SQLiteRepository<DBDeviceTicketRanges>>();
            DependencyResolver.Set<DatabaseSynchronizer>();
            DependencyResolver.Set<SQLiteRepository<DBSections>>();
            DependencyResolver.Set<SQLiteRepository<DBNovInformation>>();
            DependencyResolver.Set<SQLiteRepository<DBNovData>>();
            DependencyResolver.Set<SQLiteRepository<DBVehicleRadioInfo>>();
            DependencyResolver.Set<SQLiteRepository<DBCodeLaw>>();
            DependencyResolver.Set<SQLiteRepository<DBHearingDateTime>>();
            DependencyResolver.Set<SQLiteRepository<DBHolidayMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBAgencies>>();
            DependencyResolver.Set<SQLiteRepository<DBTitleReportLevel>>();
            DependencyResolver.Set<SQLiteRepository<DBNovasUserMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBAddressSuffixMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBDevices>>();
            DependencyResolver.Set<SQLiteRepository<DBRoutingTime>>();
            DependencyResolver.Set<SQLiteRepository<DBHearingDates>>();
            DependencyResolver.Set<SQLiteRepository<DBViolator>>();
            DependencyResolver.Set<SQLiteRepository<DBAffidavitOfService>>();
            DependencyResolver.Set<SQLiteRepository<DBAffidavitOfServiceTran>>();
            DependencyResolver.Set<SQLiteRepository<DBCourtLocations>>();
            DependencyResolver.Set<SQLiteRepository<DBApplicationLog>>();
            DependencyResolver.Set<SQLiteRepository<DBCancelNovData>>();
            DependencyResolver.Set<SQLiteRepository<DBCancelNovInformation>>();
            DependencyResolver.Set<SQLiteRepository<DBDutyHeader>>();
            DependencyResolver.Set<SQLiteRepository<DBLaterSvcAOSTran>>();
            DependencyResolver.Set<SQLiteRepository<DBLaterSvcData>>();
            DependencyResolver.Set<SQLiteRepository<DBLaterSvcDutyH>>();
            DependencyResolver.Set<SQLiteRepository<DBLaterSvcInfo>>();
            DependencyResolver.Set<SQLiteRepository<DBCodeEnforceControl>>();
            DependencyResolver.Set<SQLiteRepository<DBBoroSiteMaster>>();
            DependencyResolver.Set<SQLiteRepository<DBLocalDeviceSettings>>();
            DependencyResolver.Set<SQLiteRepository<DBNovPublicKey>>();
#endif
        }
    }
}