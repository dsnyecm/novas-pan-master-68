﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class PlaceOfOccurrenceService : BaseService, IPlaceOfOccurrenceService
    {
        public async Task<List<FirstDescriptors>> GetFirstDescriptors(string violationType)
        {
            var repo = DependencyResolver.Get<IRepository<DBFirstDescriptors>>();
            var dbFirstDescriptors2 = await repo.GetAsync<DBFirstDescriptors>();
            var dbFirstDescriptors = await repo.GetAsync<DBFirstDescriptors>(_ => _.ViolationTypes.Contains(violationType));

            var firstDescriptors = dbFirstDescriptors.Select(_ => new FirstDescriptors
            {
                Code = _.Code,
                Description = _.Description,
                ViolationTypeDesc = _.ViolationTypeDesc,
                GoElement = _.GoElement,
                GroupName = _.GroupName,
                Sort = _.Sort
            }).OrderBy(_ => _.Sort).ToList();

            return firstDescriptors;

        }

        public async Task<List<string>> GetIdentifiers()
        {
            var repo = DependencyResolver.Get<IRepository<DBAddressSuffixMaster>>();
            var dbIndicators = await repo.GetAsync();

            var indicators = dbIndicators.Select(_ => _.SuffixName).OrderBy(_ => _).ToList();
            indicators.Insert(0, "--");

            return indicators;
        }

        public async Task<List<BoroMaster>> GetBoros()
        {
            var repo = DependencyResolver.Get<IRepository<DBBoroMaster>>();
            var dbBoros = await repo.GetAsync();

            var boros = dbBoros.Select(_ => new BoroMaster
            {
                BoroId = _.BoroId,
                Name = _.Name,
                ShortName = _.ShortName
            }).OrderBy(_ => _.BoroId).ToList();

            boros = boros.Where(b => b.BoroId != "0").ToList();

            return boros;
        }

        public async Task<List<District>> GetDistricts(string boroId)
        {
            var repo = DependencyResolver.Get<IRepository<DBDistricts>>();
            var dbDistricts = await repo.GetAsync();

            var districts = dbDistricts.Select(_ => new District
            {
                Name = _.Name,
                BoroCode = _.BoroCode,
                Description = _.Description,
                DistrictId = _.DistrictId
            }).Where((district) => district.BoroCode.Equals(boroId)).OrderBy(_ => _.Description).ToList();

            return districts;
        }

        public async Task<List<Sections>> GetSections(string districtId)
        {
            var repo = DependencyResolver.Get<IRepository<DBSections>>();
            var dbSections = await repo.GetAsync<List<DBSections>>(_ => _.DistrictId.Equals(districtId));

            return dbSections.Select(_ => new Sections
            {
                DistrictId = _.DistrictId,
                SectionId = (_.SectionId.Replace(" ", "")),
                    Name = _.Name,
                    Description = _.Description
                })
                .OrderBy(_ => Convert.ToInt32(_.SectionId))
                .ToList();
        }

        public async Task<StreetCodeMaster> LookUpByStreetCode(int streetId, string boroId)
        {
            var query = string.Format("SELECT DISTINCT * FROM DBStreetCodeMaster WHERE BoroCode = {0} AND StreetCode = {1}", boroId, streetId);

            var repo = DependencyResolver.Get<IRepository<DBStreetCodeMaster>>();
            var dbStreetCodeMaster = await repo.QueryAsync(query);


            return dbStreetCodeMaster.Select(_ => new StreetCodeMaster
            {
                BoroCode = _.BoroCode,
                StreetCode = _.StreetCode,
                StreetName = _.StreetName
            }).FirstOrDefault();
        }
        
        public async Task<List<StreetCodeMaster>> GetBoroCodeByStreet(string street)
        {
           
            var repo = DependencyResolver.Get<IRepository<DBStreetCodeMaster>>();
            var dbStreetCodeMaster = await repo.GetAsync<DBStreetCodeMaster>(_ => _.StreetName.Equals(street));

            return dbStreetCodeMaster.Select(_ => new StreetCodeMaster
            {
                BoroCode = _.BoroCode,
                StreetCode = _.StreetCode,
                StreetName = _.StreetName
            }).ToList();
        
        }

        public async Task<PropertyDetails> GetBBLNumber(int streetCode, string houseNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBPropertyDetails>>();
            var dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode == streetCode && _.LowHouseKey.Equals(houseNumber));

            return dbPropertyDetails.Select(_ => new PropertyDetails
            {
                Bbl = _.Bbl,
                VacantLot = _.VacantLot,
                LowHouseNo = _.LowHouseNo,
                StreetCode = _.StreetCode,
                Pop = _.Pop,
                DistrictId = _.DistrictId,
                SectionId = _.SectionId,
                FirstName = _.FirstName,
                ZipCode = _.ZipCode,
                MdrNo = _.MdrNo
            }).FirstOrDefault();
        }

        public async Task<StreetCodeMaster> FindStreet(string streetName, string boroId)
        {
            // var query = string.Format("SELECT DISTINCT * FROM DBStreetCodeMaster WHERE BoroCode = {0} AND StreetName = '{1}'", boroId, streetName);
           
            var repo = DependencyResolver.Get<IRepository<DBStreetCodeMaster>>();

            var dbStreetCodeMaster = await repo.GetAsync<List<DBStreetCodeMaster>>(_ => _.BoroCode.Equals(boroId) && _.StreetName.Equals(streetName));
            
           // var dbStreetCodeMaster = await repo.QueryAsync(query);

            return dbStreetCodeMaster.Select(_ => new StreetCodeMaster
            {
                BoroCode = _.BoroCode,
                StreetCode = _.StreetCode,
                StreetName = _.StreetName
            }).FirstOrDefault();
        }

        public async Task<List<StreetCodeMaster>> GetStreetSearchResults(string searchWord, string boroId)
        {
           // var query = string.Format("SELECT DISTINCT * FROM DBStreetCodeMaster WHERE BoroCode = {0} AND StreetName LIKE '{1}%'", boroId, searchWord);
            
            var repo = DependencyResolver.Get<IRepository<DBStreetCodeMaster>>();

            StringComparison sc = StringComparison.OrdinalIgnoreCase;

            //var dbStreetCodeMaster = await repo.GetAsync<List<DBStreetCodeMaster>>(_ => _.BoroCode.Equals(boroId) && _.StreetName.ToUpper().Contains(searchWord.ToUpper()));
            var dbStreetCodeMaster = await repo.GetAsync<List<DBStreetCodeMaster>>(_ => _.BoroCode.Equals(boroId) && _.StreetName.ToUpper().StartsWith(searchWord.ToUpper(), sc));

            return dbStreetCodeMaster.Select(_ => new StreetCodeMaster
            {
                BoroCode = _.BoroCode,
                StreetCode = _.StreetCode,
                StreetName = _.StreetName
            }).ToList();
        }

        public async Task<PropertyDetails> GetPropertyDetails(int streetCode, string houseNumber, string boroId)
        {
            var repo = DependencyResolver.Get<IRepository<DBPropertyDetails>>();
            var dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(houseNumber));

            return dbPropertyDetails.Select(_ => new PropertyDetails
            {
                Bbl = _.Bbl,
                VacantLot = _.VacantLot,
                LowHouseNo = _.LowHouseNo,
                StreetCode = _.StreetCode,
                Pop = _.Pop,
                DistrictId = _.DistrictId,
                SectionId = _.SectionId,
                FirstName = _.FirstName,
                ZipCode = _.ZipCode,
                MdrNo = _.MdrNo
            }).FirstOrDefault(_ => _.Bbl.ToString()[0].Equals(boroId[0]));
        }

        //Added for DBDCALicenseDetails
        public async Task<PropertyDetails> GetPropertyDetailsByLicenseId(string licenseId)
        {

            var repo = DependencyResolver.Get<IRepository<DBDCALicenseDetails>>();

            ///var dbPropertyDetails2 = await repo.GetAsync<DBDCAPropertyDetails>();


           // var dbPropertyDetails = await repo.GetAsync<DBDCALicenseDetails>(_ => _.License.Equals(licenseId));
            var dbPropertyDetails = await repo.GetAsync<DBDCALicenseDetails>(_ => _.License.Equals(licenseId) || _.AmendementNumber.Equals(licenseId) || _.ApplicationNumber.Equals(licenseId) || _.RenwalNumber.Equals(licenseId)) ;

            return dbPropertyDetails.Select(_ => new PropertyDetails
            {
                LicenseId = _.License,
                ExpirationDate = _.ExpirationDate,
                BusinessName = _.BusinessName,
                TradeName = _.TradeName,
                PremiseBuildingNumber = _.PremiseBuildingNumber,
                PremiseStreetName = _.PremiseStreetName,
                PremsieLocation = _.PremiseLocation,
                PremsieCity = _.PremiseCity,
                PremiseState = _.PremiseState,
                PremiseZipCode = _.PremiseZipCode,
                PremiseCouncilDist = _.PremiseCouncilDist,
                PremiseHouseNumber = _.PremiseHouseNumber,
                Bbl = !string.IsNullOrEmpty(_.Bbl) ? Convert.ToInt64(_.Bbl) : 0,
                PremiseCommunityDist = _.PremiseCommunityDist,
                MailingBuildingNumber = _.MailingBuildingNumber,
                MailingStreetName = _.MailingStreetName,
                MailingCity = _.MailingCity,
                MailingState = _.MailingState,
                MailingZipCode = _.MailingZipCode,
                Phone = _.Phone,
                Email = _.Email,
                Fax = _.Fax,
                // PremiseCouncilDist = Convert.ToInt32(_.PolicePrecinct)
                DistrictId = Convert.ToInt32(_.PolicePrecinct)

            }).FirstOrDefault();
        }

        // fix for bug # NH-467 and bug 450 (pass all three house parameter to identify address.
        public async Task<PropertyDetails> GetPropertyDetails(int streetCode, string lowHouseNo, string lowHouseSFx, string identifier, string boroId)
        {
            int ltlowHouseNo = 0;
            if (lowHouseNo != "")
                ltlowHouseNo = Convert.ToInt32(lowHouseNo);

            var repo = DependencyResolver.Get<IRepository<DBPropertyDetails>>();
            var dbPropertyDetails = new List<DBPropertyDetails>();
            var dbPropertyDetails1 = new List<DBPropertyDetails>();
            var dbPropertyDetailsEven = new List<DBPropertyDetails>();
            var dbPropertyDetailsOdd = new List<DBPropertyDetails>();

            bool identifierIsEmpty = (String.IsNullOrEmpty(identifier) || identifier.Equals("--", StringComparison.OrdinalIgnoreCase));

            //if (!String.IsNullOrEmpty(lowHouseSFx) && !String.IsNullOrEmpty(identifier))
            if (!String.IsNullOrEmpty(lowHouseSFx) && !identifierIsEmpty)
            {
                dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo && ltlowHouseNo <= _.HighHouseNo && (_.LowHouseInd== identifier || _.HighHouseInd == identifier));
                if(dbPropertyDetails1.Any())
                {
                    dbPropertyDetails = dbPropertyDetails1;
                }
                if(dbPropertyDetails1.Count()==0)
                {
                    dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(ltlowHouseNo));
                    dbPropertyDetails = dbPropertyDetails1.Where(_ => Convert.ToInt32(lowHouseSFx) >= Convert.ToInt32(_.LowHouseSfx.Trim()) && Convert.ToInt32(lowHouseSFx) <= Convert.ToInt32(_.HighHouseSfx.Trim()) && _.LowHouseInd.Trim().ToUpper() == identifier.ToUpper()).ToList();
                }

               
            }
            //else if (!String.IsNullOrEmpty(lowHouseSFx) && String.IsNullOrEmpty(identifier))
            else if (!String.IsNullOrEmpty(lowHouseSFx) && identifierIsEmpty)
            {
                // if low HouseNo and High HoughNO is not equal

                dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo && ltlowHouseNo <= _.HighHouseNo);
                if (dbPropertyDetails1.Any())
                {
                    dbPropertyDetails = dbPropertyDetails1;
                }
                if (dbPropertyDetails1.Count() == 0)
                {

                    dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(ltlowHouseNo));
                    //dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(ltlowHouseNo) && _.LowHouseSfx.Equals(lowHouseSFx));
                    dbPropertyDetails = dbPropertyDetails1.Where(_ => Convert.ToInt32(lowHouseSFx) >= Convert.ToInt32(_.LowHouseSfx.Trim()) && Convert.ToInt32(lowHouseSFx) <= Convert.ToInt32(_.HighHouseSfx.Trim())).ToList();
                }


            }
            //else if (String.IsNullOrEmpty(lowHouseSFx) && !String.IsNullOrEmpty(identifier))
            else if (String.IsNullOrEmpty(lowHouseSFx) && !identifierIsEmpty)
            {
                dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo && ltlowHouseNo <= _.HighHouseNo && (_.LowHouseInd == identifier || _.HighHouseInd == identifier));
                if (dbPropertyDetails1.Any())
                {
                    dbPropertyDetails = dbPropertyDetails1;
                }
                if (dbPropertyDetails1.Count() == 0)
                {
                    dbPropertyDetails1 = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(ltlowHouseNo) && ltlowHouseNo >= _.LowHouseNo && ltlowHouseNo <= _.HighHouseNo);
                    dbPropertyDetails = dbPropertyDetails1.Where(_ => _.LowHouseInd.Trim().ToUpper() == identifier.ToUpper()).ToList();
                }
            }
            else
            {
                dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ => _.StreetCode.Equals(streetCode) && _.LowHouseNo.Equals(ltlowHouseNo));


                /*
                // if you do not find record while using the above condition then check range between LowerHouseNo  and HighHouseNo
                if (!dbPropertyDetails.Any())
                {
                    string continuousParity = "L";
                    if (!string.IsNullOrEmpty(lowHouseNo) && Convert.ToInt32(lowHouseNo) % 2 == 0)
                        continuousParity = "R";
                    dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ =>
                    _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo &&
                    ltlowHouseNo <= _.HighHouseNo && _.ContinuousParity.Equals(continuousParity));
                }
                // when LowHouseKey is having "183-37" kind of address then continuousParity logic flip off.
                if (!dbPropertyDetails.Any())
                {
                    string continuousParity = "R";
                    if (!string.IsNullOrEmpty(lowHouseNo) && Convert.ToInt32(lowHouseNo) % 2 == 0)
                        continuousParity = "L";
                    dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ =>
                    _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo &&
                    ltlowHouseNo <= _.HighHouseNo && _.ContinuousParity.Equals(continuousParity));
                }
             */

                //new stuff
                if (!dbPropertyDetails.Any())
                {
                    dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ =>
                   _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo &&
                   ltlowHouseNo <= _.HighHouseNo);

                    //filter even or odd
                    foreach (var item in dbPropertyDetails)
                    {
                        if (Convert.ToInt32(item.LowHouseNo) % 2 == 0 && Convert.ToInt32(item.HighHouseNo) % 2 == 0)
                        {
                            dbPropertyDetailsEven.Add(item);
                        }
                        else
                        {
                            dbPropertyDetailsOdd.Add(item);
                        }
                    }
                    if (Convert.ToInt32(ltlowHouseNo) % 2 == 0)
                    {
                        dbPropertyDetails = dbPropertyDetailsEven;
                    }
                    else
                    {
                        dbPropertyDetails = dbPropertyDetailsOdd;
                    }

                }

            }

            /*
            //var locaPds = dbPropertyDetails.ToList();

            // if you do not find record while using the abov condition then check range between LowerHouseNo  and HighHouseNo
            if (dbPropertyDetails.Count() == 0)
            {

                string continuousParity = "L";
                if (!string.IsNullOrEmpty(lowHouseNo))
                {
                    if (Convert.ToInt32(lowHouseNo) % 2 == 0)
                        continuousParity = "R";
                }


                dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ =>
                    _.StreetCode.Equals(streetCode) && _.LowHouseNo != _.HighHouseNo && ltlowHouseNo >= _.LowHouseNo &&
                    ltlowHouseNo <= _.HighHouseNo && _.ContinuousParity.Equals(continuousParity));
            }
            */
            return dbPropertyDetails.Select(_ => new PropertyDetails
            {
                Bbl = _.Bbl,
                VacantLot = _.VacantLot,
                LowHouseNo = _.LowHouseNo,
                StreetCode = _.StreetCode,
                Pop = _.Pop,
                DistrictId = _.DistrictId,
                SectionId = _.SectionId,
                FirstName = _.FirstName,
                ZipCode = _.ZipCode,
                MdrNo = _.MdrNo
            }).FirstOrDefault(_ => _.Bbl.ToString()[0].Equals(boroId[0]));
        }
        public async Task<List<PropertyDetails>> GetAllBBLNumbers(string bblNumber)
        {
            var repo = DependencyResolver.Get<IRepository<DBPropertyDetails>>();
            var dbPropertyDetails = await repo.GetAsync<DBPropertyDetails>(_ => _.Bbl.Equals(bblNumber));

            return dbPropertyDetails.Select(_ => new PropertyDetails
            {
                Bbl = _.Bbl,
                VacantLot = _.VacantLot,
                LowHouseNo = _.LowHouseNo,
                StreetCode = _.StreetCode,
                Pop = _.Pop,
                DistrictId = _.DistrictId,
                SectionId = _.SectionId,
                FirstName = _.FirstName,
                ZipCode = _.ZipCode,
                MdrNo = _.MdrNo
            }).ToList();
        }
        
        public async Task<RoutingTime> GetRoutingTimes(string districtId, string sectionId, string boroId, string violationType)
        {
            var repo = DependencyResolver.Get<IRepository<DBRoutingTime>>();

            DBRoutingTime dbRoutingTime = await repo.GetAsync(_ => _.DistrictId == districtId && _.SectionId == sectionId &&
                                          _.ViolationType == violationType && _.BoroCode == boroId);

            if (dbRoutingTime != null)
            {
                return new RoutingTime
                {
                    EffectDate = dbRoutingTime.EffectDate,
                    RoutingTimePM = dbRoutingTime?.RoutingTimePm,
                    RoutingTimeAM = dbRoutingTime?.RoutingTimeAm
                };
            }
            return null;
        }

        //Get routingtime for Residential properties only 
        public async Task<RoutingTime> GetRoutingTimes(string violationType)
        {
            var repo = DependencyResolver.Get<IRepository<DBRoutingTime>>();

            DBRoutingTime dbRoutingTime = await repo.GetAsync(_ => _.ViolationType == violationType);

            if (dbRoutingTime != null)
            {
                return new RoutingTime
                {
                    EffectDate = dbRoutingTime.EffectDate,
                    RoutingTimePM = dbRoutingTime?.RoutingTimePm,
                    RoutingTimeAM = dbRoutingTime?.RoutingTimeAm
                };
            }
            return null;
        }

        public string CleanBusinessName(string value)
        {
            var cleanedBusinessName = value?.Replace(",", "").Replace("  ", " ").Replace("'", "''").Trim();
            if (cleanedBusinessName?.EndsWith(".") == true)
                cleanedBusinessName = cleanedBusinessName.Remove(cleanedBusinessName.Length - 1);

            return cleanedBusinessName;
        }

        public async Task<List<Violator>> GetViolators(string boroCode, int streetCode, string lowHouseNo, string violationCode, string firstNameOfProperty)
        {
            var query = $"SELECT DISTINCT * FROM DBViolator WHERE BoroCode = '{boroCode}' AND StreetCode = {streetCode} AND trim(LowHouseNo) = '{lowHouseNo}' and trim(ViolationCode) = '{violationCode}' and trim(BusinessName) = '{CleanBusinessName(firstNameOfProperty)}'";

            var repo = DependencyResolver.Get<IRepository<DBViolator>>();
        
            List<DBViolator> dbViolator = await repo.QueryAsync(query);
            
            if (dbViolator != null)
            {
                return dbViolator.Select(_ => new Violator
                {
                    BoroCode = _.BoroCode,
                    StreetCode = _.StreetCode,
                    LowHouseNo = _.LowHouseNo,
                    BatchDate = _.BatchDate,
                    BusinessName = _.BusinessName,
                    ViolationCode = _.ViolationCode,
                    NextViolationCode = _.NextViolationCode,
                    MinFine = _.MinFine,
                    MaxFine = _.MaxFine,
                    NoOfTimes = _.NoOfTimes,
                    DisplaySign = _.DisplaySign
                }).ToList();
            }

            return new List<Violator>();
        }

        public async Task<List<Violator>> GetViolators(string boroCode, int streetCode, string lowHouseNo, string violationCode)
        {
            var sb = new StringBuilder()
                .AppendLine("SELECT DISTINCT BoroCode, StreetCode, LowHouseNo, BatchDate, BusinessName, ViolationCode, NextViolationCode, MinFine, MaxFine, NoOfTimes, DisplaySign")
                .AppendLine("FROM DBViolator")
                .AppendLine($"WHERE BoroCode = '{boroCode}' AND StreetCode = {streetCode} AND trim(LowHouseNo) = '{lowHouseNo}' and trim(ViolationCode) = '{violationCode}'")
                .AppendLine("ORDER BY BusinessName");

            var dbViolatorList = await DependencyResolver.Get<IRepository<DBViolator>>().QueryAsync(sb.ToString());

            if (dbViolatorList == null || !dbViolatorList.Any())
                return new List<Violator>();

            return dbViolatorList.Select(_ => new Violator
            {
                BoroCode = _.BoroCode,
                StreetCode = _.StreetCode,
                LowHouseNo = _.LowHouseNo,
                BatchDate = _.BatchDate,
                BusinessName = _.BusinessName,
                ViolationCode = _.ViolationCode,
                NextViolationCode = _.NextViolationCode,
                MinFine = _.MinFine,
                MaxFine = _.MaxFine,
                NoOfTimes = _.NoOfTimes,
                DisplaySign = _.DisplaySign
            }).ToList();
        }

        public async Task<List<Violator>> GetViolatorsForVacantLot(string violationCode, string bblnumber)
        {
            //First get property details from BBL Number 
            var propertyRepo = DependencyResolver.Get<IRepository<DBPropertyDetails>>();
            var violatorRepo = DependencyResolver.Get<IRepository<DBViolator>>();
            var propertyQuery = string.Format("SELECT * FROM DBPropertyDetails WHERE BBL = '{0}'",
                bblnumber);

            List<DBPropertyDetails> dbPropertyDetails = await propertyRepo.QueryAsync(propertyQuery);
            
            if (dbPropertyDetails != null)
            {
                //Find all violators with property details from BBL Number
                //And then do a distinct on BusinessName and return those results

                foreach (var property in dbPropertyDetails)
                {
                     var violatorQuery =
                         $"SELECT * FROM DBViolator INNER JOIN DBPropertyDetails ON DBPropertyDetails.StreetCode = {property.StreetCode} AND DBPropertyDetails.LowHouseKey = '{property.LowHouseKey}' Where DBPropertyDetails.VacantLot = 'V' AND DBViolator.ViolationCode = '{violationCode}' AND DBViolator.BusinessName = '{CleanBusinessName(property.FirstName)}'";
                    
                    List<DBViolator> dbViolators = await violatorRepo.QueryAsync(violatorQuery);

                    if (dbViolators != null)
                    {
                        return dbViolators.Select(_ => new Violator
                        {
                            BoroCode = _.BoroCode,
                            StreetCode = _.StreetCode,
                            LowHouseNo = _.LowHouseNo,
                            BatchDate = _.BatchDate,
                            BusinessName = _.BusinessName,
                            ViolationCode = _.ViolationCode,
                            NextViolationCode = _.NextViolationCode,
                            MinFine = _.MinFine,
                            MaxFine = _.MaxFine,
                            NoOfTimes = _.NoOfTimes,
                            DisplaySign = _.DisplaySign
                        }).ToList();
                    }
                    else
                    {
                        return new List<Violator>();
                    }
                }

            }
            return new List<Violator>();
        }
        
        private void MapViolation(DBViolator dbViolator, NovMaster novMaster)
        {
            dbViolator.BatchDate = novMaster.Violator.BatchDate;
            dbViolator.BoroCode = novMaster.Violator.BoroCode;
            dbViolator.BusinessName = novMaster.Violator.BusinessName;
            dbViolator.DisplaySign = novMaster.Violator.DisplaySign;
            dbViolator.LowHouseNo = novMaster.Violator.LowHouseNo;
            dbViolator.MaxFine = novMaster.Violator.MaxFine;
            dbViolator.MinFine = novMaster.Violator.MinFine;
            dbViolator.NextViolationCode = novMaster.Violator.NextViolationCode;
            dbViolator.NoOfTimes = (Int16)novMaster.Violator.NoOfTimes;
            dbViolator.StreetCode = novMaster.Violator.StreetCode;
            dbViolator.ViolationCode = novMaster.Violator.ViolationCode;
        }

        public async Task SaveViolator(NovMaster novMaster)
        {
            var repo = DependencyResolver.Get<IRepository<DBViolator>>();
            var isNewViolator = false;
            var dbViolator = await repo.GetAsync(_ => _.BusinessName == novMaster.Violator.BusinessName);

            if (dbViolator == null)
            {
                isNewViolator = true;
                dbViolator = new DBViolator();
            }

            MapViolation(dbViolator, novMaster);

            if (isNewViolator)
                await repo.InsertAsync(dbViolator);
            else
                await repo.UpdateAsync(dbViolator);
        }
    }
}
