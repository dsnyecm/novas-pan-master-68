﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;
using DSNY.Novas.Models;

namespace DSNY.Novas.Services
{
    public class LookupService : BaseService, ILookupService
    {
        public async Task<List<LookupTable>> GetLookupTable(string tableName)
        {
            var repository = DependencyResolver.Get<IRepository<DBLookup>>();

            List<DBLookup> dbLookup =
                await repository.GetAsync<List<DBLookup>>(
                    _ => _.TableName == tableName);

            var lookupTable = dbLookup.Select(_ => new LookupTable()
            {
                MiscKey = _.DBMiscellaneousKey,
                TableName = _.TableName,
                Code = _.Code,
                Description = _.Description
            }).OrderBy(_ => _.Code).ToList();

            return lookupTable;
        }

        public  async Task<List<LookupTable>> GetLookupCode(string codeName)
        {
            var lookupTable = new List<LookupTable>();
            try
            {

                var repository = DependencyResolver.Get<IRepository<DBLookup>>();

                List<DBLookup> dbLookup =
                    await repository.GetAsync<List<DBLookup>>(
                        _ => _.Code == codeName);

                if (dbLookup.Count > 0)
                {
                    lookupTable = dbLookup.Select(_ => new LookupTable()
                    {
                        MiscKey = _.DBMiscellaneousKey,
                        TableName = _.TableName,
                        Code = _.Code,
                        Description = _.Description
                    }).OrderBy(_ => _.Code).ToList();
                }
            }catch(Exception ex)
            {
                string err = "";
            }

          
           

            return lookupTable;
        }

    }
}
