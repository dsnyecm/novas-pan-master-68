﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DSNY.Novas.Models;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services
{
    public interface IAffidavitService
    {
        Task SaveAffidavit(NovMaster novMaster);
        Task SaveAffidavitTran(NovMaster novMaster);
        Task<List<AffidavitOfServiceTran>> GetAffidavitTranList(long novNumber);
        Task<AffidavitOfService> GetAffidavit(long novNumber);

    }
}