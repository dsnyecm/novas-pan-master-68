﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using DSNY.Novas.Common;
using DSNY.Novas.Data;
using DSNY.Novas.Entities;

namespace DSNY.Novas.Services
{
    public class DeviceService : BaseService, IDeviceService
    {
        public async Task<string> GetDeviceIdForHardware(string hardwareId)
        {
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            DBDevices dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == hardwareId);

#if TEST
            return "TestDevice";
#else
            return dbDevice?.DeviceId;
#endif
        }

        public async Task<string> GetDeviceIdByIMEI(string deviceIMEI)
        {
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            DBDevices dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == deviceIMEI);

#if TEST
            return "TestDevice";
#else
            return dbDevice?.DeviceId.Trim();
#endif
        }

        public async Task<string> GetDeviceIdByIMEIAndBoro(string deviceIMEI, string boroLocation)
        {
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            DBDevices dbDevice;
            if (string.IsNullOrEmpty(boroLocation))
            {
                dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == deviceIMEI);
            } else { 
                dbDevice = await deviceRepo.GetAsync(_ => _.DeviceName == deviceIMEI && _.CurrentLocation.ToUpper() == boroLocation.ToUpper() );
            }
            var dLoc = dbDevice?.CurrentLocation;
#if TEST
            return "TestDevice";
#else
            return dbDevice?.DeviceId.Trim();
#endif
        }
        public async Task<List<DBDevices>> GetAllDevices()
        {
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            List<DBDevices> devices = await deviceRepo.GetAsync();

            return devices;

        }

        public async Task<List<DBDevices>> GetAllPrinters()
        {
            IRepository<DBDevices> deviceRepo = DependencyResolver.Get<IRepository<DBDevices>>();
            List<DBDevices> devices = await deviceRepo.GetAsync();
            List<DBDevices> printerDevices = devices.FindAll(_ => _.DeviceType == "P" && !string.IsNullOrWhiteSpace(_.MacAddress));
            
            return printerDevices;

        }
    }
}
